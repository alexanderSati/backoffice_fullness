<?php defined('BASEPATH') OR exit('No direct script access allowed');

// $config["menu_id"]               = 'id';
// $config["menu_label"]            = 'name';
// $config["menu_parent"]           = 'parent';
// $config["menu_icon"] 			 = 'icon';
$config["menu_key"]              = 'slug';
$config["menu_order"]            = 'number';

$config["nav_tag_open"]          = '<ul id="nav-left-menu" class="pcoded-item pcoded-left-item" item-border="true" item-border-style="solid" subitem-border="true">';
$config["nav_tag_close"]         = '</ul>';
$config["item_tag_open"]         = '<li>'; 
$config["item_tag_close"]        = '</li>';	
$config["parent_tag_open"]       = '<li class="pcoded-hasmenu" dropdown-icon="style1" subitem-icon="style1">';	
$config["parent_tag_close"]      = '</li>';	
$config["parent_anchor_tag"]     = '';
$config["children_tag_open"]     = '<ul class="pcoded-submenu" style="display: none;">';
$config["children_tag_close"]    = '</ul>';	
$config['icon_position']		 = 'left'; // 'left' or 'right'
$config['menu_icons_list']		 = array();
// these for the future version
$config['icon_img_base_url']	 = ''; 