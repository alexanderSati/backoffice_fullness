<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

$config['num_links']            = 3;        // number of links before and after current page
$config['page_query_string']    = TRUE;     // using query string instead of URI segments
$config['query_string_segment'] = "offset"; // change default 'per_page' query string to 'offset'

$config['full_tag_open']        = '<ul class="pagination">';
$config['full_tag_close']       = '</ul>';

$config['first_link']           = '<span aria-hidden="true">«</span><span class="sr-only">Previous</span>';
$config['first_tag_open']       = '<li>';
$config['first_tag_close']      = '</li>';

$config['last_link']            = '<span aria-hidden="true">»</span><span class="sr-only">Next</span>';
$config['last_tag_open']        = '<li>';
$config['last_tag_close']       = '</li>';

$config['prev_link']            = '<span class="fa fa-arrow-left"></span>';
$config['prev_tag_open']        = '<li>';
$config['prev_tag_close']       = '</li>';

$config['next_link']            = '<span class="fa fa-arrow-right"></span>';
$config['next_tag_open']        = '<li>';
$config['next_tag_close']       = '</li>';

$config['cur_tag_open']         = '<li class="page-item active"><a class="page-link" href="#">';
$config['cur_tag_close']        = '</a></li>';

$config['num_tag_open']         = '<li class="active">';
$config['num_tag_close']        = '</li>';