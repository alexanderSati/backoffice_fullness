<?php
defined('BASEPATH') OR exit('No direct script access allowed');

/*
|--------------------------------------------------------------------------
| Display Debug backtrace
|--------------------------------------------------------------------------
|
| If set to TRUE, a backtrace will be displayed along with php errors. If
| error_reporting is disabled, the backtrace will not display, regardless
| of this setting
|
*/
defined('SHOW_DEBUG_BACKTRACE') OR define('SHOW_DEBUG_BACKTRACE', TRUE);

/*
|--------------------------------------------------------------------------
| File and Directory Modes
|--------------------------------------------------------------------------
|
| These prefs are used when checking and setting modes when working
| with the file system.  The defaults are fine on servers with proper
| security, but you may wish (or even need) to change the values in
| certain environments (Apache running a separate process for each
| user, PHP under CGI with Apache suEXEC, etc.).  Octal values should
| always be used to set the mode correctly.
|
*/
defined('FILE_READ_MODE')  OR define('FILE_READ_MODE', 0644);
defined('FILE_WRITE_MODE') OR define('FILE_WRITE_MODE', 0666);
defined('DIR_READ_MODE')   OR define('DIR_READ_MODE', 0755);
defined('DIR_WRITE_MODE')  OR define('DIR_WRITE_MODE', 0755);

/*
|--------------------------------------------------------------------------
| File Stream Modes
|--------------------------------------------------------------------------
|
| These modes are used when working with fopen()/popen()
|
*/
defined('FOPEN_READ')                           OR define('FOPEN_READ', 'rb');
defined('FOPEN_READ_WRITE')                     OR define('FOPEN_READ_WRITE', 'r+b');
defined('FOPEN_WRITE_CREATE_DESTRUCTIVE')       OR define('FOPEN_WRITE_CREATE_DESTRUCTIVE', 'wb'); // truncates existing file data, use with care
defined('FOPEN_READ_WRITE_CREATE_DESCTRUCTIVE') OR define('FOPEN_READ_WRITE_CREATE_DESTRUCTIVE', 'w+b'); // truncates existing file data, use with care
defined('FOPEN_WRITE_CREATE')                   OR define('FOPEN_WRITE_CREATE', 'ab');
defined('FOPEN_READ_WRITE_CREATE')              OR define('FOPEN_READ_WRITE_CREATE', 'a+b');
defined('FOPEN_WRITE_CREATE_STRICT')            OR define('FOPEN_WRITE_CREATE_STRICT', 'xb');
defined('FOPEN_READ_WRITE_CREATE_STRICT')       OR define('FOPEN_READ_WRITE_CREATE_STRICT', 'x+b');

/*
|--------------------------------------------------------------------------
| Exit Status Codes
|--------------------------------------------------------------------------
|
| Used to indicate the conditions under which the script is exit()ing.
| While there is no universal standard for error codes, there are some
| broad conventions.  Three such conventions are mentioned below, for
| those who wish to make use of them.  The CodeIgniter defaults were
| chosen for the least overlap with these conventions, while still
| leaving room for others to be defined in future versions and user
| applications.
|
| The three main conventions used for determining exit status codes
| are as follows:
|
|    Standard C/C++ Library (stdlibc):
|       http://www.gnu.org/software/libc/manual/html_node/Exit-Status.html
|       (This link also contains other GNU-specific conventions)
|    BSD sysexits.h:
|       http://www.gsp.com/cgi-bin/man.cgi?section=3&topic=sysexits
|    Bash scripting:
|       http://tldp.org/LDP/abs/html/exitcodes.html
|
*/
defined('EXIT_SUCCESS')        OR define('EXIT_SUCCESS', 0); // no errors
defined('EXIT_ERROR')          OR define('EXIT_ERROR', 1); // generic error
defined('EXIT_CONFIG')         OR define('EXIT_CONFIG', 3); // configuration error
defined('EXIT_UNKNOWN_FILE')   OR define('EXIT_UNKNOWN_FILE', 4); // file not found
defined('EXIT_UNKNOWN_CLASS')  OR define('EXIT_UNKNOWN_CLASS', 5); // unknown class
defined('EXIT_UNKNOWN_METHOD') OR define('EXIT_UNKNOWN_METHOD', 6); // unknown class member
defined('EXIT_USER_INPUT')     OR define('EXIT_USER_INPUT', 7); // invalid user input
defined('EXIT_DATABASE')       OR define('EXIT_DATABASE', 8); // database error
defined('EXIT__AUTO_MIN')      OR define('EXIT__AUTO_MIN', 9); // lowest automatically-assigned error code
defined('EXIT__AUTO_MAX')      OR define('EXIT__AUTO_MAX', 125); // highest automatically-assigned error code

// DATOS DE API DE PAGO EFECTIVO
// PRODUCCION
defined('AUTH_URL')         OR define('AUTH_URL', 'https://services.pagoefectivo.pe/v1/authorizations');
defined('ACCESS_KEY')       OR define('ACCESS_KEY', 'NDRlOWM0ZGZiYWZmZGZl');
defined('SECRET_KEY')       OR define('SECRET_KEY', 'xbuNZLwdCCDUdy2Pm5nMypkf/I7/w9Hza3uqK+di');
defined('SERVICE_ID')       OR define('SERVICE_ID', '1148');
defined('GENERATE_CIPS_URL') OR define('GENERATE_CIPS_URL', 'https://services.pagoefectivo.pe/v1/cips');

/* TESTING PAGOEFECTIVO
defined('AUTH_URL')         OR define('AUTH_URL', 'http://pre1a.services.pagoefectivo.pe/v1/authorizations');
defined('ACCESS_KEY')       OR define('ACCESS_KEY', 'ZjgyMjg4ODM4YzIyYTk4');
defined('SECRET_KEY')       OR define('SECRET_KEY', '39czOCcm+mnc8qOrTTIwPc3BAfePRVyNHxjMwXYH');
defined('SERVICE_ID')       OR define('SERVICE_ID', '1009');
defined('GENERATE_CIPS_URL') OR define('GENERATE_CIPS_URL', 'http://pre1a.services.pagoefectivo.pe/v1/cips');*/


// DATOS DE API DE VISANET

defined('MERCHANT_ID')         OR define('MERCHANT_ID', '650099665');
defined('JS_SCRIPT_VISANET')   OR define('JS_SCRIPT_VISANET', 'https://static-content.vnforapps.com/v2/js/checkout.js');
defined('URL_AUTH_VISANET')    OR define('URL_AUTH_VISANET', 'https://apiprod.vnforapps.com/api.security/v1/security');
defined('URL_SESSION_VISANET') OR define('URL_SESSION_VISANET', 'https://apiprod.vnforapps.com/api.ecommerce/v2/ecommerce/token/session/'.MERCHANT_ID);
defined('URL_PAY_AUTHORIZATION_VISANET') OR define('URL_PAY_AUTHORIZATION_VISANET', 'https://apiprod.vnforapps.com/api.authorization/v3/authorization/ecommerce/'.MERCHANT_ID);
defined('VISANET_PASSWORD')    OR define('VISANET_PASSWORD', '$7j?5wpZ');
defined('VISANET_USER')        OR define('VISANET_USER', 'felipe@igniweb.com');

/*
TESTING VISANET
defined('MERCHANT_ID')         OR define('MERCHANT_ID', '522591303');
defined('JS_SCRIPT_VISANET')   OR define('JS_SCRIPT_VISANET', 'https://static-content-qas.vnforapps.com/v2/js/checkout.js?qa=true');
defined('URL_AUTH_VISANET')    OR define('URL_AUTH_VISANET', 'https://apitestenv.vnforapps.com/api.security/v1/security');
defined('URL_SESSION_VISANET') OR define('URL_SESSION_VISANET', 'https://apitestenv.vnforapps.com/api.ecommerce/v2/ecommerce/token/session/'.MERCHANT_ID);
defined('URL_PAY_AUTHORIZATION_VISANET') OR define('URL_PAY_AUTHORIZATION_VISANET', 'https://apitestenv.vnforapps.com/api.authorization/v3/authorization/ecommerce/'.MERCHANT_ID);
defined('VISANET_PASSWORD')    OR define('VISANET_PASSWORD', 'd5e7nk$M');
defined('VISANET_USER')        OR define('VISANET_USER', 'integraciones.visanet@necomplus.com');
*/

defined('PATH_LIQUIDATION')    OR define('PATH_LIQUIDATION', '/home/fullnessglobal/public_html/fullness/application/logs');

