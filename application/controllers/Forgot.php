<?php

defined('BASEPATH') OR exit('No direct script access allowed');

/**
 * All  > PUBLIC <  AJAX functions should go in here
 *
 * CSRF protection has been disabled for this controller in the config file
 *
 * IMPORTANT: DO NOT DO ANY WRITEBACKS FROM HERE!!! For retrieving data only.
 */
class Forgot extends Public_Controller {

    /**
     * Constructor
     */
    
    function __construct() {
        parent::__construct();
        // load the users model
        $this->load->model('users_model');
    }

    
    public function index() {
        
        // validators
        $this->form_validation->set_error_delimiters($this->config->item('error_delimeter_left'), $this->config->item('error_delimeter_right'));
        $this->form_validation->set_rules('username', lang('users input username'), 'required|trim|max_length[256]|callback__check_username_exists');

        if ($this->form_validation->run() == TRUE)
        {
            // save the changes
            $results = $this->users_model->reset_password($this->input->post());

            if ($results)
            {
                // build email
                $reset_url  = base_url('login');

                $html = $this->load->view("emails/".$results['language']."/reset_password", $results, TRUE);

                // send email
                $this->load->library('email');
                $this->email->from($this->settings->site_email);
                $this->email->to($results['email']);
                $this->email->subject(sprintf(lang('users msg email_password_reset_title'), $results['first_name']));
                $this->email->message($html);
                $this->email->set_mailtype("html");
                $this->email->send();

                #echo $this->email->print_debugger();

                $this->session->set_flashdata('message', sprintf(lang('users msg password_reset_success'), $results['first_name']));
            }
            else
            {
                $this->session->set_flashdata('error', lang('users error password_reset_failed'));
            }

            // redirect home and display message
            redirect(base_url());
        }

        // setup page header data
        $this->set_title( lang('users title forgot') );

        $this
            ->add_js_theme( "parsley/parsley.min.js" )
            ->add_js_theme( "pages/parsley-validator.js" );

        $data = $this->includes;

        // load views
        $data['content'] = $this->load->view('forgot', $data, TRUE);
        $this->load->view($this->template, $data);
    }

    /**
     * Make sure email exists
     *
     * @param  string $email
     * @return int|boolean
     */
    function _check_email_exists($email)
    {
        if ( ! $this->users_model->email_exists($email))
        {
            $this->form_validation->set_message('_check_email_exists', sprintf(lang('users error email_not_exists'), $email));
            return FALSE;
        }
        else
        {
            return $email;
        }
    }
    
    /**
     * Make sure username exists
     *
     * @param  string $username
     * @return int|boolean
     */
    function _check_username_exists($username)
    {
        if ( ! $this->users_model->username_exists($username))
        {
            $this->form_validation->set_message('_check_username_exists', sprintf(lang('users error username_not_exists'), $username));
            return FALSE;
        }
        else
        {
            return $username;
        }
    }

}
