<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Test extends Private_Controller {

    public function __construct() {
        parent::__construct();
        $this->load->model("Shopping_order_model");
        $this->load->model("orders_model");
        //$this->load->library('pagoefectivo'); 
        $this->load->library('referral');
        $this->load->library('residual');        

        $this->load->library('plan');
        $this->load->library('rank');        

        $this->load->library('binarytree');        


    }
    
    private function index()
    {   

        $this->load->model('ProductsQuantities_model');
        echo "1542<BR/>";
        $this->ProductsQuantities_model->load_quantities_from_purchase("1542");
        echo "1543<BR/>";
        $this->ProductsQuantities_model->load_quantities_from_purchase("1543");
        echo "1544<BR/>";
        $this->ProductsQuantities_model->load_quantities_from_purchase("1544");
        
    }
    
    private function get_parents($id_user){
        echo "<pre>";
        var_dump($this->referral->direct_parents( $id_user, 100, false ));
        echo "</pre>";
    }
    
     private function set_plan(){
        $id_user = '348';
        $plan_id ='6';

        // Orden 
        $this->db->query("
        INSERT INTO `shopping_order` ( `created`, `modified`, `users_id`, `users_id_referal`, `total_value`, `total_discount`, `state`) VALUES
        (	'2019-09-25 16:46:22',	'2019-09-25 16:46:29',	".$id_user.",	NULL,	364.0000,	0.0000,	'approved');
        ");

        $id_orden = $this->db->insert_id();

        // Productos de la orden
        $this->db->query("
        INSERT INTO `shopping_products` (`shopping_order_id`, `shipping_details_id`, `product_id`, `quantity`, `points_residual`, `points_binary`, `value`, `discount`) VALUES
        (".$id_orden.",	0,	292,	2,	0.0000,	0.0000,	182.0000,	0.0000);
        ");

        // transaccion
        $this->db->query("
        INSERT INTO `transactions` ( `id_user`, `cod`, `reference`, `hash`, `description`, `btc`, `rate`, `usd`, `wallet`, `type`, `status`, `reviewer`, `date`, `last_updated`) VALUES
        (	".$id_user.",	'PURCHASE_PLAN',	'".$id_orden."',	'".$plan_id."',	'Compra de plan orden ".$id_orden."',	0,	0,	364,	'USD',	'purchase_plan',	'done',	NULL,	'2019-10-07 16:39:35',	'2019-09-25 20:46:29');");


        //  subcripcion
        $this->db->query("update subscriptions   set id_plan='{$plan_id}', preferencial_user='1' where id_user={$id_user}");
        
        // actualizar base
        $this->db->query("update subscriptions s JOIN plans p on p.id = s.id_plan set s.base = p.base where p.id in (6,7,8,5);");
        
        echo "ok";

    }

}