<?php

defined('BASEPATH') OR exit('No direct script access allowed');

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of signup
 *
 * @author Juan Manuel
 */
class Register extends Public_Controller {

    public function __construct() {
        parent::__construct();
        $this->load->model('users_model');
        $this->load->model('plans_model');
        $this->load->model('subscriptions_model', 'subscriptions');
        $this->load->model('locations_model', 'locations');
        $this->load->library('transaction');
        $this->load->library('wallet');
        $this->load->library('binarytree');
        $this->load->library('referral');
        $this->load->library('rank');
        $this->load->library('orden');

        // upload config
        $config['upload_path'] = "uploads/";
        $config['allowed_types'] = 'gif|jpg|png';
        $config['max_size'] = 500;
        $config['max_width'] = 1024;
        $config['max_height'] = 768;

        $this->load->library('upload', $config);
        
        $this->load->model  (["cart_model", "transactions_model"]);
        $this->load->model("orders_model");
        $this->load->model('Shopping_order_model');
        $this->load->model('Shipping_details_model', 'shipping_details');
        $this->load->model('ProductsQuantities_model');
        $this->load->model('Products_model');
    }



    public function import()
    {
        $linea = 0;
        $data_default = [
            'id_user' => null,
            'username' => '',
            'dni' => null,
            'first_name' => '',
            'last_name' => '',
            'email' => '',
            'birthdate' => null,
            'phone' => '000000',
            'address' => '',
            'address2' => '',
            'city' => 'LIMA',
            'department' => 'LIMA',
            'postal_code' => '',
            'language' => 'spanish',
            'password' => 'fullnessglobal',
            'password_repeat' => 'fullnessglobal',
            'plan' => 9,
            'country' => 'PE',
            'terms' => true,
            'security_pin' => 0000,
            'security_pin_repeat' => 0000,
            'status' => '1',
            'image' => ''
        ];
        
        $csv = fopen("clientes.csv", "r");
        while (($data_csv = fgetcsv($csv, ",")) == true) 
        {
            /**
             * 0 => nombre
             * 1 => apellido,
             * 2 => padre
             * 3 => lado
             * 4 => username
             */
            if($linea > 0){
                
                $data = $data_default;

                $data['username'] = $data_csv[4];
                $side_tree = ($data_csv[3] == "r") ? "right" : "left";
                $data['referral'] = $data_csv[2];
                $data['last_name'] = $data_csv[1];
                $data['first_name'] = $data_csv[0];
                $data['email'] = $data_csv[0] . '_' . $data_csv[1] . '@gmail.com';
                
                // If a referral user is specified
                if ($data['referral']) {
                    // if users exists
                    $ref = filter_var($data['referral'], FILTER_SANITIZE_SPECIAL_CHARS);
                    $user_ref = $this->users_model->get_user($ref);
                    echo "<pre>";
                    print_r($data);
                    echo "</pre>";
                    if (is_preferencial_user($user_ref['id'])) {
                        continue;
                    }
                    if (!$user_ref) {
                        continue;
                    }

                    // save the changes
                    $validation_code = $this->users_model->create_profile($data);
    
                    $wallet = "";
                    if ($validation_code) {
                        echo "<pre>";
                        print_r($data);
                        echo "</pre>";
                        
                        // Create a subscription
                        $this->subscriptions->add($data);
                        // Create a new user wallet
                        $this->wallet->create($data['id_user']);
                        // Create user binary tree
                        $this->binarytree->create($data['id_user'], $data['plan']);
                        // Create referral record
                        $this->referral->create($user_ref['id'], $data['id_user'], $data['plan'], $side_tree);
                        // Create initial rank
                        $this->rank->create($data['id_user']);
    
                        //ramdom wallet
                        $wallet_1 = $this->settings->wallet_1;
                        $wallet_2 = $this->settings->wallet_2;
    
                        if (!empty($wallet_1) || !empty($wallet_2)) {
                            if (!empty($wallet_1) && !empty($wallet_2)) {
                                $wallets = array($wallet_1, $wallet_2);
                                $ramdom = array_rand($wallets, 1);
                                $wallet = $wallets[$ramdom];
                            } else {
                                $wallet = !empty($wallet_1) ? $wallet_1 : $wallet_2;
                            }
                        }
    
                        $trans_id = $this->transaction->inscription($data['id_user'], $data['plan'], $wallet);
                        #activate
                        
                        $this->subscriptions->activate($data['id_user']);
                        $this->referral->activate($data['id_user']);
                        // data referral
                        $referral = $this->referral->get_rules(array('id_user' => $data['id_user']));
                        if ($referral and $referral['id_referral'] != null){
                            // add left/right child
                            $childs = $this->binarytree->get_childs($referral['id_referral']);
                            $parent = $this->binarytree->next_branch($referral['id_referral'], $referral['referral_side'], $childs);
                            
                            $this->binarytree->add_child($parent, $data['id_user'], $referral['referral_side']);
                        }
                        $sql1 = "INSERT INTO `shopping_order` (`created`, `modified`, `users_id`, `users_id_referal`, `total_value`, `total_discount`, `state`) VALUES ('2019-09-25 16:46:22', '2019-09-25 16:46:29', ".$data['id_user'].", NULL, 364.0000, 0.0000, 'pending');";
                        $this->db->query($sql1);
                        $id_order = $this->db->insert_id();
                        $sql2 = "INSERT INTO `shopping_products` ( `shopping_order_id`, `shipping_details_id`, `product_id`, `quantity`, `points_residual`, `points_binary`, `value`, `discount`) VALUES ($id_order, 0, 292, 2, 0.0000, 0.0000, 182.0000, 0.0000);";
                        $this->db->query($sql2);
                        $sql3 = "INSERT INTO `shipping_order` ( `shipping_details_id`, `shopping_order_id`, `description`, `last_update`) VALUES ( NULL, $id_order, 'calle 151 20-57, apto 401', '2019-09-25 16:46:30');";
                        $this->db->query($sql3);
                        $sql4 = "INSERT INTO `transactions` (`id_user`, `cod`, `reference`, `hash`, `description`, `btc`, `rate`, `usd`, `wallet`, `type`, `status`, `reviewer`, `date`, `last_updated`) VALUES (".$data['id_user'].", 'PURCHASE_PLAN', ".$id_order.", '5', 'Compra de plan orden ".$id_order."', 0, 0, 364, 'USD', 'purchase_plan', 'pending', NULL, '2019-09-25 21:46:29', '2019-09-25 21:46:29');";
                        $this->db->query($sql4);
                        
                        $this->approve( $id_order );
                    } else {
                        continue;
                    }
                } 


            }
            $linea++;            
        }
        //Cerramos el csv
        fclose($csv);   
    }

    public function approve( $order_id ){

    $orden_compra = $this->orders_model->get("*",["shopping_order_id"=>$order_id],1);
    $es_nuevo_usuario = is_first_login($orden_compra[0]["users_id"]);
    $suma_compras = $this->Shopping_order_model->get_total_purchase_user( $orden_compra[0]["users_id"] ) + floatVal($orden_compra[0]["total_value"]);


    // transaccion con la que se realizó la compra
    $transaccion = $this->transactions_model->get(
      [
        "reference"=>$order_id,
        "type IN ('purchase_plan','purchase')"=>null
      ],1
    );

    // Obtiene el usuario de la orden
    $user = $this->users_model->get_user($orden_compra[0]["users_id"]);

    // Si la transaccion fue por compra de plan 
    // Entonces debe otorgar los beneficios correspondientes
    if($transaccion[0]["type"] == "purchase_plan"){

      /**
       * Si ya tiene un plan en su subscripcion y esta adquiriendo otro
       * entonces es que esta haciendo un upgrade y debe otorgar el bono 
       */
      $subscripcion = $this->subscriptions->get_active($user["id"]);


      // Si la base == 0 significa que es la primera vez que va a adquirir un plan
      if( $subscripcion["base"] !=  0  ){
        $this->binarytree->add_upgrade_bonus( $user["id"], $subscripcion['id_plan'], $transaccion[0]["hash"] );
        log_message('debug', "INFO UPGRADE_BONUS users_id(".$user["id"].") plan:(".$subscripcion['id_plan'].")nuevo(".$transaccion[0]["hash"].")");

      }

      // Otorga los puntos a el padre cuando adquiere el plan
      $this->binarytree->add_residual_points_upgrade_plan( 
        $user["id"],
        $transaccion[0]["hash"]
      );


    }

    // Si es usuario preferencial debe devolverle a la billetera 
    // del usuario 10% del monto de la compra
    if( is_preferencial_user($user["id"])){
      $total = floatval($this->orders_model->get_total_order($order_id));
      $this->transaction->add_refund_preferencial_user($user["id"], $total*0.1, $orden_compra[0]["shopping_order_id"] );
      log_message('debug',"INFO reembolso usuario preferencial user:".$user["id"]." monto:".($total*0.1));
      
    }



    // BONUS DE ACTIVACION 3% A 5 generaciones de padres
    if(
      $suma_compras >= 220
      &&
      !is_active($user["id"])
      && 
      !$es_nuevo_usuario
      &&
      !is_preferencial_user($user["id"]) // no es usuario preferencial
    ){
      $this->residual->add_points(
        $user["id"], 
        40,
        "40 Puntos residuales por activacion en orden #".sprintf('%011d',$orden_compra[0]["shopping_order_id"])
      );
      $this->Shopping_order_model->update_first_product($orden_compra[0]["shopping_order_id"],["points_residual"=>40]);
      log_message('debug',"INFO PUNTOS_RESIDUALES_ACTIVACION users_id(".$user["id"].") PUNTOS:40");
      $this->binarytree->activation_bonus($user["id"]);
      log_message('debug',"INFO ACTIVATION_BONUS users_id(".$user["id"].")");

    }

    /**
     * Si el usuario ya alcanzo el monto de activacion(220 soles debe otorgar 
     * bono de activacion ) y el usuario esta inactivo
     */
    if(
      $suma_compras >= 220
      &&
      (!is_active($user["id"]) || $transaccion[0]["type"] == "purchase_plan" )
      &&
      !is_preferencial_user($user["id"])
    ){
      $this->plan->activarPlan( $user["id"], $transaccion[0]["hash"] );
      log_message('debug',"INFO ACTIVACION_PLAN users:".$user["id"]." plan:".$transaccion[0]["hash"]);
    }


    // Otorga la comision a los padres
    if($es_nuevo_usuario && $transaccion[0]["type"] == "purchase_plan" ){
      // $this->binarytree->add_binary_points_plan_purchase($user["id"], $transaccion[0]["hash"]);
      $this->referral->commission($user["id"]);
      log_message('debug',"INFO COMISION_ACTIVACION users_id(".$user["id"].")");
    }
    

    /**
     * Si ya se activo debe empezar a dar puntos por productos
     */
    if( 
      $this->Shopping_order_model->get_total_purchase_user( $user["id"] ) >= 220 
      &&
      !is_preferencial_user($user["id"]) // no es usuario preferencial
      && 
      $transaccion[0]["type"] != "purchase_plan"
    ){
      $this->Shopping_order_model->update_points_product($orden_compra[0]["shopping_order_id"]);
      $this->binarytree->add_residual_points_products($user["id"], $orden_compra[0]["shopping_order_id"]);
      $this->binarytree->add_binary_points_products($user["id"], $orden_compra[0]["shopping_order_id"]);
      log_message('debug',"INFO Puntos compra productos: users_id(".$user["id"].") orden(".$orden_compra[0]["shopping_order_id"].")");
    }

    $this->orders_model->update(["state"=>"approved"],["shopping_order_id"=>$order_id]);

    
    

    // aprobar la transaccion de compra
    $this->transactions_model->update($transaccion[0]["id"],["status"=>"done"] );

    
  }

    public function index($referral = null, $side = null) {

        if ($this->session->userdata('logged_in')) {
            $logged_in_user = $this->session->userdata('logged_in');
            if ($logged_in_user['is_admin']) {
                redirect('admin/dashboard');
            } else {
                if ($logged_in_user['profile'] == 'user') {
                    // redirect to landing page
                    redirect('user/dashboard');
                }
                else {
                    // redirect to orders
                    redirect('orders');
                }
                
            }
        }

        $user_ref = null;
        $side_tree = ($side == "r") ? "right" : "left";

        // If a referral user is specified
        if ($referral) {
            // if users exists
            $ref = filter_var($referral, FILTER_SANITIZE_SPECIAL_CHARS);
            $user_ref = $this->users_model->get_user($ref);

            if (is_preferencial_user($user_ref['id'])) {
                $this->session->set_flashdata('error', lang('users error is_preferencial_user_register'));
                redirect('login');
            }
            if (!$user_ref) {
                $this->session->set_flashdata('error', lang('users error validate_referral_failed'));
            }
        } else {
            $user_ref['id'] = null;
        }


        // validators
        $this->form_validation->set_error_delimiters($this->config->item('error_delimeter_left'), $this->config->item('error_delimeter_right'));
        $this->form_validation->set_rules('username', lang('users input username'), 'required|trim|min_length[5]|max_length[30]|callback__check_username');
        $this->form_validation->set_rules('dni', lang('users input dni'), 'required|numeric|min_length[6]|callback__check_dni_exists',["_check_dni_exists"=>lang("form_validation_exist")]);        
        $this->form_validation->set_rules('first_name', lang('users input first_name'), 'required|trim|min_length[2]|max_length[32]');
        $this->form_validation->set_rules('last_name', lang('users input last_name'), 'required|trim|min_length[2]|max_length[32]');
        $this->form_validation->set_rules('email', lang('users input email'), 'required|trim|max_length[256]|valid_email');
        $this->form_validation->set_rules('birthdate', lang('users input birthdate'), 'required');
        $this->form_validation->set_rules('phone', lang('users input phone'), 'required|numeric|min_length[6]');
        $this->form_validation->set_rules('address', lang('users input address'), 'required|min_length[5]');
        $this->form_validation->set_rules('address2', lang('users input address2'), 'min_length[5]');
        $this->form_validation->set_rules('city', lang('users input city'), 'required|min_length[4]');    
        $this->form_validation->set_rules('department', lang('users input department'), 'required|min_length[4]');
        $this->form_validation->set_rules('postal_code', lang('users input postal_code'), 'required|numeric');   
        $this->form_validation->set_rules('language', lang('users input language'), 'required|trim');
        $this->form_validation->set_rules('password', lang('users input password'), 'required|trim|min_length[5]');
        $this->form_validation->set_rules('password_repeat', lang('users input password_repeat'), 'required|trim|matches[password]');
        $this->form_validation->set_rules('plan', lang('users input plan'), 'required|numeric|greater_than[0]');
        $this->form_validation->set_rules('country', lang('users input country'), 'required|alpha');
        $this->form_validation->set_rules('terms', lang('users input terms'), 'required');
        $this->form_validation->set_rules('security_pin', lang('users input security_pin'), 'exact_length[4]|matches[security_pin_repeat]');
        $this->form_validation->set_rules('security_pin_repeat', lang('users input security_pin_repeat'), 'exact_length[4]');

        if ($this->form_validation->run() == TRUE) {

            if($this->settings->end_of_the_month == 1){
                $this->session->set_flashdata('error', lang('end_of_the_month_message'));
                redirect('register');
            }
           
            //CAPTCHA
            $captcha_validate = $this->_captcha_validate($this->input->post('g-recaptcha-response'));

            if ($captcha_validate["success"] === true || true) {
                $data = $this->input->post();

                $data['id_user'] = null;
                $data['status'] = '1';
                $data['image'] = '';
                //Upload image
                if (!$this->upload->do_upload('image')) {
                    if($_FILES['image']['error'] != 4)
                    {
                    $this->session->set_flashdata('error', $this->upload->display_errors() . $this->upload->upload_path);
                    }
                } else {
                    $file_data = array('upload_data' => $this->upload->data());
                    $data['image'] = $file_data['upload_data']['file_name'];
                }
                // save the changes
                $validation_code = $this->users_model->create_profile($data);

                $wallet = "";

                if ($validation_code) {
                    // Create a subscription
                    $this->subscriptions->add($data);
                    // Create a new user wallet
                    $this->wallet->create($data['id_user']);
                    // Create user binary tree
                    $this->binarytree->create($data['id_user'], $data['plan']);
                    // Create referral record
                    $this->referral->create($user_ref['id'], $data['id_user'], $data['plan'], $side_tree);
                    // Create initial rank
                    $this->rank->create($data['id_user']);

                    //ramdom wallet
                    $wallet_1 = $this->settings->wallet_1;
                    $wallet_2 = $this->settings->wallet_2;

                    if (!empty($wallet_1) || !empty($wallet_2)) {
                        if (!empty($wallet_1) && !empty($wallet_2)) {
                            $wallets = array($wallet_1, $wallet_2);
                            $ramdom = array_rand($wallets, 1);
                            $wallet = $wallets[$ramdom];
                        } else {
                            $wallet = !empty($wallet_1) ? $wallet_1 : $wallet_2;
                        }
                    }

                    $trans_id = $this->transaction->inscription($data['id_user'], $data['plan'], $wallet);
                    #activate
                    
                    $this->subscriptions->activate($data['id_user']);
                    $this->referral->activate($data['id_user']);
                    // data referral
                    $referral = $this->referral->get_rules(array('id_user' => $data['id_user']));
                    if ($referral and $referral['id_referral'] != null){
                        // add left/right child
                        $childs = $this->binarytree->get_childs($referral['id_referral']);
                        $parent = $this->binarytree->next_branch($referral['id_referral'], $referral['referral_side'], $childs);
                        
                        $this->binarytree->add_child($parent, $data['id_user'], $referral['referral_side']);
                    }

                    $this->session->language = $this->input->post('language');
                    $this->lang->load('users', $this->user['language']);
                    $this->session->set_flashdata('message', sprintf(lang('users info waiting approval'), $this->input->post('first_name', TRUE)));
                } else {
                    $this->session->set_flashdata('error', lang('users error register_failed'));
                    redirect($_SERVER['REQUEST_URI'], 'refresh');
                }

                // redirect home and display message
                redirect(base_url());
            } else {
                $this->session->set_flashdata('error', lang('captcha error'));
                redirect(base_url('register'));
            }
        }

        // setup page header data
        $this->set_title(lang('users title register'));
        $this
                ->add_css_theme("select/select2.min.css")
                ->add_js_theme("parsley/parsley.min.js")
                ->add_js_theme("pages/parsley-validator.js")
                ->add_js_theme("select/select2.full.js")
                ->add_js_theme("pages/register.js")
                ->add_js_theme("pages/select2.js")
                ->add_external_js("https://www.google.com/recaptcha/api.js");

        $data = $this->includes;

        // set content data
        $content_data = array(
            'cancel_url' => base_url(),
            'referral' => $referral,
            'user' => NULL,
            'password_required' => TRUE,
            'plans' => $this->plans_model->get_active(),
            'countries' => $this->locations->getCountries(true),
            'page_title' => $data['page_title']
        );

        if($this->settings->end_of_the_month == 1){
            $this->session->set_flashdata('error', lang('end_of_the_month_message'));
        }


        // load views
        $data['content'] = $this->load->view('register', $content_data, TRUE);
        $this->load->view($this->template, $data);
    }

    public function _check_dni_exists($dni){

        $result = $this->users_model->get_all(1, 0,["dni"=>$dni, "status"=>"1"]);
        log_message("DEBUG", "INFO ".var_export($result, true). " -> ".($result["total"] == 0));
        return $result["total"] == 0;
    }

    

    /**
     * Make sure username is available
     *
     * @param  string $username
     * @return int|boolean
     */
    function _check_username($username) {
        //avoid spaces
        $username = str_replace(' ', '_', $username);

        if ($this->users_model->username_exists($username)) {
            $this->form_validation->set_message('_check_username', sprintf(lang('users error username_exists'), $username));
            return FALSE;
        } else {
            return $username;
        }
    }

    /**
     * Make sure email is available
     *
     * @param  string $email
     * @return int|boolean
     */
    function _check_email($email) {
        if ($this->users_model->email_exists($email)) {
            $this->form_validation->set_message('_check_email', sprintf(lang('users error email_exists'), $email));
            return FALSE;
        } else {
            return $email;
        }
    }

    /**
     * Make sure email exists
     *
     * @param  string $email
     * @return int|boolean
     */
    function _check_email_exists($email) {
        if (!$this->users_model->email_exists($email)) {
            $this->form_validation->set_message('_check_email_exists', sprintf(lang('users error email_not_exists'), $email));
            return FALSE;
        } else {
            return $email;
        }
    }

    /**
     * Google Captcha validate
     *
     * @param  Variable front
     * @return int|boolean
     */
    function _captcha_validate($input) {
        $captcha = $input;
        $response = file_get_contents("https://www.google.com/recaptcha/api/siteverify?secret=6Lcvaa0UAAAAACYYjMVYa1XgVVxhoUnAuD0nZhb6&response=" . $captcha . "&remoteip=" . $_SERVER['REMOTE_ADDR']);
        $response = json_decode($response, true);

        return $response;
    }

    public function payment_method($plan_id = null, $wallet = null) {
        $idiom = $this->session->get_userdata('language');
        $this->lang->load('users', $idiom['language']);

        if ($this->session->userdata('logged_in')) {
            redirect(base_url('account'));
        }

        $plan = null;
        if (is_numeric($plan_id)) {
            $plan = $this->plans_model->get($plan_id);
        }

        if ($plan != null) {

            // Use curl to perform the currency conversion using Blockchain.info's currency conversion API
            $ch = curl_init();
            curl_setopt($ch, CURLOPT_URL, "https://blockchain.info/tobtc?currency=USD&value=" . $plan['base']);
            curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
            $conversion = curl_exec($ch);
            curl_close($ch);
            $plan_price = $conversion;
        }

        // setup page header data
        $this->set_title(lang('users title register'));
        $this
                ->add_css_theme("select/select2.min.css")
                ->add_js_theme("parsley/parsley.min.js")
                ->add_js_theme("pages/parsley-validator.js")
                ->add_js_theme("select/select2.full.js")
                ->add_js_theme("pages/register.js")
                ->add_js_theme("pages/select2.js");

        $data = $this->includes;

        // set content data
        $content_data = array(
            'cancel_url' => base_url(),
            'user' => NULL,
            'password_required' => TRUE,
            'plan' => $plan,
            'countries' => $this->locations->getCountries(true),
            'page_title' => $data['page_title'],
            'plan_price' => $plan_price,
            'wallet' => $wallet ? $wallet : ""
        );


        // load views
        $this->session->set_flashdata('message', sprintf(lang('users info waiting approval'), $this->input->post('first_name', TRUE)));
        $data['content'] = $this->load->view('choose_payment_method', $content_data, TRUE);
        $this->load->view($this->template, $data);
    }


    public function external_user($referral = null){
    
        if ($this->session->userdata('logged_in')) {
            redirect(base_url('account'));
        }

        if($this->session->userdata('user_id')) {
            redirect(base_url() . 'checkout_external/cart_list/' . $referral);
        }

        // If a referral user is not specified
        if ($referral) {
            // if users exists
            $ref = filter_var($referral, FILTER_SANITIZE_SPECIAL_CHARS);
            $user_ref = $this->users_model->get_user($ref);

            if (!$user_ref) {
                $this->session->set_flashdata('error', lang('users error validate_referral_failed'));
                redirect('login');
            }
        } else {
            $this->session->set_flashdata('error', lang('users error validate_referral_failed'));
            redirect('login');
        }


        // validators
        $this->form_validation->set_error_delimiters($this->config->item('error_delimeter_left'), $this->config->item('error_delimeter_right'));
        $this->form_validation->set_rules('dni', lang('users input dni'), 'required|numeric|min_length[6]');        
        $this->form_validation->set_rules('first_name', lang('users input first_name'), 'required|trim|min_length[2]|max_length[32]');
        $this->form_validation->set_rules('last_name', lang('users input last_name'), 'required|trim|min_length[2]|max_length[32]');
        $this->form_validation->set_rules('email', lang('users input email'), 'required|trim|max_length[256]|valid_email');
        $this->form_validation->set_rules('phone', lang('users input phone'), 'required|numeric|min_length[6]');
        $this->form_validation->set_rules('address', lang('users input address'), 'required|min_length[5]');
        $this->form_validation->set_rules('address2', lang('users input address2'), 'min_length[5]');
        $this->form_validation->set_rules('country', lang('users input country'), 'required|alpha');
        $this->form_validation->set_rules('department', lang('users input department'), 'required|min_length[4]');        
        $this->form_validation->set_rules('city', lang('users input city'), 'required|min_length[4]'); 
        $this->form_validation->set_rules('language', lang('users input language'), 'required|trim');
        $this->form_validation->set_rules('postal_code', lang('users input postal_code'), 'required|numeric');   
     
    
        if ($this->form_validation->run() == TRUE) {
           
            //CAPTCHA
            $captcha_validate = $this->_captcha_validate($this->input->post('g-recaptcha-response'));

            if ($captcha_validate["success"] === true) {
                $data = $this->input->post();

            
                $next_id = $this->users_model->next_id_user();
                
                $data['id_user'] = null;
                $data['username'] = uniqid();
                $data['image'] = '';
                $data['profile'] = 'external_user';
                $data['status'] = '0';
                $split_email = explode('@', $data['email']);                
                $data['email'] = $split_email[0].'+'.$next_id . '@' . $split_email[1];
            
                // save the changes
                $validation_code = $this->users_model->create_profile($data);


                if ($validation_code) {
                    
                    //Crear orden
                    $user = $this->users_model->get_user_by_validation_code($validation_code);
                    $orden = $this->orden->create_temporal_orden_user_external( $user["id"], $user_ref['id'] );
                    
                    // Guardar en session el id de el usuario registrado
                    $this->session->set_userdata('user_id', $user['id']);                 
                    

                    $this->session->language = $this->input->post('language');
                    $this->lang->load('users', $this->user['language']);
                    $this->session->set_flashdata('message', sprintf(lang('users info waiting approval'), $this->input->post('first_name', TRUE)));
                    redirect(base_url('checkout_external/cart_list/'.$referral));

                } else {
                    $this->session->set_flashdata('error', lang('users error register_failed'));
                    redirect('register/external_user/' + $referral, 'refresh');
                }
            
            
            } else {
                $this->session->set_flashdata('error', lang('captcha error'));
                redirect(base_url('register/external_user/' + $referral));
            }
        }

        // setup page header data
        $this->set_title(lang('users title personal_informacion'));
        $this
        ->add_css_theme("select/select2.min.css")
        ->add_js_theme("parsley/parsley.min.js")
        ->add_js_theme("pages/parsley-validator.js")
        ->add_js_theme("select/select2.full.js")
        ->add_js_theme("pages/register.js")
        ->add_js_theme("pages/select2.js")
        ->add_external_js("https://www.google.com/recaptcha/api.js");

        $data = $this->includes;

        // set content data
        $content_data = array(
            'cancel_url' => $_SERVER['HTTP_REFERER'],
            'referral' => $referral,
            'user' => NULL,
            'password_required' => TRUE,
            'plans' => $this->plans_model->get_active(),
            'countries' => $this->locations->getCountries(true),
            'page_title' => $data['page_title']
        );


        // load views
        $data['content'] = $this->load->view('register_external', $content_data, TRUE);
        $this->load->view($this->template, $data);
    }

}