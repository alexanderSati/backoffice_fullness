<?php

defined('BASEPATH') OR exit('No direct script access allowed');



class Cart extends Private_Controller {

    

    private $url;

    

    /**

     * Constructor

     */

    function __construct()

    {

        parent::__construct();

        $this->url = base_url('cart');

        

        // load the language files

        

        $this->lang->load('cart',$this->session->language);

        

        // load the users model

        $this->load->model([

            "cart_model",

            "subscriptions_model",

            "shipping_details_model",

            "Transactions_model",

            "ProductsQuantities_model",

            "categories_model"

            ]);

            

        $this->load->library("cart");

        $this->load->library('orden');

        $this->load->library('wallet');



    }

    



    function index(){
        $subscription = $this->subscriptions_model->get_active( $this->user["id"] );

        /**
         * Cuando es el primer ingreso pero ya adquirio un plan debe mostrar 
         * mensaje de que ya ha adquirido un plan
         */

        if( 
            is_first_login($this->user["id"])
            &&
            count($this->Transactions_model->get_rows(
                'id',
                ['id_user'=>$this->user["id"], "type"=> "purchase_plan", "status in ('pending','done')"=>null],
                0,
                1
            )) > 0
        ){

            $this->session->set_flashdata('warning', lang('first_plan_purchased'));
            redirect(site_url('user/dashboard'));
        }


        /**
         * Si el usuario no ha adquirido un plan y no ha 
         * escogido ser un consumidor preferencial entonces debe
         * preguntar 
         */

        if( 

            $subscription['id_plan'] == 9 &&

            (

                empty($subscription['preferencial_user'])  ||

                $subscription['preferencial_user'] = 0

            ) &&

            empty($this->session->userdata('plan_value'))

        ){

            // redirigir hacia la vista en donde se escoge
            // el plan o si desea ser consumidor preferente o
            // elegir un plan
            redirect(base_url('user/profile/select_type_purchases'));

        }



        $this
            ->add_root_css( "bower_components/datatables.net-bs4/css/dataTables.bootstrap4.min.css" )
            ->add_root_js( "bower_components/datatables.net/js/jquery.dataTables.min.js" )
            ->add_root_js( "assets/pages/data-table/js/data-table-custom.js" )
            ->add_root_js( "bower_components/datatables.net-bs4/js/dataTables.bootstrap4.min.js" )
            ->add_root_js( "bower_components/sweetalert/js/sweetalert.min.js") 
            ->add_root_css( "css/animate.css")  
            ->add_root_js( "assets/js/modal.js") 
            ->add_root_css( "assets/css/component.css" )
            ->add_root_css( "bower_components/sweetalert/css/sweetalert.css")
            ->add_root_js( "assets/js/classie.js" )
            ->add_root_js( "assets/js/modalEffects.js" )
            ->add_css_theme("select/select2.min.css")
            ->add_js_theme( "select/select2.full.js" )
            ->add_js_theme( "pages/select2.js" )
            ->add_root_css( "assets/pages/notification/notification.css" )
            ->add_root_js( "assets/js/bootstrap-growl.min.js" )
            ->add_js_theme( "pages/shopping_cart.js")
            ->set_title( lang('products') );


        $name_product = empty($_GET["name_product"])? "": $_GET["name_product"];
        $price_min = empty($_GET["price_min"])? "": $_GET["price_min"];
        $price_max = empty($_GET["price_max"])? "": $_GET["price_max"];
        $category = empty($_GET["category"])? "": $_GET["category"];
        $shoppingDetailsId = empty($_GET["shopping_details_id"])? 0 : $_GET["shopping_details_id"];
        $offset = empty($_GET["offset"])? 0 : $_GET["offset"];



        // Parametros de la url (para mantener el form)        
        $url_params = preg_replace([
            '/&?offset=\d+/',
            '/&?csrf_token=\w*/'
        ] ,"", $_SERVER['REQUEST_URI'] );

        

        // Generacion de CSRF
        $csrf = array(
            'name' => $this->security->get_csrf_token_name(),
            'hash' => $this->security->get_csrf_hash()
        );



        // Paginacion
        $this->pagination->initialize(array(
            'base_url'   => site_url(substr($url_params,1)),
            'total_rows' => $this->cart_model->get_total_rows($name_product, $price_min, $price_max, $category, $shoppingDetailsId, $offset),
            'per_page'   => $this->settings->per_page_limit
        ));



        if( strpos($url_params, '&') !== false ){

            $url_params .= "&csrf_token=".$csrf['hash'];

        }

        else{

            $url_params .= "?csrf_token=".$csrf['hash'];

        }



        $data = $this->includes;



        if($subscription["base"] > 0 && !empty($this->session->userdata("plan_id"))){
            $content_data["is_upgrade"] = true;
        }
        else{
            $content_data["is_upgrade"] = false;
        }

        $content_data["product"] = $this->cart_model->fetch_all($name_product, $price_min, $price_max, $category, $shoppingDetailsId, $offset, 12 );


        $content_data["pagination"] = $this->pagination->create_links();
        $content_data["proveedores"] = $this->shipping_details_model->get("*",["shipping_types_id IN(2,3)"=>null], null, "description ASC");
        $content_data["csrf"] = $csrf;
        $content_data["categories"] = $this->categories_model->get_all(null, null, null, "name", "ASC")['results'];
        $content_data["cart_list"] = $this->view();
        $content_data['this_url']  = $this->url;
        $data["content"] = $this->load->view("cart/shopping_cart", $content_data, true);

        if($this->settings->end_of_the_month == 1){
            $this->session->set_flashdata('error', lang('end_of_the_month_message'));
        }

        $this->load->view("..".DIRECTORY_SEPARATOR."..".DIRECTORY_SEPARATOR."themes".DIRECTORY_SEPARATOR.$this->settings->theme.DIRECTORY_SEPARATOR."template", $data);



     }



     public function get_products(){

        $filtro = empty($this->input->post("filtro"))? "":$this->input->post("filtro");

        $inicio = empty($this->input->post("inicio"))? 0:$this->input->post("inicio");

        $offset = empty($this->input->post("offset"))? $this->settings->per_page_limit:$this->input->post("offset");

        

        // Generacion de CSRF

        $csrf = array(

            'name' => $this->security->get_csrf_token_name(),

            'hash' => $this->security->get_csrf_hash()

        );



        $content_data['products'] = $this->cart_model->fetch_all($filtro, $inicio, $offset );

        $content_data["csrf"] = $csrf;

        $content_data["total"] = $this->cart_model->get_total_rows();



        echo json_encode($content_data);

    }





    private function check_only_one_wholesaler( $wholesaler_id ){



        foreach( $this->cart->contents() as $product ){

            if( $product["options"]["shipping_details_id"] != $wholesaler_id )

                return false;

        }

        return true;

    }



    

    function add(){

        // Crea una orden temporal para la compra actual
        if(count($this->cart->contents()) == 0 || empty($this->orden->get_temporal_orden($this->user['id']))){
            
            $this->orden->create_temporal_orden( $this->user["id"] );

        }



        /**
         * Si se añade pago con wallet (saldo en plataforma)
         * al carrito deberá tener precio negativo para que 
         * reste al total de la orden
         */

        if( $_POST['code'] == 'DESCUENTO_BILLETERA' ){

            $saldoDisponible = $this->wallet->get($this->user['id'], "USD");

            if( !empty($saldoDisponible) && $saldoDisponible >= $_POST["product_price"] && $_POST["product_price"] <= $this->cart->total() ){

                // volver el precio negativo
                $_POST["product_price"] *= -1;
                // extraer el valor de la compra de la billetera del usuario
                $this->wallet->subtract_funds($this->user['id'], floatval($_POST["product_price"]));

            }

            else{
                echo json_encode(["success"=>false, "message"=>lang("insufficient_funds")]);
                exit();
            }

        }


        $cant_disponible = $this->ProductsQuantities_model
                                    ->get_quantity_available($_POST['shipping_details_id'],$_POST["product_id"]);

        if( $cant_disponible <  $_POST["quantity"]){

            echo json_encode([
                "success"=>false, 
                "message"=>lang('no_quantity_in_stock'),
                "stock"=>$cant_disponible
            ]);
            exit();

        }



        foreach ($this->cart->contents() as $key => $value) {

            if($value['id'] == $data["id"] ){

                if ($cant_disponible ==  $value['qty']) {

                    echo json_encode([
                        "success"=>false, 
                        "message"=>lang('no_quantity_in_stock'),
                        "stock"=> null
                    ]);
                    exit(); 
                }

                if($value['qty'] + $data['qty'] >  $cant_disponible) {

                    echo json_encode([
                        "success"=>false, 
                        "message"=>lang('no_quantity_in_stock'),
                        "stock"=> $cant_disponible - $value['qty']

                    ]);
                    exit(); 
                }
            }
        }   

        // Inserta los datos del producto en la sesion de usuario
        $data = array(
            "id"  => $_POST["product_id"],
            "name"  => str_replace(["/","&","(",")", ","],"",$_POST["product_name"]),
            "qty"  => $_POST["quantity"],
            "price"  => $_POST["product_price"],
            "residual" => $_POST["residual"],
            "binary" => $_POST["binary"],
            "options"=>[
                "code"=>$_POST['code'],
                "shipping_details_id"=>empty($_POST['shipping_details_id'])?'0':$_POST['shipping_details_id']
            ]

        );

        if(!$this->check_only_one_wholesaler($data["options"]["shipping_details_id"]) && $data["id"] != 1){

            echo json_encode(["success"=>false, "message"=>lang('msg_unique_wholesaler')]);
            exit();
        }

        $row_id= $this->cart->insert($data); //return rowid 

        $response = [
            "success"=>true,
            "row_id"=>$row_id,
            "csrf"=>[
                'name' => $this->security->get_csrf_token_name(),
                'hash' => $this->security->get_csrf_hash()
            ],
            "view"=> $this->view(),
            "total_cart"=>$this->cart->total()
        ];
        echo json_encode( $response );

    }





    public function shopping_wholesaler(){

        $this->add_root_css( "bower_components/datatables.net-bs4/css/dataTables.bootstrap4.min.css" )

                ->add_root_js( "bower_components/datatables.net/js/jquery.dataTables.min.js" )

                ->add_root_js( "assets/pages/data-table/js/data-table-custom.js" )

                ->add_root_js( "bower_components/datatables.net-bs4/js/dataTables.bootstrap4.min.js" )

                ->add_root_js( "bower_components/sweetalert/js/sweetalert.min.js") 

                ->add_root_css( "css/animate.css")  

                ->add_root_js( "assets/js/modal.js") 

                ->add_root_css( "assets/css/component.css" )

                ->add_root_css( "bower_components/sweetalert/css/sweetalert.css")

                ->add_root_js( "assets/js/classie.js" )

                ->add_root_js( "assets/js/modalEffects.js" )

                ->add_css_theme("select/select2.min.css")

                ->add_js_theme( "select/select2.full.js" )

                ->add_js_theme( "pages/select2.js" )

                ->add_root_css( "assets/pages/notification/notification.css" )

                ->add_root_js( "assets/js/bootstrap-growl.min.js" )

                ->add_js_theme( "pages/shopping_wholesaler.js")

                ->set_title( lang('products') );



       



        $name_product = empty($_GET["name_product"])? "": $_GET["name_product"];

        $price_min = empty($_GET["price_min"])? "": $_GET["price_min"];

        $price_max = empty($_GET["price_max"])? "": $_GET["price_max"];

        $category = empty($_GET["category"])? "": $_GET["category"];

        $shoppingDetailsId = empty($_GET["shopping_details_id"])? 0 : $_GET["shopping_details_id"];

        $offset = empty($_GET["offset"])? 0 : $_GET["offset"];



        // Parametros de la url (para mantener el form)        

        $url_params = preg_replace([

            '/&?offset=\d+/',

            '/&?csrf_token=\w*/'

        ] ,"", $_SERVER['REQUEST_URI'] );



        

        

        // Generacion de CSRF

        $csrf = array(

            'name' => $this->security->get_csrf_token_name(),

            'hash' => $this->security->get_csrf_hash()

        );



        if( strpos($url_params, '&') !== false ){

            $url_params .= "&csrf_token=".$csrf['hash'];

        }

        else{

            $url_params .= "?csrf_token=".$csrf['hash'];

        }



        $data = $this->includes;



        // Paginacion

        $this->pagination->initialize(array(

            'base_url'   => site_url(substr($url_params,1)),

            'total_rows' => $this->cart_model->get_total_rows($name_product, $price_min, $price_max, $category, $shoppingDetailsId, $offset),

            'per_page'   => $this->settings->per_page_limit

        ));



        $content_data["product"] = $this->cart_model->fetch_all($name_product, $price_min, $price_max, $category, $shoppingDetailsId, $offset, 12 );

        $content_data["pagination"] = $this->pagination->create_links();

        $content_data["proveedores"] = $this->shipping_details_model->get("*",["shipping_types_id IN(2,3)"=>null], null, "description ASC");

        $content_data["csrf"] = $csrf;

        $content_data["categories"] = $this->categories_model->get_all()['results'];

        $content_data["cart_list"] = $this->view();

        $content_data['this_url']  = $this->url;







        $data["content"] = $this->load->view("cart/shopping_wholesaler", $content_data, true);

        $this->load->view("..".DIRECTORY_SEPARATOR."..".DIRECTORY_SEPARATOR."themes".DIRECTORY_SEPARATOR.$this->settings->theme.DIRECTORY_SEPARATOR."template", $data);



       







    }











    

    function load(){

        echo $this->view();

    }



    function remove()

    {   

        // quita los datos de un producto producto en la sesion de usuarioº

        $row_id = $_POST["row_id"];

        

        // si es un pago por billetera debe devolverle el saldo

        $item = $this->cart->get_item($row_id);

        if( $item["options"]["code"] == "DESCUENTO_BILLETERA" ){

            $this->wallet->add_funds($this->user['id'], $item["price"]);

        }

        

        $data = array(

        'rowid'  => $row_id,

        'qty'  => 0

        );

        

        $this->cart->update($data);

        $response = [

            "success"=>true,

            "row_id"=>$row_id,

            "csrf"=>[

                'name' => $this->security->get_csrf_token_name(),

                'hash' => $this->security->get_csrf_hash()

            ],

            "view"=> $this->view(),

            "total_cart"=>$this->cart->total()

        ];



        echo json_encode( $response );

    }

    

    function clear(){



        foreach($this->cart->contents() as $product){

            if($product["options"]["code"] == "DESCUENTO_BILLETERA"){

                $this->wallet->add_funds($this->user['id'], $product["price"]);

            }

        }



        $this->cart->destroy();



        $response = [

            "success"=>true,

            "row_id"=>$row_id,

            "csrf"=>[

                'name' => $this->security->get_csrf_token_name(),

                'hash' => $this->security->get_csrf_hash()

            ],

            "view"=> $this->view(),

            "total_cart"=>$this->cart->total()

        ];



        echo json_encode( $response );



    }

     

    function view(){

        

        $output = '';

        $output .= '

        <div >

        <div align="right">

            <button type="button" id="clear_cart" class="btn btn-warning">

                '.$this->lang->line('clear_shopping_cart').'

            </button>

        </div>

        <br />

        <table class="table table-bordered">

            <tr>

            <th width="40%">'.$this->lang->line('cart_name').'</th>

            <th width="15%">'.$this->lang->line('cart_residual').'</th>

            <th width="15%">'.$this->lang->line('cart_binary').'</th>

            <th width="15%">'.$this->lang->line('cart_quantity').'</th>

            <th width="15%">'.$this->lang->line('price_shopping_cart').'</th>

            <th width="15%">Total</th>

            <th width="15%">'.$this->lang->line('cart_action').'</th>

            </tr>

        

        ';

        $count = 0;

        foreach($this->cart->contents() as $items)

        {

            $count++;

            $output .= '

            <tr> 

                <td>'.$items["name"].'</td>';



            if ( (empty($this->session->userdata('plan_value')) && is_active()) || $this->user['profile'] == 'executive' ){

                $output .= '<td>'.$items['residual'] * $items['qty'].'</td>

                    <td>'.$items['binary'] * $items['qty'].'</td><td>';

            }else {

                $output .= '<td>0</td>

                    <td>0</td>

                    <td>';

            }



            if($items['id']!= 1){

                $output .=  '<input type="number" min="1" class="form-control" value="'.$items["qty"].'" onchange="update_qty(this.value, this)" data-rowid="'.$items["rowid"].'"/>';

            }

            else{

                $output .=  $items["qty"];

            }  

        

            $output .='</td><td>'.sprintf("%.2f",$items["price"]).'</td>

                <td>'.sprintf("%.2f",$items["subtotal"]).'</td>

                <td>

                    <button type="button" name="remove" class="btn btn-danger btn-xs remove_inventory" id="'.$items["rowid"].'">

                        '.$this->lang->line('remove_cart_list').'

                    </button>

                </td>

            </tr>

            ';

        }



        if( $count == 0 ){

            $output .= '

            <tr>

                <td colspan="5" align="center" >

                    '.$this->lang->line('cart_empty').'

                </td>

            </tr>

            ';

        }



        $output .= '

        <tr>

            <td colspan="5" align="right">Total</td>

            <td id="total_cart">'.$this->cart->total().'</td>

            <td colspan="1"></td>         

        </tr>

        

        </table>

        

        </div>

        ';

        

        

        return $output;

    }





    public function update_qty(){

        $data = array(

            'rowid' => $this->input->post("rowid"),

            'qty'   => $this->input->post("qty")

        );



        $product = $this->cart->get_item($data["rowid"]);



        $cant_disponible = $this->ProductsQuantities_model

                                    ->get_quantity_available(

                                        $product["options"]["shipping_details_id"],

                                        $product["id"]

                                    );

        if($cant_disponible < $data["qty"]){



            echo json_encode([

                "success"=>false,

                "message"=>lang('no_quantity_in_stock'),

                "stock"=>$cant_disponible

            ]);

            exit();



        }



        $this->cart->update($data);



        $response = [

            "success"=>true,

            "row_id"=>$this->input->post("rowid"),

            "csrf"=>[

                'name' => $this->security->get_csrf_token_name(),

                'hash' => $this->security->get_csrf_hash()

            ],

            "view"=> $this->view(),

            "total_cart"=>$this->cart->total()

        ];



        echo json_encode( $response );

    

        

    }





    /**

     * Genera un json con el contenido del carrito de compras

     * 

     */

    public function get_list_items_cart(){



        foreach($this->cart->contents() as $key => $item){

            $cant_disponible = $this->ProductsQuantities_model

                                        ->get_quantity_available(

                                            $item['options']['shipping_details_id'],

                                            $item['id']

                                        );

            $item["is_available"] =  $cant_disponible >= $item["qty"];

            $item["stock"] = $cant_disponible;

            $items['items'][] = $item;





            

        }

        $items['total'] = number_format ($this->cart->total(),2);

        $items['success'] = true;



        echo json_encode($items);



    }

}