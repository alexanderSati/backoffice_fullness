<?php defined('BASEPATH') OR exit('No direct script access allowed');

class Renew extends Public_Controller {


    /**
     * Constructor
     */
    function __construct()
    {
        parent::__construct();

        // load the language files
        $this->lang->load('users');
        $this->lang->load('dashboard');

        // load the users model
        $this->load->model('users_model');
        $this->load->model('plans_model');
        $this->load->model('subscriptions_model','subscriptions');
        $this->load->library('transaction');
        $this->load->library('plan');
        $this->load->library('currency');
        
        $this->wallets->labels = array(
            "USD"=>"Bitcoin", 
            "BTC"=>"Bitcoin (Operative)", 
            "XMR"=>"Monero", 
            "ETH"=>"Ethereum", 
            "LTC"=>"Litecoin", 
            "TRUMP"=>"Trumpcoin", 
            "DASH"=>"Dash", 
            "SLR"=>"Solarcoin", 
        );
        
    }
    
    /**
     * Load renew subscription when a user has completed subscription 
     *
     * @param  integer $user_id
     */
    function index($user_id = null )
    {   
        if(!$user_id){
            redirect('login');
        }
        
        $user = $this->users_model->get_user($user_id);
        $subscription = $this->subscriptions->get_subscription($user_id);
        $plan = $this->plan->get_plan($subscription['id_plan']);
        $this->session->set_userdata('user', $user_id);
        
        if($this->transaction->is_renewal_available($user_id)){
            $this->session->set_flashdata('message', lang('renewal-pending'));
            redirect('login');
        }
        
        if ($this->input->post('coin'))
        {
//            $result = $this->transaction->renewal($user,$this->input->post('plan'));
            
            $result = $this->transaction->renewal($user, $plan['id'], true, $this->input->post('coin'), $this->input->post('wallet'));
            
            if ($result !== true){
                
                switch ($result) {
                    case -1:
                           $error = lang('invalid-plan');
                        break;
                    case -3:
                           $error = lang('invalid-wallet');
                        break;
                    case -4:
                           $error = lang('error update side');
                        break;
                    case -5:
                           $error = lang('not-enough-funds');
                        break;
                    default:
                        $error = lang('error update side');
                        break;
                }
                
                $this->session->set_flashdata('error', $error);
            }else{
                $this->session->set_flashdata('message', lang('renewal-pending'));
                redirect('login');
            }
        }
        
        // setup page header data
        $this
                ->add_css_theme("select/select2.min.css")
                ->add_js_theme( "select/select2.full.js" )
                ->add_js_theme( "pages/renew.js" )
                ->set_title( lang('users title renew-subscription') );

        $data = $this->includes;
        
        $content_data = array(
            'page_title'    => $data['page_title'],
            'subscription' => $subscription,
            'current_plan' => $plan
        );

        // load views
        $data['content'] = $this->load->view('renew', $content_data, TRUE);
        $this->load->view($this->template, $data);
    }
    
    function ajax($action, $param1) {
        switch ($action) {
            case 'load_qr':
                
                $data = $this->load_qr($param1);
                echo json_encode($data);

                break;

            default:
                break;
        }
    }
    
    /**
     * Load html code to show qr 
     *
     * @param  string $coin
     * @return array Data with qr, html, coin and wallet to deposit
     */
    function load_qr($coin) {
        
        if($coin == "BTC"){
            //ramdom wallet
            $wallet_1 = $this->settings->wallet_1;
            $wallet_2 = $this->settings->wallet_2;
            $wallet = "";
            if(!empty($wallet_1) || !empty($wallet_2)){
                if(!empty($wallet_1) && !empty($wallet_2)){
                    $wallets = array($wallet_1, $wallet_2);
                    $ramdom = array_rand($wallets, 1);
                    $wallet = $wallets[$ramdom];
                }else{
                    $wallet = !empty($wallet_1) ? $wallet_1 : $wallet_2;
                }
            }

            $bitcoins = 0;//$this->input->post('amount');
            $qr_code = "https://blockchain.info/qr?data=bitcoin:{$wallet}"; // ?amount={$bitcoins}
            $img_html = '<div id="btc"><img src="' . $qr_code . '"/></div><p id="wallet_qr">' . $wallet . '</p>';
            
        } else{
            $name_wallet = "wallet_" . strtolower($coin);
            $wallet = $this->settings->$name_wallet;

            $coin_final = strtolower($this->wallets->labels[$coin]);
            $qr_code = "https://chart.googleapis.com/chart?chs=300x300&cht=qr&chl={$coin}:{$wallet}";
            $img_html = '<img src="' . $qr_code . '"/><p id="wallet_qr">' . $wallet . '</p>';
        }
        
        $img_html .= form_input(array('name' => 'wallet', 'id' => 'wallet', 'readonly' => 'readonly', 'required' => 'required', 'value' => $wallet, 'class' => 'v-hidden'));
        
        $this->currency->load($coin);
        $subscription = $this->subscriptions->get_subscription($this->session->userdata('user'));
        $plan = $this->plan->get_plan($subscription['id_plan']);
        
        $amount = round($this->currency->btc($plan['base']), 8);
       
        $img_html .= "<p>" . lang('users input amount') . ": {$amount} {$coin}</p>";
        
        
        
        return array('coin' => $coin, 'wallet' => $wallet, 'qr_code' => $qr_code, 'img_html' => $img_html);
    }
}