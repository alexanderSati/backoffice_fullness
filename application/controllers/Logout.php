<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of Logout
 *
 * @author Juan Manuel
 */
class Logout extends MY_Controller{
    
    public function __construct() {
        parent::__construct();
    }
    
   /**
     * Logout
     */
    function index()
    {
        $this->session->unset_userdata('logged_in');
        $this->session->sess_destroy();
        redirect('login');
    }

    
}

