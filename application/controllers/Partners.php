<?php

defined('BASEPATH') OR exit('No direct script access allowed');

/**
 * All  > PUBLIC <  AJAX functions should go in here
 *
 * CSRF protection has been disabled for this controller in the config file
 *
 * IMPORTANT: DO NOT DO ANY WRITEBACKS FROM HERE!!! For retrieving data only.
 */
class Partners extends Public_Controller {

  function __construct(){
    parent::__construct();
    $this->load->model("Faqs_model");
  }

  public function index(){
    $usuarios_fundadores = $this->Faqs_model->get_all();
    $data = $this->includes;
    $data["content"] = $this->load->view("founding_partners",["usuarios"=>$usuarios_fundadores], true);
    $this->load->view( $this->template, $data );
  }
    
}