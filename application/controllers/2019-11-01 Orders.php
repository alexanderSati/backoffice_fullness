<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Orders extends Private_Controller {


  /**
     * Constructor
     */
    function __construct()
    {
      parent::__construct();
      $this->load->model  (["cart_model", "transactions_model"]);

      // load the language file
      $this->lang->load('cart',$this->session->language);

      
        // load the model
        $this->load->model("orders_model");
        // load the users model
        $this->load->model('users_model');

        $this->load->model('Shopping_order_model');

        $this->load->model('Shipping_details_model', 'shipping_details');

        $this->load->model('ProductsQuantities_model');
        $this->load->model('Products_model');
        $this->load->model('PaymentMethod_model');


        $this->load->model('Subscriptions_model', 'subcription');

        //load library email
        $this->load->library([
          'email',
          'binarytree',
          'referral',
          'orden',
          'residual',
          'transaction'
        ]);


        // set constants
        define('REFERRER', "referrer");
        define('THIS_URL', base_url('admin/categories'));
        define('DEFAULT_LIMIT', $this->settings->per_page_limit);
        define('DEFAULT_OFFSET', 0);
        define('DEFAULT_SORT', "name");
        define('DEFAULT_DIR', "asc");
        $this->load->library('wallet');
        $this->load->library('binarytree');

        // use the url in session (if available) to return to the previous filter/sorted/paginated list
        if ($this->session->userdata(REFERRER))
        {
            $this->_redirect_url = $this->session->userdata(REFERRER);
        }
        else
        {
            $this->_redirect_url = THIS_URL;
        }
    }

    
    
    /**************************************************************************************
     * PUBLIC FUNCTIONS
     **************************************************************************************/

    function index()
    {
        $this
          ->add_root_js( "bower_components/datatables.net/js/jquery.dataTables.min.js" )
          ->add_root_js( "assets/pages/data-table/js/data-table-custom.js" )
          ->add_root_js( "bower_components/datatables.net-bs4/js/dataTables.bootstrap4.min.js" )            
          ->add_root_css( "bower_components/datatables.net-bs4/css/dataTables.bootstrap4.min.css" ) 
          ->add_root_js("js/pages/orders.js")
          ->set_title( lang('orders') );


        $data = $this->includes;

        $where["shopping_order.state <>"] = 'temporal';
        if( !empty( $this->shipping_details->get('*', 'id_user='.$this->user['id'], 1) ) ){
          $shipping_details = $this->shipping_details->get('*', 'id_user='.$this->user['id'], 1);
          $where["shopping_products.shipping_details_id"] = $shipping_details["id"];
        }
        

        // Obtiene el listado de compras de un usuario
        $content_data["purchases"] = $this->orders_model->get_orders($where);

        $data['content'] = $this->load->view('admin/shopping/list_orders',$content_data, true);

        $this->load->view("..".DIRECTORY_SEPARATOR."..".DIRECTORY_SEPARATOR."themes".DIRECTORY_SEPARATOR.$this->settings->theme.DIRECTORY_SEPARATOR."template", $data);
    }

  /**
  * Carlos Aguirre 2019-07-10 15:30:16
  * Carga la vista de compras realizadas
  * por un usuario
  */
  public function purchases(){
    $this
      ->add_root_js( "bower_components/datatables.net/js/jquery.dataTables.min.js" )
      ->add_root_js( "assets/pages/data-table/js/data-table-custom.js" )
      ->add_root_js( "bower_components/datatables.net-bs4/js/dataTables.bootstrap4.min.js" )
      ->add_root_css( "bower_components/datatables.net-bs4/css/dataTables.bootstrap4.min.css" )

      ->add_root_css( "assets/css/component.css" )
      ->add_root_js( "assets/js/classie.js" )
      ->add_root_js( "assets/js/modalEffects.js" )      
      
      ->add_js_theme( "pages/purchases.js" )
      ->set_title( lang('purchases') );



    // Generacion de CSRF
    $csrf = array(
    'name' => $this->security->get_csrf_token_name(),
    'hash' => $this->security->get_csrf_hash()
    );

    $data = $this->includes;
    $content_data["csrf"] = $csrf;
    // Obtiene el listado de compras de un usuario
    $content_data["purchases"] = $this->cart_model->get_purchases($this->user['id']);

    $data['content'] = $this->load->view('orders/purchases',$content_data, true);
    
    //$this->load->view($this->template, $data);
    $this->load->view("..".DIRECTORY_SEPARATOR."..".DIRECTORY_SEPARATOR."themes".DIRECTORY_SEPARATOR.$this->settings->theme.DIRECTORY_SEPARATOR."template", $data);
  }

  /**
  * Carlos Aguirre 2019-07-11 07:51:13
  * Obtiene el listado de productos adquiridos en una orden
  */
  public function get_items_order(){
    $shopping_order_id = $this->input->get("shopping_order_id");
    $envio = $this->orden->obtenerMetodoEnvio($shopping_order_id );
    $items["productos"] =$this->cart_model->get_items_purchase($shopping_order_id);
    $items["items"] =$envio;
    $items["pago"] =$this->PaymentMethod_model->obtenerRegistroPorId(["shopping_order_id"=>$shopping_order_id]);


    echo json_encode($items);
  }


  /**
   * Aprueba el estado de una compra en la base de datos
   */
  public function approve( $order_id ){
    



    $orden_compra = $this->orders_model->get("*",["shopping_order_id"=>$order_id],1);
    $es_nuevo_usuario = is_first_login($orden_compra[0]["users_id"]);
    $suma_compras = $this->Shopping_order_model->get_total_purchase_user( $orden_compra[0]["users_id"] ) + floatVal($orden_compra[0]["total_value"]);


    // transaccion con la que se realizó la compra
    $transaccion = $this->transactions_model->get(
      [
        "reference"=>$order_id,
        "type IN ('purchase_plan','purchase')"=>null
      ],1
    );

    // Obtiene el usuario de la orden
    $user = $this->users_model->get_user($orden_compra[0]["users_id"]);

    log_message('debug',"INFO aprobar orden:".$order_id."\n"
                ."suma_compras:".$suma_compras." nuevo:".var_export($es_nuevo_usuario, true)." activo:".var_export(is_active($user["id"]), true)."\n"
                ."transaccion: ".var_export($transaccion, true)
    );

    // Si la transaccion fue por compra de plan 
    // Entonces debe otorgar los beneficios correspondientes
    if($transaccion[0]["type"] == "purchase_plan"){

      /**
       * Si ya tiene un plan en su subscripcion y esta adquiriendo otro
       * entonces es que esta haciendo un upgrade y debe otorgar el bono 
       */
      $subscripcion = $this->subcription->get_active($user["id"]);


      // Si la base == 0 significa que es la primera vez que va a adquirir un plan
      if( $subscripcion["base"] !=  0  ){
        $this->binarytree->add_upgrade_bonus( $user["id"], $subscripcion['id_plan'], $transaccion[0]["hash"] );
        log_message('debug', "INFO UPGRADE_BONUS users_id(".$user["id"].") plan:(".$subscripcion['id_plan'].")nuevo(".$transaccion[0]["hash"].")");

      }

      // Otorga los puntos a el padre cuando adquiere el plan
      $this->binarytree->add_residual_points_upgrade_plan( 
        $user["id"],
        $transaccion[0]["hash"]
      );


    }

    // Si es usuario preferencial debe devolverle a la billetera 
    // del usuario 10% del monto de la compra
    if( is_preferencial_user($user["id"])){
      $total = floatval($this->orders_model->get_total_order($order_id));
      $this->transaction->add_refund_preferencial_user($user["id"], $total*0.1, $order_id );
      log_message('debug',"INFO reembolso usuario preferencial user:".$user["id"]." monto:".($total*0.1));
      
    }



    // BONUS DE ACTIVACION 3% A 5 generaciones de padres
    if(
      $suma_compras >= 220
      &&
      !is_active($user["id"])
      && 
      !$es_nuevo_usuario
      &&
      !is_preferencial_user($user["id"]) // no es usuario preferencial
    ){
      $this->residual->add_points(
        $user["id"], 
        40,
        "40 Puntos residuales por activacion en orden #".sprintf('%011d',$orden_compra[0]["shopping_order_id"])
      );
      $this->Shopping_order_model->update_first_product($order_id, ["points_residual"=>40]);
      log_message('debug',"INFO PUNTOS_RESIDUALES_ACTIVACION users_id(".$user["id"].") PUNTOS:40");
      $this->binarytree->activation_bonus($user["id"]);
      log_message('debug',"INFO ACTIVATION_BONUS users_id(".$user["id"].")");

    }

    /**
     * Si el usuario ya alcanzo el monto de activacion(220 soles debe otorgar 
     * bono de activacion ) y el usuario esta inactivo
     */
    if(
      // $suma_compras >= 220
      // &&
      (!is_active($user["id"]) || $transaccion[0]["type"] == "purchase_plan" )
      &&
      !is_preferencial_user($user["id"])
    ){
      $this->plan->activarPlan( $user["id"], $transaccion[0]["hash"] );
      log_message('debug',"INFO ACTIVACION_PLAN users:".$user["id"]." plan:".$transaccion[0]["hash"]);
    }


    // Otorga la comision a los padres
    if($es_nuevo_usuario && $transaccion[0]["type"] == "purchase_plan" ){
      // $this->binarytree->add_binary_points_plan_purchase($user["id"], $transaccion[0]["hash"]);
      $this->referral->commission($user["id"]);
      log_message('debug',"INFO COMISION_ACTIVACION users_id(".$user["id"].")");
    }
    

    /**
     * Si ya se activo debe empezar a dar puntos por productos
     */
    if( 

      ($this->Shopping_order_model->get_total_purchase_user( $user["id"] ) >= 220 || $this->Shopping_order_model->check_buy_plan_current_month($user["id"]) )
      &&
      !is_preferencial_user($user["id"]) // no es usuario preferencial
      && 
      $transaccion[0]["type"] != "purchase_plan"
    ){
      $this->Shopping_order_model->update_points_product($orden_compra[0]["shopping_order_id"]);
      $this->binarytree->add_residual_points_products($user["id"], $order_id);
      $this->binarytree->add_binary_points_products($user["id"], $order_id);
      log_message('debug',"INFO Puntos compra productos: users_id(".$user["id"].") orden(".$order_id.")");
    }


    // Si hizo pago con billetera y la compra es un mayorista debe añadirsele el pago
    if(
      !empty( $this->Shopping_order_model->get_pago_wallet($order_id) )
      &&
      !empty( $this->orden->get_wholesaler_order( $order_id ))
    ){
      $wholesaler = $this->orden->get_wholesaler_order( $order_id );
      $payment    = $this->Shopping_order_model->get_pago_wallet($order_id);
      $this->orden->add_payment_wallet_to_wholesaler( $order_id, $wholesaler["id_user"],$payment["value"]);
      log_message('debug',"INFO pago a mayorista: valor:{$payment['value']} usuario:{$wholesaler['id_user']} orden:{$order_id}");

    }

    $this->orders_model->update(["state"=>"approved"],["shopping_order_id"=>$order_id]);

    
    $this->enviarEmailAprobarCompra( $user, $order_id);

    // aprobar la transaccion de compra
    $this->transactions_model->update($transaccion[0]["id"],["status"=>"done"] );

    $this->session->set_flashdata('message', $this->lang->line('order_approved'));
    redirect(base_url("orders"));

  }

  public function approve_wholesaler( $shopping_order_id ){
    $orden_compra = $this->orders_model->get("*",["shopping_order_id"=>$order_id],1);
    
    // transaccion con la que se realizó la compra
    $transaccion = $this->transactions_model->get(
      [
        "reference"=>$order_id,
        "type IN ('purchase_plan','purchase')"=>null
      ],1
    );


    $this->orders_model->update(["state"=>"approved"],["shopping_order_id"=>$shopping_order_id]);

    // aprobar la transaccion de compra
    $this->transactions_model->update($transaccion[0]["id"],["status"=>"done"] );

    $this->ProductsQuantities_model->load_quantities_from_purchase($shopping_order_id);
    $this->session->set_flashdata('message', $this->lang->line('order_approved'));
    redirect(base_url("orders"));
  }

  /**
   * Denegar el estado de una compra en la base de datos
   */
  
   public function denied($order_id)
   {
    //Obtener los datos de la orden
    $orden_compra = $this->orders_model->get("*",["shopping_order_id"=>$order_id],1);

    //Obtener los datos del usuario de la orden
    $user = $this->users_model->get_user($orden_compra[0]['users_id']);
    
    $this->emailRechazoCompra($user, $order_id);
    
    // transaccion con la que se realizó la compra
    $transaccion = $this->transactions_model->get(
      [
        "reference"=>$order_id
      ],1
    );


    /**
     * En caso de haber pagado con saldo de wallet
     * le devuelve el saldo gastado
     */
    if( !empty( $this->Shopping_order_model->get_pago_wallet($order_id) ) ){
      $pago_wallet = $this->Shopping_order_model->get_pago_wallet($order_id);


      // Devuelve los fondos a la billetera
      $this->wallet->add_funds( 
        $orden_compra[0]["users_id"], 
        abs($pago_wallet["value"]), 
        "USD" 
      );

      // Registra la transaccion de reembolso
      $this->transaction->purchaseRefund(
        $orden_compra[0]["users_id"], 
        "Transaction for order purchase refund #".$orden_compra[0]["id"],
        abs($pago_wallet["value"])
      );
      
    }


    // Devuelve la cantidad que fue sacada del stock del mayorista
    // en caso de que fuese sacado de el
    $productos_de_orden = $this->Shopping_order_model->get_shopping_products('*',[
      "product_id <>"=>1,
      'shipping_details_id <>'=>0,
      "shopping_order_id"=>$order_id
    ]);
    foreach($productos_de_orden as $product){
      $this->Products_model->agregar_catidad_en_stock( 
        $product["product_id"],
        $product["shipping_details_id"],
        $product["quantity"]
      );
    }

    //Denegar orden
    $this->orders_model->update(["state"=>"denied"],["shopping_order_id"=>$order_id]);

    // rechazar la transaccion de compra
    $this->transactions_model->update($transaccion[0]["id"],["status"=>"rejected"] );

    $this->session->set_flashdata('message', $this->lang->line('order_denied'));
    redirect(base_url("orders"));
  }


  public function emailRechazoCompra($user, $order_id){

    //Obtener los items de la orden
    $item = $this->orders_model->get_items_order($order_id);

    //Cargar vista html
    $html = $this->load->view("emails/".$user['language']."/denied_orden", [
      "user"=>$user,
      "items"=>$item,
      "order_id"=>$order_id,
      "total_valor" => $item[0]['total_value']
    ], TRUE);

    //Configurar y enviar correo
    $this->email->from($this->settings->site_email);
    $this->email->to($user['email']);

    if ($user['language'] == 'english') {

      $this->email->subject( 'Order denied for ' . $user['first_name']);

    } elseif($user['language'] == 'spanish'){

      $this->email->subject('Orden denegada para ' . $user['first_name']);

    }elseif ($user['language'] == 'portuguese') {

      $this->email->subject('Ordem recusado por ' . $user['first_name']);
    }
    
    $this->email->message($html);
    $this->email->set_mailtype("html");
    $this->email->send();
  }



  public function enviarEmailAprobarCompra($user, $order_id){
    $item = $this->orders_model->get_items_order($order_id);  

    //Cargar vista html
    $html = $this->load->view("emails/".$user['language']."/approved_orden", [
      "user"=>$user,
      "items"=>$item,
      "order_id"=>$order_id,
      "total_valor" => $item[0]['total_value']
    ], TRUE);

    //Configurar y enviar correo
    $this->email->from($this->settings->site_email);
    $this->email->to($user['email']);

    if ($user['language'] == 'english') {

      $this->email->subject('Order approved for ' . $user['first_name']);

    } elseif($user['language'] == 'spanish'){

      $this->email->subject('Orden aprobada para ' . $user['first_name']);

    }elseif ($user['language'] == 'portuguese') {

      $this->email->subject('Ordem aprovada para ' . $user['first_name']);
    }

    
    $this->email->message($html);
    $this->email->set_mailtype("html");
    $this->email->send();
  }

  /**
   * Email bonus sale
   */
  function email_bonus_sale($user,$user_referal, $total_value)
  {
    $content_data['user'] = $user;
    $content_data['user_referal'] = $user_referal;
    $content_data['total_value']  = $total_value;
    
    //Cargar vista html
    $html = $this->load->view("emails/".$user_referal['language']."/bonus_sales", $content_data , TRUE);

    //Configurar y enviar correo
    $this->email->from($this->settings->site_email);
    $this->email->to($user_referal['email']);

    if ($user_referal['language'] == 'english') {

      $this->email->subject('Sales bonus for ' . $user_referal['first_name']);

    } elseif($user_referal['language'] == 'spanish'){

      $this->email->subject('Bono de venta para ' . $user_referal['first_name']);

    }elseif ($user_referal['language'] == 'portuguese') {

      $this->email->subject('Bônus de vendas por ' . $user_referal['first_name']);
    }

    
    $this->email->message($html);
    $this->email->set_mailtype("html");
    $this->email->send();
  }

  /**
   * Approve orden external user
   */
  function approve_external($order_id){

    // get order
    $purchase_order = $this->orders_model->get("*",["shopping_order_id"=>$order_id],1);

    $half_value = $purchase_order[0]['total_value'] / 2;
    $total_value = $half_value * 0.7;

    // Get the order user
    $user = $this->users_model->get_user($purchase_order[0]["users_id"]);

    // Transaction with which the purchase was made
    $transaccion = $this->transactions_model->get(["reference"=>$order_id, "type IN ('purchase_plan','purchase')"=>null],1);


    if(is_active($purchase_order[0]['users_id_referal'])){     
      
      // Add bonus sales
      $this->transaction->add_transaction_bonus_sales($purchase_order[0]['users_id_referal'], $total_value, $transaccion[0]);

      // Get user referal
      $user_referal = $this->users_model->get_user($purchase_order[0]["users_id_referal"]);

      //Add binary points 
      // $this->binarytree->add_binary_points_products($purchase_order[0]["users_id_referal"], $purchase_order[0]["shopping_order_id"]);
      
      //Add residual points
      // $this->binarytree->add_residual_points_products($purchase_order[0]["users_id_referal"], $purchase_order[0]["shopping_order_id"]);
      
      log_message('debug',"INFO Puntos compra productos: users_id(".$purchase_order[0]["users_id_referal"].") orden(".$purchase_order[0]["shopping_order_id"].")");
      
      // Send email bonus
      $this->email_bonus_sale($user,$user_referal, $total_value);
    }
    
    
    // Send mail order approve 
    $this->enviarEmailAprobarCompra( $user, $order_id);

    // Approve order
    $this->orders_model->update(["state"=>"approved"],["shopping_order_id"=>$order_id]);

    // Approve the purchase transaction
    $this->transactions_model->update($transaccion[0]["id"],["status"=>"done"] );

    $this->session->set_flashdata('message', $this->lang->line('order_approved'));
    redirect(base_url("orders"));
  }

  /**
   * Denid order external user
   */
  function denied_external($order_id){
    //Obtener los datos de la orden
    $orden_compra = $this->orders_model->get("*",["shopping_order_id"=>$order_id],1);

    //Obtener los datos del usuario de la orden
    $user = $this->users_model->get_user($orden_compra[0]['users_id']);
    
    $this->emailRechazoCompra($user, $order_id);
    
    // transaccion con la que se realizó la compra
    $transaccion = $this->transactions_model->get(
      [
        "reference"=>$order_id
      ],1
    );


    // Devuelve la cantidad que fue sacada del stock del mayorista
    // en caso de que fuese sacado de el
    $productos_de_orden = $this->Shopping_order_model->get_shopping_products('*',[
      "product_id <>"=>1,
      'shipping_details_id <>'=>0,
      "shopping_order_id"=>$order_id
    ]);
    foreach($productos_de_orden as $product){
      $this->Products_model->agregar_catidad_en_stock( 
        $product["product_id"],
        $product["shipping_details_id"],
        $product["quantity"]
      );
    }

    //Denegar orden
    $this->orders_model->update(["state"=>"denied"],["shopping_order_id"=>$order_id]);

    // aprobar la transaccion de compra
    $this->transactions_model->update($transaccion[0]["id"],["status"=>"rejected"] );

    $this->session->set_flashdata('message', $this->lang->line('order_denied'));
    redirect(base_url("orders"));
  }
}