<?php defined('BASEPATH') OR exit('No direct script access allowed');

class Faqs extends Admin_Controller {

    /**
     * Constructor
     */
    function __construct()
    {
        parent::__construct();

        $this->load->model('faqs_model');
        $this->lang->load('faqs');

        // set constants
        define('REFERRER', "referrer");
        define('THIS_URL', base_url('admin/faqs'));
        define('DEFAULT_LIMIT', $this->settings->per_page_limit);
        define('DEFAULT_OFFSET', 0);
        define('DEFAULT_SORT', "id");
        define('DEFAULT_DIR', "asc");

        // use the url in session (if available) to return to the previous filter/sorted/paginated list
        if ($this->session->userdata(REFERRER))
        {
            $this->_redirect_url = $this->session->userdata(REFERRER);
        }
        else
        {
            $this->_redirect_url = THIS_URL;
        }
    }


    /**
     * Settings Editor
     */
    function index()
    {

        if (!is_allowed('read')){
            $this->session->set_flashdata('error', privilegies_message());
            redirect('admin/dashboard');
        }
        
        $lang = (isset($user['language']) ? $user['language'] : $this->config->item('language'));

        // get parameters
        $limit  = $this->input->get('limit')  ? $this->input->get('limit', TRUE)  : DEFAULT_LIMIT;
        $offset = $this->input->get('offset') ? $this->input->get('offset', TRUE) : DEFAULT_OFFSET;
        $sort   = $this->input->get('sort')   ? $this->input->get('sort', TRUE)   : DEFAULT_SORT;
        $dir    = $this->input->get('dir')    ? $this->input->get('dir', TRUE)    : DEFAULT_DIR;

        // get filters
        $filters = array();

        if ($this->input->get("{$lang}_question"))
        {
            $filters["{$lang}_question"] = $this->input->get("{$lang}_question", TRUE);
        }

        if ($this->input->get("{$lang}_answer"))
        {
            $filters["{$lang}_answer"] = $this->input->get("{$lang}_answer", TRUE);
        }

        if ($this->input->get("status"))
        {
            $filters["status"] = $this->input->get("status", TRUE);
        }

        // build filter string
        $filter = "";
        foreach ($filters as $key => $value)
        {
            $filter .= "&{$key}={$value}";
        }

        // save the current url to session for returning
        $this->session->set_userdata(REFERRER, THIS_URL . "?sort={$sort}&dir={$dir}&limit={$limit}&offset={$offset}{$filter}");

        // are filters being submitted?
        if ($this->input->post())
        {
            if ($this->input->post('clear'))
            {
                // reset button clicked
                redirect(THIS_URL);
            }
            else
            {
                // apply the filter(s)
                $filter = "";

                if ($this->input->post("{$lang}_question"))
                {
                    $filter .= "&{$lang}_question=" . $this->input->post("{$lang}_question", TRUE);
                }

                if ($this->input->post("{$lang}_answer"))
                {
                    $filter .= "&{$lang}_answer=" . $this->input->post("{$lang}_answer", TRUE);
                }

                if ($this->input->post("status"))
                {
                    $filter .= "&status=" . $this->input->post("status", TRUE);
                }

                // redirect using new filter(s)
                redirect(THIS_URL . "?sort={$sort}&dir={$dir}&limit={$limit}&offset={$offset}{$filter}");
            }
        }

        // get list
        $faqs = $this->faqs_model->get_all_row($limit, $offset, $filters, $sort, $dir);

        // build pagination
        $this->pagination->initialize(array(
            'base_url'   => THIS_URL . "?sort={$sort}&dir={$dir}&limit={$limit}{$filter}",
            'total_rows' => $faqs['total'],
            'per_page'   => $limit
        ));

        // setup page header data
        $this
            // ->add_css_theme( "datatables/jquery.dataTables.min.css" )
            // ->add_css_theme( "datatables/buttons.bootstrap.min.css" )
            // ->add_css_theme( "datatables/fixedHeader.bootstrap.min.css" )
            // ->add_css_theme( "datatables/responsive.bootstrap.min.css" )
            // ->add_css_theme( "datatables/scroller.bootstrap.min.css" )

            // ->add_js_theme( "datatables/jquery.dataTables.min.js" )
            // ->add_js_theme( "datatables/dataTables.bootstrap.js" )
            // ->add_js_theme( "datatables/dataTables.buttons.min.js" )
            // ->add_js_theme( "datatables/buttons.bootstrap.min.js" )
            // ->add_js_theme( "datatables/jszip.min.js" )
            // ->add_js_theme( "datatables/pdfmake.min.js" )
            // ->add_js_theme( "datatables/vfs_fonts.js" )
            // ->add_js_theme( "datatables/buttons.html5.min.js" )
            // ->add_js_theme( "datatables/buttons.print.min.js" )
            // ->add_js_theme( "datatables/dataTables.fixedHeader.min.js" )
            // ->add_js_theme( "datatables/dataTables.keyTable.min.js" )
            // ->add_js_theme( "datatables/dataTables.responsive.min.js" )
            // ->add_js_theme( "datatables/responsive.bootstrap.min.js" )
            // ->add_js_theme( "datatables/dataTables.scroller.min.js" )
            // ->add_js_theme( "pages/tables.js" )
            // ->add_js_theme( "pages/faqs.js" )

            ->set_title( lang('admin faqs title') );

        $data = $this->includes;

        // set content data
        $content_data = array(
            'this_url'   => THIS_URL,
            'faqs'      => $faqs['results'],
            'total'      => $faqs['total'],
            'filters'    => $filters,
            'filter'     => $filter,
            'pagination' => $this->pagination->create_links(),
            'limit'      => $limit,
            'offset'     => $offset,
            'sort'       => $sort,
            'dir'        => $dir,
            'page_title' => $data['page_title'],
            'lang'       => $lang
        );

        // load views
        $data['content'] = $this->load->view('admin/faqs/list', $content_data, TRUE);
        $this->load->view($this->template, $data);
    }

    /**
     * Add new faq
     */
    function add()
    {
        
        if (!is_allowed('insert')){
            $this->session->set_flashdata('error', privilegies_message());
            redirect('admin/dashboard');
        }
        
        $lang = (isset($user['language']) ? $user['language'] : $this->config->item('language'));

        // validators
        // setup a validation for each translation
        $this->form_validation->set_error_delimiters($this->config->item('error_delimeter_left'), $this->config->item('error_delimeter_right'));
        foreach ($this->session->languages as $language_key=>$language_name)
        {
            $language_name = strtolower($language_name);
            $this->form_validation->set_rules("{$language_name}_question", lang("faqs input {$language_name}_question"), 'required|trim|min_length[5]|max_length[30]');
        }

        if ($this->form_validation->run() == TRUE)
        {
            // save the new user
            $saved = $this->faqs_model->add_faq($this->input->post());

            if ($saved)
            {
                $this->session->set_flashdata('message', sprintf(lang('faqs msg add_faq_success'), $this->input->post("{$lang}_question")));
            }
            else
            {
                $this->session->set_flashdata('error', sprintf(lang('faqs error add_faq_failed'), $this->input->post("{$lang}_question")));
            }

            // return to list and display message
            redirect($this->_redirect_url);
        }

        // setup page header data

        $this
            ->add_css_theme("summernote.css")

            ->add_js_theme( "parsley/parsley.min.js" )
            ->add_js_theme( "pages/parsley-validator.js" )
            ->add_js_theme('summernote.min.js')
            ->add_js_theme( "pages/register.js" )
            ->add_js_theme( "pages/editor.js" )
            ->add_js_theme( "pages/faqs.js" )

            ->set_title( lang('admin faqs add') );

        $data = $this->includes;

        // set content data
        $content_data = array(
            'cancel_url'        => $this->_redirect_url,
            'faq'              => NULL,
            'page_title'        => "Usuarios fundadores"
        );

        // load views
        $data['content'] = $this->load->view('admin/faqs/form', $content_data, TRUE);
        $this->load->view($this->template, $data);
    }

    /**
     * Edit existing user
     *
     * @param  int $id
     */
    function edit($id = NULL)
    {
        if (!is_allowed('update')){
            $this->session->set_flashdata('error', privilegies_message());
            redirect('admin/dashboard');
        }

        $lang = (isset($user['language']) ? $user['language'] : $this->config->item('language'));

        // make sure we have a numeric id
        if (is_null($id) OR ! is_numeric($id))
        {
            redirect($this->_redirect_url);
        }

        // get the data
        $faq = $this->faqs_model->get($id);

        // if empty results, return to list
        if ( ! $faq)
        {
            redirect($this->_redirect_url);
        }

        // validators
        // setup a validation for each translation
        $this->form_validation->set_error_delimiters($this->config->item('error_delimeter_left'), $this->config->item('error_delimeter_right'));
        foreach ($this->session->languages as $language_key=>$language_name)
        {
            $language_name = strtolower($language_name);
            $this->form_validation->set_rules("{$language_name}_question", lang("faqs input {$language_name}_question"), 'required|trim|min_length[5]|max_length[30]');
        }

        if ($this->form_validation->run() == TRUE)
        {
            // save the changes
            $saved = $this->faqs_model->edit_faq($this->input->post());

            if ($saved)
            {
                $this->session->set_flashdata('message', sprintf(lang('faqs msg edit_faq_success'), $this->input->post("{$lang}_question")));
            }
            else
            {
                $this->session->set_flashdata('error', sprintf(lang('faqs error edit_faq_failed'), $this->input->post("{$lang}_question")));
            }

            // return to list and display message
            redirect($this->_redirect_url);
        }

        // setup page header data

        $this
            ->add_css_theme("summernote.css")

            ->add_js_theme( "parsley/parsley.min.js" )
            ->add_js_theme( "pages/parsley-validator.js" )
            ->add_js_theme('summernote.min.js')
            ->add_js_theme( "pages/register.js" )
            ->add_js_theme( "pages/editor.js" )
            ->add_js_theme( "pages/faqs.js" )

            ->set_title( lang('faqs title faq_edit') );

        $data = $this->includes;

        // set content data
        $content_data = array(
            'cancel_url'        => $this->_redirect_url,
            'faq'              => $faq,
            'faq_id'           => $id,
            'page_title'        => $data['page_title']
        );

        // load views
        $data['content'] = $this->load->view('admin/faqs/form', $content_data, TRUE);
        $this->load->view($this->template, $data);
    }

    /**
     * Export list to CSV
     */
    function export()
    {
        $lang = (isset($user['language']) ? $user['language'] : $this->config->item('language'));

        // get parameters
        $sort = $this->input->get('sort') ? $this->input->get('sort', TRUE) : DEFAULT_SORT;
        $dir  = $this->input->get('dir')  ? $this->input->get('dir', TRUE)  : DEFAULT_DIR;

        // get filters
        $filters = array();

        if ($this->input->get("{$lang}_question"))
        {
            $filters["{$lang}_question"] = $this->input->get("{$lang}_question", TRUE);
        }

        if ($this->input->get("{$lang}_answer"))
        {
            $filters["{$lang}_answer"] = $this->input->get("{$lang}_answer", TRUE);
        }

        if ($this->input->get("status"))
        {
            $filters["status"] = $this->input->get("status", TRUE);
        }

        // get all faqs
        $faqs = $this->faqs_model->get_all_row(0, 0, $filters, $sort, $dir);

        if ($faqs['total'] > 0)
        {
            // manipulate the output array
            foreach ($faqs['results'] as $key=>$faq)
            {
                unset($faqs['results'][$key]['password']);
                unset($faqs['results'][$key]['deleted']);

                if ($faq['status'] == 0)
                {
                    $faqs['results'][$key]['status'] = lang('admin input inactive');
                }
                else
                {
                    $faqs['results'][$key]['status'] = lang('admin input active');
                }
            }

            // export the file
            array_to_csv($faqs['results'], "faqs");
        }
        else
        {
            // nothing to export
            $this->session->set_flashdata('error', lang('core error no_results'));
            redirect($this->_redirect_url);
        }

        exit;
    }

    /**
     * Delete a faq
     *
     * @param  int $id
     */
    function delete($id = NULL)
    {
        if (!is_allowed('delete')){
            $this->session->set_flashdata('error', privilegies_message());
            redirect('admin/dashboard');
        }
        
        $lang = (isset($user['language']) ? $user['language'] : $this->config->item('language'));

        // make sure we have a numeric id
        if ( ! is_null($id) OR ! is_numeric($id))
        {
            // get user details
            $faq = $this->faqs_model->get($id);
            
            if ($faq)
            {
                $delete = $this->faqs_model->delete_faq($id);

                if ($delete)
                {
                    $this->session->set_flashdata('message', sprintf(lang('faqs msg delete_faq'), $faq["{$lang}_question"]));
                }
                else
                {
                    $this->session->set_flashdata('error', sprintf(lang('faqs error delete_faq'), $faq["{$lang}_question"]));
                }
            }
            else
            {
                $this->session->set_flashdata('error', lang('faqs error faq_not_exist'));
            }
        }
        else
        {
            $this->session->set_flashdata('error', lang('faqs error faq_id_required'));
        }

        // return to list and display message
        redirect($this->_redirect_url);
    }

}
