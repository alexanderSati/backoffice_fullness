<?php defined('BASEPATH') OR exit('No direct script access allowed');

class Tradings extends Admin_Controller {

    /**
     * Constructor
     */
    function __construct()
    {
        parent::__construct();

        // load the language files
        $this->lang->load('trading');
        
        //Load models
        $this->load->model('users_model','users');
        $this->load->model('subscriptions_model');
        
        //Load Libraries
        $this->load->library('TradingLib');

    }
    
    /**
     * Dashboard
     */
    function index()
    {
        // setup page header data
        $this
            ->add_root_js( "bower_components/datatables.net/js/jquery.dataTables.min.js" )
            ->add_root_js( "assets/pages/data-table/js/data-table-custom.js" )
            ->add_root_js( "bower_components/datatables.net-bs4/js/dataTables.bootstrap4.min.js" )            
            ->add_root_css( "bower_components/datatables.net-bs4/css/dataTables.bootstrap4.min.css" )
            // ->add_css_theme( "floatexamples.css" )
            // ->add_css_theme( "datatables/responsive.bootstrap.min.css" )
            // ->add_css_theme( "datatables/jquery.dataTables.min.css" )
            // ->add_css_theme( "datatables/responsive.bootstrap.min.css" )

            // ->add_js_theme( "flot/jquery.flot.js" )
            // ->add_js_theme( "flot/jquery.flot.time.min.js" )
            // ->add_js_theme( "flot/date.js" )
            // ->add_js_theme( "datatables/jquery.dataTables.min.js")
            // ->add_js_theme( "datatables/dataTables.bootstrap.js")
            // ->add_js_theme( "datatables/dataTables.responsive.min.js")
            // ->add_js_theme( "datatables/responsive.bootstrap.min.js")
            // ->add_js_theme( "pages/dashboard.js" )
            ->set_title( lang('trading_history') );
		
        $data = $this->includes;
        
        // set content data
        $content_data = array(
            'rows'    => $this->tradinglib->adminResume()
        );

        // load views
        $data['content'] = $this->load->view('admin/trading', $content_data, TRUE);
        $this->load->view($this->template, $data);
    }

}