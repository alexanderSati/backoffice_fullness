<?php defined('BASEPATH') OR exit('No direct script access allowed');


class Executive extends Admin_Controller {


    /**
     * Constructor
     */
    function __construct()
    {
        parent::__construct();

        //load models
        $this->load->model('users_model');
        $this->load->model('shipping_details_model');

        // upload config
        $config['upload_path']          = "uploads/";
        $config['allowed_types']        = 'gif|jpg|png';
        $config['max_size']             = 0;
        $config['max_width']            = 1024;
        $config['max_height']           = 1024;


        $this->load->library('upload', $config);
        $this->load->library('email');
        $this->load->library('wallet');
            
        // set constants
        define('THIS_URL', base_url('admin/executive'));

    }



    /**************************************************************************************
     * PUBLIC FUNCTIONS
     **************************************************************************************/


    /**
     * Executives list page
     */
    function index()
    {

       // setup page header data
       $this
       ->add_root_js( "bower_components/datatables.net/js/jquery.dataTables.min.js" )
       ->add_root_js( "assets/pages/data-table/js/data-table-custom.js" )
       ->add_root_js( "bower_components/datatables.net-bs4/js/dataTables.bootstrap4.min.js" )            
       ->add_root_css( "bower_components/datatables.net-bs4/css/dataTables.bootstrap4.min.css" ) 
       ->add_js_theme( "pages/transactions.js" )
       ->set_title(lang('executive'));


        $data = $this->includes;


        // set content data
        $content_data = array(
            'users'   => $this->users_model->get_executive(),
            'this_url'   => THIS_URL
        );


        $data['content'] = $this->load->view('admin/users/list_executives', $content_data, TRUE);
        $this->load->view($this->template, $data);


    }

    /**
     * Add Executive
     */
    function add(){

        // validators
        $this->form_validation->set_error_delimiters($this->config->item('error_delimeter_left'), $this->config->item('error_delimeter_right'));
        $this->form_validation->set_rules('username', lang('users input username'), 'required|trim|min_length[5]|max_length[30]|callback__check_username[]');
        $this->form_validation->set_rules('first_name', lang('users input first_name'), 'required|trim|min_length[2]|max_length[32]');
        $this->form_validation->set_rules('last_name', lang('users input last_name'), 'required|trim|min_length[2]|max_length[32]');
        $this->form_validation->set_rules('email', lang('users input email'), 'required|trim|max_length[128]|valid_email|callback__check_email[]');
        $this->form_validation->set_rules('language', lang('users input language'), 'required|trim');
        $this->form_validation->set_rules('status', lang('users input status'), 'required|numeric');
        $this->form_validation->set_rules('password', lang('users input password'), 'required|trim|min_length[5]');
        $this->form_validation->set_rules('password_repeat', lang('users input password_repeat'), 'required|trim|matches[password]');
        $this->form_validation->set_rules('shiping_details', lang('users input shiping_details'), 'required|numeric');




        if ($this->form_validation->run() == TRUE)


        {


            $data = $this->input->post();

            //Upload image
            $data['image'] = '';


            if ( $this->upload->do_upload('image'))
            {
                $file_data = array('upload_data' => $this->upload->data());
                $data['image'] = $file_data['upload_data']['file_name'];
            }
            $data['profile'] = 'executive';

            // save the new user
            $saved_id = $this->users_model->add_executive($data);

            // Crear la billetera del usuario 
            $this->wallet->create($saved_id);

            if ($saved_id)
            {
                $this->send_email_register($saved_id, 'inscription_user', $data);
                $this->session->set_flashdata('message', sprintf(lang('users msg add_user_success'), $this->input->post('first_name') . " " . $this->input->post('last_name')));
            }
            else
            {
                $this->session->set_flashdata('error', sprintf(lang('users error add_user_failed'), $this->input->post('first_name') . " " . $this->input->post('last_name')));
            }

            // return to list and display message
            redirect(THIS_URL);
        }

        // setup page header data
        $this
            ->add_css_theme("select/select2.min.css")
            ->add_js_theme( "parsley/parsley.min.js" )
            ->add_js_theme( "pages/parsley-validator.js" )
            ->add_js_theme( "select/select2.full.js" )
            ->add_js_theme( "pages/register.js" )
            ->add_js_theme( "pages/select2.js" )
            ->set_title( lang('users title executive_add'));


        $data = $this->includes;

        $shipping_query = $this->shipping_details_model->get_shipping_executive('id_user is null OR id_user = 0');
        $shipping = array_combine(array_column($shipping_query, 'id'),array_column($shipping_query, 'name'));


        // set content data
       $content_data = array(
         'cancel_url' => THIS_URL,
         'page_title' => lang('users title executive_add'),
         'shipping' => $shipping,
         'password_required' => TRUE,
        );


        // load views
        $data['content'] = $this->load->view('admin/users/executives_form', $content_data, TRUE);
        $this->load->view($this->template, $data);

    }


    /**
     * Edit Executive
     * @param  int $id
     */


    function edit($id = NULL)
    {        

        // make sure we have a numeric id
        if (is_null($id) OR ! is_numeric($id))
        {
            redirect(THIS_URL);
        }


        // get the data
        $user = $this->users_model->get_executive_by_id($id);

        // if empty results, return to list
        if ( ! $user)
        {
            redirect(THIS_URL);
        }


        // validators
        $this->form_validation->set_error_delimiters($this->config->item('error_delimeter_left'), $this->config->item('error_delimeter_right'));
        $this->form_validation->set_rules('username', lang('users input username'), 'required|trim|min_length[5]|max_length[30]|callback__check_username[' . $user['username'] . ']');
        $this->form_validation->set_rules('first_name', lang('users input first_name'), 'required|trim|min_length[2]|max_length[32]');
        $this->form_validation->set_rules('last_name', lang('users input last_name'), 'required|trim|min_length[2]|max_length[32]');
        $this->form_validation->set_rules('email', lang('users input email'), 'required|trim|max_length[128]|valid_email|callback__check_email[' . $user['email'] . ']');
        $this->form_validation->set_rules('language', lang('users input language'), 'required|trim');
        $this->form_validation->set_rules('status', lang('users input status'), 'required|numeric');
        $this->form_validation->set_rules('password', lang('users input password'), 'min_length[5]|matches[password_repeat]');
        $this->form_validation->set_rules('password_repeat', lang('users input password_repeat'), 'matches[password]');

        if ($this->form_validation->run() == TRUE)
        {


            $data = $this->input->post();
       


            //Upload image
            $data['image'] = '';

            if ( ! $this->upload->do_upload('image'))
            {

                if($_FILES['image']['error'] != 4)
                {
                    $this->session->set_flashdata('error', $this->upload->display_errors());
                    redirect(THIS_URL);
                }
            }
            else
            {
                $file_data = array('upload_data' => $this->upload->data());
                $data['image'] = $file_data['upload_data']['file_name'];
                $data['image_to_delete'] = $user['image'];

            }


            // save the changes
            $saved = $this->users_model->edit_user($data);

            if ($saved)
            {
                $this->session->set_flashdata('message', sprintf(lang('users msg edit_user_success'), $this->input->post('first_name') . " " . $this->input->post('last_name')));
            }
            else
            {
                $this->session->set_flashdata('error', sprintf(lang('users error edit_user_failed'), $this->input->post('first_name') . " " . $this->input->post('last_name')));
            }

            // return to list and display message
            redirect(THIS_URL);
        }

        // setup page header data
        $this
            ->add_css_theme("select/select2.min.css")
            ->add_js_theme( "parsley/parsley.min.js" )
            ->add_js_theme( "pages/parsley-validator.js" )
            ->add_js_theme( "select/select2.full.js" )
            ->add_js_theme( "pages/register.js" )
            ->add_js_theme( "pages/select2.js" )
            ->set_title( lang('users title executive_edit') );

        $data = $this->includes;

        //get shipping details 
        $shipping_query = $this->shipping_details_model->get_shipping_executive(false);
        $shipping = array_combine(array_column($shipping_query, 'id'),array_column($shipping_query, 'name'));

        // set content data
        $content_data = array(
         'cancel_url' => THIS_URL,
         'page_title' => lang('users title executive_edit'),
         'shipping' => $shipping,
         'password_required' => FALSE,
         'user' => $user,
         'user_id'  => $id,
        );

        // load views
        $data['content'] = $this->load->view('admin/users/executives_form', $content_data, TRUE);
        $this->load->view($this->template, $data);

        // echo '<pre>';
        // print_r($user);
        // echo '</pre>';
        // die();

    }


    /**************************************************************************************
     * PRIVATE VALIDATION CALLBACK FUNCTIONS
     **************************************************************************************/


    /**
     * Make sure username is available
     *
     * @param  string $username
     * @param  string|null $current
     * @return int|boolean
     */
    function _check_username($username, $current)
    {
        if (trim($username) != trim($current) && $this->users_model->username_exists($username))
        {
            $this->form_validation->set_message('_check_username', sprintf(lang('users error username_exists'), $username));
            return FALSE;
        }
        else
        {
            return $username;
        }
    }


    /**
     * Make sure email is available
     *
     * @param  string $email
     * @param  string|null $current
     * @return int|boolean
     */
    function _check_email($email, $current)
    {
        if (trim($email) != trim($current) && $this->users_model->email_exists($email))
        {
            $this->form_validation->set_message('_check_email', sprintf(lang('users error email_exists'), $email));
            return FALSE;
        }
        else
        {
            return $email;
        }
    }

    /**
     * Send email register executive
     */
     function send_email_register($uid, $template = null, $data = array())
     {
        $user = $this->users_model->get_user($uid);
        $data['date'] = date('Y-m-d H:i');

        if ($user) {
            $this->lang->load('emails', $user['language']);
            $html = $this->load->view("emails/{$user['language']}/{$template}", $data, TRUE);

            // send email    
            $this->email->from("{$this->settings->site_email}");
            $this->email->to($user['email']);
            $this->email->subject($this->lang->line($template));
            $this->email->message($html);
            $this->email->set_mailtype("html");
            $this->email->send();
        }
     }

    public function download_stock_wholesalers(){
        $this->load->helper('download');

        $stock = $this->db->query("select * from inventario_mayoristas")->result('array');
        $csv_content = implode(",",array_keys($stock[0]))."\n";
        foreach( $stock as $row ){
            if( $row['CANT_VENDIDA'] > $row['CANT_COMPRADA'] ){
                $row['CANT_VENDIDA']= $row['CANT_COMPRADA'];
            }

            $row['BALANCE'] = 0;
            $csv_content .= implode(",",$row)."\n";
        }

        force_download( "stock_mayoristas_".date('Y_m_d_H_i_s').".csv",$csv_content  );
    }

    public function download_orders_wholesaler(){
        $this->load->helper('download');

        $fechIni = $this->input->get('fecha_ini');
        if(empty($fechIni)){
            $fechIni=date('Y-m-').'01';
        }
        $fechFin = $this->input->get('fecha_fin');
        if(empty($fechFin)){
            $fechFin=date('Y-m-t');
        }

        $orders = $this->db->query("
            SELECT
                u.id as 'ID_USUARIO',
                concat(u.username, ' - ', u.first_name,' ', u.last_name) as 'NOMBRE',
                so.shopping_order_id as 'NUM_ORDEN',
                sd.name as 'MAYORISTA',
                so.created as 'FECHA',
                p.product_name as 'PRODUCTO',
                sp.quantity as 'CANTIDAD',
                IF(so.state='approved','APROBADO',IF(so.state='pending', 'PENDIENTE', 'RECHAZADO')) as 'ESTADO'
            FROM shopping_order so
            JOIN  shopping_products sp
                ON so.shopping_order_id = sp.shopping_order_id
            JOIN users u
                ON u.id = so.users_id
            JOIN shiping_details sd
                ON sp.shipping_details_id = sd.id
            JOIN product p
                ON p.product_id = sp.product_id
            WHERE DATE(so.created ) BETWEEN '{$fechIni}' AND '{$fechFin}'
        ")->result('array');

        $csv_content = implode(",",array_keys($orders[0]))."\n";
        foreach( $orders as $row ){

            $csv_content .= implode(",",$row)."\n";
        }

        force_download( "ordenes_mayoristas_".date('Y_m_d_H_i_s').".csv",$csv_content  );
    }


    


}