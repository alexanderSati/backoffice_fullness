<?php defined('BASEPATH') OR exit('No direct script access allowed');

class Privilegies extends Admin_Controller {

    /**
     * Constructor
     */
    function __construct()
    {
        parent::__construct();

        // load the language files
        $this->lang->load('privilegies');

        $this->load->model('privilegies_model', 'privilegies');
        $this->load->model('profiles_model');
    }


    /**
     * Settings Editor
     */
    function index()
    {
        
        if (!is_allowed('read')){
            $this->session->set_flashdata('error', privilegies_message());
            redirect('admin/dashboard');
        }

        if ($this->input->post('save') == TRUE)
        {

            foreach ($this->input->post('privilegies') as $id => $data) {

                $update = array(
                    'read' => (isset($data['read'])) ? implode(',', $data['read']) : "",
                    'insert' => (isset($data['insert'])) ? implode(',', $data['insert']) : "",
                    'update' => (isset($data['update'])) ? implode(',', $data['update']) : "",
                    'delete' => (isset($data['delete'])) ? implode(',', $data['delete']) : "",
                );

                // save privilegy
                $this->privilegies->edit_privilegies($update, $id);
            }

            if ($this->input->post('save'))
            {
                $this->session->set_flashdata('message', lang('msg save_success'));
            }
            else
            {
                $this->session->set_flashdata('error', lang('error save_failed'));
            }

            // reload the page
            redirect('admin/privilegies');
        }

        // setup page header data
        $this            
            ->add_root_css("assets/icon/icofont/css/icofont.css")
            // ->add_css_theme( "datatables/responsive.bootstrap.min.css" )
            // ->add_css_theme( "datatables/jquery.dataTables.min.css" )
            // ->add_css_theme( "datatables/responsive.bootstrap.min.css" )

            // ->add_js_theme( "datatables/jquery.dataTables.min.js")
            // ->add_js_theme( "datatables/dataTables.bootstrap.js")
            // ->add_js_theme( "datatables/dataTables.responsive.min.js")
            // ->add_js_theme( "datatables/responsive.bootstrap.min.js")
            // ->add_js_theme( "pages/privilegies.js")
            
			->set_title(lang('privilegies-title'));

        $data = $this->includes;

        $privilegies = $this->privilegies->get_all();

        foreach ($privilegies as $index => $privilegy) {
            $privilegies[$index]['exp_read'] = explode(',', $privilegy['read']);
            $privilegies[$index]['exp_insert'] = explode(',', $privilegy['insert']);
            $privilegies[$index]['exp_update'] = explode(',', $privilegy['update']);
            $privilegies[$index]['exp_delete'] = explode(',', $privilegy['delete']);
        }

        // set content data
        $content_data = array(
            'privilegies'=> $privilegies,
            'cancel_url' => "/admin",
            'page_title' => $data['page_title'],
            'profiles'   => $this->profiles_model->get_all(),
        );

        // load views
        $data['content'] = $this->load->view('admin/privilegies/form', $content_data, TRUE);
        $this->load->view($this->template, $data);
    }

}
