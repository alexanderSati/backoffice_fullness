<?php defined('BASEPATH') or exit('No direct script access allowed');

class Quantities extends Private_Controller
{


    function  __construct()
    {
        parent::__construct();

        // load the models
        $this->load->model('Shipping_details_model');
        $this->load->model('productsQuantities_model');
        $this->load->model('products_model');

        define('THIS_URL', base_url('admin/quantities'));

        // use the url in session (if available) to return to the previous list
        if ($this->session->userdata(REFERRER)) {
            $this->_redirect_url = $this->session->userdata(REFERRER);
        } else {
            $this->_redirect_url = THIS_URL;
        }
    }





    function index()
    {
        // setup page header data
        $this
            ->add_root_js("bower_components/datatables.net/js/jquery.dataTables.min.js")
            ->add_root_js("assets/pages/data-table/js/data-table-custom.js")
            ->add_root_js("bower_components/datatables.net-bs4/js/dataTables.bootstrap4.min.js")
            ->add_root_css("bower_components/datatables.net-bs4/css/dataTables.bootstrap4.min.css")
            ->add_js_theme("pages/product_quantities.js")
            ->set_title(lang('users title products_quantities'));
        $data = $this->includes;
        $where = [];
        if (!empty($this->Shipping_details_model->get('*', 'id_user=' . $this->user['id'], 1))) {
            $shipping_details = $this->Shipping_details_model->get('*', 'id_user=' . $this->user['id'], 1);
            $where["q.shiping_details_id"] = $shipping_details["id"];
        }
        $product_quantities = $this->productsQuantities_model->obtenerRegistros($where);
        $content_data = array(
            'page_title'    =>  $data['page_title'],
            'product_quantities' => $product_quantities,
            'this_url'   => THIS_URL,
            'is_executive' => $this->user['profile'] == 'executive' ? true : false
        );
        $data['content'] = $this->load->view('admin/shopping/list_shipping_products_quantities', $content_data, TRUE);
        $this->load->view($this->template, $data);
    }

    function add()
    {
        $this->form_validation->set_error_delimiters($this->config->item('error_delimeter_left'), $this->config->item('error_delimeter_right'));
        $this->form_validation->set_rules('shiping_details_id', lang('users input shiping_details_id'), 'required');
        $this->form_validation->set_rules('product_id', lang('users input product_name'), 'required');
        $this->form_validation->set_rules('quantity', lang('users input quantity'), 'required|numeric|callback__negative_quantity');

        if ($this->form_validation->run() == TRUE) {
            $data = $this->input->post();
            //Metodo para saber si un producto ya esta vinculado con el punto de recogida
            $existing = $this->productsQuantities_model->get_register_existing($data);
            if ($existing) {
                $this->session->set_flashdata('error', sprintf(lang('users error exist_shiping_details_id')));
            } else {
                $saved = $this->productsQuantities_model->crear($data);
                if ($saved) {
                    $this->session->set_flashdata('message', sprintf(lang('users msg add_product_quantity')));
                } else {
                    $this->session->set_flashdata('error', sprintf(lang('users error add_product_quantity')));
                }
            }
            // return to list and display message
            redirect($this->_redirect_url);
        }
         // setup page header data
        $this->set_title(lang('users title add_products_quantities'));
        $this
            ->add_css_theme("select/select2.min.css")
            ->add_js_theme("parsley/parsley.min.js")
            ->add_js_theme("pages/parsley-validator.js")
            ->add_js_theme("select/select2.full.js")
            //->add_js_theme("pages/product_quantities.js")
            ->add_js_theme("pages/select2.js");
        $data = $this->includes;
        $shipping_detail = $this->Shipping_details_model->getShipping_details();
        $products = $this->products_model->get_products();
        //shipping_details_form
        $content_data = array(
            'page_title' => $data['page_title'],
            'products' => $products,
            'shipping_detail' => $shipping_detail,
            'product_quantity' => null,
            'edit' => 0,
            'cancel_url'        => $this->_redirect_url,
        );
        $data['content'] = $this->load->view('admin/shopping/shipping_products_quantities_form', $content_data, TRUE);
        $this->load->view($this->template, $data);
    }

    function export()
    {
        $where = [];
        if (!empty($this->Shipping_details_model->get('*', 'id_user=' . $this->user['id'], 1))) {
            $shipping_details = $this->Shipping_details_model->get('*', 'id_user=' . $this->user['id'], 1);
            $where["q.shiping_details_id"] = $shipping_details["id"];
        }
        $product_quantities = $this->productsQuantities_model->obtenerRegistros($where);

        if ($product_quantities > 0) {

            foreach ($product_quantities as $key => $product) {
                unset($product_quantities[$key]['name']);
                unset($product_quantities[$key]['id']);
            }
            // export the 
            array_to_csv($product_quantities, "products");
        } else {
            // nothing to export
            $this->session->set_flashdata('error', lang('core error no_results'));
            redirect($this->_redirect_url);
        }
        exit;
    }

    /**
     * Edit existing product
     *
     * @param  int $id
     */

    function edit($id = NULL)
    {
        // make sure we have a numeric id
        if (is_null($id) or !is_numeric($id)) {
            redirect($this->_redirect_url);
        }

        // get the data
        $product_quantity = $this->productsQuantities_model->obtenerRegistroPorId($id);

        // if empty results, return to list
        if (!$product_quantity) {
            redirect($this->_redirect_url);
        }

        // validators
        $this->form_validation->set_error_delimiters($this->config->item('error_delimeter_left'), $this->config->item('error_delimeter_right'));
        $this->form_validation->set_rules('quantity', lang('users input quantity'), 'required|numeric|callback__negative_quantity');

        if ($this->form_validation->run() == TRUE) {
            $data = $this->input->post();

            // save the changes
            $saved = $this->productsQuantities_model->actualizar($data);
            if ($saved) {
                $this->session->set_flashdata('message', sprintf(lang('users msg edit_product_quantity')));
            } else {
                $this->session->set_flashdata('error', sprintf(lang('users error edit_product_quantity')));
            }
            // return to list and display message
            redirect($this->_redirect_url);
        }
        // setup page header data
        $this
            ->add_css_theme("select/select2.min.css")
            ->add_js_theme("parsley/parsley.min.js")
            ->add_js_theme("pages/parsley-validator.js")
            ->add_js_theme("select/select2.full.js")
            ->add_js_theme("pages/product_quantities.js")
            ->add_js_theme("pages/select2.js")
            ->set_title(lang('users title edit_products_quantities'));
        $data = $this->includes;

        $shipping_detail = $this->Shipping_details_model->getShipping_details();
        $products = $this->products_model->get_products();

       // set content data
        $content_data = array(
            'cancel_url'        => $this->_redirect_url,
            'product_quantity'   => $product_quantity,
            'products' => $products,
            'shipping_detail' => $shipping_detail,
            'id'                => $id,
            'edit' => 1,
            'page_title'        => $data['page_title']
        );

        // load views
        $data['content'] = $this->load->view('admin/shopping/shipping_products_quantities_form', $content_data, TRUE);
        $this->load->view($this->template, $data);
    }

    function _negative_quantity($quantity)
   {
        if ($quantity <= 0) {
            $this->form_validation->set_message('_negative_quantity', sprintf(lang('users error number_negative')));
            return FALSE;
        } else {
            return $quantity;
        }
    }
}
