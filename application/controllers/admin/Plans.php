<?php defined('BASEPATH') OR exit('No direct script access allowed');

class Plans extends Admin_Controller {

    /**
     * Constructor
     */
    function __construct()
    {
        parent::__construct();

        $this->load->model('plans_model');
        $this->lang->load('plans');

        // set constants
        define('REFERRER', "referrer");
        define('THIS_URL', base_url('admin/plans'));
        define('DEFAULT_LIMIT', $this->settings->per_page_limit);
        define('DEFAULT_OFFSET', 0);
        define('DEFAULT_SORT', "base");
        define('DEFAULT_DIR', "asc");

        // use the url in session (if available) to return to the previous filter/sorted/paginated list
        if ($this->session->userdata(REFERRER))
        {
            $this->_redirect_url = $this->session->userdata(REFERRER);
        }
        else
        {
            $this->_redirect_url = THIS_URL; 
        }
    }


    /**
     * Settings Editor
     */
    function index()
    {
        if (!is_allowed('read')){
            $this->session->set_flashdata('error', privilegies_message());
            redirect('admin/dashboard');
        }

        $lang = (isset($user['language']) ? $user['language'] : $this->config->item('language'));

        // get parameters
        $limit  = $this->input->get('limit')  ? $this->input->get('limit', TRUE)  : DEFAULT_LIMIT;
        $offset = $this->input->get('offset') ? $this->input->get('offset', TRUE) : DEFAULT_OFFSET;
        $sort   = $this->input->get('sort')   ? $this->input->get('sort', TRUE)   : DEFAULT_SORT;
        $dir    = $this->input->get('dir')    ? $this->input->get('dir', TRUE)    : DEFAULT_DIR;

        // get filters
        $filters = array();

        if ($this->input->get('base'))
        {
            $filters['base'] = $this->input->get('base', TRUE);
        }

        if ($this->input->get('period_days'))
        {
            $filters['period_days'] = $this->input->get('period_days', TRUE);
        }

        if ($this->input->get("{$lang}_name"))
        {
            $filters["{$lang}_name"] = $this->input->get("{$lang}_name", TRUE);
        }

        // build filter string
        $filter = "";
        foreach ($filters as $key => $value)
        {
            $filter .= "&{$key}={$value}";
        }

        // save the current url to session for returning
        $this->session->set_userdata(REFERRER, THIS_URL . "?sort={$sort}&dir={$dir}&limit={$limit}&offset={$offset}{$filter}");

        // are filters being submitted?
        if ($this->input->post())
        {
            if ($this->input->post('clear'))
            {
                // reset button clicked
                redirect(THIS_URL);
            }
            else
            {
                // apply the filter(s)
                $filter = "";

                if ($this->input->post('base'))
                {
                    $filter .= "&base=" . $this->input->post('base', TRUE);
                }

                if ($this->input->post('period_days'))
                {
                    $filter .= "&period_days=" . $this->input->post('period_days', TRUE);
                }

                if ($this->input->post("{$lang}_name"))
                {
                    $filter .= "&{$lang}_name=" . $this->input->post("{$lang}_name", TRUE);
                }

                // redirect using new filter(s)
                redirect(THIS_URL . "?sort={$sort}&dir={$dir}&limit={$limit}&offset={$offset}{$filter}");
            }
        }

        // get list
        $plans = $this->plans_model->get_all_row($limit, $offset, $filters, $sort, $dir);

        // build pagination
        $this->pagination->initialize(array(
            'base_url'   => THIS_URL . "?sort={$sort}&dir={$dir}&limit={$limit}{$filter}",
            'total_rows' => $plans['total'],
            'per_page'   => $limit
        ));

        // setup page header data
        $this
            // ->add_css_theme( "datatables/jquery.dataTables.min.css" )
            // ->add_css_theme( "datatables/buttons.bootstrap.min.css" )
            // ->add_css_theme( "datatables/fixedHeader.bootstrap.min.css" )
            // ->add_css_theme( "datatables/responsive.bootstrap.min.css" )
            // ->add_css_theme( "datatables/scroller.bootstrap.min.css" )

            // ->add_js_theme( "datatables/jquery.dataTables.min.js" )
            // ->add_js_theme( "datatables/dataTables.bootstrap.js" )
            // ->add_js_theme( "datatables/dataTables.buttons.min.js" )
            // ->add_js_theme( "datatables/buttons.bootstrap.min.js" )
            // ->add_js_theme( "datatables/jszip.min.js" )
            // ->add_js_theme( "datatables/pdfmake.min.js" )
            // ->add_js_theme( "datatables/vfs_fonts.js" )
            // ->add_js_theme( "datatables/buttons.html5.min.js" )
            // ->add_js_theme( "datatables/buttons.print.min.js" )
            // ->add_js_theme( "datatables/dataTables.fixedHeader.min.js" )
            // ->add_js_theme( "datatables/dataTables.keyTable.min.js" )
            // ->add_js_theme( "datatables/dataTables.responsive.min.js" )
            // ->add_js_theme( "datatables/responsive.bootstrap.min.js" )
            // ->add_js_theme( "datatables/dataTables.scroller.min.js" )
            // ->add_js_theme( "pages/tables.js" )

            ->add_js_theme( "pages/plans.js" )

            ->set_title( lang('admin plans title') );

        $data = $this->includes;

        // set content data
        $content_data = array(
            'this_url'   => THIS_URL,
            'plans'      => $plans['results'],
            'total'      => $plans['total'],
            'filters'    => $filters,
            'filter'     => $filter,
            'pagination' => $this->pagination->create_links(),
            'limit'      => $limit,
            'offset'     => $offset,
            'sort'       => $sort,
            'dir'        => $dir,
            'page_title' => $data['page_title'],
            'lang'       => $lang
        );

        // load views
        $data['content'] = $this->load->view('admin/plans/list', $content_data, TRUE);
        $this->load->view($this->template, $data);
    }

    /*
    * Create new Plan
    */
    function add(){

        if (!is_allowed('insert')){
            $this->session->set_flashdata('error', privilegies_message());
            redirect('admin/dashboard');
        }

       // validators
       $this->form_validation->set_error_delimiters($this->config->item('error_delimeter_left'), $this->config->item('error_delimeter_right'));

       $min_profit = $this->input->post('min_profit');
       $max_profit = $this->input->post('max_profit');
       $this->form_validation->set_rules('base', lang('plans input base'), 'required|trim|numeric');
       $this->form_validation->set_rules('period_days', lang('plans input period_days'), 'required|trim|numeric');
       $this->form_validation->set_rules('period_days_before', lang('plans input period_days_before'), 'required|trim|numeric');
       $this->form_validation->set_rules('min_profit', lang('plans input min_profit'), 'required|trim|numeric');
       $this->form_validation->set_rules('max_profit', lang('plans input max_profit'), 'required|trim|numeric|greater_than['.$min_profit.']');
       $this->form_validation->set_rules('status', lang('plans input status'), 'required');
       $this->form_validation->set_rules('spanish_name', lang('plans input spanish_name'), 'required|trim');
       $this->form_validation->set_rules('spanish_features', lang('plans input spanish_features'), 'required|trim');
       $this->form_validation->set_rules('english_name', lang('plans input english_name'), 'required|trim');
       $this->form_validation->set_rules('english_features', lang('plans input english_features'), 'required|trim');
       
       if ($this->form_validation->run() == TRUE)
       {

           // save the new Plan
           $saved = $this->plans_model->add_plan($this->input->post());
       
           if ($saved)
           {
       
               // Create a subscription
               $data = $this->input->post();
               $data['id_plan'] = $saved;
               $this->session->set_flashdata('message', lang('plans msg add_user_success'));
           }
           else
           {
               $this->session->set_flashdata('error', lang('plans error add_user_failed'));
           }

           // return to list and display message
           redirect($this->_redirect_url);
       }

       // setup page header data

       $this
           ->add_js_theme( "parsley/parsley.min.js" )
           ->add_js_theme( "pages/parsley-validator.js" )
           ->add_js_theme( "pages/register.js" )
           ->add_css_theme("summernote.css")
           ->add_js_theme('summernote.min.js')
           ->add_js_theme( "pages/editor.js" )

           ->set_title( lang('plans title plans_add') );

       $data = $this->includes;

       // set content data
       $content_data = array(
           'cancel_url'        => $this->_redirect_url,
           'page_title'        => $data['page_title']
       );

       // load views
       $data['content'] = $this->load->view('admin/plans/plans_form', $content_data, TRUE);
       $this->load->view($this->template, $data);
    }

    /**
     * Edit existing Plan
     *
     * @param  int $id
     */
    function edit($id = NULL)
    {
        if (!is_allowed('update')){
            $this->session->set_flashdata('error', privilegies_message());
            redirect('admin/dashboard');
        }

        // make sure we have a numeric id
        if (is_null($id) OR ! is_numeric($id))
        {
            redirect($this->_redirect_url);
        }

        // get the data
        $plan = $this->plans_model->get_plan($id);
        $plan['excluded_days'] = explode(',', $plan['excluded_days']);

        // if empty results, return to list
        if ( ! $plan)
        {
            redirect($this->_redirect_url);
        }

        $min_profit = $this->input->post('min_profit');
        $max_profit = $this->input->post('max_profit');
        $this->form_validation->set_rules('base', lang('plans input base'), 'required|trim|numeric');
        $this->form_validation->set_rules('period_days', lang('plans input period_days'), 'required|trim|numeric');
        $this->form_validation->set_rules('period_days_before', lang('plans input period_days_before'), 'required|trim|numeric');
        $this->form_validation->set_rules('min_profit', lang('plans input min_profit'), 'required|trim|numeric');
        $this->form_validation->set_rules('max_profit', lang('plans input max_profit'), 'required|trim|numeric|greater_than['.$min_profit.']');
        $this->form_validation->set_rules('status', lang('plans input status'), 'required');
        $this->form_validation->set_rules('spanish_name', lang('plans input spanish_name'), 'required|trim');
        $this->form_validation->set_rules('spanish_features', lang('plans input spanish_features'), 'required|trim');
        $this->form_validation->set_rules('english_name', lang('plans input english_name'), 'required|trim');
        $this->form_validation->set_rules('english_features', lang('plans input english_features'), 'required|trim');

        if ($this->form_validation->run() == TRUE)
        {

            // save the changes
            $saved = $this->plans_model->edit_plan($this->input->post());

            if ($saved)
            {
                $this->session->set_flashdata('message', lang('plans msg edit_user_success'));
            }
            else
            {
                $this->session->set_flashdata('error', lang('plans error edit_user_failed'));
            }

            // return to list and display message
            redirect($this->_redirect_url);
        }

        // setup page header data
        $this                
            ->add_js_theme( "parsley/parsley.min.js" )
            ->add_js_theme( "pages/parsley-validator.js" )
            ->add_js_theme( "pages/register.js" )
            ->add_css_theme("summernote.css")
            ->add_js_theme('summernote.min.js')
            ->add_js_theme( "pages/editor.js" )

            ->set_title( lang('plans title plans_edit') );

        $data = $this->includes;

        // set content data
        $content_data = array(
            'cancel_url'        => $this->_redirect_url,
            'plan_id'           => $id,
            'plan'           => $plan,
            'page_title'        => $data['page_title']
        );

        // load views
        $data['content'] = $this->load->view('admin/plans/plans_form', $content_data, TRUE);
        $this->load->view($this->template, $data);
    }

    /**
     * Export list to CSV
     */
    function export()
    {
        $lang = (isset($user['language']) ? $user['language'] : $this->config->item('language'));

        // get parameters
        $sort = $this->input->get('sort') ? $this->input->get('sort', TRUE) : DEFAULT_SORT;
        $dir  = $this->input->get('dir')  ? $this->input->get('dir', TRUE)  : DEFAULT_DIR;

        // get filters
        $filters = array();

        if ($this->input->get('base'))
        {
            $filters['base'] = $this->input->get('base', TRUE);
        }

        if ($this->input->get('period_days'))
        {
            $filters['period_days'] = $this->input->get('period_days', TRUE);
        }

        if ($this->input->get("{$lang}_name"))
        {
            $filters["{$lang}_name"] = $this->input->get("{$lang}_name", TRUE);
        }

        // get all users
        $plans = $this->plans_model->get_all_row(0, 0, $filters, $sort, $dir);

        if ($plans['total'] > 0)
        {
            // manipulate the output array
            foreach ($plans['results'] as $key=>$user)
            {
                unset($plans['results'][$key]['password']);
                unset($plans['results'][$key]['deleted']);

                if ($user['status'] == 0)
                {
                    $plans['results'][$key]['status'] = lang('admin input inactive');
                }
                else
                {
                    $plans['results'][$key]['status'] = lang('admin input active');
                }
            }

            // export the file
            array_to_csv($plans['results'], "plans");
        }
        else
        {
            // nothing to export
            $this->session->set_flashdata('error', lang('core error no_results'));
            redirect($this->_redirect_url);
        }

        exit;
    }

    /**
     * Delete a user
     *
     * @param  int $id
     */
    function delete($id = NULL)
    {
        if (!is_allowed('delete')){
            $this->session->set_flashdata('error', privilegies_message());
            redirect('admin/dashboard');
        }
        
        // make sure we have a numeric id
        if ( ! is_null($id) OR ! is_numeric($id))
        {
            // get user details
            $plan = $this->plans_model->get_plan($id);

            if ($plan)
            {
                // soft-delete the user
                $delete = $this->plans_model->delete_plan($id);

                if ($delete)
                {
                    $this->session->set_flashdata('message', lang('plans msg delete_user'));
                }
                else
                {
                    $this->session->set_flashdata('error', lang('plans error delete_user'));
                }
            }
            else
            {
                $this->session->set_flashdata('error', lang('plans error user_not_exist'));
            }
        }
        else
        {
            $this->session->set_flashdata('error', lang('plans error user_id_required'));
        }

        // return to list and display message
        redirect($this->_redirect_url);
    }

}
