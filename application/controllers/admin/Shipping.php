<?php defined('BASEPATH') OR exit('No direct script access allowed');

class Shipping extends Private_Controller
{
    function  __construct()
    { 
        parent::__construct();
        
        // load the models
        $this->load->model('shipping_details_model');
        $this->load->model('shippingTypes_model');
        
        define('THIS_URL', base_url('admin/shipping'));
        define('REFERRER', "referrer");

        // use the url in session (if available) to return to the previous list
                  
            $this->_redirect_url = THIS_URL;

    }

    function index()
    {
         // setup page header data
         $this
         ->add_root_js( "bower_components/datatables.net/js/jquery.dataTables.min.js" )
         ->add_root_js( "assets/pages/data-table/js/data-table-custom.js" )
         ->add_root_js( "bower_components/datatables.net-bs4/js/dataTables.bootstrap4.min.js" )
         ->add_root_css( "bower_components/datatables.net-bs4/css/dataTables.bootstrap4.min.css" )

         ->add_js_theme( "pages/shipping_details.js" )

         ->set_title( lang('users title shipping_details') );
         $data = $this->includes;
         $shipping_details = $this->shipping_details_model->obtenerListaRegistros();
      
         $content_data = array(
            'page_title'    => lang('users title shipping_details'),
            'shipping_details' => $shipping_details,
            'this_url'   => THIS_URL
        );

        $data['content'] = $this->load->view('admin/shopping/list_shipping_details', $content_data, TRUE);
        $this->load->view($this->template, $data);
    }
    function add()
    {
        

        $this->form_validation->set_error_delimiters($this->config->item('error_delimeter_left'), $this->config->item('error_delimeter_right'));
        $this->form_validation->set_rules('name', lang('users input name'), 'required|callback__shipping_exists');
        $this->form_validation->set_rules('description', lang('users input description'), 'required');        
        $this->form_validation->set_rules('shipping_types_id', lang('users input shipping_types_id'), 'required|numeric');

        if ($this->form_validation->run() == TRUE) {
           $data = $this->input->post();

           $saved = $this->shipping_details_model->crear($data);

           if ($saved)
           {

               $this->session->set_flashdata('message', sprintf(lang('users msg add_shipping_detail'), $this->input->post('name')));
           }
           else
           {
               $this->session->set_flashdata('error', sprintf(lang('users error add_shipping_detail_failed'), $this->input->post('name')));
           }

           // return to list and display message
           redirect($this->_redirect_url);
        }
        // setup page header data
        $this->set_title(lang('users title add_shipping_details'));

        $this
                ->add_css_theme("select/select2.min.css")
                ->add_js_theme("parsley/parsley.min.js")
                ->add_js_theme("pages/parsley-validator.js")
                ->add_js_theme("select/select2.full.js")
                ->add_js_theme("pages/shipping_details.js")
                ->add_js_theme("pages/select2.js");

        $data = $this->includes;

        $shipping_types = $this->shippingTypes_model->getShipping_types();
    
        //shipping_details_form
        $content_data = array(
            'page_title' => $data['page_title'],
            'shipping_types' => $shipping_types,
            'shipping_detail' => NULL,
            'cancel_url'        => $this->_redirect_url,
        );
     

        $data['content'] = $this->load->view('admin/shopping/shipping_details_form', $content_data, TRUE);
        $this->load->view($this->template, $data);
    }

    /**
     * Edit existing product
     *
     * @param  int $id
     */
    function edit($id = NULL)
    {
        // make sure we have a numeric id
        if (is_null($id) OR ! is_numeric($id))
        {
            redirect($this->_redirect_url);
        }

        // get the data
        $shipping_details = $this->shipping_details_model->obtenerRegistroPorId($id);

        // if empty results, return to list
        if ( ! $shipping_details)
        {
            redirect($this->_redirect_url);
        }

        // validators
        $this->form_validation->set_error_delimiters($this->config->item('error_delimeter_left'), $this->config->item('error_delimeter_right'));
        $this->form_validation->set_rules('name', lang('users input name'), 'required');
        $this->form_validation->set_rules('description', lang('users input description'), 'required');        
        $this->form_validation->set_rules('shipping_types_id', lang('users input shipping_types_id'), 'required|numeric');

        if ($this->form_validation->run() == TRUE)
        {
            $data = $this->input->post();

            // save the changes
            $saved = $this->shipping_details_model->actualizar($data);

            if ($saved)
            {

                $this->session->set_flashdata('message', sprintf(lang('users msg edit_shipping_detail'), $this->input->post('name')));
            }
            else
            {
                $this->session->set_flashdata('error', sprintf(lang('users error edit_shipping_detail_failed'), $this->input->post('name')));
            }

            // return to list and display message
            redirect($this->_redirect_url);
        }

        // setup page header data
        $this
            ->add_css_theme("select/select2.min.css")
            ->add_js_theme("parsley/parsley.min.js")
            ->add_js_theme("pages/parsley-validator.js")
            ->add_js_theme("select/select2.full.js")
            ->add_js_theme("pages/shipping_details.js")
            ->add_js_theme("pages/select2.js")
            ->set_title( lang('users title edit_shipping_details') );

        $data = $this->includes;

        // set content data
        $content_data = array(
            'cancel_url'        => $this->_redirect_url,
            'shipping_detail'   => $shipping_details,
            'shipping_types'    => $this->shippingTypes_model->getShipping_types(),
            'id'                => $id,
            'page_title'        => $data['page_title']
        );

        // load views
        $data['content'] = $this->load->view('admin/shopping/shipping_details_form', $content_data, TRUE);
        $this->load->view($this->template, $data);
    }

    /**
     * Metodo para no repetir punto de recogida
     */
    function _shipping_exists($name)
    {
        $shipping =$this->shipping_details_model->get_shipping_name($name);

        if (empty($shipping)) {
            return $name;
        } else {
            $this->form_validation->set_message('_shipping_exists', sprintf(lang('users error shipping_exists')));
            return FALSE;
        }
    }
}