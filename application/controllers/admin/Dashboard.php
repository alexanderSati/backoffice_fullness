<?php defined('BASEPATH') OR exit('No direct script access allowed');

class Dashboard extends Admin_Controller {

    /**
     * Constructor
     */
    function __construct()
    {
        parent::__construct();

        // load the language files
        $this->lang->load('dashboard');
        $this->load->model('users_model','users');
        $this->load->model('transactions_model');
        $this->load->model('currencies_model','currencies');
        $this->load->model('locations_model','locations');
        $this->load->model('subscriptions_model');
        $this->load->library('transaction');

    }


    /**
     * Dashboard
     */
    function index()
    {
        // setup page header data
		$this
            // ->add_css_theme( "floatexamples.css" )
            // ->add_css_theme( "datatables/responsive.bootstrap.min.css" )
            // ->add_css_theme( "datatables/jquery.dataTables.min.css" )
            // ->add_css_theme( "datatables/responsive.bootstrap.min.css" )
            ->add_root_js( "bower_components/datatables.net/js/jquery.dataTables.min.js" )
            ->add_root_js( "assets/pages/data-table/js/data-table-custom.js" )
            ->add_root_js( "bower_components/datatables.net-bs4/js/dataTables.bootstrap4.min.js" )            
            ->add_root_css( "bower_components/datatables.net-bs4/css/dataTables.bootstrap4.min.css" ) 

            ->add_root_js( "js/flot/jquery.flot.js" )
            ->add_root_js( "js/flot/jquery.flot.time.min.js" )
            ->add_root_js( "js/flot/date.js" )
            
            // ->add_js_theme( "datatables/jquery.dataTables.min.js")
            // ->add_js_theme( "datatables/dataTables.bootstrap.js")
            // ->add_js_theme( "datatables/dataTables.responsive.min.js")
            // ->add_js_theme( "datatables/responsive.bootstrap.min.js")
            ->add_root_js( "js/pages/dashboard.js" )

			->set_title( lang('admin title admin') );
		
        $data = $this->includes;

        $transactions = $this->transactions_model->get(array(),500);
        $pending_transactions = $this->transactions_model->get(array('status'=>'pending'));

        foreach ($transactions as $index => $transaction) {
            $user = $this->users->get_user($transaction['id_user']);
            $transactions[$index]['name_user'] = $user['first_name'] . " " . $user['last_name'];
        }
        /*echo "<pre>";
        print_r($transactions);
        echo "</pre>";
        die(__FILE__ . ' in line ' . __LINE__);*/
        // set content data
        $content_data = array(
            'users_count'    => $this->users->count(),
            'recents'        => $this->users->get_latest(),
            'countries'      => $this->locations->getCountries(true),
            'transactions'   => $transactions,
            'pending_transactions'   => $pending_transactions,
            'currencies'     => $this->currencies->get("BTC"),
            'chart'          => $this->subscriptions_model->sale_plans(),
        );

        $data['content'] = $this->load->view('admin/dashboard', $content_data, TRUE);

        $this->load->view($this->template, $data);
    }

}