<?php defined('BASEPATH') OR exit('No direct script access allowed');

class Transactions extends Admin_Controller {

    /**
     * Constructor
     */
    function __construct()
    {
        parent::__construct();

        // load the language files
        $this->lang->load('dashboard');
        $this->lang->load('dashboard');

        $this->load->model('users_model','users');
        $this->load->model('locations_model','locations');
        $this->load->model('transactions_model');

        $this->load->library('transaction');
        $this->load->library('wallet');
        $this->load->library('email');

        define('THIS_URL', base_url('admin/transactions/payout_request'));

    }


    /**
     * Transacitons
     */
    function index()
    {
        // setup page header data
        $this
            ->add_root_js( "bower_components/datatables.net/js/jquery.dataTables.min.js" )
            ->add_root_js( "assets/pages/data-table/js/data-table-custom.js" )
            ->add_root_js( "bower_components/datatables.net-bs4/js/dataTables.bootstrap4.min.js" )            
            ->add_root_css( "bower_components/datatables.net-bs4/css/dataTables.bootstrap4.min.css" ) 
            
            ->add_js_theme( "pages/transactions.js" )

			->set_title( lang('admin title pending transactions') );
		
        $data = $this->includes;

        // set content data
        $content_data = array(
            'transactions'   => $this->transaction->admin(array('status !=' => 'pending'),2000),
            'pending_transactions'   => $this->transaction->admin(array('status' =>'pending', 'type !=' => 'trading')),
        );
           
        $data['content'] = $this->load->view('admin/transactions/transactions', $content_data, TRUE);

        $this->load->view($this->template, $data);
    }

    function ajax($request = null, $param1 = null, $param2 = null){
        if (!$this->input->is_ajax_request()) {
            // No direct script access allowed
            exit;
        }

        switch ($request) {
            case "change-status" :

                if ($param1 && $param2) {
                    
                    $result = $this->transaction->updateStatus($param1, $param2);
                    
                    switch ($result) {
                        case true:
                            $this->session->set_flashdata('message', lang("success update"));
                            echo "success";
                            break;

                        default:
                            $this->session->set_flashdata('error', lang("error update"));
                            echo "error";
                            break;
                    }                   
                    
                } else {
                    $this->session->set_flashdata('error', lang("error update"));
                    echo "error";
                }

                break;

            default :
                break;
        }
    }


    public function payout_request(){

        // setup page header data
        $this
        ->add_root_js( "bower_components/datatables.net/js/jquery.dataTables.min.js" )
        ->add_root_js( "assets/pages/data-table/js/data-table-custom.js" )
        ->add_root_js( "bower_components/datatables.net-bs4/js/dataTables.bootstrap4.min.js" )
        ->add_root_css( "bower_components/datatables.net-bs4/css/dataTables.bootstrap4.min.css" )
            ->set_title( lang('admin title payout_request') );

        $data = $this->includes;

        $where = [];

        $payout = $this->transaction->get_payouts('transactions.*,users.username,users.first_name,users.last_name,users.dni',$where,$offset ,null);

        // set content data
        $content_data = array(
            'payouts'   => $payout,
            'page_title' => lang('admin title payout_request'),
            'this_url'   => THIS_URL,
        
        );
        

        $data['content'] = $this->load->view('admin/transactions/payout_request', $content_data, TRUE);

        $this->load->view($this->template, $data);
    }


    public function approve( $transaction_id ){
        // Pone la transaccion de payout como hecha
        $this->transactions_model->update($transaction_id,["status"=>"done"]);

        $transaction = $this->transaction->get_by_id( $transaction_id );
        
        $user = $this->users->get_user($transaction['id_user']);

        $content_email['user'] = $user;
        $content_email['transaction'] = $transaction;

        //Enviar email
        $this->send_email_approve_payout($content_email);

        $this->session->set_flashdata('message', 'Payout '.$transaction["cod"]." has been approved");
        redirect( site_url('admin/transactions/payout_request') );
    }


    public function deny( $transaction_id ){
        $transaction = $this->transaction->get_by_id( $transaction_id );
        $user = $this->users->get_user($transaction['id_user']);

        $content_email['user'] = $user;
        $content_email['transaction'] = $transaction;

        // Pone la transaccion de payout como rechazada
        $this->transactions_model->update($transaction_id,["status"=>"rejected"]);


        // Genera una transaccion de reembolso de fondos al usuario
        $this->transaction->refund( $transaction['id_user'], $transaction["usd"], "Reembolso de pago '".$transaction['cod']."' rejected");

        // devolver el saldo a la billetera
        $this->wallet->add_funds($transaction['id_user'], $transaction["usd"], 'USD');

        //Enviar email
        $this->send_email_deny_payout($content_email);

        $this->session->set_flashdata('error', 'Payout '.$transaction["cod"]." has been denied");
        redirect( site_url('admin/transactions/payout_request') );
    }

    private function send_email_approve_payout($content_email){
    
        if ($content_email) {
            $html = $this->load->view("emails/".$content_email['user']['language']."/approve_payout", $content_email, TRUE);
    
            // send email
            
            $this->email->from("{$this->settings->site_email}");
            $this->email->to($content_email['user']['email']);

            if($content_email['user']['language'] == "english") {                
                $this->email->subject("Payout approve for ". $content_email['user']['first_name']);
            }elseif ($content_email['user']['language'] == "spanish") {
                $this->email->subject("Payout aprobado para " . $content_email['user']['first_name']);
            }
            elseif ($content_email['user']['language'] == "portuguese") {
                $this->email->subject("Payout aprovado para " . $content_email['user']['first_name']);
            }

            $this->email->message($html);
            $this->email->set_mailtype("html");
            $this->email->send();
        }           

    }

    private function send_email_deny_payout($content_email){
    
        if ($content_email) {
            $html = $this->load->view("emails/".$content_email['user']['language']."/deny_payout", $content_email, TRUE);
    
            // send email
            
            $this->email->from("{$this->settings->site_email}");
            $this->email->to($content_email['user']['email']);

            if($content_email['user']['language'] == "english") {                
                $this->email->subject("Payout deny for ". $content_email['user']['first_name']);
            }elseif ($content_email['user']['language'] == "spanish") {
                $this->email->subject("Payout denegado para " . $content_email['user']['first_name']);
            }
            elseif ($content_email['user']['language'] == "portuguese") {
                $this->email->subject("Payout negado por " . $content_email['user']['first_name']);
            }
            
            $this->email->message($html);
            $this->email->set_mailtype("html");
            $this->email->send();
        }           

    }

}