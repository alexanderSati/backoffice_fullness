<?php defined('BASEPATH') OR exit('No direct script access allowed');







class Users extends Admin_Controller {







    /**



     * @var string



     */



    private $_redirect_url;











    /**



     * Constructor



     */



    function __construct()



    {



        parent::__construct();







        // load the language files



        $this->lang->load('users');







        // load the users model



        $this->load->model('users_model');



        $this->load->model('profiles_model');



        $this->load->model('plans_model');



        $this->load->model('subscriptions_model','subscriptions');



        $this->load->model('locations_model','locations');



        $this->load->model('transactions_model','transactions');



        $this->load->model('wallets_model','wallets');



        



        // upload config



        $config['upload_path']          = "uploads/";



        $config['allowed_types']        = 'gif|jpg|png';



        $config['max_size']             = 0;



        $config['max_width']            = 1024;



        $config['max_height']           = 1024;







        $this->load->library('upload', $config);



        $this->load->library('referral');



        $this->load->library('binarytree');



        $this->load->library('wallet');







        // set constants



        define('REFERRER', "referrer");



        define('THIS_URL', base_url('admin/users'));



        define('THIS_URL_CAPITAL', base_url('admin/users/users_by_capital'));



        define('DEFAULT_LIMIT', $this->settings->per_page_limit);



        define('DEFAULT_OFFSET', 0);



        define('DEFAULT_SORT', "created");



        define('DEFAULT_DIR', "desc");







        // use the url in session (if available) to return to the previous filter/sorted/paginated list



        if ($this->session->userdata(REFERRER))



        {



            $this->_redirect_url = $this->session->userdata(REFERRER);



        }



        else



        {



            $this->_redirect_url = THIS_URL;



        }



    }











    /**************************************************************************************



     * PUBLIC FUNCTIONS



     **************************************************************************************/











    /**



     * User list page



     */



    function index()



    {



        if (!is_allowed('read')){



            $this->session->set_flashdata('error', privilegies_message());



            redirect('admin/dashboard');



        }







        $lang = (isset($user['language']) ? $user['language'] : $this->config->item('language'));







        // get parameters



        $limit  = $this->input->get('limit')  ? $this->input->get('limit', TRUE)  : 100;



        $offset = $this->input->get('offset') ? $this->input->get('offset', TRUE) : DEFAULT_OFFSET;



        $sort   = $this->input->get('sort')   ? $this->input->get('sort', TRUE)   : DEFAULT_SORT;



        $dir    = $this->input->get('dir')    ? $this->input->get('dir', TRUE)    : DEFAULT_DIR;







        // get filters



        $filters = array();







        if ($this->input->get('username'))



        {



            $filters['username'] = $this->input->get('username', TRUE);



        }







        if ($this->input->get('first_name'))



        {



            $filters['first_name'] = $this->input->get('first_name', TRUE);



        }







        if ($this->input->get('last_name'))



        {



            $filters['last_name'] = $this->input->get('last_name', TRUE);



        }



        



        if ($this->input->get('created'))



        {



            $filters['created'] = $this->input->get('created', TRUE);



        }







        $filters['profile']  = "user";



        



        // build filter string



        $filter = "";



        foreach ($filters as $key => $value)



        {



            $filter .= "&{$key}={$value}";



        }







        // save the current url to session for returning



        $this->session->set_userdata(REFERRER, THIS_URL . "?sort={$sort}&dir={$dir}&limit={$limit}&offset={$offset}{$filter}");







        // are filters being submitted?



        if ($this->input->post())



        {



            if ($this->input->post('clear'))



            {



                // reset button clicked



                redirect(THIS_URL);



            }



            else



            {



                // apply the filter(s)



                $filter = "";







                if ($this->input->post('username'))



                {



                    $filter .= "&username=" . $this->input->post('username', TRUE);



                }







                if ($this->input->post('first_name'))



                {



                    $filter .= "&first_name=" . $this->input->post('first_name', TRUE);



                }







                if ($this->input->post('last_name'))



                {



                    $filter .= "&last_name=" . $this->input->post('last_name', TRUE);



                }



                



                if ($this->input->post('created'))



                {



                    $filter .= "&created=" . $this->input->post('created', TRUE);



                }







                // redirect using new filter(s)



                redirect(THIS_URL . "?sort={$sort}&dir={$dir}&limit={$limit}&offset={$offset}{$filter}");



            }



        }







        // get list



        $users = $this->users_model->get_all($limit, $offset, $filters, $sort, $dir);



        if(isset($users['results'])){



            foreach ($users['results'] as $index => $user) {



                $subscription = $this->subscriptions->get_active($user['id']);



                $current_plan = $this->plans_model->get_plan($subscription['id_plan']);



                $users['results'][$index]['plan'] = $current_plan["{$lang}_name"];



                $users['results'][$index]['rank'] = _trader_info($user['id']);

                $users['results'][$index]['billetera'] = $this->wallet->get($user["id"], "USD");



            }



        }







        // build pagination



        $this->pagination->initialize(array(



            'base_url'   => THIS_URL . "?sort={$sort}&dir={$dir}&limit={$limit}{$filter}",



            'total_rows' => $users['total'],



            'per_page'   => $limit



        ));







        // setup page header data



        $this



            // ->add_css_theme( "datatables/jquery.dataTables.min.css" )



            // ->add_css_theme( "datatables/buttons.bootstrap.min.css" )



            // ->add_css_theme( "datatables/fixedHeader.bootstrap.min.css" )



            // ->add_css_theme( "datatables/responsive.bootstrap.min.css" )



            // ->add_css_theme( "datatables/scroller.bootstrap.min.css" )



        



            // ->add_js_theme( "datatables/jquery.dataTables.min.js" )



            // ->add_js_theme( "datatables/dataTables.bootstrap.js" )



            // ->add_js_theme( "datatables/dataTables.buttons.min.js" )



            // ->add_js_theme( "datatables/buttons.bootstrap.min.js" )



            // ->add_js_theme( "datatables/jszip.min.js" )



            // ->add_js_theme( "datatables/pdfmake.min.js" )



            // ->add_js_theme( "datatables/vfs_fonts.js" )



            // ->add_js_theme( "datatables/buttons.html5.min.js" )



            // ->add_js_theme( "datatables/buttons.print.min.js" )



            // ->add_js_theme( "datatables/dataTables.fixedHeader.min.js" )



            // ->add_js_theme( "datatables/dataTables.keyTable.min.js" )



            // ->add_js_theme( "datatables/dataTables.responsive.min.js" )



            // ->add_js_theme( "datatables/responsive.bootstrap.min.js" )



            // ->add_js_theme( "datatables/dataTables.scroller.min.js" )



            ->add_js_theme( "pages/tables.js" )



            ->add_js_theme( "pages/users.js" )







			->set_title( lang('users title user_list') );







        $data = $this->includes;



    

        // set content data



        $content_data = array(



            'this_url'   => THIS_URL,



            'users'      => $users['results'],



            'total'      => $users['total'],



            'filters'    => $filters,



            'filter'     => $filter,



            'pagination' => $this->pagination->create_links(),



            'limit'      => $limit,



            'offset'     => $offset,



            'sort'       => $sort,



            'dir'        => $dir,



            'page_title' => $data['page_title']



        );







        // load views



        $data['content'] = $this->load->view('admin/users/list_user', $content_data, TRUE);



        $this->load->view($this->template, $data);



    }











    /**



     * Add new customer



     */



    function add()



    {



        if (!is_allowed('insert')){



            $this->session->set_flashdata('error', privilegies_message());



            redirect('admin/dashboard');



        }







        // validators



        $this->form_validation->set_error_delimiters($this->config->item('error_delimeter_left'), $this->config->item('error_delimeter_right'));



        $this->form_validation->set_rules('username', lang('users input username'), 'required|trim|min_length[5]|max_length[30]|callback__check_username[]');



        $this->form_validation->set_rules('first_name', lang('users input first_name'), 'required|trim|min_length[2]|max_length[32]');



        $this->form_validation->set_rules('last_name', lang('users input last_name'), 'required|trim|min_length[2]|max_length[32]');



        $this->form_validation->set_rules('email', lang('users input email'), 'required|trim|max_length[128]|valid_email');



        $this->form_validation->set_rules('language', lang('users input language'), 'required|trim');



        $this->form_validation->set_rules('status', lang('users input status'), 'required|numeric');



        $this->form_validation->set_rules('password', lang('users input password'), 'required|trim|min_length[5]');



        $this->form_validation->set_rules('password_repeat', lang('users input password_repeat'), 'required|trim|matches[password]');



        $this->form_validation->set_rules('plan', lang('users input plan'), 'required|numeric|greater_than[0]');



        



        if ($this->form_validation->run() == TRUE)



        {   



            $data = $this->input->post();



            



            //Upload image



            $data['image'] = '';



            if ( ! $this->upload->do_upload('image'))



            {



                if($_FILES['image']['error'] != 4)



                {



                    $this->session->set_flashdata('error', $this->upload->display_errors());



                    redirect($this->_redirect_url);



                }



            }



            else



            {



                $file_data = array('upload_data' => $this->upload->data());



                $data['image'] = $file_data['upload_data']['file_name'];



            }



                



            // save the new user



            $saved = $this->users_model->add_user($data);







            if ($saved)



            {







                // Create a subscription



                $data = $this->input->post();



                $data['id_user'] = $saved;



                $this->subscriptions->add($data);



                $this->subscriptions->activate($data['id_user']);



                $this->session->set_flashdata('message', sprintf(lang('users msg add_user_success'), $this->input->post('first_name') . " " . $this->input->post('last_name')));



            }



            else



            {



                $this->session->set_flashdata('error', sprintf(lang('users error add_user_failed'), $this->input->post('first_name') . " " . $this->input->post('last_name')));



            }







            // return to list and display message



            redirect($this->_redirect_url);



        }







        // setup page header data







        $this



            ->add_css_theme("select/select2.min.css")







            ->add_js_theme( "parsley/parsley.min.js" )



            ->add_js_theme( "pages/parsley-validator.js" )



            ->add_js_theme( "select/select2.full.js" )



            ->add_js_theme( "pages/register.js" )



            ->add_js_theme( "pages/select2.js" )







            ->set_title( lang('users title customer_add') );







        $data = $this->includes;







        // set content data



        $content_data = array(



            'cancel_url'        => $this->_redirect_url,



            'user'              => NULL,



            'password_required' => TRUE,



            'plans'             => $this->plans_model->get_active(),



            'countries'         => $this->locations->getCountries(true),



            'page_title'        => $data['page_title'],



            'only_add'          => true



        );







        // load views



        $data['content'] = $this->load->view('admin/users/users_form', $content_data, TRUE);



        $this->load->view($this->template, $data);



    }











    /**



     * Edit existing user



     *



     * @param  int $id



     */



    function edit($id = NULL)



    {



        if (!is_allowed('update')){



            $this->session->set_flashdata('error', privilegies_message());



            redirect('admin/dashboard');



        }







        // make sure we have a numeric id



        if (is_null($id) OR ! is_numeric($id))



        {



            redirect($this->_redirect_url);



        }







        // get the data



        $user = $this->users_model->get_user($id);







        // if empty results, return to list



        if ( ! $user)



        {



            redirect($this->_redirect_url);



        }







        // validators



        $this->form_validation->set_error_delimiters($this->config->item('error_delimeter_left'), $this->config->item('error_delimeter_right'));



        $this->form_validation->set_rules('username', lang('users input username'), 'required|trim|min_length[5]|max_length[30]|callback__check_username[' . $user['username'] . ']');



        $this->form_validation->set_rules('first_name', lang('users input first_name'), 'required|trim|min_length[2]|max_length[32]');



        $this->form_validation->set_rules('last_name', lang('users input last_name'), 'required|trim|min_length[2]|max_length[32]');



        $this->form_validation->set_rules('email', lang('users input email'), 'required|trim|max_length[128]|valid_email');



        $this->form_validation->set_rules('birthdate', lang('users input birthdate'), 'required');



        $this->form_validation->set_rules('phone', lang('users input phone'), 'required|min_length[5]');



        $this->form_validation->set_rules('address', lang('users input address'), 'required|min_length[5]');



        $this->form_validation->set_rules('address2', lang('users input address2'), 'min_length[5]');



        $this->form_validation->set_rules('city', lang('users input city'), 'required');



        $this->form_validation->set_rules('department', lang('users input department'), 'required');



        $this->form_validation->set_ruleS('postal_code', lang('users input postal_code'), 'required|numeric');



        $this->form_validation->set_rules('language', lang('users input language'), 'required|trim');



        $this->form_validation->set_rules('status', lang('users input status'), 'required|numeric');



        $this->form_validation->set_rules('password', lang('users input password'), 'min_length[5]|matches[password_repeat]');



        $this->form_validation->set_rules('password_repeat', lang('users input password_repeat'), 'matches[password]');







        if ($this->form_validation->run() == TRUE)



        {



            $data = $this->input->post();



            



            //Upload image



            $data['image'] = '';



            if ( ! $this->upload->do_upload('image'))



            {



                if($_FILES['image']['error'] != 4)



                {



                    $this->session->set_flashdata('error', $this->upload->display_errors());



                    redirect($this->_redirect_url);



                }



            }



            else



            {



                $file_data = array('upload_data' => $this->upload->data());



                $data['image'] = $file_data['upload_data']['file_name'];



                $data['image_to_delete'] = $user['image'];



            }







            



            // save the changes



            $saved = $this->users_model->edit_user($data);







            if ($saved)



            {



                $this->session->set_flashdata('message', sprintf(lang('users msg edit_user_success'), $this->input->post('first_name') . " " . $this->input->post('last_name')));



            }



            else



            {



                $this->session->set_flashdata('error', sprintf(lang('users error edit_user_failed'), $this->input->post('first_name') . " " . $this->input->post('last_name')));



            }







            // return to list and display message



            redirect($this->_redirect_url);



        }







        // setup page header data



        $this



            ->add_css_theme("select/select2.min.css")







            ->add_js_theme( "parsley/parsley.min.js" )



            ->add_js_theme( "pages/parsley-validator.js" )



            ->add_js_theme( "select/select2.full.js" )



            ->add_js_theme( "pages/register.js" )



            ->add_js_theme( "pages/select2.js" )







            ->set_title( lang('users title user_edit') );







        $data = $this->includes;







        // set content data



        $content_data = array(



            'cancel_url'        => $this->_redirect_url,



            'user'              => $user,



            'user_id'           => $id,



            'password_required' => FALSE,



            'plans'             => $this->plans_model->get_active(),



            'page_title'        => $data['page_title'],



            'countries'         => $this->locations->getCountries(true),



        );







        // load views



        $data['content'] = $this->load->view('admin/users/users_form', $content_data, TRUE);



        $this->load->view($this->template, $data);



    }











    /**



     * Delete a user



     *



     * @param  int $id



     */



    function delete($id = NULL)



    {



        if (!is_allowed('delete')){



            $this->session->set_flashdata('error', privilegies_message());



            redirect('admin/dashboard');



        }







        // make sure we have a numeric id



        if ( ! is_null($id) OR ! is_numeric($id))



        {



            // get user details



            $user = $this->users_model->get_user($id);







            if ($user)



            {



                // soft-delete the user



                $delete = $this->users_model->delete_user($id);







                if ($delete)



                {



                    $this->session->set_flashdata('message', sprintf(lang('users msg delete_user'), $user['first_name'] . " " . $user['last_name']));



                }



                else



                {



                    $this->session->set_flashdata('error', sprintf(lang('users error delete_user'), $user['first_name'] . " " . $user['last_name']));



                }



            }



            else



            {



                $this->session->set_flashdata('error', lang('users error user_not_exist'));



            }



        }



        else



        {



            $this->session->set_flashdata('error', lang('users error user_id_required'));



        }







        // return to list and display message



        redirect($this->_redirect_url);



    }











    /**



     * Export list to CSV



     */



    function export()



    {



        // get parameters



        $sort = $this->input->get('sort') ? $this->input->get('sort', TRUE) : DEFAULT_SORT;



        $dir  = $this->input->get('dir')  ? $this->input->get('dir', TRUE)  : DEFAULT_DIR;







        // get filters



        $filters = array();







        if ($this->input->get('username'))



        {



            $filters['username'] = $this->input->get('username', TRUE);



        }







        if ($this->input->get('first_name'))



        {



            $filters['first_name'] = $this->input->get('first_name', TRUE);



        }







        if ($this->input->get('last_name'))



        {



            $filters['last_name'] = $this->input->get('last_name', TRUE);



        }







        // get all users



        $users = $this->users_model->get_all(0, 0, $filters, $sort, $dir);







        if ($users['total'] > 0)



        {



            // manipulate the output array



            foreach ($users['results'] as $key=>$user)



            {


                $users['results'][$key]['saldo']=$this->wallet->get($user["id"], "USD");
                unset($users['results'][$key]['password']);



                unset($users['results'][$key]['deleted']);







                if ($user['status'] == 0)



                {



                    $users['results'][$key]['status'] = lang('admin input inactive');



                }



                else



                {



                    $users['results'][$key]['status'] = lang('admin input active');



                }



            }







            // export the file



            array_to_csv($users['results'], "users");



        }



        else



        {



            // nothing to export



            $this->session->set_flashdata('error', lang('core error no_results'));



            redirect($this->_redirect_url);



        }







        exit;



    }











    /**************************************************************************************



     * PRIVATE VALIDATION CALLBACK FUNCTIONS



     **************************************************************************************/











    /**



     * Make sure username is available



     *



     * @param  string $username



     * @param  string|null $current



     * @return int|boolean



     */



    function _check_username($username, $current)



    {



        if (trim($username) != trim($current) && $this->users_model->username_exists($username))



        {



            $this->form_validation->set_message('_check_username', sprintf(lang('users error username_exists'), $username));



            return FALSE;



        }



        else



        {



            return $username;



        }



    }



    



    /**



     * Make sure email is available



     *



     * @param  string $email



     * @param  string|null $current



     * @return int|boolean



     */



    function _check_email($email, $current)



    {



        if (trim($email) != trim($current) && $this->users_model->email_exists($email))



        {



            $this->form_validation->set_message('_check_email', sprintf(lang('users error email_exists'), $email));



            return FALSE;



        }



        else



        {



            return $email;



        }



    }



    



    function users_by_capital()



    {



        $lang = (isset($user['language']) ? $user['language'] : $this->config->item('language'));







        // get parameters



        $limit  = $this->input->get('limit')  ? $this->input->get('limit', TRUE)  : 50;



        $offset = $this->input->get('offset') ? $this->input->get('offset', TRUE) : DEFAULT_OFFSET;



        $sort   = $this->input->get('sort')   ? $this->input->get('sort', TRUE)   : 'profit';



        $dir    = $this->input->get('dir')    ? $this->input->get('dir', TRUE)    : DEFAULT_DIR;



        



        $users = $this->users_model->users_by_capital($limit, $offset, $sort, $dir);



              



        if(isset($users['results'])){



            foreach ($users['results'] as $index => $user) { 



                



                $user_wallets = $this->wallets->get($user['id']);



                



                $users['results'][$index]['total'] =round($user_wallets['BTC'],2);



                $users['results'][$index]['total'] +=round($this->currency->load('XMR')->usd($user_wallets['XMR']),2);



                $users['results'][$index]['total'] +=round($this->currency->load('ETH')->usd($user_wallets['ETH']),2);



                $users['results'][$index]['total'] +=round($this->currency->load('LTC')->usd($user_wallets['LTC']),2);



                $users['results'][$index]['total'] +=round($this->currency->load('TRUMP')->usd($user_wallets['TRUMP']),2);



                $users['results'][$index]['total'] +=round($this->currency->load('DASH')->usd($user_wallets['DASH']),2);



                $users['results'][$index]['total'] +=round($this->currency->load('SLR')->usd($user_wallets['SLR']),2);



                $users['results'][$index]['total'] +=round($user_wallets['USD'],2) ;



                



                $users['results'][$index]['BTC']= round($user_wallets['BTC'],2);



                $users['results'][$index]['XMR']= round($this->currency->load('XMR')->usd($user_wallets['XMR']),2);



                $users['results'][$index]['ETH']= round($this->currency->load('ETH')->usd($user_wallets['ETH']),2);



                $users['results'][$index]['LTC']= round($this->currency->load('LTC')->usd($user_wallets['LTC']),2);



                $users['results'][$index]['TRUMP']= round($this->currency->load('TRUMP')->usd($user_wallets['TRUMP']),2);



                $users['results'][$index]['DASH']= round($this->currency->load('DASH')->usd($user_wallets['DASH']),2);



                $users['results'][$index]['SLR']= round($this->currency->load('SLR')->usd($user_wallets['SLR']),2);



                $users['results'][$index]['USD']= round($user_wallets['USD'],2) ;



            }



        }



                



        // build pagination



        $this->pagination->initialize(array(



            'base_url'   => THIS_URL_CAPITAL . "?sort={$sort}&dir={$dir}&limit={$limit}",



            'total_rows' => $users['total'],



            'per_page'   => $limit



        ));







        // setup page header data



        $this        



            ->add_js_theme( "pages/tables.js" )



            ->add_js_theme( "pages/users.js" )



            // ->add_js_theme( "datatables/jquery.dataTables.min.js" )







			->set_title( lang('users title user_stats') );







        $data = $this->includes;







        // set content data



        $content_data = array(



            'this_url'   => THIS_URL_CAPITAL,



            'users'      => $users['results'],



            'total'      => $users['total'],



            'pagination' => $this->pagination->create_links(),



            'limit'      => $limit,



            'offset'     => $offset,



            'sort'       => $sort,



            'dir'        => $dir,



            'page_title' => $data['page_title']



        );







        // load views



        $data['content'] = $this->load->view('admin/users/users_by_capital', $content_data, TRUE);



        $this->load->view($this->template, $data);



    }







    function activate_referral($id) {



        $this->referral->activate($id);



    }



}



