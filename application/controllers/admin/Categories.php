<?php defined('BASEPATH') OR exit('No direct script access allowed');

class Categories extends Admin_Controller {

    /**
     * @var string
     */
    private $_redirect_url;


    /**
     * Constructor
     */
    function __construct()
    {
        parent::__construct();

        // load the language files
        $this->lang->load('users');

        // load the model
        $this->load->model('categories_model');
       

        // set constants
        define('REFERRER', "referrer");
        define('THIS_URL', base_url('admin/categories'));
        define('DEFAULT_LIMIT', $this->settings->per_page_limit);
        define('DEFAULT_OFFSET', 0);
        define('DEFAULT_SORT', "category_id");
        define('DEFAULT_DIR', "DESC");

        // use the url in session (if available) to return to the previous filter/sorted/paginated list
        if ($this->session->userdata(REFERRER))
        {
            $this->_redirect_url = $this->session->userdata(REFERRER);
        }
        else
        {
            $this->_redirect_url = THIS_URL;
        }
    }


    /**************************************************************************************
     * PUBLIC FUNCTIONS
     **************************************************************************************/


    /**
     * Categories list page
     */
    function index()
    {
        
        // get parameters
        $limit  = $this->input->get('limit')  ? $this->input->get('limit', TRUE)  : DEFAULT_LIMIT;
        $offset = $this->input->get('offset') ? $this->input->get('offset', TRUE) : DEFAULT_OFFSET;
        $sort   = $this->input->get('sort')   ? $this->input->get('sort', TRUE)   : DEFAULT_SORT;
        $dir    = $this->input->get('dir')    ? $this->input->get('dir', TRUE)    : DEFAULT_DIR;
        
        // get filters
        $filters = array();

        if ($this->input->get('name'))
        {
            $filters['name'] = $this->input->get('name', TRUE);
        }

        if ($this->input->get('description'))
        {
            $filters['description'] = $this->input->get('description', TRUE);
        }

        // build filter string
        $filter = "";
        foreach ($filters as $key => $value)
        {
            $filter .= "&{$key}={$value}";
        }

        // save the current url to session for returning
        $this->session->set_userdata(REFERRER, THIS_URL . "?sort={$sort}&dir={$dir}&limit={$limit}&offset={$offset}{$filter}");

        // are filters being submitted?
        if ($this->input->post())
        {
            if ($this->input->post('clear'))
            {
                // reset button clicked
                redirect(THIS_URL);
            }
            else
            {
                // apply the filter(s)
                $filter = "";

                if ($this->input->post('name'))
                {
                    $filter .= "&name=" . $this->input->post('name', TRUE);
                }

                if ($this->input->post('description'))
                {
                    $filter .= "&description=" . $this->input->post('description', TRUE);
                }

                // redirect using new filter(s)
                redirect(THIS_URL . "?sort={$sort}&dir={$dir}&limit={$limit}&offset={$offset}{$filter}");
            }
        }

        // get list
        $categories = $this->categories_model->get_all($limit, $offset, $filters, $sort, $dir);
   
        // build pagination
        $this->pagination->initialize(array(
            'base_url'   => THIS_URL . "?sort={$sort}&dir={$dir}&limit={$limit}{$filter}",
            'total_rows' => $categories['total'],
            'per_page'   => $limit, 'attributes'=>["class"=>"page-link"]
        ));

        // setup page header data
		$this
            // ->add_css_theme( "datatables/jquery.dataTables.min.css" )
            // ->add_css_theme( "datatables/buttons.bootstrap.min.css" )
            // ->add_css_theme( "datatables/fixedHeader.bootstrap.min.css" )
            // ->add_css_theme( "datatables/responsive.bootstrap.min.css" )
            // ->add_css_theme( "datatables/scroller.bootstrap.min.css" )
        
            // ->add_js_theme( "datatables/jquery.dataTables.min.js" )
            // ->add_js_theme( "datatables/dataTables.bootstrap.js" )
            // ->add_js_theme( "datatables/dataTables.buttons.min.js" )
            // ->add_js_theme( "datatables/buttons.bootstrap.min.js" )
            // ->add_js_theme( "datatables/jszip.min.js" )
            // ->add_js_theme( "datatables/pdfmake.min.js" )
            // ->add_js_theme( "datatables/vfs_fonts.js" )
            // ->add_js_theme( "datatables/buttons.html5.min.js" )
            // ->add_js_theme( "datatables/buttons.print.min.js" )
            // ->add_js_theme( "datatables/dataTables.fixedHeader.min.js" )
            // ->add_js_theme( "datatables/dataTables.keyTable.min.js" )
            // ->add_js_theme( "datatables/dataTables.responsive.min.js" )
            // ->add_js_theme( "datatables/responsive.bootstrap.min.js" )
            // ->add_js_theme( "datatables/dataTables.scroller.min.js" )
            // ->add_js_theme( "pages/tables.js" )

			->set_title( lang('users title categories_list') );

        $data = $this->includes;

        // set content data
        $content_data = array(
            'this_url'   => THIS_URL,
            'categories' => $categories['results'],
            'total'      => $categories['total'],
            'filters'    => $filters,
            'filter'     => $filter,
            'pagination' => $this->pagination->create_links(),
            'limit'      => $limit,
            'offset'     => $offset,
            'sort'       => $sort,
            'dir'        => $dir,
            'page_title' => $data['page_title']
        );
    
        // load views
        $data['content'] = $this->load->view('admin/shopping/list_categories', $content_data, TRUE);
       
        $this->load->view($this->template, $data);
    }


    /**
     * Add new category
     */
    function add()
    {
      
        // validators
        $this->form_validation->set_error_delimiters($this->config->item('error_delimeter_left'), $this->config->item('error_delimeter_right'));
        $this->form_validation->set_rules('name', lang('users input name'), 'required|min_length[5]|max_length[30]|callback__category_exists');
        $this->form_validation->set_rules('description', lang('users input description'), 'required|min_length[8]|max_length[32]');

        if ($this->form_validation->run() == TRUE)
        {
            $data['category'] = $this->input->post();
            $data['id_user']  = $this->user['id'];

            // save the new category
            $saved = $this->categories_model->add_category($data);

            if ($saved)
            {
                $this->session->set_flashdata('message', sprintf(lang('users msg add_category_success'), $this->input->post('name')));
            }
            else
            {
                $this->session->set_flashdata('error', sprintf(lang('users error add_category_failed'), $this->input->post('name')));
            }

            // return to list and display message
            redirect($this->_redirect_url);
        }

        // setup page header data
        $this
            ->add_js_theme( "parsley/parsley.min.js" )
            ->add_js_theme( "pages/parsley-validator.js" )
            ->add_js_theme( "pages/register.js" )

            ->set_title( lang('users title category_add') );

        $data = $this->includes;

        // set content data
        $content_data = array(
            'cancel_url'        => $this->_redirect_url,
            'category'          => NULL,
            'page_title'        => $data['page_title']
        );

        $data['content'] = $this->load->view('admin/shopping/categories_form', $content_data, TRUE);
        $this->load->view($this->template, $data);
    }



    /**
     * Edit existing category
     *
     * @param  int $id
     */
    function edit($id = NULL)
    {
        // make sure we have a numeric id
        if (is_null($id) OR ! is_numeric($id))
        {
            redirect($this->_redirect_url);
        }

        // get the data
        $category = $this->categories_model->get_category($id);

        // if empty results, return to list
        if ( ! $category)
        {
            redirect($this->_redirect_url);
        }

        // validators
        $this->form_validation->set_error_delimiters($this->config->item('error_delimeter_left'), $this->config->item('error_delimeter_right'));
        $this->form_validation->set_rules('name', lang('users input name'), 'required|min_length[5]|max_length[30]');
        $this->form_validation->set_rules('description', lang('users input description'), 'required|min_length[8]|max_length[32]');

        if ($this->form_validation->run() == TRUE)
        {
            $data['category'] = $this->input->post();
            $data['id_user']  = $this->user['id'];
            // save the changes
            $saved = $this->categories_model->edit_category($data);

            if ($saved)
            {
                $this->session->set_flashdata('message', sprintf(lang('users msg edit_category_success'), $this->input->post('name') ));
            }
            else
            {
                $this->session->set_flashdata('error', sprintf(lang('users error edit_category_failed'), $this->input->post('name')));
            }

            // return to list and display message
            redirect($this->_redirect_url);
        }

        // setup page header data
        $this
            ->add_js_theme( "parsley/parsley.min.js" )
            ->add_js_theme( "pages/parsley-validator.js" )
            ->add_js_theme( "pages/register.js" )

            ->set_title( lang('users title category_edit') );

        $data = $this->includes;

        // set content data
        $content_data = array(
            'cancel_url'        => $this->_redirect_url,
            'category'          => $category,
            'category_id'       => $id,
            'page_title'        => $data['page_title']
        );

        // load views
        $data['content'] = $this->load->view('admin/shopping/categories_form', $content_data, TRUE);
        $this->load->view($this->template, $data);
    }

    /**
     * Export list to CSV
     */
    function export()
    {
        // get parameters
        $sort = $this->input->get('sort') ? $this->input->get('sort', TRUE) : DEFAULT_SORT;
        $dir  = $this->input->get('dir')  ? $this->input->get('dir', TRUE)  : DEFAULT_DIR;

        // get filters
        $filters = array();

        if ($this->input->get('name'))
        {
            $filters['name'] = $this->input->get('name', TRUE);
        }

        if ($this->input->get('description'))
        {
            $filters['description'] = $this->input->get('description', TRUE);
        }

        // get all categories
        $categories = $this->categories_model->get_all(0, 0, $filters, $sort, $dir);

        if ($categories['total'] > 0)
        {
            // manipulate the output array
            foreach ($categories['results'] as $key=>$category)
            {
                unset($categories['results'][$key]['deleted']);
            }
            // export the file
            array_to_csv($categories['results'], "categories");
        }
        else
        {
            // nothing to export
            $this->session->set_flashdata('error', lang('core error no_results'));
            redirect($this->_redirect_url);
        }

        exit;
    }

    /**
     * Metodo para no repetir categorias
     */
     function _category_exists($name)
     {
         // get the data
        $category = $this->categories_model->get_category_name($name);    

        if (empty($category)) {
            return $name;
        } else {
            $this->form_validation->set_message('_category_exists', sprintf(lang('users error category_exists')));
            return FALSE;
        }

     }

    
}