<?php defined('BASEPATH') OR exit('No direct script access allowed');

class Products extends Admin_Controller {

    /**
     * @var string
     */
    private $_redirect_url;


    /**
     * Constructor
     */
    function __construct()
    {
        parent::__construct();

        // load the language files
        $this->lang->load('users');

        // load the models
        $this->load->model('categories_model');
        $this->load->model('products_model');

        // upload config

        $config['upload_path']          = "uploads/products";
        $config['allowed_types']        = 'gif|jpg|png';
        $config['file_name']            = strtotime("now");
        $config['max_size']             = 0;
        $config['max_width']            = 1024;
        $config['max_height']           = 1024;

        $this->load->library('upload', $config);
       

        // set constants
        define('REFERRER', "referrer");
        define('THIS_URL', base_url('admin/products'));
        define('DEFAULT_LIMIT', $this->settings->per_page_limit);
        define('DEFAULT_OFFSET', 0);
        define('DEFAULT_SORT', "product_id");
        define('DEFAULT_DIR', "DESC");

        // use the url in session (if available) to return to the previous filter/sorted/paginated list
        if ($this->session->userdata(REFERRER))
        {
            $this->_redirect_url = $this->session->userdata(REFERRER);
        }
        else
        {
            $this->_redirect_url = THIS_URL;
        }
    }


    /**************************************************************************************
     * PUBLIC FUNCTIONS
     **************************************************************************************/


    /**
     * Products list page
     */
    function index()
    {
        
        // get parameters
        $limit  = $this->input->get('limit')  ? $this->input->get('limit', TRUE)  : DEFAULT_LIMIT;
        $offset = $this->input->get('offset') ? $this->input->get('offset', TRUE) : DEFAULT_OFFSET;
        $sort   = $this->input->get('sort')   ? $this->input->get('sort', TRUE)   : DEFAULT_SORT;
        $dir    = $this->input->get('dir')    ? $this->input->get('dir', TRUE)    : DEFAULT_DIR;
        
        // get filters
        $filters = array();

        if ($this->input->get('code'))
        {
            $filters['code'] = $this->input->get('code', TRUE);
        }

        if ($this->input->get('product_name'))
        {
            $filters['product_name'] = $this->input->get('product_name', TRUE);
        }

        if ($this->input->get('product_price'))
        {
            $filters['product_price'] = $this->input->get('product_price', TRUE);
        }

        if ($this->input->get('product_price_distributor'))
        {
            $filters['product_price_distributor'] = $this->input->get('product_price_distributor', TRUE);
        }

        // build filter string
        $filter = "";
        foreach ($filters as $key => $value)
        {
            $filter .= "&{$key}={$value}";
        }

        // save the current url to session for returning
        $this->session->set_userdata(REFERRER, THIS_URL . "?sort={$sort}&dir={$dir}&limit={$limit}&offset={$offset}{$filter}");

        // are filters being submitted?
        if ($this->input->post())
        {
            if ($this->input->post('clear'))
            {
                // reset button clicked
                redirect(THIS_URL);
            }
            else
            {
                // apply the filter(s)
                $filter = "";

                if ($this->input->post('code'))
                {
                    $filter .= "&code=" . $this->input->post('code', TRUE);
                }

                if ($this->input->post('product_name'))
                {
                    $filter .= "&product_name=" . $this->input->post('product_name', TRUE);
                }

                if ($this->input->post('product_price'))
                {
                    $filter .= "&product_price=" . $this->input->post('product_price', TRUE);
                }

                if ($this->input->post('product_price_distributor'))
                {
                    $filter .= "&product_price_distributor=" . $this->input->post('product_price_distributor', TRUE);
                }

                // redirect using new filter(s)
                redirect(THIS_URL . "?sort={$sort}&dir={$dir}&limit={$limit}&offset={$offset}{$filter}");
            }
        }

        // get list
        $products = $this->products_model->get_all($limit, $offset, $filters, $sort, $dir);

        // build pagination
        $this->pagination->initialize(array(
            'base_url'   => THIS_URL . "?sort={$sort}&dir={$dir}&limit={$limit}{$filter}",
            'total_rows' => $products['total'],
            'per_page'   => $limit
        ));

        // setup page header data
		$this->set_title( lang('users title product_list') );

        $data = $this->includes;

        // set content data
        $content_data = array(
            'this_url'   => THIS_URL,
            'products'   => $products['results'],
            'total'      => $products['total'],
            'filters'    => $filters,
            'filter'     => $filter,
            'pagination' => $this->pagination->create_links(),
            'limit'      => $limit,
            'offset'     => $offset,
            'sort'       => $sort,
            'dir'        => $dir,
            'page_title' => $data['page_title']
        );
    
        // load views
        $data['content'] = $this->load->view('admin/shopping/list_products', $content_data, TRUE);
       
        $this->load->view($this->template, $data);
    }

    /**
     * Export list to CSV
     */
    function export()
    {
        // get parameters
        $sort = $this->input->get('sort') ? $this->input->get('sort', TRUE) : DEFAULT_SORT;
        $dir  = $this->input->get('dir')  ? $this->input->get('dir', TRUE)  : DEFAULT_DIR;

        // get filters
        $filters = array();

        if ($this->input->get('code'))
        {
            $filters['code'] = $this->input->get('code', TRUE);
        }

        if ($this->input->get('product_name'))
        {
            $filters['product_name'] = $this->input->get('product_name', TRUE);
        }

        if ($this->input->get('product_price'))
        {
            $filters['product_price'] = $this->input->get('product_price', TRUE);
        }

        if ($this->input->get('product_price_distributor'))
        {
            $filters['product_price_distributor'] = $this->input->get('product_price_distributor', TRUE);
        }

        // get all products
        $products = $this->products_model->get_all(0, 0, $filters, $sort, $dir);

        if ($products['total'] > 0)
        {
            // manipulate the output array
            foreach ($products['results'] as $key=>$product)
            {
                unset($products['results'][$key]['category_id']);
                unset($products['results'][$key]['product_image']);
                unset($products['results'][$key]['deleted']);
            }
            // export the file
            array_to_csv($products['results'], "products");
        }
        else
        {
            // nothing to export
            $this->session->set_flashdata('error', lang('core error no_results'));
            redirect($this->_redirect_url);
        }

        exit;
    }

    /**
     * Add new customer
     */
    function add()
    {

        // validators
        $this->form_validation->set_error_delimiters($this->config->item('error_delimeter_left'), $this->config->item('error_delimeter_right'));
        $this->form_validation->set_rules('code', lang('users input code'), 'required|numeric');
        $this->form_validation->set_rules('product_name', lang('users input product_name'), 'required|min_length[5]|max_length[200]|callback__product_exists');        
        $this->form_validation->set_rules('product_price', lang('users input product_price'), 'required|numeric');
        $this->form_validation->set_rules('product_price_distributor', lang('users input product_price_distributor'), 'required|numeric');
        $this->form_validation->set_rules('category_id', lang('users input category_id'), 'required');
        $this->form_validation->set_rules('residual_points', lang('users input residual_points'), 'required|numeric');
        $this->form_validation->set_rules('binary_points', lang('users input binary_points'), 'required|numeric');
        $this->form_validation->set_rules('stock_quantity', lang('users input stock_quantity'), 'required|numeric');

        
        if ($this->form_validation->run() == TRUE)
        {   
            $data = $this->input->post();
            
            //Upload image
            $data['image'] = '';
            if ( ! $this->upload->do_upload('image'))
            {
                if($_FILES['image']['error'] != 4)
                {
                    $this->session->set_flashdata('error', $this->upload->display_errors());
                    redirect($this->_redirect_url);
                }
            } else {
                $file_data = array('upload_data' => $this->upload->data());
                $data['image'] = $file_data['upload_data']['file_name'];
            }
            $data['id_user']  = $this->user['id'];
    
        
            // save the new product
            $saved = $this->products_model->add_product($data);

            if ($saved)
            {

                $this->session->set_flashdata('message', sprintf(lang('users msg add_product_success'), $this->input->post('product_name')));
            }
            else
            {
                $this->session->set_flashdata('error', sprintf(lang('users error add_product_failed'), $this->input->post('product_name')));
            }

            // return to list and display message
            redirect($this->_redirect_url);
        }

        // setup page header data

        $this
            ->add_css_theme("select/select2.min.css")

            ->add_js_theme( "parsley/parsley.min.js" )
            ->add_js_theme( "pages/parsley-validator.js" )
            ->add_js_theme( "select/select2.full.js" )
            ->add_js_theme( "pages/register.js" )
            ->add_js_theme( "pages/select2.js" )

            ->set_title( lang('users title product_add') );

        $data = $this->includes;

        // set content data
        $content_data = array(
            'cancel_url'        => $this->_redirect_url,
            'product'           => NULL,
            'categories'        => $this->categories_model->getCategories(),
            'page_title'        => $data['page_title'],
            'only_add'          => true
        );

        // load views
        $data['content'] = $this->load->view('admin/shopping/products_form', $content_data, TRUE);
        $this->load->view($this->template, $data);
    }
    /**
     * Edit existing product
     *
     * @param  int $id
     */
    function edit($id = NULL)
    {
        // make sure we have a numeric id
        if (is_null($id) OR ! is_numeric($id))
        {
            redirect($this->_redirect_url);
        }

        // get the data
        $product = $this->products_model->get_product($id);

        // if empty results, return to list
        if ( ! $product)
        {
            redirect($this->_redirect_url);
        }

        // validators
        $this->form_validation->set_error_delimiters($this->config->item('error_delimeter_left'), $this->config->item('error_delimeter_right'));
        $this->form_validation->set_rules('code', lang('users input code'), 'required|numeric');
        $this->form_validation->set_rules('product_name', lang('users input product_name'), 'required|min_length[5]|max_length[200]');
        $this->form_validation->set_rules('product_price', lang('users input product_price'), 'required|numeric');
        $this->form_validation->set_rules('product_price_distributor', lang('users input product_price_distributor'), 'required|numeric');        
        $this->form_validation->set_rules('category_id', lang('users input category_id'), 'required');
        $this->form_validation->set_rules('residual_points', lang('users input residual_points'), 'required|numeric');
        $this->form_validation->set_rules('binary_points', lang('users input binary_points'), 'required|numeric');
        $this->form_validation->set_rules('stock_quantity', lang('users input stock_quantity'), 'required|numeric');

        if ($this->form_validation->run() == TRUE)
        {
            $data = $this->input->post();
            
            //Upload image
            $data['image'] = '';
            if ( ! $this->upload->do_upload('image'))
            {
                if($_FILES['image']['error'] != 4)
                {
                    $this->session->set_flashdata('error', $this->upload->display_errors());
                    redirect($this->_redirect_url);
                }
            }
            else
            {
                $file_data = array('upload_data' => $this->upload->data());
                $data['image'] = $file_data['upload_data']['file_name'];
                $data['image_to_delete'] = $product['product_image'];
            }
            $data['id_user']  = $this->user['id'];

            // save the changes
            $saved = $this->products_model->edit_product($data);

            if ($saved)
            {

                $this->session->set_flashdata('message', sprintf(lang('users msg edit_product_success'), $this->input->post('product_name')));
            }
            else
            {
                $this->session->set_flashdata('error', sprintf(lang('users error edit_product_failed'), $this->input->post('product_name')));
            }

            // return to list and display message
            redirect($this->_redirect_url);
        }

        // setup page header data
        $this
            ->add_js_theme( "parsley/parsley.min.js" )
            ->add_js_theme( "pages/parsley-validator.js" )
            ->add_js_theme( "pages/register.js" )

            ->set_title( lang('users title product_edit') );

        $data = $this->includes;

        // set content data
        $content_data = array(
            'cancel_url'        => $this->_redirect_url,
            'product'           => $product,
            'categories'        => $this->categories_model->getCategories(),
            'product_id'        => $id,
            'page_title'        => $data['page_title']
        );

        // load views
        $data['content'] = $this->load->view('admin/shopping/products_form', $content_data, TRUE);
        $this->load->view($this->template, $data);
    }

    /**
     * Remove a product
     *
     * @param  int $id
     */
    function remove($id = NULL)
    {
        // make sure we have a numeric id
        if ( ! is_null($id) OR ! is_numeric($id))
        {
            // get user details
            $product = $this->products_model->get_product($id);

            if ($product)
            {
                // soft-delete the user
                $delete = $this->products_model->remove_product($id);

                if ($delete)
                {
                    $this->session->set_flashdata('message', sprintf(lang('users msg remove_product'), $product['product_name']));
                }
                else
                {
                    $this->session->set_flashdata('error', sprintf(lang('users error remove_product'), $product['product_name']));
                }
            }
            else
            {
                $this->session->set_flashdata('error', lang('users error product_not_exist'));
            }
        }
        else
        {
            $this->session->set_flashdata('error', lang('users error product_id_required'));
        }

        // return to list and display message
        redirect($this->_redirect_url);
    }

    /**
     * Activate a product
     *
     * @param  int $id
     */
    function activate($id = NULL)
    {
        // make sure we have a numeric id
        if ( ! is_null($id) OR ! is_numeric($id))
        {
            // get user details
            $product = $this->products_model->get_product($id);

            if ($product)
            {
                // soft-delete the user
                $delete = $this->products_model->activate_product($id);

                if ($delete)
                {
                    $this->session->set_flashdata('message', sprintf(lang('users msg activate_product'), $product['product_name']));
                }
                else
                {
                    $this->session->set_flashdata('error', sprintf(lang('users error activate_product'), $product['product_name']));
                }
            }
            else
            {
                $this->session->set_flashdata('error', lang('users error product_not_exist'));
            }
        }
        else
        {
            $this->session->set_flashdata('error', lang('users error product_id_required'));
        }

        // return to list and display message
        redirect($this->_redirect_url);
    }

    /**
     * Metodo para no repetir productos
     */
    function _product_exists($product_name)
    {
        // get the data
       $product = $this->products_model->get_product_name($product_name);    

       if (empty($product)) {
           return $product_name;
       } else {
           $this->form_validation->set_message('_product_exists', sprintf(lang('users error product_exists')));
           return FALSE;
       }

    }
}