<?php



defined('BASEPATH') OR exit('No direct script access allowed');



/**

 * All  > PUBLIC <  AJAX functions should go in here

 *

 * CSRF protection has been disabled for this controller in the config file

 *

 * IMPORTANT: DO NOT DO ANY WRITEBACKS FROM HERE!!! For retrieving data only.

 */

class Login extends Public_Controller {



    /**

     * Constructor

     */

    

    function __construct() {

        parent::__construct();

        $this->load->model('users_model');

    }



    

    public function index() {

        

        if ($this->session->userdata('logged_in')) {

            $logged_in_user = $this->session->userdata('logged_in');

            if ($logged_in_user['is_admin']) {

                redirect('admin/dashboard');

            } else {

                if ($logged_in_user['profile'] == 'user') {

                    // redirect to landing page

                    redirect('user/dashboard');

                }

                else {

                    // redirect to orders

                    redirect('orders');

                }

                

            }

        }



        // set form validation rules

        $this->form_validation->set_error_delimiters($this->config->item('error_delimeter_left'), $this->config->item('error_delimeter_right'));

        $this->form_validation->set_rules('username', lang('users input username_email'), 'required|trim|max_length[256]');

        $this->form_validation->set_rules('password', lang('users input password'), 'required|trim|max_length[72]|callback__check_login');

        

        if ($this->form_validation->run() == TRUE) {



            if ($this->session->userdata('redirect')) {

                // redirect to desired page

                $redirect = $this->session->userdata('redirect');

                $this->session->unset_userdata('redirect');

                redirect($redirect);

            } else {

                $logged_in_user = $this->session->userdata('logged_in');

                if ($logged_in_user['is_admin']) {

                    // redirect to admin dashboard

                    redirect('admin/dashboard');

                } else {

                    if ($logged_in_user['profile'] == 'user') {

                        // redirect to landing page

                        redirect('user/dashboard');

                    }

                    else {

                        // redirect to orders

                        redirect('orders');

                    }

                    

                }

            }

        }



        // setup page header data

        $this->set_title(lang('users title login'));



        $this

                ->add_js_theme("parsley/parsley.min.js")

                ->add_js_theme("pages/parsley-validator.js")

                ->add_external_js("https://www.google.com/recaptcha/api.js");



        $data = $this->includes;



        // load views

        $data['content'] = $this->load->view('login', $data, TRUE);

        $this->load->view($this->template, $data);

    }

    

    

    /**************************************************************************************

     * PRIVATE VALIDATION CALLBACK FUNCTIONS

     **************************************************************************************/





    /**

     * Verify the login credentials

     *

     * @param  string $password

     * @return boolean

     */

    function _check_login($password)

    {



        //CAPTCHA

        $captcha= $this->input->post('g-recaptcha-response');

        $response = file_get_contents("https://www.google.com/recaptcha/api/siteverify?secret=6Lcvaa0UAAAAACYYjMVYa1XgVVxhoUnAuD0nZhb6&response=".$captcha."&remoteip=".$_SERVER['REMOTE_ADDR']);

        $response = json_decode($response, true);



        // Descomentar la siguiente linea para desactivar el recaptcha

        //$response["success"] = true;

        

        

        if($response["success"] === true ){

            // limit number of login attempts

            $ok_to_login = $this->users_model->login_attempts();



            if ($ok_to_login)

            {

                $login = $this->users_model->login($this->input->post('username', TRUE), $password);



                if ($login)

                {

                    $subscription = $this->users_model->check_susbcription_completed($login['id']);

                    

                    if(!$subscription){

                        $this->session->set_userdata('logged_in', $login);

                        return TRUE;

                    }

                    

                    $this->session->set_flashdata('error', lang('error required_renewal'));

                    redirect('renew/'. $login['id']);

                    

                }



                $this->form_validation->set_message('_check_login', lang('users error invalid_login'));

                return FALSE;

            }



            $this->form_validation->set_message('_check_login', sprintf(lang('users error too_many_login_attempts'), $this->config->item('login_max_time')));

            return FALSE;

        }else{

            $this->session->set_flashdata('error', lang('captcha error'));

            redirect('login');

        }

    }

    

    

}
