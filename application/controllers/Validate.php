<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of Validate
 *
 * @author Juan Manuel
 */
class Validate extends Public_Controller{
    
    function __construct() {
        parent::__construct();
        $this->load->model('users_model','users');
        $this->load->model('subscriptions_model','subscriptions');
    }
    
    /**
     * Validate new account
     */
    function index()
    {
        
        if ($this->session->userdata('logged_in'))
        {
            redirect(base_url('login'));
        }
        
        // get codes
        $encrypted_email = filter_var($this->input->get('e'),FILTER_SANITIZE_ENCODED);
        $validation_code = filter_var($this->input->get('c'),FILTER_SANITIZE_ENCODED);

        // validate account
        //$validated = $this->users->validate_account($encrypted_email, $validation_code);

        if ($validated)
        {
            // Subscription activation
//            $user_id = $this->users->sha1_search($encrypted_email);
//            $this->subscriptions->activate($user_id);
            
            $this->session->set_flashdata('message', lang('users msg validate_success'));
        }
        else
        {
            $this->session->set_flashdata('error', lang('users error validate_failed'));
        }

        redirect(base_url('login'));
    }
}
