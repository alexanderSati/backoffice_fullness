<?php

defined('BASEPATH') OR exit('No direct script access allowed');


/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of Rates
 *
 * @author Juan Manuel
 */
class Ranks extends MY_Controller {

    //put your code here
    function __construct() {
        parent::__construct();

        $this->lang->load('settings');
        $this->load->library('transaction');
        $this->load->library('rank');
        $this->load->model('users_model', 'users');

        // Create an array with allowed hours for cron job
        $current_hour = date("G");

        $current_day = date('d');
        $last_month_day = date('t');

        $day = ($current_day == $last_month_day) ? TRUE : FALSE;
        $hour = ($current_hour > 20) ? TRUE : FALSE;

        if (!$day || !$hour) {
            echo "Access denied";
            exit();
        }
    }

    function index() {
        try {
            // Get allowed users to updated
            $users = $this->rank->get_allowed();
            if ($users) {
                array_walk($users, array($this, '_rank_status'), $users);
            }
        } catch (Exception $e) {
            $this->_alert($e->getMessage());
        }
    }

    function _rank_status($user) {
        if (is_active($user['id_user'])) {
            $data_u = $this->users->get_user($user['id_user']);
            $updated = _get_calculate_rank($user['id_user']);
            if (($user['rank'] < $updated) && ($data_u['status'] == 1) && ($updated > 0)) {
                $user['rank'] = $updated;
            }
        }
        // Update rank data
        $user['last_updated'] = date("Y-m-d H:i:s");
        $this->rank->update($user['id_user'], $user);
    }

    function loadDB() {

        // $users = $this->users->get_all();
        // foreach($users['results'] as $user){
        //     $this->ranks->insert($user['id']);
        // }
    }

    private function _alert($alert = "") {
        $this->load->library('email');
        $this->email->from("{$this->settings->site_email}");
        $this->email->to("{$this->settings->site_alerts_email}", TRUE);
        $this->email->subject("Referral bonus cron job");
        $this->email->message("Cron job could create recents users file " . date("Y-m-d H:i:s") . "{$alert}");
        $this->email->set_mailtype("html");
        $this->email->send();
        return false;
    }

}