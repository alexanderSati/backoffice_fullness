<?php

defined('BASEPATH') OR exit('No direct script access allowed');


/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of Rates
 *
 * @author Juan Manuel
 */
class Rates extends MY_Controller {

    //put your code here
    function __construct() {
        parent::__construct();

        $this->lang->load('settings');
        $this->load->model("currencies_model", "currencies");

        $this->api = new stdClass();
        $this->api->url = "https://api.coinmarketcap.com/v1/ticker";

        if (!$this->input->is_cli_request()) {
            echo "Access denied";
            exit();
        }
    }

    function index() {
        try {

            // Get loaded coins in database
            $currencies = $this->currencies->get_ids();

            // Set data values from API values
            foreach ($currencies as $id) {

                $rate = $this->_getRate($id);
                if ($rate) {
                    $data = array(
                        'price_usd' => $rate[0]['price_usd'],
                        'price_btc' => $rate[0]['price_btc'],
                        'last_updated' => date("Y-m-d H:i:s"),
                    );
                    // Call update method in model to update currency
                    $this->currencies->update($id, $data);
                }
            }
        } catch (Exception $e) {
            $this->_alert($e->getMessage());
        }
    }

    private function _getRate($id = null) {
        if ($id) {
            $data = @file_get_contents("{$this->api->url}/{$id}");
            return ($data !== FALSE) ? json_decode($data, true) : $this->_alert();
        }
    }

    private function _alert($alert = "") {
        $this->load->library('email');
        $this->email->from("{$this->settings->site_email}");
        $this->email->to("{$this->settings->site_alerts_email}", TRUE);
        $this->email->subject("Exchange rate API failure");
        $this->email->message("Cron job could reach api exchange rates at " . date("Y-m-d H:i:s") . "{$alert}");
        $this->email->set_mailtype("html");
        $this->email->send();
        return false;
    }

}
