<?php defined('BASEPATH') OR exit('No direct script access allowed');


/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of Rates
 *
 * @author Juan Manuel
 */
class Transactions extends MY_Controller{
    //put your code here
    function __construct() {
        parent::__construct();
        
        // Create an array with allowed hours for cron job
        $current_hour = date("G");
        
        if($current_hour != 23){
            echo "Access denied";
            exit();
        }
        
        $this->lang->load('settings');
        $this->load->library('transaction');
        
    }
    
    function index(){
        try {
            $this->transaction->referral_bonus();
        } catch (Exception $e) {
            $this->_alert($e->getMessage());
        }
    }
    
    private function _alert($alert = ""){
        $this->load->library('email');
        $this->email->from("{$this->settings->site_email}");
        $this->email->to("{$this->settings->site_alerts_email}", TRUE);
        $this->email->subject("Referral bonus cron job");
        $this->email->message("Cron job could update pending transactions ". date("Y-m-d H:i:s")."{$alert}");
        $this->email->set_mailtype("html");
        $this->email->send();
        return false;
    }
    

    
}
