<?php

defined('BASEPATH') OR exit('No direct script access allowed');


/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of Rates
 *
 * @author Juan Manuel
 */
class Binary extends MY_Controller {

    const BONUS = [
        9 => 0,
        5 => 0,
        6 => 10,
        7 => 15,
        8 => 30
    ];

    //put your code here
    function __construct() {
        parent::__construct();

        $current_hour = date("G");
        /*if ($current_hour != 23) {
            echo "Access denied";
            exit();
        }*/

        $this->lang->load('settings');
        $this->load->library('binarytree');
        $this->load->library('referral');
        $this->load->library('rank');
        $this->load->model('subscriptions_model', 'subscription');
        $this->load->library('traceability');
    }

    function index() {

        $records = array();

        try {
            // Get the list of user to update
            $users = $this->binarytree->bonus_list();
            
            foreach ($users as $user) {
                if (is_active($user['id_user'])) {
                    $plan = $this->subscription->get_active($user['id_user']);
                    
                    if ($plan) {

                        $this->binarytree->points_back($user);

                      
                        //get referrals by subscription
                        $directs = $this->referral->get_directs_subscription($user['id_user'], $plan['id']);
                        
                        if (($directs['left_count'] > 0) && ($directs['right_count'] > 0)) {
                            // Determine which one of both is the minor side
                            $minor = ($user['left_points'] < $user['right_points']) ? $user['left_points'] : $user['right_points'];
                            $handler = ($user['left_points'] < $user['right_points']) ? 'left' : 'right';

                            if ($minor > 0) {
                                // Math bonus value
                                if(array_key_exists($plan['id_plan'], self::BONUS)){
                                    $bonus = ($minor * self::BONUS[$plan['id_plan']])/100;
                                }else{
                                    $bonus = 0;
                                }

                                $rank = $this->rank->get($user['id_user']);
                                switch ($rank['rank']) {
                                    case 4:
                                        $bonus = $bonus > 8000 ? 8000 : $bonus;
                                        break;
                                    case 5:
                                        $bonus = $bonus > 20000 ? 20000 : $bonus;
                                        break;
                                    case 6:
                                        $bonus = $bonus > 40000 ? 40000 : $bonus;
                                        break;
                                    case 7:
                                        $bonus = $bonus > 60000 ? 60000 : $bonus;
                                        break;
                                    case 8:
                                        $bonus = $bonus > 80000 ? 80000 : $bonus;
                                        break;
                                    case 8:
                                        $bonus = $bonus > 80000 ? 80000 : $bonus;
                                        break;
                                    
                                    default:
                                        break;
                                }

                                $records[] = array(
                                    'id_user' => $user['id_user'],
                                    'minor' => $minor,
                                    'bonus' => $bonus,
                                    'handler' => ($handler == 'left') ? 'Left' : 'Right'
                                );

                                // Update major side points discounting minor site points
                                if ($handler == 'left') {
                                    $user['right_points'] = $user['right_points'] - $user['left_points'];
                                    $user['left_points'] = 0;
                                }

                                if ($handler == 'right') {
                                    $user['left_points'] = $user['left_points'] - $user['right_points'];
                                    $user['right_points'] = 0;
                                }
                                
                                $this->traceability->insert_settlement_binary([
                                    'id_user' => $user['id_user'],
                                    'points' => $bonus,
                                    'type' => 'binary'
                                ]);
                                // Update tree profit
                                $user['profit'] = ($user['profit'] + $bonus);
                            }
                        }
                    }
                }

                $user['last_bonus'] = date("Y-m-d H:i:s");

                // Update user tree with new values (left and right points, profit, json sides)
                $this->binarytree->update_parent($user);
            }

            if ($records) {
                $this->binarytree->add_bonus($records);
            }
        } catch (Exception $e) {
            $this->_alert($e->getMessage());
        }
    }

    private function _alert($alert = "") {
        $this->load->library('email');
        $this->email->from("{$this->settings->site_email}");
        $this->email->to("{$this->settings->site_alerts_email}", TRUE);
        $this->email->subject("Binary profit job");
        $this->email->message("An error was captured while a binary process were running " . date("Y-m-d H:i:s") . "{$alert}");
        $this->email->set_mailtype("html");
        $this->email->send();
        return false;
    }

}
