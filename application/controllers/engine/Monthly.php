<?php defined('BASEPATH') OR exit('No direct script access allowed');


/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of Rates
 *
 * @author Juan Manuel
 */
class Monthly extends MY_Controller{
    //put your code here
    function __construct() {
        parent::__construct();
        
        // Create an array with allowed hours for cron job
        $next_slice = date("Y-m-d", strtotime("{$this->settings->next_monthly_fee}"));

        /*if(date("Y-m-d") != $next_slice){
            echo date("Y-m-d");
            echo "<br> Access denied";
            exit();
        }*/
        
        $this->lang->load('settings');
        $this->load->library('transaction');
        $this->load->model('users_model','users');
        $this->load->model('subscriptions_model','subscriptions');
        $this->load->model('settings_model');
        
    }
    
    function index(){
        
        try {
            $next_slice = date("Y-m-t", strtotime('last day of next month'));
            $this->settings_model->save_settings(array('next_monthly_fee' => $next_slice),1);
            $users = $this->users->get_all(0,array(
                'status' => 1,
                'profile' => 'user'
            ));
            foreach($users['results'] as $user){
                $this->subscriptions->update([
                    'id_user' => $user['id'],
                    'preferencial_user' => '1'
                ]);
            }
        } catch (Exception $e) {
            echo $e->getMessage();
        }
    }
    
}
