<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class Infinity_bonus extends MY_Controller {

    //put your code here
    function __construct() {
        parent::__construct();

        $this->lang->load('settings');
        $this->load->library('transaction');
        $this->load->library('residual');
        $this->load->model('users_model', 'users');
        $this->load->model('subscriptions_model', 'subscription');
        $current_hour = date("G");

        $current_day = date('d');
        $last_month_day = date('t');

        $day = ($current_day == $last_month_day) ? TRUE : FALSE;
        $hour = ($current_hour > 20) ? TRUE : FALSE;

        /*if (!$day || !$hour) {
            echo "Access denied";
            exit();
        }*/
    }

    function index() {
        try {
            $users = $this->residual->get_allowed_users();
            
            if ($users) {
                array_walk($users, array($this, '_settle'), $users);
            }
        } catch (Exception $e) {
            $this->_alert($e->getMessage());
        }
    }

    function _settle($id_user) {
        
        if (is_active($id_user)) {
            
            $infinity_bonus = $this->residual->infinity_bonus($id_user);
            if ($infinity_bonus['rank'] > 0){
                $bonus_profit = ($infinity_bonus['points'] * $infinity_bonus['percentage_profit']) / 100;
                $description = "Infinite bonus benefit: {$infinity_bonus['points']} points with a percentage of {$infinity_bonus['percentage_profit']}%";
                
                $this->transaction->add_infinity_bonus($id_user, $bonus_profit, $description);
                $this->residual->add_infinity_bonus($id_user, $infinity_bonus['points']);
                $this->subscriptions->add_profit($id_user,$infinity_bonus['points']);
            }
        }
    }

    private function _alert($alert = "") {
        $this->load->library('email');
        $this->email->from("{$this->settings->site_email}");
        $this->email->to("{$this->settings->site_alerts_email}", TRUE);
        $this->email->subject("Referral bonus cron job");
        $this->email->message("Cron job could create recents users file " . date("Y-m-d H:i:s") . "{$alert}");
        $this->email->set_mailtype("html");
        $this->email->send();
        return false;
    }

}