<?php defined('BASEPATH') OR exit('No direct script access allowed');

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of Bonus
 *
 * @author Juan Manuel Pinzon
 */
class Bonus extends MY_Controller{
    //put your code here
    
    function __construct() {
        parent::__construct();
        
        $current_hour = date("G");
        if($current_hour != 23){
            echo "Access denied"; 
            exit();
        }
        
        $this->lang->load('settings');
        $this->load->model('subscriptions_model','subscriptions');
        $this->load->library('transaction');
    }
    
    function index(){
        
        // Get active susbcriptions
        $subscriptions = $this->subscriptions->get_bonus_allowed(40);
        array_walk($subscriptions, array($this,'_bonusCharge'), $subscriptions);
        
    }
    
    private function _bonusCharge(&$element){
        
        try{
            // Get current day
            $day = date('D');
            $excluded = explode(',',$element['excluded_days']);

            // Excluded days verification
            if (!in_array($day,$excluded)){
                // Load bonus
                $this->transaction->addBonus($element);
            }
            
        } catch (Exception $ex) {
            $this->_alert($ex->getMessage());
        }
                
    }
    
    
    private function _alert($alert = ""){
        $this->load->library('email');
        $this->email->from("{$this->settings->site_email}");
        $this->email->to("{$this->settings->site_alerts_email}", TRUE);
        $this->email->subject("Daily profit updated failure");
        $this->email->message("Cron job could charge a daily profit ". date("Y-m-d H:i:s")."{$alert}");
        $this->email->set_mailtype("html");
        $this->email->send();
        return false;
    }
    
}
