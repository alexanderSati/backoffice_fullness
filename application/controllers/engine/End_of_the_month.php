<?php defined('BASEPATH') OR exit('No direct script access allowed');

class End_of_the_month extends MY_Controller {

    var $steps = ['rank','binary','infinity_bonus', 'close_month'];
    var $for_running = [];
    var $step = 0;

    const BONUS = [
        9 => 0,
        5 => 0,
        6 => 10,
        7 => 15,
        8 => 30
    ];

    function __construct()
    {
        parent::__construct();
        $this->check_request();

        $this->load->model('end_of_the_month_model');
        $this->load->model('settings_model');
        $this->lang->load('settings');
        $this->load->library('transaction');
        $this->load->library('referral');
        $this->load->library('residual');

        $this->load->model('users_model', 'users');
        $this->load->model('subscriptions_model', 'subscription');
    }

    /**
     * Rank
     * Binary
     * infinity_bonus
     */

    function index()
    {
        $this->start();
        echo "este es el step {$this->for_running['step']}<br>";
        echo "Entra al paso: {$this->steps[$this->for_running['step']]}<br>";
        switch ($this->steps[$this->for_running['step']]) {
            case 'rank':
                $this->load->library('rank');
                //$this->run_rank();
                $this->run_fullness_bonus();
                $this->next_step($this->step);
                
                break;
            case 'binary':
                $this->step = 1;
                $this->load->library('binarytree');
                $this->load->library('referral');
                $this->load->library('rank');

                $this->load->library('traceability');

                $this->run_binary();
                
                break;
            case 'infinity_bonus':
                $this->step = 2;
                $this->load->library('residual');

                $this->run_infinity_bonus();
                break;
            case 'close_month':
                $this->step = 3;
                $this->subscription->close_month();
                $this->next_step($this->step);
                break;
            default:
                $this->run_end_cron();
                break;
        }

    }

    private function run_end_cron()
    {
        if($this->settings->end_of_the_month == 1){
            $this->update_setting_end_of_the_month(0);
            $this->send_email_final();
        }
        die('run_end_cron');
    }


    public function run_fullness_bonus(){
        log_message('DEBUG', 'INFO BONO_FULLNESS');

        $users = $this->referral->get_with_two_referrals_master();
        log_message('DEBUG', 'INFO BONO_FULLNESS:'.count( $users ).' usuarios, mes:"'.date("Y-m-d").'"');
        foreach($users as $u){
            $this->transaction->add_bonus_fullness($u["id_user"]);
        }
    
    }

    private function run_infinity_bonus()
    {
        try {
            $users = $this->residual->get_allowed_users();
            if ($users) {
                array_walk($users, array($this, '_settle'), $users);
            }else{
                $this->next_step($this->step);
            }
        } catch (Exception $e) {
            $this->_alert($e->getMessage());
        }
    }

    private function run_binary()
    {
        $records = array();

        try {
            // Get the list of user to update
            // Obtiene los usuario que ya han hecho binario
            $users = $this->binarytree->bonus_list();
            if(empty($users)){
                $this->next_step($this->step);
            }

            foreach ($users as $user) {
                if (is_active($user['id_user'])) {
                    $plan = $this->subscription->get_active($user['id_user']);
                    if ($plan) {
                        $this->binarytree->points_back($user);
                        //get referrals by subscription
                        // Obtiene los referidos de un usuario
                        $directs = $this->referral->get_directs_subscription($user['id_user'], $plan['id']);
                        if (($directs['left_count'] > 0) && ($directs['right_count'] > 0)) {
                            // Determine which one of both is the minor side
                            $minor = ($user['left_points'] < $user['right_points']) ? $user['left_points'] : $user['right_points'];
                            $handler = ($user['left_points'] < $user['right_points']) ? 'left' : 'right';
                            if ($minor > 0) {
                                // Math bonus value
                                if(array_key_exists($plan['id_plan'], self::BONUS)){
                                    $bonus = ($minor * self::BONUS[$plan['id_plan']])/100;
                                }else{
                                    $bonus = 0;
                                }

                                $rank = $this->residual->unilevel($user['id_user']);
                                switch ($rank['rank']) {
                                    case 4:
                                        $bonus = $bonus > 8000 ? 8000 : $bonus;
                                        break;
                                    case 5:
                                        $bonus = $bonus > 20000 ? 20000 : $bonus;
                                        break;
                                    case 6:
                                        $bonus = $bonus > 40000 ? 40000 : $bonus;
                                        break;
                                    case 7:
                                        $bonus = $bonus > 60000 ? 60000 : $bonus;
                                        break;
                                    case 8:
                                        $bonus = $bonus > 80000 ? 80000 : $bonus;
                                        break;
                                    case 8:
                                        $bonus = $bonus > 80000 ? 80000 : $bonus;
                                        break;

                                    default:
                                        break;
                                }

                                $records[] = array(
                                    'id_user' => $user['id_user'],
                                    'minor' => $minor,
                                    'bonus' => $bonus,
                                    'handler' => ($handler == 'left') ? 'Left' : 'Right'
                                );

                                // Update major side points discounting minor site points
                                if ($handler == 'left') {
                                    $user['right_points'] = $user['right_points'] - $user['left_points'];
                                    $user['left_points'] = 0;
                                }

                                if ($handler == 'right') {
                                    $user['left_points'] = $user['left_points'] - $user['right_points'];
                                    $user['right_points'] = 0;
                                }

                                $this->traceability->insert_settlement_binary([
                                    'id_user' => $user['id_user'],
                                    'points' => $minor,
                                    'type' => 'binary'
                                ]);
                                // Update tree profit
                                $user['profit'] = ($user['profit'] + $bonus);
                            }
                        }
                    }
                }

                $user['last_bonus'] = date("Y-m-d H:i:s");

                // Update user tree with new values (left and right points, profit, json sides)
                $this->binarytree->update_parent($user);
            }

            if ($records) {
                $this->binarytree->add_bonus($records);
            }
        } catch (Exception $e) {
            $this->_alert($e->getMessage());
        }
    }

    private function run_rank()
    {
        echo "Run rank<br>";
        try {
            // Get allowed users to updated
            $users = $this->rank->get_allowed();
            if(isset($users['id_user'])){
                $tmp = $users;
                $users = [];
                $users[] = $tmp;
            }
            
            if ($users) {
                array_walk($users, array($this, '_rank_status'), $users);
            }else{
                echo "Next step binary<br>";
                $this->next_step($this->step);
            }
        } catch (Exception $e) {
            $this->_alert($e->getMessage());
        }
    }

    private function _rank_status($user)
    {
        if (is_active($user['id_user'])) {
            $data_u = $this->users->get_user($user['id_user']);
            $updated = _get_calculate_rank($user['id_user']);
            if (($user['rank'] < $updated) && ($data_u['status'] == 1) && ($updated > 0)) {
                $user['rank'] = $updated;
            }
        }
        // Update rank data
        $user['last_updated'] = date("Y-m-d H:i:s");
        $this->rank->update($user['id_user'], $user);
    }
    private function _settle($id_user) {
        if (is_active($id_user)) {
            $infinity_bonus = $this->residual->infinity_bonus($id_user);
            if ($infinity_bonus['rank'] > 0){
                $bonus_profit = ($infinity_bonus['points'] * $infinity_bonus['percentage_profit']) / 100;
                $description = "Bono residual al infinito: {$infinity_bonus['points']} puntos con porcentaje del {$infinity_bonus['percentage_profit']}%";
                
                $this->transaction->add_infinity_bonus($id_user, $bonus_profit, $description);
                $this->residual->add_infinity_bonus($id_user, $infinity_bonus['points']);
                $this->subscriptions->add_profit($id_user,$infinity_bonus['points']);
            }else{
                $this->users->update_infinity_bonus($id_user);
            }
        }else{
            $this->users->update_infinity_bonus($id_user);
        }
    }

    private function _alert($alert = "")
    {
        $this->load->library('email');
        $this->email->from("{$this->settings->site_email}");
        $this->email->to("{$this->settings->site_alerts_email}", TRUE);
        if($type = 'rank'){
            $this->email->subject("Referral bonus cron job");
            $this->email->message("Cron job could create recents users file " . date("Y-m-d H:i:s") . "{$alert}");
        }else{
            $this->email->subject("Binary profit job");
            $this->email->message("An error was captured while a binary process were running " . date("Y-m-d H:i:s") . "{$alert}");
        }

        $this->email->set_mailtype("html");
        $this->email->send();
        return false;
    }

    private function start()
    {

        $for_running = $this->end_of_the_month_model->get(['date_at' => date('Y-m-d')]);
        if(!$for_running){
            $this->update_setting_end_of_the_month(1);
            $for_running = $this->end_of_the_month_model->init();
            $this->send_email_start();
        }
        $this->for_running = $for_running;
    }

    private function update_setting_end_of_the_month($value)
    {
        $this->settings_model->update('end_of_the_month', ['value' => $value ]);
    }

    private function check_request()
    {
        if(!isset($_GET['cron_test'])){
            if (!$this->input->is_cli_request()) {
                echo "Access denied: the request must be by CLI";
                exit();
            }
            $this->is_the_cutoff_date();
        }
    }

    private function next_step($step)
    {
        $current_step = $this->end_of_the_month_model->get(['date_at' => date('Y-m-d'), 'step' => $step]);
        $current_step['complete'] = 1;

        $this->end_of_the_month_model->update($current_step['id_end_of_the_month'],$current_step);
        $this->end_of_the_month_model->insert(null,['step' => ++$current_step['step']]);

    }

    private function is_the_cutoff_date()
    {
        $current_hour = date("G");
        $current_day = date('d');
        $last_month_day = date('t');

        $day = ($current_day == $last_month_day) ? TRUE : FALSE;
        $hour = ($current_hour >= 23) ? TRUE : FALSE;

        if (!$day || !$hour) {
            echo "Access denied: he will only run until the {$last_month_day} of this month after 23 hours";
            exit();
        }
    }

    private function send_email_final()
    {
            $html = $this->load->view("emails/spanish/email_closing", $data, TRUE);
            // send email
            $this->load->library('email');
            $this->email->from("{$this->settings->site_email}");
            $this->email->to("{$this->settings->site_email}");
            $this->email->subject("Fin del cierre de mes");
            $this->email->message($html);
            $this->email->set_mailtype("html");
            $this->email->send();
    }
    private function send_email_start()
    {
        $html = $this->load->view("emails/spanish/email_start", $data, TRUE);
        // send email
        $this->load->library('email');
        $this->email->from("{$this->settings->site_email}");
        $this->email->to("{$this->settings->site_email}");
        $this->email->subject("Inicio del cierre de mes");
        $this->email->message($html);
        $this->email->set_mailtype("html");
        $this->email->send();
    }


}