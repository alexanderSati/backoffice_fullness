<?php defined('BASEPATH') OR exit('No direct script access allowed');


/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of Rates
 *
 * @author Juan Manuel
 */
class Recents extends MY_Controller{
    //put your code here
    function __construct() {
        parent::__construct();
        
        $this->lang->load('settings');
        $this->load->model('locations_model','locations');
        $this->load->model('users_model','users');
        if(!$this->input->is_cli_request()){
            echo "Access denied";
            exit();
        }        
    }
    
    function index(){
        try {
            $contries = array('NL', 'US', 'PE' , 'MX' , 'IT');
            // Get json file content
            $json = json_decode(file_get_contents(FCPATH."/recentUsers.json"),true);

            // get a random name
            $this->db->select('nameset, UPPER(SUBSTR(nameset, 1, 2)) as country, LOWER(givenname) as first_name, LOWER(surname) as last_name');
            $this->db->from('fakenames');
            $this->db->where_in('nameset',$contries);
            $this->db->order_by('givenname','random');
            $this->db->limit(1);

            $new_recent = $this->db->get()->row_array();
           
            $min_epoch = strtotime("-20 minutes");
            $max_epoch = strtotime('now');

            $rand_epoch = rand($min_epoch, $max_epoch);
            $new_recent['created'] = date('Y-m-d H:i:s', $rand_epoch);
            
            $json[] = $new_recent;
            
            $this->_array_sort_by_column($json, 'created',SORT_DESC);

            $json_encoded = json_encode($json,JSON_PRETTY_PRINT);

            // Save json file with a new user
            file_put_contents(FCPATH."/recentUsers.json",$json_encoded);
                        
        } catch (Exception $e) {
            $this->_alert($e->getMessage());
        }
        
    }
    
    private function _alert($alert = ""){
        $this->load->library('email');
        $this->email->from("{$this->settings->site_email}");
        $this->email->to("{$this->settings->site_alerts_email}", TRUE);
        $this->email->subject("Recent users job");
        $this->email->message("Cron job could create recents users file ". date("Y-m-d H:i:s")."{$alert}");
        $this->email->set_mailtype("html");
        $this->email->send();
        return false;
    }
    
    private function _array_sort_by_column(&$arr, $col, $dir = SORT_ASC) {
        $sort_col = array();
        foreach ($arr as $key=> $row) {
            $sort_col[$key] = $row[$col];
        }

        array_multisort($sort_col, $dir, $arr);
    }
    
}
