<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class Profile extends Private_Controller {

    /**
     * Constructor
     */
    function __construct() {
        parent::__construct();

        // load the language file
        $this->lang->load('users');

        // load the users model
        $this->load->model([
            'users_model',
            'plans_model',
            'subscriptions_model'
        ]);

        $this->load->model('locations_model', 'locations');
        $this->load->library([
            'token',
            'plan'
        ]);



        // upload config
        $config['upload_path'] = "uploads/";
        $config['allowed_types'] = 'gif|jpg|png';
        $config['max_size'] = 0;
        $config['max_width'] = 1024;
        $config['max_height'] = 1024;

        $this->load->library('upload', $config);
    }

    /*     * ************************************************************************************
     * PUBLIC FUNCTIONS
     * ************************************************************************************ */

    /**
     * Profile Editor
     */
    function index() {
        if (!is_allowed('read')) {
            $this->session->set_flashdata('error', privilegies_message());
            redirect('user/dashboard');
        }
        if ($this->session->userdata('logged_in')) {
            $logged_in_user = $this->session->userdata('logged_in');
            if ($logged_in_user['is_admin']) {
                redirect('admin/settings');
            } elseif ($logged_in_user['profile'] == 'executive') {
                redirect('orders');
            }
        }

        // validators
        $this->form_validation->set_error_delimiters($this->config->item('error_delimeter_left'), $this->config->item('error_delimeter_right'));
        $this->form_validation->set_rules('username', lang('users input username'), 'required|trim|min_length[5]|max_length[30]|callback__check_username');
        $this->form_validation->set_rules('first_name', lang('users input first_name'), 'required|trim|min_length[2]|max_length[32]');
        $this->form_validation->set_rules('last_name', lang('users input last_name'), 'required|trim|min_length[2]|max_length[32]');
        $this->form_validation->set_rules('email', lang('users input email'), 'required|trim|max_length[128]|valid_email');
        $this->form_validation->set_rules('birthdate', lang('users input birthdate'), 'required');
        $this->form_validation->set_rules('phone', lang('users input phone'), 'required|numeric|min_length[6]');
        $this->form_validation->set_rules('address', lang('users input address'), 'required|min_length[8]');
        $this->form_validation->set_rules('address2', lang('users input address2'), 'min_length[8]');
        $this->form_validation->set_rules('city', lang('users input city'), 'required|min_length[4]');    
        $this->form_validation->set_rules('department', lang('users input department'), 'required|min_length[4]');
        $this->form_validation->set_rules('postal_code', lang('users input postal_code'), 'required|numeric');   
        $this->form_validation->set_rules('bank_account', lang('users input bank_account'), 'required|min_length[8]');
        $this->form_validation->set_rules('language', lang('users input language'), 'required|trim');
        $this->form_validation->set_rules('password_repeat', lang('users input password_repeat'), 'min_length[5]');
        $this->form_validation->set_rules('password', lang('users input password'), 'min_length[5]|matches[password_repeat]');
        $this->form_validation->set_rules('security_pin', lang('users input security_pin'), 'exact_length[4]|matches[security_pin_repeat]');
        $this->form_validation->set_rules('security_pin_repeat', lang('users input security_pin_repeat'), 'exact_length[4]');
        $this->form_validation->set_rules('country', lang('users input country'), 'required|alpha');
        #$this->form_validation->set_rules('token', lang('users input token'), 'required|trim');

        if ($this->form_validation->run() == TRUE) {

            if ($this->token->validate($this->input->post('token')) || true) {
                $data = $this->input->post();
                //Upload image
                $data['image'] = '';
                if (!$this->upload->do_upload('image')) {
                    if ($_FILES['image']['error'] != 4) {
                        $this->session->set_flashdata('error', $this->upload->display_errors());
                        redirect('user/profile');
                    }
                } else {
                    $file_data = array('upload_data' => $this->upload->data());
                    $data['image'] = $file_data['upload_data']['file_name'];
                }

                // save the changes
                $saved = $this->users_model->edit_profile($data, $this->user);

                if ($saved) {
                    // reload the new user data and store in session
                    $this->user = $this->users_model->get_user($this->user['id']);
//                    unset($this->user['password']);
//                    unset($this->user['security_pin']);
//                    unset($this->user['salt']);

                    $this->session->set_userdata('logged_in', $this->user);
                    $this->session->language = $this->user['language'];
                    $this->lang->load('users', $this->user['language']);
                    $this->session->set_flashdata('message', lang('users msg edit_profile_success'));
                } else {
                    $this->session->set_flashdata('error', lang('users error edit_profile_failed'));
                }

                // reload page and display message
                redirect('user/profile');
            } else {
                $this->session->set_flashdata('error', lang('token-error'));
            }
        }

        // setup page header data
        $this->set_title(lang('users title profile'));

        $this
                ->add_css_theme("select/select2.min.css")
                ->add_js_theme("parsley/parsley.min.js")
                ->add_js_theme("pages/parsley-validator.js")
                ->add_js_theme("select/select2.full.js")
                ->add_js_theme("pages/register.js")
                ->add_js_theme("pages/select2.js")
                ->add_js_theme("pages/profile.js");

        $data = $this->includes;

        $user = $this->users_model->get_user($this->user['id']);

        // set content data
        $content_data = array(
            'cancel_url' => base_url(),
            'user' => $user,
            'plans' => $this->plans_model->get_active(),
            'countries' => $this->locations->getCountries(true),
            'page_title' => $data['page_title']
        );

        // load views
        $data['content'] = $this->load->view('user/profile_form', $content_data, TRUE);
        // $data['chat'] = true;
        $this->load->view($this->template, $data);
    }

    /*     * ************************************************************************************
     * PRIVATE VALIDATION CALLBACK FUNCTIONS
     * ************************************************************************************ */

    /**
     * Make sure username is available
     *
     * @param  string $username
     * @return int|boolean
     */
    function _check_username($username) {
        if (trim($username) != $this->user['username'] && $this->users_model->username_exists($username)) {
            $this->form_validation->set_message('_check_username', sprintf(lang('users error username_exists'), $username));
            return FALSE;
        } else {
            return $username;
        }
    }

    /**
     * Make sure email is available
     *
     * @param  string $email
     * @return int|boolean
     */
    function _check_email($email) {
        if (trim($email) != $this->user['email'] && $this->users_model->email_exists($email)) {
            $this->form_validation->set_message('_check_email', sprintf(lang('users error email_exists'), $email));
            return FALSE;
        } else {
            return $email;
        }
    }

    function ajax($request = null, $param1 = null, $param2 = null) {
        if (!$this->input->is_ajax_request()) {
            // No direct script access allowed
            exit;
        }

        switch ($request) {

            case "request-token" :

                $token = $this->token->send($this->user['id']);

                if ($token) {
//                    $message = lang("send-token") . ". " . sprintf(lang("show-token"), $token);
                    $message = lang("send-token");
                    echo $message;
                }

                break;

            default :
                break;
        }
    }

    /**
     * Carlos Aguirre 2019-07-19 17:41:58
     * Muestra la vista donde se eligirá un plan 
     * o se establecera como comprador preferencial
     */

    public function select_type_purchases(){
        
        $this
            ->add_root_css( "assets/css/component.css" )
            ->add_root_js( "assets/js/classie.js" )
            ->add_root_js( "assets/js/modalEffects.js" );
        
        $data = $this->includes;

        $content_data['planes'] = $this->plan->get_active();
        $data['content'] = $this->load->view('user/select_type_purchases', $content_data, true);
        $this->load->view( $this->template, $data );
    }




    /**
     * Funcion que pone a un usuario como comprador preferente
     * actualizando la subscripcion activa del usuario
     * poniendo el campo de `preferencial_user`=1
     */
    public function activate_preferencial(){

        $subscripcion_activa = $this->subscriptions_model->get_active($this->user['id']);

        /**
         * Se debe establecer el campo `subscriptions`.`preferencial_user` = '1'
         * que indica que el usuario escogio ser un "Comprador preferencial"
         */
        $subscripcion_activa['preferencial_user'] = '1';

        $this->subscriptions_model->update( $subscripcion_activa );

        $this->session->set_flashdata('message', "Se ha escogido comprador preferencial");
        redirect(base_url( 'cart' ));
        
    }


    /**
     * Carlos Aguirre 2019-07-19 18:15:08
     * Crea en la sesion del usuario en donde se almacenará
     * temporalmente el plan que se ha escogido
     */
    public function set_temporal_plan($plan_id){
        
        $plan = $this->plan->get_plan( $plan_id );
        $this->session->set_userdata('plan_id', $plan_id);
        $this->session->set_userdata('plan_value', $plan["base"]);

        redirect('Cart');
        
    }
    /**
     * Edit user executive
     */
    function executive(){
        if ($this->session->userdata('logged_in')) {
            $logged_in_user = $this->session->userdata('logged_in');
            if ($logged_in_user['is_admin']) {
                redirect('admin/settings');
            } elseif ($logged_in_user['profile'] == 'user') {
                redirect('user/dashboard');
            }
        }

        // validators
       $this->form_validation->set_error_delimiters($this->config->item('error_delimeter_left'), $this->config->item('error_delimeter_right'));
       $this->form_validation->set_rules('username', lang('users input username'), 'required|trim|min_length[5]|max_length[30]|callback__check_username[' . $user['username'] . ']');
       $this->form_validation->set_rules('first_name', lang('users input first_name'), 'required|trim|min_length[2]|max_length[32]');
       $this->form_validation->set_rules('last_name', lang('users input last_name'), 'required|trim|min_length[2]|max_length[32]');
       $this->form_validation->set_rules('email', lang('users input email'), 'required|trim|max_length[128]|valid_email|callback__check_email[' . $user['email'] . ']');
       $this->form_validation->set_rules('language', lang('users input language'), 'required|trim');
       $this->form_validation->set_rules('password', lang('users input password'), 'min_length[5]|matches[password_repeat]');
       $this->form_validation->set_rules('password_repeat', lang('users input password_repeat'), 'matches[password]');

       if ($this->form_validation->run() == TRUE)
        {

            $data = $this->input->post();
            
            //Upload image
            $data['image'] = '';
            if ( ! $this->upload->do_upload('image'))
            {
                if($_FILES['image']['error'] != 4)
                {
                    $this->session->set_flashdata('error', $this->upload->display_errors());
                    redirect('user/profile/executive');
                }
            }
            else
            {
                $file_data = array('upload_data' => $this->upload->data());
                $data['image'] = $file_data['upload_data']['file_name'];
            }
            // save the changes
            $saved = $this->users_model->edit_profile($data, $this->user);

            if ($saved) {
                // reload the new user data and store in session
                $this->user = $this->users_model->get_user($this->user['id']);
                $this->session->set_userdata('logged_in', $this->user);
                $this->session->language = $this->user['language'];
                $this->lang->load('users', $this->user['language']);
                $this->session->set_flashdata('message', lang('users msg edit_profile_success'));
            } else {
                $this->session->set_flashdata('error', lang('users error edit_profile_failed'));
            }

            // return to list and display message
            redirect('user/profile/executive');
        }

       // setup page header data
       $this->set_title(lang('users title profile'));

       $this
               ->add_css_theme("select/select2.min.css")
               ->add_js_theme("parsley/parsley.min.js")
               ->add_js_theme("pages/parsley-validator.js")
               ->add_js_theme("select/select2.full.js")
               ->add_js_theme("pages/register.js")
               ->add_js_theme("pages/select2.js")
               ->add_js_theme("pages/profile.js");

       $data = $this->includes;

       //get shipping details 
       $shipping_query = $this->shipping_details_model->get_shipping_executive();

       $shipping = array_combine(array_column($shipping_query, 'id'),array_column($shipping_query, 'name'));

       $user = $this->users_model->get_executive_by_id($this->user['id']);

       // set content data
       $content_data = array(
           'cancel_url' => base_url() . 'orders',
           'user' => $user,
           'shipping' => $shipping,
           'page_title' => $data['page_title'],
       );

       // load views
       $data['content'] = $this->load->view('user/profile_executive_form', $content_data, TRUE);
       // $data['chat'] = true;
       $this->load->view($this->template, $data);
    }

}