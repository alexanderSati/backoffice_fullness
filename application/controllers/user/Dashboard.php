<?php defined('BASEPATH') OR exit('No direct script access allowed');







class Dashboard extends Private_Controller {







    /**



     * Constructor



     */



    function __construct()



    {



        parent::__construct();







        // load the language files



        $this->lang->load('dashboard');



        $this->lang->load('news');



        $this->lang->load('transactions');



        



        $this->load->model('users_model','users');



        $this->load->model('locations_model','locations');



        $this->load->model('currencies_model','currencies');



        $this->load->model('subscriptions_model','subscriptions');



        $this->load->model('shopping_order_model');



        $this->load->model('plans_model');



        $this->load->model('news_model');



        $this->load->model('binarytree_model');



        $this->load->model('referrals_model','referrals');



        $this->load->model('residual_model');



        $this->load->library('transaction');



        $this->load->library('wallet');



        $this->load->library('residual');



        $this->load->library('binarytree');



        $this->load->library('rank');















    }







    /**



	 * Profile Users



     */



    function index(){







        if (!is_allowed('read')){



            $this->session->set_flashdata('error', privilegies_message() );



            redirect('user/account/privilegies');



        }







        // setup page header data



        $this



            ->add_root_css( "assets/pages/list-scroll/list.css" )



            ->add_root_css( "bower_components/owl.carousel/css/owl.carousel.css" )



            ->add_root_css( "bower_components/owl.carousel/css/owl.theme.default.css" )



            ->add_root_css( "bower_components/swiper/css/swiper.min.css" )



            ->add_root_js( "bower_components/jquery-slimscroll/js/jquery.slimscroll.js" )



            ->add_root_js( "bower_components/stroll/js/stroll.js" )



            ->add_root_js( "assets/pages/list-scroll/list-custom.js" )



            ->add_root_js( "bower_components/owl.carousel/js/owl.carousel.min.js" )



            ->add_root_js( "assets/js/owl-custom.js" )



            ->add_root_js( "bower_components/swiper/js/swiper.min.js" )



            ->add_root_js( "assets/js/swiper-custom.js" )



            ->add_root_js( "bower_components/datatables.net/js/jquery.dataTables.min.js" )



            ->add_root_js( "assets/pages/data-table/js/data-table-custom.js" )



            ->add_root_js("js/countdown.min.js")



            ->add_root_js( "js/pages/dashboard_user.js" )



            ->add_root_js( "bower_components/datatables.net-bs4/js/dataTables.bootstrap4.min.js" )            



            ->add_root_css( "bower_components/datatables.net-bs4/css/dataTables.bootstrap4.min.css" ) 



            ->set_title( lang('dashboard') );







        $data = $this->includes;







        $chart = $this->transaction->userBonus($this->user['id']);



        $chart_js = array();



        foreach ($chart as $index => $item) {



            $date = explode(" ", $item['date']);



            $chart_js[$index]['date'] = $date[0];



            $chart_js[$index]['value'] = $item['usd'];



        }



        



        // set content data



        $content_data['recent'] =$this->_recents();



               



        $content_data['currencies'] = $this->currencies->get("BTC");



        $content_data['subscription'] = $this->subscriptions->get_active($this->user['id']);



        



        if ($content_data['subscription']) {



            if (!empty($content_data['subscription']['period_days']) && !empty($content_data['subscription']['period_days_left'])) {



                



                $period_days = $content_data['subscription']['period_days'] ;



                $period_days_left = $content_data['subscription']['period_days_left'];







                $content_data['subscription']['payments_done'] = ((($period_days - $period_days_left)*100)/$period_days);



            }



        }







        $this->currency->load("USD");



        $content_data['profit_btc'] = $this->currency->btc($content_data['subscription']['profit']);







        $content_data['transactions'] = $this->transaction->user($this->user['id']);



        $content_data['chart'] = $chart_js;







        $content_data['message'] = (array_key_exists('wellcome_message',(array) $this->settings)) ? $this->settings->wellcome_message[$this->session->language] : "";



        $content_data['pdf_link'] = (array_key_exists('file_to_download',(array) $this->settings)) ? $this->settings->file_to_download[$this->session->language] : "";







        $news = $this->get_posts_by_category_slug($this->settings->dashboards_news);



        $sign = $this->get_posts_by_category_slug($this->settings->sign, 1);        



        

        $bonos = $this->transactions->get_sum_by_type_and_user($this->user['id']);

        $content_data['bonos']  = round($bonos["infinity"],2) +  round($bonos["referral"],2) + round($bonos["binary"],2) + round($bonos["activation"],2) + round($bonos["upgrade"],2) + round($bonos["bonus_fullness_global"],2) + round($bonos["bonus_award"],2) + round($bonos["bonus_external_sales"],2) ;









        $content_data['post_info'] = $news;



        $content_data['post_sign'] = $sign;



        $content_data['stats_tree'] = $this->binarytree_model->get_score($this->user['id']);



        $content_data['profit'] = $this->subscriptions->get_subscription($this->user['id']);    



        $content_data['total_referrals'] = count($this->referrals->get_referrals($this->user['id']));



        $content_data['saldo'] = $this->wallet->get($this->user['id'], 'USD');



        $content_data['residual'] = $this->residual->get_score($this->user['id']);



        $content_data['residual_table'] = $this->residual->get_residual_table($this->user['id']);



        $content_data['binary_table'] = $this->binarytree->get_table_binary($this->user['id']);



        $content_data['current_plan'] = $this->plans_model->get_plan($content_data['subscription']['id_plan']);



        $content_data['qty_purchases'] = $this->shopping_order_model->get_quantity_purchases_user($this->user['id']);











        $points_all_time = $this->binarytree->get_points_all_time($this->user['id']);







        $content_data['total_points_binary'] = $points_all_time;



        







        $unilevel = $this->residual->unilevel($this->user['id']);







        $content_data['rank'] = $this->rank->get_rank($this->user['id']);



       



        $content_data['list_awards'] = [



            ['points' => 3000,'bonus' => '500 S/', 'icon' => '500_soles.png'],



            ['points' => 12000,'bonus' => '1500 S/', 'icon' => '500_soles.png'],



            ['points' => 40000,'bonus' => lang('cruise'), 'icon' => 'crucero.png'],



            ['points' => 90000,'bonus' => 'CANCUN', 'icon' => 'cancun.png'],



            ['points' => 200000,'bonus' => 'DUBAI', 'icon' => 'dubai.png'],



            ['points' => 600000,'bonus' => 'AUTO', 'icon' => 'auto.png'],



            ['points' => 1400000,'bonus' => 'MERCEDES', 'icon' => 'mercedes_benz.png'],



            ['points' => 3000000,'bonus' => 'RANGE ROVER', 'icon' => 'range_rover.png'],



            ['points' => 7000000,'bonus' => lang('suitcase'), 'icon' => 'maletin_1_millon.png'],



            ['points' => 15000000,'bonus' => lang('house_million'), 'icon' => 'casa_1_millon.png'],



        ];



        



        $data['content'] = $this->load->view('user/dashboard', $content_data, TRUE);



        // $data['chat'] = true;



        



        $this->load->view($this->template, $data);



    }



    



    private function _recents(){



        // set content data



        $content_data = array(



            'recents'        => array(),



            'countries'      => $this->locations->getCountries(true)



        );



        



        // Get json file content



        $json = json_decode(file_get_contents(FCPATH."/recentUsers.json"),true);



        $users = $this->users->get_latest();



        $content_data['recents'] = array_merge($json, $users);







        $this->_array_sort_by_column($content_data['recents'], 'created',SORT_DESC);



        



        return $content_data;



        



    }



    



    private function _array_sort_by_column(&$arr, $col, $dir = SORT_ASC) {



        $sort_col = array();



        foreach ($arr as $key=> $row) {



            $sort_col[$key] = $row[$col];



        }







        array_multisort($sort_col, $dir, $arr);



    }



    



    public function get_posts_by_category_slug( $slug = null, $limit = 4 ) {



        if($slug){



            $category_id = $this->news_model->get_category_id($slug);



            



            if ($category_id) {



                $info = $this->news_model->all_by_category_id($category_id, $limit);



                



                if($limit > 1) {



                    if ($info) {



                        foreach ($info as &$post) {



                            $post['image'] = $this->news_model->get_featured_image($post['ID']);







                            $timestamp = strtotime($post['post_date']);



                            $post['post_date'] = date("F j, Y", $timestamp);



                        }



                    }



                }



                return $info;



            }



            



        }



        return false;



    }







}