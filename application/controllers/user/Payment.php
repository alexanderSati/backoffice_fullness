<?php defined('BASEPATH') OR exit('No direct script access allowed');





class Payment extends Private_Controller {





  /**


   * Constructor


   */


  function __construct()


  {


      parent::__construct();


      $this->load->model("PaymentMethod_model");


      $this->load->library([


        "cart", 


        "orden",


        "PagoEfectivo"


      ]);


      


      


  }








  /**************************************************************************************


   * PUBLIC FUNCTIONS


   **************************************************************************************/





	/**
   * Carlos Aguirre 2019-10-16 14:13:09
   * Guarda en la tabla `payment_method` el
   * tipo de pago que eligió el usuario
   */

  public function establecer_pago(){

    $order_id = $this->orden->get_temporal_orden($this->user['id'])["shopping_order_id"];
    $total = $this->cart->total();
    $tipo_envio = $this->input->post("tipo_pago");

    // Si el monto es 0 ( no queda restante a pagar )

    if( $total == 0 ){
      
      $data = [
        "method"=>'BILLETERA',
        "shopping_order_id"=>$order_id,
        "created"=>date("Y-m-d H:i:s")
      ];


    }
    else{

      $data = [
        "method"=>$tipo_envio,
        "shopping_order_id"=>$order_id,
        "created"=>date("Y-m-d H:i:s")
      ];

      if( $tipo_envio == 'PAGO_ONLINE' ){

        if($this->pagoefectivo->connect() === true){

          $pagoEfectivo = $this->pagoefectivo->get_cips($order_id, $total);
          $data["code"] = $pagoEfectivo["cip"]; // Codigo del pago
          $data["url"] = $pagoEfectivo["cipUrl"]; // URL del comprobante

        }
      }
    }

    if( $tipo_envio != "PAGO_VISANET" ){

      $this->PaymentMethod_model->crear($data);

    }

    echo json_encode(["success"=>true]);
    exit();

  }








  /**


   * Carlos Aguirre 2019-10-18 14:47:05


   * Notifica el pago realizado de una compra 


   * online


   */


  public function notify_payment(){


    $firma = $this->input->get_request_header("PE-Signature");


    $data =  json_decode($this->input->raw_input_stream, true);





    // Verifica la firma de seguridad


    if( $firma == hash_hmac('sha256', $this->input->raw_input_stream , SECRET_KEY) ){


      http_response_code(200);


      $this->PaymentMethod_model->actualizar(


        [


          "status"=>"approved"


        ],


        [


          "code"=>$data["data"]["cip"],


          "method"=>"PAGO_ONLINE"


        ]


      );


      echo json_encode(["success"=> true]);





    }


    else{


      log_message('debug', 'INFO error aprobando CIPS '.var_export($data, true)."\nFIRMA RECIBIDA:".var_export($firma, true)."\nFIRMA ESPERADA:".hash_hmac('sha256', json_encode($data) , SECRET_KEY));





      http_response_code(400);


      echo json_encode(["success"=> false]);


    }


    exit();





  }











}


