<?php defined('BASEPATH') OR exit('No direct script access allowed');

class Dashboard extends Private_Controller {

    /**
     * Constructor
     */
    function __construct()
    {
        parent::__construct();

        // load the language files
        $this->lang->load('dashboard');
        $this->load->model('users_model','users');
        $this->load->model('locations_model','locations');
        $this->load->model('currencies_model','currencies');
        $this->load->model('subscriptions_model','subscriptions');
        $this->load->library('transaction');
    }

    /**
	 * Profile Users
     */
	function index(){

        if (!is_allowed('read')){
            $this->session->set_flashdata('error', privilegies_message() );
            redirect('user/account/privilegies');
        }

        // setup page header data
        $this
            ->add_css_theme( "floatexamples.css" )
            ->add_css_theme( "datatables/responsive.bootstrap.min.css" )
            ->add_css_theme( "datatables/jquery.dataTables.min.css" )
            ->add_css_theme( "datatables/responsive.bootstrap.min.css" )

            ->add_js_theme( "moris/raphael-min.js")
            ->add_js_theme( "moris/morris.min.js")
            ->add_js_theme( "gauge/gauge.min.js")
            ->add_js_theme( "datatables/jquery.dataTables.min.js")
            ->add_js_theme( "datatables/dataTables.bootstrap.js")
            ->add_js_theme( "datatables/dataTables.responsive.min.js")
            ->add_js_theme( "datatables/responsive.bootstrap.min.js")
            ->add_js_theme( "pages/user_dashboard.js")
            ->add_js_theme( "pages/morris.js" )
            ->add_js_theme( "pages/gauge.js" )

            ->set_title( lang('admin title admin') );

        $data = $this->includes;

        $chart = $this->transaction->userBonus($this->user['id']);
        $chart_js = array();
        foreach ($chart as $index => $item) {
            $date = explode(" ", $item['date']);
            $chart_js[$index]['date'] = $date[0];
            $chart_js[$index]['value'] = $item['usd'];
        }
        
        // set content data
        $content_data['recent'] =$this->_recents();
               
        $content_data['currencies'] = $this->currencies->get("BTC");
        $content_data['subscription'] = $this->subscriptions->get_active($this->user['id']);

        $this->currency->load("USD");
        $content_data['profit_btc'] = $this->currency->btc($content_data['subscription']['profit']);

        $content_data['transactions'] = $this->transaction->user($this->user['id']);
        $content_data['chart'] = $chart_js;
        
        $settings = $this->settings_model->get_settings();
        
        $data_languages['languages'] = $this->settings->{'wellcome_message'};
        
        $content_message['message'] = $data_languages['languages'][$this->session->language];
        
        if (!$content_message['message'] == null){
            $content_data['message'] = $data_languages['languages'][$this->session->language];
        }
        $data['content'] = $this->load->view('user/dashboard', $content_data, TRUE);
        $data['chat'] = true;
        
        $this->load->view($this->template, $data);
    }
    
    private function _recents(){
        // set content data
        $content_data = array(
            'recents'        => array(),
            'countries'      => $this->locations->getCountries(true)
        );
        
        // Get json file content
        $json = json_decode(file_get_contents(FCPATH."/recentUsers.json"),true);
        $users = $this->users->get_latest();
        $content_data['recents'] = array_merge($json, $users);

        $this->_array_sort_by_column($content_data['recents'], 'created',SORT_DESC);
        
        return $content_data;
        
    }
    
    private function _array_sort_by_column(&$arr, $col, $dir = SORT_ASC) {
        $sort_col = array();
        foreach ($arr as $key=> $row) {
            $sort_col[$key] = $row[$col];
        }

        array_multisort($sort_col, $dir, $arr);
    }
    


}
