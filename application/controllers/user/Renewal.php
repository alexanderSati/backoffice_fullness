<?php defined('BASEPATH') OR exit('No direct script access allowed');

class Renewal extends Private_Controller {

    /**
     * Constructor
     */
    function __construct()
    {
        parent::__construct();

        // load the language file
        $this->lang->load('users');

        // load the users model
        $this->load->model('users_model');
        $this->load->model('plans_model');
        $this->load->model('locations_model','locations');
        $this->load->library('token');
        $this->load->library('transaction');
        
    }


    /**************************************************************************************
     * PUBLIC FUNCTIONS
     **************************************************************************************/


    /**
	 * Profile Editor
     */
	function index()
	{
            
            
        $this->form_validation->set_error_delimiters($this->config->item('error_delimeter_left'), $this->config->item('error_delimeter_right'));
        $this->form_validation->set_rules('plan', lang('users input plan'), 'required|numeric|greater_than[0]');
        
       

        // setup page header data
	$this->set_title( lang('account-plan') );

        $this
            ->add_css_theme("select/select2.min.css")
            ->add_js_theme( "select/select2.full.js" )
            ->add_js_theme( "pages/register.js" )
            ->add_js_theme( "pages/select2.js" )
            ->add_js_theme( "pages/profile.js" );

        $data = $this->includes;

        $user = $this->users_model->get_user($this->user['id']);

        // set content data
        $content_data = array(
            'user'              => $user,
            'plans'             => $this->plans_model->get_active(),
            'page_title'        => $data['page_title']
        );
  

        if ($this->form_validation->run() == TRUE)
        {
            $result = $this->transaction->renewal($user,$this->input->post('plan'));
            
            if ($result !== true){
                
                switch ($result) {
                    case -1:
                           $error = lang('invalid-plan');
                        break;
                    case -3:
                           $error = lang('invalid-wallet');
                        break;
                    case -4:
                           $error = lang('error update side');
                        break;
                    case -5:
                           $error = lang('not-enough-funds');
                        break;
                    default:
                        $error = lang('error update side');
                        break;
                }
                
                $this->session->set_flashdata('error', $error);
            }else{
                $this->session->set_flashdata('message', lang('renewal-success'));
                redirect('user/dashboard');
            }
        }
        
        
        // load views
        $data['content'] = $this->load->view('user/renewal', $content_data, TRUE);
        // $data['chat'] = true;
        $this->load->view($this->template, $data);
	}



}
