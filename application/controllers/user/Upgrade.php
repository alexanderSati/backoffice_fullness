<?php defined('BASEPATH') OR exit('No direct script access allowed');

class Upgrade extends Private_Controller {


    /**
     * Constructor
     */
    function __construct()
    {
        parent::__construct();

        // load the language files
        $this->lang->load('users');
        $this->lang->load('dashboard');

        // load the users model
        $this->load->model('users_model');
        $this->load->model('profiles_model');
        $this->load->model('plans_model');
        $this->load->model('subscriptions_model','subscriptions');
        $this->load->model('locations_model','locations');
        $this->load->library('transaction');
        $this->load->library('referral');
        $this->load->library('plan');
        $this->load->library('wallet');

    }


    /**************************************************************************************
     * PUBLIC FUNCTIONS
     **************************************************************************************/


    /**
     * Account list page
     */
    function index()
    {

        $this
            ->add_root_js( "bower_components/sweetalert/js/sweetalert.min.js") 
            ->add_root_css( "bower_components/sweetalert/css/sweetalert.css")
            ->set_title( lang('upgrade_title') );

        $data = $this->includes;

        // get user subscription
        $subscription = $this->subscriptions->get_active($this->user['id']);
    
        // get plans active
        $plans = $this->plan->get_active();
        foreach ($plans as $key => $value) {
            if ($plans[$key]['base'] <= $subscription['base'] ) {
                unset($plans[$key]);
            }            
        }
        // set content data
        $content_data = array(
            'page_title'    => $data['page_title'],
            'plans'         => $plans,
            'subscription'  => $subscription,
            'current_plan'  => $this->plan->get_plan($subscription['id_plan']),
        );
        // Saldo disponible en la billetera
        $content_data["saldoDisponible"] = floatVal($this->wallet->get($this->user['id'], "USD"));
        
        
        if($this->input->post()){

            $selected = $this->input->post('plan');

            if (in_array($selected,$this->plan->ids)){

                // si pago con saldo en billetera debe descontar
                // el pago de su billetera
                // if(!empty( $this->input->post('pago_wallet') )){
                    
                //     $pagoWallet    = $this->input->post('pago_wallet');
                //     $saldoEnWallet = $this->wallet->get($this->user["id"], "USD");


                //     // Debe tener saldos en la billetera
                //     if($pagoWallet <=$saldoEnWallet ){
                //         $this->wallet->subtract_funds($this->user["id"], $pagoWallet  );
                //     }
                //     else{
                //         $this->session->set_flashdata('error', lang('not-enough-funds'));
                //         redirect('user/upgrade');
                //     }

                    

                // }


                // Poner nuevo plan en sesion y enviar a cargar productos
                $nuevoPlan = $this->plan->get_plan( $selected );
                $this->session->set_userdata('plan_id', $selected);
                $this->session->set_userdata('plan_value', floatval($nuevoPlan["base"]) - floatval( $subscription["base"] ) );
                redirect(site_url("cart"));

                $action = $this->plan->upgrade($this->user['id'],$selected);
                
                if($action === TRUE){
                        $this->session->set_flashdata('message',lang('upgrade_success'));
                }elseif($action === FALSE){
                        //fail
                        $this->session->set_flashdata('error', lang('upgrade_fail'));
                }else{
                    $errors = array(
                        -1 => 'invalid-plan',
                        -2 => 'invalid_subscription',
                        -3 => 'plan_current',
                        -4 => 'invalid-plan',
                        -5 => 'plan_donwgrade',
                        -6 => 'not-enough-funds',
                        -7 => 'payment_fail',
                    );
                    $this->session->set_flashdata('error', lang($errors[$action]));
                }
                redirect('user/upgrade');
            }
        }

        // load views
        $data['content'] = $this->load->view('user/upgrade', $content_data, TRUE);
        $this->load->view($this->template, $data);
    }


    public function cancel_upgrade(){
        $this->session->unset_userdata(["plan_id", "plan_value"]);
        redirect("Cart");
    }
    

}