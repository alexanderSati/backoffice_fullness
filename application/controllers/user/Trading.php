<?php defined('BASEPATH') OR exit('No direct script access allowed');

class Trading extends Private_Controller {


    /**
     * Constructor
     */
    function __construct()
    { 
        parent::__construct();
        
        // load the language files
        $this->load->language('trading');
        $this->load->language('users');

        //Load library
        $this->load->library("TradingLib");
        $this->load->config('pusher');
    }

    /**
     * Transactions list page
     */
     function index($symbol = null, $action = 'list'){

        $trading = new TradingLib(); 
        
        //Load BTC and coin values
        $symbol = strtoupper($symbol);
        $coin = new Currency($symbol);
        $btc_amount = 1;
        $btc_price  = $coin->price_btc;
        $btc_total  = $coin->price_btc;
        $btc_fee    = $trading->getFee($btc_total);
        
           
        // Secure validations
        if (!is_allowed('read')){
            $this->session->set_flashdata('error', privilegies_message());
            redirect('user/dashboard');
        }
        
         // Monthly payments notifications
        if(!is_active()){
            $this->session->set_flashdata('error', lang('monthly_suspended'));
            redirect('user/transactions/deposit');
        }
        
        if(!$trading->isAllowed('coin',$symbol) || !$symbol){
            $this->session->set_flashdata('error', $this->lang->line('tradig_invalid_coin'));
            redirect('user/dashboard');
        }
        
        if(!$trading->isAllowed('action',$action)){
            $this->session->set_flashdata('error', $this->lang->line('tradig_invalid_action'));
            redirect('user/dashboard');
        }
        
        if( ($action != 'list') && $this->input->post()){
            $this->_postProcess($symbol, $action);
            // Load user values from browser form 
            $btc_amount  = filter_input(INPUT_POST, 'amount', FILTER_SANITIZE_SPECIAL_CHARS);
            $btc_price  = filter_input(INPUT_POST, 'btc_price', FILTER_SANITIZE_SPECIAL_CHARS);
            $btc_total  = $btc_amount * $btc_price;
            $btc_fee    = $trading->getFee($btc_total);
        }
        
        // Template third party libraries
        $this
            ->add_css_theme( "datatables/responsive.bootstrap.min.css" )
            ->add_css_theme( "datatables/jquery.dataTables.min.css" )
            ->add_css_theme( "datatables/responsive.bootstrap.min.css" )
            ->add_js_theme( "datatables/jquery.dataTables.min.js")
            ->add_js_theme( "datatables/dataTables.bootstrap.js")
            ->add_js_theme( "datatables/dataTables.responsive.min.js")
            ->add_js_theme( "datatables/responsive.bootstrap.min.js")
            ->add_js_theme( "input_mask/jquery.inputmask.js")
            ->add_js_theme( "highcharts.js")
            ->add_js_theme( "detail.min.js")
            ->add_js_theme( "base.min.js")
            ->add_js_theme( "pages/trading.js")
            ->add_js_theme( "pusher/pusher.min.js")
            ->set_title( lang('make-trading') );
        $data = $this->includes;
        
        // Load user wallet
        $wallet = $this->wallets->get($this->user['id'],$symbol);
        
        $buy_orders = $trading->markets($symbol, 'buy');
        $sell_orders = $trading->markets($symbol, 'sell');
        $trade_history = $trading->history($symbol);

        $template_data = array(
            'page_title'    => $data['page_title'],
            'balance'       => number_format($wallet,$trading::DECIMALS),
            'symbol'        => $symbol,
            'amount'        => number_format($btc_amount,$trading::DECIMALS),
            'btc_price'     => number_format($btc_price,$trading::DECIMALS),
            'btc_total'     => number_format($btc_total,$trading::DECIMALS),
            'btc_fee'       => number_format($btc_fee,$trading::DECIMALS),
            'btc_fee_total' => number_format($btc_total,$trading::DECIMALS),
            
            'buy_orders'    => $buy_orders,
            'sell_orders'   => $sell_orders,
            'history_orders'=> $trade_history,
            // Pusher live markets
            'pusherChannel' => $symbol,
            'pusherKey' => $this->config->item('pusher_app_key'),
            'pusherCluster' => $this->config->item('pusher_app_cluster'),
            'current_fee'   => $trading::FEE,
            'current_user'  => $this->user['id'],
            'chartSymbols'  => array(
                                'BTC' => 'bitcoin',
                                'XMR' => 'monero',
                                'ETH' => 'ethereum',
                                'LTC' => 'litecoin',
                                'TRUMP'=>'trumpcoin',
                                'DASH' => 'dash',
                                'SLR' => 'solarcoin'
                            )
        );
        
        $data['content'] = $this->load->view('user/trading', $template_data, TRUE);
        $this->load->view($this->template, $data);
    }
    
    private function _postProcess($symbol = null, $action = null){
        

        if($action && $symbol){
            
            // Form validation
            $this->form_validation->set_error_delimiters($this->config->item('error_delimeter_left'), $this->config->item('error_delimeter_right'));
            $this->form_validation->set_rules('amount', lang('amount'), 'required|trim|numeric');
            $this->form_validation->set_rules('btc_price', lang('price'), 'required|trim|numeric');
            
            if ($this->form_validation->run() == TRUE)
            {
                
                $amount     = number_format(filter_input(INPUT_POST, 'amount', FILTER_SANITIZE_SPECIAL_CHARS),8);
                $btc_price  = number_format(filter_input(INPUT_POST, 'btc_price', FILTER_SANITIZE_SPECIAL_CHARS),8);
                
                $trading = new TradingLib();
                $result = $trading->publish($action, $symbol,$amount,$btc_price);
                
                if(!$result){
                    foreach($trading->errors as $error){
                        $this->session->set_flashdata('error', $error);
                    }
                }else{
                    $this->session->set_flashdata('message', $this->lang->line('tradig_published'));
                }
            }
        
        }
        
        redirect("user/trading/{$symbol}");
        
    }
    
    function get($market = null, $market_id = null){
        
        $id = filter_var($market_id, FILTER_SANITIZE_NUMBER_INT);
        
        if($market && $id){
            $trading = new TradingLib(); 
            $result = $trading->processLiveMarket($id);

                if(!$result){
                    foreach($trading->errors as $error){
                        $this->session->set_flashdata('error', $error);
                    }
                }else{
                    $this->session->set_flashdata('message', $this->lang->line('tradig_processed'));
                }
        }else{
            $this->session->set_flashdata('error', $this->lang->line('tradig_access_denied'));
            redirect('user/dashboard');
        }
        redirect("user/trading/{$market}");
    }
    
    
    function cancel($market = null, $market_id = null){
        
        $id = filter_var($market_id, FILTER_SANITIZE_NUMBER_INT);
        
        if($market && $id){
            $trading = new TradingLib(); 
            $result = $trading->unpublish($id);

                if(!$result){
                    foreach($trading->errors as $error){
                        $this->session->set_flashdata('error', $error);
                    }
                }else{
                    $this->session->set_flashdata('message', $this->lang->line('tradig_canceled'));
                }
        }else{
            $this->session->set_flashdata('error', $this->lang->line('tradig_access_denied'));
            redirect('user/dashboard');
        }
        redirect("user/trading/{$market}");
    }
    
}