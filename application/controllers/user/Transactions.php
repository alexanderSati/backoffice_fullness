<?php defined('BASEPATH') OR exit('No direct script access allowed');

class Transactions extends Private_Controller {


    /**
     * Constructor
     */
    function __construct()
    { 
        parent::__construct();
        // load the language files
        $this->lang->load('users');
        $this->lang->load('dashboard');
        $this->lang->load('transactions');
        $this->lang->load('wallets');

        // load the models
        $this->load->model('users_model');
        $this->load->model('wallets_model','wallets');
        $this->load->model('transactions_model');
        $this->load->model('currencies_model');
        $this->load->library('transaction');
        $this->load->library('currency');
        $this->load->library('wallet');

        $this->load->model('Shopping_order_model');
        
        $this->load->model('subscriptions_model','subscriptions');
        $this->load->library('plan');
        
        $this->load->language('transactions');
        $this->wallets->labels = array(
            "USD"=>"Bitcoin", 
            "BTC"=>"Bitcoin (Operative)", 
            "XMR"=>"Monero", 
            "ETH"=>"Ethereum", 
            "LTC"=>"Litecoin", 
            "TRUMP"=>"Trumpcoin", 
            "DASH"=>"Dash", 
            "SLR"=>"Solarcoin", 
        );

    }


    /**************************************************************************************
     * PUBLIC FUNCTIONS
     **************************************************************************************/


    /**
     * Transactions list page
     */
    function index()
    {
        if (!is_allowed('read')){
            $this->session->set_flashdata('error', privilegies_message());
            redirect('user/dashboard');
        }

        // setup page header data
		$this
            ->add_root_js( "bower_components/datatables.net/js/jquery.dataTables.min.js" )
            ->add_root_js( "assets/pages/data-table/js/data-table-custom.js" )
            ->add_root_js( "bower_components/datatables.net-bs4/js/dataTables.bootstrap4.min.js" )            
            ->add_root_css( "bower_components/datatables.net-bs4/css/dataTables.bootstrap4.min.css" ) 

            ->add_root_js( "js/flot/jquery.flot.js" )
            ->add_root_js( "js/flot/jquery.flot.time.min.js" )
            ->add_root_js( "js/flot/date.js" )

			->set_title( lang('users title transactions') );

        $data = $this->includes;

        // set content data
        $content_data = array(
            'page_title'    => $data['page_title'],
            'transactions'  => $this->transaction->user($this->user['id']),
            'billetera'     => $this->wallet->get($this->user['id'], "USD"),

        );


        // load views
        $data['content'] = $this->load->view('user/transactions/transactions', $content_data, TRUE);
        $this->load->view($this->template, $data);
    }
    
    /**
     * Transactions transfer page
     */
    function transfer()
    {

        
        $active_suscription = $this->users_model->special_date_validation($_SESSION['logged_in']['id']);
        $active_suscription = true;
        if (!is_allowed('read') || $active_suscription == FALSE){
            $msg = privilegies_message();
            if( $active_suscription == FALSE ){
                $msg = lang('inactive_binary');
            }
            $this->session->set_flashdata('error', $msg);
            redirect('user/dashboard');
        }
        
        // Monthly payments notifications
        // if(!is_active()){
        //     $this->session->set_flashdata('error', lang('monthly_suspended'));
        //     redirect('user/transactions/deposit');
        // }
        
        


        // validators
        $this->form_validation->set_error_delimiters($this->config->item('error_delimeter_left'), $this->config->item('error_delimeter_right'));
        $this->form_validation->set_rules('id_user', lang('users input user'), 'required|trim');
        $this->form_validation->set_rules('amount', lang('users input amount'), 'required|trim|numeric|callback__negative_amount');
        $this->form_validation->set_rules('wallet', lang('users input wallet'), 'required|trim');
        $this->form_validation->set_rules('security-pin', lang('security-pin'), 'exact_length[4]|required|trim');

        
        if ($this->form_validation->run() == TRUE)
        {

            if($this->settings->end_of_the_month == 1){
                $this->session->set_flashdata('error', lang('end_of_the_month_message'));
                redirect('user/transactions/transfer');
            }
            if(is_active($this->user['id'])){
                if ($this->users_model->second_password($this->user['id'],$this->input->post('security-pin'))) {

                    // make a transfer
                    $saved = $this->transaction->transfer_u2u($this->user['id'], $this->input->post('id_user'), $this->input->post('amount'), $this->input->post('wallet') );

                    switch ($saved) {
                        case ($saved > 0):
                            $this->session->set_flashdata('message', lang('transfer-success'));
                            break;
                        case "-1":
                            $this->session->set_flashdata('error', lang('invalid-user'));
                            break;
                        case "-2":
                            $this->session->set_flashdata('error', lang('invalid-amount'));
                            break;
                        case "-4":
                            $this->session->set_flashdata('error', lang('not-enough-funds'));
                            break;
                        default:
                            $this->session->set_flashdata('error', lang('invalid-transfer'));
                            break;
                    }

                } else {
                    $this->session->set_flashdata('error', lang('second_password'));
                }
            }
            else {
                $this->session->set_flashdata('error', lang('users error is_not_active_transfer'));
                redirect('user/transactions/transfer');
            }

            // reload page and display message
            redirect('user/transactions/transfer');
        }

        // setup page header data
        $this
            ->add_root_js( "bower_components/datatables.net/js/jquery.dataTables.min.js" )
            ->add_root_js( "assets/pages/data-table/js/data-table-custom.js" )
            ->add_root_js( "bower_components/datatables.net-bs4/js/dataTables.bootstrap4.min.js" )
            ->add_root_css( "bower_components/datatables.net-bs4/css/dataTables.bootstrap4.min.css" )
        
            ->add_js_theme( "pages/transactions.js")

            ->set_title( lang('users title transfer') );

        $data = $this->includes;
        // set content data
        $content_data = array(
            'page_title'    => $data['page_title'],
            'transactions'  => $this->transaction->user($this->user['id'], "transfer"),
            'saldo' => $this->wallet->get($this->user['id'], 'USD')
        );
    
    
        if($this->settings->end_of_the_month == 1){
            $this->session->set_flashdata('error', lang('end_of_the_month_message'));
        }
        // load views
        $data['content'] = $this->load->view('user/transactions/transfer', $content_data, TRUE);
        $this->load->view($this->template, $data);
    }

    /**
     * Transactions deposit page
     */
    function deposit()
    {
        if (!is_allowed('read')){
            $this->session->set_flashdata('error', privilegies_message());
            redirect('user/dashboard');
        }

        // validators
        $this->form_validation->set_error_delimiters($this->config->item('error_delimeter_left'), $this->config->item('error_delimeter_right'));
        $this->form_validation->set_rules('amount', lang('users input last_name'), 'required|trim|numeric');

        if ($this->form_validation->run() == TRUE && is_active())
        {
            //ramdom wallet
            $wallet_1 = $this->settings->wallet_1;
            $wallet_2 = $this->settings->wallet_2;
            $wallet = "";
            if(!empty($wallet_1) || !empty($wallet_2)){
                if(!empty($wallet_1) && !empty($wallet_2)){
                    $wallets = array($wallet_1, $wallet_2);
                    $ramdom = array_rand($wallets, 1);
                    $wallet = $wallets[$ramdom];
                }else{
                    $wallet = !empty($wallet_1) ? $wallet_1 : $wallet_2;
                }
            }
            
            // make deposit
            $saved = $this->transaction->btc_deposit($this->user['id'], $this->input->post('amount'), $wallet);
            
            $bitcoins = $this->currency->btc($this->input->post('amount'));
            
            $qr_code = "https://blockchain.info/qr?data=bitcoin:{$wallet}?amount={$bitcoins}";

            switch ($saved) {
                case ($saved > 0):
                    $this->session->set_flashdata('message', lang('deposit-success'));
                    $transaction_id = $saved;
                    break;
                case "-1":
                    $this->session->set_flashdata('error', lang('invalid-amount'));
                    break;
                case "-2":
                    $this->session->set_flashdata('error', lang('invalid-user'));
                    break;
                default:
                    $this->session->set_flashdata('error', lang('invalid-deposit'));
                    break;
            }

            // reload page and display message
//            redirect('user/transactions/deposit');
        }

        // setup page header data
        $this
            ->add_css_theme( "datatables/responsive.bootstrap.min.css" )
            ->add_css_theme( "datatables/jquery.dataTables.min.css" )
            ->add_css_theme( "datatables/responsive.bootstrap.min.css" )

            ->add_js_theme( "datatables/jquery.dataTables.min.js")
            ->add_js_theme( "datatables/dataTables.bootstrap.js")
            ->add_js_theme( "datatables/dataTables.responsive.min.js")
            ->add_js_theme( "datatables/responsive.bootstrap.min.js")
            ->add_js_theme( "pages/transactions.js")

            ->set_title( lang('users title deposit') );

        $data = $this->includes;
        
        // set content data
        $content_data = array(
            'page_title'    => $data['page_title'],
            'transactions'  => $this->transaction->user($this->user['id'], ["'deposit'","'monthly'"]),
            'qr_code'       => isset($qr_code) ? $qr_code : "",
            'transaction_id'=> isset($transaction_id) ? $transaction_id : null,
            'wallet'        => isset($wallet) ? $wallet : null,
            'type_deposit'  => 'deposit'
        );
        
        // Si esta leyendo esto le informo que no quede muy contento con este codigo, pero fue algo que se tuvo que sacar ASAP.
        // Y que esto no tiene niguna buena practica de programacion.
        if(!is_active()){

            $params = array(
                'id_user' => $this->user['id'],
                'reference' => date('m/Y'),
                'type' => 'monthly',
                'status' => 'waiting',
            );

            $monthly = $this->transactions_model->get_where_row($params, 'id', 'DESC');

            if($monthly){
                $this->session->unmark_flash('error');
                $link = str_replace("<a href='/user/transactions/deposit'>", "<a data-toggle='modal' href='/user/transactions/depositQR/{$monthly['id']}/deposit' data-target='#modal_qr'>",$this->lang->line('monthly_suspended'));
                $this->session->set_flashdata('error', $link);
            }
        }

        // load views
        $data['content'] = $this->load->view('user/transactions/deposit', $content_data, TRUE);
        $this->load->view($this->template, $data);
    }
    
    /**
     * Transactions coin deposit page
     */
    function coin_deposit()
    {
        if (!is_allowed('read') || !is_active()){
            $this->session->set_flashdata('error', privilegies_message());
            redirect('user/dashboard');
        }

        // validators
        $this->form_validation->set_error_delimiters($this->config->item('error_delimeter_left'), $this->config->item('error_delimeter_right'));
        $this->form_validation->set_rules('amount', lang('users input last_name'), 'required|trim|numeric');
        $this->form_validation->set_rules('amount_final', lang('users input last_name'), 'required|trim|numeric');

        if ($this->form_validation->run() == TRUE)
        {
            
            if($this->input->post('type') == "BTC"){
                //ramdom wallet
                $wallet_1 = $this->settings->wallet_1;
                $wallet_2 = $this->settings->wallet_2;
                $wallet = "";
                if(!empty($wallet_1) || !empty($wallet_2)){
                    if(!empty($wallet_1) && !empty($wallet_2)){
                        $wallets = array($wallet_1, $wallet_2);
                        $ramdom = array_rand($wallets, 1);
                        $wallet = $wallets[$ramdom];
                    }else{
                        $wallet = !empty($wallet_1) ? $wallet_1 : $wallet_2;
                    }
                }
                
                $bitcoins = $this->input->post('amount');
                $qr_code = "https://blockchain.info/qr?data=bitcoin:{$wallet}?amount={$bitcoins}";
                
            } else{
                $name_wallet = "wallet_" . strtolower($this->input->post('type'));
                $wallet = $this->settings->$name_wallet;
                
                $coin = strtolower($this->wallets->labels[$this->input->post('type')]);
                $qr_code = "https://chart.googleapis.com/chart?chs=300x300&cht=qr&chl={$coin}:{$wallet}";
            }
            
            // make deposit
            $saved = $this->transaction->coin_deposit($this->user['id'], $this->input->post('amount'), $this->input->post('type'), $this->input->post('amount_final'), $this->input->post('type_final'), $wallet);
            
            switch ($saved) {
                case ($saved > 0):
                    $this->session->set_flashdata('message', lang('deposit-success'));
                    $transaction_id = $saved;
                    break;
                case "-1":
                    $this->session->set_flashdata('error', lang('invalid-amount'));
                    break;
                case "-2":
                    $this->session->set_flashdata('error', lang('invalid-user'));
                    break;
                default:
                    $this->session->set_flashdata('error', lang('invalid-deposit'));
                    break;
            }

            // reload page and display message
//            redirect('user/transactions/deposit');
        }

        // setup page header data
        $this
            ->add_css_theme( "datatables/responsive.bootstrap.min.css" )
            ->add_css_theme( "datatables/jquery.dataTables.min.css" )
            ->add_css_theme( "datatables/responsive.bootstrap.min.css" )

            ->add_js_theme( "datatables/jquery.dataTables.min.js")
            ->add_js_theme( "datatables/dataTables.bootstrap.js")
            ->add_js_theme( "datatables/dataTables.responsive.min.js")
            ->add_js_theme( "datatables/responsive.bootstrap.min.js")
            ->add_js_theme( "pages/transactions.js")

            ->set_title( lang('users title deposit') );

        $data = $this->includes;
        
        // set content data
        $content_data = array(
            'page_title'    => $data['page_title'],
            'transactions'  => $this->transaction->user($this->user['id'], "coin-deposit"),
            'qr_code'       => isset($qr_code) ? $qr_code : "",
            'transaction_id'=> isset($transaction_id) ? $transaction_id : null,
            'wallet'        => isset($wallet) ? $wallet : null,
            'type_deposit'  => 'coin_deposit'
        );

        // load views
        $data['content'] = $this->load->view('user/transactions/coin_deposit', $content_data, TRUE);
        $this->load->view($this->template, $data);
    }

    /**
     * Transactions deposit page
     */
    function payout()
    {
        // $active_suscription = $this->users_model->special_date_validation($_SESSION['logged_in']['id']);
        // if (!is_allowed('read') || $active_suscription == FALSE){
        //     $msg = privilegies_message();
        //     if( $active_suscription == FALSE ){
        //         $msg = 'Each investor needs to activate its binary in order to be able to make transfers and to request payouts;';
        //     }
        //     $this->session->set_flashdata('error', $msg);
        //     redirect('user/dashboard');
        // }
       
        $compras = $this->Shopping_order_model->get_total_purchase_user($this->user['id']);
        $active_suscription = $this->subscriptions->is_active($this->user['id']);
        $day = getdate();
        // Monthly payments notifications
        if(!$active_suscription ){
            $this->session->set_flashdata('error', lang('users error payout_by_active'));
            redirect('user/dashboard');
        }elseif($compras < 220) {
            $this->session->set_flashdata('error', lang('users error missing_purchases'));
            redirect('user/dashboard');
        }
        elseif($day['mday'] > 8){
            $this->session->set_flashdata('error', lang('users error days_passed'));
            redirect('user/dashboard');
        }

        // validators
        $this->form_validation->set_error_delimiters($this->config->item('error_delimeter_left'), $this->config->item('error_delimeter_right'));
        $this->form_validation->set_rules('wallet', lang('users input wallet'), 'required|trim');
        $this->form_validation->set_rules('amount', lang('users input amount'), 'required|trim|numeric|callback__negative_amount');
        $this->form_validation->set_rules('security-pin', lang('security-pin'), 'min_length[4]|required|trim');
        
        if ($this->form_validation->run() == TRUE)
        {
            if($this->settings->end_of_the_month == 1){
                $this->session->set_flashdata('error', lang('end_of_the_month_message'));
                redirect('user/transactions/payout');
            }
            if ($this->users_model->second_password($this->user['id'],$this->input->post('security-pin'))) {

                // make a transfer
                $saved = $this->transaction->payout($this->user['id'], 'USD', $this->input->post('amount'));

                switch ($saved) {
                    case ($saved > 0):
                        $this->session->set_flashdata('message', lang('payout-success'));
                        break;
                    case "-1":
                        $this->session->set_flashdata('error', lang('invalid-amount-payout'));
                        break;
                    case "-2":
                        $this->session->set_flashdata('error', lang('invalid-user'));
                        break;
                    case "-3":
                        $this->session->set_flashdata('error', lang('invalid-wallet'));
                        break;
                    case "-4":
                        $this->session->set_flashdata('error', lang('payout-limit'));
                        break;
                    case "-5":
                        $this->session->set_flashdata('error', lang('enough-funds'));
                        break;
                    default:
                        $this->session->set_flashdata('error', lang('invalid-payout'));
                        break;
                }

            } else {
                $this->session->set_flashdata('error', lang('second_password'));
            }

            // reload page and display message
            redirect('user/transactions/payout');
        }

        // setup page header data
        $this
            // ->add_css_theme( "datatables/responsive.bootstrap.min.css" )
            // ->add_css_theme( "datatables/jquery.dataTables.min.css" )
            // ->add_css_theme( "datatables/responsive.bootstrap.min.css" )

            // ->add_js_theme( "datatables/jquery.dataTables.min.js")
            // ->add_js_theme( "datatables/dataTables.bootstrap.js")
            // ->add_js_theme( "datatables/dataTables.responsive.min.js")
            // ->add_js_theme( "datatables/responsive.bootstrap.min.js")
            ->add_root_js( "assets/pages/form-masking/inputmask.js")
            ->add_root_js( "assets/pages/form-masking/jquery.inputmask.js")
            ->add_root_js( "assets/pages/form-masking/autoNumeric.js")
            ->add_root_js( "assets/pages/form-masking/form-mask.js")            
            // ->add_js_theme( "pages/transactions.js")

            ->set_title( lang('users title payout') );

        $data = $this->includes;

        // Get user wallets from database
        $user_wallets = $this->wallets->get($this->user['id']);
        
          
        $wallets = array(
            'USD' => array(
                'label' => "BTC",
                'coins' => $this->currency->load('USD')->btc($user_wallets['USD']),
                'usd'   => $user_wallets['USD']
            ),
            'XMR' => array(
                'label' => "XMR",
                'coins' => $user_wallets['XMR'],
                'usd'   => $this->currency->load('XMR')->usd($user_wallets['XMR'])
            ),
            'ETH' => array(
                'label' => "ETH",
                'coins' => $user_wallets['ETH'],
                'usd'   => $this->currency->load('ETH')->usd($user_wallets['ETH'])
            ),
            'LTC' => array(
                'label' => "LTC",
                'coins' => $user_wallets['LTC'],
                'usd'   => $this->currency->load('LTC')->usd($user_wallets['LTC'])
            ),
            'TRUMP'=> array(
                'label' => "TRUMP",
                'coins' => $user_wallets['TRUMP'],
                'usd'   => $this->currency->load('TRUMP')->usd($user_wallets['TRUMP'])
            ),
            'DASH' => array(
                'label' => "DASH",
                'coins' => $user_wallets['DASH'],
                'usd'   => $this->currency->load('DASH')->usd($user_wallets['DASH'])
            ),
            'SLR' => array(
                'label' => "SLR",
                'coins' => $user_wallets['SLR'],
                'usd'   => $this->currency->load('SLR')->usd($user_wallets['SLR'])
            ),
        );
        
        $subscription = $this->subscriptions->get_subscription($this->user['id']);

        // set content data
        $content_data = array(
            'page_title'    => $data['page_title'],
            'transactions'  => $this->transaction->user($this->user['id'], "payout"),
            'wallets'       => $wallets,
            'allowed'       => $this->transaction->is_payout_available($this->user['id']),
            'allowed_date'  => $this->transaction->is_payout_available($this->user['id'],true),
            'subscription'  => $subscription,
            'plan'          => $this->plan->get_plan($subscription['id_plan'])
        );

        if($this->settings->end_of_the_month == 1){
            $this->session->set_flashdata('error', lang('end_of_the_month_message'));

        }

        // load views
        $data['content'] = $this->load->view('user/transactions/payout', $content_data, TRUE);
        $this->load->view($this->template, $data);
    }

    function ajax($request = null, $param1 = null, $param2 = null, $param3 = null){
        if (!$this->input->is_ajax_request()) {
            // No direct script access allowed
            exit;
        }

        switch ($request) {
            case "saerch-user" :

                $user = $this->users_model->get_user($param1);

                $response = array();
                if($user && $user['id'] != $this->user['id']){
                    $response['data'] = $user;
                    $response['message'] = "success";
                } else {
                    $response['data'] = lang("no-found-username");
                    $response['message'] = "error";
                }

                echo json_encode($response);

                break;

            case "convert-price" :

                $this->currency->load($param2);
                echo $this->currency->usd($param1);
                break;
            
            case "convert-price-coin" :

                $this->currency->load($param2);
                $amount_usd = $this->currency->usd($param1);
                
                $this->currency->load($param3);
                echo $this->currency->btc($amount_usd) * (1 - Transaction::COIN_DEPOSIT_FEE/100);
                break;

            case "convert-price-btc" :

                $this->currency->load("USD");
                echo $this->currency->btc($param1);
                break;

            case "request-token" :

                $token = $this->token->send($this->user['id']);
                
                if($token){
//                    $message = lang("send-token"). ". " . sprintf(lang("show-token"), $token);
                    $message = lang("send-token");
                    echo $message;
                }
                
                break;
                
            case "calc-fee":
                
                $refl = new ReflectionClass('Transaction');
                $percent = $refl->getConstant("TRANSFER_U2U_FEE");
                $fee = ($param1 * ($percent/100));
                echo json_encode(array('fee' => $fee, 'percent' => $percent));
                break; 
            
            default :
                break;
        }
    }
    
    function wallets(){
        
        $active_suscription = $this->users_model->special_date_validation($_SESSION['logged_in']['id']);
        if ($active_suscription == FALSE){
            $this->session->set_flashdata('error', $this->lang->line('inactive_binary'));
            redirect('user/dashboard');
        }
        
        // Monthly payments notifications
        if(!is_active()){
            $this->session->set_flashdata('error', lang('monthly_suspended'));
            redirect('user/transactions/deposit');
        }
        
        // setup page header data
        $this->add_css_theme( "datatables/responsive.bootstrap.min.css" )
        ->add_css_theme( "datatables/jquery.dataTables.min.css" )
        ->add_css_theme( "datatables/responsive.bootstrap.min.css" )

        ->add_js_theme( "datatables/jquery.dataTables.min.js")
        ->add_js_theme( "datatables/dataTables.bootstrap.js")
        ->add_js_theme( "datatables/dataTables.responsive.min.js")
        ->add_js_theme( "datatables/responsive.bootstrap.min.js")
        ->add_js_theme( "pages/transactions.js")
        ->set_title( lang('users title wallets') );
        
        $data = $this->includes;
                
        // validators
        $this->form_validation->set_error_delimiters($this->config->item('error_delimeter_left'), $this->config->item('error_delimeter_right'));
        $this->form_validation->set_rules('amount', lang('users input amount'), 'required|trim|numeric');
        $this->form_validation->set_rules('wallet_from', lang('invalid_wallet_from'), "required|trim");
        $this->form_validation->set_rules('wallet_to', lang('invalid_wallet_to'), "required|trim");
        $this->form_validation->set_rules('security-pin', lang('security-pin'), 'min_length[4]|required|trim');

        if ($this->form_validation->run() == TRUE)
        {      
           if ($this->users_model->second_password($this->user['id'],$this->input->post('security-pin'))) {
                
            // make a transfer
                $saved = $this->transaction->transfer_btw_wallets(
                        $this->user['id'], 
                        $this->input->post('wallet_from'), 
                        $this->input->post('wallet_to'), 
                        $this->input->post('amount')
                );
                                
                switch ($saved) {
                    case ($saved === TRUE):
                        $this->session->set_flashdata('message', lang('transfer-success'));
                    case -1:
                        $this->session->set_flashdata('error', lang('invalid-amount'));
                        break;
                    case -2:
                        $this->session->set_flashdata('error', lang('invalid-user'));
                        break;
                    case -3:
                        $this->session->set_flashdata('error', lang('invalid-wallet'));
                        break;
                    case -4:
                        $this->session->set_flashdata('error', lang('invalid_btc_transfer'));
                        break;
                    case -5:
                        $this->session->set_flashdata('error', lang('enough-funds'));
                        break;
                    default:
                        $this->session->set_flashdata('error', lang('invalid-transfer'));
                        break;
                }

            } else {
                $this->session->set_flashdata('error', lang('second_password'));
            }

            // reload page and display message
            redirect('user/transactions/wallets');
        }
        
        // Get user wallets from database
        $user_wallets = $this->wallets->get($this->user['id']);
          
        $wallets = array(
            'USD' => array(
                'label' => "BTC",
                'coins' => $this->currency->load('USD')->btc($user_wallets['USD']),
                'usd'   => $user_wallets['USD']
            ),
            'BTC' => array(
                'label' => "BTC (Operative)",
                'coins' => $this->currency->load('USD')->btc($user_wallets['BTC']),
                'usd'   => $user_wallets['BTC']
            )
        );
        
        // set content data
        $content_data = array(
            'page_title'    => $data['page_title'],
            //'transactions'  => $this->transaction->user($this->user['id']),
            'wallets'  => $wallets,
        );

        // load views
        $data['content'] = $this->load->view('user/transactions/wallets', $content_data, TRUE);
        $this->load->view($this->template, $data);
    
    }
    
    public function depositQR($id, $type_deposit = null, $status = null, $hash = null) {
        
        $valid_status = array(
            "save"      => "pending",
            "cancel"    => "cancelled"
        );
        
        $hashed = strip_tags(filter_var($hash,FILTER_SANITIZE_STRING));
        $hashed = substr(str_replace("%20", "", $hashed) ,0,100);

        if($id){
            $transaction = $this->transactions_model->get_row($id);
            if(array_key_exists($status, $valid_status)){ 
                $this->transactions_model->update($transaction['id'], array('status' => $valid_status[$status],'hash' => ($hash) ? $hashed : null));
                if($status == "save"){
                    $user = $this->users_model->get_user($transaction['id_user']);
                    $data = array(
                        'transaction' => $transaction['id'],
                        'date' => date('Y-m-d H:i'),
                        'name' => "{$user['first_name']} {$user['last_name']}",
                        'username' => $user['username'],
                        'email'  => $user['email'],
                        'usd'    => $transaction['usd'],
                        'wallet' => $transaction['wallet'],
                        'btc'    => $transaction['btc'],
                        'description' => $transaction['description']
                     );
                        
                    $type_deposit == "deposit" ? $this->transaction->_sendEmail(1,'btc_deposit_admin',$data) : $this->transaction->_sendEmail(1,'coin_deposit_admin',$data);
                }
                redirect('user/transactions/'. $type_deposit);
            }
            if(!$status){
                $transaction['type_deposit'] = $type_deposit ? $type_deposit : '';
                $transaction['link_qr'] = $type_deposit == "coin_deposit" ? "https://chart.googleapis.com/chart?chs=300x300&cht=qr&chl=" : null;
                $this->load->view('user/transactions/modalQR', $transaction);
            }
        }else{
            show_404();
        }
    }
    function _negative_amount($amount)
    {
        if ($amount <= 0) {
        
            $this->form_validation->set_message('_negative_amount', sprintf(lang('users error number_negative')));
            return FALSE;
        } else {
            return $amount;
        }
        
    }
}