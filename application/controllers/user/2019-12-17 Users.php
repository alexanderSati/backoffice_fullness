<?php defined('BASEPATH') OR exit('No direct script access allowed');

class Users extends Private_Controller {


    /**
     * Constructor
     */
    function __construct()
    {
        parent::__construct();

        // load the language files
        $this->lang->load('users');

        // load the users model
        $this->load->model('users_model');
        $this->load->model('profiles_model');
        $this->load->model('plans_model');
        $this->load->model('subscriptions_model','subscriptions');
        $this->load->model('locations_model','locations');
        
        // upload config
        $config['upload_path']          = "uploads/";
        $config['allowed_types']        = 'gif|jpg|png';
        $config['max_size']             = 0;
        $config['max_width']            = 1024;
        $config['max_height']           = 1024;

        $this->load->library('upload', $config);
        $this->load->library('wallet');
        $this->load->library('transaction');
        $this->load->library('referral');
        $this->load->library('binarytree');
        $this->load->library('rank');
    }


    /**************************************************************************************
     * PUBLIC FUNCTIONS
     **************************************************************************************/


    /**
     * New user from user
     */
    function index()
    {
        if (!is_allowed('read')){
            $this->session->set_flashdata('error', privilegies_message());
            redirect('user/users');
        }

        if (is_preferencial_user()) {
            $this->session->set_flashdata('error', lang('users error is_preferencial_user'));
            redirect('user/dashboard');
        }
        if(!is_active($this->user['id'])){
            $this->session->set_flashdata('error', lang('users error is_not_active_add'));
            redirect('user/dashboard');
        }

        // validators
        $this->form_validation->set_error_delimiters($this->config->item('error_delimeter_left'), $this->config->item('error_delimeter_right'));
        $this->form_validation->set_rules('doc_type', lang('doc_type_lbl'), 'required|trim');
        $this->form_validation->set_rules('username', lang('users input username'), 'required|trim|min_length[5]|max_length[30]|callback__check_username[]');
        $this->form_validation->set_rules('first_name', lang('users input first_name'), 'required|trim|min_length[2]|max_length[32]');
        $this->form_validation->set_rules('last_name', lang('users input last_name'), 'required|trim|min_length[2]|max_length[32]');
        $this->form_validation->set_rules('email', lang('users input email'), 'required|trim|max_length[128]|valid_email');
        $this->form_validation->set_rules('dni', lang('users input dni'),'required|numeric|callback__check_dni_exists',["_check_dni_exists"=>lang("form_validation_exist")]);
        $this->form_validation->set_rules('birthdate', lang('users input birthdate'), 'required');
        $this->form_validation->set_rules('phone', lang('users input phone'), 'required|min_length[5]');
        $this->form_validation->set_rules('address', lang('users input address'), 'required|min_length[5]');
        $this->form_validation->set_rules('address2', lang('users input address2'), 'min_length[5]');
        $this->form_validation->set_rules('city', lang('users input city'), 'required');
        $this->form_validation->set_rules('department', lang('users input department'), 'required');
        $this->form_validation->set_ruleS('postal_code', lang('users input postal_code'), 'required|numeric');
        $this->form_validation->set_ruleS('bank_account', lang('users input bank_account'), 'required|min_length[5]');
        $this->form_validation->set_rules('language', lang('users input language'), 'required|trim');
        $this->form_validation->set_rules('password', lang('users input password'), 'required|trim|min_length[5]');
        $this->form_validation->set_rules('password_repeat', lang('users input password_repeat'), 'required|trim|matches[password]');
        $this->form_validation->set_rules('country', lang('users input country'), 'required|alpha');
        $this->form_validation->set_rules('terms', lang('users input terms'), 'required');


        if ($this->form_validation->run() == TRUE)
        {
            if($this->settings->end_of_the_month == 1){
                $this->session->set_flashdata('error', lang('end_of_the_month_message'));
                redirect(site_url('user/create'));
            }
            $data = $this->input->post();
            
            //Upload image
            $data['image'] = '';
            if (!$this->upload->do_upload('image'))
            {     
                if($_FILES['image']['error'] != 4)
                {       
                $this->session->set_flashdata('error', $this->upload->display_errors());
                redirect(site_url('user/create'));
                }
            }
            else
            {
                $file_data = array('upload_data' => $this->upload->data());
                $data['image'] = $file_data['upload_data']['file_name'];
            }

                $data['status'] = '1';
                $data['plan'] = '9';
        

                // save the new user
                $saved = $this->users_model->add_user($data);

                if ($saved)
                {
                    // Create a subscription
                    $data = $this->input->post();
                    $data['id_user'] = $saved;
                    $data['status'] = '1';
                    $data['plan'] = '9';



                    $this->subscriptions->add($data);
                    // Create a new user wallet
                    $this->wallet->create($data['id_user']);
                    // Create user binary tree
                    $this->binarytree->create($data['id_user'],$data['plan']);
                    // Create referral record
                    $this->referral->create($this->user['id'],$data['id_user'],$data['plan'], $data['side']);                    
                    // Create initial rank
                    $this->rank->create($data['id_user']);


                    // active subscription
                    $this->subscriptions->activate($data['id_user']);
                    // Activate referral rules and status
                    $this->referral->activate($data['id_user']);
                    // add left/right child
                    $childs = $this->binarytree->get_childs($this->user['id']);
                    $parent = $this->binarytree->next_branch($this->user['id'],$data['side'],$childs);
                   
                    $this->binarytree->add_child($parent, $data['id_user'], $data['side']);          


                    $this->session->set_flashdata('message', sprintf(lang('users msg add_user_success'), $this->input->post('first_name') . " " . $this->input->post('last_name')));
                }
                else
                {
                    $this->session->set_flashdata('error', sprintf(lang('users error add_user_failed'), $this->input->post('first_name') . " " . $this->input->post('last_name')));
                }

            // return to list and display message
            redirect(base_url('user/create'));
        }

        // setup page header data

        $this
            ->add_css_theme("select/select2.min.css")

            ->add_js_theme( "parsley/parsley.min.js" )
            ->add_js_theme( "pages/parsley-validator.js" )
            ->add_js_theme( "select/select2.full.js" )
            ->add_js_theme( "pages/register.js" )
            ->add_js_theme( "pages/select2.js" )

            ->set_title( lang('sign-friend') );

        $data = $this->includes;

        // set content data
        $content_data = array(
            'cancel_url'        => base_url('user/dashboard'),
            'user'              => NULL,
            'password_required' => TRUE,
            'plans'             => $this->plans_model->get_active(),
            'countries'         => $this->locations->getCountries(true),
            'page_title'        => $data['page_title'],
            'only_add'          => true
        );
        
        if($this->settings->end_of_the_month == 1){
            $this->session->set_flashdata('error', lang('end_of_the_month_message'));
        }

        // load views
        $data['content'] = $this->load->view('user/users_form', $content_data, TRUE);
        $this->load->view($this->template, $data);
    }

    public function _check_dni_exists($dni){

        $result = $this->users_model->get_all(1, 0,["dni"=>$dni, "status"=>"1"]);
        log_message("DEBUG", "INFO ".var_export($result, true). " -> ".($result["total"] == 0));
        return $result["total"] == 0;
    }

    /**************************************************************************************
     * PRIVATE VALIDATION CALLBACK FUNCTIONS
     **************************************************************************************/


    /**
     * Make sure username is available
     *
     * @param  string $username
     * @param  string|null $current
     * @return int|boolean
     */
    function _check_username($username, $current)
    {
        if (trim($username) != trim($current) && $this->users_model->username_exists($username))
        {
            $this->form_validation->set_message('_check_username', sprintf(lang('users error username_exists'), $username));
            return FALSE;
        }
        else
        {
            return $username;
        }
    }


    /**
     * Make sure email is available
     *
     * @param  string $email
     * @param  string|null $current
     * @return int|boolean
     */
    function _check_email($email, $current)
    {
        if (trim($email) != trim($current) && $this->users_model->email_exists($email))
        {
            $this->form_validation->set_message('_check_email', sprintf(lang('users error email_exists'), $email));
            return FALSE;
        }
        else
        {
            return $email;
        }
    }

}