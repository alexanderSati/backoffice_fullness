<?php defined('BASEPATH') OR exit('No direct script access allowed');

class Faqs extends Private_Controller {

    /**
     * Constructor
     */
    function __construct()
    {
        parent::__construct();

        $this->load->model('faqs_model');
        $this->lang->load('faqs');
    }


    /**
     * Settings Editor
     */
    function index()
    {
        
        if (!is_allowed('read')){
            $this->session->set_flashdata('error', privilegies_message());
            redirect('user/dashboard');
        }

        $lang = (isset($user['language']) ? $user['language'] : $this->config->item('language'));

        // get list
        $faqs = $this->faqs_model->get_active(null, 'id');

        // setup page header data
        $this
            ->set_title( lang('faqs') );

        $data = $this->includes;

        // set content data
        $content_data = array(
            'faqs'      => $faqs,
            'page_title' => $data['page_title'],
            'lang'       => $lang
        );

        // load views
        $data['content'] = $this->load->view('user/faqs', $content_data, TRUE);
        $this->load->view($this->template, $data);
    }

}
