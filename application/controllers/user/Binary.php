<?php defined('BASEPATH') OR exit('No direct script access allowed');



class Binary extends Private_Controller {



    private $_defaul_tree;





    /**

     * Constructor

     */

    function __construct()

    {

        parent::__construct();



        // load the language files

        $this->lang->load('users');

        $this->lang->load('dashboard');



        $this->load->model('plans_model','plans');

        $this->load->model('users_model');

        $this->load->model('subscriptions_model','subscriptions');



        $this->load->library('binarytree');



        $this->_defaul_tree = array(

            array(

                'image' => '/uploads/user-anonymous-disabled-2.gif',

                'text' => array(

                    'name' => 'Available',

                    'desc' => 'Plan'

                ),

                'children' => array(

                    array(

                        'image' => '/uploads/user-anonymous-disabled-2.gif',

                        'text' => array(

                            'name' => 'Available',

                            'desc' => 'Plan'

                        ),

                    ),

                    array(

                        'image' => '/uploads/user-anonymous-disabled-2.gif',

                        'text' => array(

                            'name' => 'Available',

                            'desc' => 'Plan'

                        ),

                    )

                )

            ),

            array(

                'image' => '/uploads/user-anonymous-disabled-2.gif',

                'text' => array(

                    'name' => 'Available',

                    'desc' => 'Plan'

                ),

                'children' => array(

                    array(

                        'image' => '/uploads/user-anonymous-disabled-2.gif',

                        'text' => array(

                            'name' => 'Available',

                            'desc' => 'Plan'

                        ),

                    ),

                    array(

                        'image' => '/uploads/user-anonymous-disabled-2.gif',

                        'text' => array(

                            'name' => 'Available',

                            'desc' => 'Plan'

                        ),

                    )

                )

            )

        );



    }





    /**************************************************************************************

     * PUBLIC FUNCTIONS

     **************************************************************************************/





    /**

     * Binary list page

     */

    function index()
    {
        if (!is_allowed('read')){
            $this->session->set_flashdata('error', privilegies_message());
            redirect('user/dashboard');
        }

        // Monthly payments notifications

        // if(!is_active()){
        //     $this->session->set_flashdata('error', lang('monthly_suspended'));
        //     redirect('user/transactions/deposit');
        // }

        $lang = (isset($user['language']) ? $user['language'] : $this->config->item('language'));

        // setup page header data
		$this
            ->add_css_theme( "treant/Treant.css" )
            ->add_css_theme( "treant/collapsable.css" )
            ->add_css_theme( "treant/simple-scrollbar.css" )
            ->add_css_theme( "treant/vendor/perfect-scrollbar/perfect-scrollbar.css" )
            ->add_js_theme( "treant/vendor/raphael.js")
            ->add_js_theme( "treant/Treant.js")
            ->add_js_theme( "treant/vendor/jquery.mousewheel.js")
            ->add_js_theme( "treant/vendor/perfect-scrollbar/perfect-scrollbar.js")
            ->add_js_theme( "treant/vendor/jquery.easing.js")
            ->add_js_theme( "pages/binary.js")
			->set_title( lang('users title binary') );


        $data = $this->includes;

        $childs = $this->binarytree->get_childs($this->user['id']);
        $tree  = array_walk_multi($childs,false);

        $subscription = $this->subscriptions->get_active($this->user['id']);
        $my_plan = $this->plans->get($subscription['id_plan']);


        $image = ($this->user['image'] != "") ? $this->user['image'] : "user-anonymous-disabled.gif" ;

        $array_tree = array(
            'image' => "/uploads/". $image,
            'text' => array(
                'name' => $this->user['username'], 
                'desc' => ($my_plan["{$lang}_name"] != "") ? $my_plan["{$lang}_name"] : "Plan"
            ),
            'children' => array_replace_recursive($this->_defaul_tree, $tree),
        );

        // BONUS BINARIO
        $bonus = $this->binarytree->get_amount_liquidation_binary($this->user['id']);

        $csv = file(PATH_LIQUIDATION."/reporte_binario_".date("Y-m-t").".csv");

        foreach ($csv as $line) {
            $binary_liquidation[]=explode(",",$line);
        }

        // set content data
        $content_data = array(
            'ptos_izq'      =>$this->binarytree->get_sum_by_side($this->user['id'], 'left'),
            'ptos_der'      =>$this->binarytree->get_sum_by_side($this->user['id'], 'right'),
            'page_title'    => $data['page_title'],
            'binary'        => $array_tree,
            'stats_tree'    => $this->binarytree->get_score($this->user['id']),
            'bonus'         => $bonus,
            'binary_liquidation' => $binary_liquidation
        );

        // load views
        $data['content'] = $this->load->view('user/networks/binary', $content_data, TRUE);

        $this->load->view($this->template, $data);

    }



    function unassigned()

    {

        // setup page header data

        $this

            ->add_css_theme( "floatexamples.css" )

            ->add_css_theme( "datatables/responsive.bootstrap.min.css" )

            ->add_css_theme( "datatables/jquery.dataTables.min.css" )

            ->add_css_theme( "datatables/responsive.bootstrap.min.css" )



            ->add_js_theme( "flot/jquery.flot.js" )

            ->add_js_theme( "flot/jquery.flot.time.min.js" )

            ->add_js_theme( "flot/date.js" )

            ->add_js_theme( "datatables/jquery.dataTables.min.js")

            ->add_js_theme( "datatables/dataTables.bootstrap.js")

            ->add_js_theme( "datatables/dataTables.responsive.min.js")

            ->add_js_theme( "datatables/responsive.bootstrap.min.js")

            ->add_js_theme( "pages/binary.js" )



            ->set_title( lang('users title unassigned') );



        $data = $this->includes;



        $list_pending = array();

        $pending = $this->binarytree->get_unassigned($this->user['id']);

        foreach ($pending as $item) {

            $user_data = $this->users_model->get_user($item);

            $list_pending[$item] = $user_data['first_name']." ".$user_data['last_name'];

        }



        // set content data

        $content_data = array(

            'pending'   => $list_pending

        );



        $data['content'] = $this->load->view('user/networks/unassigned', $content_data, TRUE);



        $this->load->view($this->template, $data);

    }



    function ajax($request = null, $param1 = null, $param2 = null){

        if (!$this->input->is_ajax_request()) {

            // No direct script access allowed

            exit;

        }



        switch ($request) {

            case "change-status" :



                if ($param1 && $param2) {

                    $result = $this->binarytree->add_child($this->user['id'], $param1, $param2);



                    switch ($result) {

                        case ($result > 0):

                            $this->session->set_flashdata('message', lang('success update side'));

                            echo "success";

                            break;

                        case "-1":

                            $this->session->set_flashdata('error', lang('user-already'));

                            echo "error";

                            break;

                        case "-2":

                            $this->session->set_flashdata('error', lang('invalid-side'));

                            echo "error";

                            break;

                        case "-3":

                            $this->session->set_flashdata('error', lang('side-busy'));

                            echo "error";

                            break;

                        case "-4":

                            $this->session->set_flashdata('error', lang('parent-not'));

                            echo "error";

                            break;

                        default:

                            $this->session->set_flashdata('error', lang('error update side'));

                            echo "error";

                            break;

                    }



                } else {

                    $this->session->set_flashdata('error', lang("error update side"));

                    echo "error";

                }



                break;



            default :

                break;

        }

    }



}