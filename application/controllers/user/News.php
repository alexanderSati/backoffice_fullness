<?php defined('BASEPATH') OR exit('No direct script access allowed');

class News extends Private_Controller {

    /**
     * Constructor
     */
    function __construct()
    {
        parent::__construct();

        // load the language file
        $this->lang->load('news');

        $this->load->model('news_model');
        
    }


    /**************************************************************************************
     * PUBLIC FUNCTIONS
     **************************************************************************************/


    /**
     * News
     * 
     * @param string $category Category slug
     * @param string $post Post slug
     */
    function index($category = null, $post = null)
    {
        $lang = (isset($user['language']) ? $user['language'] : $this->config->item('language'));
        
        if ($category) {
            $category_id = $this->news_model->get_category_id($category);
        }else{
            redirect("user/dashboard");
        }
        
        if($category_id){
            
            
            if ($post) {
                $info = $this->news_model->get_post_by_slug($post);
                
                if ($info) {
                    $info->image = $this->news_model->get_featured_image($info->ID);

                    $title = $info->post_title;
                    $view = 'user/single-post';
                    
                    $data_languages['languages'] = $this->settings->{'wellcome_message'};
        
                    $message = $data_languages['languages'][$this->session->language];

                    if (!$message == null){
                        $message = $data_languages['languages'][$this->session->language];
                    }
                    
                }else {
                    $this->session->set_flashdata('error', lang('error_news'));
                    redirect("user/dashboard");
                }
            }else{
                // real category -> Bitwexchange News
                $info = $this->news_model->all_by_category_id($category_id);
                
                if ($info) {
                    foreach ($info as &$post) {
                        $post['image'] = $this->news_model->get_featured_image($post['ID']);

                        $timestamp = strtotime($post['post_date']);
                        $post['post_date'] = date("F j, Y", $timestamp);
                    }

                    $title = lang('news');
                    $view = 'user/news';
                }else{
                    $this->session->set_flashdata('error', lang('error_news'));
                    redirect("user/dashboard");
                }
            }
        }
        
        $this
            ->set_title( $title );

        $data = $this->includes;
        
        // set content data
        $content_data = array(
            'post_info'             => $info,
            'wordpress_site_url'    => $this->news_model->get_base_url_wordpress(), 
            'page_title'            => $data['page_title'],
            'lang'                  => $lang,
            'message'               => isset($message) ? $message : ""
        );
        
        // load views
        $data['content'] = $this->load->view($view, $content_data, TRUE);
        $this->load->view($this->template, $data);

    }

}