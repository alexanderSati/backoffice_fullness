<?php defined('BASEPATH') OR exit('No direct script access allowed');



class Unilevel extends Private_Controller {





    /**

     * Constructor

     */

    function __construct()

    {

        parent::__construct();



        //load library

        $this->load->library('residual');

        $this->load->library('rank');

        $this->load->library('referral');



        //load model

        $this->load->model('referrals_model');

        $this->_defaul_tree = array();

    }   





    function index(){        



        $lang = (isset($user['language']) ? $user['language'] : $this->config->item('language'));

                

        // setup page header data

		$this

            ->add_css_theme( "treant/Treant.css" )

            ->add_css_theme( "treant/collapsable.css" )

            ->add_css_theme( "treant/simple-scrollbar.css" )

            ->add_css_theme( "treant/vendor/perfect-scrollbar/perfect-scrollbar.css" )

            ->add_js_theme( "treant/vendor/raphael.js")

            ->add_js_theme( "treant/Treant.js")

            ->add_js_theme( "treant/vendor/jquery.mousewheel.js")

            ->add_js_theme( "treant/vendor/perfect-scrollbar/perfect-scrollbar.js")

            ->add_js_theme( "treant/vendor/jquery.easing.js")

            ->add_js_theme( "pages/binary.js")

            ->set_title( lang('users title unilevel') );





            $data = $this->includes;



        $unilevel = $this->residual->unilevel($this->user['id']);        

        $items = $this->referrals_model->get_items();

        $referral = $this->referrals_model->tree_unilevel($items, $this->user['id']);

        $tree = array_walk_multi_external($referral,false);   

        $referral_by_user = $this->referral->get_referrals($this->user['id']);
        

        for($i=0; $i < count($referral_by_user); $i++){
            $unilevel_by_user = $this->residual->unilevel($referral_by_user[$i]['id_user']); 
            $referral_by_user[$i]['pp']= $unilevel_by_user['pp'];
            $referral_by_user[$i]['gp']= $unilevel_by_user['gp'];
            $referral_by_user[$i]['pt']= $unilevel_by_user['gp'] + $unilevel_by_user['pp'];
            $referral_by_user[$i]['score']= $unilevel_by_user['score'];
            $referral_by_user[$i]['rank']= $unilevel_by_user['rank'];
            $referral_by_user[$i]['max_line']= $unilevel_by_user['max_line'];
            $referral_by_user[$i]['percentage_profit']= $unilevel_by_user['percentage_profit'];

        }
       // print_r($referral_by_user);

        $image = ($this->user['image'] != "") ? $this->user['image'] : "user-anonymous-disabled.gif" ;



        $array_tree = array(

            'image' => "/uploads/". $image,

            'text' => array(

                'name' => $this->user['username'],

                /*'title' =>  'PP : ' . $unilevel['pp']. " - BONUS: ". $unilevel['bonus'],*/

                'desc' => $this->rank->get_rank($this->user['id'])["name"],

                /*'contact' => array(

                    'val' => 'Max : ' . $unilevel['max_line'] .' - PG : ' . $unilevel['gp'] .' - PC : ' . $unilevel['score']

                ),*/

            ),

            #'innerHTML' => '<p>'.lang('points'). ': ' . $residual_pp.'</p><p>'.lang('points'). ': ' . $residual_pg['points'].'</p>',

            'children' => array_replace_recursive($this->_defaul_tree, $tree),

        );



        $csv = file(PATH_LIQUIDATION."/reporte_residual".date("Y-m-t").".csv");



        foreach ($csv as $line) {

            $residual_liquidation[]=explode(",",str_replace("\"","", $line));

        }



        // set content data

        $content_data = array(

            'page_title'    => $data['page_title'],

            'unilevel'      => $array_tree,

            'residual' =>  $residual,

            'residual_pg' => $unilevel['gp'],

            'residual_pp' => $unilevel['pp'],

            'max_line' => $unilevel['max_line'],

            'rank' => $this->rank->get_rank($this->user['id']),

            'bonus' => $this->residual->get_amount_liquidation_residual($this->user['id']),

            'residual_liquidation'=>$residual_liquidation,

            'referrals'=> $referral_by_user

        );

//print_r($residual_liquidation);

        // load views

        $data['content'] = $this->load->view('user/networks/unilevel', $content_data, TRUE);

        $this->load->view($this->template, $data);



    }



}