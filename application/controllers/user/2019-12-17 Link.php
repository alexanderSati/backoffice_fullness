<?php defined('BASEPATH') OR exit('No direct script access allowed');

class Link extends Private_Controller {

    /**
     * Constructor
     */
    function __construct()
    {
        parent::__construct();

        // load the language file
        $this->lang->load('users');

        // load the users model
        $this->load->model('users_model');
    }

    function index(){

        // setup page header data
        $this
                ->set_title(lang('users title link'));

        $this
                ->add_css_theme("select/select2.min.css")
                ->add_js_theme("parsley/parsley.min.js")
                ->add_js_theme("pages/parsley-validator.js")
                ->add_js_theme("select/select2.full.js")
                ->add_js_theme("pages/register.js")
                ->add_js_theme("pages/select2.js")
                ->add_js_theme("pages/profile.js");
        $data = $this->includes;

        // consult user
        $user = $this->users_model->get_user($this->user['id']);

        // set content data
        $content_data = array(           
            'page_title' => $data['page_title'],
            'user' => $user
        );

        if(!is_active($this->user['id']) ){
            $this->session->set_flashdata('error', lang("no_active_user_refer"));
            redirect("user/dashboard");
        }

        if(is_preferencial_user()){
            $this->session->set_flashdata('error', lang('users error is_preferencial_user'));
            redirect('user/dashboard');
        }

        $data['content'] = $this->load->view('user/link', $content_data, TRUE);
    
        $this->load->view($this->template, $data);
    }
    function shop() {

        // setup page header data
        $this
                ->set_title(lang('link_shop'));

        $data = $this->includes;

        // consult user
        $user = $this->users_model->get_user($this->user['id']);

        // set content data
        $content_data = array(           
            'page_title' => $data['page_title'],
            'user' => $user
        );

        if(!is_active($this->user['id']) ){
            $this->session->set_flashdata('error', lang("no_active_user_refer"));
            redirect("user/dashboard");
        }

        if(is_preferencial_user()){
            $this->session->set_flashdata('error', lang('users error is_preferencial_user'));
            redirect('user/dashboard');
        }

        $data['content'] = $this->load->view('user/link_shop', $content_data, TRUE);
    
        $this->load->view($this->template, $data);
    }
}