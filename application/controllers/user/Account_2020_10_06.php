<?php defined('BASEPATH') OR exit('No direct script access allowed');

class Account extends Private_Controller {


    /**
     * Constructor
     */
    function __construct()
    {
        parent::__construct();

        // load the language files
        $this->lang->load('users');
        $this->lang->load('dashboard');

        // load the users model
        $this->load->model('users_model');
        $this->load->model('profiles_model');
        $this->load->model('plans_model');
        $this->load->model('subscriptions_model','subscriptions');
        $this->load->model('locations_model','locations');
        $this->load->library('transaction');
        $this->load->library('referral');
        $this->load->library('binarytree');
        $this->load->library('rank');



    }


    /**************************************************************************************
     * PUBLIC FUNCTIONS
     **************************************************************************************/


    /**
     * Account list page
     */
    function index()
    {
        if (!is_allowed('read')){
            $this->session->set_flashdata('error', privilegies_message());
            redirect('user/dashboard');
        }
        
        // Monthly payments notifications
        // if(!is_active()){
        //     $this->session->set_flashdata('error', lang('monthly_suspended'));
        //     redirect('user/transactions/deposit');
        // }

        // setup page header data
        $this
            ->add_root_js( "bower_components/datatables.net/js/jquery.dataTables.min.js" )
            ->add_root_js( "assets/pages/data-table/js/data-table-custom.js" )
            ->add_root_js( "bower_components/datatables.net-bs4/js/dataTables.bootstrap4.min.js" )
            ->add_root_css( "bower_components/datatables.net-bs4/css/dataTables.bootstrap4.min.css" )
           
            ->set_title( lang('users title account') );

        $data = $this->includes;


        // Obtener el rango
        $minor = 0;
        $user = $this->binarytree->get_score($this->user['id']);
        $minor = ($user['left_points'] < $user['right_points']) ? $user['left_points'] : $user['right_points'];
        $points_all_time = $this->binarytree->get_points_all_time($this->user['id']);
        $total_points = ($minor + $points_all_time);
        $rank = $this->residual->unilevel($this->user['id']);

        
//        $chart = $this->transaction->userBonus($this->user['id']);
        $chart_js = array();
//        foreach ($chart as $index => $item) {
//            $date = explode(" ", $item['date']);
//            $chart_js[$index]['date'] = $date[0];
//            $chart_js[$index]['value'] = $item['usd'];
//        }

        // get user subscription
        $subscription = $this->subscriptions->get_active($this->user['id']);
        
        // set content data
        $content_data = array(
            'page_title'    => $data['page_title'],
            'plans'         => $this->plans_model->get_active(),
            'subscription'  => $subscription,
            'current_plan'  => $this->plans_model->get_plan($subscription['id_plan']),
            //'transactions'  => $this->transaction->user($this->user['id']),
            'referrals'     => $this->referral->get_referrals($this->user['id']),
            'chart'         => $chart_js,
            'showChart'    => false,
            'rank'         => $this->rank->get_rank($this->user["id"])
        );
        
        // load views
        $data['content'] = $this->load->view('user/account', $content_data, TRUE);
        $this->load->view($this->template, $data);
    }

    function privilegies(){

        // setup page header data
        $this            
            ->add_css_theme( "datatables/responsive.bootstrap.min.css" )
            ->add_css_theme( "datatables/responsive.bootstrap.min.css" )

            ->add_js_theme( "datatables/dataTables.bootstrap.js")
            ->add_js_theme( "datatables/dataTables.responsive.min.js")
            ->add_js_theme( "datatables/responsive.bootstrap.min.js")

            ->set_title( lang('users title account') );

            $data = $this->includes;

        // set content data
        $content_data = array(
            'privilegies_message'    => privilegies_message()
        );

        $data['content'] = $this->load->view('user/privilegies_message', $content_data, TRUE);
        $this->load->view($this->template, $data);
    }

}