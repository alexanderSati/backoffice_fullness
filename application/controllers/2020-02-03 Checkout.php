<?php
defined('BASEPATH') OR exit('No direct script access allowed');
//require_once( FCPATH."vendor".DIRECTORY_SEPARATOR. "mpdf".DIRECTORY_SEPARATOR."index.php");

class Checkout extends Private_Controller {

  /**
     * Constructor
     */
  function __construct()
  {
      parent::__construct();


      $this->url = base_url('cart');

      // load the language files
  
      $this->lang->load('cart',$this->session->language);
      $this->lang->load('checkout',$this->session->language);

      // load the cart model
      $this->load->model("cart_model");

      // load the shopping_order_model
      $this->load->model("shopping_order_model");
      $this->load->model("Shipping_details_model");
      $this->load->model("Products_model");
      $this->load->model("PaymentMethod_model");



      // load the users model
      $this->load->model('users_model');



      $this->load->library([
        "cart",
        'email',
        'wallet',
        'transaction',
        'plan',
        'referral',
        'binarytree',
        'orden',
        'PagoEfectivo'

      ]);
      
  }
  
  public function cart_list(){

    if($this->settings->end_of_the_month == 1){
      redirect('cart');
    }

    $this    
    ->add_root_js( "js/pages/select2.js" )
    ->add_root_js( "js/select/select2.full.js" )
    ->add_root_css("css/select/select2.min.css")
    ->add_root_js( "js/pages/cart_list.js" )
    ->add_root_js( "bower_components/datatables.net-bs4/js/dataTables.bootstrap4.min.js" )
    ->add_root_js( "bower_components/sweetalert/js/sweetalert.min.js")  
    ->add_root_js( "assets/js/modal.js") 

    ->add_root_css( "bower_components/sweetalert/css/sweetalert.css")

    ->set_title( lang('users title cart_list') );

    // Generacion de CSRF
    $csrf = array(
        'name' => $this->security->get_csrf_token_name(),
        'hash' => $this->security->get_csrf_hash()
    );

    $tipo_envio = "";
    foreach( $this->cart->contents() as $item ){

      if( $item['options']['shipping_details_id'] != 0 ){

        $tipo_envio=$item['options']['shipping_details_id'];
        
        break;
        
      }
    }

    $orden_temporal = $this->orden->get_temporal_orden( $this->user["id"] )["shopping_order_id"];
    
    $data = $this->includes;
    $content_data["csrf"] = $csrf;
    $content_data["consecutivo_orden"] = sprintf('%011d', $orden_temporal);
    $content_data["oficinas"] = $this->Shipping_details_model->get("*", [
      "state"=>'activo',
      "shipping_types_id"=>2
    ]);
    $content_data["mayoristas"] = $this->Shipping_details_model->get("*", [
      "state"=>'activo',
      "shipping_types_id"=>3
    ]);
    $content_data["tipo_envio"] = $tipo_envio;
    // averigua el saldo disponible de un usuario en la plataforma
    $content_data["saldoDisponible"] = $this->wallet->get($this->user['id'], "USD");

    $content_data["metodo_pago"] =$this->PaymentMethod_model->obtenerRegistroPorId([
      "payment_method.shopping_order_id"=>$orden_temporal
    ]);

    $content_data["visanet"] = $this->get_visanet_payment($this->user['id'],$orden_temporal );
    $data["content"] = $this->load->view("checkout/cart_list", $content_data, true);
    $this->load->view("..".DIRECTORY_SEPARATOR."..".DIRECTORY_SEPARATOR."themes".DIRECTORY_SEPARATOR.$this->settings->theme.DIRECTORY_SEPARATOR."template", $data);
  }


  private function get_visanet_payment($id_user, $shopping_order_id){
    $this->load->library("pagovisanet");
    $amount= number_format(floatval($this->cart->total()),2);
    $session_data = $this->pagovisanet->start_session($amount, $id_user);
    $token = $this->pagovisanet->get_token();
    $session_key = $session_data["sessionKey"];
    $expiration  = $session_data["expirationTime"];
    $this->pagovisanet->register($id_user, $shopping_order_id, $token, $session_key, $expiration, $amount);

    $registro = $this->pagovisanet->get_session( $id_user, $shopping_order_id, $amount );
    

    return ["id"=>$registro["id"], "amount"=>$registro["amount"], "session_key"=>$registro["session_key"], "shopping_order_id"=>$registro["shopping_order_id"]];
  }

  public  function visanet_response(){
    $data =[
      "method"=>"PAGO_VISANET",
      "code"=>$this->input->post("transactionToken"),
      "shopping_order_id"=>$this->orden->get_temporal_orden( $this->user["id"] )["shopping_order_id"],
      "created"=>date("Y-m-d H:i:s")
    ];

    $this->PaymentMethod_model->crear($data);
    redirect(site_url("checkout/cart_list"));
  }


  /**
   * Carlos Aguirre 2019-07-15 11:12:35
   * Realiza el registro de la compra en 
   * la base de ddatos, tablas `shopping_order` y`shopping_products`
   */
  public function buy( $user_id = null ){


    if( empty($user_id) ){
      $user_id = $this->user["id"];
    }

    $total_cart = $this->cart->total();
    if ($total_cart >= 0) { 


      $data["shopping_order_id"] = $this->orden->get_temporal_orden($this->user['id'])["shopping_order_id"];
      $data["users_id"]          = $user_id;
      $data["total_value"]       = $total_cart;
      $data["total_discount"]    = 0;
      $data["state"]    = 'pending';
      // Actualiza el registro de la tabla `shopping_order`
      $this->shopping_order_model->actualizar($data);



      $shopping_order_id = $data["shopping_order_id"];


      // Si esta adquiriendo un plan 
      if( !empty( $this->session->userdata("plan_id") ) ){
        $this->transaction->purchase_plan( $user_id, $total_cart,[
          "reference"=> $shopping_order_id, 
          "hash" => $this->session->userdata("plan_id")
        ] );
        $this->session->unset_userdata("plan_id");
        $this->session->unset_userdata("plan_value");
      }
      else{// Si es una compra normal
        // Crea la transaccion tipo purchase
        $this->transaction->purchase($user_id, $shopping_order_id, $total_cart);
      }

      $pago = $this->PaymentMethod_model->obtenerRegistroPorId([
        "payment_method.shopping_order_id"=>$shopping_order_id
      ]);
      
      if($pago["method"] == "PAGO_VISANET"){
        $this->load->library("pagovisanet");
        $autorizacion = $this->pagovisanet->get_authorization_payment($shopping_order_id);
        if( $autorizacion["data"]["dataMap"]["ACTION_CODE"] == "000" ){
          $this->PaymentMethod_model->actualizar([
              "status"=>"approved",
              "description"=>json_encode($autorizacion['data'])
            ],[
              "shopping_order_id"=>$shopping_order_id
          ]);
        }
        else{
          $this->PaymentMethod_model->actualizar([
              "status"=>"canceled",
              "description"=>$autorizacion["data"]["data"]["ACTION_DESCRIPTION"]
            ],[
              "shopping_order_id"=>$shopping_order_id
          ]);
        }
      } 
      
      // Registrar metodo de envío
      $this->orden->registrar_envio( $shopping_order_id, $this->input->post('envio') );

      
      // Crear el registro de la tabla `shopping_products`
      $product_to_insert = [];
      foreach($this->cart->contents() as $product){

        $product_to_insert["shopping_order_id"] = $shopping_order_id;
        $product_to_insert["shipping_details_id"] = $product["options"]["shipping_details_id"];
        $product_to_insert["product_id"] = $product["id"];
        $product_to_insert["value"] = $product["price"];
        $product_to_insert["quantity"] = $product["qty"];
        $product_to_insert["discount"] =0;

        $this->shopping_order_model->create_shopping_products($product_to_insert);


        // Si la compra fue realizada a un determinado mayorista, debe
        // reducir su cantidad de stock
        if( $product_to_insert["shipping_details_id"] != 0 ){
          $this->Products_model->reducir_catidad_en_stock( 
            $product["id"],
            $product_to_insert["shipping_details_id"],
            $product["qty"]
          );
        }
        // Si ha hecho pago con wallet entonces debe crear transaccion para 
        // regitrar el pago ( orden de compra )
        if($product["options"]["code"] == "DESCUENTO_BILLETERA"){
          $this->transaction->wallet_payment($user_id, $shopping_order_id, $product["price"]);
        }

      }
      
      if($this->session->userdata('logged_in')){

        $this->enviarEmailCompra( $shopping_order_id  );
        
        
        $this->session->set_flashdata('message', $this->lang->line('buy_successfull'));
      }
      else{
        $this->session->set_flashdata(
          'message', 
          $this->lang->line('external_buy_successfull')
        );
      }
    } else {
      $this->session->set_flashdata(
        'error', 
        $this->lang->line('total_zero')
      );
    }

    $this->cart->destroy();
    redirect(site_url('checkout/checkout_success/'.$shopping_order_id), 'location');
    
  
  }


  private function enviarEmailCompra($shopping_order_id){

    $user = $this->users_model->get_user($this->user['id']);

    $results['user'] = $user; 
    $results['order_id'] = $shopping_order_id;
    $results['items'] = $this->cart->contents();
    $results['envio'] = $this->input->post('envio');
    $results['total_valor'] = $this->cart->total();
    //Cargar vista html
    $html = $this->load->view("emails/".$results['user']['language']."/purchase", $results, TRUE);

    //Configurar y enviar correo
    $this->email->from($this->settings->site_email);
    $this->email->to($results['user']['email']);
    
    if($results['user']['language'] == 'english') {
      $this->email->subject('Purchase made for ' . $results['user']['first_name']);
    }elseif ($results['user']['language'] == 'spanish') {
      $this->email->subject('Compra realizada por ' . $results['user']['first_name']);
    }
    elseif ($results['user']['language'] == 'portuguese') {
      $this->email->subject('Compra feita por ' . $results['user']['first_name']);
    }

    $this->email->message($html);
    $this->email->set_mailtype("html");
    $this->email->send();
  }



  public function checkout_success($order_id){
    

    $data = $this->includes;
    $conten_data["products"] = $this->shopping_order_model->get_items_order(
      "*",
      [ "shopping_order_id"=>$order_id ]
    );

    $conten_data["orden"] = $this->shopping_order_model->get("*",["shopping_order_id"=>$order_id],1);
    $conten_data["pago"] = $this->PaymentMethod_model->obtenerRegistroPorId([
      "payment_method.shopping_order_id"=>$order_id
    ]);
    $conten_data["envio"] = $this->orden->obtenerMetodoEnvio($order_id);


    $conten_data["user"] = $this->users_model->get_user($conten_data["orden"]["users_id"]);

    $data["content"] = $this->load->view("checkout/checkout_success", $conten_data, true);
    $this->load->view($this->template, $data);
  }


  public function bill_card(){
    echo  FCPATH."vendor".DIRECTORY_SEPARATOR. "mpdf".DIRECTORY_SEPARATOR."index.php";
  }

}