<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Cart_external extends Public_Controller {

    public $url;

    /**
     * Constructor
     */
    function __construct()
    {
        parent::__construct();
        $this->url = base_url('cart_external');

        // load the language files        
        $this->lang->load('cart',$this->session->language);
        
        // load the users model
        $this->load->model([
            "cart_model",
            "subscriptions_model",
            "shipping_details_model",
            "Transactions_model",
            "users_model",
            "categories_model",
            "ProductsQuantities_model"

            ]);
            
        $this->load->library("cart");
        $this->load->library('orden');
        // $this->load->library('wallet');
    }

    public function referrals($username){  
        
        // Redirect users logged in
        if ($this->session->userdata('logged_in')) {
            $logged_in_user = $this->session->userdata('logged_in');
            if ($logged_in_user['is_admin']) {
                redirect('admin/dashboard');
            } else {
                if ($logged_in_user['profile'] == 'user') {
                    // redirect to landing page
                    redirect('user/dashboard');
                }
                else {
                    // redirect to orders
                    redirect('orders');
                }
                
            }
        }

        if($username) {
            $user_ref = $this->users_model->get_user($username);

            if (!$user_ref || $user_ref['profile'] != 'user') {
                $this->session->set_flashdata('error', lang('users error validate_referral_failed'));
                redirect('login');
            }
        }else {

            $this->session->set_flashdata('error', lang('users error validate_referral_failed'));
            redirect('login');
        }

        $this
            ->add_root_css( "bower_components/datatables.net-bs4/css/dataTables.bootstrap4.min.css" )
            ->add_root_js( "bower_components/datatables.net/js/jquery.dataTables.min.js" )
            ->add_root_js( "assets/pages/data-table/js/data-table-custom.js" )
            ->add_root_js( "bower_components/datatables.net-bs4/js/dataTables.bootstrap4.min.js" )
            ->add_root_js( "bower_components/sweetalert/js/sweetalert.min.js") 
            ->add_root_css( "css/animate.css")  
            ->add_root_js( "assets/js/modal.js") 
            
            ->add_root_css( "assets/css/component.css" )
            ->add_root_css( "bower_components/sweetalert/css/sweetalert.css")
            ->add_root_js( "assets/js/classie.js" )
            ->add_root_js( "assets/js/modalEffects.js" )

            ->add_css_theme("select/select2.min.css")
            ->add_js_theme( "select/select2.full.js" )
            ->add_js_theme( "pages/select2.js" )
            ->add_root_css( "assets/pages/notification/notification.css" )
            ->add_root_js( "assets/js/bootstrap-growl.min.js" )
            ->add_js_theme( "pages/shopping_cart_external.js")
            ->set_title( lang('products') );

            $name_product = empty($_GET["name_product"])? "": $_GET["name_product"];
            $price_min = empty($_GET["price_min"])? "": $_GET["price_min"];
            $price_max = empty($_GET["price_max"])? "": $_GET["price_max"];
            $category = empty($_GET["category"])? "": $_GET["category"];
            $shoppingDetailsId = empty($_GET["shopping_details_id"])? 0 : $_GET["shopping_details_id"];
            $offset = empty($_GET["offset"])? 0 : $_GET["offset"];
    
            // Parametros de la url (para mantener el form)        
            $url_params = preg_replace([
                '/&?offset=\d+/',
                '/&?csrf_token=\w*/'
            ] ,"", $_SERVER['REQUEST_URI'] );
    
            
            
            // Generacion de CSRF
            $csrf = array(
                'name' => $this->security->get_csrf_token_name(),
                'hash' => $this->security->get_csrf_hash()
            );
    
            // Paginacion
            $this->pagination->initialize(array(
                'base_url'   => site_url(substr($url_params,1)),
                'total_rows' => $this->cart_model->get_total_rows($name_product, $price_min, $price_max, $category, $shoppingDetailsId, $offset),
                'per_page'   => $this->settings->per_page_limit
            ));
    
            if( strpos($url_params, '&') !== false ){
                $url_params .= "&csrf_token=".$csrf['hash'];
            }
            else{
                $url_params .= "?csrf_token=".$csrf['hash'];
            }
    
            $data = $this->includes;

            $content_data["product"] = $this->cart_model->fetch_all_external($name_product, $price_min, $price_max, $category, $shoppingDetailsId, $offset, 12 );

            $content_data["pagination"] = $this->pagination->create_links();
            $content_data["proveedores"] = $this->shipping_details_model->get("*",["shipping_types_id IN(0,1)"=>null], null, "description ASC");

            $content_data["csrf"] = $csrf;
            $content_data["categories"] = $this->categories_model->get_all_external()['results'];
            $content_data["cart_list"] = $this->view();
            $content_data['this_url']  = $this->url;
            $content_data['user']  = $user_ref;     

            $data["content"] = $this->load->view("cart/shopping_cart_external", $content_data, true);
   
            $this->load->view("..".DIRECTORY_SEPARATOR."..".DIRECTORY_SEPARATOR."themes".DIRECTORY_SEPARATOR.$this->settings->theme.DIRECTORY_SEPARATOR."external", $data);
    }

    public function get_products(){
        $filtro = empty($this->input->post("filtro"))? "":$this->input->post("filtro");
        $inicio = empty($this->input->post("inicio"))? 0:$this->input->post("inicio");
        $offset = empty($this->input->post("offset"))? $this->settings->per_page_limit:$this->input->post("offset");
        
        // Generacion de CSRF
        $csrf = array(
            'name' => $this->security->get_csrf_token_name(),
            'hash' => $this->security->get_csrf_hash()
        );

        $content_data['products'] = $this->cart_model->fetch_all_external($filtro, $inicio, $offset );
        $content_data["csrf"] = $csrf;
        $content_data["total"] = $this->cart_model->get_total_rows();

        echo json_encode($content_data);
    }
    function add(){


        // Inserta los datos del producto en la sesion de usuario
        $data = array(
        "id"  => $_POST["product_id"],
        "name"  => str_replace(["/","&","(",")"],"",$_POST["product_name"]),
        "qty"  => $_POST["quantity"],
        "price"  => $_POST["product_price"],
        "options"=>[
            "code"=>$_POST['code'],
            "shipping_details_id"=>empty($_POST['shipping_details_id'])?'0':$_POST['shipping_details_id']
        ]
        );

        $cant_disponible = $this->ProductsQuantities_model
                            ->get_quantity_available($_POST['shipping_details_id'],$_POST["product_id"]);

       
        if( $cant_disponible <  $_POST["quantity"]){
          echo json_encode([
            "success"=>false, 
            "message"=>lang('no_quantity_in_stock'),
            "stock"=>$cant_disponible
           ]);
          exit();
        }                         
        
        foreach ($this->cart->contents() as $key => $value) {
            if($value['id'] == $data["id"] ){
                if ($cant_disponible ==  $value['qty']) {
                    echo json_encode([
                        "success"=>false, 
                        "message"=>lang('no_quantity_in_stock'),
                        "stock"=> null
                    ]);
                    exit(); 
                }
                
                if($value['qty'] + $data['qty'] >  $cant_disponible) {
                    echo json_encode([
                        "success"=>false, 
                        "message"=>lang('no_quantity_in_stock'),
                        "stock"=> $cant_disponible - $value['qty']
                    ]);
                    exit(); 
                }
            }
        }     

        $row_id= $this->cart->insert($data); //return rowid 

        $response = [
            "success"=>true,
            "row_id"=>$row_id,
            "csrf"=>[
                'name' => $this->security->get_csrf_token_name(),
                'hash' => $this->security->get_csrf_hash()
            ],
            "view"=> $this->view(),
            "total_cart"=>$this->cart->total()
        ];

        echo json_encode( $response );

    }
    
    function load(){
        echo $this->view();
    }

    function remove()
    {   
        // quita los datos de un producto producto en la sesion de usuarioº
        $row_id = $_POST["row_id"];
        
                
        $data = array(
        'rowid'  => $row_id,
        'qty'  => 0
        );
        
        $this->cart->update($data);
        $response = [
            "success"=>true,
            "row_id"=>$row_id,
            "csrf"=>[
                'name' => $this->security->get_csrf_token_name(),
                'hash' => $this->security->get_csrf_hash()
            ],
            "view"=> $this->view(),
            "total_cart"=>$this->cart->total()
        ];

        echo json_encode( $response );
    }
    
    function clear(){

        $this->cart->destroy();

        $response = [
            "success"=>true,
            "row_id"=>$row_id,
            "csrf"=>[
                'name' => $this->security->get_csrf_token_name(),
                'hash' => $this->security->get_csrf_hash()
            ],
            "view"=> $this->view(),
            "total_cart"=>$this->cart->total()
        ];

        echo json_encode( $response );

    }
     
    function view(){
        
        $output = '';
        $output .= '
        <div >
        <div align="right">
            <button type="button" id="clear_cart" class="btn btn-warning">
                '.$this->lang->line('clear_shopping_cart').'
            </button>
        </div>
        <br />
        <table class="table table-bordered">
            <tr>
            <th width="40%">'.$this->lang->line('cart_name').'</th>
            <th width="15%">'.$this->lang->line('cart_quantity').'</th>
            <th width="15%">'.$this->lang->line('price_shopping_cart').'</th>
            <th width="15%">Total</th>
            <th width="15%">'.$this->lang->line('cart_action').'</th>
            </tr>
        
        ';
        $count = 0;
        foreach($this->cart->contents() as $items)
        {
            $count++;
            $output .= '
            <tr> 
                <td>'.$items["name"].'</td><td>';


            if($items['id']!= 1){
                $output .=  '<input type="number" min="1" class="form-control" value="'.$items["qty"].'" onchange="update_qty(this.value, this)" data-rowid="'.$items["rowid"].'"/>';
            }
            else{
                $output .=  $items["qty"];
            }  
        
            $output .='</td><td>'.$items["price"].'</td>
                <td>'.$items["subtotal"].'</td>
                <td>
                    <button type="button" name="remove" class="btn btn-danger btn-xs remove_inventory" id="'.$items["rowid"].'">
                        '.$this->lang->line('remove_cart_list').'
                    </button>
                </td>
            </tr>
            ';
        }

        if( $count == 0 ){
            $output .= '
            <tr>
                <td colspan="5" align="center" >
                    '.$this->lang->line('cart_empty').'
                </td>
            </tr>
            ';
        }

        $output .= '
        <tr>
            <td colspan="3" align="right">Total</td>
            <td id="total_cart">'.$this->cart->total().'</td>
            <td></td>    
        </tr>
        
        </table>
        
        </div>
        ';
        
        
        return $output;
    }


    public function update_qty(){
        $data = array(
            'rowid' => $this->input->post("rowid"),
            'qty'   => $this->input->post("qty")
        );

        
        $product = $this->cart->get_item($data["rowid"]);
        
        $cant_disponible = $this->ProductsQuantities_model
        ->get_quantity_available(
            $product["options"]["shipping_details_id"],
            $product["id"]
        );
        if($cant_disponible < $data["qty"]){
            
            echo json_encode([
                "success"=>false,
                "message"=>lang('no_quantity_in_stock'),
                "stock"=>$cant_disponible
                ]);
                exit();
                
            }
        $this->cart->update($data);
        $response = [
            "success"=>true,
            "row_id"=>$this->input->post("rowid"),
            "csrf"=>[
                'name' => $this->security->get_csrf_token_name(),
                'hash' => $this->security->get_csrf_hash()
            ],
            "view"=> $this->view(),
            "total_cart"=>$this->cart->total()
        ];

        echo json_encode( $response );
    
        
    }


    /**
     * Genera un json con el contenido del carrito de compras
     * 
     */
    public function get_list_items_cart(){

        foreach($this->cart->contents() as $key => $item){
            $cant_disponible = $this->ProductsQuantities_model
                                        ->get_quantity_available(
                                            $item['options']['shipping_details_id'],
                                            $item['id']
                                        );
            $item["is_available"] =  $cant_disponible >= $item["qty"];
            $item["stock"] = $cant_disponible;
            $items['items'][] = $item;


            
        }
        $items['total'] = number_format ($this->cart->total(),2);
        $items['success'] = true;

        echo json_encode($items);

    }

}