<div class="text-center">
  <img src="/themes/able/images/logo.png" alt="logo" width="200">
  <!-- <img src="/themes/able/images/logo.png" alt="logo.png" style="max-width:200px;"> -->
</div>

<div class="pcoded-content" style="margin-top:3em">
  <!-- [ breadcrumb ] start -->

  <!-- [ breadcrumb ] end -->
    <div class="pcoded-inner-content">
        <!-- Main-body start -->
        <div class="main-body">
            <div class="page-wrapper">
                <!-- Page-body start -->
                <div class="page-body">
                    <!-- Basic table card start -->
                    <div class="card">
                        <div class="card-header">
                            <h5>Usuarios Fundadores</h5>
                       
                        </div>
                        <div class="card-block table-border-style">
                            <div class="table-responsive">
                                <table class="table">
                                  <thead>
                                      <tr>
                                        <th style="width:80px"></th>
                                        <th >Nombre</th>
                                        <th>Descripción</th>
                                      </tr>
                                  </thead>
                                  <tbody>
                                    <?php foreach( $usuarios as $usuario): ?>
                                    <tr>
                                      <td><img src="<?= base_url("/uploads/user_icon.png"); ?>" style="width:50px" /></td>
                                      <td><?= $usuario["spanish_question"] ?></td>
                                      <td><?= $usuario["spanish_answer"] ?></td>
                                    </tr>
                                    <?php endforeach; ?>
                                      
                                  </tbody>
                                </table>
                            </div>
                        </div>
                    </div>
                    <!-- Basic table card end -->

                </div>
                <!-- Page-body end -->
            </div>
        </div>
        <!-- Main-body end -->

    </div>
</div>