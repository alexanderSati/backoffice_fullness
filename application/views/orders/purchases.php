<!-- [ page content ] start -->
<div class="card px-3">
  <div class="card-block">

    <div class="dt-responsive table-responsive">
      <table id="datatable-responsive" class="table table-striped table-bordered nowrap">
        <thead>
          <tr>
            <th><?= $this->lang->line('cart_date'); ?></th>
            <th><?= $this->lang->line('order_number'); ?></th>
            <th><?= $this->lang->line('cart_user'); ?></th>
            <th><?= $this->lang->line('price_shopping_cart'); ?></th>
            <!--<th><?= $this->lang->line('cart_discount'); ?></th>-->
            <th><?= $this->lang->line('cart_state'); ?></th>
            <th><?= $this->lang->line('cart_details'); ?></th>
          </tr>
        </thead>
        <tbody>
          <?php foreach($purchases as $purchase): ?>

          <tr>
            <td><?= $purchase['created'] ?></td>
            <td><?= sprintf('%011d',$purchase['shopping_order_id']); ?></td>
            <td><?= $purchase['first_name']; ?> <?= $purchase['last_name']; ?></td>
            <td align="right">
              <?= $purchase['total_value']." S/"; ?>
            </td>
            <!--<td align="right">
                <?= "S/ ".$purchase['total_discount']; ?>
              </td>-->

            <td align="right">

              <?php if($purchase['state'] == "approved"): ?>
              <span class="label label-success">
                <?= strtolower($this->lang->line('cart_state_approved')); ?>
              </span>
              <?php elseif($purchase['state'] == "pending"): ?>
              <span class="label label-warning">
                <?= strtolower($this->lang->line('cart_state_pending')); ?>
              </span>
              <?php else: ?>
              <span class="label label-danger">
                <?= strtolower($this->lang->line('cart_state_denied')); ?>
              </span>
              <?php endif; ?>

            </td>
            <td align="center">
              <button class="btn btn-primary btn_open_modal waves-effect md-trigger" data-modal="modal-detail"
                data-toggle="modal" id="btn_open_modal" data-target="#modal-detail"
                data-shopping_order_id="<?= $purchase['shopping_order_id']; ?>">
                <?= strtolower($this->lang->line('cart_details')); ?>
              </button>
            </td>
          </tr>
          <?php endforeach; ?>
        </tbody>
      </table>

    </div>
    <!-- DETALLES DE COMPRA -->
    <div class="modal fade" id="modal-detail" tabindex="-1" role="dialog" style="z-index: 1050; display: none;"
      aria-hidden="true">
      <div class="modal-dialog modal-lg" role="document">
        <div class="modal-content">
          <div class="modal-header">
            <h4 class="modal-title"><?= $this->lang->line('cart_items') ?></h4>
            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
              <span aria-hidden="true">×</span>
            </button>
          </div>
          <div class="modal-body">
            <b><?= lang('users title order') ?>:</b> <span id="shipping_code"></span> <br>
            <b><?= lang('users input shipping_types_id') ?>: </b><span id="shipping_type"></span><br />
            <b><?= lang('users title send_to') ?>: </b> <span id="shipping_name"></span><br />
            <b><?= lang('payment_method') ?>: </b> <span id="payment_method"></span><br />



            <div class="dt-responsive table-responsive">
              <table class="table table-striped table-bordered nowrap">
                <thead>
                  <tr>
                    <th></th>
                    <th><?= lang('name_product')?></th>
                    <th><?= lang('quantity_product')?></th>
                    <th><?= lang('residual_points'); ?> </th>
                    <th><?= lang('binary_points'); ?> </th>
                    <th><?= lang('price_product')?></th>
                    <th><?= lang('total_product')?></th>
                  </tr>
                </thead>
                <tbody id="buy_products">

                </tbody>
              </table>
            </div>
            <div class="text-right">
              <button type="button" class="btn btn-default waves-effect"
                data-dismiss="modal"><?= lang('core button close') ?></button>
              <!--<button type="button" class="btn btn-primary">Save changes</button>-->
            </div>
          </div>

        </div>
      </div>
    </div>
    <!-- ./DETALLES DE COMPRA -->

  </div>
  <!-- end of card-block -->
</div>
<!-- [ page content ] end -->

<input type="hidden" name="PAGO_OFICINA" value="<?= lang("lbl_pay_oficina"); ?>" >
<input type="hidden" name="PAGO_MAYORISTA" value="<?= lang("lbl_pay_wholesaler"); ?>" >
<input type="hidden" name="BILLETERA" value="<?= lang("lbl_pay_wallet"); ?>" >
<input type="hidden" name="PAGO_ONLINE" value="<?= lang("lbl_pay_online"); ?>" >
<input type="hidden" name="PAGO_VISANET" value="<?= lang("pay_creditcard_label"); ?>" >


