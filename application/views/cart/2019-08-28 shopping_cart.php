<div class="card">

  <?php if($this->session->userdata('logged_in')): ?>
  <form method="get" action="<?= site_url('cart') ?>" id="filter_products">
    <div class="row">
      <div class="col-lg-12 col-md-12">
        <div class="box-header with-border card-header">
          <h3 class="box-title"><?= lang('products') ?></h3>

          <?php if( !empty($this->session->userdata("plan_id")) ): ?>
          <table>
            <tr>
              <td>
                <b>Plan:</b>
                <?php
                  switch( $this->session->userdata("plan_id") ){
                    case 5:
                      echo "Consultivo";
                      break;
                    case 6:
                      echo "Star";
                      break;
                    case 7:
                      echo "Plus";
                      break;
                    case 8:
                      echo "Master";
                      break;
                  }
                ?>
              </td>
              <td>
                <b><?= $this->lang->line("residual_points"); ?>:</b>
                <?php
                  switch( $this->session->userdata("plan_id") ){
                    case 5:
                      echo "15";
                      break;
                    case 6:
                      echo "50";
                      break;
                    case 7:
                      echo "100";
                      break;
                    case 8:
                      echo "250";
                      break;
                  }
                ?>
              </td>
            </tr>
          </table>
          <?php endif; ?>
        </div>
      </div>
    </div>

    <?php if( !empty($this->session->userdata('plan_value')) ): ?>
    <div class="card-block">
      <div class="callout callout-success">
        <h4 id="text_price">
          $<span id="parcial_price">
            <?= number_format ( $this->cart->total(), 2 ); ?>
          </span>
          <?= lang('cart_of') ?>
          $<span id="total_price">
            <?= $this->session->userdata('plan_value') ?>
        </h4>
      </div>
      <?php endif; ?>

      <div class="box-body">
        <div class="card-block">
          <div class="row">
            <div class="col-md-2">
              <div class="form-group">
                <label><?= $this->lang->line('cart_name'); ?>:</label>
                <input type="text" class="form-control" placeholder="<?= lang('users input name')?>" name="name_product"
                  value="<?= $this->input->get("name_product") ?>">
              </div>
            </div>
            <div class=" col-md-2" style="display:none">
              <div class="form-group">
                <label>Mín:</label>
                <input type="text" class="form-control" placeholder="Min" name="price_min"
                  value="<?= $this->input->get("price_min") ?>">
              </div>
            </div>
            <div class="col-md-2" style="display:none">
              <div class="form-group">
                <label>Max:</label>
                <input type="text" class="form-control" placeholder="Max" name="price_max"
                  value="<?= $this->input->get("price_max") ?>">
              </div>
            </div>
            <div class="col-md-2">
              <div class="form-group">
                <label><?= $this->lang->line('product_category'); ?>:</label>
                <select name="category" id="category" class="form-control select2_single form-control">
                  <option value="">-- seleccione --</option>
                  <?php foreach( $categories as $category ): ?>
                  <option value="<?= $category["category_id"] ?>"
                    <?= $this->input->get("category") == $category["category_id"]? "selected":""  ?>>
                    <?= $category["name"] ?>
                  </option>
                  <?php endforeach;  ?>
                </select>
              </div>
            </div>
            <div class="col-md-2" style="display:none">
              <div class="form-group">
                <label><?= $this->lang->line('product_ubication'); ?>:</label>
                <select name="shopping_details_id" id="shopping_details"
                  class="form-control select2_single form-control">
                  <option value="">-- seleccione --</option>
                  <?php foreach( $proveedores as $proveedor ): ?>
                  <option value="<?= $proveedor["id"] ?>"
                    <?= $this->input->get("shopping_details_id") == $proveedor["id"]? "selected":""  ?>>
                    <?= $proveedor["description"] ?>
                  </option>
                  <?php endforeach;  ?>
                </select>
              </div>
            </div>
            <div class="col-md-2" style="margin-top: 1.8em;">

              <button type="submit" name="submit" value="<?php echo lang('core button filter'); ?>"
                class="btn btn-info " data-toggle="tooltip" title="<?php echo lang('admin tooltip filter'); ?>">
                <span class="glyphicon glyphicon-filter"></span> <?php echo lang('core button filter'); ?>
              </button>
              <a href="<?php echo $this_url; ?>" class="btn btn-danger tooltips" data-toggle="tooltip"
                title="<?php echo lang('admin tooltip filter_reset'); ?>">
                <?php echo lang('core button reset'); ?>
              </a>
            </div>


            <div class="col-md-2 ml-auto float-right">
              <button class="btn btn-primary" type="button" style="margin-top: 1.8em; color: white"
                onclick="change_shipping()">
                <?= lang('change_shipping'); ?>
              </button>
            </div>
          </div>
        </div>
      </div>
      <!-- /.box-body -->
    </div>

    <input type="hidden" name="<?= $csrf['name'] ?>" value="<?= $csrf['hash'] ?>" id="csrf_data" />
  </form>
  <?php endif; ?>

</div>


<div id="list_products">

  <div class="row">


    <!-- nuevo producto -->

    <?php foreach($product as $row): ?>
    <div class="col-md-3">
      <div class="card prod-view">
        <div class="prod-item text-center">
          <div class="prod-img">
            <div class="option-hover">
              <button type="button"
                class="btn btn-success btn-icon waves-effect waves-light m-r-15 hvr-bounce-in option-icon">
                <i class="icofont icofont-cart-alt f-20"></i>
              </button>
              <button type="button"
                class="btn btn-primary btn-icon waves-effect waves-light m-r-15 hvr-bounce-in option-icon">
                <i class="icofont icofont-eye-alt f-20"></i>
              </button>
              <button type="button" class="btn btn-danger btn-icon waves-effect waves-light hvr-bounce-in option-icon">
                <i class="icofont icofont-heart-alt f-20"></i>
              </button>
            </div>
            <div class="hvr-shrink">
              <img src="<?= base_url().'uploads/'.$row['product_image']; ?>" class="img-fluid o-hidden" alt="prod1.jpg"
                style="padding: 2em;">
            </div>
            <?php if( $row['precio_original'] != $row['product_price']): ?>
            <div class="p-sale"><?=$row['descuento']?>%</div>
            <?php endif; ?>
          </div>
          <div class="prod-info">

            <a href="#!" class="txt-muted">
              <h6 style="text-transform: capitalize !important;" class="nombre_producto">
                <?= ucfirst( mb_strtolower (  $row['product_name'])); ?>
              </h6>
            </a>

            <span class="prod-price">
              <i class="icofont icofont-cur-dollar"></i>S/ <?=number_format((float)$row['product_price'], 2, '.', '');?>
              <?php if( $row['precio_original'] != $row['product_price']): ?>
              <small class="old-price">
                <i class="icofont icofont-cur-dollar"></i>
                S/ <?=number_format((float)$row['precio_original'], 2, '.', '');?>
              </small>
              <?php endif; ?>
            </span>




            <input type="hidden" class="type_user" value="<?= $row['tip_usuario']?>" />
            <input type="number" name="quantity" class="form-control quantity"
              style="width: 90%;margin-left: 1em;margin-right: 1em;" id="<?= $row['product_id']?>" min="0"
              placeholder="<?= lang('cart_quantity')?>" /><br />

            <button class="btn btn-success btn-round waves-effect waves-light add_cart" name="add_cart" type="button"
              data-productname="<?= $row['product_name']?>" data-price="<?= $row['product_price']?>"
              data-productid="<?= $row['product_id']?>" data-code="<?= $row['code']?>"
              data-shipping_details_id="<?= $row['shiping_details_id']?>" data-binary="<?=$row['binary_points']?>"
              data-residual="<?=$row['residual_points']?>">
              <?= $this->lang->line('add_shopping_cart'); ?>
            </button>
            <br />
            <?php if(empty($this->session->userdata('plan_value')) ): ?>

            <table class="table ">
              <tr>
                <th style="padding:0.5em !important"><strong><?= lang('users title points_in_residual') ?></strong></th>
                <th style="padding:0.5em !important"><strong><?= lang('users title points_in_binary') ?> </strong></th>
              </tr>
              <tr>
                <td style="padding:0.5em !important"><span><?=$row['residual_points']?></span></td>
                <td style="padding:0.5em !important"><span><?=$row['binary_points']?></span></td>
              </tr>
            </table>

            <?php endif; ?>

          </div>
        </div>
      </div>
    </div>
    <?php endforeach; ?>
    <!-- ./nuevo producto -->

    <?php if( empty( $product ) || count( $product ) == 0 ): ?>

    <div class="col-md-12 justify-content-center">
      <div class="alert alert-warning background-warning ">
        <button type="button" class="close" data-dismiss="alert" aria-label="Close">
          <i class="icofont icofont-close-line-circled text-white"></i>
        </button>
        <strong><?= lang("no_products_message"); ?></strong>
      </div>
    </div>

    <?php endif; ?>

  </div>
</div>

<?php if(!empty($pagination)): ?>
<div class="card">
  <div class="card-block">
    <?= $pagination  ?>
  </div>
</div>
<?php endif; ?>

<!-- modal de productos -->


<div class="modal fade" id="cart_list" tabindex="-1" role="dialog" style="z-index: 1050; display: none;"
  aria-hidden="true">
  <div class="modal-dialog modal-lg" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h4 class="modal-title"><?= $this->lang->line('shopping_cart'); ?></h4>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">×</span>
        </button>
      </div>
      <div class="modal-body">
        <div class="table-responsive" id="cart_details">
          <?= $cart_list; ?>
        </div>

        <div class="alert alert-success background-success" id="no_plan_price" style="display:none">
          <button type="button" class="close" data-dismiss="alert" aria-label="Close">
            <i class="icofont icofont-close-line-circled text-white"></i>
          </button>
          <strong><?= $this->lang->line("no_plan_price"); ?></strong>
        </div>
      </div>
      <div class="modal-footer">
        <a class="btn btn-danger waves-effect" href="<?php ?>" data-dismiss="modal">
          <?= $this->lang->line('close_cart_list'); ?>
        </a>
        <button class="btn btn-primary" id="btn_go_checkout">
          <a href="<?= site_url('checkout/cart_list') ?>"
            style="color:white;"><?= $this->lang->line('buy_cart_list'); ?></a>
        </button>

      </div>
    </div>
  </div>
</div>

<!-- modal de productos -->

<a data-toggle="modal" data-target="#cart_list" data-modal="cart_list" style="color: white;"
  class="btn btn-default btn_open_modal waves-effect md-trigger float">
  <!--<i class="fa fa-plus my-float"></i>-->
  <i class="fa fa-shopping-cart my-float"></i>
</a>

<div class="modal fade" id="modal_recogida" tabindex="-1" role="dialog" style="z-index: 1050; display: none;"
  aria-hidden="true">
  <div class="modal-dialog modal-sm" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h4 class="modal-title">Lugar de recogida</h4>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">×</span>
        </button>
      </div>
      <div class="modal-body">
        <div class="form-group">

          <div class="text-center">
            <img src="<?= base_url("themes/able/images/shipping.jpg") ?>" style="width:120px" />

          </div>
          <label for="items_ubication"><?= lang("product_ubication"); ?>:</label><br />
          <select style="width:250px" id="items_ubication" class="form-control select2_single form-control"
            onchange="seleccionar_envio()">
            <option value="0">Courier</option>
            <?php foreach( $proveedores as $proveedor ): ?>
            <option value="<?= $proveedor["id"] ?>"
              <?= $this->input->get("shopping_details_id") == $proveedor["id"]? "selected":""  ?>>
              <?= $proveedor["description"] ?>
            </option>
            <?php endforeach;  ?>
          </select>
        </div>
      </div>
      <div class="modal-footer">
        <button type="submit" form="filter_products" class="btn btn-default waves-effect ">
          <?= lang("core button filter"); ?>
        </button>
      </div>
    </div>
  </div>
</div>

<input type="hidden" value="<?= lang('users msg confirm_clear_cart') ?>" id="confirm_clear_cart">
<input type="hidden" value="<?= lang('users msg confirm_remove_inventory') ?>" id="confirm_remove_inventory">
<input type="hidden" value="<?= lang('users button confirm') ?>" id="confirm_button">
<input type="hidden" value="<?= lang('users button cancel') ?>" id="cancel_button">
<input type="hidden" value="<?= lang('users msg correct_clear') ?>" id="correct_clear">
<input type="hidden" value="<?= lang('users msg correct_remove_inventory') ?>" id="correct_remove_inventory">

<input type="hidden" value="<?= lang('message_add') ?>" id="message_add">
<script>
var plan_value = < ? = empty($this - > session - > userdata('plan_value')) ? 1 : 0; ? > ;
</script>