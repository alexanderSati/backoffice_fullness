<?php defined('BASEPATH') OR exit('No direct script access allowed'); ?>



<div class="p-4">







<div class="card">



  <form method="get" action="<?= $this_url ?>/referrals/<?= $user['username']?>" id="filter_products">



    <div class="row">



      <div class="col-lg-12 col-md-12">



        <div class="card-header">



          <h3><?= lang('products') ?></h3>



        </div>



      </div>



    </div>







      <div class="box-body">



        <div class="card-block">



          <div class="row">



            <div class="col-md-2">



              <div class="form-group">



                <label><?= lang('cart_name'); ?>:</label>



                <input type="text" class="form-control" placeholder="<?= lang('users input name')?>" name="name_product"



                  value="<?= $this->input->get("name_product") ?>">



              </div>



            </div>



            <div class=" col-md-2" style="display:none">



              <div class="form-group">



                <label>Mín:</label>



                <input type="text" class="form-control" placeholder="Min" name="price_min"



                  value="<?= $this->input->get("price_min") ?>">



              </div>



            </div>



            <div class="col-md-2" style="display:none">



              <div class="form-group">



                <label>Max:</label>



                <input type="text" class="form-control" placeholder="Max" name="price_max"



                  value="<?= $this->input->get("price_max") ?>">



              </div>



            </div>



            <div class="col-md-2">



              <div class="form-group">



                <label><?= lang('product_category'); ?>:</label>



                <select name="category" id="category" class="form-control select2_single">



                  <option value="">-- seleccione --</option>



                  <?php foreach( $categories as $category ): ?>



                  <option value="<?= $category["category_id"] ?>"



                    <?= $this->input->get("category") == $category["category_id"]? "selected":""  ?>>



                    <?= $category["name"] ?>



                  </option>



                  <?php endforeach;  ?>



                </select>



              </div>



            </div>



            <div class="col-md-2" style="display:none">



              <div class="form-group">



                <label><?= lang('product_ubication'); ?>:</label>



                <select name="shopping_details_id" id="shopping_details"



                  class="form-control select2_single form-control">



                  <option value="">-- seleccione --</option>



                  <?php foreach( $proveedores as $proveedor ): ?>



                  <option value="<?= $proveedor["id"] ?>"



                    <?= $this->input->get("shopping_details_id") == $proveedor["id"]? "selected":""  ?>>



                    <?= $proveedor["description"] ?>



                  </option>



                  <?php endforeach;  ?>



                </select>



              </div>



            </div>



            <div class="col-md-2" style="margin-top: 1.8em; display: flex">







              <button type="submit" style="height: 44px" name="submit" value="<?php echo lang('core button filter'); ?>"



                class="btn btn-info " data-toggle="tooltip" title="<?php echo lang('admin tooltip filter'); ?>">



                <span class="glyphicon glyphicon-filter"></span> <?php echo lang('core button filter'); ?>



              </button>



              <a style="height: 44px; margin-left: 4px;" href="<?php echo $this_url; ?>/referrals/<?= $user['username']?>" class="btn btn-danger tooltips" data-toggle="tooltip"



                title="<?php echo lang('admin tooltip filter_reset'); ?>">



                <?php echo lang('core button reset'); ?>



              </a>



            </div>











            <div class="col-md-2 ml-auto float-right">



              <button class="btn btn-primary" type="button" style="margin-top: 1.8em; color: white"



                onclick="change_shipping()">



                <?= lang('change_shipping'); ?>



              </button>



            </div>



          </div>



        </div>



      </div>



      <!-- /.box-body -->



    </div>







    <input type="hidden" name="<?= $csrf['name'] ?>" value="<?= $csrf['hash'] ?>" id="csrf_data" />



  </form>







</div>











<div id="list_products" class="p-4">







  <div class="row">











    <!-- nuevo producto -->



    <?php foreach($product as $row): ?>



    <div class="col-md-3">



      <div class="card prod-view">



        <div class="prod-item text-center">



          <div class="prod-img">



            <div class="option-hover">



              <button type="button"



                class="btn btn-success btn-icon waves-effect waves-light m-r-15 hvr-bounce-in option-icon">



                <i class="icofont icofont-cart-alt f-20"></i>



              </button>



              <button type="button"



                class="btn btn-primary btn-icon waves-effect waves-light m-r-15 hvr-bounce-in option-icon">



                <i class="icofont icofont-eye-alt f-20"></i>



              </button>



              <button type="button" class="btn btn-danger btn-icon waves-effect waves-light hvr-bounce-in option-icon">



                <i class="icofont icofont-heart-alt f-20"></i>



              </button>



            </div>



            <div class="hvr-shrink">



              <img src="<?= base_url().'uploads/'.$row['product_image']; ?>" class="img-fluid o-hidden" alt="prod1.jpg"



                style="padding: 4em;">



            </div>



          </div>



          <div class="prod-info">



            <a class="txt-muted">



              <h6 style="text-transform: capitalize !important;" class="nombre_producto">



              <?= ucfirst( mb_strtolower ($row['product_name'])); ?>



              </h6>



            </a>



            <span class="prod-price ">



                S/<?= number_format((float)$row['precio_original'], 2, '.', '');?>                     



            </span>



    



            <input type="number" name="quantity" class="form-control quantity"



              style="width: 90%;margin-left: 1em;margin-right: 1em;" id="<?= $row['product_id']?>" min="0"



              placeholder="<?= lang('cart_quantity')?>" /><br />







            <button class="btn btn-success btn-round waves-effect waves-light add_cart" name="add_cart" type="button"



              data-productname="<?= $row['product_name']?>" data-price="<?= $row['precio_original']?>"



              data-productid="<?= $row['product_id']?>" data-code="<?= $row['code']?>"



              data-shipping_details_id="<?= $row['shiping_details_id']?>">



              <?= lang('add_shopping_cart'); ?>



            </button>







          </div>



        </div>



      </div>



    </div>



    <?php endforeach; ?>



    <!-- ./nuevo producto -->







    <?php if( empty( $product ) || count( $product ) == 0 ): ?>







    <div class="col-md-12 justify-content-center">



      <div class="alert alert-warning background-warning ">



        <button type="button" class="close" data-dismiss="alert" aria-label="Close">



          <i class="icofont icofont-close-line-circled text-white"></i>



        </button>



        <strong><?= lang("no_products_message"); ?></strong>



      </div>



    </div>







    <?php endif; ?>







  </div>



</div>







<?php if(!empty($pagination)): ?>



<div class="p-4">



  <div class="card">



    <div class="card-block">



      <?= $pagination  ?>



    </div>



  </div>



</div>



<?php endif; ?>







<!-- modal de productos -->











<div class="modal fade" id="cart_list" tabindex="-1" role="dialog" style="z-index: 1050; display: none;"



  aria-hidden="true">



  <div class="modal-dialog modal-lg" role="document">



    <div class="modal-content">



      <div class="modal-header">



        <h4 class="modal-title"><?= lang('shopping_cart'); ?></h4>



        <button type="button" class="close" data-dismiss="modal" aria-label="Close">



          <span aria-hidden="true">×</span>



        </button>



      </div>



      <div class="modal-body">



        <div class="table-responsive" id="cart_details">



          <?= $cart_list; ?>



        </div>







        <div class="alert alert-success background-success" id="no_plan_price" style="display:none">



          <button type="button" class="close" data-dismiss="alert" aria-label="Close">



            <i class="icofont icofont-close-line-circled text-white"></i>



          </button>



          <strong><?= lang("no_plan_price"); ?></strong>



        </div>



      </div>



      <div class="modal-footer">



        <a class="btn btn-danger waves-effect" href="<?php ?>" data-dismiss="modal">



          <?= lang('close_cart_list'); ?>



        </a>



        <button class="btn btn-primary" id="btn_go_checkout" onclick="send_purchase()">         



           <?= lang('buy_cart_list'); ?>



        </button>







      </div>



    </div>



  </div>



</div>







<!-- modal de productos -->







<a data-toggle="modal" data-target="#cart_list" data-modal="cart_list" style="color: white;"



  class="btn btn-default btn_open_modal waves-effect md-trigger float">



  <!--<i class="fa fa-plus my-float"></i>-->



  <i class="fa fa-shopping-cart my-float"></i>



</a>







<div class="modal fade" id="modal_recogida" data-backdrop="static" tabindex="-1" role="dialog" style="z-index: 1050; display: none;"



  aria-hidden="true">



  <div class="modal-dialog modal-sm" role="document">



    <div class="modal-content">



      <div class="modal-header">



        <h4 class="modal-title"><?=lang('users title shipping_details')?></h4>



      </div>



      <div class="modal-body">



        <div class="form-group">







          <div class="text-center">



            <img src="<?= base_url("themes/able/images/shipping.jpg") ?>" style="width:120px" />







          </div>



          <label for="items_ubication"><?= lang("product_ubication"); ?>:</label><br />



          <select style="width:250px" id="items_ubication" class="form-control select2_single form-control"



            onchange="seleccionar_envio()">



            <option value="0">Courier</option>



            <?php foreach( $proveedores as $proveedor ): ?>



            <option value="<?= $proveedor["id"] ?>"



              <?= $this->input->get("shopping_details_id") == $proveedor["id"]? "selected":""  ?>>



              <?= $proveedor["description"] ?>



            </option>



            <?php endforeach;  ?>



          </select>



        </div>



      </div>



      <div class="modal-footer">



        <button type="submit" form="filter_products" class="btn btn-default waves-effect ">



          <?= lang("core button filter"); ?>



        </button>



      </div>



    </div>



  </div>



</div>







<input type="hidden" value="<?= lang('users msg confirm_clear_cart') ?>" id="confirm_clear_cart">



<input type="hidden" value="<?= lang('users msg confirm_remove_inventory') ?>" id="confirm_remove_inventory">



<input type="hidden" value="<?= lang('users button confirm') ?>" id="confirm_button">



<input type="hidden" value="<?= lang('users button cancel') ?>" id="cancel_button">



<input type="hidden" value="<?= lang('users msg correct_clear') ?>" id="correct_clear">



<input type="hidden" value="<?= lang('users msg correct_remove_inventory') ?>" id="correct_remove_inventory">



<input type="hidden" value="<?= lang('users error number_negative')?>" id="zero_quantity">



<input type="hidden" value="<?= lang('message_add') ?>" id="message_add">



<input type="hidden" value="<?= $this->cart->total() ?>" id="total_cart_external">



<input type="hidden" value="<?= lang('total_zero'); ?>" id="total_cart_zero">



<input type="hidden" value="<?= $user['username']?>" id="referral-user">



</div>



