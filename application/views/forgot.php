<?php defined('BASEPATH') OR exit('No direct script access allowed'); ?>


<div themebg-pattern="theme1 animate form">
  <!-- Pre-loader start -->
  <div class="theme-loader">
    <div class="loader-track">
      <div class="preloader-wrapper">
        <div class="spinner-layer spinner-blue">
          <div class="circle-clipper left">
            <div class="circle"></div>
          </div>
          <div class="gap-patch">
            <div class="circle"></div>
          </div>
          <div class="circle-clipper right">
            <div class="circle"></div>
          </div>
        </div>
        <div class="spinner-layer spinner-red">
          <div class="circle-clipper left">
            <div class="circle"></div>
          </div>
          <div class="gap-patch">
            <div class="circle"></div>
          </div>
          <div class="circle-clipper right">
            <div class="circle"></div>
          </div>
        </div>
      
        <div class="spinner-layer spinner-yellow">
          <div class="circle-clipper left">
            <div class="circle"></div>
          </div>
          <div class="gap-patch">
            <div class="circle"></div>
          </div>
          <div class="circle-clipper right">
            <div class="circle"></div>
          </div>
        </div>
      
        <div class="spinner-layer spinner-green">
          <div class="circle-clipper left">
            <div class="circle"></div>
          </div>
          <div class="gap-patch">
            <div class="circle"></div>
          </div>
          <div class="circle-clipper right">
            <div class="circle"></div>
          </div>
        </div>
      </div>
    </div>
  </div>
  <!-- Pre-loader end -->
    
  <section class="login-block">

    <?php // System messages ?>
      <?php if ($this->session->flashdata('message')) : ?>
        <div class="alert alert-success alert-dismissable">
          <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
          <?php echo $this->session->flashdata('message'); ?>
        </div>
      <?php elseif ($this->session->flashdata('error')) : ?>
        <div class="alert alert-danger alert-dismissable">
            <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
            <?php echo $this->session->flashdata('error'); ?>
        </div>
      <?php elseif (validation_errors()) : ?>
        <div class="alert alert-danger alert-dismissable">
          <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
          <?php echo validation_errors(); ?>
        </div>
      <?php elseif ($this->error) : ?>
        <div class="alert alert-danger alert-dismissable">
          <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
          <?php echo $this->error; ?>
        </div>
      <?php endif; ?>

      <!-- Container-fluid starts -->
      <div class="container-fluid">
        <div class="row">
          <div class="col-sm-12">
            <!-- Authentication card start -->
            <?php echo form_open('', array('class'=>'md-float-material form-material', 'role'=>'form','data-parsley-validate'=>'')); ?>
              <div class="text-center">
                <img width="200" src="/themes/<?= $this->settings->theme ?>/images/logo.png" alt="logo">
              </div>
              <div class="auth-box card">
                <div class="card-block">
                  <div class="row m-b-20">
                    <div class="col-md-12">
                      <h3 class="text-center txt-primary"><?= $page_title ?></h3>
                    </div>
                  </div>
                
                  <div class="form-group form-primary">
                    <?php echo form_input(array('name'=>'username','required'=>'required', 'value'=>set_value('username', (isset($user['username']) ? $user['username'] : '')), 'class'=>'form-control')); ?>
                    <span class="form-bar"></span>
                    <?php echo form_label('username', 'username', array('class' => 'float-label')); ?>
                  </div>
                  <div class="row">
                    <div class="col-md-12">
                    <?php echo form_submit(array('name'=>'submit', 'class'=>'btn btn-info submit btn-md btn-block waves-effect text-center m-b-20'), lang('users button reset_password')); ?>
                    <a class="text-right float-right f-w-600" href="<?php echo base_url('login'); ?>"><?php echo lang('core button login'); ?></a>
                  </div>
                  
                  <div class="col-md-12 text-center m-t-20">

                    <p>New to site?
                      <a class="f-w-600" href="<?php echo base_url('register'); ?>"><?php echo lang('users link register_account'); ?></a>
                    </p>
                    <div><?= lang("verify-spam") ?></div>                  
                    <br />
                    <div>
                      <h3><?php echo $this->settings->site_name; ?></h3>
                    </div>
                  </div>
                </div>
                <!-- end of card-block -->
              </div>
              <!-- end of auto-box card -->
            <?php echo form_close(); ?>
            <!-- form -->
          </div>
          <!-- end of col-sm-12 line 88 -->
        </div>
        <!-- end of row line 87 -->
      </div>
      <!-- end of container-fluid -->
  </section>
  <!-- content -->
  <div class="footer">
    <p class="text-center m-b-0">Copyright &copy; 2019, All rights reserved.</p>
  </div>
    
</div>
