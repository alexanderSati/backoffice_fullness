<?php defined('BASEPATH') OR exit('No direct script access allowed');

$lang = (isset($user['language']) ? $user['language'] : $this->config->item('language'));

?>

<div class="wrapper_payment">
  <div class="animate form">


    <section class="login_content payment">



      <h1><?= $page_title ?></h1>

      <h4><?= lang('your_plan'); ?></h4>

      <div class="container show_plan">




        <div class="row">

          <div class="">


            <!-- price element -->
            <div class=" col-xs-12 element-pricing">
              <div class="pricing">
                <div class="title">
                  <h2><?= $plan["{$lang}_name"] ?></h2>
                  <h2 style="font-size: 28px">S/<?= $plan["base"] ?></h2>
                </div>
                <div class="x_content">
                  <div class="">
                    <div class="pricing_features">
                      <ul class="list-unstyled text-left">
                        <li><?= $plan["{$lang}_features"] ?></li>
                      </ul>
                    </div>
                  </div>

                </div>
              </div>
            </div>
            <!-- price element -->

          </div>

        </div>

      </div>


      <h1 class="text-center">Información de pago</h1>

      <h4 style="display:none"><?= lang('users title register qr_to_pay'); ?></h4>


      <div class="display_qr" style="display:none">
        <div id="btc">
          <img src="https://blockchain.info/qr?data=bitcoin:<?php echo $wallet; ?>?amount=<?php echo $plan_price; ?>" />
          <!--                <p class="text_inversions"><?php //echo lang('pay_text_inversions'); ?></p>
                <p class="congratulations"><?php //echo lang('pay_congratulations'); ?></p>-->
        </div>
        <p><?php echo $wallet; ?></p>
      </div>

      <div class="alerts_messages">
        <?php // System messages ?>
        <?php if ($this->session->flashdata('message')) : ?>
        <div class="alert alert-success alert-dismissable">
          <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
          <?php echo $this->session->flashdata('message'); ?>
        </div>
        <?php elseif ($this->session->flashdata('error')) : ?>
        <div class="alert alert-danger alert-dismissable">
          <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
          <?php echo $this->session->flashdata('error'); ?>
        </div>
        <?php elseif (validation_errors()) : ?>
        <div class="alert alert-danger alert-dismissable">
          <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
          <?php echo validation_errors(); ?>
        </div>
        <?php elseif ($this->error) : ?>
        <div class="alert alert-danger alert-dismissable">
          <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
          <?php echo $this->error; ?>
        </div>
        <?php endif; ?>
      </div>

      <a class="btn btn-primary cancel" href="<?= base_url(); ?>"><?= lang('back_to_home');?></a>

      <div class="clearfix"></div>
      <br />
      <div>
        <h1><?php echo $this->settings->site_name; ?></h1>
      </div>
    </section>

  </div>
</div>