
<?php 
    $lang = (isset($user['language']) ? $user['language'] : $this->config->item('language')); 
?>
<!-- [ page content ] start -->
<div class="card">
  <div class="card-block m-l-50 m-r-50 m-t-20">
    <div class="row">
      <!-- Multiple Open Accordion start -->
      <div class="col-lg-12">
        <div class="card">
          <div class="card-header">
            <h4 class="card-header-text"><?= lang('users title select_option') ?></h4>
          </div>
          <div class="card-block accordion-block color-accordion-block">
            <div id="accordion" role="tablist" aria-multiselectable="false">
              <div class="accordion-panel">
                <div class="accordion-heading" role="tab" id="headingOne">
                  <h3 class="card-title accordion-title">
                    <a class="accordion-msg waves-effect waves-dark f-w-600" data-toggle="collapse"
                      data-parent="#accordion" href="#collapseOne" aria-expanded="true" aria-controls="collapseOne">
                      <?= lang('users title preferred_consumer')?>
                    </a>
                  </h3>
                </div>
                <div id="collapseOne" class="panel-collapse collapse in" role="tabpanel" aria-labelledby="headingOne">
                  <div class="accordion-content accordion-desc">
                    <p>
                      Anim pariatur cliche reprehenderit, enim eiusmod high life accusamus terry richardson ad squid. 3
                      wolf moon officia aute, non cupidatat skateboard dolor brunch. Food truck quinoa nesciunt laborum
                      eiusmod. Brunch 3 wolf moon tempor, sunt aliqua put a bird on it squid single-origin coffee nulla
                      assumenda shoreditch et. Nihil anim keffiyeh helvetica, craft beer labore wes anderson cred
                      nesciunt sapiente ea proident. Ad vegan excepteur butcher vice lomo. Leggings occaecat craft beer
                      farm-to-table, raw denim aesthetic synth nesciunt you probably haven't heard of them accusamus
                      labore sustainable VHS.
                      <div>
                        <button class="btn btn-primary waves-effect md-trigger"
                          style="margin:auto; display:block; margin-bottom: 1em" data-toggle="modal"
                          data-target="#confirm_preferencial" data-modal="confirm_preferencial">
                          <?= lang('users button accept') ?>
                        </button>
                      </div>
                    </p>
                  </div>
                </div>
              </div>
              <!-- panel 2 -->
              <?php foreach($planes as $plan): ?>
              <?php if( $plan["base"] != 0 ): ?>
              <div class="accordion-panel">
                <div class="accordion-heading" role="tab" id="headingTwo">
                  <h3 class="card-title accordion-title">
                    <a class="accordion-msg waves-effect waves-dark f-w-600" data-toggle="collapse"
                      data-parent="#accordion" href="#plan_<?= $plan['id'] ?>" aria-expanded="false"
                      aria-controls="plan_<?= $plan['id'] ?>">
                      Plan <?= $plan["{$lang}_name"]; ?>
                    </a>
                  </h3>
                </div>
                <div id="plan_<?= $plan['id'] ?>" class="panel-collapse collapse" role="tabpanel"
                  aria-labelledby="headingTwo">
                  <div class="accordion-content accordion-desc">

                    <?= $plan["{$lang}_features"]; ?>

                    <div>
                      <button class="btn btn-primary waves-effect md-trigger"
                        style="margin:auto; display:block; margin-bottom: 1em" data-toggle="modal"
                        data-target="#modal_plan_<?= $plan["id"]; ?>" data-modal="modal_plan_<?= $plan["id"]; ?>">
                        <?= lang('users button accept') ?>
                      </button>
                    </div>
                  </div>
                </div>
                <?php endif; ?>
                <?php endforeach; ?>
                <!-- end of panel 2 -->
              </div>
            </div>
          </div>
        </div>

        <!-- CONFIRMACION OPOCION DE COMPRADOR PREFERENCIAL -->
        <div class="modal fade" id="confirm_preferencial">
          <div class="modal-dialog">
            <div class="modal-content">
              <div class="modal-header">
              <div class="md-header">
                  <h4 class="md-title text-center"> <?= lang('users msg confirm_preferred') ?></h4>                  
                </div>
                  <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">×</span></button>
                  </button>
              </div>
              <div class="modal-body">

                <div class="row">
                  <div class="col-md-6">
                    <button type="button" class="btn btn-danger pull-right" style="width: 7em;" data-dismiss="modal">
                    <?= lang('users button cancel')?>
                    </button>

                  </div>
                  <div class="col-md-6">
                    <a type="button" class="btn btn-primary pull-left" style="width: 7em;"
                      href="<?= site_url('user/profile/activate_preferencial'); ?>">
                      <?= lang('users button confirm')?>
                    </a>

                  </div>


                </div>

              </div>
              <div class="modal-footer">
              </div>
            </div>
            <!-- /.modal-content -->
          </div>
          <!-- /.modal-dialog -->
        </div>

        <!-- ./CONFIRMACION OPOCION DE COMPRADOR PREFERENCIAL -->

        <!-- CONFIRMACION OPOCION DE COMPRADOR PLAN -->
        <?php foreach( $planes as $plan ): ?>
        <?php if( $plan["base"] != 0 ): ?>
        <div class="md-modal md-effect-1" id="modal_plan_<?= $plan["id"]; ?>">
          <div class="md-content">
            <button type="button" class="close m-2" data-dismiss="modal" aria-label="Close">
              <span class="f-18" aria-hidden="true">×</span>
            </button>
            <div class="md-header">
              <h4 class="md-title text-center"> <?= sprintf(lang('users msg confirm_select_option'), $plan["{$lang}_name"]) ?></h4>
              
            </div>
            <div class="md-body">
              <div class="row">
                <div class="col-md-12 text-center">
                  <button type="button" class="btn btn-danger waves-effect" style="width: 7em;" data-dismiss="modal">
                    <?= lang('users button cancel')?>
                  </button>

                  <a type="button" class="btn btn-primary waves-effect" style="width: 7em;"
                    href="<?= site_url('user/profile/set_temporal_plan/'.$plan['id']); ?>">
                    <?= lang('users button confirm')?>
  
                  </a>
                </div>
              </div>
            </div>
          </div>
          <!-- /.modal-content -->
        </div>
        <?php endif; ?>
        <?php endforeach; ?>
        <!-- ./CONFIRMACION OPOCION DE COMPRADOR PLAN -->

      </div>
      <!-- end of card-block -->
    </div>
    <!-- [ page content ] end -->