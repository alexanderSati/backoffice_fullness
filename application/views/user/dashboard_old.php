<script src="https://cdn.jsdelivr.net/clipboard.js/1.6.0/clipboard.min.js"></script>
<div class="row top_tiles flex-tiles">
  
    <?php if (isset($message) && !($message=='<p><br></p>')){?>       
    <div class="col-md-12 col-sm-12 col-xs-12">        
        <div class="x_panel">
            <strong><b><h3><?= lang('wellcome') ?></h3></b></strong>
            <hr>            
            <?= $message ?>           
        </div>        
    </div>
    <?php }?>
    
    <div class="animated flipInY col-lg-4 col-md-4 col-sm-6 col-xs-12">
        <div class="tile-stats">
            <div class="icon"><i class="fa fa-bitcoin"></i>
            </div>
            <div class="count">$ <?= $currencies['price_usd'] ?> USD</div>

            <h3><?= lang('bitcoin_price') ?></h3>
        </div>
    </div>
    <div class="animated flipInY col-lg-4 col-md-4 col-sm-6 col-xs-12">
        <div class="tile-stats">
            <div class="pull-right" style="margin: 10px">
                <a href="<?php echo base_url('user/create'); ?>" class="btn btn-warning sign-friend"
                   title="<?= lang("sign-friend") ?>">
                    <i class="fa fa-user-plus"></i> <?= lang("sign-friend") ?>
                </a>
            </div>
            <div class="count pull-left"><?= lang('affiliate_Link') ?></div>

            <h3 style="font-size: 15px;clear: both;">
                <span id="link-left" class="clipboard-hide"><?= base_url() ?>referrals/<?= $this->user['username'] ?>/a</span>
                <span>.../referrals/<?= $this->user['username'] ?>/a</span>
                <button class="btn btn-info btn-copy btn-left" style="font-size: 10px;margin-left: 10px"
                        data-clipboard-action="copy" data-clipboard-target="#link-left">
                    <i class="fa fa-clipboard" aria-hidden="true"></i>
                </button>
            </h3>
            <h3 style="font-size: 15px;clear: both;">
                <span id="link-right" class="clipboard-hide"><?= base_url() ?>referrals/<?= $this->user['username'] ?>/b</span>
                <span>.../referrals/<?= $this->user['username'] ?>/b</span>
                <button class="btn btn-info btn-copy btn-right" style="font-size: 10px;margin-left: 10px"
                        data-clipboard-action="copy" data-clipboard-target="#link-right">
                    <i class="fa fa-clipboard" aria-hidden="true"></i>
                </button>
            </h3>
        </div>
    </div>
    <div class="animated flipInY col-lg-4 col-md-4 col-sm-6 col-xs-12">
        <div class="tile-stats">
            <div class="icon"><i class="fa fa-dollar"></i>
            </div>
            <div class="count">S/<?= $subscription['profit'] ?></div>
            <h3><?= $profit_btc ?> BTC</h3>

            <h3><?= lang('revenue') ?></h3>
        </div>
    </div>
</div>

<div class="row">
    <div class="col-md-12 col-sm-12 col-xs-12">
        <div class="x_panel">
            <div class="x_title">
                <h2><?= lang('daily-bonus') ?></h2>
                <div class="clearfix"></div>
            </div>
            <div class="x_content">
                <div class="col-md-9 col-sm-9 col-xs-12">
                    <?php if (empty($chart)) { ?>
                        <div class="text-center"><?= lang('no-data') ?></div>
                    <?php } ?>
                    <div id="graph_line" style="width:100%; height:300px;"></div>
                </div>
                <div class="col-md-3 col-sm-12 col-xs-12">
                    <div class="text-center">
                        <div class="x_title">
                            <h2><?= lang("days_remaining") ?></h2>
                            <div class="clearfix"></div>

                        </div>
                        <span id="gauge-text">0 <?= lang("days_remaining") ?></span>
                        <canvas id="gauge" class="" style="width: 100%;height: 220px;"></canvas>
                        <div class="goal-wrapper">
                            <span class="gauge-value pull-left"></span>
                            <span  class="gauge-value pull-left"><?= $subscription['period_days'] ?> <?= lang("days") ?></span>
                            <span id="goal-text" class="goal-value pull-right">0 <?= lang("days") ?></span>
                        </div>
                        <script>
                            $(document).ready(function () {
                                var opts = {
                                    lines: 12, // The number of lines to draw
                                    angle: 0, // The length of each line
                                    lineWidth: 0.4, // The line thickness
                                    pointer: {
                                        length: 0.75, // The radius of the inner circle
                                        strokeWidth: 0.042, // The rotation offset
                                        color: '#1D212A' // Fill color
                                    },
                                    limitMax: 'false', // If true, the pointer will not go past the end of the gauge
                                    colorStart: '#95c959', // Colors
                                    colorStop: '#95c959', // just experiment with them
                                    strokeColor: '#F0F3F3', // to see which ones work best for you
                                    generateGradient: true
                                };
                                var max_value = <?= (isset($subscription['period_days'])) ? $subscription['period_days'] : "0"  ?>;
                                var value_days = <?= (isset($subscription['period_days_left'])) ? $subscription['period_days_left'] : "0" ?>;
                                var target = document.getElementById('gauge'); // your canvas element
                                var gauge = new Gauge(target).setOptions(opts); // create sexy gauge!
                                gauge.maxValue = max_value; // set max gauge value
                                gauge.animationSpeed = 32; // set animation speed (32 is default value)
                                var gauge_set = (max_value - value_days);
                                gauge.set( (gauge_set != 0) ? gauge_set : 0.1 ); // set actual value
                                $("#gauge-text").text(value_days + ' <?= lang("days_remaining") ?>');
                            });
                        </script>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

<div class="row">
    <div class="col-md-3">
        <div class="x_panel">
            <div class="x_title">
                <h2><?= lang('recent_users') ?></h2>
                <div class="clearfix"></div>
            </div>
            <ul class="list-unstyled top_profiles scroll-view">
                <?php foreach($recent['recents'] as $recent) { ?>
                    <li class="media event">
                        <div class="pull-left border-aero profile_thumb recent-users">
                            <img src="/themes/<?= $this->settings->theme ?>/images/countries/<?= ($recent['country'] !='') ? strtolower($recent['country']) : "none"?>.gif" alt="">
                        </div>
                        <div class="media-body">
                            <a class="title" href="#"><?= ucfirst($recent['first_name'])." ".ucfirst($recent['last_name']) ?></a>
                            <p><?= $recent['country'] ?></p>
                            <p class="text-right">
                                <small><?= format_interval($recent['created']) ?></small>
                            </p>
                        </div>
                    </li>
                <?php } ?>
            </ul>
        </div>
    </div>

    <div class="col-md-9 col-sm-9 col-xs-12">
        <div class="x_panel">
            <div class="x_title">
                <h2><?= lang('latest_transactions') ?></h2>
                <div class="clearfix"></div>
            </div>
            <div class="x_content">
                <table id="datatable-responsive" class="table table-striped table-bordered dt-responsive nowrap"
                       cellspacing="0" width="100%">
                    <thead>
                    <tr>
                        <th><?= lang('date') ?></th>
                        <th><?= lang('code') ?></th>
                        <th><?= lang('description') ?></th>
                        <th><?= lang('value_btc') ?></th>
                        <th><?= lang('amount') ?></th>
                        <th><?= lang('status') ?></th>
                    </tr>
                    </thead>
                    <tbody>
                    <?php foreach ($transactions as $transaction) { ?>
                        <tr>
                            <td><?= $transaction['date'] ?></td>
                            <td><?= $transaction['cod'] ?></td>
                            <td><?= $transaction['description'] ?></td>
                            <td><?= $transaction['btc'] ?></td>
                            <td><?= $transaction['usd'] ?> <sub><?= empty($transaction['wallet'])? 'USD' : $transaction['wallet'] ?></sub></td>
                            <td><span class="badge <?= ($transaction['status'] == 'done') ? 'bg-green' :'' ?><?= ($transaction['status'] == 'pending') ? 'bg-orange' :'' ?>"><?= $transaction['status'] ?></span></td>
                        </tr>
                    <?php } ?>
                    </tbody>
                </table>

            </div>
        </div>
    </div>
</div>

<!--[if lte IE 8]>
<script type="text/javascript" src="js/excanvas.min.js"></script><![endif]-->
<script>var chart = <?= json_encode($chart) ?>;</script>

