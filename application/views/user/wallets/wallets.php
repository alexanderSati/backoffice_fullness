<?php defined('BASEPATH') OR exit('No direct script access allowed');

$lang = (isset($user['language']) ? $user['language'] : $this->config->item('language')); ?>



<!-- [ page content ] start -->

<div class="card px-3">

  <div class="card-block">



    <div class="row">

      <?php foreach ($wallets as $code => $value) : ?>

      <?php if($code == "USD"): ?>

      <div class="col-lg-5 col-md-12 col-sm-12 col-xs-12 wallet">

        <div class="card widget-statstic-card">

          <div class="card-header borderless content-<?= $code ?>">

  

             <div class="row align-items-center m-l-0">

              <div class="col-auto">

                <i class="fa fa-money text-info fa-3x" aria-hidden="true"></i>

              </div>

              <div class="col-auto">

                <h6 class="text-muted m-b-10">Saldo disponible total </h6>

                <h2 class="m-b-0">S/ <?= round($value['usd'], 2, PHP_ROUND_HALF_EVEN); ?> </h2>
                 <!-- <div class="count" style="font-size: 24px"> -->
               </div>

              </div>

            <div class="card-block">

              <i class="feather st-icon bg-c-blue">

                <img class="coin-logos wallet-icon" src="/themes/<?= $this->settings->theme ?>/images/coins/<?= $code ?>.png"

                  alt="">

              </i>

            </div>

          </div>

        </div>

      </div>

      <?php endif;?>

      <?php endforeach; ?>

    </div>





    <div class="row" >

      

      <div class="col-md-3" >

        <div class="card">

          <div class="card-block">

              <div class="row align-items-center m-l-0">

                  <div class="col-auto">

                    <i class="fa fa-money text-info fa-3x" aria-hidden="true"></i>

                  </div>

                  <div class="col-auto">

                      <h6 class="text-muted m-b-10"><?= lang("bonus_referral"); ?></h6>

                      <h2 class="m-b-0">S/ <?= !empty( $bonos["referral"] )?  round($bonos["referral"], 2, PHP_ROUND_HALF_UP):0 ?></h2>

                  </div>

              </div>

          </div>

        </div>

      </div>
      
      
      <div class="col-md-3" >

        <div class="card">

          <div class="card-block">

              <div class="row align-items-center m-l-0">

                  <div class="col-auto">

                    <i class="fa fa-money text-info fa-3x" aria-hidden="true"></i>

                  </div>

                  <div class="col-auto">

                      <h6 class="text-muted m-b-10">Bono binario</h6>

                      <h2 class="m-b-0">S/ <?= !empty( $bonos["binary"] )?  round($bonos["binary"], 2, PHP_ROUND_HALF_UP):0 ?></h2>

                  </div>

              </div>

          </div>

        </div>

      </div>



      <div class="col-md-3" >

        <div class="card">

          <div class="card-block">

              <div class="row align-items-center m-l-0">

                  <div class="col-auto">

                    <i class="fa fa-money text-info fa-3x" aria-hidden="true"></i>

                  </div>

                  <div class="col-auto">

                      <h6 class="text-muted m-b-10"><?= lang("bonus_activation"); ?></h6>

                      <h2 class="m-b-0">S/ <?= !empty( $bonos["activation"] )? round($bonos["activation"], 2, PHP_ROUND_HALF_UP):0 ?></h2>

                  </div>

              </div>

          </div>

        </div>

      </div>





      <div class="col-md-3" >

        <div class="card">

          <div class="card-block">

              <div class="row align-items-center m-l-0">

                  <div class="col-auto">

                    <i class="fa fa-money text-info fa-3x" aria-hidden="true"></i>

                  </div>

                  <div class="col-auto">

                      <h6 class="text-muted m-b-10"><?= lang("bonus_infinity"); ?></h6>

                      <h2 class="m-b-0">S/ <?= !empty( $bonos["infinity"] )? $bonos["infinity"]:0 ?></h2>

                  </div>

              </div>

          </div>

        </div>

      </div>





      <div class="col-md-3" >

        <div class="card">

          <div class="card-block">

              <div class="row align-items-center m-l-0">

                  <div class="col-auto">

                    <i class="fa fa-money text-info fa-3x" aria-hidden="true"></i>

                  </div>

                  <div class="col-auto">

                      <h6 class="text-muted m-b-10"><?= lang("bonus_fullness"); ?></h6>

                      <h2 class="m-b-0">S/ <?= !empty( $bonos["bonus_fullness_global"] )? $bonos["bonus_fullness_global"]:0 ?></h2>

                  </div>

              </div>

          </div>

        </div>

      </div>





      <div class="col-md-3" >

        <div class="card">

          <div class="card-block">

              <div class="row align-items-center m-l-0">

                  <div class="col-auto">

                    <i class="fa fa-money text-info fa-3x" aria-hidden="true"></i>

                  </div>

                  <div class="col-auto">

                      <h6 class="text-muted m-b-10"><?= lang("bonus_upgrade"); ?></h6>

                      <h2 class="m-b-0">S/ <?= !empty( $bonos["upgrade"] )? $bonos["upgrade"]:0 ?></h2>

                  </div>

              </div>

          </div>

        </div>

      </div>





      <?php if(false): ?>

      <div class="col-md-3" >

        <div class="card">

          <div class="card-block">

              <div class="row align-items-center m-l-0">

                  <div class="col-auto">

                    <i class="fa fa-money text-info fa-3x" aria-hidden="true"></i>

                  </div>

                  <div class="col-auto">

                      <h6 class="text-muted m-b-10"><?= lang("wallet_payment"); ?></h6>

                      <h2 class="m-b-0">S/ -<?= !empty( $bonos["wallet_payment"] )? $bonos["wallet_payment"]:0 ?></h2>

                  </div>

              </div>

          </div>

        </div>

      </div>





      <div class="col-md-3" >

        <div class="card">

          <div class="card-block">

            <div class="row align-items-center m-l-0">

              <div class="col-auto">

                <i class="fa fa-money text-info fa-3x" aria-hidden="true"></i>

              </div>

              <div class="col-auto">

                <h6 class="text-muted m-b-10"><?= lang("label_payout"); ?></h6>

                <h2 class="m-b-0">S/ -<?= !empty( $bonos["payout"] )? $bonos["payout"]:0 ?></h2>

              </div>

            </div>

          </div>

        </div>

      </div>

      <?php endif; ?>





      <div class="col-md-3" >

        <div class="card">

          <div class="card-block">

            <div class="row align-items-center m-l-0">

              <div class="col-auto">

                <i class="fa fa-money text-info fa-3x" aria-hidden="true"></i>

              </div>

              <div class="col-auto">

                <h6 class="text-muted m-b-10"><?= lang("label_bonus_award"); ?></h6>

                <h2 class="m-b-0">S/ <?= !empty( $bonos["bonus_award"] )? $bonos["bonus_award"]:0 ?></h2>

              </div>

            </div>

          </div>

        </div>

      </div>



      <div class="col-md-3" >

        <div class="card">

          <div class="card-block">

            <div class="row align-items-center m-l-0">

              <div class="col-auto">

                <i class="fa fa-money text-info fa-3x" aria-hidden="true"></i>

              </div>

              <div class="col-auto">

                <h6 class="text-muted m-b-10"><?= lang("label_bonus_external_sales"); ?></h6>

                <h2 class="m-b-0">S/ <?= !empty( $bonos["bonus_external_sales"] )? $bonos["bonus_external_sales"]:0 ?></h2>

              </div>

            </div>

          </div>

        </div>

      </div>







    </div>



    <!-- end of [ row line 8 ] -->

    <div class="row">

      <div class="col-md-12 col-sm-12 col-xs-12">

        <div class="card">

          <div class="card-header">

            <ul class="nav nav-tabs card-header-tabs  justify-content-between">

              <li class="nav-item">

                <h4><?= $page_title ?></h4>

              </li>

              <li class="nav-item">

               <button type="button" class="btn btn-primary btn-round" data-toggle="modal" data-target="#default-Modal"><?= lang('users title month-wallet')?></button>

              </li>

            </ul>

          </div>

          <div class="card-block">

            <div class="dt-responsive table-responsive">

              <table id="datatable-responsive" class="table table-striped table-bordered nowrap">

                <thead>

                  <tr>

                    <th><?= lang('date') ?></th>

                    <th><?= lang('code') ?></th>

                    <th><?= lang('description') ?></th>                   

                    <th><?= lang('total') ?></th>

                    <th><?= lang('status') ?></th>

                  </tr>

                </thead>

                <tbody>

                  <?php if (isset($transactions) and $transactions != false) {

                                    foreach ($transactions as $transaction) { ?>

                  <tr>

                    <td><?= $transaction['date'] ?></td>

                    <td><?= $transaction['cod'] ?></td>

                    <td><?= $transaction['description'] ?></td>

                    <td><?= $transaction['usd'] ?>

                      <sub>S/</sub></td>

                    <td >

                      <span class="label label-<?php 

                        if ($transaction['status'] == "done") {

                          echo 'success';

                        } else if ($transaction['status'] == "pending" || $transaction['status'] == "waiting" ) {

                          echo 'warning';

                        } else if($transaction['status'] == "rejected" || $transaction['status'] == "cancelled" || $transaction['status'] == "failed") {

                          echo 'danger';

                        }

                    ?>">

                        <?= $transaction['status'] ?>

                      </span>

                    </td>

                  </tr>

                  <?php }

                      } ?>

                </tbody>

              </table>

            </div>

          </div>

        </div>

      </div>

    </div>

    <!-- end of [ row line 27 ] -->

  </div>

  <!-- end of card-block -->

</div>

<!-- [ page content ] end -->

<!-- [modal] start --->

<div class="modal fade show" id="default-Modal" tabindex="-1" role="dialog" style="z-index: 1050; display: none;">

    <div class="modal-dialog" role="document">

        <div class="modal-content">

            <div class="modal-header">

                <h4 class="modal-title"><?= lang('users title month-wallet')?></h4>

                <button type="button" class="close" data-dismiss="modal" aria-label="Close">

                    <span aria-hidden="true">×</span>

                </button>

            </div>

            <?php echo form_open_multipart('', array('id'=>'wallet-by-month', 'class' => 'wizard-form', 'data-parsley-validate' => '','role' => 'form')); ?>

            <div class="modal-body">

                <strong for="month-wallets"><?=lang('date_month') ?>:</strong>

                <input type="date" name="month-wallets" id="month-wallets" required>  

            </div>

            <div class="modal-footer">

                <button type="button" class="btn btn-default waves-effect " data-dismiss="modal"><?= lang('core button close')?></button>

                <button type="submit" class="btn btn-primary waves-effect waves-light "><?= lang('core button search')?></button>

            </div>

            <?php echo form_close(); ?>     

        </div>

    </div>

</div>

<!-- [modal] end --->