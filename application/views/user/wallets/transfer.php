<?php defined('BASEPATH') OR exit('No direct script access allowed');
$lang = (isset($user['language']) ? $user['language'] : $this->config->item('language'));
$select_wallets = array("XMR"=>"Monero", "ETH"=>"Ethereum", "LTC"=>"Litecoin", "TRUMP"=>"Trumpcoin", "DASH"=>"Dash", "SLR"=>"Solarcoin" )?>

<div class="row">
    <div class="col-md-12 col-sm-12 col-xs-12">

        <div class="x_panel">
          <div class="x_title">
            <h2><i class="fa fa-btc" aria-hidden="true"></i> <?php echo ucfirst(lang('make-trading')); ?></h2>
 
            <div class="clearfix"></div>
          </div>
          <div class="x_content">


            <div id="currencies_tabs" role="tabpanel" data-example-id="togglable-tabs">
              <ul id="myTab" class="nav nav-tabs bar_tabs" role="tablist">
                <?php foreach ($currencies as $currency) { ?>
                    <li role="presentation" tab="<?php echo strtolower($currency['name']); ?>" class="<?php echo strtolower($currency['name']) == 'bitcoin' ? 'active' : '' ?>"><a href="#tab_content_<?php echo strtolower($currency['name']); ?>" id="home-tab" role="tab" data-toggle="tab" aria-expanded="true"><?php echo $currency['name']; ?></a>
                    </li>
                <?php } ?>
              </ul>
              <div id="myTabContent" class="tab-content">
                <?php foreach ($currencies as $currency) { ?>
                    <div role="tabpanel" class="tab-pane fade in <?php echo strtolower($currency['name']) == 'bitcoin' ? 'active' : '' ?>" id="tab_content_<?php echo strtolower($currency['name']); ?>" aria-labelledby="home-tab">
                      <div class="animated flipInY col-lg-4 col-md-6 col-sm-6 col-xs-12">
                          <div class="tile-stats wallets content-<?= $currency['symbol'] ?>">
                              <div class="pull-right coin-logos">
                                  <img src="/themes/<?= $this->settings->theme ?>/images/coins/<?= $currency['symbol'] ?>.png" alt="">
                              </div>
                              <div class="count" style="font-size: 24px">
                                  <?= $currency['price_btc'] ?> BTC
                              </div>
                              <div class="count" style="font-size: 24px">
                                  <?= $currency['price_usd'] ?> USD
                              </div>
                              <div class="count" style="font-size: 24px">
                                  <?= $currency['name'] ?>
                              </div>
                          </div>
                      </div>
                      <div class="col-lg-8 col-md-6 col-sm-6 col-xs-12">
                          <a class="btn btn-warning center-block make_trading col-xs-2 <?php echo strtolower($currency['name']) == 'bitcoin' ? 'hide' : '' ?>" href="javascript:$('#trading').removeClass('hide');setTrading('<?php echo strtolower($currency['name']); ?>')"><?php echo ucfirst(lang('make-trading')); ?></a>
                      </div>
                      <br>
                      <iframe src="/chart.php?coin=<?php echo strtolower($currency['name']); ?>"  width="100%" height="585" style="border:none;"></iframe>
                    </div>
                <?php } ?>
               
              </div>
            </div>

          </div>
        </div>

        <div class="x_panel hide" id="trading">
            <div class="x_title">
                <h2><?= lang("users title wallets transfer") ?></h2>
                <div class="clearfix"></div>
            </div>
            <div class="x_content">
                <?php echo form_open('', array('class' => 'form-horizontal form-label-left', 'data-parsley-validate' => '','role' => 'form')); ?>

                <div class="notifications-transfer"></div>

                <div class="item form-group">
                    <?php echo form_label(lang('origen-wallet') . ' *', 'wallet', array('class' => 'control-label col-md-1 col-sm-1 col-xs-12')); ?>
                    <div class="col-md-4 col-sm-4 col-xs-12">
                        <?php echo form_input(array('name' => 'wallet', 'id'=>'wallet', 'required'=>'required', 'readonly' => 'readonly', 'value' => " BTC (operative) ". $btc_user , 'class' => 'form-control col-md-7 col-xs-12')); ?>
                    </div>
                </div>

                <div class="item form-group">
                    <?php echo form_label(lang('coin-tobuy'), 'wallet-to', array('class' => 'control-label col-md-1 col-sm-1 col-xs-12')); ?>
                    <div class="col-md-4 col-sm-4 col-xs-12">
                        <?php echo form_dropdown('wallet-to', $select_wallets, "", 'id="type" required="required" class="form-control col-md-7 col-xs-12"'); ?>
                    </div>
                </div>

                <div class="item form-group">
                    <?php echo form_label(lang('amount') . ' *', 'amount', array('class' => 'control-label col-md-1 col-sm-1 col-xs-12')); ?>
                    <div class="col-md-4 col-sm-4 col-xs-12">
                        <?php echo form_input(array('name' => 'amount', 'id'=>'amount-trading', 'required'=>'required', 'value' => '', 'class' => 'form-control col-md-7 col-xs-12')); ?>
                        <p><?= lang('amount-trading'); ?></p>
                    </div>
                </div>

                <div class="item form-group">
                    <?php echo form_label(lang('dollar-price') . ' *', 'amount_usd', array('class' => 'control-label col-md-1 col-sm-1 col-xs-12')); ?>
                    <div class="col-md-4 col-sm-4 col-xs-12">
                        <?php echo form_input(array('name' => 'amount_usd', 'id'=>'amount_usd', 'required'=>'required','readonly'=>'readonly', 'value' => '', 'class' => 'form-control col-md-7 col-xs-12')); ?>
                    </div>
                </div>

                <div class="item form-group">
                    <?php echo form_label(lang('security-pin') . ' *', 'security-pin', array('class' => 'control-label col-md-1 col-sm-1 col-xs-12')); ?>
                    <div class="col-md-4 col-sm-4 col-xs-12">
                        <?php echo form_password(array('name' => 'security-pin', 'id'=>'security-pin','maxlength' => "4",'required'=>'required', 'value' => '', 'class' => 'form-control col-md-7 col-xs-12', 'autocomplete' => 'off')); ?>
                    </div>
                    <div><?= lang("verify-spam") ?></div> 
                </div>

                <div class="ln_solid"></div>

                <div class="form-group">
                    <div class="col-md-6 col-md-offset-1 col-xs-12">
                        <button type="submit" name="submit" class="btn btn-success"><span
                                class="glyphicon glyphicon-save"></span> <?php echo lang('pay'); ?></button>                               
                    </div>
                </div>

                <?php echo form_close(); ?>
            </div>
        </div>
    </div>
    
    <div class="col-md-4 col-sm-6 col-xs-12">
        <div class="x_panel">
            <div class="x_title">
                <h2><i class="fa fa-money" aria-hidden="true"></i> <?= lang('buy_trading') ?></h2>
                <div class="clearfix"></div>
            </div>
            <div class="x_content">
                <?php echo form_open('', array('class' => 'form-horizontal form-label-left', 'data-parsley-validate' => '','role' => 'form')); ?>
                    <div class="item form-group">
                        <?php echo form_label(lang('balance') . ' <sub>(USD)</sub>', '', array('class' => 'control-label col-md-4 col-sm-2 col-xs-12')); ?>
                        <div class="col-md-8 col-sm-10 col-xs-12">
                            <?php echo form_label('0', '',  array('id' =>'balance_buy', 'class' => 'control-label col-md-12 col-sm-12 col-xs-12')); ?>
                        </div>
                    </div>
                    <div class="item form-group hidden">
                        <?php echo form_label(lang('origen-wallet') . ' *', 'wallet', array('class' => 'control-label col-md-4 col-sm-2 col-xs-12')); ?>
                        <div class="col-md-8 col-sm-4 col-xs-12">
                            <?php echo form_input(array('name' => 'wallet', 'id'=>'wallet_buy', 'required'=>'required', 'readonly' => 'readonly', 'value' => " BTC (operative) ". $btc_user , 'class' => 'form-control col-md-7 col-xs-12')); ?>
                        </div>
                    </div>

                    <div class="item form-group">
                        <?php echo form_label(lang('coin-tobuy'), 'wallet-to', array('class' => 'control-label col-md-4 col-sm-2 col-xs-12')); ?>
                        <div class="col-md-8 col-sm-4 col-xs-12">
                            <?php echo form_dropdown('wallet-to', $select_wallets, "", 'id="coin_to_buy" required="required" class="form-control col-md-7 col-xs-12"'); ?>
                        </div>
                    </div>
                    <div class="item form-group">
                        <?php echo form_label(lang('amount') . ' <sub></sub> *', 'amount', array('id' => 'amount_buy_label', 'class' => 'control-label col-md-4 col-sm-2 col-xs-12')); ?>
                        <div class="div_input col-md-8 col-sm-10 col-xs-12">
                            <?php echo form_input(array('name' => 'amount', 'id'=>'amount_trading_buy', 'required'=>'required', 'class' => 'form-control col-md-7 col-xs-12')); ?>
                            <span class="currency currency_to_buy"></span>
                        </div>
                    </div>
                    <div class="item form-group">
                        <?php echo form_label(lang('dollar-price') . ' <sub>(USD)</sub> *', 'amount_usd', array('class' => 'control-label col-md-4 col-sm-2 col-xs-12')); ?>
                        <div class="div_input col-md-8 col-sm-10 col-xs-12">
                            <?php echo form_input(array('name' => 'amount_usd', 'id'=>'amount_usd_buy', 'required'=>'required', 'class' => 'form-control col-md-7 col-xs-12')); ?>
                            <span class="currency"><?php echo lang('dollar'); ?></span>
                        </div>
                    </div>
                    <div class="item form-group">
                        <?php echo form_label(lang('total') . ' <sub>(USD)</sub> *', 'total', array('class' => 'control-label col-md-4 col-sm-2 col-xs-12')); ?>
                        <div class="div_input col-md-8 col-sm-10 col-xs-12">
                            <?php echo form_input(array('name' => 'total', 'id'=>'total_buy', 'required'=>'required', 'readonly' => 'readonly', 'class' => 'form-control col-md-7 col-xs-12')); ?>
                            <span class="currency"><?php echo lang('dollar'); ?></span>
                        </div>
                    </div>
                    <div class="item form-group">
                        <?php echo form_label(lang('fee') . ' <sub>(USD)</sub>', 'fee', array('class' => 'control-label col-md-4 col-sm-2 col-xs-12')); ?>
                        <div class="div_input col-md-8 col-sm-10 col-xs-12">
                            <?php echo form_input(array('name' => 'fee', 'id'=>'fee_buy', 'required'=>'required', 'readonly' => 'readonly', 'class' => 'form-control col-md-7 col-xs-12')); ?>
                            <span class="currency"><?= lang('dollar'); ?></span>
                        </div>
                    </div>
                    <div class="item form-group">
                        <?php echo form_label(lang('total_fee') . ' <sub>(USD)</sub>', 'total_fee', array('class' => 'control-label col-md-4 col-sm-2 col-xs-12')); ?>
                        <div class="div_input col-md-8 col-sm-10 col-xs-12">
                            <?php echo form_input(array('name' => 'total_fee', 'id'=>'total_fee_buy', 'required'=>'required', 'readonly' => 'readonly', 'class' => 'form-control col-md-7 col-xs-12')); ?>
                            <span class="currency"><?php echo lang('dollar'); ?></span>
                        </div>
                    </div>
                    <div class="item form-group">
                        <?php echo form_label(lang('security-pin') . ' *', 'security-pin', array('class' => 'control-label col-md-4 col-sm-1 col-xs-12')); ?>
                        <div class="col-md-8 col-sm-4 col-xs-12">
                            <?php echo form_password(array('name' => 'security-pin', 'id'=>'security-pin-buy','maxlength' => "4",'required'=>'required', 'value' => '', 'class' => 'form-control col-md-12 col-xs-12', 'autocomplete' => 'off')); ?>
                        </div>
                        <div class="col-md-offset-4"><?= lang("verify-spam") ?></div> 
                    </div>
                    <div class="ln_solid"></div>

                    <div class="form-group">
                        <div class="col-md-8 col-md-offset-4 col-xs-12">
                            <button type="submit" name="submit" class="btn btn-success"><span
                                    class="glyphicon glyphicon-save"></span> <?php echo lang('buy_trading'); ?></button>                               
                        </div>
                    </div>
                <?php echo form_close(); ?>
            </div>
        </div>   
    </div>
    <div class="col-md-4 col-sm-6 col-xs-12">
        <div class="x_panel">
            <div class="x_title">
                <h2><i class="fa fa-money" aria-hidden="true"></i> <?= lang('sell') ?></h2>
                <div class="clearfix"></div>
            </div>
            <div class="x_content">
                <?php echo form_open('', array('class' => 'form-horizontal form-label-left', 'data-parsley-validate' => '','role' => 'form')); ?>
                    <div class="item form-group">
                        <?php echo form_label(lang('balance') . ' <sub></sub>', '', array('id' =>'label_balance_sell', 'class' => 'control-label col-md-4 col-sm-2 col-xs-12')); ?>
                        <div class="col-md-8 col-sm-10 col-xs-12">
                            <?php echo form_label('0', '', array('id' =>'balance_sell', 'class' => 'control-label col-md-12 col-sm-2 col-xs-12')); ?>
                        </div>
                    </div>
                    <div class="item form-group hidden">
                        <?php echo form_label(lang('origen-wallet') . ' *', 'wallet', array('class' => 'control-label col-md-4 col-sm-2 col-xs-12')); ?>
                        <div class="col-md-8 col-sm-4 col-xs-12">
                            <?php echo form_input(array('name' => 'wallet', 'id'=>'wallet_sell', 'required'=>'required', 'readonly' => 'readonly', 'value' => " BTC (operative) ". $btc_user , 'class' => 'form-control col-md-7 col-xs-12')); ?>
                        </div>
                    </div>

                    <div class="item form-group">
                        <?php echo form_label(lang('coin-to-sell'), 'wallet-to', array('class' => 'control-label col-md-4 col-sm-2 col-xs-12')); ?>
                        <div class="col-md-8 col-sm-4 col-xs-12">
                            <?php echo form_dropdown('wallet-to', $select_wallets, "", 'id="coin_to_sell" required="required" class="form-control col-md-7 col-xs-12"'); ?>
                        </div>
                    </div>
                    <div class="item form-group">
                        <?php echo form_label(lang('amount') . ' <sub></sub> *', 'amount', array('id' => 'amount_sell_label', 'class' => 'control-label col-md-4 col-sm-2 col-xs-12')); ?>
                        <div class="div_input col-md-8 col-sm-10 col-xs-12">
                            <?php echo form_input(array('name' => 'amount', 'id'=>'amount_trading_sell', 'required'=>'required', 'class' => 'form-control col-md-7 col-xs-12')); ?>
                            <span class="currency currency_to_sell"></span>
                        </div>
                    </div>
                    <div class="item form-group">
                        <?php echo form_label(lang('dollar-price') . ' <sub>(USD)</sub> *', 'amount_usd', array('class' => 'control-label col-md-4 col-sm-2 col-xs-12')); ?>
                        <div class="div_input col-md-8 col-sm-10 col-xs-12">
                            <?php echo form_input(array('name' => 'amount_usd', 'id'=>'amount_usd_sell', 'required'=>'required', 'class' => 'form-control col-md-7 col-xs-12')); ?>
                            <span class="currency"><?php echo lang('dollar'); ?></span>
                        </div>
                    </div>
                    <div class="item form-group">
                        <?php echo form_label(lang('total') . ' <sub>(USD)</sub> *', 'total', array('class' => 'control-label col-md-4 col-sm-2 col-xs-12')); ?>
                        <div class="div_input col-md-8 col-sm-10 col-xs-12">
                            <?php echo form_input(array('name' => 'total', 'id'=>'total_sell', 'required'=>'required', 'readonly' => 'readonly', 'class' => 'form-control col-md-7 col-xs-12')); ?>
                            <span class="currency"><?php echo lang('dollar'); ?></span>
                        </div>
                    </div>
                    <div class="item form-group">
                        <?php echo form_label(lang('fee') . ' <sub>(USD)</sub>' , 'fee', array('class' => 'control-label col-md-4 col-sm-2 col-xs-12')); ?>
                        <div class="div_input col-md-8 col-sm-10 col-xs-12">
                            <?php echo form_input(array('name' => 'fee', 'id'=>'fee_sell', 'required'=>'required', 'readonly' => 'readonly', 'class' => 'form-control col-md-7 col-xs-12')); ?>
                            <span class="currency"><?php echo lang('dollar'); ?></span>
                        </div>
                    </div>
                    <div class="item form-group">
                        <?php echo form_label(lang('total_fee') . ' <sub>(USD)</sub>', 'total_fee', array('class' => 'control-label col-md-4 col-sm-2 col-xs-12')); ?>
                        <div class="div_input col-md-8 col-sm-10 col-xs-12">
                            <?php echo form_input(array('name' => 'total_fee', 'id'=>'total_fee_sell', 'required'=>'required', 'readonly' => 'readonly', 'class' => 'form-control col-md-7 col-xs-12')); ?>
                            <span class="currency"><?php echo lang('dollar'); ?></span>
                        </div>
                    </div>
                    <div class="item form-group">
                        <?php echo form_label(lang('security-pin') . ' *', 'security-pin', array('class' => 'control-label col-md-4 col-sm-1 col-xs-12')); ?>
                        <div class="col-md-8 col-sm-4 col-xs-12">
                            <?php echo form_password(array('name' => 'security-pin', 'id'=>'security-pin-sell','maxlength' => "4",'required'=>'required', 'value' => '', 'class' => 'form-control col-md-12 col-xs-12', 'autocomplete' => 'off')); ?>
                        </div>
                        <div class="col-md-offset-4"><?= lang("verify-spam") ?></div> 
                    </div>
                    <div class="ln_solid"></div>

                    <div class="form-group">
                        <div class="col-md-6 col-md-offset-4 col-xs-12">
                            <button type="submit" name="submit" class="btn btn-success"><span
                                    class="glyphicon glyphicon-open"></span> <?php echo lang('sell'); ?></button>                               
                        </div>
                    </div>
                <?php echo form_close(); ?>
            </div>
        </div>
    </div>

    <div id="trade_history" class="col-md-4 col-sm-6 col-xs-12">
        <div class="x_panel">
            <div class="x_title">
                <h2><i class="fa fa-list-alt" aria-hidden="true"></i> <?= lang('trade_history') ?></h2>
                <div class="clearfix"></div>
            </div>
            <div class="x_content">
                <table class="table table-striped dt-responsive nowrap">
                    <thead>
                        <tr>
                            <th>Time</th>
                            <th></th>
                            <th>Price</th>
                            <th class="currency_history"><?php echo lang('dollar'); ?></th>
                        </tr>
                    </thead>
                    <tbody>
                        <?php for($i=0; $i < 15; $i++){ ?>
                        <tr class="buy_order">
                            <td class="date_trading"><?= date('d/m/Y'); ?></td>
                            <td>BUY</td>
                            <td>113.99662</td>
                            <td>26</td>
                        </tr>
                        <tr class="sell_order">
                            <td class="date_trading"><?= date('d/m/Y'); ?></td>
                            <td>SELL</td>
                            <td>113.99662</td>
                            <td>26</td>
                        </tr>
                        <?php } ?>
                    </tbody>
                </table>
            </div>
        </div>
    </div>

    <div id="sell_order" class="col-md-4 col-sm-6 col-xs-12">
        <div class="x_panel">
            <div class="x_title">
                <h2><span class="glyphicon glyphicon glyphicon-stats" aria-hidden="true"></span> <?= lang('sell_order') ?></h2>
                <div class="clearfix"></div>
            </div>
            <div class="x_content">
                <table class="table table-striped dt-responsive nowrap">
                    <thead>
                        <tr>
                            <th>Price</th>
                            <th class="currency_history">ETH</th>
                            <th>USD</th>
                            <th></th>
                        </tr>
                    </thead>
                    <tbody>
                        <?php for($i=0; $i < 10; $i++){ ?>
                        <tr>
                            <td>0.11399999</td>
                            <td>0.02456299</td>
                            <td>0.00574569</td>
                            <td><a class="badge bg-blue" href="">Cancel</a></td>
                        </tr>
                        <?php } ?>
                    </tbody>
                </table>
            </div>
        </div>
    </div>

    <div id="buy_order" class="col-md-4 col-sm-6 col-xs-12">
        <div class="x_panel">
            <div class="x_title">
                <h2><span class="glyphicon glyphicon glyphicon-stats" aria-hidden="true"></span> <?= lang('buy_order') ?></h2>
                <div class="clearfix"></div>
            </div>
            <div class="x_content">
                <table class="table table-striped dt-responsive nowrap">
                    <thead>
                        <tr>
                            <th>Time</th>
                            <th></th>
                            <th>Price</th>
                            <th class="currency_history">ETH</th>
                        </tr>
                    </thead>
                    <tbody>
                        <?php for($i=0; $i < 10; $i++){ ?>
                        <tr>
                            <td><?= now(); ?></td>
                            <td>BUY</td>
                            <td>0.11399662</td>
                            <td>266760000</td>
                        </tr>
                        <?php } ?>
                    </tbody>
                </table>
            </div>
        </div>
    </div>

    <div class="clearfix"></div>
</div>