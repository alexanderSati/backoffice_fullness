<?php defined('BASEPATH') OR exit('No direct script access allowed');



$lang = (isset($user['language']) ? $user['language'] : $this->config->item('language')); 



?>

<div class="row">

    <div class="col-xl-3">

        <div class="card">

            <div class="card-block">

                <div class="row align-items-center m-l-0">

                    <div class="col-auto">

                        <i class="feather icon-thumbs-up f-28"></i>

                    </div>



                    <div class="col-auto">

                        <h6 class="text-muted m-b-10"><?= ucwords(lang('points')) ?></h6>

                        <h2 class="m-b-0 f-30"><?php echo $residual_pp ?> <h2>

                    </div>

                </div>

            </div>

        </div>

    </div>





    <div class="col-xl-3">

        <div class="card">

            <div class="card-block">

                <div class="row align-items-center m-l-0" style="height: 4.3em;">



                    <div class="col-auto">

                        <i class="fa fa-star" style="font-size: 28px;"></i>

                    </div>

                    <div class="col-auto">

                        <h6 class="text-muted m-b-10"><?= lang('current-rank') ?></h6>

                        <h2 class="m-b-0 m-b-0 f-20" style="height: 1.2em;" ><?php echo _rank_title($rank["id"]) ?><h2>

                    </div>

                </div>

            </div>

        </div>

    </div>





    <div class="col-xl-3">

        <div class="card">

            <div class="card-block">

                <div class="row align-items-center m-l-0">

                    <div class="col-auto">

                        <i class="fa fa-credit-card" style="font-size: 28px;"></i>

                    </div>

                    <div class="col-auto">

                        <h6 class="text-muted m-b-10"><?= lang('users title bonus') ?></h6>

                        <h2 class="m-b-0 f-30">S/ <?php echo $bonus ?><h2>

                    </div>

                </div>

            </div>

        </div>

    </div>



    <div class="col-xl-3">

        <div class="card">

            <div class="card-block">

                <div class="row align-items-center m-l-0">

                    <div class="col-auto">

                        <i class="fa fa-group" style="font-size: 28px;"></i>

                    </div>



                    <div class="col-auto">

                        <h6 class="text-muted m-b-10"><?= lang('users title grupal-point') ?></h6>

                        <h2 class="m-b-0 f-30"><?php echo $residual_pg?><h2>

                    </div>

                </div>

            </div>

        </div>

    </div>

</div>





<div class="row">

    <div class="col-md-12 col-sm-12 col-xs-12">

        <div class="card">            

                <div class="card-header">

                    <h2><?= lang('users title unilevel') ?></h2>

                    <div class="clearfix"></div>

                </div>

                <div class=" card-block" style="min-height: 50vh">

                    <div class="chart" style="width:100%;overflow: auto; height: 96%;" id="tree"></div>

                    <div>

                    <h4><b style="font-size: 1.5em;margin-top: 1em;display: block;margin-bottom: 1em;" >Liquidación Residual</b></h4>

                </div>

                <div>

                    <?php 



                        $table="<table class='table' >";

                        // $table.="

                        //     <thead>

                        //         <tr>

                        //             <th>{$residual_liquidation[0][0]}</th>

                        //             <th>{$residual_liquidation[0][1]}</th>

                        //             <th>{$residual_liquidation[0][2]}</th>

                        //             <th>{$residual_liquidation[0][3]}</th>

                        //             <th>{$residual_liquidation[0][4]}</th>

                        //             <th>{$residual_liquidation[0][5]}</th>

                        //             <th>{$residual_liquidation[0][6]}</th>

                        //             <th>{$residual_liquidation[0][7]}</th>

                        //             <th>{$residual_liquidation[0][8]}</th>

                        //         </tr>

                        //     </thead>

                        // ";

                         $table.="

                            <thead>

                                <tr>
                                    <th>Nombre</th>
                                    <th>Plan</th>
                                    <th>Ptos Personales</th>
                                    <th>Ptos Grupales</th>
                                    <th>Ptos Totales</th>                                                                    
                                    <th>Rango</th>
                                    <th>Porcentaje</th>     

                                </tr>

                            </thead>

                        ";


                        for($i = 0; $i< count($referrals); $i++){

                            $table.="

                                <tr>
                                <td>{$referrals[$i]['first_name']} {$referrals[$i]['last_name']}</td>
                                <td>{$referrals[$i]['spanish_name']}</td>
                                <td>{$referrals[$i]['pp']}</td>
                                <td>{$referrals[$i]['gp']}</td>
                                <td>{$referrals[$i]['pt']}</td>                         
                                <td>{$referrals[$i]['rank']}</td>
                                <td>{$referrals[$i]['percentage_profit']}</td>                     
                                </tr>

                            ";

                        } 

                        $table .= "</table>";

                        echo $table;

                    ?>

                </div>    

        </div>

    </div>

</div>



<script>

$(document).ready(function() {



    var chart_config = {

        chart: {

            container: "#tree",

            scrollbar: 'fancy',

            animateOnInit: true,

            rootOrientation: 'NORTH',

            connectors: {

                type: "step",

                style: {

                    stroke: "#0e6983",

                    'stroke-width': 3

                }

            },

            node: {

                collapsable: true

            },

            animation: {

                nodeAnimation: "easeOutBounce",

                nodeSpeed: 700,

                connectorsAnimation: "bounce",

                connectorsSpeed: 700

            }

        },

        nodeStructure: <?= (isset($unilevel)) ? json_encode($unilevel,JSON_PRETTY_PRINT) : null ?>

    };



    tree = new Treant(chart_config);

});

</script>