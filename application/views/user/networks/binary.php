<?php defined('BASEPATH') OR exit('No direct script access allowed');


$lang = (isset($user['language']) ? $user['language'] : $this->config->item('language')); ?>

<div class="row">
    <div class="col-xl-4">
        <div class="card">
            <div class="card-block">
                <div class="row align-items-center m-l-0">

                    <div class="col-auto">
                        <i class="icon feather icon-users f-30 text-c-red"></i>
                    </div>

                    <div class="col-auto">
                        <h6 class="text-muted m-b-10"><?= lang('left-points') ?></h6>
                        <h2 class="m-b-0 f-30"><?= $ptos_izq ?></h2>
                    </div>
                </div>
            </div>
        </div>
    </div>


    <div class="col-xl-4">
        <div class="card">
            <div class="card-block">
                <div class="row align-items-center m-l-0">
                    <div class="col-auto">
                        <i class="icon feather icon-users f-30 text-c-blue"></i>
                    </div>
                    <div class="col-auto">
                        <h6 class="text-muted m-b-10"><?= lang('right-points') ?></h6>
                        <h2 class="m-b-0 f-30"><?= $ptos_der ?></h2>
                    </div>
                </div>
            </div>
        </div>

    </div>


    <div class="col-xl-4">
        <div class="card">
            <div class="card-block">
                <div class="row align-items-center m-l-0">

                    <div class="col-auto">
                        <i class="icon feather icon-navigation-2 f-30 text-c-green"></i>
                    </div>

                    <div class="col-auto">

                        <h6 class="text-muted m-b-10"><?= lang('profit') ?></h6>
                        <h2 class="m-b-0 f-30">S/<?= number_format($bonus, 2) ?></h2>

                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

<div class="row">
    <div class="col-md-12 col-sm-12 col-xs-12">
        <div class="card">            
            <div class="card-header">
                <h2><?= lang('users title binary') ?></h2>
                <div class="clearfix"></div>
            </div>


            <div class="card-block" style="min-height: 70vh">
                <div class="text-center">
                    <?php echo lang('users input side-left'); ?>
                    <i class="fa fa-caret-square-o-left" style="font-size:28px;"></i>
                    &nbsp;&nbsp;&nbsp;&nbsp;
                    <i class="fa fa-caret-square-o-right" style="font-size:28px;"></i>
                    <?php echo lang('users input side-right'); ?>
                </div>
                <div class="chart" style="width:100%; height: 96%; overflow: auto;" id="tree"></div>

                <div>
                    <h4><b style="font-size: 1.5em;margin-top: 1em;display: block;margin-bottom: 1em;" >Liquidación binaria</b></h4>
                </div>
                <div>
                    <?php 

                        $table="<table class='table' >";
                        $table.="
                            <thead>
                                <tr>
                                    <th>{$binary_liquidation[0][0]}</th>
                                    <th>{$binary_liquidation[0][1]}</th>
                                    <th>{$binary_liquidation[0][2]}</th>
                                    <th>{$binary_liquidation[0][3]}</th>
                                    <th>{$binary_liquidation[0][4]}</th>
                                    <th>{$binary_liquidation[0][5]}</th>
                                </tr>
                            </thead>
                        ";
                        for($i = 1; $i< count($binary_liquidation); $i++){
                            $table.="
                                <tr>
                                <td>{$binary_liquidation[$i][0]}</td>
                                <td>{$binary_liquidation[$i][1]}</td>
                                <td>{$binary_liquidation[$i][2]}</td>
                                <td>{$binary_liquidation[$i][3]}</td>
                                <td>{$binary_liquidation[$i][4]}</td>
                                <td>{$binary_liquidation[$i][5]}</td>
                                </tr>
                            ";
                        } 
                        $table .= "</table>";
                        echo $table;
                    ?>
                </div>

            </div>              
        </div>
    </div>
</div>

<script>

$(document).ready(function() {

    var chart_config = {

        chart: {
            container: "#tree",
            scrollbar: 'fancy',
            animateOnInit: true,
            rootOrientation: 'NORTH',
            connectors: {
                type: "step",
                style: {
                    stroke: "#0e6983",
                    'stroke-width': 3
                }
            },
            node: {
                collapsable: true
            },
            animation: {
                nodeAnimation: "easeOutBounce",
                nodeSpeed: 700,
                connectorsAnimation: "bounce",
                connectorsSpeed: 700
            }
        },
        nodeStructure: <?= (isset($binary)) ? json_encode($binary,JSON_PRETTY_PRINT) : null ?>
    };
    tree = new Treant(chart_config);
});
</script>