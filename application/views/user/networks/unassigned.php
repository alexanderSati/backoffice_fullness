<div class="row">
    <div class="col-md-12 col-sm-12 col-xs-12">
        <div class="x_panel">
            <div class="x_title">
                <h2><?= lang('users title unassigned') ?></h2>
                <div class="clearfix"></div>
            </div>
            <div class="x_content">
                <table id="admin-transactions" class="table table-striped table-bordered dt-responsive nowrap"
                       cellspacing="0" width="100%">
                    <thead>
                    <tr>
                        <th><?= lang('t_user') ?></th>
                        <th><?= lang('status') ?></th>
                    </tr>
                    </thead>
                    <tbody>
                    <?php if (!empty($pending)) {
                        foreach ($pending as $user_key => $user) { ?>
                            <tr>
                                <td><?= $user ?></td>
                                <td>
                                    <select name="users-status" rowid="<?= $user_key ?>" class="form-control users-status" >
                                        <option value="">Unassigned</option>
                                        <option value="left">Left</option>
                                        <option value="right">Right</option>
                                    </select>
                                </td>
                            </tr>
                        <?php }
                    } else { ?>
                        <tr><td colspan="2"><?= lang("no-data-table"); ?></td></tr>
                    <?php } ?>
                    </tbody>
                </table>

            </div>
        </div>
    </div>
</div>