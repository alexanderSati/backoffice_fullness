<?php defined('BASEPATH') OR exit('No direct script access allowed');
$lang = (isset($user['language']) ? $user['language'] : $this->config->item('language'));?>

<!-- [ page content ] start -->
<div class="card px-3">
  <div class="card-block">

    <!---TRANSFER MOENY------>
    <div class="row">
      <div class="col-md-12 col-sm-12 col-xs-12">
        <div class="card">
          <div class="card-header">
            <h4><?= lang('users title saldo'). ':' ?> <?= round($saldo, 2, PHP_ROUND_HALF_EVEN) .' S/' ?></h4>
          </div>
          <div class="card-block">
            <?php echo form_open('', array('class' => 'form-horizontal form-label-left', 'data-parsley-validate' => '','role' => 'form', 'id' => 'form-transfer' , 'novalidate')); ?>

            <div class="notifications-transfer"></div>
            <div class="form-group row text-center">
              <?php echo form_label(lang('transfer-to') . ' *', 'base', array('class' => 'col-sm-2 col-form-label')); ?>
              <div class="col-md-4 col-sm-4 col-xs-12 m-r-20">
                <?php echo form_input(array('name' => 'transfer-to', 'id'=>'transfer-to', 'required'=>'required', 'value' => '', 'class' => 'form-control')); ?>
              </div>
              <a id="search-transfer" class="btn btn-info m-r-20"><i class="fa fa-search" aria-hidden="true"></i>
                <?php //lang("search"); ?></a>
              <a id="clear-transfer" class="btn btn-warning"><i class="fa fa-times-circle-o" aria-hidden="true"></i>
                <?php //lang("clear"); ?></a>
            </div>


            <div class="continue-transfer hide" style="display: none;">
              <div>
                <hr />
              </div>

              <input type="hidden" name="id_user" id="id_user">

              <input type="hidden" name="wallet" id="wallet" value="USD">

              <div class="form-group row">
                <label class="col-sm-2 col-form-label"><?= lang('users input dni') ?></label>
                <div class="col-md-4 col-sm-4 col-xs-12">
                  <input class="form-control" type="text" name="dni-user-to-transfer" id="dni-user-to-transfer"
                    disabled>
                </div>
              </div>

              <div class="form-group row">
                <label class="col-sm-2 col-form-label"><?= lang('users col name') ?></label>
                <div class="col-md-4 col-sm-4 col-xs-12">
                  <input class="form-control" type="text" name="name-user-to-transfer" id="name-user-to-transfer"
                    disabled>
                </div>
              </div>

              <div class="form-group row">
                <?php echo form_label(lang('amount') . ' <cite><sub>(SOL/.)</sub></cite> *', 'amount', array('class' => 'col-sm-2 col-form-label')); ?>
                <div class="col-md-4 col-sm-4 col-xs-12">
                  <?php echo form_input(array('name' => 'amount', 'type' => 'text',  'id'=>'amount-transfer', 'required'=>'required', 'class' => 'form-control')); ?>
                </div>
              </div>

              <div class="form-group row">
                <?php echo form_label(lang('security-pin') . ' *', 'security-pin', array('class' => 'col-sm-2 col-form-label')); ?>
                <div class="col-md-4 col-sm-4 col-xs-12">
                  <?php echo form_password(array('name' => 'security-pin', 'id'=>'security-pin','maxlength' => "4",'required'=>'required', 'value' => '', 'class' => 'form-control', 'autocomplete' => 'off', 'data-toggle' => "tooltip" , 'data-placement' => "right", 'title'=> "", 'data-original-title'=> lang('pin_tooltip'))); ?>
                </div>
              </div>

              <div>
                <hr />
              </div>

              <div class="form-group row text-center">
                <div class="col-md-12 col-md-offset-1 col-xs-12">
                  <button type="button" data-toggle="modal" id="button-transfer" data-target="#modal-default"
                    name="submit" class="btn btn-info"><span class="fa fa-check-square-o"></span>
                    <?php echo lang('transfer'); ?></button>
                </div>
              </div>
            </div>

            <?php echo form_close(); ?>
          </div>
          <!-- end of card-block -->
        </div>
        <!-- end of card -->
      </div>
    </div>

    <!-- Table Content -->
    <div class="row">
      <div class="col-md-12 col-sm-12 col-xs-12">
        <div class="card">
          <div class="card-header">
            <h4><?= lang('transactions') ?></h4>
          </div>
          <div class="card-block">
            <div class="dt-responsive table-responsive">
              <table id="order-table" class="table table-striped table-bordered nowrap">
                <thead>
                  <tr>
                    <th><?= lang('code') ?></th>
                    <th><?= lang('description') ?></th>
                    <th><?= lang('amount') ?></th>
                    <th><?= lang('date') ?></th>
                    <th><?= lang('status') ?></th>
                  </tr>
                </thead>
                <tbody>
                  <?php if (isset($transactions) and $transactions != false): ?>
                  <?php foreach ($transactions as $transaction): ?>
                  <tr>
                    <td><?= $transaction['cod'] ?></td>
                    <td><?= $transaction['description'] ?></td>
                    <td><?= $transaction['usd'] ?> <sub> S/</sub></td>
                    <td><?= $transaction['date'] ?></td>
                    <td>
                      <?php
                        if($transaction['status'] == "done") {
                            echo "<span class='pcoded-badge label label-success'>".$transaction['status']."</span>";
                        }
                        elseif($transaction['status'] == "pending" || $transaction['status'] == "waiting" ){
                            echo "<span class='pcoded-badge label label-warning'>".$transaction['status']."</span>";
                        }
                        else {
                            echo "<span class='pcoded-badge label label-danger'>".$transaction['status']."</span>";
                        }
                    ?>
                    </td>
                  </tr>
                  <?php endforeach; ?>
                  <?php endif; ?>
                </tbody>
              </table>
            </div>
          </div>
        </div>
      </div>
    </div>

    <!-----Modal confirm transfer ------>
    <div class="modal fade in" id="modal-default">
      <div class="modal-dialog">
        <div class="modal-content">
          <div class="modal-header">
            <h4 class="modal-title"><?= lang('users msg transfer')?></h4>
            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
              <span aria-hidden="true">×</span></button>
          </div>
          <div class="modal-body text-center">
            <h5><?= lang('users input remember') ?></h5>
          </div>
          <div class="modal-footer">
            <button type="button" class="btn btn-danger pull-left"
              data-dismiss="modal"><?php echo lang('users button cancel'); ?></button>
            <button type="submit" class="btn btn-success" id="confirm_transfer" form="form-transfer"
              onclick="removeButton()"><?php echo lang('users button confirm'); ?></button>
          </div>
        </div>
        <!-- /.modal-content -->
      </div>
      <!-- /.modal-dialog -->
    </div>
  </div>
  <!-- end of card-block -->
</div>
<!-- [ page content ] end -->

<script>
function removeButton() {
  document.getElementById("form-transfer").addEventListener("submit", function() {
    $("#confirm_transfer").css('display', 'none');
  });
}
$(document).on("keydown", "form", function(event) {
  return event.key != "Enter";
});
</script>