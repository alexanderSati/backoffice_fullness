<?php defined('BASEPATH') OR exit('No direct script access allowed');
$lang = (isset($user['language']) ? $user['language'] : $this->config->item('language')); ?>

<!-- [ page content ] start -->
<div class="card px-3">
    <div class="card-block">

        <div class="row">
            <div class="col-md-6 col-sm-6 col-xs-12">
                <div class="x_panel">
                    <div class="x_title">
                    <!-- <h2>--><?//= lang("account-plan") ?><!--</h2>-->
                        <div class="clearfix"></div>
                    </div>
                    <div class="x_content">

                    </div>
                </div>
            </div>
            <div class="col-md-6 col-sm-6 col-xs-12">
                <div class="x_panel">
                    <div class="x_title">
                        <!-- <h2>--><?//= lang("account-rank") ?><!--</h2>-->
                        <div class="clearfix"></div>
                    </div>
                    <div class="x_content">

                    </div>
                </div>
            </div>
        </div>

        <?php if($this->user["profile"]=="executive"):?>
        <div class="row">
            <div class="col-lg-5 col-md-12 col-sm-12 col-xs-12">
                <div class="card widget-statstic-card">
                    <div class="card-header borderless content-<?= $code ?>">
            
                        <div class="count" style="font-size: 24px">
                        S/ <?= round($billetera, 2, PHP_ROUND_HALF_EVEN); ?> 
                        </div>
                        <div class="card-block">
                        <i class="feather st-icon bg-c-blue">
                            <img class="coin-logos" src="<?= site_url('/themes/able/images/coins/USD.png'); ?>"
                            alt="">
                        </i>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <?php endif;?>


        <div class="row">
            <div class="col-md-12 col-sm-12 col-xs-12">
                <div class="x_panel">
                    <div class="x_title">
                        <h2><?= lang('transactions') ?></h2>
                        <div class="clearfix"></div>
                    </div>
                    <div class="dt-responsive table-responsive">
                        <table id="order-table" class="table table-striped table-bordered nowrap">
                            <thead>
                            <tr>
                                <th><?= lang('code') ?></th>
                                <th><?= lang('description') ?></th>
                                <th><?= lang('value_usd') ?></th>
                                <th><?= lang('date') ?></th>
                                <th><?= lang('status') ?></th>
                            </tr>
                            </thead>
                            <tbody>
                            <?php foreach ($transactions as $transaction) { ?>
                                <tr>
                                    <td><?= $transaction['cod'] ?></td>
                                    <td><?= $transaction['description'] ?></td>                                   
                                    <td><?= $transaction['usd'] ?></td>
                                    <td><?= $transaction['date'] ?></td>
                                    <td>
                                      <?php
                                        if ($transaction['status'] == "done") {
                                            echo "<span class='pcoded-badge label label-success'>" . $transaction['status'] . "</span>";
                                        } elseif ($transaction['status'] == "pending" || $transaction['status'] == "waiting") {
                                            echo "<span class='pcoded-badge label label-warning'>" . $transaction['status'] . "</span>";
                                        } else {
                                            echo "<span class='pcoded-badge label label-danger'>" . $transaction['status'] . "</span>";
                                        }
                                        ?>
                                    </td>
                                </tr>
                            <?php } ?>
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!-- end of card-block -->            
</div>
<!-- [ page content ] end -->
