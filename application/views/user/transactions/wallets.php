<?php defined('BASEPATH') OR exit('No direct script access allowed');
$lang = (isset($user['language']) ? $user['language'] : $this->config->item('language')); ?>

<div class="row top_tiles">
    <?php foreach ($wallets as $code => $value) { ?>
        <div class="animated flipInY col-lg-4 col-md-4 col-sm-3 col-xs-12">
            <div class="tile-stats wallets content-<?= $code ?>">
                <div class="pull-right coin-logos">
                    <img src="/themes/<?= $this->settings->theme ?>/images/coins/<?= $code ?>.png" alt="">
                </div>
                <div class="count" style="font-size: 24px">
                    <?= ($code =='BTC') ? '<span class="label label-success">Operative</span><br>' : "" ?>
                    <?= number_format($value['coins'],8) ?> <?= $value['label'] ?>
                </div>
                <div class="count" style="font-size: 24px">
                    S/<?= round($value['usd'],2) ?> 
                </div>
            </div>
        </div>
    <?php } ?>
</div>

<div class="row">
    <div class="col-md-12 col-sm-12 col-xs-12">
        <div class="x_panel">
            <div class="x_title">
                <h2><?= lang("new-btc-trasnfer") ?></h2>
                <div class="clearfix"></div>
            </div>
            <div class="x_content">
                
                <div class="notifications-transfer"></div>
                
                <?php echo form_open('', array('class' => 'form-horizontal form-label-left', 'data-parsley-validate' => '','role' => 'form')); ?>

                    <div class="item form-group">
                        <?php echo form_label(lang('select-wallet') . ' *', 'wallet', array('class' => 'control-label col-md-1 col-sm-1 col-xs-12')); ?>
                        <div class="col-md-4 col-sm-4 col-xs-12">
                            <select class="form-control" name="wallet_from" required="">
                                <?php foreach ($wallets as $code => $value) { ?>
                                        <option value="<?= $code ?>"><?= $this->wallets->labels[$code] ?></option>
                                <?php } ?>
                            </select>
                        </div>
                    </div>  
                
                <div class="continue-transfer">
                    <div class="ln_solid"></div>

                    <div class="item form-group">
                        <?php echo form_label(lang('transfer-to') . ' *', 'base', array('class' => 'control-label col-md-1 col-sm-1 col-xs-12')); ?>
                            <div class="col-md-4 col-sm-4 col-xs-12">
                                <select class="form-control" name="wallet_to" required="">
                                    <?php foreach ($wallets as $code => $value) { ?>
                                        <?php if($code != 'USD') { ?>
                                            <option value="<?= $code ?>"><?= $value['label'] ?></option>
                                        <?php } ?>
                                    <?php } ?>
                                </select>
                            </div>
                    </div>
                    
                    <div class="item form-group">
                        <?php echo form_label(lang('amount') . ' *', 'amount', array('class' => 'control-label col-md-1 col-sm-1 col-xs-12')); ?>
                        <div class="col-md-4 col-sm-4 col-xs-12">
                            <?php echo form_input(array('name' => 'amount', 'type' => 'text',  'id'=>'amount', 'required'=>'required', 'value' => '1', 'class' => 'form-control col-md-7 col-xs-12')); ?>
                        </div>
                    </div>
                    
                    
                    <div class="item form-group">
                        <?php echo form_label(lang('security-pin') . ' *', 'security-pin', array('class' => 'control-label col-md-1 col-sm-1 col-xs-12')); ?>
                        <div class="col-md-4 col-sm-4 col-xs-12">
                            <?php echo form_password(array('name' => 'security-pin', 'id'=>'security-pin','maxlength' => "4",'required'=>'required', 'value' => '', 'class' => 'form-control col-md-7 col-xs-12', 'autocomplete' => 'off')); ?>
                        </div>
                        <div><?= lang("verify-spam") ?></div>
                    </div>

                    <div class="ln_solid"></div>

                    <div class="form-group">
                        <div class="col-md-6 col-md-offset-1 col-xs-12">
                            <button type="submit" name="submit" class="btn btn-success"><span
                                    class="glyphicon glyphicon-save"></span> <?php echo lang('transfer'); ?></button>
                        </div>
                    </div>
                </div>


                <?php echo form_close(); ?>
            </div>
        </div>
    </div>
</div>
