<?php
defined('BASEPATH') OR exit('No direct script access allowed');
$lang = (isset($user['language']) ? $user['language'] : $this->config->item('language'));
$origin_wallets = array("XMR"=>"Monero", "ETH"=>"Ethereum", "LTC"=>"Litecoin","DASH"=>"Dash", "BTC"=>"Bitcoin");
$select_wallets = array("USD"=>"Bitcoin", "XMR"=>"Monero", "ETH"=>"Ethereum", "LTC"=>"Litecoin", "TRUMP"=>"Trumpcoin", "DASH"=>"Dash", "SLR"=>"Solarcoin");

$col_lg = "col-lg-12";
$col_lg_sub = "col-lg-4";
$height_qr = "";
if (!empty($qr_code)) {
    $col_lg = "col-lg-6";
    $col_lg_sub = "col-lg-8";
    $height_qr = "height_qr";
}
?>

<div class="row">
    <div class="col-md-12 col-sm-12 col-xs-12">
        <div class="<?= $col_lg; ?> col-md-12 col-sm-12 col-xs-12">
            <div class="x_panel <?= $height_qr; ?>">
                <div class="x_title">
                    <h2><?= lang("new-deposit") ?></h2>
                    <div class="clearfix"></div>
                </div>
                <div class="x_content">
                    <div class="<?= (!empty($qr_code)) ? '' : 'col-lg-6 col-md-6' ?>">
                        <?php echo form_open('', array('class' => 'form-horizontal form-label-left', 'data-parsley-validate' => '', 'role' => 'form')); ?>

                        <div class="item form-group" >
                            <?php echo form_label(lang('origin-coin') . ' *', 'wallet', array('class' => 'col-md-12 col-xs-12')); ?>
                            <select name="type" id="type" class="select-custom type-coin" >
                                <?php foreach ($origin_wallets as $key => $value) { ?>
                                    <option value="<?= $key; ?>"><?= $value; ?></option>
                                <?php } ?>
                            </select>
                            <?php echo form_input(array('name' => 'amount', 'type' => 'text', 'id' => 'amount_coin_deposit', 'required' => 'required', 'value' => '', 'class' => 'form-control form-custom col-md-7 col-xs-12', 'placeholder' => '0.00000000', 'data-toggle' => 'tooltip', 'data-placement' => 'top', 'title' => lang('amount_coin_tooltip'))); ?>
                        </div>

                        <div class="item form-group">
                            <?php echo form_label(lang('final_coin') . ' *', 'wallet', array('class' => 'col-md-12 col-xs-12')); ?>
                            <select name="type_final" id="type_final" class="select-custom type-coin">
                                <?php foreach ($select_wallets as $key => $value) { ?>
                                    <option value="<?= $key; ?>"><?= $value; ?></option>
                                <?php } ?>
                            </select>
                            <?php echo form_input(array('name' => 'amount_final', 'id' => 'amount_coin_final', 'readonly' => 'readonly', 'required' => 'required', 'value' => '', 'class' => 'form-control form-custom-coin col-md-7 col-xs-12')); ?>
                        </div>

                        <div class="ln_solid"></div>

                        <div class="form-group">
                            <button type="submit" name="submit" class="btn btn-success">
                                <span class="glyphicon glyphicon-save"></span> <?php echo lang('button-coin-transfer'); ?>
                            </button>
                        </div>

                        <?php echo form_close(); ?>
                    </div>
                </div>
            </div>
        </div>
        <?php if (!empty($qr_code)) { ?>
            <div class="<?= $col_lg; ?> col-md-12 col-sm-12 col-xs-12">
                <div class="x_panel">
                    <div class="x_title">
                        <h2><?= lang('users title register qr_to_pay'); ?></h2>
                        <div class="clearfix"></div>
                    </div>
                    <div class="x_content">
                        <div class="col-md-12 col-sm-12 col-xs-12">
                            <div class="col-md-6 col-sm-12 col-xs-12">
                                <div class="display_qr">
                                    <div id="btc">
                                        <img src="<?php echo $qr_code; ?>"/>
                                    </div>
                                    <?php if ($wallet) { ?>
                                        <p id="wallet_qr"><?php echo $wallet ?></p>
                                    <?php } ?>
                                </div>
                            </div>
                            <div class="col-md-6 col-sm-12 col-xs-12 text-center">
                                <?php if (!empty($transaction_id)) { ?>
                                    <br>
                                    <p><i><?= lang('deposit_message') ?>.</i></p>
                                    <div class="ln_solid"></div>
                                    <br>
                                    <a id="cancel_deposit" type="button" class="btn btn-default" href="<?php echo base_url() . 'user/transactions/depositQR/' . $transaction_id . '/' . $type_deposit . '/cancel' ?>"><?= lang('cancel_deposit'); ?></a>
                                    <a id="save_deposit" type="button" class="btn btn-primary" href="<?php echo base_url() . 'user/transactions/depositQR/' . $transaction_id . '/' . $type_deposit . '/save' ?>"><?= lang('deposit_payed'); ?></a>
                                <?php } ?>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        <?php } ?>
    </div>
</div>

<div class="row">
    <div class="col-md-12 col-sm-12 col-xs-12">
        <div class="x_panel">
            <div class="x_title">
                <h2><?= lang('transactions') ?></h2>
                <div class="clearfix"></div>
            </div>
            <div class="x_content">
                <table id="datatable-responsive" class="table table-striped table-bordered dt-responsive nowrap"
                       cellspacing="0" width="100%">
                    <thead>
                        <tr>
                            <th><?= lang('date') ?></th>
                            <th><?= lang('code') ?></th>
                            <th><?= lang('description') ?></th>
                            <th><?= lang('value_btc') ?></th>
                            <th><?= lang('rate') ?></th>
                            <th><?= lang('amount') ?></th>
                            <th><?= lang('status') ?></th>
                        </tr>
                    </thead>
                    <tbody>
                        <?php
                        if (isset($transactions) and $transactions != false) {
                            foreach ($transactions as $transaction) {
                                ?>
                                <tr>
                                    <td><?= $transaction['date'] ?></td>
                                    <td><?= $transaction['cod'] ?></td>
                                    <td><?= $transaction['description'] ?></td>
                                    <td><?= $transaction['btc'] ?></td>
                                    <td><?= $transaction['rate'] ?></td>
                                    <td><?= $transaction['usd'] ?> <sub><?= empty($transaction['wallet'])? 'USD' : $transaction['wallet'] ?></sub></td>
                                    <td><span class="badge bg-<?= ($transaction['status'] == 'done') ? 'purple' : 'black' ?>"><?= $transaction['status'] ?></span>
                                        <?php if ($transaction['status'] == 'waiting') { ?>
                                            <a class="badge bg-blue" data-toggle="modal" href="/user/transactions/depositQR/<?= $transaction['id'] . '/' . $type_deposit; ?>" data-target="#modal_qr"><?= lang('qr_code'); ?></a>
        <?php } ?>
                                    </td>
                                </tr>
                            <?php }
                        }
                        ?>
                    </tbody>
                </table>
            </div>
        </div>
    </div>
</div>


<div id="modal_qr" class="modal fade bs-example-modal-sm" tabindex="-1" role="dialog" aria-hidden="true">
    <div class="modal-dialog modal-sm">
        <div class="modal-content">

        </div>
    </div>
</div>