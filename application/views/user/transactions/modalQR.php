<?php $select_wallets = array("XMR" => "Monero", "ETH" => "Ethereum", "LTC" => "Litecoin", "DASH" => "Dash"); ?>
<script>
    var btcLink = '<?= base_url('user/transactions/depositQR/' . $id . '/' . $type_deposit . '/save/') ?>';
    var hashLang = '<?= $this->lang->line('invalid_hash'); ?>';
</script>

<div class="modal-content">

    <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">×</span>
        </button>
        <h4 class="modal-title" id="myModalLabel2"><?= lang('users title register qr_to_pay'); ?></h4>
    </div>
    <div class="modal-body">
        <div class="display_qr">
            <div id="modal_btc">
                <?php
                $pieces = explode(' ', str_replace(':', ' ', $description));
                $wallet = array_pop($pieces);

                if ($link_qr) {

                    $search_coin = explode("|", $cod);
                    if ($search_coin[0] == "BTC") {
                        $qr = "https://blockchain.info/qr?data=bitcoin:{$wallet}?amount=" . $btc;
                    } else {
                        $coin = strtolower($select_wallets[$search_coin[0]]);
                        $qr = "{$link_qr}{$coin}:{$wallet}";
                    }
                }

                if (isset($wallet)) {
                    if (isset($qr)) {
                        ?>
                        <img src="<?= $qr; ?>"/>
                        <p id="wallet_qr"><?= $wallet; ?></p>
                    <?php } else { ?>
                        <img src="https://blockchain.info/qr?data=bitcoin:<?= $wallet; ?>?amount=<?= $this->currency->btc($usd); ?>"/>
                        <br>
                        <p id="wallet_qr"><?= $wallet; ?></p>
                        <br>
                        <?php
                        if ($type == 'monthly') {
                            echo form_label($this->lang->line('hash_label'), 'hash', ['class' => 'text-danger']);
                            echo form_input(['type' => 'text', 'name' => 'hash', 'id' => 'hash', 'class' => 'form-control', 'required' => 'required']);
                        }
                        ?>
                    <?php }
                } ?>
            </div>
        </div>
    </div>
    <div class="modal-footer">
        <a id="cancel_deposit" type="button" class="btn btn-default" href="<?php echo base_url() . 'user/transactions/depositQR/' . $id . '/' . $type_deposit . '/cancel' ?>"><?= lang('cancel_deposit'); ?></a>
        <?php if ($type == 'monthly') { ?>
            <button id="save_deposit" class="btn btn-primary" onclick="savePayment()"><?= lang('deposit_payed'); ?></button>
        <?php } else { ?>
            <a id="save_deposit" type="button" class="btn btn-primary" href="<?php echo base_url() . 'user/transactions/depositQR/' . $id . '/' . $type_deposit . '/save' ?>"><?= lang('deposit_payed'); ?></a>
        <?php } ?>

    </div>

</div>