<?php defined('BASEPATH') OR exit('No direct script access allowed');
$lang = (isset($user['language']) ? $user['language'] : $this->config->item('language')); ?>


<!-- [ page content ] start -->
<div class="card px-3">
    <div class="card-block">
        <div class="row top_tiles">
            <?php foreach ($wallets as $code => $value) : ?>
            <?php if($code == "USD"): ?>
                <div class="col-lg-4 col-md-4 col-sm-3 col-xs-12">
                    <div class="card widget-statstic-card">
                        <div class="card-header borderless wallets content-<?= $code ?>">
                            <div class="card-header-left">
                                <div class="count" style="font-size: 24px">
                                    S/<?= round($value['usd'],2) ?> SL/. 
                                </div>                                
                            </div>
                            
                            <div class="card-block">
                                <i class="feather st-icon bg-c-blue">                                    
                                    <img src="/themes/<?= $this->settings->theme ?>/images/coins/<?= $code ?>.png" alt="">
                                </i>                                
                            </div>
                        </div>
                    </div>
                </div>
            <?php endif;?>
            <?php endforeach; ?>
        </div>

        <div class="row">
            <div class="col-md-12 col-sm-12 col-xs-12">
                <div class="card">
                    <div class="card-header">
                        <h4><?= lang("request-payout") ?></h4>                        
                    </div>
                    <div class="card-block">
                        <?php if($allowed) { ?>
                            <?php echo form_open('', array('class' => 'form-horizontal form-label-left', 'data-parsley-validate' => '','role' => 'form', 'novalidate')); ?>

                            <div class="notifications-transfer"></div>

                            <input type="hidden" name="wallet" id="wallet" value="USD">

                            <div class="form-group row">
                                <?php echo form_label(lang('amount') . ' *', 'amount', array('class' => 'col-sm-2 col-form-label')); ?>
                                <div class="col-md-4 col-sm-4 col-xs-12">
                                    <?php echo form_input(array('name' => 'amount', 'id'=>'amount', 'required'=>'required', 'value' => '', 'type' => 'number', 'class' => 'form-control')); ?>
                                </div>
                            </div>

                            <div class="form-group row">
                                <?php echo form_label(lang('security-pin') . ' *', 'security-pin', array('class' => 'col-sm-2 col-form-label')); ?>
                                <div class="col-md-4 col-sm-4 col-xs-12">
                                    <?php echo form_password(array('name' => 'security-pin', 'id'=>'security-pin','maxlength' => "4",'required'=>'required', 'value' => '', 'class' => 'form-control', 'autocomplete' => 'off')); ?>
                                </div>
                                <div><?= lang("verify-spam") ?></div>
                            </div>

                            <div><hr/></div>

                            <div class="form-group row text-center">
                                <div class="col-md-12 col-md-offset-1 col-xs-12">
                                    <button type="submit" name="submit" class="btn btn-info"><span
                                        class="fa fa-check-square-o"></span> <?php echo lang('request-payout'); ?>
                                    </button>
                                </div>
                            </div>
                            <?php echo form_close(); ?>
                        <?php }else{ ?>
                            <div class="bs-example" data-example-id="simple-jumbotron">
                                <div class="jumbotron text-center">
                                    <h2><?= $this->lang->line('payout-limit-title') ?></h2>
                                    <?php $text_date_frecuency = 'date_' . $plan['max_payout_interval'] . ($plan['max_payouts_frecuency'] > 1 ? 's' : '' ); ?>
                                    <p class="f-18 m-t-40"><?= sprintf($this->lang->line('payout-limit-text'), $plan['max_payout_attemp'], $plan['max_payouts_frecuency'], $this->lang->line($text_date_frecuency)) ?> <?= $allowed_date ?></p>
                                </div>
                            </div>
                        <?php } ?>    
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!-- end of card-block -->            
</div>
<!-- [ page content ] end -->