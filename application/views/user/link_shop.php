<?php defined('BASEPATH') OR exit('No direct script access allowed');
$lang = (isset($user['language']) ? $user['language'] : $this->config->item('language')); 
?>

<div class="alert alert-success" id="copy-message" style="display: none">
    <button type="button" class="close" id="close"><span aria-hidden="true">&times;</span></button>
    <?php echo lang('users link copy'); ?>
</div>
<div class="card">
    <div class="row">
        <div class="col-md-12 col-sm-12 col-xs-12">
            <div class="x_panel">
                <div class="x_title card-header">
                    <h2><?= $page_title ?></h2>
                    <div class="clearfix"></div>
                </div>
                <div class="x_content card-block">
                    <label for="left" class="control-label"><?php echo lang('core button link-shop'); ?></label>

                    <div class="input-group">
                        <input class="form-control" type="text" onclick="Link()"
                            value="<?php echo base_url()."cart_external/referrals/".$user['username'] ?>">
                        <span class="input-group-text" onclick="Link()"><i class="fa fa-clipboard"
                                aria-hidden="true"></i></span>
                    </div>
                </div>
            </div>

        </div>
    </div>
</div>                    


<script>
function Link() {
    // Create a "hidden" text field
    var aux = document.createElement("input");
   
    aux.setAttribute("value", "<?php echo base_url()."cart_external/referrals/".$user['username'] ?>");

    // Add the field to the page
    document.body.appendChild(aux);

    // Select the content of the field
    aux.select();

    // Copy the selected text
    document.execCommand("copy");

    // Remove the field from the page
    document.body.removeChild(aux);

    // Alert configuration
    const alert = document.getElementById("copy-message");
    alert.style.display = "block";
}
//  Close Alert 
const copy = document.getElementById('close');

copy.addEventListener('click', () => {
    const alert = document.getElementById("copy-message");
    alert.style.display = "none";
});
</script>