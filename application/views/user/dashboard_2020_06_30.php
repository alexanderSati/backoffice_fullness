<!-- [ page content ] start -->
<?php
$lang = (isset($user['language']) ? $user['language'] : $this->config->item('language'));
$ranks = array(
	0=>'entrepreneur.png',
	1 => 'rango1.png',
	2 => 'rango2.png',
	3 => 'rango3.png',
	4 => 'rango4.png',
	5 => 'rango5.png',
	6 => 'rango6.png',
	7 => 'rango7.png',
	8 => 'rango8.png',
	9 => 'rango9.png',
	10 => 'rango10.png',
	11 => 'rango11.png'
	);
?>

<!-- site Analytics card start -->
<div class="row">
	<div class="col-lg-7 col-md-12">
		<div class="card">
      <div class="card-header">
				<h5>Fullness Global</h5>
			</div>
			<div class="card-block">
				<div class="owl-carousel carousel-nav owl-theme">
					<div>
						<img class="d-block img-fluid" src="/uploads/slider1.png" alt="Third slide">
					</div>
					<div>
						<img class="d-block img-fluid" src="/uploads/slider2.png" alt="Third slide">
					</div>
				</div>
			</div>
		</div>
	</div>
	<div class="col-lg-5 col-md-12">
    <div class="row">
			<div class="col-md-12">
				<div class="card">
					<div class="card-block">
              		<div class="row align-items-center">
                		<div class="col-8">
                  			<h4 class="text-c-yellow">S/<?= round($profit['profit'], 2, PHP_ROUND_HALF_EVEN); ?> </h4>
                  			<h6 class="text-muted m-b-0"><?= lang('profit') ?></h6>
              			</div>
              			<div class="col-4 text-right">
                  			<i class="feather icon-bar-chart-2 f-28"></i>
              			</div>
          			</div>
      			</div>
  			</div>
		</div>
	</div>
<?php  if( !is_preferencial_user() ): ?>
    <div class="row">
			<div class="col-md-12">
      		<div class="card">
        		<div class="card-block">
          			<div class="row align-items-center">
            			<div class="col-8"  >
              				<h4 class="text-c-yellow">S/ <?= round(floatval($saldo)+$binary_bonus+$unilevel_bonus, 2, PHP_ROUND_HALF_EVEN); ?> </h4>
              				<b for="" class="text-muted" ><?= lang("label_commission") ?></b>
          				</div>
          				<div class="col-4 text-right">
              				<i class="feather icon-credit-card f-28"></i>
          				</div>
      				</div>
  				</div>
			</div>
		</div>
	</div>
<?php endif;  ?>
	<div class="row">
		<div class="col-md-6">
      		<div class="card">
    			<div class="card-block">
          			<div class="row client-detail">
			            <div class="col-10">
			                <h5 class="text-c-yellow <?= $rank  ?>"><?= _rank_title($rank["id"])  ?></h5>
			                <h6 class="text-muted m-b-0"><?= lang('current-rank') ?></h6>
			            </div>
        			</div>
    			</div>
    			<div class="card-footer" style="background-image: linear-gradient(to bottom, #dd4e7c, #f27955, #e3ae44);padding:4px 20px">
      				<div class="row align-items-center">
        				<div class="col-9">
          					<p class="text-white m-b-0"></p>
      					</div>
				      	<div class="col-3 text-right">
				          	<i class="feather icon-trending-up text-white f-16"></i>
				      	</div>
  					</div>
				</div>
			</div>
		</div>
		<div class="col-md-6">
    <div class="card">
      <div class="card-block">
        <div class="row align-items-center">
          <div class="col-8">
            <h5 class="text-c-green"><?= $total_referrals ?></h5>
            <h6 class="text-muted m-b-0"><?= lang('users title total_referrals') ?></h6>
        </div>
        <div class="col-4 text-right">
            <i class="feather icon-file-text f-28"></i>


        </div>


    </div>


</div>


<div class="card-footer" style="background-image: linear-gradient(to bottom, #00829f, #00916b, #479933);padding:4px 20px">


    <div class="row align-items-center">


      <div class="col-9">


        <p class="text-white m-b-0"></p>


    </div>


    <div class="col-3 text-right">


        <i class="feather icon-trending-up text-white f-16"></i>


    </div>


</div>


</div>


</div>


</div>


</div>


<div class="row">





  <div class="col-md-6">


    <div class="card">


        <a href="<?= base_url();?>user/wallets">


          <div class="card-block">


            <div class="row align-items-center">


              <div class="col-8">


                <h5 class="text-c-red">S/<?=  round($saldo, 2, PHP_ROUND_HALF_EVEN);?></h5>





                <h6 class="text-muted m-b-0"><?= lang('users title saldo') ?></h6>


            </div>


            <div class="col-4 text-right">


                <i class="feather icon-calendar f-28"></i>


            </div>


        </div>


    </div>


    <div class="card-footer" style="background-image: linear-gradient(to bottom, #84389a, #a42788, #ba1571);padding:4px 20px">


        <div class="row align-items-center">


          <div class="col-9">


            <p class="text-white m-b-0"></p>


        </div>


        <div class="col-3 text-right">


            <i class="feather icon-trending-up text-white f-16"></i>


        </div>


    </div>


</div>


</a>


</div>


</div>


<div class="col-md-6">


    <div class="card">


      <a href="<?= base_url("user/dashboard#residual_points_table");?>">


          <div class="card-block">


            <div class="row align-items-center">


              <div class="col-8">


                <h5 class="text-c-blue" style="display:none"><?= $residual ?></h5>


                <h6 class="text-muted m-b-0"><?= lang('users title residual_points') ?></h6>


            </div>


            <div class="col-4 text-right">


                <i class="feather icon-thumbs-up f-28"></i>


            </div>


        </div>


    </div>


    <div class="card-footer " style="background-image: linear-gradient(to bottom, #006cb6, #007ab9, #30aee5);padding:4px 20px">


        <div class="row align-items-center">


          <div class="col-9">


            <p class="text-white m-b-0"></p>


        </div>


        <div class="col-3 text-right">


            <i class="feather icon-trending-up text-white f-16"></i>


        </div>


    </div>


</div>


</a>


</div>





</div>


<div class="col-md-6">


    <div class="card">


        <a href="#binary_points_table">


          <div class="card-block">


            <div class="row align-items-center">


              <div class="col-8">


                <h5 class="text-c-red" style="display:none"><?= $stats_tree['left_points'] ?></h5>


                <h6 class="text-muted m-b-0"><?= lang('users title left-points') ?></h6>


            </div>


            <div class="col-4 text-right">


                <i class="fa fa-caret-square-o-left f-28" style="font-size: 28px"></i>


            </div>


        </div>


    </div>


    <div class="card-footer" style="background-image: linear-gradient(to bottom, #b81186, #da0058, #e0291d);padding:4px 20px">


        <div class="row align-items-center">


          <div class="col-9">


            <p class="text-white m-b-0"></p>


        </div>


        <div class="col-3 text-right">


            <i class="feather icon-trending-up text-white f-16"></i>


        </div>


    </div>


</div>


</a>


</div>





</div>





<div class="col-md-6">


    <div class="card">


        <a href="#binary_points_table">


          <div class="card-block">


            <div class="row align-items-center">


              <div class="col-8">


                <h5 class="text-c-red" style="display:none"><?= $stats_tree['right_points'] ?></h5>


                <h6 class="text-muted m-b-0"><?= lang('users title right-points') ?></h6>


            </div>


            <div class="col-4 text-right">


                <i class="fa fa-caret-square-o-right f-28" style="font-size: 28px"></i>


            </div>


        </div>


    </div>


    <div class="card-footer" style="background-image: linear-gradient(to bottom, #b81186, #da0058, #e0291d);padding:4px 20px">


        <div class="row align-items-center">


          <div class="col-9">


            <p class="text-white m-b-0"></p>


        </div>


        <div class="col-3 text-right">


            <i class="feather icon-trending-up text-white f-16"></i>


        </div>


    </div>


</div>


</div>


</a>


</div>


</div>


</div>


<!-- site Analytics card end -->


</div>


<!-- site Analytics card end -->


<div class="row">


  <div class="col-md-4 col-xs-12">


    <div class="card">


      <div class="card-block text-center" style="min-height: 266px;">


        <i class="feather icon-briefcase text-c-red d-block f-40"></i>


        <h4 class="m-t-20"><?= $current_plan["{$lang}_name"] ?></h4>


        <p class="m-b-20"><?= lang('users title plan_active') ?></p>


        <?php if($qty_purchases > 0 && !is_preferencial_user()): ?>


            <button class="btn btn-danger btn-sm btn-round"><a style="color:white"


                href="<?php echo base_url() . 'user/upgrade'?>"><?= lang('upgrade_title') ?></a></button>


            <?php endif; ?>





        </div>


    </div>


</div>


<div class="col-md-8 col-xs-12" >


    <!-- Swiper slider card start -->


    <div class="card"  style="height: 266px;" >


      <div class="card-header">


        <h4><?= lang('rank')?></h4>


    </div>


    <div class="card-block">


        <div class="swiper-container">


          <div class="swiper-wrapper">





              <?php foreach($ranks as $key => $imgrank):?>


                <div class="swiper-slide" style='<?= $key == $rank["id"]? "border-radius: 15%; background-color:#47993370; padding:0.5em":""; ?>'>


                  <img class="img-fluid width-90" src="/uploads/rangos/<?= $imgrank ?>"   alt="Card image cap">


              </div>


          <?php endforeach;?>








      </div>


  </div>


</div>


</div>


<!-- Swiper slider card end -->


</div>


<div class="col-md-12 col-sm-12 col-xs-12">


  <div class="card client-map">


    <div class="card-block">





      <div class="">


        <h3 class="text-center text-muted ">


          <?= lang('countdown_end_of_month'); ?>


      </h3>


      <div class=" client-card-box">





          <input type="hidden" id="last_day_month" value="<?= date('Y-m-t',strtotime('today'))." 23:00:00"; ?>" />


          <div class="row">


            <div class="col-3 text-center client-border p-10">


              <p class="text-muted m-0"><?= lang("days"); ?></p>


              <span class="text-c-red f-20 f-w-600" id="countdown_days">835</span>


          </div>


          <div class="col-3 text-center client-border p-10">


              <p class="text-muted m-0"><?= lang("hours"); ?></p>


              <span class="text-c-red f-20 f-w-600" id="countdown_hours">02</span>


          </div>





          <div class="col-3 text-center client-border p-10">


              <p class="text-muted m-0"><?= lang("minutes"); ?></p>


              <span class="text-c-red f-20 f-w-600" id="countdown_minutes">02</span>


          </div>





          <div class="col-3 text-center  p-10">


              <p class="text-muted m-0"><?= lang("seconds"); ?></p>


              <span class="text-c-red f-20 f-w-600" id="countdown_seconds">02</span>


          </div>


      </div>





  </div>


</div>


</div>


</div>


</div>


</div>








<div class="row">


  <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">


    <div class="card" style="height: 97.5%">


      <?php if (isset($message) && !($message=='<p><br></p>')){?>


          <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">


            <div class="welcome_message x_panel">


              <div class="x_title card-header">


                <h2><?= lang('welcome') ?></h2>


                <div class="clearfix"></div>


            </div>


            <div class="x_content card-block">


                <?= $message ?>


            </div>


        </div>


    </div>


<?php }?>


</div>


</div>


<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">





    <div class="card">


      <div class="latest-update-card">





        <div class="card-block">


          <div class="card-header">


            <h4><?= lang('users title awards')?>


        </h4>


        <br>


        <p style="font-size: .9375rem;">


          <?php


          foreach ($list_awards as $key => $value) {


            if ($total_points_binary < $value['points']){


              $next_bonus_icon = $value['icon'];


              $next_bonus = $value['bonus'];


              $missing_points = $value['points'] - $total_points_binary;


              break;


          }





      }


      ?>


      <strong style="display:none"><?=  lang('users title total_points_binary'). '</strong>: '  ?></p>


      <p id="next_prize" style="font-size: .9375rem;"><strong><?= lang('users title next_prize'); ?></strong>: <!--<i class="<?= $next_bonus_icon ?>"></i>--><img src="<?= base_url("themes/able/images/".$next_bonus_icon) ?>" style="width:3em" /> <?=$next_bonus?> </p>


      <p id="point_missing" style="font-size: .9375rem;"><strong><?= lang('users title point_missing')?></strong> : </p>


      <p class="text-center" style="font-size: .9375rem;"><?= lang('users title progress_bar')?></p>


      <div class="progress">


          <div id="progressbar" class="progress-bar-striped progress-bar-primary" role="progressbar"></div>


      </div>


  </div>





  <div class="">


    <?php foreach ($list_awards as $key => $value) :?>


      <div class="row p-t-20 p-b-20 not-won" id="<?= ++$key ?>">


        <div class="col-auto text-right update-meta">


          <p class="text-muted m-b-0 d-inline"><?= $value['points'] ?></p>


          <!--<i class="<?= $value['icon'] ?>"></i>-->


          <img src="<?= base_url("themes/able/images/".$value["icon"]) ?>" style="height:6em" />





      </div>


      <div class="col">


          <p class="text-muted m-b-0"><?= $value['bonus'] ?></p>


          <div class="progress">


            <div class="progress-bar-striped progress-bar-primary progress-bar-bonus" data-points="<?= $value['points'] ?>" role="progressbar"></div>


        </div>


    </div>


</div>


<?php endforeach; ?>





</div>


</div>


</div>


</div>


</div>











<div class="col-md-12 col-sm-12 col-xs-12" id="residual_points_table">


    <div class="card">


      <div class="card-block">


        <div class="x_panel">


          <div class="x_title">


            <h2><?= lang('label_residual') ?></h2>


            <div class="clearfix"></div>


        </div>


        <div class="dt-responsive table-responsive">


            <table id="residual-table" class="table table-striped table-bordered nowrap">


              <thead>


                <tr>


                  <th><?= lang('description') ?></th>


                  <th style="display:none"><?= lang('points') ?></th>


                  <th><?= lang('date') ?></th>


              </tr>


          </thead>


          <tbody>





            <?php foreach ($residual_table as $resiual_td) : ?>


                <tr>


                  <td><?= $resiual_td['description'] ?></td>


                  <td style="display:none"><?= $resiual_td['points']  ?></td>


                  <td><?= $resiual_td['date'] ?></td>


              </tr>


          <?php endforeach;?>


      </tbody>


  </table>


</div>


</div>


</div>


</div>


</div>





<div class="col-md-12 col-sm-12 col-xs-12" id="binary_points_table">


    <div class="card">


      <div class="card-block">


        <div class="x_panel">


          <div class="x_title">


            <h2><?= lang('label_binary') ?></h2>


            <div class="clearfix"></div>


        </div>


        <div class="dt-responsive table-responsive">


            <table id="binary-table" class="table table-striped table-bordered nowrap">


              <thead>


                <tr>


                  <th><?= lang('description') ?></th>


                  <th style="display:none"><?= lang('label_points_left') ?></th>


                  <th style="display:none"><?= lang('label_points_right') ?></th>


                  <th><?= lang('date') ?></th>





              </tr>


          </thead>


          <tbody>





            <?php foreach ($binary_table as $binary_td) : ?>


                <tr>


                  <td><?= $binary_td['description'] ?></td>


                  <td style="display:none"><?= $binary_td['left_points']  ?></td>


                  <td style="display:none"><?= $binary_td['right_points'] ?></td>


                  <td><?= substr($binary_td['last_updated'], 0,7) ?></td>





              </tr>


          <?php endforeach;?>


      </tbody>


  </table>


</div>


</div>


</div>


</div>


</div>














<div class="col-md-12">


    <div class="card">


      <div class="row">


        <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">


          <div class="x_panel">


            <div class="x_title card-header">


              <h2><?= lang('recent_users') ?></h2>


              <div class="clearfix"></div>


          </div>


          <div class="card-block user-card">


              <ul class="card-block scroll-list cards basic-list list-icons-img">


                <?php foreach($recent['recents'] as $recent) { ?>


                    <li>


                      <img


                      src="/themes/<?= $this->settings->theme ?>/images/countries/<?= ($recent['country'] !='') ? strtolower($recent['country']) : "none"?>.gif"


                      alt="" class="img-fluid p-absolute d-block text-center">


                      <h6><?= ucfirst($recent['first_name'])." ".ucfirst($recent['last_name']) ?>


                      <label class="label label-info"><?= $recent['country'] ?></label>


                  </h6>


                  <p><?= format_interval(date("Y-m-").substr($recent['created'],8)) ?></p>


              </li>


          <?php } ?>


      </ul>


  </div>


</div>


</div>


</div>





</div>


</div>


</div>





<div class="card" id="contact">


  <div class="card-header borderless">


      <div class="card-header-left">


          <h4>Contacto</h4>


      </div>


  </div>


  <div class="card-block-big card-power">


    <div class="table-responsive">


      <table class="table table-framed">





          <tbody>


              <tr>








                  <td><b>Teléfono fijo:</b></td>


                  <td>(01)635 4385</td>


              </tr>


              <tr>








                  <td><b>Whatsapp:</b></td>


                  <td>+51 993978137</td>


              </tr>


              <tr>





                  <td><b>Mail:</b></td>


                  <td>info@fullnessglobal.com</td>


              </tr>


              <tr>





                  <td><b>Dirección:</b></td>


                  <td>Calle velasquez 351- San Borja,Lima,Perú</td>


              </tr>


          </tbody>


      </table>


  </div>


</div>


</div>





<div class="card">


    <div class="card-block-big card-power">


        <p class="text-center" style="margin-top: 3em" >


          <img src="<?= base_url("themes/able/images/visa-eps-vector-logo-400x400.png") ?>" style="width:48px"/>


          <b>Esta tienda está autorizada por Visa para realizar transacciones electrónicas.</b>


      </p>


  </div>


</div>





<!-- [ page content ] end -->


<style>


    .not-won {


      opacity: 0.6;


      background: transparent;


  }





  .not-won i {


      background: #ddd;


  }





  .not-won h6 {


      color: #ddd;


  }


</style>





<script>


    var chart = <?= json_encode($chart) ?>;


    let total = <?= $total_points_binary ?>;


    $(document).ready(function() {


      $('#residual-table').DataTable();

      $('#binary-table').DataTable();



      $('.progress-bar-bonus').each(function(){


        var points = $(this).data('points');


        this.style.width = `${total / points * 100}%`;


    });


  });





    (function () {





      let progress = document.getElementById('progressbar');


      let point_missing = document.getElementById('point_missing');


      let next_prize = document.getElementById('next_prize');


      let rangos = [];








      progress.style.width = `${total / 3000 * 100}%`





      if (total >= 3000 && total < 12000) {


    //Executive


    rangos.push('1');


    progress.style.width = `${total / 12000 * 100}%`;


} else if (total >= 12000 && total < 40000) {


    //Senior


    rangos.push('1', '2');


    progress.style.width = `${total / 40000 * 100}%`;


} else if (total >= 40000 && total < 90000) {


    //Ruby


    rangos.push('1', '2', '3');


    progress.style.width = `${total / 90000 * 100}%`;


} else if (total >= 90000 && total < 200000) {


    //Diamond


    rangos.push('1', '2', '3', '4');


    progress.style.width = `${total / 200000 * 100}%`;


} else if (total >= 200000 && total < 600000) {


    //Double Diamond


    rangos.push('1', '2', '3', '4', '5');


    progress.style.width = `${total / 600000 * 100}%`;


} else if (total >= 600000 && total < 1400000) {


    //Triple Diamond


    rangos.push('1', '2', '3', '4', '5', '6');


    progress.style.width = `${total / 1400000 * 100}%`;


} else if (total >= 1400000 && total < 3000000) {


    //Red Diamond


    rangos.push('1', '2', '3', '4', '5', '6', '7');


    progress.style.width = `${total / 3000000 * 100}%`;


} else if (total >= 3000000 && total < 7000000) {


    //Black Diamond


    rangos.push('1', '2', '3', '4', '5', '6', '7', '8');


    progress.style.width = `${total / 7000000 * 100}%`;


} else if (total >= 7000000 && total < 15000000) {


    //Grand Diamond


    rangos.push('1', '2', '3', '4', '5', '6', '7', '8', '9');


    progress.style.width = `${total / 15000000 * 100}%`;


} else if (total >= 15000000) {


    //Grand Black Diamond


    rangos.push('1', '2', '3', '4', '5', '6', '7', '8', '9', '10');


    progress.style.width = `100%`;


}





for (const key in rangos) {


    let id = document.getElementById(rangos[key]);


    id.classList.remove("not-won");


}


})();


</script>