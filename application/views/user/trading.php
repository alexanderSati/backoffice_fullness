<?php defined('BASEPATH') OR exit('No direct script access allowed');

$lang = (isset($user['language']) ? $user['language'] : $this->config->item('language'));

?>

<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
  <div id="charts">
    <div id="highcharts-graph" data-highcharts-chart="0"></div>
  </div>
  <div id="metadata" class="hidden" data-slug="<?= $chartSymbols[$symbol] ?>"
    data-name="<?= ucfirst($chartSymbols[$symbol]) ?>" data-apidomain="https://graphs.coinmarketcap.com"
    data-mobile="False"></div>
  <div id="static_domain" data-staticdomain="files.coinmarketcap.com"></div>
</div>

<script>
var feePercent = <?= ($current_fee / 100) ?>;
var pusherChannel = '<?= $pusherChannel ?>';
var pusherKey = '<?= $pusherKey ?>';
var pusherCluster = '<?= $pusherCluster ?>';
</script>
<div class="container">
  <div class="row">
    <div class="col-lg-8 col-md-12 col-sm-12 col-xs-12">
      <div class="row">
        <div class="col-md-6 col-sm-12 col-xs-12">

          <div class="x_panel">
            <div class="x_title">
              <h2><i class="fa fa-plus-circle buy_order" aria-hidden="true"></i>
                <?= lang('buy_trading') ?>&nbsp;<?= $symbol ?></h2>
              <div class="clearfix"></div>
            </div>
            <div class="x_content">
              <?php echo form_open("user/trading/{$symbol}/buy", array('id' => 'buyForm', 'method' => 'POST', 'class' => 'form-horizontal form-label-left form_order calc buy', 'role' => 'form')); ?>
              <div class="item form-group">
                <?php echo form_label(lang('balance'), '', array('class' => 'control-label col-md-3 col-sm-2 col-xs-12')); ?>
                <div class="col-md-9 col-sm-10 col-xs-12">
                  <?php echo form_label("<code>{$balance} {$symbol}</code>", '',  array('id' =>'balance_buy', 'class' => 'control-label col-md-12 col-sm-12 col-xs-12')); ?>
                </div>
              </div>
              <div class="item form-group">
                <?php echo form_label(lang('amount') . ' *', 'amount', array('id' => 'amount_buy_label', 'class' => 'control-label col-md-3 col-sm-2 col-xs-12')); ?>
                <div class="col-md-9 col-sm-10 col-xs-12 form-group has-feedback">
                  <input class="form-control has-feedback-left" name="amount" value="<?= $amount ?>" type="text"
                    required="">
                  <span class="form-control-feedback left span-has-feedback"><?= $symbol ?></span>
                </div>
              </div>
              <div class="item form-group">
                <?php echo form_label(lang('price') . ' *', 'price', array('class' => 'control-label col-md-3 col-sm-2 col-xs-12')); ?>
                <div class="col-md-9 col-sm-10 col-xs-12 form-group has-feedback">
                  <input class="form-control has-feedback-left" name="btc_price" value="<?= $btc_price ?>" type="text"
                    required="" data-inputmask="'mask': '9.99999999'">
                  <span class="form-control-feedback left span-has-feedback"><?php echo lang('bitcoin'); ?></span>
                </div>
              </div>
              <div class="item form-group">
                <?php echo form_label(lang('total') . ' *', 'total', array('class' => 'control-label col-md-3 col-sm-2 col-xs-12')); ?>
                <div class="col-md-9 col-sm-10 col-xs-12 form-group has-feedback">
                  <input class="form-control has-feedback-left" name="btc_total" value="<?= $btc_total ?>" type="text"
                    readonly="readonly">
                  <span class="form-control-feedback left span-has-feedback"><?php echo lang('bitcoin'); ?></span>
                </div>
              </div>
              <div class="item form-group">
                <?php echo form_label(lang('fee')." (".$current_fee."%)", 'fee', array('class' => 'control-label col-md-3 col-sm-2 col-xs-12')); ?>
                <div class="col-md-9 col-sm-10 col-xs-12 form-group has-feedback">
                  <input class="form-control has-feedback-left" name="btc_fee" value="<?= $btc_fee ?>" type="text"
                    readonly="readonly">
                  <span class="form-control-feedback left span-has-feedback"><?php echo lang('bitcoin'); ?></span>
                </div>

              </div>
              <div class="item form-group">
                <?php echo form_label(lang('total_fee'), 'total_fee', array('class' => 'control-label col-md-3 col-sm-2 col-xs-12')); ?>
                <div class="col-md-9 col-sm-3 col-xs-12">
                  <div class="input-group">
                    <input class="form-control" type="text" name="btc_fee_total"
                      value="<?= number_format($btc_fee + $btc_total,8); ?>" readonly="">
                    <span class="input-group-btn">
                      <button type="submit" class="btn btn-success"><?php echo lang('buy_trading'); ?></button>
                    </span>
                  </div>
                </div>
              </div>
              <div class="divider-dashed"></div>
              <p><?= lang('form_fee_text') ?></p>
              <?php echo form_close(); ?>
            </div>
          </div>

        </div>
        <div class="col-md-6 col-sm-12 col-xs-12">

          <div class="x_panel">
            <div class="x_title">
              <h2><i class="fa fa-minus-circle sell_order_text" aria-hidden="true"></i>
                <?= lang('sell') ?>&nbsp;<?= $symbol ?></h2>
              <div class="clearfix"></div>
            </div>
            <div class="x_content">
              <?php echo form_open("user/trading/{$symbol}/sell", array('id' => 'sellForm', 'method' => 'POST', 'class' => 'form-horizontal form-label-left form_order calc sell', 'role' => 'form')); ?>
              <div class="item form-group">
                <?php echo form_label(lang('balance'), '', array('class' => 'control-label col-md-3 col-sm-2 col-xs-12')); ?>
                <div class="col-md-9 col-sm-10 col-xs-12">
                  <?php echo form_label("<code>{$balance} {$symbol}</code>", '',  array('id' =>'balance_buy', 'class' => 'control-label col-md-12 col-sm-12 col-xs-12')); ?>
                </div>
              </div>
              <div class="item form-group">
                <?php echo form_label(lang('amount') . ' *', 'amount', array('id' => 'amount_buy_label', 'class' => 'control-label col-md-3 col-sm-2 col-xs-12')); ?>
                <div class="col-md-9 col-sm-10 col-xs-12 form-group has-feedback">
                  <input class="form-control has-feedback-left" name="amount" value="<?= $amount ?>" type="text"
                    required="">
                  <span class="form-control-feedback left span-has-feedback"><?= $symbol ?></span>
                </div>
              </div>
              <div class="item form-group">
                <?php echo form_label(lang('price') . ' *', 'price', array('class' => 'control-label col-md-3 col-sm-2 col-xs-12')); ?>
                <div class="col-md-9 col-sm-10 col-xs-12 form-group has-feedback">
                  <input class="form-control has-feedback-left" name="btc_price" value="<?= $btc_price ?>" type="text"
                    data-inputmask="'mask': '9.99999999'" required="">
                  <span class="form-control-feedback left span-has-feedback"><?php echo lang('bitcoin'); ?></span>
                </div>
              </div>
              <div class="item form-group">
                <?php echo form_label(lang('total') . ' *', 'total', array('class' => 'control-label col-md-3 col-sm-2 col-xs-12')); ?>
                <div class="col-md-9 col-sm-10 col-xs-12 form-group has-feedback">
                  <input class="form-control has-feedback-left" name="btc_total" value="<?= $btc_total ?>" type="text"
                    readonly="readonly">
                  <span class="form-control-feedback left span-has-feedback"><?php echo lang('bitcoin'); ?></span>
                </div>
              </div>
              <div class="item form-group">
                <?php echo form_label(lang('fee')." (".$current_fee."%)", 'fee', array('class' => 'control-label col-md-3 col-sm-2 col-xs-12')); ?>
                <div class="col-md-9 col-sm-10 col-xs-12 form-group has-feedback">
                  <input class="form-control has-feedback-left" name="btc_fee" value="<?= $btc_fee ?>" type="text"
                    readonly="readonly">
                  <span class="form-control-feedback left span-has-feedback"><?php echo lang('bitcoin'); ?></span>
                </div>

              </div>
              <div class="item form-group">
                <?php echo form_label(lang('total_fee_less'), 'total_fee', array('class' => 'control-label col-md-3 col-sm-2 col-xs-12')); ?>
                <div class="col-md-9 col-sm-3 col-xs-12">
                  <div class="input-group">
                    <input class="form-control" type="text" name="btc_fee_total"
                      value="<?= number_format($btc_total - $btc_fee,8) ?>" readonly="">
                    <span class="input-group-btn">
                      <button type="submit" class="btn btn-warning"><?php echo lang('sell'); ?></button>
                    </span>
                  </div>
                </div>
              </div>
              <div class="divider-dashed"></div>
              <p><?= lang('form_fee_text') ?></p>
              <?php echo form_close(); ?>
            </div>
          </div>

        </div>
      </div>
      <div class="clearfix"></div>
      <div class="row">
        <div class="col-md-6 col-sm-12 col-xs-12 ">

          <div class="x_panel">
            <div class="x_title">
              <h2><span class="fa fa-angle-double-right" aria-hidden="true"></span> <?= lang('sell_order') ?></h2>
              <div class="clearfix"></div>
            </div>
            <div class="x_content">
              <table class="table table-striped dt-responsive nowrap col-md-12" id="sellTable">
                <thead>
                  <tr>
                    <th>Cod</th>
                    <th><?= lang('price') ?></th>
                    <th><?= $symbol ?></th>
                    <th><?= lang('bitcoin') ?></th>
                    <th></th>
                  </tr>
                </thead>
                <tbody>
                  <?php foreach($sell_orders as $sell_order){ ?>
                  <tr id="<?= $sell_order['id'] ?>" role="row" class="odd">
                    <td><?= $sell_order['id'] ?></td>
                    <td><?= number_format($sell_order['btc_price'],8) ?></td>
                    <td><?= number_format($sell_order['amount'],8) ?></td>
                    <td><?= number_format($sell_order['btc_total'] + $sell_order['btc_fee'],8) ?></td>

                    <?php if($current_user === $sell_order['id_user']){ ?>
                    <td><a href="/user/trading/cancel/<?= $symbol ?>/<?= $sell_order['id'] ?>"
                        class="btn btn-danger btn-xs"><?php echo lang('cancel'); ?></a></td>
                    <?php }else{ ?>
                    <td><a href="/user/trading/get/<?= $symbol ?>/<?= $sell_order['id'] ?>"
                        class="btn btn-success btn-xs"><?php echo lang('buy'); ?></a></td>
                    <?php } ?>
                  </tr>
                  <?php } ?>
                </tbody>
              </table>
            </div>
          </div>

        </div>
        <div class="col-md-6 col-sm-12 col-xs-12">

          <div class="x_panel">
            <div class="x_title">
              <h2><span class="fa fa-angle-double-left" aria-hidden="true"></span> <?= lang('buy_order') ?></h2>
              <div class="clearfix"></div>
            </div>
            <div class="x_content">
              <table class="table table-striped dt-responsive nowrap" id="buyTable">
                <thead>
                  <tr>
                    <th>Cod</th>
                    <th><?= lang('price') ?></th>
                    <th class="currency_history"><?= $symbol ?></th>
                    <th><?= lang('bitcoin') ?></th>
                    <th></th>
                  </tr>
                </thead>
                <tbody>
                  <?php foreach($buy_orders as $buy_order){ ?>
                  <tr id="<?= $buy_order['id'] ?>" role="row" class="odd">
                    <td><?= $buy_order['id'] ?></td>
                    <td><?= number_format($buy_order['btc_price'],8) ?></td>
                    <td><?= number_format($buy_order['amount'],8) ?></td>
                    <td><?= number_format(($buy_order['btc_total'] + $buy_order['btc_fee']),8) ?></td>
                    <?php if($current_user === $buy_order['id_user']){ ?>
                    <td><a href="/user/trading/cancel/<?= $symbol ?>/<?= $buy_order['id'] ?>"
                        class="btn btn-danger btn-xs"><?php echo lang('cancel'); ?></a></td>
                    <?php }else{ ?>
                    <td><a href="/user/trading/get/<?= $symbol ?>/<?= $buy_order['id'] ?>"
                        class="btn btn-warning btn-xs"><?php echo lang('sell'); ?></a></td>
                    <?php } ?>
                  </tr>
                  <?php } ?>
                </tbody>
              </table>
            </div>
          </div>

        </div>
      </div>
    </div>
    <div class="col-lg-4 col-md-12 col-sm-12 col-xs-12">

      <div class="x_panel">
        <div class="x_title">
          <h2><i class="fa fa-list-alt" aria-hidden="true"></i>&nbsp;<?= $symbol ?>&nbsp;<?= lang('trade_history') ?>
          </h2>
          <div class="clearfix"></div>
        </div>
        <div class="x_content">
          <table class="table table-striped dt-responsive nowrap" id="historyTable">
            <thead>
              <tr>
                <th><?= lang('time') ?></th>
                <th></th>
                <th><?php echo lang('amount'); ?></th>
                <th><?= lang('price') ?></th>
              </tr>
            </thead>
            <tbody>
              <?php foreach($history_orders as $history){ ?>
              <tr class="<?= $history['type'] ?>_order">
                <td class="date_trading"><?= date("H:i:s",strtotime($history['date'])) ?></td>
                <td><?= lang($history['type']) ?></td>
                <td><?= number_format($history['amount'],8) ?></td>
                <td><?= number_format($history['btc_total'],8) ?></td>
              </tr>
              <?php } ?>
            </tbody>
          </table>
        </div>
      </div>

    </div>
  </div>
</div>