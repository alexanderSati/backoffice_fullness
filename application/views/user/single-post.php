<?php defined('BASEPATH') OR exit('No direct script access allowed'); ?>
<?php $num_post  = 1; ?>
<?php $image_tmp = [
    1 => 'https://ichef.bbci.co.uk/news/660/cpsprodpb/17D3/production/_98999060_gettyimages-862634710.jpg',
    2 => 'https://www.eltiempo.com/files/article_main/files/crop/uploads/2018/02/12/5a8206958f656.r_1533519176701.0-227-3000-1727.jpeg',
    3 => 'https://files.lafm.com.co/assets/public/styles/image_631x369/public/2018-06/bitcoin-afp_1_0.jpg?itok=2yeKXG1i',
    4 => 'https://static.iris.net.co/dinero/upload/images/2018/11/28/264829_1.jpg'

]; ?>

<div class="row">
    <div class="col-lg-8 col-md-12 col-sm-12 col-xs-12">
        <div class="x_panel">
            <div class="x_title">
                <h2><?= $page_title ?></h2>
                <div class="clearfix"></div>
            </div>
            <div class="x_content">
                <!-- start accordion -->
                <!--<div class="accordion" id="accordion1" role="tablist" aria-multiselectable="true">-->
                    <?php //foreach ($posts as $post) { ?>
                        <div class="panel">
                            <div class="panel-body">
                                <div class="post">
                                    <div class="post_image">
                                        <img src="<?= $image_tmp[$num_post]; ?>" alt="<?= $post_info->post_title; ?>">
                                    </div>
                                    <p class="post_content"><?= nl2br($post_info->post_content); ?></p>
                                    <p><?= nl2br($post_info->post_excerpt); ?></p>
                                </div>
                            </div>
                        </div>
                    <?php //}?>
                <!--</div>-->
                <!-- end of accordion -->
            </div>
        </div>
    </div>
    <div class="col-lg-4 col-md-12 col-sm-12 col-xs-12">
        <?php if (isset($message) && !($message=='<p><br></p>')){?>        
            <div class="x_panel">
                <div class="x_title">
                    <h2><?= lang('welcome') ?></h2>
                    <div class="clearfix"></div>
                </div>
                <div class="x_content">
                    <?= $message ?> 
                </div>
            </div>   
        <?php }?>
    </div>
</div>