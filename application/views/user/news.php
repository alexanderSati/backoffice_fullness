<?php defined('BASEPATH') OR exit('No direct script access allowed'); ?>
<?php $num_post  = 1; ?>

<div class="row">
    <div class="col-md-12 col-sm-12 col-xs-12">
        <div class="x_panel">
            <div class="x_title">
                <h2><?= $page_title ?></h2>
                <div class="clearfix"></div>
            </div>
            <div class="x_content">
                <!-- start accordion -->
                <!--<div class="accordion" id="accordion1" role="tablist" aria-multiselectable="true">-->
                <div class="posts">
                    
                    <?php foreach ($post_info as $post) { ?>
                        
                        <?php $news_content = ($num_post > 1) ? 'side_news_content' : 'main_news_content'; ?>
                        <?php if ($num_post == 1) { ?>
                            <div class="main_news">
                        <?php } ?>
                        <?php if ($num_post == 2) { ?>
                            <div class="side_news">
                        <?php } ?>
                    
                        <div class="panel">
                            <a href="<?= base_url()."user/news/{$post['slug']}/{$post['post_name']}"; ?>">
                                <div class="<?= $news_content; ?>">
                                    <div class="news_image">
                                        <img src="<?= $post['image']; ?>" alt="<?= $post['post_title']; ?>">
                                    </div>
                                    <div class="news_content">
                                        <h5><i class="fa fa-clock-o" aria-hidden="true"></i> <?= $post['post_date']; ?></h5>
                                        <h3 class="panel-title"><?= $post['post_title']; ?></h3>
                                    </div>
                                </div>
                            </a>
                        </div>
                            
                        <?php if ($num_post == 1) { ?>
                            </div>
                        <?php } ?>
                        <?php if ($num_post == count($post_info)) { ?>
                            </div>
                        <?php } ?>
                    <?php $num_post++; ?>
                    <?php } ?>
                </div>
                <!--</div>-->
                <!-- end of accordion -->
            </div>
        </div>
    </div>
</div>
