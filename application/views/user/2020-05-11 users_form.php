<?php defined('BASEPATH') OR exit('No direct script access allowed'); ?>

<?php $lang = (isset($user['language']) ? $user['language'] : $this->config->item('language')); ?>


<div class="card px-5">
  <div class="card-block">
    <?php echo form_open_multipart('', array('id'=>'example-advanced-form', 'class' => 'wizard-form', 'data-parsley-validate' => '','role' => 'form')); ?>

    <?php // hidden id ?>
    <?php if (isset($user_id)) : ?>
    <?php echo form_hidden('id', $user_id); ?>
    <?php endif; ?>


    <!-- TIPO DE DCTO -->
    <div class="form-group row m-t-20 px-5">
      <div class="col-md-5 col-lg-2">
        <label for="doc_type" class="block"><?= lang("doc_type_lbl") ?> *</label></div>
        <div class="col-md-7 col-lg-9">
          <select class="form-control" name="doc_type" required="" id="doc_type" >
            <option value="">-- seleccione --</option>
            <option value="DNI">DNI</option>
            <option value="EXTRANGE_CARD">Carnet de extranjería</option>
            <option value="PASSPORT">Pasaporte</option>
          </select>
        </div>
    </div>
    <!-- ./TIPO DE DCTO -->

    <!----DNI--->
    <div class="form-group row m-t-20 px-5">
      <div class="col-md-5 col-lg-2">
        <?php echo form_label(lang('users input dni') . ' *', 'dni', array('class' => 'block')); ?>
      </div>
      <div class="col-md-7 col-lg-9">
        <?php echo form_input(
            array('name' => 'dni','required'=>'required', 'value' => set_value('dni', (isset($user['dni']) ? $user['dni'] : '')), 
            'class' => 'form-control '.(!empty(form_error('dni'))? 'error-input':''))); ?>
      </div>
    </div>
    <!-- ./DNI -->
    <div class="form-group row px-5">
      <div class="col-md-5 col-lg-2">
        <?php echo form_label(lang('users input username') . ' *', 'username', array('class' => 'block')); ?>
      </div>
      <div class="col-md-7 col-lg-9">
        <input type="text" name="username" required <?= (isset($user_id)) ? "readonly" : "" ?>
          value="<?= set_value('username', (isset($user['username']) ? $user['username'] : '')) ?>"
          class="form-control <?= (!empty(form_error('username'))? 'error-input':'') ?>">
      </div>
    </div>
    <div class="form-group row px-5">
      <div class="col-md-5 col-lg-2">
        <?php echo form_label(lang('users input first_name') . ' *', 'first_name', array('class' => 'block')); ?>
      </div>
      <div class="col-md-7 col-lg-9">
        <?php echo form_input(array(
            'name' => 'first_name',
            'required'=>'required', 
            'value' => set_value('first_name', (isset($user['first_name']) ? $user['first_name'] : '')), 
            'class' => 'form-control '.(!empty(form_error('first_name'))? 'error-input':'')
        )); ?>
      </div>
    </div>
    <div class="form-group row px-5">
      <div class="col-md-5 col-lg-2">
        <?php echo form_label(lang('users input last_name') . ' *', 'last_name', array('class' => 'block')); ?>
      </div>
      <div class="col-md-7 col-lg-9">
        <?php echo form_input(array(
            'name' => 'last_name',
            'required'=>'required', 
            'value' => set_value('last_name', (isset($user['last_name']) ? $user['last_name'] : '')), 
            'class' => 'form-control '.(!empty(form_error('last_name'))? 'error-input':'')
            )); ?>
      </div>
    </div>
    <div class="form-group row px-5">
      <div class="col-md-5 col-lg-2">
        <?php echo form_label(lang('users input email') . ' *', 'email', array('class' => 'block')); ?>
      </div>
      <div class="col-md-7 col-lg-9">
        <?php echo form_input(array(
            'name' => 'email',
            'required'=>'required', 
            'value' => set_value('email', (isset($user['email']) ? $user['email'] : '')), 
            'class' => 'form-control '.(!empty(form_error('email'))? 'error-input':''), 
            'type' => 'email'
        )); ?>
      </div>
    </div>

    <!--------BIRTHDATE-------->
    <div class="form-group row px-5">
      <div class="col-md-5 col-lg-2">
        <?php echo form_label(lang('users input birthdate') . ' *', 'birthdate', array('class' => 'block')); ?>
      </div>
      <div class="col-md-7 col-lg-9">
        <?php echo form_input(array(
            'name' => 'birthdate',
            'required'=>'required', 
            'value' => set_value('birthdate', (isset($user['birthdate']) ? $user['birthdate'] : '')), 
            'class' => 'form-control '.(!empty(form_error('birthdate'))? 'error-input':''), 
            'type' => 'date'
        )); ?>
      </div>
    </div>

    <!--------PHONE------------>
    <div class="form-group row px-5">
      <div class="col-md-5 col-lg-2">
        <?php echo form_label(lang('users input phone') . ' *', 'phone', array('class' => 'block')); ?>
      </div>
      <div class="col-md-7 col-lg-9">
        <?php echo form_input(array(
            'name' => 'phone',
            'required'=>'required', 
            'value' => set_value('phone', (isset($user['phone']) ? $user['phone'] : '')), 
            'class' => 'form-control '.(!empty(form_error('phone'))? 'error-input':''), 
            'type' => 'text'
            )); ?>
      </div>
    </div>
    <!--------ADDRESS---------->
    <div class="form-group row px-5">
      <div class="col-md-5 col-lg-2">
        <?php echo form_label(lang('users input address') . ' *', 'address', array('class' => 'block')); ?>
      </div>
      <div class="col-md-7 col-lg-9">
        <?php echo form_input(array(
            'name' => 'address',
            'required'=>'required', 
            'value' => set_value('address', (isset($user['address']) ? $user['address'] : '')), 
            'class' => 'form-control '.(!empty(form_error('address'))? 'error-input':''), 
            'type' => 'text'
        )); ?>
      </div>
    </div>

    <!--------ADDRESS 2---------->
    <div class="form-group row px-5">
      <div class="col-md-5 col-lg-2">
        <?php echo form_label(lang('users input address2'), 'address2', array('class' => 'block')); ?>
      </div>
      <div class="col-md-7 col-lg-9">
        <?php echo form_input(array(
            'name' => 'address2',
            'value' => set_value('address2', (isset($user['address2']) ? $user['address2'] : '')), 
            'class' => 'form-control '.(!empty(form_error('address2'))? 'error-input':''), 
            'type' => 'text'
        )); ?>
      </div>
    </div>

    <!--------DEPARTMENT --------------->
    <div class="form-group row px-5">
      <div class="col-md-5 col-lg-2">
        <?php echo form_label(lang('users input department'). ' *', 'department', array('class' => 'block')); ?>
      </div>
      <div class="col-md-7 col-lg-9">
        <?php echo form_input(array(
            'name' => 'department',
            'required'=>'required' ,
            'value' => set_value('department', (isset($user['department']) ? $user['department'] : '')), 
            'class' => 'form-control '.(!empty(form_error('department'))? 'error-input':''), 
            'type' => 'text'
            )); ?>
      </div>
    </div>
    <!--------CITY--------------->
    <div class="form-group row px-5">
      <div class="col-md-5 col-lg-2">
        <?php echo form_label(lang('users input city'). ' *', 'city', array('class' => 'block')); ?>
      </div>
      <div class="col-md-7 col-lg-9">
        <?php echo form_input(array(
            'name' => 'city', 
            'required'=>'required', 
            'value' => set_value('city', (isset($user['city']) ? $user['city'] : '')), 
            'class' => 'form-control '.(!empty(form_error('city'))? 'error-input':''), 
            'type' => 'text'
        )); ?>
      </div>
    </div>


    <!--------POSTAL CODE------------->
    <div class="form-group row px-5">
      <div class="col-md-5 col-lg-2">
        <?php echo form_label(lang('users input postal_code'). ' *', 'postal_code', array('class' => 'block')); ?>
      </div>
      <div class="col-md-7 col-lg-9">
        <?php echo form_input(array(
            'name' => 'postal_code',
            'required'=>'required',
            'value' => set_value('postal_code', (isset($user['postal_code']) ? $user['postal_code'] : '')), 
            'class' => 'form-control '.(!empty(form_error('postal_code'))? 'error-input':''), 
            'type' => 'text')); ?>
      </div>
    </div>

    <!-------- BANK ACCOUNT ---------->
    <div class="form-group row px-5" style="display:none">
      <div class="col-md-5 col-lg-2">
        <?php echo form_label(lang('users input bank_account'). ' *', 'bank_account', array('class' => 'block')); ?>
      </div>
      <div class="col-md-7 col-lg-9">
        <?php echo form_input(array(
            'name' => 'bank_account',
            'value' => set_value('bank_account', (isset($user['bank_account']) ? $user['bank_account'] : '10000001')), 
            'class' => 'form-control '.(!empty(form_error('postal_code'))? 'error-input':''), 
            'type' => 'text'
        )); ?>
      </div>
    </div>

    <div class="form-group row px-5">
      <div class="col-md-5 col-lg-2">
        <?php echo form_label(lang('users input language'), 'language', array('class' => 'block')); ?>
      </div>
      <div class="col-md-7 col-lg-9">
        <?php echo form_dropdown('language', $this->languages, (isset($user['language']) ? $user['language'] : $this->config->item('language')), 'id="language" class="form-control"'); ?>
      </div>
    </div>
    <div class="form-group row px-5">
      <div class="col-md-5 col-lg-2">
        <?php echo form_label(lang('users input country'), 'country', array('class' => 'block')); ?>
      </div>
      <div class="col-md-7 col-lg-9">
        <?php echo form_dropdown(
            'country', 
            $countries, 
            (isset($user['country']) ? $user['country'] : ''), 
            'id="country" required="required" class="select2_single form-control '.(!empty(form_error('postal_code'))? 'error-input':'').'"'
        ); ?>
      </div>
    </div>

    <!-- <div class="form-group row px-5">
        <div class="col-md-5 col-lg-2">
            <?php echo form_label(lang('users input status'), '', array('class'=>'block')); ?>
        </div>    
        <div class="col-md-7 col-lg-9">
            <div class="radio">
                <label>
                    <?php echo form_radio(array('name'=>'status', 'id'=>'radio-status-1', 'class'=>'flat', 'value'=>'1', 'checked'=>(( ! isset($user['status']) OR (isset($user['status']) && (int)$user['status'] == 1) OR $user['id'] == 1) ? 'checked' : FALSE))); ?>
                    <?php echo lang('admin input active'); ?>
                </label>
            </div>
            <?php if ( !$user['id'] OR $user['id'] > 1) : ?>
                <div class="radio">
                    <label>
                        <?php echo form_radio(array('name'=>'status', 'id'=>'radio-status-2', 'class'=>'flat', 'value'=>'0', 'checked'=>((isset($user['status']) && (int)$user['status'] == 0) ? 'checked' : FALSE))); ?>
                        <?php echo lang('admin input inactive'); ?>
                    </label>
                </div>
            <?php endif; ?>
        </div>
    </div> -->

    <?php // administrator ?>
    <!-- <div class="form-group row px-5">
        <div class="col-md-5 col-lg-2">
            <?php echo form_label(lang('users input is_admin'), '', array('class'=>'block')); ?>
        </div>
        <div class="col-md-7 col-lg-9">
            <?php if ( ! $user['id'] OR $user['id'] > 1) : ?>
                <div class="radio">
                    <label>
                        <?php echo form_radio(array('name'=>'is_admin', 'id'=>'radio-is_admin-1', 'class'=>'flat', 'value'=>'0', 'checked'=>(( ! isset($user['is_admin']) OR (isset($user['is_admin']) && (int)$user['is_admin'] == 0) && $user['id'] != 1) ? 'checked' : FALSE))); ?>
                        <?php echo lang('core text no'); ?>
                    </label>
                </div>
            <?php endif; ?>
            <div class="radio">
                <label>
                    <?php echo form_radio(array('name'=>'is_admin', 'id'=>'radio-is_admin-2', 'class'=>'flat', 'value'=>'1', 'checked'=>((isset($user['is_admin']) && (int)$user['is_admin'] == 1) ? 'checked' : FALSE))); ?>
                    <?php echo lang('core text yes'); ?>
                </label>
            </div>
        </div>
    </div> -->

    <div class="form-group row px-5">
      <div class="col-md-5 col-lg-2">
        <?php echo form_label(lang('users input password') . ' *' , 'password', array('class' => 'block')); ?>
      </div>
      <div class="col-md-7 col-lg-9">
        <?php echo form_password(array(
            'name' => 'password', 
            'value' => '', 
            'id'=>'password', 
            'class' => 'form-control '.(!empty(form_error('postal_code'))? 'error-input':'')
            )); ?>
      </div>
    </div>
    <div class="form-group row px-5">
      <div class="col-md-5 col-lg-2">
        <?php echo form_label(lang('users input password_repeat'). ' *', 'password_repeat', array('class' => 'block')); ?>
      </div>
      <div class="col-md-7 col-lg-9">
        <?php echo form_password(array(
            'name' => 'password_repeat', 
            'data-parsley-trigger'=> 'change' , 
            'data-parsley-equalto'=>'#password', 
            'value' => '', 
            'class' => 'form-control '.(!empty(form_error('postal_code'))? 'error-input':'')
            )); ?>
      </div>
      <?php if (!$password_required) : ?>
      <span class="help-block"><br /><?php echo lang('users help passwords'); ?></span>
      <?php endif; ?>
    </div>
    <div class="form-group row px-5">
      <div class="col-md-5 col-lg-2">
        <?php echo form_label(lang('users input image') . ' *' , 'image', array('class' => 'block')); ?>
      </div>
      <div class="col-md-7 col-lg-9">
        <?php echo form_upload(array('name' => 'image', 'id'=>'image', 'class' => 'form-control')); ?>
      </div>
    </div>

    <div class="form-group row px-5">
      <div class="col-md-5 col-lg-2">
        <?php echo form_label(lang('users input side'), '', array('class'=>'control-label')); ?>
      </div>

      <div class="col-md-7 col-lg-9">
        <div class="form-radio">
          <div class="radio">
            <label>
              <?php echo form_radio(array('name'=>'side', 'id'=>'radio-side-1', 'class'=>'', 'value'=>'left', 'checked' => true)); ?>
              <i class="helper"></i>
              <?php echo lang('users input side-left'); ?>
            </label>
          </div>
        </div>
        <div class="form-radio">
          <div class="radio">
            <label>
              <?php echo form_radio(array('name'=>'side', 'id'=>'radio-side-2', 'class'=>'', 'value'=>'right')); ?>
              <i class="helper"></i>
              <?php echo lang('users input side-right'); ?>
            </label>
          </div>
        </div>
        <div><?= lang('users input remember') ?></div>
      </div>
    </div>
    <!-- TERMINOS Y CONDICIONES -->
    <div class="col-md-12 text-center">
      <div class="checkbox-fade fade-in-primary">
        <label>
          <?php echo form_checkbox(array('name' => 'terms', 'id'=>'terms', 'class' => 'form-control flat','checked'=> FALSE,'value'=> 'off','required'=>'required')); ?>
          <span class="cr"><i class="cr-icon feather icon-check txt-success f-w-600"></i></span>
          <span class="text-inverse"><?= lang('users input terms'); ?></span>
        </label>
      </div>
    </div>
    <!-- ./TERMINOS Y CONDICIONES -->

    <div>
      <hr />
    </div>




    <div class="form-group row px-5">
      <div class="col-md-12 text-center">
        <a class="btn btn-danger" href="<?php echo $cancel_url; ?>" style="background-color: #DD3B3B">
          <?php echo lang('core button cancel'); ?>
        </a>
        <button type="submit" name="submit" class="btn btn-primary"><span class="fa fa-check-square-o"></span>
          <?php echo lang('core button register'); ?></button>
      </div>
    </div>

    <?php echo form_close(); ?>
  </div>
  <!-- end of card-block -->
</div>