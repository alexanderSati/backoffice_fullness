<?php defined('BASEPATH') OR exit('No direct script access allowed');
$lang = (isset($user['language']) ? $user['language'] : $this->config->item('language')); ?>

<div id="token-message"></div>


<div class="card px-5">
  <div class="card-block">
    <?php echo form_open_multipart('', array('id'=>'example-advanced-form', 'class' => 'wizard-form', 'data-parsley-validate' => '','role' => 'form')); ?>

    <!-----------DNI---------->
    <div class="form-group row m-t-20 px-5">
      <div class="col-md-5 col-lg-2">
        <?php echo form_label(lang('users input dni') . ' *', 'dni', array('class' => 'block')); ?>
      </div>
      <div class="col-md-7 col-lg-9">
        <input type="text" name="dni" 
          value="<?= set_value('dni', (isset($user['dni']) ? $user['dni'] : '')) ?>" class="form-control">
      </div>
    </div>

    <div class="form-group row px-5">
      <div class="col-md-5 col-lg-2">
        <?php echo form_label(lang('users input username') . ' *', 'username', array('class' => 'block')); ?>
      </div>
      <div class="col-md-7 col-lg-9">
        <input type="text" name="username" required <?= (isset($user['username'])) ? "readonly" : "" ?>
          value="<?= set_value('username', (isset($user['username']) ? $user['username'] : '')) ?>"
          class="form-control">
      </div>
    </div>

    <div class="form-group row px-5">
      <div class="col-md-5 col-lg-2">
        <?php echo form_label(lang('users input first_name') . ' *', 'first_name', array('class' => 'block')); ?>
      </div>
      <div class="col-md-7 col-lg-9">
        <?php echo form_input(array('name' => 'first_name','required'=>'required', 'value' => set_value('first_name', (isset($user['first_name']) ? $user['first_name'] : '')), 'class' => 'form-control')); ?>
      </div>
    </div>

    <div class="form-group row px-5">
      <div class="col-md-5 col-lg-2">
        <?php echo form_label(lang('users input last_name') . ' *', 'last_name', array('class' => 'block')); ?>
      </div>
      <div class="col-md-7 col-lg-9">
        <?php echo form_input(array('name' => 'last_name','required'=>'required', 'value' => set_value('last_name', (isset($user['last_name']) ? $user['last_name'] : '')), 'class' => 'form-control')); ?>
      </div>
    </div>

    <div class="form-group row px-5">
      <div class="col-md-5 col-lg-2">
        <?php echo form_label(lang('users input email') . ' *', 'email', array('class' => 'block')); ?>
      </div>
      <div class="col-md-7 col-lg-9">
        <input type="email" name="email" 
          value="<?= set_value('email', (isset($user['email']) ? $user['email'] : '')) ?>" class="form-control">
      </div>
    </div>
    <!--------BIRTHDATE--------->
    <div class="form-group row px-5">
      <div class="col-md-5 col-lg-2">
        <?php echo form_label(lang('users input birthdate') . ' *', 'birthdate', array('class' => 'block')); ?>
      </div>
      <div class="col-md-7 col-lg-9">
        <?php echo form_input(array('name' => 'birthdate','required'=>'required', 'value' => set_value('birthdate', (isset($user['birthdate']) ? $user['birthdate'] : '')), 'class' => 'form-control', 'type' => 'date')); ?>
      </div>
    </div>

    <!--------PHONE--------->
    <div class="form-group row px-5">
      <div class="col-md-5 col-lg-2">
        <?php echo form_label(lang('users input phone') . ' *', 'phone', array('class' => 'block')); ?>
      </div>
      <div class="col-md-7 col-lg-9">
        <?php echo form_input(array('name' => 'phone','required'=>'required', 'value' => set_value('phone', (isset($user['phone']) ? $user['phone'] : '')), 'class' => 'form-control')); ?>
      </div>
    </div>

    <!--------ADDRESS---------->
    <div class="form-group row px-5">
      <div class="col-md-5 col-lg-2">
        <?php echo form_label(lang('users input address') . ' *', 'address', array('class' => 'block')); ?>
      </div>
      <div class="col-md-7 col-lg-9">
        <?php echo form_input(array('name' => 'address','required'=>'required', 'value' => set_value('address', (isset($user['address']) ? $user['address'] : '')), 'class' => 'form-control')); ?>
      </div>
    </div>

    <!--------ADDRESS 2---------->
    <div class="form-group row px-5">
      <div class="col-md-5 col-lg-2">
        <?php echo form_label(lang('users input address2'), 'address2', array('class' => 'block')); ?>
      </div>
      <div class="col-md-7 col-lg-9">
        <?php echo form_input(array('name' => 'address2', 'value' => set_value('address2', (isset($user['address2']) ? $user['address2'] : '')), 'class' => 'form-control')); ?>
      </div>
    </div>

    <div class="form-group row px-5">
      <div class="col-md-5 col-lg-2">
        <?php echo form_label(lang('users input country'), 'country', array('class' => 'block')); ?>
      </div>
      <div class="col-md-7 col-lg-9">
        <?php echo form_dropdown('country', $countries, $user['country'], 'id="country" required="required" class="select2_single form-control"'); ?>
      </div>
    </div>

    <!--------DEPARTMENT---------->
    <div class="form-group row px-5">
      <div class="col-md-5 col-lg-2">
        <?php echo form_label(lang('users input department') . ' *', 'department', array('class' => 'block')); ?>
      </div>
      <div class="col-md-7 col-lg-9">
        <?php echo form_input(array('name' => 'department','required'=>'required', 'value' => set_value('department', (isset($user['department']) ? $user['department'] : '')), 'class' => 'form-control')); ?>
      </div>
    </div>

    <!--------CITY---------->
    <div class="form-group row px-5">
      <div class="col-md-5 col-lg-2">
        <?php echo form_label(lang('users input city') . ' *', 'city', array('class' => 'block')); ?>
      </div>
      <div class="col-md-7 col-lg-9">
        <?php echo form_input(array('name' => 'city','required'=>'required', 'value' => set_value('city', (isset($user['city']) ? $user['city'] : '')), 'class' => 'form-control')); ?>
      </div>
    </div>


    <!--------POSTAL CODE---------->
    <div class="form-group row px-5">
      <div class="col-md-5 col-lg-2">
        <?php echo form_label(lang('users input postal_code') . ' *', 'postal_code', array('class' => 'block')); ?>
      </div>
      <div class="col-md-7 col-lg-9">
        <?php echo form_input(array('name' => 'postal_code','required'=>'required', 'value' => set_value('postal_code', (isset($user['postal_code']) ? $user['postal_code'] : '')), 'class' => 'form-control')); ?>
      </div>
    </div>
    <!---------- BANK COUNT----->
    <div class="form-group row px-5" style="display:none">
      <div class="col-md-5 col-lg-2">
        <?php echo form_label(lang('users input bank_account') . ' *', 'bank_account', array('class' => 'block')); ?>
      </div>
      <div class="col-md-7 col-lg-9" style="padding: 1em 0.7em 0 0.7em;">
        <?php echo form_input(array('name' => 'bank_account', 'value' => set_value('bank_account', (isset($user['bank_account']) ? $user['bank_account'] : '10000001')), 'class' => 'form-control')); ?>
      </div>
    </div>

    <div class="form-group row px-5">
      <div class="col-md-5 col-lg-2">
        <?php echo form_label(lang('users input language'), 'language', array('class' => 'block')); ?>
      </div>
      <div class="col-md-7 col-lg-9">
        <?php echo form_dropdown('language', $this->languages, $lang, 'id="language" required="required" class="form-control"'); ?>
      </div>
    </div>


    <div class="form-group row px-5">
      <div class="col-md-5 col-lg-2">
        <?php echo form_label(lang('users input password'), 'password', array('class' => 'block')); ?>
      </div>
      <div class="col-md-7 col-lg-9">
        <?php echo form_password(array('name' => 'password', 'value' => '', 'id'=>'password', 'class' => 'form-control', 'autocomplete' => 'off')); ?>
      </div>
    </div>

    <div class="form-group row px-5">
      <div class="col-md-5 col-lg-2">
        <?php echo form_label(lang('users input password_repeat'), 'password_repeat', array('class' => 'block')); ?>
      </div>
      <div class="col-md-7 col-lg-9">
        <?php echo form_password(array('name' => 'password_repeat', 'data-parsley-trigger'=> 'change' , 'data-parsley-equalto'=>'#password', 'value' => '', 'class' => 'form-control', 'autocomplete' => 'off')); ?>
      </div>
    </div>



    <div class="form-group row px-5">
      <div class="col-md-5 col-lg-2">
        <?php echo form_label(lang('users input security_pin'), 'password', array('class' => 'block')); ?>
      </div>
      <div class="col-md-7 col-lg-9">
        <?php echo form_password(array('name' => 'security_pin', 'maxlength' => "4",'value' => '', 'id'=>'security_pin', 'class' => 'form-control', 'autocomplete' => 'off')); ?>
      </div>

      <div class="col-md-12"><?= lang("information") ?></div>
    </div>

    <div class="form-group row px-5">
      <div class="col-md-5 col-lg-2">
        <?php echo form_label(lang('users input security_pin_repeat'), 'password_repeat', array('class' => 'block')); ?>
      </div>
      <div class="col-md-7 col-lg-9">
        <?php echo form_password(array('name' => 'security_pin_repeat', 'maxlength' => "4",'data-parsley-trigger'=> 'change' , 'data-parsley-equalto'=>'#security_pin', 'value' => '', 'class' => 'form-control', 'autocomplete' => 'off')); ?>
      </div>
    </div>

    <div class="form-group row px-5">
      <div class="col-md-5 col-lg-2">
        <?php echo form_label(lang('users input image') . ' *' , 'image', array('class' => 'block')); ?>
      </div>
      <div class="col-md-7 col-lg-9">
        <?php echo form_upload(array('name' => 'image', 'id'=>'image', 'class' => 'form-control')); ?>
      </div>
    </div>

    

    <div>
      <hr />
    </div>

    <div class="form-group row text-center">
      <div class="col-md-12 col-md-offset-1 col-xs-12">
        <a class="btn btn-primary request-token" href="javascript:void(0)"><?php echo lang('request-token'); ?></a>
        <a class="btn btn-primary" href="<?php echo $cancel_url; ?>"><?php echo lang('core button cancel'); ?></a>
        <button type="submit" name="submit" class="btn btn-success">
          <span class="fa fa-check-square-o"></span> <?php echo lang('core button save'); ?>
        </button>
      </div>
    </div>

    <?php echo form_close(); ?>
  </div>
  <!-- end of card-block -->
</div>