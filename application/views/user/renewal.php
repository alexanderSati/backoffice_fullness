<?php defined('BASEPATH') OR exit('No direct script access allowed');
$lang = (isset($user['language']) ? $user['language'] : $this->config->item('language')); ?>

<div id="token-message"></div>
<div class="row">
    <div class="col-md-12 col-sm-12 col-xs-12">
        <div class="x_panel">
            <div class="x_title">
                <h2><?= $page_title ?></h2>
                <div class="clearfix"></div>
            </div>
            <div class="x_content">
                <?php echo form_open_multipart('', array('class' => 'form-horizontal form-label-left', 'data-parsley-validate' => '','role' => 'form')); ?>

     
                    <div class="row">

                        <div class="col-md-6">

                            <?php foreach ($plans as $plan) { ?>
                                <!-- price element -->
                                <div class="col-md-4 col-sm-6 col-xs-12 element-pricing <?= ($plan['id'] == 1 ) ? "active" : "" ; ?>">
                                    <div class="pricing">
                                        <div class="title">
                                            <h2><?= $plan["{$lang}_name"] ?></h2>
                                            <h2 style="font-size: 28px">S/<?= $plan["base"] ?></h2>
                                        </div>
                                        <div class="x_content">
                                            <div class="">
                                                <div class="pricing_features">
                                                    <ul class="list-unstyled text-left">
                                                        <li><?= $plan["{$lang}_features"] ?></li>
                                                    </ul>
                                                </div>
                                            </div>
                                            <div class="pricing_footer">
                                                <?php echo lang('users input check'); ?>
                                                <div class="radio hide">
                                                    <label>
                                                        <input type="radio" id="plan_<?= $plan['id'] ?>" class="flat" <?= ($plan['id'] == 1 ) ? "checked" : "" ; ?> class="form-control col-md-7 col-xs-12" value="<?= $plan['id'] ?>" name="plan">
                                                    </label>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <!-- price element -->
                            <?php } ?>
                        </div>

                    </div>

                <div class="ln_solid"></div>

                <div class="form-group">
                    <div class="col-md-6 col-xs-12">
                        <button type="submit" name="submit" class="btn btn-primary"><span class="glyphicon glyphicon-save"></span> <?php echo lang('renew'); ?></button>
                    </div>
                </div>

                <?php echo form_close(); ?>
            </div>
        </div>
    </div>
</div>

