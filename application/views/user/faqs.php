<?php defined('BASEPATH') OR exit('No direct script access allowed'); ?>

<div class="row">
    <div class="col-md-12 col-sm-12 col-xs-12">
        <div class="x_panel">
            <div class="x_title">
                <h2><?= $page_title ?></h2>
                <div class="clearfix"></div>
            </div>
            <div class="x_content">
                <!-- start accordion -->
                <div class="accordion" id="accordion1" role="tablist" aria-multiselectable="true">
                    <?php foreach ($faqs as $key => $faq) {?>
                        <div class="panel">
                            <a class="panel-heading <?= ($key != 0) ? 'collapsed' : '' ; ?>" role="tab" id="heading<?= $faq['id'] ?>" data-toggle="collapse" data-parent="#accordion1" href="#collapse<?= $faq['id'] ?>" aria-expanded="true" aria-controls="collapse">
                                <h4 class="panel-title"><?= $faq["{$lang}_question"] ?></h4>
                            </a>
                            <div id="collapse<?= $faq['id'] ?>" class="panel-collapse collapse <?= ($key == 0) ? 'in' : '' ; ?>" role="tabpanel" aria-labelledby="heading<?= $faq['id'] ?>">
                                <div class="panel-body">
                                    <?= $faq["{$lang}_answer"] ?>
                                </div>
                            </div>
                        </div>
                    <?php } ?>
                </div>
                <!-- end of accordion -->
            </div>
        </div>
    </div>
</div>

