<?php defined('BASEPATH') OR exit('No direct script access allowed');
$lang = (isset($user['language']) ? $user['language'] : $this->config->item('language')); ?>

<!-- [ page content ] start -->
<div class="card px-3">
  <div class="card-block">

    <div class="row">
      <div class="col-md-12 col-sm-12 col-xs-12">
        <div class="card card-border-primary">
          <div class="card-header">
            <h4><?= lang("account-plan") ?></h4>
          </div>
          <div class="card-block">
            <h3 class="text-center"><i class="fa fa-briefcase" aria-hidden="true"></i>
              <?= $current_plan["{$lang}_name"] ?></h3>
            <div>
              <hr />
            </div>
            <div class="text-center">
              <ul class="list-inline">
                <li class="list-inline-item">
                  <h3>S/ <?= $subscription['base'] ?></h3>
                  <span><?= lang("users col amount") ?></span>
                </li>
                <li class="list-inline-item m-l-50 p-l-50">
                  <h3><?= sprintf("%.0f",$current_plan['discount']) ?>%</h3>
                  <span><?= lang("lbl_discount") ?></span>
                </li>
                <li class="list-inline-item m-l-50 p-l-50">
                  <h3>S/ <?= $current_plan['commission'] ?></h3>
                  <span><?= lang("lbl_commision") ?></span>
                </li>
              </ul>
              <div>
                <hr />
              </div>
              <p class="text-center m-t-20">
                <?= $this->lang->line('active_since') ?> <?= $subscription['start_date'] ?> :
                <?= $this->lang->line('days_left') ?> <?= $subscription['period_days_left'] ?>
              </p>
            </div>
          </div>
        </div>
      </div>
    </div>

    <div class="row">
      <div class="col-md-12 col-sm-12 col-xs-12">
        <div class="card card-border-primary">
          <div class="card-header">
            <h4><?= $page_title ?></h4>
          </div>
          <div class="card-block">
            <?php echo form_open_multipart('', array('class' => 'form-horizontal form-label-left', 'data-parsley-validate' => '','role' => 'form', "id"=> "form_upgrade")); ?>

            <div class="row">

              <?php foreach ($plans as $plan): ?>
              <?php if( $plan["base"] > 0 ): ?>
              <!-- price element -->
              <div class="col-md-4 col-sm-6 col-xs-12 element-pricing" onclick="stylePlan(<?= $plan['id'] ?>)"
                id="<?= $plan['id'] ?>">
                <div class="card ">
                  <div class="p-15 text-center bg-c-blue text-white">
                    <h5><?= $plan["{$lang}_name"] ?></h5>
                    <h3 class="f-26">S/ <?= $plan["base"] ?></h3>
                  </div>
                  <div class="card-block">
                    <ul class="list-unstyled text-left f-16 f-w-600">
                      <li class="mb-4"><?= $plan["{$lang}_features"] ?></li>
                    </ul>
                    <div class="pricing_footer p-t-15">
                      <?php echo lang('users input check'); ?>
                      <div class="radio hide">
                        <label>
                          <input type="radio" id="plan_<?= $plan['id'] ?>" class="flat"
                            <?= ($plan['id'] == $current_plan['id'] ) ? "checked" : "" ; ?> class="form-control"
                            value="<?= $plan['id'] ?>" name="plan" style="position: absolute; opacity: 0;">
                        </label>
                      </div>
                    </div>
                  </div>
                </div>
              </div>
              <!-- price element -->
              <?php endif;?>
              <?php endforeach; ?>
              <?php if (count($plans) == 0) { ?>
              <div class="bs-example" data-example-id="simple-jumbotron" style="margin:auto;">
                <div class="jumbotron text-center">
                  <h2><?= lang('users title maximum_plan')?></h2>
                  <p class="f-18 m-t-40"><?=lang('users msg maximum_plan')?></p>
                </div>
              </div>
              <?php } ?>
            </div>

            <div>
              <hr />
            </div>

            <div class="form-group">
              <?php if( false): ?>
              <div class="form-group">
                <label for="exampleInputEmail1">Saldo de billetera (Disponible
                  S/ <?= number_format($saldoDisponible,2); ?>)</label>
                <input type="text" class="form-control" name="pago_wallet" id="" placeholder="monto">
              </div>
              <?php endif; ?>

              <?php if(count($plans) > 0) {?>
              <div class="form-group">
                <div class="col-md-12 col-xs-12 text-center">
                  <button type="button" name="submit" class="btn btn-lg btn-info" data-toggle="modal"
                    data-target="#confirm_upgrade">
                    <i class="fa fa-refresh" aria-hidden="true"></i>
                    <?php echo lang('upgrade_button'); ?></button>
                </div>
              </div>
              <?php } ?>
            </div>

            <?php echo form_close(); ?>
          </div>
          <!-- end of card-block -->
        </div>
      </div>
    </div>

  </div>
  <!-- end of card-block -->
</div>
<!-- [ page content ] end -->

<div class="modal fade" id="confirm_upgrade" style="display: none;">
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title"><?= $this->lang->line('modal_title') ?></h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">×</span></button>
      </div>
      <div class="modal-body">
        <p><?= $this->lang->line('question_confirm') ?></p>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-default pull-left" data-dismiss="modal"><?= lang('users button cancel')?></button>
        <button type="submit" form="form_upgrade" class="btn btn-primary"><?= lang('users button confirm')?></button>
        <!--<button type="submit" class="btn btn-primary" onclick="select_plan()"><?= lang('users button confirm')?></button>-->
      </div>
    </div>
    <!-- /.modal-content -->
  </div>
  <!-- /.modal-dialog -->
</div>
<script>
/**
  Metodo para cambiar el estilo cuando se seleccione un plan ademas de checkear el radio input
 */

const stylePlan = (parm) => {
  let id = document.getElementById(parm);
  let plan = document.getElementById(`plan_${parm}`);

  for (let key in <?= json_encode($plans) ?>) {

    if (<?= json_encode($plans) ?>[key]['id'] != parm) {
      let otherIds = document.getElementById(`${<?= json_encode($plans) ?>[key]['id']}`);

      $(otherIds).removeClass("active");
    } else {
      id.classList.add('active');
      plan.checked = true;
    }
  }
}
$(document).ready(function(){
  $('#form_upgrade').submit(function(e){
    var flag_selected_radio = false
    $('#form_upgrade input[type=radio]').each(function(){
      if($(this).is(':checked')){
        flag_selected_radio = true;
      }
    });
    if (flag_selected_radio === false){
      swal({
        title: `<?= lang('users error plan_not_select') ?>`,
        confirmButtonText: `<?= lang('users button confirm') ?>`,
        type: 'error',
      });

      e.preventDefault();
    }
  });
});


</script>