<?php defined('BASEPATH') OR exit('No direct script access allowed');

$lang = (isset($user['language']) ? $user['language'] : $this->config->item('language')); 

$ranks = array(

    0=>'entrepreneur.png',

    1 => 'rango1.png',

    2 => 'rango2.png',

    3 => 'rango3.png',

    4 => 'rango4.png',

    5 => 'rango5.png',

    6 => 'rango6.png',

    7 => 'rango7.png',

    8 => 'rango8.png',

    9 => 'rango9.png',

    10 => 'rango10.png',

    11 => 'rango11.png'

);

?>



<div class="card px-3">

  <div class="card-block">



    <div class="row">

      <!-- Invoice list card start -->

      <div class="col-sm-12 col-md-12 col-xs-12">

        <div class="card card-border-primary">

          <div class="card-header">

            <h5><?= lang("account-plan") ?></h5>

          </div>

          <div class="card-block">

            <div class="row">

              <div class="col-sm-12">

                <h4 class="text-center"><i class="fa fa-briefcase" aria-hidden="true"></i>

                  <?= $current_plan["{$lang}_name"] ?></h4>

              </div>

              <div class="col-sm-12 text-center">

                <hr />

                <ul class="list-inline ">

                  <li class="list-inline-item ">

                    <h3>S/ <?= $subscription['base'] ?> </h3>

                    <span class="ml-2"><?= lang("users col amount") ?></span>

                  </li>

                  <li class="list-inline-item m-l-50 p-l-50">

                    <h3 class="ml-3"><?= sprintf("%.0f",$current_plan['discount']) ?>%</h3>

                    <span><?= lang("lbl_discount") ?></span>

                  </li>

                  <li class="list-inline-item m-l-50 p-l-50">

                    <h3>S/ <?= $current_plan['commission'] ?></h3>

                    <span><?= lang("lbl_commision") ?></span>

                  </li>

                </ul>

              </div>

            </div>

          </div>

          <div class="card-footer">

            <?php if( $subscription['base'] > 0  ): ?>

            <div class="task-board m-0 float-right">

              <a href="/user/upgrade" class="btn btn-info pull-right"><i class="fa fa-edit m-right-xs"></i>

                <?= $this->lang->line('upgrade_title') ?></a>

            </div>

            <?php endif; ?>

          </div>

          <!-- end of card-footer -->

        </div>

      </div>

      <!-- Invoice list card end -->



      <!-- List rangue card start -->

      <div class="col-sm-12 col-md-12 col-xs-12">

        <div class="card card-border-primary">

          <div class="card-header">

            <h5><?= lang("account-rank") ?></h5>

          </div>

          <div class="card-block">

            <div class="row" style="float: none;">

              <div class="col-md-6 col-sm-6 col-xs-12 text-center">

                <p style="font-size: 20px;"><?= lang('current-rank') ?></p>



                

                <?php if(!empty(_trader_info($this->user['id']))) { ?>

                <img style="border-radius: 1em; height: 230px;" src="<?= base_url() . "uploads/rangos/" . $ranks[$rank["id"]] ?>"

                class="img-fluid" />

                <?php } ?>

              </div>

              <div class="col-md-6 col-sm-6 col-xs-12">

                <p style="font-size: 20px;"><?= lang('rank-list') ?></p>

                <ul>

                 <li><i class="fa fa-shirtsinbulk" aria-hidden="true" style="color: grey;"></i> Entrepeneur (0 - 1999

                    <?= lang('points')?>)</li>

                  <li><i class="fa fa-shirtsinbulk" aria-hidden="true" style="color: #1479b7;"></i> Assistant (2000 - 4999

                    <?= lang('points')?>)</li>

                  <li><i class="fa fa-shirtsinbulk" aria-hidden="true" style="color: green;"></i> Executive (5000 -

                    14999 <?= lang('points')?>)</li>

                  <li><i class="fa fa-shirtsinbulk" aria-hidden="true" style="color: #ffeb3b;"></i> Senior (15000 -

                    29999 <?= lang('points')?>)</li>

                  <li><i class="fa fa-shirtsinbulk" aria-hidden="true" style="color: red;"></i> Ruby (30000 - 59999

                    <?= lang('points')?>)</li>

                  <li><i class="fa fa-shirtsinbulk" aria-hidden="true" style="color: #00FFFF"></i> Diamond (60000 -

                    119999 <?= lang('points')?>)</li>

                  <li><i class="fa fa-shirtsinbulk" aria-hidden="true" style="color: #00FFFF"></i> Double Diamond

                    (120000 - 299999 <?= lang('points')?>)</li>

                  <li><i class="fa fa-shirtsinbulk" aria-hidden="true" style="color: #00FFFF"></i> Triple Diamond

                    (300000 - 499999 <?= lang('points')?>)</li>

                  <li><i class="fa fa-shirtsinbulk" aria-hidden="true" style="color: red;"></i> Red Diamond (500000 -

                    999999 <?= lang('points')?>)</li>

                  <li><i class="fa fa-shirtsinbulk" aria-hidden="true" style="color: grey;"></i> Black Diamond (1000000

                    - 1999999 <?= lang('points')?>)</li>

                  <li><i class="fa fa-shirtsinbulk" aria-hidden="true" style="color: grey;"></i> Grand Diamond (2000000

                    - 4999999 <?= lang('points')?>)</li>

                  <li><i class="fa fa-shirtsinbulk" aria-hidden="true" style="color: grey;"></i> Grand Black Diamond

                    (5000000 <?= lang('points')?>)</li>

                </ul>

              </div>

            </div>

            <!-- end of row line 95 -->

          </div>

        </div>

      </div>

      <!-- List rangue card end -->

    </div>



    <?php if ($showChart){ ?>

    <div class="row">

      <div class="col-md-12 col-sm-12 col-xs-12">

        <div class="card">

          <div class="card-header">

            <h4><?= lang('daily-bonus') ?></h4>

            <div class="clearfix"></div>

          </div>

          <div class="card-block">

            <div class="col-md-12 col-sm-12 col-xs-12">

              <?php if (empty($chart)) { ?>

              <div class="text-center"><?= lang('no-data') ?></div>

              <?php } ?>

              <div id="graph_line" style="width:100%; height:300px;"></div>

            </div>

          </div>

        </div>

      </div>

    </div>

    <?php } ?>



    <!-- Default ordering table start -->

    <div class="card">

      <div class="card-header">

        <h5><?= lang('my-referrals') ?></h5>

      </div>

      <div class="card-block">

        <div class="dt-responsive table-responsive">

          <table id="order-table" class="table table-striped table-bordered nowrap">

            <thead>

              <tr>

                <th></th>

                <th><?= lang('users col fullname') ?></th>

                <th><?= lang('users col username') ?></th>

                <th><?= lang('account-rank') ?></th>

                <th><?= lang('account-plan') ?></th>

                <th><?= lang('date') ?></th>

                <th><?= lang('side') ?></th>

              </tr>

            </thead>

            <tbody>

              <?php foreach ($referrals as $referral) { ?>

              <tr>

                <td>

                  <center>

                    <img class="img-circle"

                      src="<?= base_url(); ?>uploads/<?= ($referral['image'] != "") ? $referral['image'] : 'user-anonymous-disabled.gif' ?>"

                      style="width: 30px;height: 30px;">

                  </center>

                </td>

                <td><?= $referral['first_name'] ?> <?= $referral['last_name'] ?></td>

                <td><?= $referral['username'] ?></td>

                <td><?= $referral['rank'] ?></td>

                <td><?= $referral["{$lang}_name"]  ?></td>

                <td><?= $referral['start_date'] ?></td>

                <td>

                  <?php

                  if ($lang == 'english') {

                      echo ($referral['referral_side'] == 'left') ? "<span class='badge bg-blue'> Left </span>" : "<span class='badge bg-green'> Right </span>";

                  } else if ($lang == 'spanish') {

                    echo ($referral['referral_side'] == 'left') ? "<span class='badge bg-blue'> Izquierda </span>" : "<span class='badge bg-green'> Derecha </span>";

                  }

                  ?>

                </td>

              </tr>

              <?php } ?>

            </tbody>

          </table>

        </div>

      </div>

    </div>

    <!-- Default ordering table end -->



  </div>

  <!-- end of card-block -->

</div>





<script>

var chart = <?= json_encode($chart) ?>;

</script>