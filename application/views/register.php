<?php defined('BASEPATH') OR exit('No direct script access allowed');
$lang = (isset($user['language']) ? $user['language'] : $this->config->item('language'));
?>

<div themebg-pattern="theme1">
  <!-- Pre-loader start -->
  <div class="theme-loader">
    <div class="loader-track">
      <div class="preloader-wrapper">
        <div class="spinner-layer spinner-blue">
          <div class="circle-clipper left">
            <div class="circle"></div>
          </div>
          <div class="gap-patch">
            <div class="circle"></div>
          </div>
          <div class="circle-clipper right">
            <div class="circle"></div>
          </div>
        </div>
        <div class="spinner-layer spinner-red">
          <div class="circle-clipper left">
            <div class="circle"></div>
          </div>
          <div class="gap-patch">
            <div class="circle"></div>
          </div>
          <div class="circle-clipper right">
            <div class="circle"></div>
          </div>
        </div>

        <div class="spinner-layer spinner-yellow">
          <div class="circle-clipper left">
            <div class="circle"></div>
          </div>
          <div class="gap-patch">
            <div class="circle"></div>
          </div>
          <div class="circle-clipper right">
            <div class="circle"></div>
          </div>
        </div>

        <div class="spinner-layer spinner-green">
          <div class="circle-clipper left">
            <div class="circle"></div>
          </div>
          <div class="gap-patch">
            <div class="circle"></div>
          </div>
          <div class="circle-clipper right">
            <div class="circle"></div>
          </div>
        </div>
      </div>
    </div>
  </div>
  <!-- Pre-loader end -->

  <section class="login-block">

    <!-- Container-fluid starts -->
    <div class="container-fluid">
      <div class="row">
        <div class="col-sm-12">

          <?php echo form_open_multipart($this->uri->uri_string(), array('class' => 'md-float-material form-material', 'data-parsley-validate' => '','role' => 'form')); ?>
          <div class="text-center">
            <img width="200" src="/themes/<?= $this->settings->theme ?>/images/logo.png" alt="logo">
          </div>
          <div class="auth-box card">
            <div class="card-block">
              <!-- row text-left start -->
              <div class="row text-left">
                <div class="col-12">
                  <div class="checkbox-fade">
                    <h2><?= $page_title ?></h2>
                  </div>
                  <div class="text-right float-right">
                    <span class="dropdown">
                      <label class="control-label"><?php echo lang('users title change_language')?></label>
                      <button id="session-language" type="button" data-toggle="dropdown" aria-haspopup="true"
                        aria-expanded="false" class="btn btn-default">
                        <i class="fa fa-language"></i>
                      </button>
                      <ul id="session-language-dropdown" class="dropdown-menu p-3" role="menu"
                        aria-labelledby="session-language">
                        <?php foreach ($this->languages as $key=>$name) : ?>
                        <li>
                          <a href="#" rel="<?php echo $key; ?>">
                            <?php if ($key == $this->session->language) : ?>
                            <i class="fa fa-check selected-session-language"></i>
                            <?php endif; ?>
                            <?php echo $name; ?>
                          </a>
                        </li>
                        <?php endforeach; ?>
                      </ul>
                    </span>
                  </div>
                </div>
              </div>
              <!-- end of row text-left -->

              <?php // System messages ?>
              <?php if ($this->session->flashdata('message')) : ?>
              <div class="alert alert-success alert-dismissable">
                <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
                <?php echo $this->session->flashdata('message'); ?>
              </div>
              <?php elseif ($this->session->flashdata('error')) : ?>
              <div class="alert alert-danger alert-dismissable">
                <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
                <?php echo $this->session->flashdata('error'); ?>
              </div>
              <?php elseif (validation_errors()) : ?>
              <div class="alert alert-danger alert-dismissable">
                <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
                <?php echo validation_errors(); ?>
              </div>
              <?php elseif ($this->error) : ?>
              <div class="alert alert-danger alert-dismissable">
                <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
                <?php echo $this->error; ?>
              </div>
              <?php endif; ?>

              <div class="form-group form-primary form-static-label m-t-25">
                <?php echo form_input(array('name' => 'username','required'=>'required','data-parsley-pattern'=>"^[a-zA-Z0-9,@.+_-]+$", 'value' => set_value('username', (isset($user['username']) ? $user['username'] : '')), 'class' => 'form-control')); ?>
                <span class="form-bar"></span>
                <?php echo form_label(lang('users input username') . ' *', 'username', array('class' => 'float-label')); ?>
              </div>

              <!----DNI--->
              <div class="form-group form-primary">
                <?php echo form_input(array('name' => 'dni','required'=>'required', 'value' => set_value('dni', (isset($user['dni']) ? $user['dni'] : '')), 'class' => 'form-control')); ?>
                <span class="form-bar"></span>
                <?php echo form_label(lang('users input dni') . ' *', 'dni', array('class' => 'float-label')); ?>
              </div>

              <div class="form-group form-primary">
                <?php echo form_input(array('name' => 'first_name','required'=>'required', 'value' => set_value('first_name', (isset($user['first_name']) ? $user['first_name'] : '')), 'class' => 'form-control')); ?>
                <span class="form-bar"></span>
                <?php echo form_label(lang('users input first_name') . ' *', 'first_name', array('class' => 'float-label')); ?>
              </div>
              <div class="form-group form-primary">
                <?php echo form_input(array('name' => 'last_name','required'=>'required', 'value' => set_value('last_name', (isset($user['last_name']) ? $user['last_name'] : '')), 'class' => 'form-control')); ?>
                <span class="form-bar"></span>
                <?php echo form_label(lang('users input last_name') . ' *', 'last_name', array('class' => 'float-label')); ?>
              </div>
              <div class="form-group form-primary">
                <?php echo form_input(array('name' => 'email','required'=>'required', 'value' => set_value('email', (isset($user['email']) ? $user['email'] : '')), 'class' => 'form-control', 'type' => 'email')); ?>
                <span class="form-bar"></span>
                <?php echo form_label(lang('users input email') . ' *', 'email', array('class' => 'float-label')); ?>
              </div>

              <!--------BIRTHDATE-------->
              <div class="form-group form-primary form-static-label">
                <?php echo form_input(array('name' => 'birthdate','required'=>'required', 'value' => set_value('birthdate', (isset($user['birthdate']) ? $user['birthdate'] : '')), 'class' => 'form-control', 'type' => 'date')); ?>
                <span class="form-bar"></span>
                <?php echo form_label(lang('users input birthdate') . ' *', 'birthdate', array('class' => 'float-label')); ?>
              </div>

              <!--------PHONE------------>
              <div class="form-group form-primary">
                <?php echo form_input(array('name' => 'phone','required'=>'required', 'value' => set_value('phone', (isset($user['phone']) ? $user['phone'] : '')), 'class' => 'form-control', 'type' => 'text')); ?>
                <span class="form-bar"></span>
                <?php echo form_label(lang('users input phone') . ' *', 'phone', array('class' => 'float-label')); ?>
              </div>
              <!--------ADDRESS---------->
              <div class="form-group form-primary">
                <?php echo form_input(array('name' => 'address','required'=>'required', 'value' => set_value('address', (isset($user['address']) ? $user['address'] : '')), 'class' => 'form-control', 'type' => 'text')); ?>
                <span class="form-bar"></span>
                <?php echo form_label(lang('users input address') . ' *', 'address', array('class' => 'float-label')); ?>
              </div>

              <!--------ADDRESS 2---------->
              <div class="form-group form-primary">
                <?php echo form_input(array('name' => 'address2','value' => set_value('address2', (isset($user['address2']) ? $user['address2'] : '')), 'class' => 'form-control', 'type' => 'text')); ?>
                <span class="form-bar"></span>
                <?php echo form_label(lang('users input address2'), 'address2', array('class' => 'float-label')); ?>
              </div>

              <div class="form-group form-primary form-static-label">
                <?php echo form_dropdown('country', $countries, false, 'id="country" required="required" class="select2_single form-control"'); ?>
                <span class="form-bar"></span>
                <?php echo form_label(lang('users input country'), 'country', array('class' => 'float-label')); ?>
              </div>

              <!--------DEPARTMENT --------------->
              <div class="form-group form-primary">
                <?php echo form_input(array('name' => 'department','required'=>'required' ,'value' => set_value('department', (isset($user['department']) ? $user['department'] : '')), 'class' => 'form-control', 'type' => 'text')); ?>
                <span class="form-bar"></span>
                <?php echo form_label(lang('users input department'). ' *', 'department', array('class' => 'float-label')); ?>
              </div>

              <!--------CITY--------------->
              <div class="form-group form-primary">
                <?php echo form_input(array('name' => 'city', 'required'=>'required', 'value' => set_value('city', (isset($user['city']) ? $user['city'] : '')), 'class' => 'form-control', 'type' => 'text')); ?>
                <span class="form-bar"></span>
                <?php echo form_label(lang('users input city'). ' *', 'city', array('class' => 'float-label')); ?>
              </div>

              <!--------POSTAL CODE------------->
              <div class="form-group form-primary">
                <?php echo form_input(array('name' => 'postal_code','required'=>'required','value' => set_value('postal_code', (isset($user['postal_code']) ? $user['postal_code'] : '')), 'class' => 'form-control', 'type' => 'text')); ?>
                <span class="form-bar"></span>
                <?php echo form_label(lang('users input postal_code'). ' *', 'postal_code', array('class' => 'float-label')); ?>
              </div>

              <!-------- BANK ACCOUNT ---------->
              <div class="form-group form-primary" style="display:none">
                <?php echo form_input(array('name' => 'bank_account','value' => set_value('bank_account', (isset($user['bank_account']) ? $user['bank_account'] : '10000001')), 'class' => 'form-control', 'type' => 'text')); ?>
                <span class="form-bar"></span>
                <?php echo form_label(lang('users input bank_account'). ' *', 'bank_account', array('class' => 'float-label')); ?>
              </div>

              <div class="form-group form-primary form-static-label">
                <?php echo form_dropdown('language', $this->languages, $lang, 'id="language" required="required" class="form-control"'); ?>
                <span class="form-bar"></span>
                <?php echo form_label(lang('users input language'), 'language', array('class' => 'float-label')); ?>
              </div>

              <div class="form-group form-primary">
                <?php echo form_password(array('name' => 'password', 'required'=>'required', 'value' => '', 'id'=>'password', 'class' => 'form-control', 'autocomplete' => 'off')); ?>
                <span class="form-bar"></span>
                <?php echo form_label(lang('users input password') . ' *' , 'password', array('class' => 'float-label')); ?>
              </div>
              <div class="form-group form-primary">
                <?php echo form_password(array('name' => 'password_repeat', 'data-parsley-trigger'=> 'change' , 'data-parsley-equalto'=>'#password', 'required'=>'required', 'value' => '', 'class' => 'form-control', 'autocomplete' => 'off')); ?>
                <span class="form-bar"></span>
                <?php echo form_label(lang('users input password_repeat'). ' *', 'password_repeat', array('class' => 'float-label')); ?>

                <div class="form-group form-primary m-t-20" style="display:none;">
                  <?php echo form_password(array('name' => 'security_pin', 'maxlength' => "4",'value' => '', 'id'=>'security_pin', 'class' => 'form-control', 'autocomplete' => 'off')); ?>
                  <span class="form-bar"></span>
                  <?php echo form_label(lang('users input security_pin'). ' *', 'password', array('class' => 'float-label')); ?>
                </div>
                <div class="form-group form-primary" style="display:none;">
                  <?php echo form_password(array('name' => 'security_pin_repeat', 'maxlength' => "4",'data-parsley-trigger'=> 'change' , 'data-parsley-equalto'=>'#security_pin', 'value' => '', 'class' => 'form-control', 'autocomplete' => 'off')); ?>
                  <span class="form-bar"></span>
                  <?php echo form_label(lang('users input security_pin_repeat'). ' *', 'password_repeat', array('class' => 'float-label')); ?>
                </div>

                <?php if (!$password_required) : ?>
                <span class="help-block"><br /><?php echo lang('users help passwords'); ?></span>
                <?php endif; ?>
              </div>
              <div class="form-group form-primary form-static-label">
                <?php echo form_upload(array('name' => 'image', 'id'=>'image', 'class' => 'form-control')); ?>
                <?php echo form_label(lang('users input image') . ' *' , 'image', array('class' => 'float-label')); ?>
              </div>

              <!-- TERMINOS Y CONDICIONES -->
              <div class="col-md-12 text-center">
                <div class="checkbox-fade fade-in-primary">
                  <label>
                    <?php echo form_checkbox(array('name' => 'terms', 'id'=>'terms', 'class' => 'form-control flat','checked'=> FALSE,'value'=> 'off','required'=>'required')); ?>
                    <span class="cr"><i class="cr-icon icofont icofont-ui-check txt-info"></i></span>
                    <span class="text-inverse"><?= lang('users input terms'); ?></span>
                  </label>
                </div>
              </div>
              <!-- ./TERMINOS Y CONDICIONES -->

              <!-- PLAN -->
              <input type="hidden" value="9" name="plan" />
              <!-- ./PLAN -->

              <div><br /></div>

              <div class="form-group" style="">
                <div class="col-md-12 text-center">
                  <div class="g-recaptcha" data-sitekey="6Lcvaa0UAAAAAOcf3ZDNEQhrovpknWQb-q3ui_H9"></div>
                </div>
              </div>

              <div>
                <hr>
              </div>

              <div class="form-group">
                <div class="text-center mx-auto">
                  <a class="btn btn-danger cancel"
                    href="<?php echo $cancel_url; ?>"><?php echo lang('core button cancel'); ?></a>
                  <?php if ($this->session->userdata('logged_in')) : ?>
                  <button type="submit" name="submit" class="btn btn-danger">
                    <span class="fa fa-check-square-o"></span>
                    <?php echo lang('core button save'); ?>
                  </button>
                  <?php else : ?>
                  <button id="send" type="submit" name="submit" class="btn btn-primary send"><span
                      class="fa fa-check"></span> <?php echo lang('users button register'); ?></button>
                  <?php endif; ?>
                  <div><?= lang("verify-spam") ?></div>
                </div>
              </div>
              <div>
                <h4 class="text-center"><?php echo $this->settings->site_name; ?></h4>
              </div>
            </div>
            <!-- end of card-block -->
          </div>
          <!-- end of auth-box card -->
          <?php echo form_close(); ?>
          <!-- end of form -->
        </div>
        <!-- end of col-sm-12 -->
      </div>
      <!-- end of row -->
    </div>
    <!-- end of container-fluid -->
  </section>
  <!-- content -->

  <div class="footer">
    <p class="text-center m-b-0">Copyright &copy; 2019, All rights reserved.</p>
  </div>
</div>
<style>
.g-recaptcha>div {
  margin: auto;
  display: block
}
</style>