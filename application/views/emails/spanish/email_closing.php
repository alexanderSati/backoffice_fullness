<?php defined('BASEPATH') OR exit('No direct script access allowed');

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 *
 * @author Jesus Alejandro Pinto Montoya
 */

?>
<style type="text/css">
      @import url('https://fonts.googleapis.com/css?family=Open+Sans');

      * {
        margin: 0;
        padding: 0;
        font-size: 100%;
        font-family: "Open Sans", Helvetica, Arial, sans-serif;
        line-height: 1.65;
      }

      img {
        max-width: 100%;
        margin: 0 auto;
        display: block;
      }

      body,
      .body-wrap {
        width: 97% !important;
        margin: 0 auto;
        height: 100%;
        background: #efefef;
        -webkit-font-smoothing: antialiased;
        -webkit-text-size-adjust: none;
      }

      a {
        color: #3ab795;
        text-decoration: none;
      }

      .text-center {
        text-align: center;
      }

      .text-right {
        text-align: right;
      }

      .text-left {
        text-align: left;
      }

      .button a {
        display: inline-block;
        color: #ffffff;
        background: #3ab795;
        border: 2px solid #3ab795;
        padding: 9px 20px 10px;
        text-transform: uppercase;
        font-size: 12px;
        font-weight: normal;
      }

      .highlight {
        font-size: 22px;
        font-weight: bold;
      }

      h1,
      h2,
      h3,
      h4,
      h5,
      h6 {
        margin-bottom: 20px;
        line-height: 1.25;
      }

      h1 {
        font-size: 32px;
      }

      h2 {
        font-size: 28px;
      }

      h3 {
        font-size: 24px;
      }

      h4 {
        font-size: 20px;
      }

      h5 {
        font-size: 16px;
      }

      p,
      ul,
      ol {
        font-size: 14px;
        font-weight: normal;
        margin-bottom: 20px;
      }

      p.footnote {
        font-size: 10px;
        margin-top: 5px;
      }

      .container {
        display: block !important;
        clear: both !important;
        margin: 20px auto 0 !important;
        max-width: 580px !important;
      }

      .container table {
        width: 100% !important;
        border-collapse: collapse;
      }

      .container .preheader {
        font-size: 12px;
        padding: 5px 5px 5px 5px;
        color: #adadad;
        text-align: center;
      }

      .container .masthead {
        padding: 80px 0;
        background: #2a333b;
        color: white;
        background-image: url("https://s3.ca-central-1.amazonaws.com/hover-email/hover_logo_75x21.png");
        background-repeat: no-repeat;
        background-position: center 15px;
        border-radius: 10px 10px 0 0;
      }

      .container .masthead h1 {
        margin: 0 auto !important;
        max-width: 90%;
      }

      .container .content {
        background: white;
        padding: 20px 20px 0 20px;
      }

      .container .content.footer {
        background: none;
        padding-top: 0;
      }

      .container .content.footer p {
        margin-bottom: 0;
        color: #888;
        text-align: center;
        font-size: 12px;
      }

      .container .content.footer a {
        color: #888;
        text-decoration: none;
        font-weight: bold;
      }
    </style>
  </head>


  <body
    style="width:97% !important;margin-top:0;margin-bottom:0;margin-right:auto;margin-left:auto;height:100%;background-color:#efefef;background-image:none;background-repeat:repeat;background-position:top left;background-attachment:scroll;-webkit-font-smoothing:antialiased;-webkit-text-size-adjust:none;">

    <table class="body-wrap"
      style="width:97% !important;margin-top:0;margin-bottom:0;margin-right:auto;margin-left:auto;height:100%;background-color:#efefef;background-image:none;background-repeat:repeat;background-position:top left;background-attachment:scroll;-webkit-font-smoothing:antialiased;-webkit-text-size-adjust:none;">
      <tr>
        <td class="container"
          style="display:block !important;clear:both !important;margin-top:20px !important;margin-bottom:0 !important;margin-right:auto !important;margin-left:auto !important;max-width:580px !important;">


          <!-- Message start -->
          <table style="width:100% !important;border-collapse:collapse;">

            <tr>
              <td align="center" class="masthead"
                style="padding-top:80px;padding-bottom:80px;padding-right:0;padding-left:0;background-color:#163D5B;background-image:url('https://fullnessglobal.com/wp-content/uploads/2019/07/Sin-título-2.png');background-repeat:no-repeat;background-position:center 15px;background-attachment:scroll;color:white;border-radius:10px 10px 0 0;">
                <br>
                <h1
                  style="line-height:1.25;font-size:32px;margin-top:0 !important;margin-bottom:0 !important;margin-right:auto !important;margin-left:auto !important;max-width:90%;">
                  Sistema finalizó el cierre</h1>
              </td>
            </tr>
            <tr>
              <td class="content"
                style="background-color:white;background-image:none;background-repeat:repeat;background-position:top left;background-attachment:scroll;padding-top:20px;padding-bottom:0;padding-right:20px;padding-left:20px;border-radius: 0 0 10px 10px">


                <p style="font-size:20px;font-weight:normal;margin-bottom:20px;">ATENCION, se le informa que el sistema ya ha hecho el cierre mensual.</p>
                <img alt="Coinbase" width="80" src="https://www.coinbase.com/assets/app/succeed-green-dcb087e9c6e5265b4c49f75c9c2e1d08bc894bc54816d9a5a476611f631b2929.png"
                    />
                    <br>
                    <br>
            </td>
             
            </tr>
            
          </table>
          <!-- body end -->
        </td>
      </tr>
    </table>
  </body>