<?php defined('BASEPATH') OR exit('No direct script access allowed');

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Rank up
 *
 * @author Jesus Alejandro Pinto Montoya
 */
?>
    <style type="text/css">
        @media only screen {
          .container {
            padding: 10px;
          }
    
        }
    
        @media only screen {
          .content {
            padding: 10px;
          }
    
        }
    
        @media only screen and (max-width: 620px) {
          table[class=body] h1,
          table[class=body] h2,
          table[class=body] h3,
          table[class=body] h4 {
            font-weight: 600 !important;
          }
    
        }
    
        @media only screen and (max-width: 620px) {
          table[class=body] h1 {
            font-size: 22px !important;
          }
    
        }
    
        @media only screen and (max-width: 620px) {
          table[class=body] h2 {
            font-size: 18px !important;
          }
    
        }
    
        @media only screen and (max-width: 620px) {
          table[class=body] h3 {
            font-size: 16px !important;
          }
    
        }
    
        @media only screen and (max-width: 620px) {
          table[class=body] .content,
          table[class=body] .wrapper {
            padding: 10px !important;
          }
    
        }
    
        @media only screen and (max-width: 620px) {
          table[class=body] .container {
            padding: 0 !important;
            width: 100% !important;
          }
    
        }
    
        @media only screen and (max-width: 620px) {
          table[class=body] .btn table,
          table[class=body] .btn a {
            width: 100% !important;
          }
    
        }
    
        @media only screen and (max-width: 620px) {
          .header {
            margin-bottom: 10px !important;
            margin-top: 20px !important;
          }
    
        }
    
        @media only screen and (max-width: 620px) {
          table[class=body] .wrapper {
            padding: 40px 20px !important;
          }
    
        }
    
        @media only screen and (max-width: 620px) {
          .main.signup .panel:nth-of-type(2),
          .main.signup .panel:nth-of-type(3) {
            padding-bottom: 10px !important;
          }
    
        }
    
        @media only screen and (max-width: 620px) {
          .main.signup .panel:nth-of-type(1) img.intro {
            width: 120px !important;
          }
    
        }
    
        @media only screen and (max-width: 620px) {
          .no-media-query {
            display: none !important;
          }
    
        }
    
        @media only screen and (max-width: 620px) {
          .small-screen-block {
            width: 100% !important;
            height: auto !important;
            overflow: inherit !important;
            float: none !important;
            display: block !important;
            max-height: none !important;
            position: relative !important;
          }
    
        }
    
        @media only screen {
          .media-query-available--inline {
            width: auto !important;
            min-height: auto !important;
            overflow: auto !important;
            float: none !important;
            display: inline !important;
            max-height: auto !important;
          }
    
        }
    
        @media only screen {
          .media-query-not-vailable {
            display: none !important;
          }
    
        }
      </style>
    </head>
    
    <body style="margin: 0; font-family: 'Helvetica Neue', 'Helvetica', Helvetica, Arial, sans-serif; font-size: 14px; height: 100% !important; line-height: 1.6em; -webkit-font-smoothing: antialiased; padding: 0; -ms-text-size-adjust: 100%; -webkit-text-size-adjust: 100%; width: 100% !important; background-color: #f6f6f6;">
    
      <table class="body" style="box-sizing: border-box; background-color: white; border-collapse: separate !important; mso-table-lspace: 0pt; mso-table-rspace: 0pt; width: 100%;" width="100%" bgcolor="white">
        <tbody>
          <tr>
            <td style="box-sizing: border-box; font-family: 'Helvetica Neue', Helvetica, Arial, 'Lucida Grande', sans-serif; font-size: 14px; vertical-align: top;" valign="top"></td>
            <td class="container" style="box-sizing: border-box; font-family: 'Helvetica Neue', Helvetica, Arial, 'Lucida Grande', sans-serif; font-size: 14px; vertical-align: top; display: block; margin: 0 auto !important; max-width: 580px; padding: 10px 0; width: 580px;"
              width="580" valign="top">
              <div class="content" style="box-sizing: border-box; display: block; margin: 0 auto; max-width: 580px; padding: 10px 0;">
    
                
    
                <table class="main signup" style="box-sizing: border-box; background: #ffffff; border: none; border-radius: 3px; font-size: 15px; line-height: 24px; border-collapse: separate !important; mso-table-lspace: 0pt; mso-table-rspace: 0pt; width: 100%;" width="100%">
                  <tbody>
                    <tr>
                      <td style="box-sizing: border-box; font-family: 'Helvetica Neue', Helvetica, Arial, 'Lucida Grande', sans-serif; font-size: 14px; vertical-align: top;" valign="top">
                        <table class="panel panel-dark" style="box-sizing: border-box; border-radius: 8px; margin-top: 0; background-color: #15233b; color: white; font-size: 15px; padding: 20px 20px; mso-table-rspace: 0pt; width: 100%; border-collapse: separate !important; mso-table-lspace: 0pt;"
                          width="100%" bgcolor="#15233b">
                          <tbody>
                           <tr>
                            <td>
                                <div class="header" style="box-sizing: border-box; width: 100%;">
                                    <table style="box-sizing: border-box; border-collapse: separate !important; mso-table-lspace: 0pt; mso-table-rspace: 0pt; width: 100%;" width="100%">
                                        <tbody>
                                        <tr>
                                            <td style="box-sizing: border-box; font-family: 'Helvetica Neue', Helvetica, Arial, 'Lucida Grande', sans-serif; font-size: 14px; vertical-align: top;" valign="top">
                                                <img src="https://fullnessglobal.com/wp-content/uploads/2019/07/Sin-título-2.png" height="32" alt="" style="-ms-interpolation-mode: bicubic; max-width: 100%;">
                                            </td>
                                        </tr>
                                        </tbody>
                                    </table>
                                </div>
                            </td>
                           </tr>
                            
                            <tr>
                              <td class="align-center" style="box-sizing: border-box; font-family: 'Helvetica Neue', Helvetica, Arial, 'Lucida Grande', sans-serif; font-size: 14px; vertical-align: top; text-align: center;" valign="top" align="center">
                                <img class="intro" src="https://img.icons8.com/bubbles/2x/up.png" style="-ms-interpolation-mode: bicubic; max-width: 100%; width: 180px; margin-bottom: 30px;"
                                  width="200">    
                                <h1 style="font-family: 'Helvetica Neue', Helvetica, Arial, 'Lucida Grande', sans-serif; font-weight: 600; line-height: 32px; margin: auto; font-size: 24px; text-transform: none; max-width: 400px; color: #9bbba7; margin-bottom: 26px;">
                                  <?php echo $username ?><br> has subido al rango
                                    <strong>
                                    
                                    <?php
                                        switch ($rank) {
                                            case 1:
                                                echo 'Executive';
                                                break;
                                            case 2: 
                                                echo 'Senior';
                                                break;
                                            case 3:
                                                echo 'Ruby';
                                                break;
                                            case 4:
                                                echo 'Diamond';
                                                break;
                                            case 5: 
                                                echo 'Double Diamond';
                                                break;
                                            case 6:
                                                echo 'Triple Diamond';
                                                break;
                                            case 7:
                                                echo 'Red Diamond';
                                                break;
                                            case 8:  
                                                echo 'Black Diamond';
                                                break;
                                            case 9: 
                                                echo 'Gran Diamond';
                                                break;
                                            case 10:
                                                echo 'Grand Black Diamond';
                                                break; 

                                            default:
                                                # code...
                                                break;
                                        }
                                    ?>
                                    </strong>
                                </h1>
    
                                <p class="shrink-400" style="font-family: 'Helvetica Neue', Helvetica, Arial, 'Lucida Grande', sans-serif; font-weight: normal; margin: 0; max-width: 400px; margin-left: auto; margin-right: auto; font-size: 15px; color: white; margin-bottom: 0px;">

                                    Ha ganado el premio de
                                  <strong>
                                  <?php
                                    switch ($rank) {
                                        case 1:
                                            echo '500 Soles';
                                            break;
                                        case 2: 
                                            echo 'un Resort';
                                            break;
                                        case 3:
                                            echo 'un Crusero';
                                            break;
                                        case 4:
                                            echo 'un viaje a Cancun';
                                            break;
                                        case 5: 
                                            echo 'un viaje a Dubai';
                                            break;
                                        case 6:
                                            echo 'un Auto';
                                            break;
                                        case 7:
                                            echo 'un Mercedes';
                                            break;
                                        case 8:  
                                            echo 'un Range Rover';
                                            break;
                                        case 9: 
                                            echo 'un Maletin 1/2 millon';
                                            break;
                                        case 10:
                                            echo 'un Casa 1 Millon';
                                            break; 

                                        default:
                                            # code...
                                            break;
                                    }
                                  ?>
                                  </strong>
                                  <span class="media-query-not-vailable"></span>
                                </p>
                              </td>
                            </tr>
                          </tbody>
                        </table>

                      </td>
                    </tr>
    
                  </tbody>
                </table>
         
      
    
    </body>

