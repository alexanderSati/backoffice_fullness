<?php defined('BASEPATH') OR exit('No direct script access allowed');

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of token_transactions
 *
 * @author Juan Manuel
 */

?>
<p>
    <img width="260" src="<?= base_url() ?>/themes/gentella/images/logo.png"
         alt="Exchange">
</p>
<h1>Atencion, Un nuevo deposito de moneda a sido solicitado.</h1>
<hr>
<p>Se han anexado los datos de usuario y de la transaccion.</p>
<br>
<p><strong>Fecha:</strong>&nbsp;<?= $date ?></p>
<p><strong>Codigo:</strong>&nbsp;<?= $transaction ?></p>
<p><strong>Nombre usuario:</strong>&nbsp;<?= $name ?></p>
<p><strong>Login:</strong>&nbsp;<?= $username ?></p>
<p><strong>Email:</strong>&nbsp;<?= $email ?></p>
<p><strong>Descripción:</strong>&nbsp;<?= $description ?></p>
<p><strong>Cantidad a depositar:</strong>&nbsp;<?= $usd ?></p>
<p><strong>BTC:</strong>&nbsp;<?= $btc ?></p>
<p><strong>Moneda a depositar:</strong>&nbsp;<?= $wallet ?></p>
<p><strong>Estado:&nbsp;</strong>Pendiente</p>