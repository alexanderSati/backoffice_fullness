<?php defined('BASEPATH') OR exit('No direct script access allowed');

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of token_transactions
 *
 * @author Juan Manuel
 */

?>
<p>
    <img width="260" src="<?= base_url() ?>/themes/gentella/images/logo.png"
         alt="Exchange">
</p>
<h1>Solicitud de deposito.</h1>
<hr>
<p>Su solicitud ha sido procesada exitosamente.</p>
<br>
<p><strong>Fecha:</strong>&nbsp;<?= $date ?></p>
<p><strong>Codigo:</strong>&nbsp;<?= $transaction ?></p>
<p><strong>USD:</strong>&nbsp;<?= $usd ?></p>
<p><strong>BTC:</strong>&nbsp;<?= $btc ?></p>
<p><strong>Estado:&nbsp;</strong>&nbsp;<?= $status ?></p>