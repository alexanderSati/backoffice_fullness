<?php defined('BASEPATH') OR exit('No direct script access allowed');

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of token_transactions
 *
 * @author Juan Manuel
 */

?>
<p>
    <img width="260" src="<?= base_url() ?>/themes/gentella/images/logo.png"
         alt="Exchange">
</p>
<h1>Atención, Un nuevo pago a sido solicitado.</h1>
<hr>
<p>Se han anexado los datos de usuario y de la transaccion.</p>
<br>
<p><strong>Fecha:</strong>&nbsp;<?= $date ?></p>
<p><strong>Transacc|ion:</strong>&nbsp;<?= $transaction ?></p>
<p><strong>Username:</strong>&nbsp;<?= $username ?></p>
<p><strong>Email:</strong>&nbsp;<?= $email ?></p>
<p><strong>Billetera:</strong>&nbsp;<?= $wallet ?></p>
<p><strong>Cantidad:</strong>&nbsp;<?= $amount ?></p>
<p></p>
<h2>Transaccion</h2>
<hr>
<p><strong>USD:</strong>&nbsp;<?= $usd ?></p>
<p><strong>Comision:</strong>&nbsp;<?= $comission_rate ?>%</p>
<p><strong>Comision en USD:</strong>&nbsp;<?= round($comission_usd,2) ?></p>
<p><strong>USD a pagar:</strong>&nbsp;<?= round($payout_usd,2) ?></p>
<p><strong>BTC a pagar:</strong>&nbsp;<?= $payout_btc ?></p>
<p><strong>Estado:&nbsp;</strong>Pendiente</p>
<hr>
<p><strong>QR Address:&nbsp;</strong></p>
<h2><?= $qr_code ?></h2>