<?php defined('BASEPATH') OR exit('No direct script access allowed');

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of token_transactions
 *
 * @author Juan Manuel
 */

?>
<p>
    <img width="260" src="<?= base_url() ?>/themes/gentella/images/logo.png"
         alt="Exchange">
</p>
<h1>Solicitud de deposito.</h1>
<hr>
<p>Su solicitud ha sido recibida exitosamente, proximamente un agente le estare enviado un email con la informacion necesaria para continuar.</p>
<br>
<p><strong>Fecha:</strong>&nbsp;<?= $date ?></p>
<p><strong>Codigo:</strong>&nbsp;<?= $transaction ?></p>
<p><strong>Nombre Usuario:</strong>&nbsp;<?= $name ?></p>
<p><strong>Login:</strong>&nbsp;<?= $username ?></p>
<p><strong>Email:</strong>&nbsp;<?= $email ?></p>
<p><strong>Cantidad a pagar:</strong>&nbsp;<?= $amount ?></p>
<p><strong>Moneda a pagar:</strong>&nbsp;<?= $coin ?></p>
<p><strong>BTC:</strong>&nbsp;<?= $btc ?></p>

<p><strong>Cantidad a recibir:</strong>&nbsp;<?= $amount_final ?></p>
<p><strong>Moneda a recibir:</strong>&nbsp;<?= $coin_final ?></p>

<p><strong>Estado:&nbsp;</strong>Espera por confirmación</p>