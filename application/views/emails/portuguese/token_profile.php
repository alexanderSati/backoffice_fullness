<?php

/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

?>
<p>
    <img width="260" src="<?= base_url() ?>/themes/gentella/images/logo.png"
         alt="Exchange">
</p>
<p>Para continuar con el proceso de edicion, es necesario realizar una verificacion de seguridad.</p>
<p>
    <p>Ingrese este codigo de verificacion en el formulario de edicion</p>
    <hr>
    <h2><?= $token ?></h2>
    <br>
    <br>
    <strong>Este token expira en 10 minutos, posterior a este tiempo debera solicitar un nuevo token.</strong>