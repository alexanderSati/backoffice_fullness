<?php defined('BASEPATH') OR exit('No direct script access allowed');

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Approved orden
 *
 * @author Jesus Alejandro Pinto Montoya
 */

?>
<table border="0" cellpadding="0" cellspacing="0" width="100%"  style="background: #efefef;">	
		<tr>
			<td style="padding: 10px 0 30px 0;">
				<table align="center" border="0" cellpadding="0" cellspacing="0" width="600" style="border-collapse: collapse;">
					<tr>
						<td align="center" bgcolor="#163D5B" style="border-radius:10px 10px 0 0; padding: 40px 0 30px 0; color: #153643; font-size: 28px; font-weight: bold; font-family: Arial, sans-serif;">
                            <img src="https://fullnessglobal.com/wp-content/uploads/2019/07/Sin-título-2.png" alt="Fullnessglobal" width="130" height="50" style="display: block;" />							
						</td>
					</tr>
                    <tr>
						<td bgcolor="#ffffff" style="padding: 40px 30px 40px 30px;">
							<table border="0" cellpadding="0" cellspacing="0" width="100%">
								<tr>
                                Best regards <?php echo $user['first_name'] ?>, you are informed that your order  with number <?php echo sprintf('%011d',$order_id) ?> <br>
                                with the following products has been approved:        
                                </tr>
                                <tr>
                                <table style="border: 1px solid #b3afaf; border-collapse: collapse;">
										<tbody>
											<tr>
												<th width="40%" style="border: 1px solid #b3afaf; ">Name</th>
												<th width="15%" style="border: 1px solid #b3afaf; ">Quantity</th>
												<th width="15%" style="border: 1px solid #b3afaf; ">Price</th>
												<th width="15%" style="border: 1px solid #b3afaf; ">Subtotal</th>
											</tr>
											<?php foreach($items as $item): ?>
												<tr> 
													<td style="border: 1px solid #b3afaf; "><?php echo $item['product_name'] ?>
												</td>
												<td style="border: 1px solid #b3afaf; text-align: center;">
													<?php echo $item['quantity'] ?>
												</td>
												<td style="border: 1px solid #b3afaf; text-align: right;">
													<?php echo $item['value'] ?>
												</td>
												<td style="border: 1px solid #b3afaf; text-align: right;">
													<?php echo $item['quantity'] * $item['value'] ?>
												</td>
												</tr> 
												<?php endforeach; ?>
													<tr>
														<td colspan="3" align="center" style="border: 1px solid #b3afaf; ">Total</td>
														<td style="border: 1px solid #b3afaf; text-align: right;">
															<?= $total_valor ?>
														</td>
													</tr>
										</tbody>
									</table>
                                </tr>                               
			
							</table>
						</td>
					</tr>
				</table>
			</td>
		</tr>
</table>