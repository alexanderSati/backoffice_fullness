<?php defined('BASEPATH') OR exit('No direct script access allowed');

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of token_transactions
 *
 * @author Juan Manuel
 */

?>
<p>
    <img width="260" src="<?= base_url() ?>/themes/gentella/images/logo.png"
         alt="Exchange">
</p>
<h1>Payout request has been updated.</h1>
<hr>
<p>Your transaction has been updated.</p>
<br>
<p><strong>Date:</strong>&nbsp;<?= $date ?></p>
<p><strong>Transaction:</strong>&nbsp;<?= $transaction ?></p>
<p><strong>Wallet:</strong>&nbsp;<?= $wallet ?></p>
<p><strong>Amount:</strong>&nbsp;<?= $amount ?></p>
<p><strong>Wallet QR:</strong>&nbsp;<?= $wallet_qr ?></p>
<p><strong>Status:&nbsp;</strong>&nbsp;<?= $status ?></p>