<?php defined('BASEPATH') OR exit('No direct script access allowed');

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of token_transactions
 *
 * @author Juan Manuel
 */

?>
<p>
    <img width="260" src="<?= base_url() ?>/themes/gentella/images/logo.png"
         alt="Exchange">
</p>
<h1>A new Coin deposit has been requested by you.</h1>
<hr>
<p>Below, user and transaction information.</p>
<br>
<p><strong>Date:</strong>&nbsp;<?= $date ?></p>
<p><strong>Transaction:</strong>&nbsp;<?= $transaction ?></p>
<p><strong>User Name:</strong>&nbsp;<?= $name ?></p>
<p><strong>Login:</strong>&nbsp;<?= $username ?></p>
<p><strong>Email:</strong>&nbsp;<?= $email ?></p>
<p><strong>Amount to pay:</strong>&nbsp;<?= $amount ?></p>
<p><strong>Coin to pay:</strong>&nbsp;<?= $coin ?></p>
<p><strong>BTC:</strong>&nbsp;<?= $btc ?></p>

<p><strong>Amount to receive:</strong>&nbsp;<?= $amount_final ?></p>
<p><strong>Coin to receive:</strong>&nbsp;<?= $coin_final ?></p>

<p><strong>Status:&nbsp;</strong>Waiting</p>