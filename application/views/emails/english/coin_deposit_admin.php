<?php defined('BASEPATH') OR exit('No direct script access allowed');

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of token_transactions
 *
 * @author Juan Manuel
 */

?>
<p>
    <img width="260" src="<?= base_url() ?>/themes/gentella/images/logo.png"
         alt="Exchange">
</p>
<h1>Attention, A new Coin deposit has been requested.</h1>
<hr>
<p>Below, user and transaction information.</p>
<br>
<p><strong>Date:</strong>&nbsp;<?= $date ?></p>
<p><strong>Transaction:</strong>&nbsp;<?= $transaction ?></p>
<p><strong>User Name:</strong>&nbsp;<?= $name ?></p>
<p><strong>Login:</strong>&nbsp;<?= $username ?></p>
<p><strong>Email:</strong>&nbsp;<?= $email ?></p>
<p><strong>Description:</strong>&nbsp;<?= $description ?></p>
<p><strong>Amount to deposit:</strong>&nbsp;<?= $usd ?></p>
<p><strong>BTC:</strong>&nbsp;<?= $btc ?></p>
<p><strong>Coin to deposit:</strong>&nbsp;<?= $wallet ?></p>
<p><strong>Status:&nbsp;</strong>Pending</p>