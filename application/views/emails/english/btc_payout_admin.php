<?php defined('BASEPATH') OR exit('No direct script access allowed');

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of token_transactions
 *
 * @author Juan Manuel
 */

?>
<p>
    <img width="260" src="<?= base_url() ?>/themes/gentella/images/logo.png"
         alt="Exchange">
</p>
<h1>Attention, A new payout has been requested.</h1>
<hr>
<p>Below, user and transaction information.</p>
<br>
<p><strong>Date:</strong>&nbsp;<?= $date ?></p>
<p><strong>Transaction:</strong>&nbsp;<?= $transaction ?></p>
<p><strong>Username:</strong>&nbsp;<?= $username ?></p>
<p><strong>Email:</strong>&nbsp;<?= $email ?></p>
<p><strong>Wallet:</strong>&nbsp;<?= $wallet ?></p>
<p><strong>Amount:</strong>&nbsp;<?= $amount ?></p>
<p></p>
<h2>Transaction</h2>
<hr>
<p><strong>USD:</strong>&nbsp;<?= $usd ?></p>
<p><strong>Comission rate:</strong>&nbsp;<?= $comission_rate ?>%</p>
<p><strong>Comission USD:</strong>&nbsp;<?= round($comission_usd,2) ?></p>
<p><strong>USD to pay:</strong>&nbsp;<?= round($payout_usd,2) ?></p>
<p><strong>BTC to pay:</strong>&nbsp;<?= $payout_btc ?></p>
<p><strong>Status:&nbsp;</strong>Pending</p>
<hr>
<p><strong>QR Address:&nbsp;</strong></p>
<h2><?= $qr_code ?></h2>