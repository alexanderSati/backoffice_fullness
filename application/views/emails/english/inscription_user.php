<?php defined('BASEPATH') OR exit('No direct script access allowed');

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of token_transactions
 *
 * @author Julian David
 */

?>
<body style="background: #efefef;">
    <table align="center" border="0" cellpadding="0" cellspacing="0" style=" border-collapse: collapse; width: 100%; max-width: 600px;" class="content">
        <tbody>
        <tr>
                            <td align="center" bgcolor="#163D5B" style="border-radius:10px 10px 0 0; padding: 40px 0 30px 0; color: #153643; font-size: 28px; font-weight: bold; font-family: Arial, sans-serif;">
                                <img src="https://fullnessglobal.com/wp-content/uploads/2019/07/Sin-título-2.png" alt="Fullnessglobal" width="130" height="50" style="display: block;" />							
                            </td>
                        </tr>
        
        <tr>
            <td align="center" bgcolor="#ffffff" style="padding: 40px 20px 40px 20px; color: #555555; font-family: Arial, sans-serif; font-size: 20px; line-height: 30px; border-bottom: 1px solid #f6f6f6;" class="">
                <b>You have registered</b><br>                    
                Thank you very much for choosing Fullness global!
            </td>
        </tr>
        <tr>
            <td align="center" bgcolor="#ffffff">
                <b style="font-size: 20px; font-family: Arial, sans-serif;" >
                Your information
                </b>                
            </td>
        </tr>
        <tr> 
            <td align="center" bgcolor="#ffffff" style=" font-family: Arial, sans-serif; line-height: 30px; border-bottom: 1px solid #f6f6f6;">
                        <p><strong>Date:</strong>&nbsp;<?= $date ?></p>
                        <p><strong>Username:</strong>&nbsp;<?= $username ?></p>     
                        <p><strong>Email:</strong>&nbsp;<?= $email ?></p>        
            </td>
        </tr>
        
    
        <tr>
            <td align="center" bgcolor="#163D5B" style="padding: 15px 10px 15px 10px; color: white; font-family: Arial, sans-serif; font-size: 12px; line-height: 18px;">
                2019 © <a href="<?= base_url() ?>" style="color: #ffffff;" target=”_blank”>Fullness global</a>
            </td>
        </tr>
    </tbody>
    </table>
    
</body>