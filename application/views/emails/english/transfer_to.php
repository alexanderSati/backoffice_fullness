<?php defined('BASEPATH') OR exit('No direct script access allowed');

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Transfer to
 *
 * @author Jesus Alejandro Pinto Montoya
 */

?>
<table border="0" cellpadding="0" cellspacing="0" width="100%" style="background: #efefef;">	
		<tr>
			<td style="padding: 10px 0 30px 0;">
				<table align="center" border="0" cellpadding="0" cellspacing="0" width="600" style="border-collapse: collapse;">
					<tr>
						<td align="center" bgcolor="#163D5B" style="border-radius:10px 10px 0 0; padding: 40px 0 30px 0; color: #153643; font-size: 28px; font-weight: bold; font-family: Arial, sans-serif;">
                            <img src="https://fullnessglobal.com/wp-content/uploads/2019/07/Sin-título-2.png" alt="Fullnessglobal" width="130" height="50" style="display: block;" />							
						</td>
					</tr>
                    <tr>
						<td bgcolor="#ffffff" style="padding: 40px 30px 40px 30px;">
							<table border="0" cellpadding="0" cellspacing="0" width="100%">
								<tr>
                                Best regards <?php echo $user_to['first_name'] ?>, you are informed that you received a transfer for <?php echo $user_from['first_name'] ?> with username <?php echo $user_from['username'] ?>,
                                for a value of <?php echo $amount ?>.       
                                </tr>                              			
							</table>
						</td>
					</tr>
				</table>
			</td>
		</tr>
</table>