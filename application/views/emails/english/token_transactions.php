<?php defined('BASEPATH') OR exit('No direct script access allowed');

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of token_transactions
 *
 * @author Juan Manuel
 */

?>

<table border="0" cellpadding="0" cellspacing="0" width="100%"  style="background: #efefef;">	
		<tr>
			<td style="padding: 10px 0 30px 0;">
				<table align="center" border="0" cellpadding="0" cellspacing="0" width="600" style="border-collapse: collapse;">
					<tr>
						<td align="center" bgcolor="#163D5B" style="border-radius:10px 10px 0 0; padding: 40px 0 30px 0; color: #153643; font-size: 28px; font-weight: bold; font-family: Arial, sans-serif;">
                            <img src="https://fullnessglobal.com/wp-content/uploads/2019/07/Sin-título-2.png" alt="Fullnessglobal" width="130" height="50" style="display: block;" />							
						</td>
					</tr>
					<tr>
						<td bgcolor="#ffffff" style="padding: 40px 30px 40px 30px;">
							<table border="0" cellpadding="0" cellspacing="0" width="100%">
								<tr>
									<td style="padding: 20px 0 30px 0; color: #153643; font-family: Arial, sans-serif; font-size: 16px; line-height: 20px;">
                                    To continue with the edit process, it's necessary to perform a security check.
                                    <br>
                                    Please, use this security code to continue in form
                                    </td>
        
                                </tr>
                                <hr>
                                <tr>
                                <td style="text-align: center;padding: 20px 0 30px 0; color: #153643; font-family: Arial, sans-serif; line-height: 20px;">
                                        <h2><?= $token ?></h2>
                                    </td>  
                                </tr>
                                <hr>
								<tr>
									<td>
										<table border="0" cellpadding="0" cellspacing="0" width="100%">
											<tr>
												<td width="260" valign="top">
													<table border="0" cellpadding="0" cellspacing="0" width="100%">
														<tr>
															<td style="padding: 25px 0 0 0; color: #153643; font-family: Arial, sans-serif; font-size: 16px; line-height: 20px;">
                                                            <strong>This code will expire in 15 minutes, after this time you must request a new token.</strong>
														</tr>
													</table>
												</td>
												<td style="font-size: 0; line-height: 0;" width="20">
													&nbsp;
												</td>
											</tr>
										</table>
									</td>
								</tr>
							</table>
						</td>
					</tr>
				</table>
			</td>
		</tr>
	</table>
