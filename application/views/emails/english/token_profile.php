<?php

/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

?>
<p>
    <img width="260" src="<?= base_url() ?>/themes/gentella/images/logo.png"
         alt="Exchange">
</p>
<p>To continue with the edit process, it's necessary to perform a security check.</p>
<p>
    <p>Please, use this security code to continue in edit form</p>
    <hr>
    <h2><?= $token ?></h2>
    <br>
    <br>
    <strong>This code will expire in 10 minutes, after this time you must request a new token.</strong>