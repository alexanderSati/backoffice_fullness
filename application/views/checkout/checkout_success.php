<div class="card">
  <div class="card-header">
    <h5><?= $this->lang->line("order_registered"); ?></h5>

  </div>
  <div class="card-block">

    <div class="alert alert-success background-success">
      <button type="button" class="close" data-dismiss="alert" aria-label="Close">
        <i class="icofont icofont-close-line-circled text-white"></i>
      </button>
      <strong><?= $this->lang->line("order_registered"); ?>!</strong>
    </div>

    <div class="row">
      <div class="col-md-6 col-sm-12">
        <div class="card">
          <div class="card-block">
            <div class="row align-items-center m-l-0">
              <div class="col-auto">
                <i class="icon feather icon-shopping-cart f-30 text-c-red"></i>
              </div>
              <div class="col-auto">
                <h6 class="text-muted m-b-10"><?= $this->lang->line("order_number"); ?></h6>
                <h2 class="m-b-0">#<?= sprintf('%011d',$orden["shopping_order_id"]); ?></h2>
              </div>
            </div>
          </div>
        </div>
      </div>
      <div class="col-md-6 col-sm-12">
        <div class="card">
          <div class="card-block" style="padding:1.1em !important">
            <div class="row align-items-center m-l-0">
              <table>
                <tr>
                  <td><b>DNI:</b></td>
                  <td><?= $user["dni"] ?> </td>
                </tr>
                <tr>
                  <td><b><?= $this->lang->line("name_product"); ?>:</b></td>
                  <td><?= $user["first_name"] ?> <?= $user["last_name"] ?></td>
                </tr>
                <tr>
                  <td><b><?= $this->lang->line("shipping_label"); ?>:</b></td>
                  <td><?= $envio["tipo"] ?> - <?= $envio["nombre"] ?></td>
                </tr>
              </table>
            </div>
          </div>
        </div>
      </div>
    </div>

    <!-- LISTADO DE PRODUCTOS -->
    <div class="row">
      <div class="col">
        <div class="card latest-update-card">
          <div class="card-header">
            <h5><?= lang('users title payment_method') ?></h5>
          </div>
          <div class="card-block">

            <!-- metodo de pago -->
            <h2></h2>
            <p class="text-center">


              <?php 

                switch($pago["method"]){ 
                  case 'BILLETERA': 
                    echo '<img src="'.base_url('themes/able/images/pago_en_efectivo.jpg').'" style="border-radius: 8px" /><br/>';
                    echo '<h3 class="text-center" >'.lang('wallets').'</h3>';
                    break;
                  case 'PAGO_OFICINA':
                    echo '<img src="'.base_url('themes/able/images/pago_en_efectivo.jpg').'" style="border-radius: 8px" /><br/>';
                    echo '<h3 class="text-center" >'.$this->lang->line('pay_office_label').'</h3>';
                    break;
                  case 'PAGO_MAYORISTA':
                    echo '<img src="'.base_url('themes/able/images/pago_en_efectivo.jpg').'" style="border-radius: 8px" /><br/>';
                    echo '<h3 class="text-center" >'.$this->lang->line('pay_wholesaler_label').'</h3>';
                    break;
                  case 'PAGO_ONLINE':
                    echo '<img src="'.base_url('uploads/banking_pay.jpg').'" style="border-radius: 8px" /><br/>';
                    echo $this->lang->line('pay_online_label');
                    echo "<h3 class='text-center' ><a href='".$pago["url"]."' target='_blank' class='f-20'>".lang('payment_code')." ".$pago["code"].'</a></h3>';
                    echo "<iframe src=". $pago["url"] ." style='width:90%;display:block; margin:auto; border:none; height: 1600px' ></iframe>";

                    break;

                  case 'PAGO_VISANET':
                    $visanet_response = json_decode($pago["description"], true);
                    echo "<h3>".lang('pay_creditcard_label')."</h3>";
                    echo "
                    <div class='table-responsive'>
                    <table class='table table-framed'>
                        <tbody>
                            <tr>
                              <td><b>Página:</b></td>
                              <td><a href='https://backoffice.fullnessglobal.com'>fullnessglobal.com</a></td>
                            </tr>
                            <tr>
                              <td><b>Télefono:</b></td>
                              <td>(01)635 4385</td>
                            </tr>
                            <tr>
                              <td><b>Dirección:</b></td>
                              <td>Calle velasquez 351- San Borja,Lima,Perú</td>
                            </tr>
                            <tr>
                              <td><b>Número de tarjeta:</b></td>
                              <td>{$visanet_response["dataMap"]["CARD"]}</td>
                            </tr>
                            <tr>
                              <td><b>Monto:</b></td>
                              <td>S/. {$pago["amount"]}</td>
                            </tr>
                            <tr>
                              <td><b>Moneda:</b></td>
                              <td>PEN</td>
                            </tr>
                            <tr>
                              <td><b>Fecha del pedido:</b></td>
                              <td>{$orden["created"]}</td>
                            </tr>
                            <tr>
                              <td><b>Descripción:</b></td>
                              <td>".(empty($visanet_response["dataMap"]["ACTION_DESCRIPTION"])?$pago["description"]:$visanet_response["dataMap"]["ACTION_DESCRIPTION"] )."</td>
                            </tr>
                            <tr>
                              <td><b>Comprobante:</b></td>
                              <td>
                                <p>Recuerde imprimir y guardar su comprobante</p>
                                <button class='btn btn-primary btn-round waves-effect waves-light' onclick='window.print()' >imprimir</button>
                              </td>
                            </tr>
                            <tr>
                              <td colspan='2'><h4 class='text-center' ><a href='".base_url("uploads/terms_politics.pdf")."' target='_blank' >Términos y Condiciones</a></h4></td>
                            </tr>
                        </tbody>
                    </table>
                </div>
                    ";
                  default:
                }
              ?>
              <br/>

            </p>
            <!-- ./metodo de pago -->
          </div>
        </div>
      </div>
    </div>
    <!-- ./LISTADO DE PRODUCTOS -->


    <div class="row">
      <div class="col-md-12">
        <div class="card">
          <div class="card-header">
            <h5><?= $this->lang->line("product_list"); ?></h5>
          </div>
          <div class="card-block">
            <p class="text-center">


            <table class="table">
              <thead>
                <tr>
                  <th>COD</th>
                  <th></th>
                  <th><?= $this->lang->line("product_name"); ?></th>
                  <th><?= $this->lang->line("product_qty"); ?></th>
                  <th><?= $this->lang->line("product_price"); ?></th>
                  <th><?= $this->lang->line("subtotal"); ?></th>
                </tr>
              </thead>
              <tbody>

                <?php foreach( $products as $product ): ?>
                <tr>
                  <td><?= $product["code"]; ?></td>
                  <td>
                    <img src="<?= base_url("uploads/".$product["product_image"] ); ?>" alt="Product image"
                      class="img-fluid rounded" style="max-height:5em" />
                  </td>
                  <td><?= $product["product_name"]; ?></td>
                  <td><?= $product["quantity"]; ?></td>
                  <td align="right">
                    S/<?= number_format(floatVal($product["value"]),2); ?>
                  </td>
                  <td align="right">
                    S/<?= number_format(floatVal($product["value"])*intval($product["quantity"]),2) ?>
                  </td>
                </tr>
                <?php endforeach; ?>


              </tbody>
              <tfoot>
                <tr>
                  <td colspan="5" align="right"><b>Total</b></td>
                  <td align="right">S/<?= number_format($orden["total_value"],2) ?></td>
                </tr>
              </tfoot>
            </table>
              <br/>
              <div class="text-center" >
                <a class="btn btn-info" style="margin-top:2em" href="<?= site_url("Orders/purchases"); ?>">
                  <?= $this->lang->line("return_label"); ?>
                </a>
              </div>
            </p>
          </div>
        </div>
      </div>
    </div>


  </div>
</div>