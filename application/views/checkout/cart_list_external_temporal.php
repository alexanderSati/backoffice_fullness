<div class="m-4">

<div class="row">
  <div class="col-md-6">
    <!-- numero de orden -->
    <div class="card">
      <div class="card-block">
        <div class="row align-items-center m-l-0">
          <div class="col-auto">
            <i class="icon feather icon-users f-30 text-c-red"></i>
          </div>
          <div class="col-auto">
            <h6 class="text-muted m-b-10"><?= $this->lang->line('order_number'); ?></h6>
            <h2 class="m-b-0">#<?= $consecutivo_orden; ?></h2>
          </div>
        </div>
      </div>
    </div>
    <!-- ./numero de orden -->
  </div>

</div>






<!-- tabla productos -->
<div class="card" id='cart_details'>
  <div class="card-header">
    <h5><?= $this->lang->line('shopping_cart'); ?></h5>
    <span style="display:none">use class <code>table-hover</code> inside table element</span>
    <br />
    <br />

    <div class="card-header-right">

      <!-- vaciar carro -->
      <div align="right" style="margin-right: 0.6em;">
        <button type="button" id="clear_cart" class="btn btn-warning">
          <?= $this->lang->line('clear_shopping_cart')  ?>
        </button>
      </div>
      <!-- ./vaciar carro -->
    </div>
  </div>
  <div class="card-block table-border-style">
    <div class="table-responsive">
      <table class="table table-hover">
        <thead>
          <tr>
            <th width="10%">Cod</th>
            <th width="30%"><?= $this->lang->line('cart_name') ?></th>
            <th width="15%"><?= $this->lang->line('cart_quantity') ?></th>
            <th width="15%"><?= $this->lang->line('price_shopping_cart') ?></th>
            <th width="15%"><?= $this->lang->line('subtotal_price_shopping_cart') ?></th>
            <th width="15%"><?= $this->lang->line('cart_action') ?></th>
          </tr>
        </thead>
        <tbody id="items_purchase">
          <?php if( count( $this->cart->contents()) == 0 ): ?>

          <tr>
            <td colspan="5" align="center">
              <?= $this->lang->line('cart_empty');?>
            </td>
          </tr>
          <?php endif; ?>
        </tbody>
      </table>
    </div>
  </div>
</div>
<!-- ./tabla productos -->

<input type="hidden" name="<?= $csrf['name'] ?>" value="<?= $csrf['hash'] ?>" id="csrf_data">


<div class="card">
  <div class="card-block">
    <h2><?= lang('users title shipping_method') ?></h2>
    <br />


    <div class="row">
      <div class="col-md-2">
        <div class="radio" id="radio_courier">
          <label onclick="open_tab('courier')">
            <input type="radio" name="metodo" form="envio_formulario" id="check_courier" value="direccion" checked="">
            <b>Courier</b>
          </label>
        </div>
      </div>
      <div class="col-md-2">
        <div class="radio" id="radio_oficina">
          <label onclick="open_tab('oficina_ppal')">
          <input type="radio" name="metodo" form="envio_formulario"  value="Retiro en oficina ppal">
            <b><?= $this->lang->line('withdraw_office'); ?></b>
          </label>
        </div>
      </div>
      <div class="col-md-2">     
      </div>
    </div>

    <!-- OPCIONES DE ENVIO -->
    <input type="hidden" id="tipo_envio" value="<?= $tipo_envio ?>" />
    <form role="tablist" aria-multiselectable="true" method="post" id="envio_formulario"
      action="<?= site_url("checkout_external/buy"); ?>">

      <div class="accordion-panel">
        <div class="accordion-heading" role="tab" id="headingOne">
          <h3 class="card-title accordion-title">
            <a class="accordion-msg waves-effect waves-dark scale_active collapsed" data-parent="#envio_formulario"
              aria-expanded="true" aria-controls="courier">
              <b>Courier</b>
            </a>
          </h3>
        </div>
        <div id="courier" class="panel-collapse in collapse in show" role="tabpanel" aria-labelledby="headingOne">
          <div class="accordion-content accordion-desc">
            <input type="text" class="form-control" form="envio_formulario" name="envio[direccion]"
              id="direccion_courier" placeholder="Ingrese su dirección">
          </div>
        </div>
      </div>           
    </form>
    <!-- ./OPCIONES DE ENVIO -->

    <div class="row">
      <div class="col-md-6">
        <a class="btn btn-info pull-right" style="width: 8em;" href="<?= $_SERVER['HTTP_REFERER'] ?>">
          <i class="fa fa-arrow-left" aria-hidden="true"></i>
          <?= $this->lang->line('cart_return'); ?>
        </a>
      </div>
      <div class="col-md-6">

        <button class="btn btn-primary pull-left" style="width: 8em;" data-toggle="modal" data-target="#modal-default">
          <?= $this->lang->line('cart_buy'); ?>
        </button>

      </div>
    </div>

    </p>
  </div>
</div>




<!-- CONFIRMAR COMPRA -->
<div class="modal fade" id="modal-default" tabindex="-1" role="dialog" style="z-index: 1050; display: none;"
  aria-hidden="true">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h4 class="modal-title"><?= lang('users title confirm_purchase') ?></h4>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">×</span>
        </button>
      </div>
      <div class="modal-body">
        <p><?= $this->lang->line('question_confirm_purchase'); ?></p>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-danger pull-right" data-dismiss="modal">
          <?= $this->lang->line('cancel_buy'); ?>
        </button>
        <button type="submit" onclick="enviar_formulario()" id="btn_comprar" class="btn btn-primary pull-right">
          <?= $this->lang->line('cart_buy'); ?>
        </button>
      </div>
    </div>
  </div>
</div>
<!-- ./CONFIRMAR COMPRA -->

<!-- NO ENVIO EMAIL -->
<div class="modal fade" id="no_shipping_modal" tabindex="-1" role="dialog" style="z-index: 1050; display: none;"
  aria-hidden="true">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h4 class="modal-title"><?= $this->lang->line("message"); ?></h4>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">×</span>
        </button>
      </div>
      <div class="modal-body">
        <p><?= $this->lang->line("no_shipping_selected"); ?></p>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-default waves-effect "
          data-dismiss="modal"><?= $this->lang->line("core button close") ?></button>
      </div>
    </div>
  </div>
</div>
<!-- NO ENVIO EMAIL -->
</div>

<input type="hidden" value="<?= lang('users msg confirm_clear_cart') ?>" id="confirm_clear_cart">
<input type="hidden" value="<?= lang('users msg confirm_remove_inventory') ?>" id="confirm_remove_inventory">
<input type="hidden" value="<?= lang('users button confirm') ?>" id="confirm_button">
<input type="hidden" value="<?= lang('users button cancel') ?>" id="cancel_button">
<input type="hidden" value="<?= lang('users msg correct_clear') ?>" id="correct_clear">
<input type="hidden" value="<?= lang('users msg correct_remove_inventory') ?>" id="correct_remove_inventory">

<style>
#select2-select_mayorista-container {
  max-height: 2.5em;
}

#select2-oficina-container {
  max-height: 2.5em;
}
</style>
