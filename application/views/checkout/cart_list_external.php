<div class="m-4">
  <div class="row">


    <div class="col-md-6">


      <!-- numero de orden -->


      <div class="card">


        <div class="card-block">


          <div class="row align-items-center m-l-0">


            <div class="col-auto">


              <i class="icon feather icon-users f-30 text-c-red"></i>


            </div>


            <div class="col-auto">


              <h6 class="text-muted m-b-10"><?= $this->lang->line('order_number'); ?></h6>


              <h2 class="m-b-0">#<?= $consecutivo_orden; ?> - PERÚ</h2>


            </div>


          </div>


        </div>


      </div>


      <!-- ./numero de orden -->


    </div>
  </div>



  <!-- tabla productos -->


  <div class="card" id='cart_details'>


    <div class="card-header">


      <h5><?= $this->lang->line('shopping_cart'); ?></h5>


      <span style="display:none">use class <code>table-hover</code> inside table element</span>


      <br />


      <br />





      <div class="card-header-right">





        <!-- vaciar carro -->


        <div align="right" style="margin-right: 0.6em;">


          <button type="button" id="clear_cart" class="btn btn-warning">


            <?= $this->lang->line('clear_shopping_cart')  ?>


          </button>


        </div>


        <!-- ./vaciar carro -->


      </div>


    </div>


    <div class="card-block table-border-style">


      <div class="table-responsive">


        <table class="table table-hover">


          <thead>


            <tr>


              <th width="10%">Cod</th>


              <th width="30%"><?= $this->lang->line('cart_name') ?></th>


              <th width="15%"><?= $this->lang->line('cart_quantity') ?></th>


              <th width="15%"><?= $this->lang->line('price_shopping_cart') ?></th>


              <th width="15%"><?= $this->lang->line('subtotal_price_shopping_cart') ?></th>


              <th width="15%"><?= $this->lang->line('cart_action') ?></th>


            </tr>


          </thead>


          <tbody id="items_purchase">


            <?php if (count($this->cart->contents()) == 0) : ?>





              <tr>


                <td colspan="5" align="center">


                  <?= $this->lang->line('cart_empty'); ?>


                </td>


              </tr>


            <?php endif; ?>


          </tbody>



        </table>


      </div>

    </div>


  </div>


  <!-- ./tabla productos -->





  <input type="hidden" name="<?= $csrf['name'] ?>" value="<?= $csrf['hash'] ?>" id="csrf_data">








  <div class="card">
    <div class="card-block">
      <h2><?= lang('users title shipping_method') ?></h2>
      <br />


      <div class="row">
        <div class="col-md-2">
          <div class="radio" id="radio_courier">
            <label onclick="open_tab('courier')">
              <input type="radio" name="metodo" form="envio_formulario" id="check_courier" value="direccion" checked="">
              <b>Courier</b>
            </label>
          </div>
        </div>
        <div class="col-md-2">
          <div class="radio" id="radio_oficina">
            <label onclick="open_tab('oficina_ppal')">
              <input type="radio" name="metodo" form="envio_formulario" value="Retiro en oficina ppal">
              <b><?= $this->lang->line('withdraw_office'); ?></b>
            </label>
          </div>
        </div>
        <div class="col-md-2">
        </div>
      </div>

      <!-- OPCIONES DE ENVIO -->
      <input type="hidden" id="tipo_envio" value="<?= $tipo_envio ?>" />
      <form role="tablist" aria-multiselectable="true" method="post" id="envio_formulario" action="<?= site_url("checkout_external/buy"); ?>">

        <div class="accordion-panel">
          <div class="accordion-heading" role="tab" id="headingOne">
            <h3 class="card-title accordion-title">
              <a class="accordion-msg waves-effect waves-dark scale_active collapsed" data-parent="#envio_formulario" aria-expanded="true" aria-controls="courier">
                <b>Courier</b>
              </a>
            </h3>
          </div>
          <div id="courier" class="panel-collapse in collapse in show" role="tabpanel" aria-labelledby="headingOne">
            <div class="accordion-content accordion-desc">
              <input type="text" class="form-control" form="envio_formulario" name="envio[direccion]" id="direccion_courier" placeholder="Ingrese su dirección">
            </div>
          </div>
        </div>
      </form>
      <!-- ./OPCIONES DE ENVIO -->
    </div>
  </div>





  <!-- METODO DE PAGO -->


  <h2> <?= lang('users title payment_method') ?> </h2>


  <br />








  <div id="accordion" role="tablist" aria-multiselectable="true">


    <div class="accordion-panel">


      <div class="accordion-heading" role="tab" id="headingOne" style="display:none">


        <h3 class="card-title accordion-title">


          <a class="accordion-msg waves-effect waves-dark scale_active collapsed" data-toggle="collapse" data-parent="#accordion" href="#collapseOne" aria-expanded="false" aria-controls="collapseOne">


            <?= lang('wallets') ?> <h6 style="display:inline"><?= lang('users title available') ?> (


              S/<?= number_format($saldoDisponible, 2); ?> )</h6>


          </a>


        </h3>


      </div>


      <div id="collapseOne" class="panel-collapse in collapse" role="tabpanel" aria-labelledby="headingOne" style="display:none">


        <div class="accordion-content accordion-desc" style="display:none">





          <div class="form-group">


            <br>


            <input type="text" class="form-control" placeholder="<?= lang('users title saldo') ?>" id="wallet_discount">


          </div>








          <div class="text-center">


            <button class="btn btn-info" onclick="agregarPagoPorBilletera(this)" id="btn_pay_wallet">


              <?= lang('users button accept') ?>


            </button><br /><br />





            <h4 id="mensaje_billetera" class="text-success" style="display:none">


              Pago con saldo de billetera añadido


            </h4>


            <h4 id="mensaje_billetera_error" class="text-danger" style="display:none">


              Error añadiendo pago


            </h4>


          </div>


        </div>


      </div>


    </div>





    <?php if (!empty($metodo_pago)) : ?>


      <div class="alert alert-primary background-primary">


        <button type="button" class="close" data-dismiss="alert" aria-label="Close">


          <i class="icofont icofont-close-line-circled text-white"></i>


        </button>


        <?= lang("pay_registered_label") ?>


      </div>


    <?php endif; ?>





    <div class="col-lg-12 col-xl-12" id="metodos_pago" data-isPay="<?= (!empty($metodo_pago) ? "true" : "false") ?>">


      <div class="sub-title"></div>





      <!-- PAGO ONLINE -->


      <ul class="nav nav-tabs md-tabs tabs-left b-none" role="tablist" style="width: 225px;">


        <li class="nav-item">


          <a class="nav-link  active show" data-toggle="tab" href="#pago_online" role="tab" aria-selected="false">


            <label onclick="document.querySelector('[name=tipo_pago][value=PAGO_ONLINE]').checked = true;">


              <input type="radio" name="tipo_pago" value="PAGO_ONLINE" <?= ($metodo_pago["method"] == "PAGO_ONLINE") ? "checked" : "" ?>>


              <i class="helper" style="margin-top: 1.7em;"></i>


              <b><?= $this->lang->line('pay_online_label'); ?></b>


            </label>


          </a>


          <div class="slide"></div>


        </li>


        <!-- ./PAGO ONLINE -->





        <!-- PAGO EN OFICINA -->


        <li class="nav-item" id="pago_en_oficina">


          <a class="nav-link" data-toggle="tab" href="#pago_oficina" role="tab" aria-selected="true">


            <label onclick="document.querySelector('[name=tipo_pago][value=PAGO_OFICINA]').checked = true;" style="margin-left: -16px;">


              <input type="radio" name="tipo_pago" value="PAGO_OFICINA" <?= ($metodo_pago["method"] == "PAGO_OFICINA") ? "checked" : "" ?>>


              <b><?= $this->lang->line('pay_office_label'); ?></b>


            </label>


          </a>





          <div class="slide"></div>


        </li>


        <!-- PAGO EN OFICINA -->





        <!-- PAGO EN MAYORISTA -->


        <li class="nav-item" id="pago_en_mayorista" style ="display:none">


          <a class="nav-link" data-toggle="tab" href="#pago_mayorista" role="tab" aria-selected="false">


            <label onclick="document.querySelector('[name=tipo_pago][value=PAGO_MAYORISTA]').checked = true;" style="margin-left:-4em;">


              <input type="radio" name="tipo_pago" value="PAGO_MAYORISTA" <?= ($metodo_pago["method"] == "PAGO_MAYORISTA") ? "checked" : "" ?>>


              <i class="helper" style="margin-top: 1.7em;"></i>


              <b><?= $this->lang->line('pay_wholesaler_label'); ?></b>


            </label>


          </a>


          <div class="slide"></div>


        </li>


        <!-- ./PAGO EN MAYORISTA -->








        <!-- PAGO VISANET -->


        <li class="nav-item" id="pago_en_visanet">


          <a class="nav-link" data-toggle="tab" href="#pago_visanet" role="tab" aria-selected="false">


            <label onclick="document.querySelector('[name=tipo_pago][value=PAGO_VISANET]').checked = true;" style="margin-left:0em;">


              <input type="radio" name="tipo_pago" value="PAGO_VISANET" <?= ($metodo_pago["method"] == "PAGO_VISANET") ? "checked" : "" ?>>


              <i class="helper" style="margin-top: 1.7em;"></i>


              <b><?= $this->lang->line('pay_creditcard_label'); ?> <img src="<?= base_url("themes/able/images/visa-eps-vector-logo-400x400.png") ?>" style="width:48px" /></b>


            </label>


          </a>


          <div class="slide"></div>


        </li>


        <!-- ./PAGO VISANET -->








      </ul>


      <!-- Tab panes -->


      <div class="tab-content tabs-left-content card-block" style="width: 86%;">





        <!-- PAGO ONLINE TAB -->


        <div class="tab-pane active show" id="pago_online" role="tabpanel">





          <p class="m-0">


            <table>


              <tr>


                <td colspan="3"></td>


                <td colspan="1"> <img src="<?= base_url("themes/able/images/logo_pago_efectivo.png") ?>" style="width:150px;float: right;" alt=""></td>


              </tr>


              <tr>


                <td colspan="4" style="white-space:normal">





                  <b><i class="fa fa-check" aria-hidden="true"></i>


                    Depósitos en efectivo vía PagoEfectivo - </b>Paga en BBVA, BCP, Interbank, Scotiabank,


                  BanBif, Western Union, Tambo+, Kasnet, Full Carga, Red Digital,


                  Money Gram, Caja Arequipa, Disashop, en cualquier agente o


                  agencia autorizada a nivel nacional a la cuenta de PagoEfectivo


                  <a data-toggle="modal" data-target="#default-Modal" style="color:#1c6fc4"><b>¿Cómo funciona?</b></a>


                  <br />


                  <br />


                  <b><i class="fa fa-check" aria-hidden="true"></i>


                    Transferencias bancarias vía PagoEfectivo -</b> Paga en BBVA, BCP, Interbank, Scotiabank, Banbif,


                  Caja Arequipa, a través de la banca por internet o banca móvil en la opcion de pago de servicios


                  <a data-toggle="modal" data-target="#default-Modal" style="color:#1c6fc4"><b>¿Cómo funciona?</b></a>


                </td>


              </tr>


              <tr>


                <td colspan="4" class="text-center">


                  <img src="<?= base_url("themes/able/images/pago_efectivo.png") ?>" style="width: 80%; max-width: 650px; !important" alt="">


                </td>





              </tr>


            </table>





          </p>





        </div>


        <!-- ./PAGO ONLINE TAB -->





        <!-- PAGO OFICINA TAB -->


        <div class="tab-pane" id="pago_oficina" role="tabpanel ">


          <p>


            <div class="m-0 text-center">


              <img src="<?= base_url('themes/able/images/pago_en_efectivo.jpg') ?>" style="max-width:240px;border-radius: 15px;" />


              <div>


                <h4 class="text-center"><?= $this->lang->line('pay_office_label'); ?></h4>


                <p>


                  Debe dirigirse al sitio elegido a recoger su pedido, alli se le solicitará que pague el valor total


                  del monto de su compra.


                </p>


              </div>


            </div>


          </p>


        </div>


        <!-- ./PAGO OFICINA TAB -->





        <!-- PAGO MAYORISTA TAB -->


        <div class="tab-pane" id="pago_mayorista" role="tabpanel">


          <p>


            <div class="m-0 text-center">


              <img src="<?= base_url('themes/able/images/pago_en_efectivo.jpg') ?>" style="max-width:240px;border-radius: 15px;" />


              <div>


                <h4 class="text-center"><?= $this->lang->line('pay_office_label'); ?></h4>


                <p>


                  Debe dirigirse al sitio elegido a recoger su pedido, alli se le solicitará que pague el valor total


                  del monto de su compra.


                </p>


              </div>


            </div>


          </p>


        </div>


        <!-- ./PAGO MAYORISTA TAB -->








        <!-- PAGO VISANET TAB -->


        <div class="tab-pane" id="pago_visanet" role="tabpanel">


          <p>


            <div class="m-0 text-center">


              <div>


                <h4 class="text-center">VISANET</h4>


                <form action='<?= site_url("checkout/visanet_response")  ?>' method='post'>


                  <script src='<?= JS_SCRIPT_VISANET ?>' data-sessiontoken='<?= $visanet["session_key"] ?>' data-channel='web' data-merchantid='<?= MERCHANT_ID ?>' data-merchantlogo='https://fullnessglobal.com/wp-content/uploads/2019/07/Sin-t%C3%ADtulo-2.png' data-formbuttoncolor='#D80000' data-purchasenumber='<?= $visanet["shopping_order_id"] ?>' data-amount='<?= $visanet["amount"] ?>' data-expirationminutes='10' data-timeouturl='<?= site_url("checkout/cart_list")  ?>'></script>


                </form>





              </div>


            </div>


          </p>


        </div>


        <!-- ./PAGO VISANET TAB -->





      </div>


    </div>





  </div>


  <!-- ./METODO DE PAGO -->














  <div class="row">


    <div class="col-12">


      <p class="text-center text-info"><b>Al seleccionar un método de pago acepta nuestros</b></p>


      <a href='<?= base_url("uploads/terms_politics.pdf") ?>' target='_blank'>
        <h5 class='text-center'>Términos y Condiciones</h5>
      </a>





    </div>


  </div>


  <div class="row" style="margin-top:2em">














    <div class="col-6">


      <a class="btn btn-info pull-right" style="width: 8em;" href="<?= $_SERVER['HTTP_REFERER'] ?>">


        <i class="fa fa-arrow-left" aria-hidden="true"></i>


        <?= $this->lang->line('cart_return'); ?>


      </a>


    </div>


    <div class="col-6">





      <button class="btn btn-primary pull-left" style="width: 8em;" data-toggle="modal" data-target="#modal-default" id="btn-open-buy">


        <?= $this->lang->line('cart_buy'); ?>


      </button>





    </div>








  </div>





  </p>


</div>


</div>














<!-- CONFIRMAR COMPRA -->


<div class="modal fade" id="modal-default" tabindex="-1" role="dialog" style="z-index: 1050; display: none;" aria-hidden="true">


  <div class="modal-dialog" role="document">


    <div class="modal-content">


      <div class="modal-header">


        <h4 class="modal-title"><?= lang('users title confirm_purchase') ?></h4>


        <button type="button" class="close" data-dismiss="modal" aria-label="Close">


          <span aria-hidden="true">×</span>


        </button>


      </div>


      <div class="modal-body">


        <p><?= $this->lang->line('question_confirm_purchase'); ?></p>


      </div>


      <div class="modal-footer">


        <button type="button" class="btn btn-danger pull-right" data-dismiss="modal">


          <?= $this->lang->line('cancel_buy'); ?>


        </button>





        <button type="submit" onclick="enviar_formulario()" id="btn_comprar" class="btn btn-primary pull-right">


          <?= $this->lang->line('cart_buy'); ?>


        </button>




      </div>


    </div>


  </div>


</div>


<!-- ./CONFIRMAR COMPRA -->





<!-- NO ENVIO  -->


<div class="modal fade" id="no_shipping_modal" tabindex="-1" role="dialog" style="z-index: 1050; display: none;" aria-hidden="true">


  <div class="modal-dialog" role="document">


    <div class="modal-content">


      <div class="modal-header">


        <h4 class="modal-title"><?= $this->lang->line("message"); ?></h4>


        <button type="button" class="close" data-dismiss="modal" aria-label="Close">


          <span aria-hidden="true">×</span>


        </button>


      </div>


      <div class="modal-body">


        <p><?= $this->lang->line("no_shipping_selected"); ?></p>


      </div>


      <div class="modal-footer">


        <button type="button" class="btn btn-default waves-effect " data-dismiss="modal"><?= $this->lang->line("core button close") ?></button>


      </div>


    </div>


  </div>


</div>


<!-- NO ENVIO  -->








<div class="modal fade" id="default-Modal" tabindex="-1" role="dialog" style="z-index: 1050; display: none;" aria-hidden="true">


  <div class="modal-dialog modal-lg" role="document">


    <div class="modal-content">


      <div class="modal-header">


        <h4 class="modal-title"></h4>


        <button type="button" class="close" data-dismiss="modal" aria-label="Close">


          <span aria-hidden="true">×</span>


        </button>


      </div>


      <div class="modal-body" style="min-height:65vh">


        <iframe src="https://cip.pagoefectivo.pe/CNT/QueEsPagoEfectivo.aspx" style="border:none;min-height:65vh;min-width: 560px;display: block;margin: auto;">


        </iframe>


      </div>





    </div>


  </div>


</div>
</div>





<input type="hidden" value="<?= lang('users msg confirm_clear_cart') ?>" id="confirm_clear_cart">


<input type="hidden" value="<?= lang('users msg confirm_remove_inventory') ?>" id="confirm_remove_inventory">


<input type="hidden" value="<?= lang('users button confirm') ?>" id="confirm_button">


<input type="hidden" value="<?= lang('users button cancel') ?>" id="cancel_button">


<input type="hidden" value="<?= lang('users msg correct_clear') ?>" id="correct_clear">


<input type="hidden" value="<?= lang('users msg correct_remove_inventory') ?>" id="correct_remove_inventory">


<input type="hidden" value="<?= lang('message_no_product_available'); ?>" id="message_no_product" />


<style>
  #select2-select_mayorista-container {


    max-height: 2.5em;


  }





  #select2-oficina-container {


    max-height: 2.5em;


  }
</style>
