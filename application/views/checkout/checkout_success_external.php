<div class="p-4">
<div class="card">

  <div class="card-header">
    <h5><?= $this->lang->line("order_registered"); ?></h5>

  </div>
  <div class="card-block">

    <div class="alert alert-success background-success">
      <button type="button" class="close" data-dismiss="alert" aria-label="Close">
        <i class="icofont icofont-close-line-circled text-white"></i>
      </button>
      <strong><?= $this->lang->line("order_registered"); ?>!</strong>
    </div>

    <div class="row">
      <div class="col-md-6 col-sm-12">
        <div class="card">
          <div class="card-block">
            <div class="row align-items-center m-l-0">
              <div class="col-auto">
                <i class="icon feather icon-shopping-cart f-30 text-c-red"></i>
              </div>
              <div class="col-auto">
                <h6 class="text-muted m-b-10"><?= $this->lang->line("order_number"); ?></h6>
                <h2 class="m-b-0">#<?= sprintf('%011d',$orden["shopping_order_id"]); ?></h2>
              </div>
            </div>
          </div>
        </div>
      </div>
      <div class="col-md-6 col-sm-12">
        <div class="card">
          <div class="card-block" style="padding:1.1em !important">
            <div class="row align-items-center m-l-0">
              <table>
                <tr>
                  <td><b>DNI:</b></td>
                  <td><?= $user["dni"] ?> </td>
                </tr>
                <tr>
                  <td><b><?= $this->lang->line("name_product"); ?>:</b></td>
                  <td><?= $user["first_name"] ?> <?= $user["last_name"] ?></td>
                </tr>
                <tr>
                  <td><b><?= $this->lang->line("shipping_label"); ?>:</b></td>
                  <td><?= $envio["tipo"] ?> - <?= $envio["nombre"] ?></td>
                </tr>
              </table>
            </div>
          </div>
        </div>
      </div>
    </div>

    <!-- LISTADO DE PRODUCTOS -->
    <div class="row">
      <div class="col">
        <div class="card latest-update-card">
          <div class="card-header">
            <h5><?= $this->lang->line("product_list"); ?></h5>
          </div>
          <div class="card-block">
            <table class="table">
              <thead>
                <tr>
                  <th>COD</th>
                  <th></th>
                  <th><?= $this->lang->line("product_name"); ?></th>
                  <th><?= $this->lang->line("product_qty"); ?></th>
                  <th><?= $this->lang->line("product_price"); ?></th>
                  <th><?= $this->lang->line("subtotal"); ?></th>
                </tr>
              </thead>
              <tbody>

                <?php foreach( $products as $product ): ?>
                <tr>
                  <td><?= $product["code"]; ?></td>
                  <td>
                    <img src="<?= base_url("uploads/".$product["product_image"] ); ?>" alt="Product image"
                      class="img-fluid rounded" style="max-height:5em" />
                  </td>
                  <td><?= $product["product_name"]; ?></td>
                  <td><?= $product["quantity"]; ?></td>
                  <td align="right">
                    S/<?= number_format(floatVal($product["value"]),2); ?>
                  </td>
                  <td align="right">
                    S/<?= number_format(floatVal($product["value"])*intval($product["quantity"]),2) ?>
                  </td>
                </tr>
                <?php endforeach; ?>


              </tbody>
              <tfoot>
                <tr>
                  <td colspan="5" align="right"><b>Total</b></td>
                  <td align="right">$<?= number_format($orden["total_value"],2) ?></td>
                </tr>
              </tfoot>
            </table>
          </div>
        </div>
      </div>
    </div>
    <!-- ./LISTADO DE PRODUCTOS -->


    <div class="row">
      <div class="col-md-12">
        <div class="card">
          <div class="card-block">
            <h5><?= lang('users title banking_pay')?></h5>
            <p class="text-center">
              <img src="<?= base_url('uploads/banking_pay.jpg') ?>" />
              <br />

              <a class="btn btn-info" style="margin-top:2em" href="<?= site_url("login"); ?>">
                <?= $this->lang->line("return_label"); ?>
              </a>
            </p>
          </div>
        </div>
      </div>
    </div>
  </div>
</div>
</div>