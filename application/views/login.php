<?php defined('BASEPATH') OR exit('No direct script access allowed'); ?>



<div themebg-pattern="theme4">

  <!-- Pre-loader start -->

  <div class="theme-loader">

    <div class="loader-track">

      <div class="preloader-wrapper">

        <div class="spinner-layer spinner-blue">

          <div class="circle-clipper left">

            <div class="circle"></div>

          </div>

          <div class="gap-patch">

            <div class="circle"></div>

          </div>

          <div class="circle-clipper right">

            <div class="circle"></div>

          </div>

        </div>

        <div class="spinner-layer spinner-red">

          <div class="circle-clipper left">

            <div class="circle"></div>

          </div>

          <div class="gap-patch">

            <div class="circle"></div>

          </div>

          <div class="circle-clipper right">

            <div class="circle"></div>

          </div>

        </div>



        <div class="spinner-layer spinner-yellow">

          <div class="circle-clipper left">

            <div class="circle"></div>

          </div>

          <div class="gap-patch">

            <div class="circle"></div>

          </div>

          <div class="circle-clipper right">

            <div class="circle"></div>

          </div>

        </div>



        <div class="spinner-layer spinner-green">

          <div class="circle-clipper left">

            <div class="circle"></div>

          </div>

          <div class="gap-patch">

            <div class="circle"></div>

          </div>

          <div class="circle-clipper right">

            <div class="circle"></div>

          </div>

        </div>

      </div>

    </div>

  </div>

  <!-- Pre-loader end -->



  <div id="pcoded" class="pcoded">

    <!-- Main-body start -->

    <section class="login-block">

      <!-- Container-fluid starts -->

      <div class="container-fluid">

        <div class="row">

          <div class="col-sm-12">

            <!-- Authentication card start -->

            <?php // System messages ?>

            <?php if ($this->session->flashdata('message')) : ?>

            <div class="alert alert-success alert-dismissable">

              <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>

              <?php echo $this->session->flashdata('message'); ?>

            </div>

            <?php elseif ($this->session->flashdata('error')) : ?>

            <div class="alert alert-danger alert-dismissable">

              <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>

              <?php echo $this->session->flashdata('error'); ?>

            </div>

            <?php elseif (validation_errors()) : ?>

            <div class="alert alert-danger alert-dismissable">

              <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>

              <?php echo validation_errors(); ?>

            </div>

            <?php elseif ($this->error) : ?>

            <div class="alert alert-danger alert-dismissable">

              <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>

              <?php echo $this->error; ?>

            </div>

            <?php endif; ?>



            <?php echo form_open('', array('class'=>'md-float-material form-material m-t-20 m-b-40','data-parsley-validate'=>'')); ?>

            <div class="text-center">

              <img width="200" src="/themes/<?= $this->settings->theme ?>/images/logo.png" alt="logo">

              <!-- <img src="/themes/able/images/logo.png" alt="logo.png" style="max-width:200px;"> -->

            </div>



            <div class="auth-box card">

              <div class="card-block">

                <div class="row m-b-20">

                  <div class="col-md-12">

                    <h3 class="text-center txt-primary"><?= $page_title ?></h3>

                  </div>

                </div>



                <div class="form-group form-primary form-static-label">

                  <?php echo form_input(array('name'=>'username', 'id'=>'username', 'required'=>'', 'class'=>'form-control', 'maxlength'=>256)); ?>

                  <!-- 'placeholder'=>lang('users input username'), -->

                  <span class="form-bar"></span>

                  <?php echo form_label(lang('users input username'), 'username', array('class' => 'float-label'));?>

                </div>

                <div class="form-group form-primary form-static-label">

                  <?php echo form_password(array('name'=>'password', 'id'=>'password', 'required'=>'', 'class'=>'form-control', 'maxlength'=>72, 'autocomplete'=>'off')); ?>

                  <!-- 'placeholder'=>lang('users input password'), -->

                  <span class="form-bar"></span>

                  <?php echo form_label(lang('users input password'), 'password', array('class' => 'float-label'));?>

                </div>

                <?php if(true): ?>

                <div class="form-group form-primary" style="padding-left: 22px;margin-bottom: 18px;">

                  <div class="g-recaptcha" data-sitekey="6Lcvaa0UAAAAAOcf3ZDNEQhrovpknWQb-q3ui_H9"></div>

                </div>

                <?php endif; ?>

                <div class="row m-t-25 text-left">

                  <div class="col-12">

                    <div class="forgot-phone text-right float-right">

                      <a class="text-right f-w-600"

                        href="<?php echo base_url('forgot'); ?>"><?php echo lang('users link forgot_password'); ?></a>

                    </div>

                  </div>

                </div>

                <div class="row m-t-30">

                  <div class="col-md-12">

                    <?php echo form_submit(array('name'=>'submit', 'class'=>'btn btn-info btn-md btn-block waves-effect text-center m-b-20'), lang('core button login')); ?>

                  </div>

                </div>



                <p class="text-inverse text-left" style="display:none"><?php echo lang('users title new_site')   ?>

                  <a

                    href="<?php echo base_url('register'); ?>"><b><?php echo lang('users link register_account'); ?></b></a>

                </p>

              </div>

              <!-- end of card-block -->

            </div>

            <!-- end of auth-box card -->

            <?php echo form_close(); ?>

            <!-- end of form -->

          </div>

          <!-- end of col-sm-12 -->

        </div>

        <!-- end of row -->

      </div>

      <!-- end of container-fluid -->

    </section>

    <!-- end of Main-body -->

    <div class="footer">

      <p class="text-center m-b-0">Copyright &copy; 2019, All rights reserved.</p>

    </div>

  </div>

  <!-- end of pcoded -->

</div>
