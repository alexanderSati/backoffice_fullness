<?php
/**
 * Created by PhpStorm.
 * User: Igniweb038
 * Date: 07/02/17
 * Time: 08:37
 */ ?>

<!-- sidebar menu -->
<nav class="pcoded-navbar">
  <div class="pcoded-inner-navbar main-menu">
    <div class="">
      <div class="main-menu-header">


        <img src="/uploads/<?= ($this->user['image'] != "") ? $this->user['image'] : 'img.jpg' ?>" alt="..."
          class="img-menu-user img-radius" style="height: 50px;width: 50px;">
        <div class="user-details">
          <p id="more-details"><?= $this->user['username'] ?><i class="feather icon-chevron-down m-l-10"></i></p>
        </div>
      </div>
      <div class="main-menu-content">
        <ul>
          <li class="more-details">
            <a href="<?php echo base_url('user/profile'); ?>">
              <i class="feather icon-user"></i><?php echo lang('core button profile'); ?>
            </a>
            <?php if ($this->user['is_admin']) { ?>
            <a href="<?php echo base_url('/admin/settings'); ?>">
              <i class="feather icon-settings"></i><?php echo lang('admin button settings'); ?>
            </a>
            <?php } ?>
            <a href="<?php echo base_url('logout'); ?>">
              <i class="feather icon-log-out"></i><?php echo lang('core button logout'); ?>
            </a>
          </li>
        </ul>
      </div>
    </div>

    <?php echo $this->multi_menu->get_menu(); ?>
    <?php if ($this->user['profile'] == 'user' && !is_preferencial_user()) : ?>
    <ul class="pcoded-item pcoded-left-item" item-border="true" item-border-style="solid" subitem-border="true">
      <li>
        <a href="<?php echo base_url('user/create'); ?>" class="waves-effect waves-dark btn btn-success sign-friend"
          title="<?= lang("sign-friend") ?>">
          <span class="pcoded-micon">
            <i class="fa fa-user-plus"></i>
          </span>
          <span class="pcoded-mtext"><?= lang("sign-friend") ?></span>
        </a>
      </li>
    </ul>
    <?php endif; ?>

  </div>
</nav>
<!-- /sidebar menu -->