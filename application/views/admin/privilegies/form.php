<?php defined('BASEPATH') OR exit('No direct script access allowed'); ?>
<div class="card px-3">
    <div class="card-header">
        <h4><?= $page_title ?></h4>                
    </div>
    <div class="card-block">
                    
        <div class="card-content privilegies">

            <?php echo form_open('', array('role'=>'form')); ?>
                      
                <ul id="myTab" class="nav nav-tabs md-tabs" role="tablist">
                    <?php foreach ($profiles as $key_profile => $profile) { ?>
                        <li class="nav-item">
                            <a class="<?= ($key_profile == 0) ? "nav-link active" : "nav-link" ; ?>" data-toggle="tab" href="#tab_content_<?= $profile['profile_key'] ?>" id="<?= $profile['profile_key'] ?>-tab"
                                role="tab"><?= $profile['profile_name'] ?> <i class="fa fa-expeditedssl"></i></a>
                                <div class="slide"></div>
                        </li>                        
                    <?php } ?>
                </ul>
                <div id="myTabContent" class="tab-content card-block">
                    <?php foreach ($profiles as $key_profile => $profile) { ?>
                        <div role="tabpanel" class="tab-pane <?= ($key_profile == 0) ? "active" : "" ; ?>"
                                id="tab_content_<?= $profile['profile_key'] ?>" aria-labelledby="profile-tab">
                            <div class="dt-responsive table-responsive">                                
                                <table id="datatable-responsive" class="table table-striped table-bordered nowrap">
                                    <thead>
                                    <tr>
                                        <th><?= lang("module") ?></th>
                                        <th><?= lang("section") ?></th>
                                        <th><?= lang("actions") ?></th>
                                    </tr>
                                    </thead>
                                    <tbody>
                                        
                                        <?php foreach ($privilegies as $privilegy) { ?>
                                            <tr>
                                                <td><?= $privilegy['class'] ?></td>
                                                <td><?= $privilegy['method'] ?></td>
                                                <td>
                                                    <div class="checkbox-zoom zoom-primary">
                                                        <label>                                                   
                                                            <?php echo form_checkbox(array('name'=>"privilegies[". $privilegy['id'] ."][read][]", 'id'=>"privilegies[". $privilegy['id'] ."][read][]", 'value'=>$profile['profile_key'], 'checked'=>(( isset($privilegy['read']) && in_array($profile['profile_key'], $privilegy['exp_read'])) ? 'checked' : FALSE))); ?>                                                        
                                                            <span class="cr"><i class="cr-icon icofont icofont-ui-check txt-primary"></i></span>
                                                            <span class="backspace"><?= lang("read") ?></span>
                                                        </label>
                                                        <label>
                                                            <?php echo form_checkbox(array('name'=>"privilegies[". $privilegy['id'] ."][insert][]", 'id'=>"privilegies[". $privilegy['id'] ."][insert][]", 'value'=>$profile['profile_key'], 'checked'=>(( isset($privilegy['insert']) && in_array($profile['profile_key'], $privilegy['exp_insert']) ) ? 'checked' : FALSE))); ?>                                                        
                                                            <span class="cr"><i class="cr-icon icofont icofont-ui-check txt-primary"></i></span>
                                                            <span class="backspace"><?= lang("insert") ?></span>
                                                        </label>
                                                        <label>
                                                            <?php echo form_checkbox(array('name'=>"privilegies[". $privilegy['id'] ."][update][]", 'id'=>"privilegies[". $privilegy['id'] ."][update][]", 'value'=>$profile['profile_key'], 'checked'=>(( isset($privilegy['update']) && in_array($profile['profile_key'], $privilegy['exp_update']) ) ? 'checked' : FALSE))); ?>                                                        
                                                            <span class="cr"><i class="cr-icon icofont icofont-ui-check txt-primary"></i></span>
                                                            <span class="backspace"><?= lang("update") ?></span>
                                                        </label>
                                                        <label>
                                                            <?php echo form_checkbox(array('name'=>"privilegies[". $privilegy['id'] ."][delete][]", 'id'=>"privilegies[". $privilegy['id'] ."][delete][]", 'value'=>$profile['profile_key'], 'checked'=>(( isset($privilegy['delete']) && in_array($profile['profile_key'], $privilegy['exp_delete']) ) ? 'checked' : FALSE))); ?>                                                        
                                                            <span class="cr"><i class="cr-icon icofont icofont-ui-check txt-primary"></i></span>
                                                            <span><?= lang("delete") ?></span>
                                                        </label>
                                                    </div>
                                                </td>
                                            </tr>
                                        <?php } ?>
                                    </tbody>
                                </table>
                            </div>
                        </div>
                    <?php } ?>
                </div>
           
            <?php if (is_allowed('update')){ ?>
                <div class="row text-left">
                    <div class="form-group col-sm-12 text-center">
                        <a class="btn btn-primary m-r-20" href="<?php echo $cancel_url; ?>"><?php echo lang('core button cancel'); ?></a>
                        <button type="submit" name="submit" class="btn btn-success"><span class="fa fa-check-square-o"></span> <?php echo lang('core button save'); ?></button>
                    </div>
                </div>
            <?php } ?>

            <div class="row"><br /></div>

            <?= form_hidden('save',true) ?>

            <?php echo form_close(); ?>


        </div>
    
    </div>
</div>