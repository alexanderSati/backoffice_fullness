<div class="card px-3">
    <div class="card-header">       
        <!-- <div class="icon"><i class="fa fa-bitcoin"></i></div> -->
        <h4 class="text-left"><?= lang('trading_history') ?>
        <p class="text-center f-18"><?= lang('total_profit') ?>: $&nbsp;<?= round(array_sum(array_column($rows,'profit_usd')),2) ?>&nbsp;PEN</p></h4>
    </div>
    <div class="card-block">
                
        <div class="card-content">
            <div class="dt-responsive table-responsive">
                <table id="order-table" class="table table-striped table-bordered nowrap">
                    <thead>
                        <tr>
                            <th><?= lang('date') ?></th>
                            <th><?= lang('total_usd') ?></th>
                            <th><?= lang('total_btc') ?></th>
                            <th><?= lang('n_transactions') ?></th>
                        </tr>
                    </thead>
                    <tbody>
                    <?php foreach ($rows as $row) { ?>
                        <tr>
                            <td><?= $row['date'] ?></td>
                            <td><?= round($row['profit_usd'],2) ?></td>
                            <td><?= number_format($row['profit_btc'],8) ?></td>
                            <td><?= $row['amount'] ?></td>
                        </tr>
                    <?php } ?>
                    </tbody>
                </table>
            
            </div>

        </div>
            
        
   
    </div>
</div>