<?php defined('BASEPATH') OR exit('No direct script access allowed'); ?>

<?php $lang = (isset($user['language']) ? $user['language'] : $this->config->item('language')); ?>
<div class="card px-3">
          <div class="card-header">
            <h2><?= $page_title ?></h2>
            <div class="clearfix"></div>
          </div>
          <div class="card-block px-sm-5 mx-sm-5">
            <?php echo form_open_multipart('', array('class' => 'form-horizontal form-label-left', 'data-parsley-validate' => '','role' => 'form')); ?>

            <?php // hidden id ?>

            <?php if (isset($id)) : ?>
            <?php echo form_hidden('id', $id); ?>
            <?php endif; ?>
            <!------SHIPPING TYPES---------->
            <div class="item form-group row">
              <div class="col-md-5 col-lg-2">
                <?php echo form_label(lang('users input shipping_types_id') . '*', 'shipping_types_id', array('class' => 'block')); ?>
              </div>
              <div class="col-md-7 col-lg-9">
                <?php echo form_dropdown('shipping_types_id', $shipping_types,(isset($shipping_detail['shipping_types_id']) ? $shipping_detail['shipping_types_id'] : ''), 'id="shipping_types_id" required="required" class=" select2_single form-control"'); ?>
              </div>
            </div>
            <!----NAME------------->
            <div class="form-group row">
              <div class="col-md-5 col-lg-2">
                <?php echo form_label(lang('users input name') . ' *', 'name', array('class' => 'block')); ?>
              </div>
              <div class="col-md-7 col-lg-9">
                <?php echo form_input(array('name' => 'name','required'=>'required', 'value' => set_value('name', (isset($shipping_detail['name']) ? $shipping_detail['name'] : '')), 'class' => 'form-control')); ?>
              </div>
            </div>
            <!----DESCRIPTION------>
            <div class="form-group row">
              <div class="col-md-5 col-lg-2">
                <?php echo form_label(lang('users input description') . ' *', 'description', array('class' => 'block')); ?>
              </div>
              <div class="col-md-7 col-lg-9">
                <?php echo form_input(array('name' => 'description','required'=>'required', 'value' => set_value('description', (isset($shipping_detail['description']) ? $shipping_detail['description'] : '')), 'class' => 'form-control')); ?>
              </div>
            </div>



            <div>
              <hr />
            </div>

            <div class="form-group row text-center">
              <div class="col-md-12 col-md-offset-1 col-xs-12">
                <a class="btn btn-primary"
                  href="<?php echo $cancel_url; ?>"><?php echo lang('core button cancel'); ?></a>
                <button type="submit" name="submit" class="btn btn-success"><span
                    class="glyphicon glyphicon-save"></span> <?php echo lang('core button save'); ?></button>
              </div>
            </div>

            <?php echo form_close(); ?>
          </div>

</div>