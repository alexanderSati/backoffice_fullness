<?php defined('BASEPATH') OR exit('No direct script access allowed'); ?>

<?php $lang = (isset($user['language']) ? $user['language'] : $this->config->item('language')); ?>
<div class="card px-3">
    <div class="card-header">
        <h4><?= $page_title ?></h4>                
    </div>
    <div class="card-block px-sm-5 mx-sm-5">
    
        <?php echo form_open_multipart('', array('id'=>'main', 'class' => 'form-horizontal form-label-left', 'data-parsley-validate' => '','role' => 'form', 'novalidate')); ?>

        <?php // hidden id ?>

        <?php if (isset($product_id)) : ?>
            <?php echo form_hidden('product_id', $product_id); ?>
        <?php endif; ?>

        <div class="form-group row">
            <?php echo form_label(lang('users input code') . ' *', 'code', array('class' => 'col-sm-3 col-form-label')); ?>                    
            <div class="col-sm-9">
                <?php echo form_input(array('name' => 'code','required'=>'required', 'value' => set_value('code', (isset($product['code']) ? $product['code'] : '')), 'class' => 'form-control')); ?>
            </div>
        </div>

        <div class="form-group row">
            <?php echo form_label(lang('users input product_name') . ' *', 'product_name', array('class' => 'col-sm-3 col-form-label')); ?>
            <div class="col-sm-9">
                <?php echo form_input(array('name' => 'product_name','required'=>'required', 'value' => set_value('product_name', (isset($product['product_name']) ? $product['product_name'] : '')), 'class' => 'form-control')); ?>
            </div>
        </div>

        <!-------PRICE CLIENT------------->
        <div class="form-group row">
            <?php echo form_label(lang('users input product_price') . ' *', 'product_price', array('class' => 'col-sm-3 col-form-label')); ?>
            <div class="col-sm-9">
                <?php echo form_input(array('name' => 'product_price','required'=>'required', 'value' => set_value('product_price', (isset($product['product_price']) ? $product['product_price'] : '')), 'class' => 'form-control')); ?>
            </div>
        </div>
        <!-------PRICE DISTRIBUTO--------->
        <div class="form-group row">
            <?php echo form_label(lang('users input product_price_distributor') . ' *', 'product_price_distributor', array('class' => 'col-sm-3 col-form-label')); ?>
            <div class="col-sm-9">                    
                <?php echo form_input(array('name' => 'product_price_distributor','required'=>'required', 'value' => set_value('product_price_distributor', (isset($product['product_price_distributor']) ? $product['product_price_distributor'] : '')), 'class' => 'form-control')); ?>
            </div>
        </div>

        <!------RESIDUAL POINTS----------->
        <div class="form-group row">
            <?php echo form_label(lang('users input residual_points') . ' *', 'residual_points', array('class' => 'col-sm-3 col-form-label')); ?>
            <div class="col-sm-9">    
                <?php echo form_input(array('name' => 'residual_points','required'=>'required', 'value' => set_value('residual_points', (isset($product['residual_points']) ? $product['residual_points'] : '')), 'class' => 'form-control')); ?>
            </div>                                        
        </div>

        <!-----BINARY PRODUCT------------->
        <div class="form-group row">
            <?php echo form_label(lang('users input binary_points') . ' *', 'binary_points', array('class' => 'col-sm-3 col-form-label')); ?>
            <div class="col-sm-9">
                <?php echo form_input(array('name' => 'binary_points','required'=>'required', 'value' => set_value('binary_points', (isset($product['binary_points']) ? $product['binary_points'] : '')), 'class' => 'form-control')); ?>
            </div>
        </div>

        <!-----STOCK--------------------->
        <div class="form-group row">
            <?php echo form_label(lang('users input stock_quantity') . ' *', 'stock_quantity', array('class' => 'col-sm-3 col-form-label')); ?>
            <div class="col-sm-9">
                <?php echo form_input(array('name' => 'stock_quantity','required'=>'required', 'value' => set_value('stock_quantity', (isset($product['stock_quantity']) ? $product['stock_quantity'] : '')), 'class' => 'form-control')); ?>
            </div>
        </div>

        <!------CATEGORY---------->
        <div class="form-group row">
            <?php echo form_label(lang('users input category_id') . '*', 'category_id', array('class' => 'col-sm-3 col-form-label')); ?>
            <div class="col-sm-9">
                <?php echo form_dropdown('category_id', $categories,(isset($product['category_id']) ? $product['category_id'] : ''), 'id="category_id" required="required" class="form-control"'); ?>
            </div>
        </div>

        <!-------IMAGE-------->
        <div class="form-group row">
            <?php echo form_label(lang('users input image') . ' *' , 'image', array('class' => 'col-sm-3 col-form-label')); ?>
            <div class="col-sm-9">
                <?php echo form_upload(array('name' => 'image', 'id'=>'image', 'class' => 'form-control')); ?>
            </div>
        </div>

        <div><hr/></div>

        <div class="form-group row text-center">
            <div class="col-md-12 col-md-offset-1 col-xs-12">
                <a class="btn btn-primary" href="<?php echo $cancel_url; ?>"><?php echo lang('core button cancel'); ?></a>
                <button type="submit" name="submit" class="btn btn-success"><span
                        class="fa fa-check-square-o"></span> <?php echo lang('core button save'); ?></button>
            </div>
        </div>

        <?php echo form_close(); ?>
    </div>       
</div>
