<?php defined('BASEPATH') OR exit('No direct script access allowed'); ?>

<?php $lang = (isset($user['language']) ? $user['language'] : $this->config->item('language')); ?>
<div class="card px-3">

            <div class="card-header">
                <h2><?= $page_title ?></h2>
                <div class="clearfix"></div>
            </div>
            <div class="card-block px-sm-5 mx-sm-5">
                <?php echo form_open_multipart('', array('class' => 'form-horizontal form-label-left', 'data-parsley-validate' => '','role' => 'form')); ?>

                <?php // hidden id ?>

                <?php if (isset($category_id)) : ?>
                    <?php echo form_hidden('category_id', $category_id); ?>
                <?php endif; ?>
                <br>               
                <div class="form-group row">
                    <?php echo form_label(lang('users input name') . ' *', 'name', array('class' => 'col-sm-2 col-form-label')); ?>
                    <div class="col-sm-9">
                        <?php echo form_input(array('name' => 'name','required'=>'required', 'value' => set_value('name', (isset($category['name']) ? $category['name'] : '')), 'class' => 'form-control')); ?>
                    </div>
                </div>
                <div class="form-group row">
                    <?php echo form_label(lang('users input description') . ' *', 'description', array('class' => 'col-sm-2 col-form-label')); ?>
                    <div class="col-sm-9">
                        <?php echo form_textarea(array('name' => 'description','rows' =>4 ,'col' => 40,'required'=>'required', 'value' => set_value('description', (isset($category['description']) ? $category['description'] : '')), 'class' => 'form-control')); ?>
                    </div>
                </div>

                <div><hr/></div>

                <div class="form-group row text-center">
                    <div class="col-md-6 col-md-offset-1 col-xs-12">
                        <a class="btn btn-primary" href="<?php echo $cancel_url; ?>"><?php echo lang('core button cancel'); ?></a>
                        <button type="submit" name="submit" class="btn btn-success"><span
                                class="glyphicon glyphicon-save"></span> <?php echo lang('core button save'); ?></button>
                    </div>
                </div>

                <?php echo form_close(); ?>
            </div>
</div>
