<?php defined('BASEPATH') OR exit('No direct script access allowed');?>

<?php $lang = (isset($user['language']) ? $user['language'] : $this->config->item('language')); ?>

<div class="card px-3">
          <div class="card-header">
            <h2><?= $page_title ?></h2>
            <div class="clearfix"></div>
          </div>
          <br>
          <div class="card-block px-sm-5 mx-sm-5">
            <?php echo form_open_multipart('', array('class' => 'form-horizontal form-label-left', 'data-parsley-validate' => '','role' => 'form')); ?>

            <?php // hidden id ?>

            <?php if (isset($id)) : ?>
            <?php echo form_hidden('id', $id); ?>
            <?php endif; ?>

            <!------SHIPPING DATAILS---------->
            <div class="item form-group row">
              <div class="col-md-5 col-lg-2">
                <?php echo form_label(lang('users input shiping_details_id') . '*', 'shiping_details_id', array('class' => 'block')); ?>
              </div>

              <div class="col-md-7 col-lg-9">
                <select id="shiping_details_id" name="shiping_details_id" required="required"
                    class="select2_single form-control"
                    <?= $edit == 1 ? 'disabled' : '' ?>
                  >
                  <?php foreach( $shipping_detail as $key => $sd ): ?>
                  <option value="<?= $sd["id"] ?>" data-id_user="<?= $sd["id_user"]; ?>" >
                    <?=$sd["name"]?>
                  </option>
                  <?php endforeach; ?>
                </select>
              </div>
            </div>

            <!------PRODUCTS---------->
            <div class="item form-group row">
              <div class="col-md-5 col-lg-2">
                <?php echo form_label(lang('users input product_name') . '*', 'product_id', array('class' => 'block')); ?>
              </div>
              <div class="col-md-7 col-lg-9">
                <?php if ($edit == 1) {           ?>
                <?php echo form_dropdown('product_id', $products,(isset($product_quantity['product_id']) ? $product_quantity['product_id'] : ''), array('id' => 'product_id',  'required' => 'required', 'class' => 'select2_single form-control', 'disabled' => true)); ?>
                <?php }else {?>
                <?php echo form_dropdown('product_id', $products,(isset($product_quantity['product_id']) ? $product_quantity['product_id'] : ''), array('id' => 'product_id',  'required' => 'required', 'class' => 'select2_single form-control')); ?>
                <?php }?>
              </div>
            </div>

            
            <!----QUANTITY------------->
            <div class="form-group row">
              <div class="col-md-5 col-lg-2">
                <?php echo form_label(lang('users input quantity') . ' *', 'quantity', array('class' => 'block')); ?>
              </div>
              <div class="col-md-7 col-lg-9">
                <?php echo form_input(array('name' => 'quantity','required'=>'required','type' => 'number' , 'value' => set_value('quantity', (isset($product_quantity['quantity']) ? $product_quantity['quantity'] : '')), 'class' => 'form-control')); ?>
              </div>
            </div>

            <div>
              <hr />
            </div>

            <div class="form-group row text-center">
              <div class="col-md-12 col-md-offset-1 col-xs-12">
                <a class="btn btn-primary"
                  href="<?php echo $cancel_url; ?>"><?php echo lang('core button cancel'); ?></a>
                <button type="submit" name="submit" class="btn btn-success"><span
                    class="glyphicon glyphicon-save"></span>
                  <?php echo lang('core button save'); ?></button>
              </div>
            </div>

            <?php echo form_close(); ?>
            <input type="hidden" name="executive_id_user" id="executive_id_user" value="<?= $this->user["id"]; ?>" />
          </div>
</div>

<script>
console.log("#shiping_details_id option[data-id_user='" + $("#executive_id_user").val() + "']");
$(document).ready(function() {

  if (
    $("#shiping_details_id option[data-id_user='" + $("#executive_id_user").val() + "']").length > 0) {

    $("#shiping_details_id").val($("#shiping_details_id option[data-id_user='" + $("#executive_id_user").val() +
        "']")
      .val()).trigger("change");
    setTimeout(() => {
      $("#shiping_details_id").select2("enable", false)
    }, 500);

  }
});
</script>