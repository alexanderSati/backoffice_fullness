<?php defined('BASEPATH') OR exit('No direct script access allowed'); ?>

<div class="card px-3">
    <div class="card-header">
        <h4><?= $page_title ?></h4>
    </div>
    <div class="col-md-12 text-left m-15">
        <a class="btn btn-info" href="<?php echo base_url('admin/products/add'); ?>" title="<?php echo lang('users tooltip add_new_product') ?>" data-toggle="tooltip"><i class="fa fa-plus"></i><?php echo lang('users button add_new_product'); ?></a>
    </div>
    <div class="card-block">
    <div class="row">
        <div class="col-md-12 col-sm-12 col-xs-12">
                    
            <div class="card-content">
                <div class="table-responsive"> 
                    <table class="table table-striped table-bordered nowrap dataTable no-footer dtr-inline">
                        <thead>

                        <?php // sortable headers ?>
                        <tr>
                            <td align="center">
                                <a href="<?php echo current_url(); ?>?sort=product_id&dir=<?php echo (($dir == 'asc' ) ? 'desc' : 'asc'); ?>&limit=<?php echo $limit; ?>&offset=<?php echo $offset; ?><?php echo $filter; ?>"><?php echo lang('users col user_id'); ?></a>
                                <?php if ($sort == 'product_id') : ?><span class="glyphicon glyphicon-arrow-<?php echo (($dir == 'asc') ? 'up' : 'down'); ?>"></span><?php endif; ?>
                            </td>

                            <td>
                                <a href="<?php echo current_url(); ?>?sort=code&dir=<?php echo (($dir == 'asc' ) ? 'desc' : 'asc'); ?>&limit=<?php echo $limit; ?>&offset=<?php echo $offset; ?><?php echo $filter; ?>"><?php echo lang('users col code'); ?></a>
                                <?php if ($sort == 'code') : ?><span class="glyphicon glyphicon-arrow-<?php echo (($dir == 'asc') ? 'up' : 'down'); ?>"></span><?php endif; ?>
                            </td>

                            <td>
                                <a href="<?php echo current_url(); ?>?sort=product_name&dir=<?php echo (($dir == 'asc' ) ? 'desc' : 'asc'); ?>&limit=<?php echo $limit; ?>&offset=<?php echo $offset; ?><?php echo $filter; ?>"><?php echo lang('users col product_name'); ?></a>
                                <?php if ($sort == 'product_name') : ?><span class="glyphicon glyphicon-arrow-<?php echo (($dir == 'asc') ? 'up' : 'down'); ?>"></span><?php endif; ?>
                            </td>
                            <td>
                                <a href="<?php echo current_url(); ?>?sort=product_price&dir=<?php echo (($dir == 'asc' ) ? 'desc' : 'asc'); ?>&limit=<?php echo $limit; ?>&offset=<?php echo $offset; ?><?php echo $filter; ?>"><?php echo lang('users col product_price'); ?></a>
                                <?php if ($sort == 'product_price') : ?><span class="glyphicon glyphicon-arrow-<?php echo (($dir == 'asc') ? 'up' : 'down'); ?>"></span><?php endif; ?>
                            </td>

                            <td>
                                <a href="<?php echo current_url(); ?>?sort=product_price_distributor&dir=<?php echo (($dir == 'asc' ) ? 'desc' : 'asc'); ?>&limit=<?php echo $limit; ?>&offset=<?php echo $offset; ?><?php echo $filter; ?>"><?php echo lang('users col product_price_distributor'); ?></a>
                                <?php if ($sort == 'product_price_distributor') : ?><span class="glyphicon glyphicon-arrow-<?php echo (($dir == 'asc') ? 'up' : 'down'); ?>"></span><?php endif; ?>
                            </td>

                            <td>
                            <?= lang("users title binary_points"); ?>
                            </td>

                            <td>
                            <?= lang("users title residual_points"); ?>
                            </td>
                                
            
                            <td align="center">
                                <?php echo lang('admin col actions'); ?>
                            </td>
                        </tr>

                        <?php // search filters ?>
                        <tr>
                            <?php echo form_open("{$this_url}?sort={$sort}&dir={$dir}&limit={$limit}&offset=0{$filter}", array('role'=>'form', 'id'=>"filters")); ?>
                            <th>
                            </th>
                            <th <?php echo ((isset($filters['code'])) ? ' class="has-success"' : ''); ?>>
                                <?php echo form_input(array('name'=>'code', 'id'=>'code', 'class'=>'form-control input-sm', 'placeholder'=>lang('users input code'), 'value'=>set_value('code', ((isset($filters['code'])) ? $filters['code'] : '')))); ?>
                            </th>
                            <th <?php echo ((isset($filters['product_name'])) ? ' class="has-success"' : ''); ?>>
                                <?php echo form_input(array('name'=>'product_name', 'id'=>'product_name', 'class'=>'form-control input-sm', 'placeholder'=>lang('users input product_name'), 'value'=>set_value('product_name', ((isset($filters['product_name'])) ? $filters['product_name'] : '')))); ?>
                            </th>
                            <th <?php echo ((isset($filters['product_price'])) ? ' class="has-success"' : ''); ?>>
                                <?php echo form_input(array('name'=>'product_price', 'id'=>'product_price', 'class'=>'form-control input-sm', 'placeholder'=>lang('users input product_price'), 'value'=>set_value('product_price', ((isset($filters['product_price'])) ? $filters['product_price'] : '')))); ?>
                            </th>
                            <th <?php echo ((isset($filters['product_price_distributor'])) ? ' class="has-success"' : ''); ?>>
                                <?php echo form_input(array('name'=>'product_price_distributor', 'id'=>'product_price_distributor', 'class'=>'form-control input-sm', 'placeholder'=>lang('users input product_price_distributor'), 'value'=>set_value('product_price_distributor', ((isset($filters['product_price_distributor'])) ? $filters['product_price_distributor'] : '')))); ?>
                            </th>

                            <th>
                                
                            </th>
                            <th>
                            </th>
                            <th>
                                <div class="text-center">
                                    <a href="<?php echo $this_url; ?>" class="btn btn-danger tooltips" data-toggle="tooltip" title="<?php echo lang('admin tooltip filter_reset'); ?>">
                                        <span class="glyphicon glyphicon-refresh"></span> <?php echo lang('core button reset'); ?>
                                    </a>
                                    <button type="submit" name="submit" value="<?php echo lang('core button filter'); ?>" class="btn btn-info " data-toggle="tooltip" title="<?php echo lang('admin tooltip filter'); ?>">
                                        <span class="glyphicon glyphicon-filter"></span> <?php echo lang('core button filter'); ?>
                                    </button>
                                </div>
                            </th>
                            <?php echo form_close(); ?>
                        </tr>

                        </thead>
                        <tbody>

                        <?php // data rows ?>
                        <?php if ($total) : ?>
                            <?php foreach ($products as $product) : ?>
                                <tr>
                                    <td align="center" <?php echo (($sort == 'product_id') ? ' class="sorted"' : ''); ?>>
                                        <?php echo $product['product_id']; ?>
                                    </td>
                                    <td <?php echo (($sort == 'code') ? ' class="sorted"' : ''); ?>>
                                        <?php echo $product['code']; ?>
                                    </td>
                                    <td <?php echo (($sort == 'product_name') ? ' class="sorted"' : ''); ?>>
                                        <?php echo $product['product_name']; ?>
                                    </td>
                                    <td <?php echo (($sort == 'product_price') ? ' class="sorted"' : ''); ?>>
                                        <?php echo $product['product_price']; ?>
                                    </td>
                                    <td <?php echo (($sort == 'product_price_distributor') ? ' class="sorted"' : ''); ?>>
                                        <?php echo $product['product_price_distributor']; ?>
                                    </td>
                                    <td>
                                        <?= $product["binary_points"]; ?>
                                    </td>
                                    <td>
                                        <?= $product["residual_points"]; ?>
                                    </td>
                                    <td align="center">
                                        
                                        <a href="<?php echo $this_url; ?>/edit/<?php echo $product['product_id']; ?>" class="btn waves-effect waves-light btn-warning btn-icon m-r-20" title="<?php echo lang('admin button edit'); ?>"><i class="fa fa-pencil"></i></a>

                                        <?php if($product['deleted'] == '0') {?>                                    
                                            <button class="btn waves-effect waves-light btn-danger btn-icon"  title="<?php echo lang('admin button remove'); ?>" data-toggle="modal" data-target="#modal-default" onclick="modalData(0,<?php echo $product['product_id'];?>)">
                                                <i class="fa fa-times"></i>
                                            </button>
                                        
                                        <?php } elseif ($product['deleted'] == '1') {?>                                    
                                            <button class="btn waves-effect waves-light btn-success btn-icon"  title="<?php echo lang('admin button activate'); ?>" data-toggle="modal" data-target="#modal-default" onclick="modalData(1,<?php echo $product['product_id'];?>)">
                                                <i class="fa fa-check"></i>
                                            </button>                                        
                                        <?php } ?>                                    
                                    </td>                                
                                </tr>
                            <?php endforeach; ?>
                        <?php else : ?>
                            <tr>
                                <td colspan="7">
                                    <?php echo lang('core error no_results'); ?>
                                </td>
                            </tr>
                        <?php endif; ?>

                        </tbody>
                    </table>

                    <?php // list tools ?>
                    <div class="panel-footer">
                        <div class="row">
                            <div class="col-md-2 text-left">
                                <label><?php echo sprintf(lang('admin label rows'), $total); ?></label>
                            </div>
                            <div class="col-md-8 pagination justify-content-center">
                                <?php echo $pagination; ?>
                            </div>
                            <div class="col-md-2 text-right">
                                <?php if ($total) : ?>
                                    <a href="<?php echo $this_url; ?>/export?sort=<?php echo $sort; ?>&dir=<?php echo $dir; ?><?php echo $filter; ?>" class="btn btn-success tooltips" data-toggle="tooltip" title="<?php echo lang('admin tooltip csv_export'); ?>"><span class="glyphicon glyphicon-export"></span> <?php echo lang('admin button csv_export'); ?></a>
                                <?php endif; ?>
                            </div>
                        </div>
                    </div>
                </div>

                <?php // delete modal ?>
                <?php if ($total) : ?>
                    <?php foreach ($products as $product) : ?>
                        <div class="modal fade" id="modal-<?php echo $product['product_id']; ?>" tabindex="-1" role="dialog" aria-labelledby="modal-label-<?php echo $product['product_id']; ?>" aria-hidden="true">
                            <div class="modal-dialog">
                                <div class="modal-content">
                                    <div class="modal-header">
                                        <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                                        <h4 id="modal-label-<?php echo $product['product_id']; ?>"><?php echo lang('users title user_delete');  ?></h4>
                                    </div>
                                    <div class="modal-body">
                                        <p><?php echo sprintf(lang('users msg delete_confirm'), $product['name']); ?></p>
                                    </div>
                                    <div class="modal-footer">
                                        <button type="button" class="btn btn-default" data-dismiss="modal"><?php echo lang('core button cancel'); ?></button>
                                        <button type="button" class="btn btn-primary btn-delete-user" data-id="<?php echo $product['product_id']; ?>"><?php echo lang('admin button delete'); ?></button>
                                    </div>
                                </div>
                            </div>
                        </div>
                    <?php endforeach; ?>
                <?php endif; ?>
            </div>        
        </div>
    </div>

    <div class="modal fade" id="modal-default" style="display: none;">
      <div class="modal-dialog">
        <div class="modal-content">
          <div class="modal-header">              
            <h5 class="modal-title" id="modal-title"></h5>
            <button type="button" class="close" data-dismiss="modal" aria-label="Close" onclick="clean()">
                <span aria-hidden="true">×</span>
            </button>
          </div>
          <div class="modal-footer">
            <button type="button" class="btn btn-danger pull-left" data-dismiss="modal" onclick="clean()"><?php echo lang('users button cancel'); ?></button>
            <a href="" class="btn btn-success" id="product-deleted"><?php echo lang('users button confirm'); ?></a>
          </div>
        </div>
        <!-- /.modal-content -->
      </div>
      <!-- /.modal-dialog -->
     </div>
    </div>
</div>

<script>
    function modalData(type,id) {
        const a = document.getElementById('product-deleted');
        const title = document.getElementById('modal-title');
        if (type === 0) {
            a.href=`<?php echo $this_url; ?>/remove/${id}`;
            title.innerText = '<?php echo lang('users msg remove-sure')?>';
        }
        else {
            a.href=`<?php echo $this_url; ?>/activate/${id}`;
            title.innerText = '<?php echo lang('users msg activate-sure')?>';
        }
    }
    function clean() {
        const a = document.getElementById('product-deleted');
        const title = document.getElementById('modal-title');
        a.href = "";
        title.innerText = "";
               
    }
</script>
<style>
/**Tabla responsive **/
    @media screen and (max-width: 580px) {
     table {
       display: block;
       overflow-x: auto;
     }
}
</style>