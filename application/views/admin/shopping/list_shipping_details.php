<?php defined('BASEPATH') OR exit('No direct script access allowed');
$lang = (isset($user['language']) ? $user['language'] : $this->config->item('language')); ?>

<!-- [ page content ] start -->
<div class="card px-3">
    <div class="card-block">
        <!-- end of [ row line 8 ] -->
        <div class="row">
            

            <div class="col-md-12 col-sm-12 col-xs-12">
                <div class="card-header">
                    <h4><?= $page_title ?></h4>                    
                </div>
                <div style="margin: 1em 1em 1.5em 0;">
                        <a href="<?php echo base_url('admin/shipping/add'); ?>" class="btn btn-info" title="<?php echo lang('users tooltip add_new_shipping') ?>" data-toggle="tooltip" >  <i class="fa fa-plus"></i><?= lang('users button add_shipping_details') ?> </a>                </div>
                <div class="card-content">                   
                        <div class="dt-responsive table-responsive">
                            <table id="datatable-responsive" class="table table-striped table-bordered nowrap">
                                <thead>
                                <tr>
                                    <th><?= lang('users col user_id') ?></th>
                                    <th><?= lang('users col name') ?></th>
                                    <th><?= lang('users col description') ?></th>
                                    <th><?= lang('admin col actions') ?></th>
                                </tr>
                                </thead>
                                <tbody>
                                <?php if (isset($shipping_details)) {
                                    foreach ($shipping_details as $shipping) { ?>
                                        <tr>
                                            <td><?= $shipping['id'] ?></td>
                                            <td><?= $shipping['name'] ?></td>
                                            <td><?= $shipping['description'] ?></td>
                                            <td>
                                                <div class="text-center">
                                                    <div class="btn-group">
                                                        <a href="<?php echo $this_url; ?>/edit/<?php echo $shipping['id']; ?>" class="btn btn-warning btn-icon" title="<?php echo lang('admin button edit'); ?>"><i class="fa fa-pencil"></i></a>
                                                    </div>
                                                </div>
                                            </td>                                    
                                        </tr>
                                    <?php }
                                } ?>
                                </tbody>
                            </table>
                    </div>
                </div>
            </div>
        </div>
        <!-- end of [ row line 27 ] -->
    </div>
    <!-- end of card-block -->            
</div>
<!-- [ page content ] end -->