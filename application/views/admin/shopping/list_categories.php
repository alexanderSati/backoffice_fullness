<?php defined('BASEPATH') OR exit('No direct script access allowed'); ?>
<div class="card px-3">
    <div class="card-header">
        <h4><?= $page_title ?></h4>                    
    </div>
    <div class="col-md-12 text-left m-15">
        <a class="btn btn-info" href="<?php echo base_url('admin/categories/add'); ?>" title="<?php echo lang('users tooltip add_new_category') ?>" data-toggle="tooltip"><i class="fa fa-plus"></i><?php echo lang('users button add_new_category'); ?></a>
    </div>
    <div class="card-block">    
        <div class="col-md-12 col-sm-12 col-xs-12">
                
            <div class="card-content">
                <div class="table-responsive">        
                    <table class="table table-striped table-bordered nowrap dataTable no-footer dtr-inline">
                        <thead>
                            <?php // sortable headers ?>
                            <tr>
                                <td align="center">
                                    <a href="<?php echo current_url(); ?>?sort=category_id&dir=<?php echo (($dir == 'asc' ) ? 'desc' : 'asc'); ?>&limit=<?php echo $limit; ?>&offset=<?php echo $offset; ?><?php echo $filter; ?>"><?php echo lang('users col user_id'); ?></a>
                                    <?php if ($sort == 'category_id') : ?><span class="glyphicon glyphicon-arrow-<?php echo (($dir == 'asc') ? 'up' : 'down'); ?>"></span><?php endif; ?>
                                </td>
                                <td>
                                    <a href="<?php echo current_url(); ?>?sort=name&dir=<?php echo (($dir == 'asc' ) ? 'desc' : 'asc'); ?>&limit=<?php echo $limit; ?>&offset=<?php echo $offset; ?><?php echo $filter; ?>"><?php echo lang('users col name'); ?></a>
                                    <?php if ($sort == 'name') : ?><span class="glyphicon glyphicon-arrow-<?php echo (($dir == 'asc') ? 'up' : 'down'); ?>"></span><?php endif; ?>
                                </td>
                                <td>
                                    <a href="<?php echo current_url(); ?>?sort=description&dir=<?php echo (($dir == 'asc' ) ? 'desc' : 'asc'); ?>&limit=<?php echo $limit; ?>&offset=<?php echo $offset; ?><?php echo $filter; ?>"><?php echo lang('users col description'); ?></a>
                                    <?php if ($sort == 'description') : ?><span class="glyphicon glyphicon-arrow-<?php echo (($dir == 'asc') ? 'up' : 'down'); ?>"></span><?php endif; ?>
                                </td>                
                                <td align="center">
                                    <?php echo lang('admin col actions'); ?>
                                </td>
                            </tr>

                            <?php // search filters ?>
                            <tr>
                                <?php echo form_open("{$this_url}?sort={$sort}&dir={$dir}&limit={$limit}&offset=0{$filter}", array('role'=>'form', 'id'=>"filters")); ?>
                                <th></th>
                                <th<?php echo ((isset($filters['name'])) ? ' class="has-success"' : ''); ?>>
                                    <?php echo form_input(array('name'=>'name', 'id'=>'name', 'class'=>'form-control input-sm', 'placeholder'=>lang('users input name'), 'value'=>set_value('name', ((isset($filters['name'])) ? $filters['name'] : '')))); ?>
                                </th>
                                <th<?php echo ((isset($filters['description'])) ? ' class="has-success"' : ''); ?>>
                                    <?php echo form_input(array('name'=>'description', 'id'=>'description', 'class'=>'form-control input-sm', 'placeholder'=>lang('users input description'), 'value'=>set_value('description', ((isset($filters['description'])) ? $filters['description'] : '')))); ?>
                                </th>
                                <th>
                                    <div class="text-center">
                                        <a href="<?php echo $this_url; ?>" class="btn btn-danger" data-toggle="tooltip" title="<?php echo lang('admin tooltip filter_reset'); ?>"><span class="glyphicon glyphicon-refresh"></span> <?php echo lang('core button reset'); ?></a>
                                        <button type="submit" name="submit" value="<?php echo lang('core button filter'); ?>" class="btn btn-info" data-toggle="tooltip" title="<?php echo lang('admin tooltip filter'); ?>"><span class="glyphicon glyphicon-filter"></span> <?php echo lang('core button filter'); ?></button>
                                    </div>
                                </th>
                                <?php echo form_close(); ?>
                            </tr>
                        </thead>
                        <tbody>

                        <?php // data rows ?>
                        <?php if ($total) : ?>
                            <?php foreach ($categories as $category) : ?>
                                <tr>
                                    <td align="center" <?php echo (($sort == 'category_id') ? ' class="sorted"' : ''); ?>>
                                        <?php echo $category['category_id']; ?>
                                    </td>
                                    <td <?php echo (($sort == 'name') ? ' class="sorted"' : ''); ?>>
                                        <?php echo $category['name']; ?>
                                    </td>
                                    <td <?php echo (($sort == 'description') ? ' class="sorted"' : ''); ?>>
                                        <?php echo $category['description']; ?>
                                    </td>
                                    <td align="center">
                                        <a href="<?php echo $this_url; ?>/edit/<?php echo $category['category_id']; ?>" class="btn waves-effect waves-light btn-warning btn-icon" title="<?php echo lang('admin button edit'); ?>"><i class="fa fa-pencil"></i></a>                                        
                                    </td>
                                </tr>
                            <?php endforeach; ?>
                        <?php else : ?>
                            <tr>
                                <td colspan="7">
                                    <?php echo lang('core error no_results'); ?>
                                </td>
                            </tr>
                        <?php endif; ?>

                        </tbody>
                    </table>
                </div>

                <?php // list tools ?>                
                <div class="row">
                    <div class="col-md-2 text-left">
                        <label><?php echo sprintf(lang('admin label rows'), $total); ?></label>
                    </div>
                    <div class="col-md-8 pagination justify-content-center">
                        <?php echo $pagination; ?>
                    </div>
                    <div class="col-md-2 text-right">
                        <?php if ($total) : ?>
                            <a href="<?php echo $this_url; ?>/export?sort=<?php echo $sort; ?>&dir=<?php echo $dir; ?><?php echo $filter; ?>" class="btn btn-success tooltips" data-toggle="tooltip" title="<?php echo lang('admin tooltip csv_export'); ?>"><span class="glyphicon glyphicon-export"></span> <?php echo lang('admin button csv_export'); ?></a>
                        <?php endif; ?>
                    </div>
                </div>               
            </div>
            <!-- end of card-content -->

            <?php // delete modal ?>
            <?php if ($total) : ?>
                <?php foreach ($categories as $category) : ?>
                    <div class="modal fade" id="modal-<?php echo $category['category_id']; ?>" tabindex="-1" role="dialog" aria-labelledby="modal-label-<?php echo $category['category_id']; ?>" aria-hidden="true">
                        <div class="modal-dialog">
                            <div class="modal-content">
                                <div class="modal-header">
                                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                                    <h4 id="modal-label-<?php echo $category['category_id']; ?>"><?php echo lang('users title user_delete');  ?></h4>
                                </div>
                                <div class="modal-body">
                                    <p><?php echo sprintf(lang('users msg delete_confirm'), $category['name']); ?></p>
                                </div>
                                <div class="modal-footer">
                                    <button type="button" class="btn btn-default" data-dismiss="modal"><?php echo lang('core button cancel'); ?></button>
                                    <button type="button" class="btn btn-primary btn-delete-user" data-id="<?php echo $category['category_id']; ?>"><?php echo lang('admin button delete'); ?></button>
                                </div>
                            </div>
                        </div>
                    </div>
                <?php endforeach; ?>
            <?php endif; ?>               
            
        </div>    
    </div>
</div>

<style>
/**Tabla responsive **/
    @media screen and (max-width: 580px) {
     table {
       display: block;
       overflow-x: auto;
     }
}
</style>