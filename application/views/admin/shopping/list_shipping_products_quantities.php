<?php defined('BASEPATH') or exit('No direct script access allowed');
$lang = (isset($user['language']) ? $user['language'] : $this->config->item('language')); ?>

<!-- [ page content ] start -->
<div class="card px-3">
    <div class="card-block">
        <!-- end of [ row line 8 ] -->
        <div class="row">
            <div class="col-md-12 col-sm-12 col-xs-12">
                <div class="card-header">
                    <h4><?= $page_title ?></h4>
                </div>
                <div style="margin: 1em 1em 1.5em 0;">
                    <?php if (!$is_executive) { ?>
                        <a href="<?php echo $this_url; ?>/add" class="btn btn-info" title="<?php echo lang('users tooltip add_new_product_quantities') ?>" data-toggle="tooltip"> <i class="fa fa-plus"></i><?= lang('users button add_product_quantities') ?> </a> </div>
            <?php } ?>
            <div class="card-content">
                <div class="dt-responsive table-responsive">
                    <table id="order-table" class="table table-striped table-bordered nowrap">
                        <thead>
                            <tr>
                                <th><?= lang('users col user_id') ?></th>
                                <th><?= lang('users col product_name') ?></th>
                                <th><?= lang('users col name_details') ?></th>
                                <th><?= lang('users input residual_points'); ?></th>
                                <th><?= lang('users title binary_points'); ?></th>
                                <th><?= lang('users col quantities') ?></th>
                                <?php if (!$is_executive) { ?>
                                    <th><?= lang('admin col actions') ?></th>
                                <?php } ?>
                            </tr>
                        </thead>
                        <tbody>
                            <?php if (isset($product_quantities)) {
                                foreach ($product_quantities as $quantities) { ?>
                                    <tr>
                                        <td><?= $quantities['id'] ?></td>
                                        <td><?= $quantities['product_name'] ?></td>
                                        <td><?= $quantities['name'] ?></td>
                                        <td><?= intval($quantities['residual_points']) * $quantities['quantity'] ?></td>
                                        <td><?= intval($quantities['binary_points']) * $quantities['quantity'] ?></td>
                                        <td><?= $quantities['quantity'] ?></td>
                                        <?php if (!$is_executive) { ?>
                                            <td>
                                                <div class="text-center">
                                                    <div class="btn-group">
                                                        <a href="<?php echo $this_url; ?>/edit/<?php echo $quantities['id']; ?>" class="btn btn-warning btn-icon" title="<?php echo lang('admin button edit'); ?>"><i class="fa fa-pencil"></i></a>
                                                    </div>
                                                </div>
                                            </td>
                                        <?php } ?>
                                    </tr>
                            <?php }
                            } ?>
                        </tbody>
                    </table>
                    <?php // list tools 
                    ?>
                    <div class="panel-footer">
                        <div class="row">
                            <div class="col-md-2 text-left">
                                <label><?php echo sprintf(lang('admin label rows'), $total); ?></label>
                            </div>
                            <div class="col-md-8 pagination justify-content-center">
                                <?php echo $pagination; ?>
                            </div>
                            <div class="col-md-2 text-right">
                                <?php if ($product_quantities) : ?>
                                    <a href="<?php echo $this_url; ?>/export?sort=<?php echo $sort; ?>&dir=<?php echo $dir; ?><?php echo $filter; ?>" class="btn btn-success tooltips" data-toggle="tooltip" title="<?php echo lang('admin tooltip csv_export'); ?>"><span class="glyphicon glyphicon-export"></span> <?php echo lang('admin button csv_export'); ?></a>
                                <?php endif; ?>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            </div>
        </div>
        <!-- end of [ row line 27 ] -->
    </div>
    <!-- end of card-block -->


</div>


<!-- [ page content ] end -->