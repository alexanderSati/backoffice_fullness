<!-- nuevo tema -->
<div class="row">

  <!-- CART I -->
  <div class="col-md-6">
    <div class="card">
      <div class="card-block">
        <div class="row align-items-center">
          <div class="col-8">
            <h4 class="text-c-yellow"><?= $users_count ?></h4>
            <h6 class="text-muted m-b-0"></h6>
          </div>
          <div class="col-4 text-right">
            <i class="feather icon-users f-28"></i>
          </div>
        </div>
      </div>
      <div class="card-footer bg-c-yellow">
        <div class="row align-items-center">
          <div class="col-9">
            <p class="text-white m-b-0"><b><?= lang('d_users') ?></b></p>
          </div>
          <div class="col-3 text-right">
            <i class="feather icon-trending-up text-white f-16"></i>
          </div>
        </div>
      </div>
    </div>
  </div>
  <!-- CART I -->

  <!-- CART II -->
  <div class="col-md-6">
    <div class="card">
      <div class="card-block">
        <div class="row align-items-center">
          <div class="col-8">
            <h4 class="text-c-green"><?= count($pending_transactions); ?></h4>
            <h6 class="text-muted m-b-0"></h6>
          </div>
          <div class="col-4 text-right">
            <i class="feather icon-info f-28"></i>
          </div>
        </div>
      </div>
      <div class="card-footer bg-c-green">
        <div class="row align-items-center">
          <div class="col-9">
            <p class="text-white m-b-0"><b><?= lang('admin title pending transactions') ?></b></p>
          </div>
          <div class="col-3 text-right">
            <i class="feather icon-trending-up text-white f-16"></i>
          </div>
        </div>
      </div>
    </div>
  </div>
  <!-- CART II -->
</div>
<!-- ./nuevo tema -->


<!-- GRAFICO DE VENTA DE PLANES -->
<div class="row">
  <div class="col-lg-12 col-md-12">
    <div class="card px-3">
      <div class="card-header borderless">
        <h5><?= lang('daily_sale_plans') ?></h5>

        <div class="card-header-right">

        </div>
      </div>
      <div class="card-block">
        <div class="demo-container" style="height:280px">
          <div id="linear-chart" style="width:100%;height:250px;" class="demo-placeholder"></div>
        </div>
      </div>
    </div>
  </div>
</div>
<!-- ./GRAFICO DE VENTA DE PLANES -->

<!-- USUARIOS RECIENTES -->
<div class="card table-card px-3">
  <div class="card-header borderless ">
    <h5><?= lang('recent_users') ?></h5>
    <div class="card-header-right">
      <ul class="list-unstyled card-option">
        <li class="first-opt"><i class="feather icon-chevron-left open-card-option"></i></li>
        <li><i class="feather icon-maximize full-card"></i></li>
        <li><i class="feather icon-minus minimize-card"></i></li>
        <li><i class="feather icon-refresh-cw reload-card"></i></li>
        <li><i class="feather icon-trash close-card"></i></li>
        <li><i class="feather icon-chevron-left open-card-option"></i></li>
      </ul>
    </div>
  </div>
  <div class="card-block">
    <div class="table-responsive">
      <table class="table table-hover">
        <thead>
          <tr>
            <th>
              Pais
            </th>
            <th>Usuario</th>
            <th>Último ingreso</th>

          </tr>
        </thead>
        <tbody>
          <?php foreach($recents as $recent)  :?>
          <tr>
            <td>
              <div class="d-inline-block align-middle">
                <img
                  src="/themes/<?= $this->settings->theme ?>/images/countries/<?= ($recent['country'] !='') ? strtolower($recent['country']) : "none"?>.gif"
                  alt="user image" class="img-radius img-40 align-top m-r-15" style="height:40px">
                <div class="d-inline-block">
                  <h6><?= $countries[$recent['country']]?></h6>
                  <p class="text-muted m-b-0"><?=  $recent['country']; ?></p>
                </div>
              </div>
            </td>
            <td><?= ucfirst($recent['first_name'])." ".ucfirst($recent['last_name']) ?></td>
            <td><?= format_interval($recent['created']) ?></td>

          </tr>
          <?php endforeach;?>

        </tbody>
      </table>

    </div>
  </div>
</div>

<!-- TABLA DE PLANES -->
<div class="card px-3">
  <div class="card-block">
    <div class="dt-responsive table-responsive">
      <table id="order-table" class="table table-striped table-bordered nowrap">
        <thead>
          <tr>
            <th><?= lang('t_user') ?></th>
            <th><?= lang('code') ?></th>
            <th><?= lang('description') ?></th>
            <th><?= lang('date') ?></th>
            <th><?= lang('value_usd') ?></th>
            <th><?= lang('status') ?></th>
          </tr>
        </thead>
        <tbody>
          <?php foreach ($transactions as $llave => $transaction): ?>
          <tr>
            <td><?= $transaction['name_user'] ?></td>
            <td><?= $transaction['cod'] ?></td>
            <td><?= $transaction['description'] ?></td>
            <td><?= $transaction['date'] ?></td>
            <td><?= $transaction['usd'] ?></td>
            <td>
              <?php
              if ($transaction['status'] == "done") {
                  echo "<span class='pcoded-badge label label-success'>" . $transaction['status'] . "</span>";
              } elseif ($transaction['status'] == "pending" || $transaction['status'] == "waiting") {
                  echo "<span class='pcoded-badge label label-warning'>" . $transaction['status'] . "</span>";
              } else {
                  echo "<span class='pcoded-badge label label-danger'>" . $transaction['status'] . "</span>";
              }
              ?>
            </td>
          </tr>
          <?php endforeach;?>


        </tbody>

      </table>
    </div>
  </div>
</div>

<!-- ./TABLA DE PLANES -->



<!--[if lte IE 8]>
<script type="text/javascript" src="js/excanvas.min.js"></script><![endif]-->
<script>
var chart = <?= json_encode($chart) ?>;
</script>