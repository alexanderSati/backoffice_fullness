<?php defined('BASEPATH') OR exit('No direct script access allowed'); ?>

<?php $lang = (isset($user['language']) ? $user['language'] : $this->config->item('language')); ?>

<div class="card px-3">
    <div class="card-header">
        <h4><?= $page_title ?></h4>                
    </div>
  <div class="card-block">
            
    <div class="card-content px-sm-5 mx-sm-5">
        <?php echo form_open_multipart('', array('class' => 'form-horizontal form-label-left', 'data-parsley-validate' => '','role' => 'form')); ?>

        <?php // hidden id ?>
        <?php if (isset($user_id)) : ?>
            <?php echo form_hidden('id', $user_id); ?>
        <?php endif; ?>

        <div class="form-group row">
            <?php echo form_label(lang('users input username') . ' *', 'username', array('class' => 'col-sm-3 col-form-label')); ?>
            <div class="col-sm-9">
                <input type="text" name="username" required <?= (isset($user_id)) ? "readonly" : "" ?>
                        value="<?= set_value('username', (isset($user['username']) ? $user['username'] : '')) ?>"
                        class="form-control">
            </div>
        </div>
        <div class="form-group row">
            <?php echo form_label(lang('users input first_name') . ' *', 'first_name', array('class' => 'col-sm-3 col-form-label')); ?>
            <div class="col-sm-9">
                <?php echo form_input(array('name' => 'first_name','required'=>'required', 'value' => set_value('first_name', (isset($user['first_name']) ? $user['first_name'] : '')), 'class' => 'form-control')); ?>
            </div>
        </div>
        <div class="form-group row">
            <?php echo form_label(lang('users input last_name') . ' *', 'last_name', array('class' => 'col-sm-3 col-form-label')); ?>
            <div class="col-sm-9">
                <?php echo form_input(array('name' => 'last_name','required'=>'required', 'value' => set_value('last_name', (isset($user['last_name']) ? $user['last_name'] : '')), 'class' => 'form-control')); ?>
            </div>
        </div>
        <div class="form-group row">
            <?php echo form_label(lang('users input email') . ' *', 'email', array('class' => 'col-sm-3 col-form-label')); ?>
            <div class="col-sm-9">
                <?php echo form_input(array('name' => 'email','required'=>'required', 'value' => set_value('email', (isset($user['email']) ? $user['email'] : '')), 'class' => 'form-control', 'type' => 'email')); ?>
            </div>
        </div>
        <div class="form-group row">
            <?php echo form_label(lang('users input shiping_details'), 'shiping_details', array('class' => 'col-sm-3 col-form-label')); ?>
            <div class="col-sm-9">
              <?php if (isset($user['shiping_details'])) {           ?>
                <?php echo form_dropdown('shiping_details', $shipping, (isset($user['shiping_details']) ? $user['shiping_details'] : ''), "id='shiping_details' class='form-control' disabled"); ?>
            <?php }else {?>
                <?php echo form_dropdown('shiping_details', $shipping, (isset($user['shiping_details']) ? $user['shiping_details'] : ''), "id='shiping_details' class='form-control'"); ?>                
            <?php }?>
            </div>
        </div>
        <div class="form-group row">
            <?php echo form_label(lang('users input language'), 'language', array('class' => 'col-sm-3 col-form-label')); ?>
            <div class="col-sm-9">
                <?php echo form_dropdown('language', $this->languages, (isset($user['language']) ? $user['language'] : $this->config->item('language')), 'id="language" class="form-control"'); ?>
            </div>
        </div>
        <div class="form-radio row">
            <?php echo form_label(lang('users input status'), '', array('class'=>'col-sm-3 col-form-label')); ?>
            <div class="col-sm-9">
                <div class="radio radio-inline">
                    <label>
                        <?php echo form_radio(array('name'=>'status', 'id'=>'radio-status-1', 'class'=>'flat', 'value'=>'1', 'checked'=>(( ! isset($user['status']) OR (isset($user['status']) && (int)$user['status'] == 1) OR $user['id'] == 1) ? 'checked' : FALSE))); ?>
                        <i class="helper"></i><?php echo lang('admin input active'); ?>
                    </label>
                </div>
                <?php if ( ! $user['id'] OR $user['id'] > 1) : ?>
                    <div class="radio radio-inline">
                        <label>
                            <?php echo form_radio(array('name'=>'status', 'id'=>'radio-status-2', 'class'=>'flat', 'value'=>'0', 'checked'=>((isset($user['status']) && (int)$user['status'] == 0) ? 'checked' : FALSE))); ?>
                            <i class="helper"></i><?php echo lang('admin input inactive'); ?>
                        </label>
                    </div>
                <?php endif; ?>
            </div>
        </div>

        <div class="form-group row">
            <?php echo form_label(lang('users input password') . ' *' , 'password', array('class' => 'col-sm-3 col-form-label')); ?>
            <div class="col-sm-9">
                <?php echo form_password(array('name' => 'password', 'value' => '', 'id'=>'password', 'class' => 'form-control')); ?>
            </div>
        </div>
        <div class="form-group row">
            <?php echo form_label(lang('users input password_repeat'). ' *', 'password_repeat', array('class' => 'col-sm-3 col-form-label')); ?>
            <div class="col-sm-9">
                <?php echo form_password(array('name' => 'password_repeat', 'data-parsley-trigger'=> 'change' , 'data-parsley-equalto'=>'#password', 'value' => '', 'class' => 'form-control')); ?>
            </div>
            <?php if (!$password_required) : ?>
                <span class="help-block"><br/><?php echo lang('users help passwords'); ?></span>
            <?php endif; ?>
        </div>
        <div class="form-group row">
            <?php echo form_label(lang('users input image') . ' *' , 'image', array('class' => 'col-sm-3 col-form-label')); ?>
            <div class="col-sm-9">
                <?php echo form_upload(array('name' => 'image', 'id'=>'image', 'class' => 'form-control')); ?>
            </div>
        </div>

        <div><hr/></div>

        <div class="form-group row text-center">
            <div class="col-md-12 col-md-offset-1 col-xs-12">
                <a class="btn btn-primary" href="<?php echo $cancel_url; ?>"><?php echo lang('core button cancel'); ?></a>
                <button type="submit" name="submit" class="btn btn-info"><span
                        class="fa fa-check-square-o"></span> <?php echo lang('core button save'); ?></button>
            </div>
        </div>

        <?php echo form_close(); ?>
    </div>
        

</div>
</div>