<?php defined('BASEPATH') OR exit('No direct script access allowed'); ?>
<div class="card px-3">
    <div class="card-header">
        <h4><?= $page_title ?></h4>
        <div class="col-md-12 text-right">
        </div>                
    </div>
  <div class="card-block">      
    
    <div class="dt-responsive table-responsive">                
        <table class="table table-striped table-bordered nowrap">
            <thead>
            <tr>
                <td>
                    <a href="<?php echo current_url(); ?>?sort=id&dir=<?php echo (($dir == 'asc' ) ? 'desc' : 'asc'); ?>&limit=<?php echo $limit; ?>&offset=<?php echo $offset; ?>"><?php echo lang('users col user_id'); ?></a>
                        <?php if ($sort == 'id') : ?><span class="glyphicon glyphicon-arrow-<?php echo (($dir == 'asc') ? 'up' : 'down'); ?>"></span><?php endif; ?>
                </td>
                <td>
                    <a href="<?php echo current_url(); ?>?sort=username&dir=<?php echo (($dir == 'asc' ) ? 'desc' : 'asc'); ?>&limit=<?php echo $limit; ?>&offset=<?php echo $offset; ?>"><?php echo lang('users col username'); ?></a>
                    <?php if ($sort == 'username') : ?><span class="glyphicon glyphicon-arrow-<?php echo (($dir == 'asc') ? 'up' : 'down'); ?>"></span><?php endif; ?>
                </td>
                <td>
                    <?php echo lang('users col fullname'); ?>
                </td>
                <td>
                    <?php echo lang('users col wallets'); ?>
                </td>
                <td>
                    <?php echo lang('users col total'); ?>
                </td>
                <td>
                    <a href="<?php echo current_url(); ?>?sort=profit&dir=<?php echo (($dir == 'asc' ) ? 'desc' : 'asc'); ?>&limit=<?php echo $limit; ?>&offset=<?php echo $offset; ?>"><?php echo lang('users col profit'); ?></a>
                    <?php if ($sort == 'profit') : ?><span class="glyphicon glyphicon-arrow-<?php echo (($dir == 'asc') ? 'up' : 'down'); ?>"></span><?php endif; ?>
                </td>
                <td>
                    <a href="<?php echo current_url(); ?>?sort=payout&dir=<?php echo (($dir == 'asc' ) ? 'desc' : 'asc'); ?>&limit=<?php echo $limit; ?>&offset=<?php echo $offset; ?>"><?php echo lang('users col payout'); ?></a>
                    <?php if ($sort == 'payout') : ?><span class="glyphicon glyphicon-arrow-<?php echo (($dir == 'asc') ? 'up' : 'down'); ?>"></span><?php endif; ?>
                </td>
            </tr>
            </thead>
        <tbody>

            <?php // data rows ?>
            <?php if ($total) : ?>
                <?php foreach ($users as $user) : ?>
                    <tr>
                        <td>
                            <?php echo $user['id']; ?>
                        </td>
                        <td>
                            <?php echo $user['username']; ?>
                        </td>
                        <td>
                            <?php echo $user['first_name']." ".$user['last_name']; ?>
                        </td>                                   
                        <td>
                            <strong><?php echo lang('users col BTC'); ?>: </strong><?php echo '$ '. $user['USD']; ?>
                            <br>                                        
                            <strong><?php echo lang('users col XMR'); ?>: </strong><?php echo '$ '. $user['XMR']; ?>
                            <br>
                            <strong><?php echo lang('users col ETH'); ?>: </strong><?php echo '$ '. $user['ETH']; ?>
                            <br>
                            <strong><?php echo lang('users col LTC'); ?>: </strong><?php echo '$ '. $user['LTC']; ?>
                            <br>
                            <strong><?php echo lang('users col TRUMP'); ?>: </strong><?php echo '$ '. $user['TRUMP']; ?>
                            <br>
                            <strong><?php echo lang('users col DASH'); ?>: </strong><?php echo '$ '. $user['DASH']; ?>
                            <br>
                            <strong><?php echo lang('users col SLR'); ?>: </strong><?php echo '$ '. $user['SLR']; ?>
                            <br>
                            <strong><?php echo lang('users col USD'); ?>: </strong><?php echo '$ '. $user['BTC']; ?>
                        </td>
                        <td>
                            <?php echo '$ '. $user['total']; ?>
                        </td>
                        <td>
                            <?php echo '$ '. $user['profit']; ?>
                        </td>
                        <td>
                            <?php echo $user['payout']; ?>
                        </td>
                    </tr>
                <?php endforeach; ?>
            <?php else : ?>
                <tr>
                    <td colspan="7">
                        <?php echo lang('core error no_results'); ?>
                    </td>
                </tr>
            <?php endif; ?>

            </tbody>
        </table>

        <?php // list tools ?>
        <div class="panel-footer m-b-30">
            <div class="row">
                <div class="col-md-2 text-left">
                    <label><?php echo sprintf(lang('admin label rows'), $total); ?></label>
                </div>
                <div class="col-md-8 pagination justify-content-center">
                    <?php echo $pagination; ?>
                </div>
            </div>
        </div>
    </div>
   
  </div>
  <!-- end of card-block -->
</div>
<style>
/**Tabla responsive **/
    @media screen and (max-width: 580px) {
     table {
       display: block;
       overflow-x: auto;
     }
}
</style>

