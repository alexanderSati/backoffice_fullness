<?php defined('BASEPATH') OR exit('No direct script access allowed'); ?>

<div class="card px-3">
    <div class="card-block">
    <div class="row">
        <div class="col-md-12 col-sm-12 col-xs-12">
            <div class="x_panel">
                <div class="x_title">
                    <h2><?= $page_title ?></h2>
                </div>
                <div class="card-block">
                    <div class="dt-responsive table-responsive">

                        <table id="datatable-responsive" class="table table-striped table-bordered nowrap">
                            <thead>

                            <?php // sortable headers ?>
                            <tr>
                                <td>
                                    <?php echo lang('users col user_id'); ?>
                                </td>
                                <td>
                                    <?php echo lang('users col username'); ?>
                                </td>
                                <td>
                                    <?php echo lang('users col first_name'); ?>
                                </td>
                                <td>
                                    <?php echo lang('users col last_name'); ?>
                                </td>
                                <td>
                                    <?php echo lang('admin col status'); ?>
                                </td>
                                <td>
                                    <?php echo lang('users input profile'); ?>
                                </td>
                                <td>
                                    <?php echo lang('admin col actions'); ?>
                                </td>
                            </tr>
                            </thead>
                            <tbody>

                            <?php // data rows ?>
                
                                <?php foreach ($users as $user) : ?>
                                    <tr>
                                        <td>
                                            <?php echo $user['id']; ?>
                                        </td>
                                        <td >
                                            <?php echo $user['username']; ?>
                                        </td>
                                        <td>
                                            <?php echo $user['first_name']; ?>
                                        </td>
                                        <td>
                                            <?php echo $user['last_name']; ?>
                                        </td>
                                        <td>
                                            <?php echo ($user['status']) ? '<span class="active">' . lang('admin input active') . '</span>' : '<span class="inactive">' . lang('admin input inactive') . '</span>'; ?>
                                        </td>
                                        <td>
                                            <?php echo $user['profile']; ?>
                                        </td>
                                        <td>
                                            <div class="text-center">
                                                <div class="btn-group">
                                                    <a href="<?php echo $this_url; ?>/edit/<?php echo $user['id']; ?>" class="btn btn-warning btn-icon" title="<?php echo lang('admin button edit'); ?>"><i class="fa fa-pencil"></i></a>
                                                </div>
                                            </div>
                                        </td>
                                    </tr>
                                <?php endforeach; ?>
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>
  </div>
</div>