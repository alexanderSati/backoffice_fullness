<?php defined('BASEPATH') OR exit('No direct script access allowed'); ?>

<?php $lang = (isset($user['language']) ? $user['language'] : $this->config->item('language')); ?>
<div class="card px-3">
  <div class="card-header">
    <h4><?= $page_title ?></h4>
  </div>
  <div class="card-block">

    <div class="card-content px-sm-5 mx-sm-5">
      <?php echo form_open_multipart('', array('class' => 'form-horizontal form-label-left', 'data-parsley-validate' => '','role' => 'form', 'novalidate')); ?>

      <?php // hidden id ?>

      <?php if (isset($user_id)) : ?>
      <?php echo form_hidden('id', $user_id); ?>
      <?php endif; ?>

      <div class="form-group row">
        <?php echo form_label(lang('users input username') . ' *', 'username', array('class' => 'col-sm-3 col-form-label')); ?>
        <div class="col-sm-8">
          <input type="text" name="username" required <?= (isset($user_id)) ? "readonly" : "" ?>
            value="<?= set_value('username', (isset($user['username']) ? $user['username'] : '')) ?>"
            class="form-control">
        </div>
      </div>
      <div class="form-group row">
        <?php echo form_label(lang('users input first_name') . ' *', 'first_name', array('class' => 'col-sm-3 col-form-label')); ?>
        <div class="col-sm-8">
          <?php echo form_input(array('name' => 'first_name','required'=>'required', 'value' => set_value('first_name', (isset($user['first_name']) ? $user['first_name'] : '')), 'class' => 'form-control')); ?>
        </div>
      </div>
      <div class="form-group row">
        <?php echo form_label(lang('users input last_name') . ' *', 'last_name', array('class' => 'col-sm-3 col-form-label')); ?>
        <div class="col-sm-8">
          <?php echo form_input(array('name' => 'last_name','required'=>'required', 'value' => set_value('last_name', (isset($user['last_name']) ? $user['last_name'] : '')), 'class' => 'form-control')); ?>
        </div>
      </div>
      <div class="form-group row">
        <?php echo form_label(lang('users input email') . ' *', 'email', array('class' => 'col-sm-3 col-form-label')); ?>
        <div class="col-sm-8">
          <?php echo form_input(array('name' => 'email','required'=>'required', 'value' => set_value('email', (isset($user['email']) ? $user['email'] : '')), 'class' => 'form-control', 'type' => 'email')); ?>
        </div>
      </div>
      <!--------BIRTHDATE--------->
      <div class="form-group row">
        <?php echo form_label(lang('users input birthdate') . ' *', 'birthdate', array('class' => 'col-sm-3 col-form-label')); ?>
        <div class="col-sm-8">
          <?php echo form_input(array('name' => 'birthdate','required'=>'required', 'value' => set_value('birthdate', (isset($user['birthdate']) ? $user['birthdate'] : '')), 'class' => 'form-control', 'type' => 'date')); ?>
        </div>
      </div>

      <!-------PHONE------------->
      <div class="form-group row">
        <?php echo form_label(lang('users input phone') . ' *', 'phone', array('class' => 'col-sm-3 col-form-label')); ?>
        <div class="col-sm-8">
          <?php echo form_input(array('name' => 'phone','required'=>'required', 'value' => set_value('phone', (isset($user['phone']) ? $user['phone'] : '')), 'class' => 'form-control')); ?>
        </div>
      </div>

      <!-------ADDRESS----------->
      <div class="form-group row">
        <?php echo form_label(lang('users input address') . ' *', 'address', array('class' => 'col-sm-3 col-form-label')); ?>
        <div class="col-sm-8">
          <?php echo form_input(array('name' => 'address','required'=>'required', 'value' => set_value('address', (isset($user['address']) ? $user['address'] : '')), 'class' => 'form-control')); ?>
        </div>
      </div>

      <!-------ADDRESS 2----------->
      <div class="form-group row">
        <?php echo form_label(lang('users input address2'), 'address2', array('class' => 'col-sm-3 col-form-label')); ?>
        <div class="col-sm-8">
          <?php echo form_input(array('name' => 'address2', 'value' => set_value('address2', (isset($user['address2']) ? $user['address2'] : '')), 'class' => 'form-control')); ?>
        </div>
      </div>

      <!--------CITY--------------->
      <div class="form-group row">
        <?php echo form_label(lang('users input city') . " *", 'city', array('class' => 'col-sm-3 col-form-label')); ?>
        <div class="col-sm-8">
          <?php echo form_input(array('name' => 'city', 'required'=>'required', 'value' => set_value('city', (isset($user['city']) ? $user['city'] : '')), 'class' => 'form-control')); ?>
        </div>
      </div>

      <!--------DEPARTMENT--------->
      <div class="form-group row">
        <?php echo form_label(lang('users input department') . " *", 'department', array('class' => 'col-sm-3 col-form-label')); ?>
        <div class="col-sm-8">
          <?php echo form_input(array('name' => 'department', 'required'=>'required', 'value' => set_value('department', (isset($user['department']) ? $user['department'] : '')), 'class' => 'form-control')); ?>
        </div>
      </div>

      <!--------POSTAL CODE--------->
      <div class="form-group row">
        <?php echo form_label(lang('users input postal_code') . " *", 'postal_code', array('class' => 'col-sm-3 col-form-label')); ?>
        <div class="col-sm-8">
          <?php echo form_input(array('name' => 'postal_code', 'required'=>'required', 'value' => set_value('postal_code', (isset($user['postal_code']) ? $user['postal_code'] : '')), 'class' => 'form-control')); ?>
        </div>
      </div>
      <!--------BANK ACCOUNT-------->
      <div class="form-group row" style="display:none">
        <?php echo form_label(lang('users input bank_account') . " *", 'bank_account', array('class' => 'col-sm-3 col-form-label')); ?>
        <div class="col-sm-8">
          <?php echo form_input(array('name' => 'bank_account', 'value' => set_value('bank_account', (isset($user['bank_account']) ? $user['bank_account'] : '10000001')), 'class' => 'form-control')); ?>
        </div>
      </div>

      <div class="form-group row">
        <?php echo form_label(lang('users input language'), 'language', array('class' => 'col-sm-3 col-form-label')); ?>
        <div class="col-sm-8">
          <?php echo form_dropdown('language', $this->languages, (isset($user['language']) ? $user['language'] : $this->config->item('language')), 'id="language" class="form-control"'); ?>
        </div>
      </div>
      <div class="form-group row">
        <?php echo form_label(lang('users input country'), 'country', array('class' => 'col-sm-3 col-form-label')); ?>
        <div class="col-sm-8">
          <?php echo form_dropdown('country', $countries, (isset($user['country']) ? $user['country'] : ''), 'id="country" required="required" class=" form-control"'); ?>
        </div>
      </div>
      <div class="form-group row">
        <?php echo form_label(lang('users input plan'), 'plan', array('class' => 'col-sm-3 col-form-label')); ?>
        <div class="col-sm-8">
          <select name="plan" id="plan" class="form-control" <?= (isset($only_add)) ? "" : "disabled"; ?>>
            <?php foreach ($plans as $plan) { ?>
            <option value="<?= $plan['id'] ?>"><?= $plan["{$lang}_name"] ?></option>
            <?php } ?>
          </select>
        </div>
      </div>
      <div class="form-radio row">
        <?php echo form_label(lang('users input status'), '', array('class'=>'col-sm-3 col-form-label')); ?>
        <div class="col-sm-8">
          <div class="radio radio-inline">
            <label>
              <?php echo form_radio(array('name'=>'status', 'id'=>'radio-status-1', 'value'=>'1', 'checked'=>(( ! isset($user['status']) OR (isset($user['status']) && (int)$user['status'] == 1) OR $user['id'] == 1) ? 'checked' : FALSE))); ?>
              <i class="helper"></i><?php echo lang('admin input active'); ?>
            </label>
          </div>
          <?php if ( ! $user['id'] OR $user['id'] > 1) : ?>
          <div class="radio radio-inline">
            <label>
              <?php echo form_radio(array('name'=>'status', 'id'=>'radio-status-2', 'value'=>'0', 'checked'=>((isset($user['status']) && (int)$user['status'] == 0) ? 'checked' : FALSE))); ?>
              <i class="helper"></i><?php echo lang('admin input inactive'); ?>
            </label>
          </div>
          <?php endif; ?>
        </div>
      </div>

      <?php // administrator ?>
      <div class="form-radio row">
        <?php echo form_label(lang('users input is_admin'), '', array('class'=>'col-sm-3 col-form-label')); ?>
        <div class="col-sm-8">
          <?php if ( ! $user['id'] OR $user['id'] > 1) : ?>
          <div class="radio radio-inline">
            <label>
              <?php echo form_radio(array('name'=>'is_admin', 'id'=>'radio-is_admin-1', 'value'=>'0', 'checked'=>(( ! isset($user['is_admin']) OR (isset($user['is_admin']) && (int)$user['is_admin'] == 0) && $user['id'] != 1) ? 'checked' : FALSE))); ?>
              <i class="helper"></i><?php echo lang('core text no'); ?>
            </label>
          </div>
          <?php endif; ?>
          <div class="radio radio-inline">
            <label>
              <?php echo form_radio(array('name'=>'is_admin', 'id'=>'radio-is_admin-2', 'value'=>'1', 'checked'=>((isset($user['is_admin']) && (int)$user['is_admin'] == 1) ? 'checked' : FALSE))); ?>
              <i class="helper"></i><?php echo lang('core text yes'); ?>
            </label>
          </div>
        </div>
      </div>

      <div class="form-group row">
        <?php echo form_label(lang('users input password') . ' *' , 'password', array('class' => 'col-sm-3 col-form-label')); ?>
        <div class="col-sm-8">
          <?php echo form_password(array('name' => 'password', 'value' => '', 'id'=>'password', 'class' => 'form-control')); ?>
        </div>
      </div>
      <div class="form-group row">
        <?php echo form_label(lang('users input password_repeat'). ' *', 'password_repeat', array('class' => 'col-sm-3 col-form-label')); ?>
        <div class="col-sm-8">
          <?php echo form_password(array('name' => 'password_repeat', 'data-parsley-trigger'=> 'change' , 'data-parsley-equalto'=>'#password', 'value' => '', 'class' => 'form-control')); ?>
        </div>
        <?php if (!$password_required) : ?>
        <div class="col-sm-9 text-right">
          <span class="help-block"><br /><?php echo lang('users help passwords'); ?></span>
        </div>
        <?php endif; ?>
      </div>
      <div class="form-group row">
        <?php echo form_label(lang('users input image') . ' *' , 'image', array('class' => 'col-sm-3 col-form-label')); ?>
        <div class="col-sm-8">
          <?php echo form_upload(array('name' => 'image', 'id'=>'image', 'class' => 'form-control')); ?>
        </div>
      </div>

      <div class="form-group row text-center">
        <div class="col-md-12 col-md-offset-1 col-xs-12">
          <a class="btn btn-primary m-r-20"
            href="<?php echo $cancel_url; ?>"><?php echo lang('core button cancel'); ?></a>
          <button type="submit" name="submit" class="btn btn-success"><span class="fa fa-check-square-o"></span>
            <?php echo lang('core button save'); ?></button>
        </div>
      </div>

      <?php echo form_close(); ?>
    </div>

  </div>
</div>