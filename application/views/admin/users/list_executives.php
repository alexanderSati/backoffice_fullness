<?php defined('BASEPATH') OR exit('No direct script access allowed'); ?>

<div class="card px-3">
  <div class="card-header">
    <h4><?= lang('executive') ?></h4>     
  </div>

  <div class="col-md-12 text-left m-15">
    <a class="btn btn-info" href="<?php echo  $this_url; ?>/add" title="<?php echo lang('users tooltip add_new_executive') ?>" data-toggle="tooltip">
      <i class="fa fa-plus"></i><?php echo lang('users button add_new_executive'); ?>
    </a>
    <a class="btn btn-success" target="_blank" href="<?= site_url('admin/executive/download_stock_wholesalers'); ?>">
      Descargar Stock
    </a>
    <button class="btn btn-success" data-toggle="modal" data-target="#modal-ord-mayoristas">
      Descargar Informe de Compras
    </button>
  </div>

  <div class="card-block">          
    <div class="card-content">     
      <div class="dt-responsive table-responsive">
        <table id="datatable-responsive" class="table table-striped table-bordered nowrap">
          <thead>
            <tr>
              <th><?= lang('users col user_id') ?></th>
              <th><?= lang('users col username') ?></th>
              <th><?= lang('users col first_name') ?></th>
              <th><?= lang('users col last_name') ?></th>
              <th><?= lang('users input shiping_details') ?></th>  
              <th><?= lang('admin col actions') ?></th>
            </tr>


          </thead>
          <tbody>
            <?php foreach ($users as $user) {   ?>
            <tr>
              <td><?= $user['id'] ?></td>
              <td><?= $user['username'] ?></td>
              <td><?= $user['first_name']?></td>
              <td><?= $user['last_name'] ?></td>
              <td><?= $user['name'] ?></td>
              <td align="center">
                        <a href="<?php echo $this_url; ?>/edit/<?php echo $user['id']; ?>" class="btn btn-warning btn-icon" title="<?php echo lang('admin button edit'); ?>"><i class="fa fa-pencil"></i></a>                                     
               </td>
            </tr>
            <?php } ?>

          </tbody>
        </table>
      </div>
    </div>   
  </div>
</div>

<div id="modal-ord-mayoristas" class="modal fade" tabindex="-1" role="dialog" aria-labelledby="exampleModalCenterTitle" style="display: none;" aria-hidden="true">
  <div class="modal-dialog modal-dialog-centered" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title" id="exampleModalCenterTitle">Descargar ordenes de mayoristas</h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">×</span></button>
      </div>
      <div class="modal-body">
        <div class="form-group" >
          <label>Fecha inicial:</label>
          <input type="date" class="form-control" id="orders_fech_ini" />
        </div>
        <div class="form-group" >
          <label>Fecha final:</label>
          <input type="date" class="form-control" id="orders_fech_end" />
        </div>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-secondary has-ripple" data-dismiss="modal">Cerrar<span class="ripple ripple-animate" style="height: 75.35px; width: 75.35px; animation-duration: 0.7s; animation-timing-function: linear; background: rgb(255, 255, 255) none repeat scroll 0% 0%; opacity: 0.4; top: -17.925px; left: 1.49169px;"></span></button>
        <button type="button" class="btn  btn-primary" onclick="descargarInformeOrdenes()">Descargar</button>
      </div>
    </div>
  </div>
</div>

<script>
descargarInformeOrdenes = function(){
  let link = $('#site_url').val()+"index.php/admin/executive/download_orders_wholesaler";
  if(
    $('#orders_fech_ini').val() != ""
    &&
    $('#orders_fech_end').val() != ""
  ){
    link+="?fecha_ini="+$('#orders_fech_ini').val()+"&fecha_fin="+$('#orders_fech_end').val();
  }
  console.log(link);
  window.open(link,"_blank");
}
</script>