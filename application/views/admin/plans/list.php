<?php defined('BASEPATH') OR exit('No direct script access allowed'); ?>

<div class="card px-3">
    <div class="card-header">
        <h4><?= $page_title ?></h4>                
    </div>
    <div class="card-block">                    
        <div class="card-content">
            <div class="dt-responsive">
                <table class="table table-striped table-bordered nowrap">
                    <thead>

                    <?php // sortable headers ?>
                    <tr>
                        <td align="center">
                            <a href="<?php echo current_url(); ?>?sort=id&dir=<?php echo (($dir == 'asc' ) ? 'desc' : 'asc'); ?>&limit=<?php echo $limit; ?>&offset=<?php echo $offset; ?><?php echo $filter; ?>"><?php echo lang('plans col id'); ?></a>
                            <?php if ($sort == 'id') : ?><i class="fa fa-sort-<?php echo (($dir == 'asc') ? 'up' : 'desc'); ?>"></i><?php endif; ?>
                        </td>
                        <td>
                            <a href="<?php echo current_url(); ?>?sort=base&dir=<?php echo (($dir == 'asc' ) ? 'desc' : 'asc'); ?>&limit=<?php echo $limit; ?>&offset=<?php echo $offset; ?><?php echo $filter; ?>"><?php echo lang('plans col base'); ?></a>
                            <?php if ($sort == 'base') : ?><span class="fa fa-sort-<?php echo (($dir == 'asc') ? 'up' : 'desc'); ?>"></span><?php endif; ?>
                        </td>
                        <td>
                            <a href="<?php echo current_url(); ?>?sort=period_days&dir=<?php echo (($dir == 'asc' ) ? 'desc' : 'asc'); ?>&limit=<?php echo $limit; ?>&offset=<?php echo $offset; ?><?php echo $filter; ?>"><?php echo lang('plans col period_days'); ?></a>
                            <?php if ($sort == 'period_days') : ?><span class="fa fa-sort-<?php echo (($dir == 'asc') ? 'up' : 'desc'); ?>"></span><?php endif; ?>
                        </td>
                        <td>
                            <a href="<?php echo current_url(); ?>?sort=spanish_name&dir=<?php echo (($dir == 'asc' ) ? 'desc' : 'asc'); ?>&limit=<?php echo $limit; ?>&offset=<?php echo $offset; ?><?php echo $filter; ?>"><?php echo lang('plans col spanish_name'); ?></a>
                            <?php if ($sort == 'spanish_name') : ?><span class="fa fa-sort-<?php echo (($dir == 'asc') ? 'up' : 'desc'); ?>"></span><?php endif; ?>
                        </td>
                        <td>
                            <a href="<?php echo current_url(); ?>?sort=status&dir=<?php echo (($dir == 'asc' ) ? 'desc' : 'asc'); ?>&limit=<?php echo $limit; ?>&offset=<?php echo $offset; ?><?php echo $filter; ?>"><?php echo lang('admin col status'); ?></a>
                            <?php if ($sort == 'status') : ?><span class="fa fa-sort-<?php echo (($dir == 'asc') ? 'up' : 'desc'); ?>"></span><?php endif; ?>
                        </td>
                        <td align="center">
                            <?php echo lang('admin col actions'); ?>
                        </td>
                    </tr>

                    <?php // search filters ?>
                    <tr>
                        <?php echo form_open("{$this_url}?sort={$sort}&dir={$dir}&limit={$limit}&offset=0{$filter}", array('role'=>'form', 'id'=>"filters")); ?>
                        <th>
                        </th>
                        <th <?php echo ((isset($filters['base'])) ? ' class="has-success"' : ''); ?>>
                            <?php echo form_input(array('name'=>'base', 'id'=>'base', 'class'=>'form-control input-sm', 'placeholder'=>lang('plans input base'), 'value'=>set_value('base', ((isset($filters['base'])) ? $filters['base'] : '')))); ?>
                        </th>
                        <th <?php echo ((isset($filters['period_days'])) ? ' class="has-success"' : ''); ?>>
                            <?php echo form_input(array('name'=>'period_days', 'id'=>'period_days', 'class'=>'form-control input-sm', 'placeholder'=>lang('plans input period_days'), 'value'=>set_value('period_days', ((isset($filters['period_days'])) ? $filters['period_days'] : '')))); ?>
                        </th>
                        <th <?php echo ((isset($filters["{$lang}_name"])) ? ' class="has-success"' : ''); ?>>
                            <?php echo form_input(array('name'=>"{$lang}_name", 'id'=>"{$lang}_name", 'class'=>'form-control input-sm', 'placeholder'=>lang("plans input {$lang}_name"), 'value'=>set_value("{$lang}_name", ((isset($filters["{$lang}_name"])) ? $filters["{$lang}_name"] : '')))); ?>
                        </th>
                        <th></th>
                        <th>
                            <div class="text-center">
                                <a href="<?php echo $this_url; ?>" class="btn btn-danger" data-toggle="tooltip" title="<?php echo lang('admin tooltip filter_reset'); ?>"><span class="glyphicon glyphicon-refresh"></span> <?php echo lang('core button reset'); ?></a>
                                <button type="submit" name="submit" value="<?php echo lang('core button filter'); ?>" class="btn btn-success tooltips" data-toggle="tooltip" title="<?php echo lang('admin tooltip filter'); ?>"><span class="glyphicon glyphicon-filter"></span> <?php echo lang('core button filter'); ?></button>
                            </div>
                        </th>
                        <?php echo form_close(); ?>
                    </tr>

                    </thead>
                    <tbody>

                    <?php // data rows ?>
                    <?php if ($total) : ?>
                        <?php foreach ($plans as $plan) : ?>
                            <tr>
                                <td align="center" <?php echo (($sort == 'id') ? ' class="sorted"' : ''); ?>>
                                    <?php echo $plan['id']; ?>
                                </td>
                                <td <?php echo (($sort == 'base') ? ' class="sorted"' : ''); ?>>
                                    <?php echo $plan['base']; ?>
                                </td>
                                <td <?php echo (($sort == 'period_days') ? ' class="sorted"' : ''); ?>>
                                    <?php echo $plan['period_days']; ?>
                                </td>
                                <td <?php echo (($sort == "{$lang}_name") ? ' class="sorted"' : ''); ?>>
                                    <?php echo $plan["{$lang}_name"]; ?>
                                </td>
                                <td <?php echo (($sort == 'status') ? ' class="sorted"' : ''); ?>>
                                    <?php echo ( $plan['status'] == 'active' ) ? '<span class="active">' . lang('admin input active') . '</span>' : '<span class="inactive">' . lang('admin input inactive') . '</span>'; ?>
                                </td>
                                <td>
                                    <div class="text-center">
                                        <div class="btn-group">
                                            <!-- <?php //if( empty( $plan['has_subscription'] ) == TRUE ): ?>
                                                <a href="#modal-<?php //echo $plan['id']; ?>" data-toggle="modal" class="btn btn-danger btn-icon" title="<?php //echo lang('admin button delete'); ?>"><i class="fa fa-trash"></i></a>
                                            <?php //endif; ?> -->
                                            <a href="<?php echo $this_url; ?>/edit/<?php echo $plan['id']; ?>" class="btn btn-warning btn-icon" title="<?php echo lang('admin button edit'); ?>"><i class="fa fa-pencil"></i></a>
                                        </div>
                                    </div>
                                </td>
                            </tr>
                        <?php endforeach; ?>
                    <?php else : ?>
                        <tr>
                            <td colspan="7">
                                <?php echo lang('core error no_results'); ?>
                            </td>
                        </tr>
                    <?php endif; ?>

                    </tbody>
                </table>

                <?php // list tools ?>
                <div class="panel-footer">
                    <div class="row">
                        <div class="col-md-2 text-left">
                            <label><?php echo sprintf(lang('admin label rows'), $total); ?></label>
                        </div>
                        <div class="col-md-2 text-left">
                            <?php if ($total > 10) : ?>
                                <select id="limit" class="form-control">
                                    <option value="10"<?php echo ($limit == 10 OR ($limit != 10 && $limit != 25 && $limit != 50 && $limit != 75 && $limit != 100)) ? ' selected' : ''; ?>>10 <?php echo lang('admin input items_per_page'); ?></option>
                                    <option value="25"<?php echo ($limit == 25) ? ' selected' : ''; ?>>25 <?php echo lang('admin input items_per_page'); ?></option>
                                    <option value="50"<?php echo ($limit == 50) ? ' selected' : ''; ?>>50 <?php echo lang('admin input items_per_page'); ?></option>
                                    <option value="75"<?php echo ($limit == 75) ? ' selected' : ''; ?>>75 <?php echo lang('admin input items_per_page'); ?></option>
                                    <option value="100"<?php echo ($limit == 100) ? ' selected' : ''; ?>>100 <?php echo lang('admin input items_per_page'); ?></option>
                                </select>
                            <?php endif; ?>
                        </div>
                        <div class="col-md-6 pagination justify-content-center">
                            <?php echo $pagination; ?>
                        </div>
                        <div class="col-md-2 text-right">
                            <?php if ($total) : ?>
                                <a href="<?php echo $this_url; ?>/export?sort=<?php echo $sort; ?>&dir=<?php echo $dir; ?><?php echo $filter; ?>" class="btn btn-success tooltips" data-toggle="tooltip" title="<?php echo lang('admin tooltip csv_export'); ?>"><span class="glyphicon glyphicon-export"></span> <?php echo lang('admin button csv_export'); ?></a>
                            <?php endif; ?>
                        </div>
                    </div>
                </div>

            </div>

            <?php // delete modal ?>
            <?php if ($total) : ?>
                <?php foreach ($plans as $plan) : ?>
                    <div class="modal fade" id="modal-<?php echo $plan['id']; ?>" tabindex="-1" role="dialog" aria-labelledby="modal-label-<?php echo $plan['id']; ?>" aria-hidden="true">
                        <div class="modal-dialog">
                            <div class="modal-content">
                                <div class="modal-header">
                                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                                    <h4 id="modal-label-<?php echo $plan['id']; ?>"><?php echo lang('plans title user_delete');  ?></h4>
                                </div>
                                <div class="modal-body">
                                    <p><?php echo sprintf(lang('plans msg delete_confirm'), $plan['spanish_name']); ?></p>
                                </div>
                                <div class="modal-footer">
                                    <button type="button" class="btn btn-default" data-dismiss="modal"><?php echo lang('core button cancel'); ?></button>
                                    <button type="button" class="btn btn-primary btn-delete-plan" data-id="<?php echo $plan['id']; ?>"><?php echo lang('admin button delete'); ?></button>
                                </div>
                            </div>
                        </div>
                    </div>
                <?php endforeach; ?>
            <?php endif; ?>
        </div>    
    </div>
</div>

<style>
/**Tabla responsive **/
    @media screen and (max-width: 580px) {
     table {
       display: block;
       overflow-x: auto;
     }
}
</style>
