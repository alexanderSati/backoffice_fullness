<?php defined('BASEPATH') OR exit('No direct script access allowed'); ?>
<?php $lang = (isset($plan['language']) ? $plan['language'] : $this->config->item('language')); ?>

<div class="card px-3">
    <div class="card-header">
        <h4><?= $page_title ?></h4>
    </div>
    <div class="card-block mx-5 px-5">            
        <div class="card-content">
            <?php echo form_open('', array('class' => 'form-horizontal form-label-left', 'data-parsley-validate' => '','role' => 'form')); ?>

            <?php // hidden id ?>
            <?php if (isset($plan_id)) : ?>
                <?php echo form_hidden('id', $plan_id); ?>
            <?php endif; ?>

            <div class="form-group row">
                <?php echo form_label(lang('plans input base') . ' *', 'base', array('class' => 'col-sm-2 col-form-label')); ?>
                <div class="col-sm-9">
                    <?php echo form_input(array('name' => 'base','type'=>'number','required'=>'required', 'value' => set_value('base', (isset($plan['base']) ? $plan['base'] : '')), 'class' => 'form-control')); ?>
                </div>
            </div>
            <div class="form-group row">
                <?php echo form_label(lang('plans input period_days') . ' *', 'period_days', array('class' => 'col-sm-2 col-form-label')); ?>
                <div class="col-sm-9">
                    <?php echo form_input(array('name' => 'period_days','type'=>'number','required'=>'required', 'value' => set_value('period_days', (isset($plan['period_days']) ? $plan['period_days'] : '')), 'class' => 'form-control')); ?>
                </div>
            </div>
            <div class="form-group row">
                <?php echo form_label(lang('plans input period_days_before') . ' *', 'period_days_before', array('class' => 'col-sm-2 col-form-label')); ?>
                <div class="col-sm-9">
                    <?php echo form_input(array('name' => 'period_days_before','type'=>'number','required'=>'required', 'value' => set_value('period_days_before', (isset($plan['period_days_before']) ? $plan['period_days_before'] : '')), 'class' => 'form-control')); ?>
                </div>
            </div>
            <div class="form-group row">
                <?php echo form_label(lang('plans input min_profit') . ' *', 'min_profit', array('class' => 'col-sm-2 col-form-label')); ?>
                <div class="col-sm-9">
                    <?php echo form_input(array('name' => 'min_profit','type'=>'number','required'=>'required', 'value' => set_value('min_profit', (isset($plan['min_profit']) ? $plan['min_profit'] : '')), 'class' => 'form-control')); ?>
                </div>
            </div>
            <div class="form-group row">
                <?php echo form_label(lang('plans input max_profit') . ' *', 'max_profit', array('class' => 'col-sm-2 col-form-label')); ?>
                <div class="col-sm-9">
                    <?php echo form_input(array('name' => 'max_profit','type'=>'number','required'=>'required', 'value' => set_value('max_profit', (isset($plan['max_profit']) ? $plan['max_profit'] : '')), 'class' => 'form-control')); ?>
                </div>
            </div>
            <div class="form-group row">
                <?php echo form_label(lang('plans input excluded_days'), '', array('class'=>'col-sm-2 col-form-label')); ?>
                <div class="col-sm-9">
                
                    <div class="checkbox-color checkbox-primary">                        
                        <?php echo form_checkbox(array('name'=>'excluded_days[]', 'id'=>'radio-excluded_days-1', 'class'=>'border-checkbox-group border-checkbox-group-primary', 'value'=>'Mon', 'checked'=>(( isset($plan['excluded_days']) && in_array("Mon", $plan['excluded_days'])) ? 'checked' : FALSE))); ?>
                        <label for="radio-excluded_days-1"><?php echo lang('admin input monday'); ?></label>
                         &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                        
                       
                        <?php echo form_checkbox(array('name'=>'excluded_days[]', 'id'=>'radio-excluded_days-2', 'class'=>'border-checkbox-group border-checkbox-group-primary', 'value'=>'Tue', 'checked'=>(( isset($plan['excluded_days']) && in_array("Tue", $plan['excluded_days']) ) ? 'checked' : FALSE))); ?>
                        <label for="radio-excluded_days-2"><?php echo lang('admin input thuesday'); ?></label>
                        &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                        
                        <?php echo form_checkbox(array('name'=>'excluded_days[]', 'id'=>'radio-excluded_days-3', 'class'=>'border-checkbox-group border-checkbox-group-primary', 'value'=>'Wed', 'checked'=>(( isset($plan['excluded_days']) && in_array("Wed", $plan['excluded_days']) ) ? 'checked' : FALSE))); ?>
                        <label for="radio-excluded_days-3"><?php echo lang('admin input wednesday'); ?></label> 
                        &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                    
                        <?php echo form_checkbox(array('name'=>'excluded_days[]', 'id'=>'radio-excluded_days-4', 'class'=>'border-checkbox-group border-checkbox-group-primary', 'value'=>'Thu', 'checked'=>(( isset($plan['excluded_days']) && in_array("Thu", $plan['excluded_days']) ) ? 'checked' : FALSE))); ?>
                        <label for="radio-excluded_days-4"><?php echo lang('admin input thursday'); ?></label>
                        &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                    
                        <?php echo form_checkbox(array('name'=>'excluded_days[]', 'id'=>'radio-excluded_days-5', 'class'=>'border-checkbox-group border-checkbox-group-primary', 'value'=>'Fri', 'checked'=>(( isset($plan['excluded_days']) && in_array("Fri", $plan['excluded_days']) ) ? 'checked' : FALSE))); ?>
                        <label for="radio-excluded_days-5"><?php echo lang('admin input friday'); ?></label>
                        &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                    
                        <?php echo form_checkbox(array('name'=>'excluded_days[]', 'id'=>'radio-excluded_days-6', 'class'=>'border-checkbox-group border-checkbox-group-primary', 'value'=>'Sat', 'checked'=>(( isset($plan['excluded_days']) && in_array("Sat", $plan['excluded_days']) ) ? 'checked' : FALSE))); ?>
                        <label for="radio-excluded_days-6"><?php echo lang('admin input saturday'); ?></label>
                        &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                    
                        <?php echo form_checkbox(array('name'=>'excluded_days[]', 'id'=>'radio-excluded_days-7', 'class'=>'border-checkbox-group border-checkbox-group-primary', 'value'=>'Sun', 'checked'=>(( ! isset($plan['excluded_days']) || in_array("Sun", $plan['excluded_days']) ) ? 'checked' : FALSE))); ?>
                        <label for="radio-excluded_days-7"><?php echo lang('admin input sunday'); ?></label>                        
                    </div>
                </div>
            </div>
            <div class="form-radio row">
                <?php echo form_label(lang('plans input status'), '', array('class'=>'col-sm-2 col-form-label')); ?>
                <div class="col-sm-9">            
                    <div class="radio">
                        <label>
                            <?php echo form_radio(array('name'=>'status', 'id'=>'radio-status-1', 'class'=>'flat', 'value'=>'active', 'checked'=>((isset($plan['status']) && $plan['status'] == "active") or (!isset($plan['status'])) ) ? 'checked' : FALSE)); ?>
                            <i class="helper"></i><?php echo lang('admin input active'); ?>
                        </label>
                    </div>
                    <div class="radio">
                        <label>
                            <?php echo form_radio(array('name'=>'status', 'id'=>'radio-status-2', 'class'=>'flat', 'value'=>'inactive', 'checked'=>((isset($plan['status']) && $plan['status'] == "inactive") ) ? 'checked' : FALSE)); ?>
                            <i class="helper"></i><?php echo lang('admin input inactive'); ?>
                        </label>
                    </div>
                </div>               
            </div>

            <div class="form-group row m-t-30">
                <?php echo form_label('&nbsp;', '', array('class'=>'col-sm-2 col-form-label')); ?>
                <div class="col-sm-9">
                    <ul class="nav nav-tabs" role="tablist">
                        <?php foreach ($this->session->languages as $language_key=>$language_name) : ?>
                            <li class="nav-item">
                                <a class= "<?php echo ($language_key == $this->session->language) ? 'nav-link active' : 'nav-link'; ?>" data-toggle="tab" href="#<?php echo $language_key; ?>" role="tab"><?php echo $language_name; ?></a>                                
                                </li>
                        <?php endforeach; ?>
                    </ul>
                    
                    <div id="myTabContent" class="tab-content tabs card-block">
                    <?php foreach ($this->session->languages as $language_key=>$language_name) :?>
                        <?php $language_name = strtolower($language_name); ?>
                        <div class="<?php echo ($language_key == $this->session->language) ? 'tab-pane active' : 'tab-pane'; ?>" id="<?php echo $language_key; ?>" role="tabpanel">
                            <br />
                            <label for="<?= $language_name ?>_name"><?= lang("plans input {$lang}_name") ?></label>
                            <?php echo form_input(array('name' => "{$language_name}_name",'required'=>'required', 'value' => set_value("{$language_name}_name", (isset($plan["{$language_name}_name"]) ? $plan["{$language_name}_name"] : '')), 'class' => 'form-control')); ?>
                            <br />
                            <label for="<?= $language_name ?>_features"><?= lang("plans input {$lang}_features") ?></label>
                            <?php
                            $field_data['name']  = $language_name ."_features";
                            $field_data['id']    = $language_name ."_features";
                            $field_data['class'] = "form-control editor";
                            $field_data['value'] =  (isset($plan["{$language_name}_features"])) ? $plan["{$language_name}_features"] : '';

                            echo form_textarea($field_data); ?>

                        </div>
                    <?php endforeach; ?>
                    </div>
                </div>
            </div>

            <div><hr/></div>

            <div class="form-group row text-center">
                <div class="col-md-12 col-md-offset-1 col-xs-12">
                    <a class="btn btn-primary" href="<?php echo $cancel_url; ?>"><?php echo lang('core button cancel'); ?></a>
                    <button type="submit" name="submit" class="btn btn-info"><span
                            class="fa fa-check-square-o"></span> <?php echo lang('core button save'); ?></button>
                </div>
            </div>
            

            <?php echo form_close(); ?>
        </div>    
    </div>
</div>

<style type="text/css">
            
</style>