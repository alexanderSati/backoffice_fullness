<?php defined('BASEPATH') OR exit('No direct script access allowed'); ?>

<?php $lang = (isset($faq['language']) ? $faq['language'] : $this->config->item('language')); ?>

<div class="card px-3">
    <div class="card-header">
        <h4><?= $page_title ?></h4>                
    </div>  
        <div id="message-required" class="alert background-info alert-info alert-dismissable">
            <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
            <strong><?= lang('faqs msg required-input')?></strong>
        </div>
    <div class="card-block">

        <div class="card-content px-3">
            <?php echo form_open('', array('class' => 'form-horizontal form-label-left', 'data-parsley-validate' => '','role' => 'form')); ?>

            <?php // hidden id ?>
            <?php if (isset($faq_id)) : ?>
                <?php echo form_hidden('id', $faq_id); ?>
            <?php endif; ?>

            <div class="form-group row">
                <?php echo form_label("Usuarios Fundadores" . ' *', 'question', array('class' => 'col-sm-2 col-form-label')); ?>
                <div class="col-md-8 col-sm-8 col-xs-12">
                    
                    <ul class="nav nav-tabs" role="tablist">
                        <?php foreach ($this->session->languages as $language_key=>$language_name) : ?>
                            <li class="nav-item">
                                <a id="nav_<?php echo $language_key; ?>" class="nav-link<?php echo ($language_key == $this->session->language) ? ' active' : ''; ?>" data-toggle="tab" href="#<?php echo $language_key; ?>" role="tab"><?php echo $language_name; ?>&nbsp;&nbsp;<i class="fa fa-language"></i></a>
                            </li>
                        <?php endforeach; ?>
                    </ul>

                    <div class="tab-content tabs">
                        <?php foreach ($this->session->languages as $language_key=>$language_name) :?>
                            <?php $language_name = strtolower($language_name); ?>
                            <div role="tabpanel" class="tab-pane<?php echo ($language_key == $this->session->language) ? ' active' : ''; ?>" id="<?php echo $language_key; ?>">
                                <br />
                                <label for="<?= $language_name ?>_question"><?= /*lang("faqs input {$lang}_question")*/ "Usuario" ?></label>
                                <?php echo form_input(array('name' => "{$language_name}_question",'id' => "{$language_name}_question", 'required'=>'required', 'value' => set_value("{$language_name}_question", (isset($faq["{$language_name}_question"]) ? $faq["{$language_name}_question"] : '')), 'class' => 'form-control')); ?>
                                <br/>
                                <label for="<?= $language_name ?>_answer"><?= /*lang("faqs input {$lang}_answer")*/ "Descripción" ?></label>
                                <br/>
                                <?php
                                $field_data['name']  = $language_name ."_answer";
                                $field_data['id']    = $language_name ."_answer";
                                $field_data['class'] = "form-control editor";
                                $field_data['value'] =  (isset($faq["{$language_name}_answer"])) ? $faq["{$language_name}_answer"] : '';

                                echo form_textarea($field_data); ?>

                            </div>
                        <?php endforeach; ?>
                    </div>
                    
                </div>
            </div>

            <div class="form-radio row">
                <?php echo form_label(lang('faqs input status'), '', array('class'=>'col-sm-2 col-form-label')); ?>

                <div class="col-sm-8">
                    <div class="radio">
                        <label>
                            <?php echo form_radio(array('name'=>'status', 'id'=>'radio-status-1', 'class'=>'flat', 'value'=>'active', 'checked'=>(( ! isset($faq['status']) OR (isset($faq['status']) && $faq['status'] == 'active') OR $faq['id'] == 1) ? 'checked' : FALSE))); ?>
                            <i class="helper"></i><?php echo lang('admin input active'); ?>
                        </label>
                    </div>
                    <div class="radio">
                        <label>
                            <?php echo form_radio(array('name'=>'status', 'id'=>'radio-status-2', 'class'=>'flat', 'value'=>'inactive', 'checked'=>((isset($faq['status']) && $faq['status'] == 'inactive') ? 'checked' : FALSE))); ?>
                            <i class="helper"></i><?php echo lang('admin input inactive'); ?>
                        </label>
                    </div>
                </div>
            </div>
            
            <div class="form-group row text-center">
                <div class="col-md-12 col-md-offset-1 col-xs-12">
                    <a class="btn btn-primary m-r-20" href="<?php echo $cancel_url; ?>"><?php echo lang('core button cancel'); ?></a>
                    <button id="tab-button" type="submit" name="submit" class="btn btn-success"><span
                            class="fa fa-check-square-o"></span> <?php echo lang('core button save'); ?></button>
                </div>
            </div>

            <?php echo form_close(); ?>
        </div>
       
    
    </div>
</div>
