<?php defined('BASEPATH') OR exit('No direct script access allowed'); ?>
<div class="card px-3">
    <div class="card-header">
        <h4><?= $page_title ?></h4>                       
    </div>
    <div class="card-block">
        <div class="row">
            <div class="col-sm-12 text-left m-b-20">
                <a class="btn btn-success" href="<?php echo base_url('admin/faqs/add'); ?>" title="<?php echo lang('faqs tooltip add') ?>" data-toggle="tooltip"><span class="fa fa-plus-circle"></span> <?php echo lang('faqs tooltip add'); ?></a>
            </div>
        </div>
            
        <div class="card-content">
            <div class="dt-responsive">
                <table class="table table-lg table-striped table-bordered nowrap">
                    <thead>
                        <?php // sortable headers ?>
                        <tr>
                            <td>
                                <a href="<?php echo current_url(); ?>?sort=id&dir=<?php echo (($dir == 'asc' ) ? 'desc' : 'asc'); ?>&limit=<?php echo $limit; ?>&offset=<?php echo $offset; ?><?php echo $filter; ?>"><?php echo lang('faqs col id'); ?></a>
                                <?php if ($sort == 'id') : ?><span class="glyphicon glyphicon-arrow-<?php echo (($dir == 'asc') ? 'up' : 'down'); ?>"></span><?php endif; ?>
                            </td>
                            <td>
                                <a href="<?php echo current_url(); ?>?sort={$lang}_question&dir=<?php echo (($dir == 'asc' ) ? 'desc' : 'asc'); ?>&limit=<?php echo $limit; ?>&offset=<?php echo $offset; ?><?php echo $filter; ?>"><?php echo lang("faqs col {$lang}_question"); ?></a>
                                <?php if ($sort == "{$lang}_question") : ?><span class="glyphicon glyphicon-arrow-<?php echo (($dir == 'asc') ? 'up' : 'down'); ?>"></span><?php endif; ?>
                            </td>
                            <td>
                                <a href="<?php echo current_url(); ?>?sort={$lang}_answer&dir=<?php echo (($dir == 'asc' ) ? 'desc' : 'asc'); ?>&limit=<?php echo $limit; ?>&offset=<?php echo $offset; ?><?php echo $filter; ?>"><?php echo lang("faqs col {$lang}_answer"); ?></a>
                                <?php if ($sort == "{$lang}_answer") : ?><span class="glyphicon glyphicon-arrow-<?php echo (($dir == 'asc') ? 'up' : 'down'); ?>"></span><?php endif; ?>
                            </td>
                            <td>
                                <a href="<?php echo current_url(); ?>?sort=status&dir=<?php echo (($dir == 'asc' ) ? 'desc' : 'asc'); ?>&limit=<?php echo $limit; ?>&offset=<?php echo $offset; ?><?php echo $filter; ?>"><?php echo lang('admin col status'); ?></a>
                                <?php if ($sort == 'status') : ?><span class="glyphicon glyphicon-arrow-<?php echo (($dir == 'asc') ? 'up' : 'down'); ?>"></span><?php endif; ?>
                            </td>
                            <td class="text-center">
                                <?php echo lang('admin col actions'); ?>
                            </td>
                        </tr>

                        <?php // search filters ?>
                        <tr>
                            <?php echo form_open("{$this_url}?sort={$sort}&dir={$dir}&limit={$limit}&offset=0{$filter}", array('role'=>'form', 'id'=>"filters")); ?>
                            <th>
                            </th>
                            <th <?php echo ((isset($filters["{$lang}_question"])) ? ' class="has-success"' : ''); ?>>
                                <?php echo form_input(array('name'=>"{$lang}_question", 'id'=>"{$lang}_question", 'class'=>'form-control input-sm', 'placeholder'=>lang("faqs input {$lang}_question"), 'value'=>set_value("{$lang}_question", ((isset($filters["{$lang}_question"])) ? $filters["{$lang}_question"] : '')))); ?>
                            </th>
                            <th <?php echo ((isset($filters["{$lang}_answer"])) ? ' class="has-success"' : ''); ?>>
                                <?php echo form_input(array('name'=>"{$lang}_answer", 'id'=>"{$lang}_answer", 'class'=>'form-control input-sm', 'placeholder'=>lang("faqs input {$lang}_answer"), 'value'=>set_value("{$lang}_answer", ((isset($filters["{$lang}_answer"])) ? $filters["{$lang}_answer"] : '')))); ?>
                            </th>
                            <th></th>
                            <th>
                                <div class="text-center">
                                    <a href="<?php echo $this_url; ?>" class="btn btn-danger" data-toggle="tooltip" title="<?php echo lang('admin tooltip filter_reset'); ?>"><span class="fa fa-refresh"></span> <?php echo lang('core button reset'); ?></a>
                                    <button type="submit" name="submit" value="<?php echo lang('core button filter'); ?>" class="btn btn-success" data-toggle="tooltip" title="<?php echo lang('admin tooltip filter'); ?>"><span class="fa fa-filter"></span> <?php echo lang('core button filter'); ?></button>
                                </div>
                            </th>
                            <?php echo form_close(); ?>
                        </tr>
                    </thead>
                    <tbody>

                    <?php // data rows ?>
                    <?php if ($total) : ?>
                        <?php foreach ($faqs as $faq) : ?>
                            <tr>
                                <td <?php echo (($sort == 'id') ? ' class="sorted"' : ''); ?>>
                                    <?php echo $faq['id']; ?>
                                </td>
                                <td <?php echo (($sort == "{$lang}_question") ? ' class="sorted"' : ''); ?>>
                                    <?php echo $faq["{$lang}_question"]; ?>
                                </td>
                                <td  <?php echo (($sort == "{$lang}_answer") ? '' : ''); ?>>
                                    <?php echo $faq["{$lang}_answer"],'<p><span><b><ul><li>'; ?>
                                </td>
                                <td <?php echo (($sort == 'status') ? '' : ''); ?>>
                                    <?php echo ($faq['status'] == "active") ? '<span class="active">' . lang('admin input active') . '</span>' : '<span class="inactive">' . lang('admin input inactive') . '</span>'; ?>
                                </td>
                                <td>
                                    <div class="text-center">
                                        <div class="btn-group">
                                                <a href="#modal-<?php echo $faq['id']; ?>" data-toggle="modal" class="btn btn-danger btn-icon" title="<?php echo lang('admin button delete'); ?>"><span class="fa fa-trash-o"></span></a>
                                           
                                            <a href="<?php echo $this_url; ?>/edit/<?php echo $faq['id']; ?>" class="btn btn-warning btn-icon" title="<?php echo lang('admin button edit'); ?>"><span class="fa fa-pencil"></span></a>
                                        </div>
                                    </div>
                                </td>
                            </tr>
                        <?php endforeach; ?>
                    <?php else : ?>
                        <tr>
                            <td>
                                <?php echo lang('core error no_results'); ?>
                            </td>
                        </tr>
                    <?php endif; ?>

                    </tbody>
                </table>

                <?php // list tools ?>
                <div class="panel-footer">
                    <div class="row">
                        <div class="col-md-2 text-left">
                            <label><?php echo sprintf(lang('admin label rows'), $total); ?></label>
                        </div>
                        <div class="col-md-2 text-left">
                            <?php if ($total > 10) : ?>
                                <select id="limit" class="form-control">
                                    <option value="10"<?php echo ($limit == 10 OR ($limit != 10 && $limit != 25 && $limit != 50 && $limit != 75 && $limit != 100)) ? ' selected' : ''; ?>>10 <?php echo lang('admin input items_per_page'); ?></option>
                                    <option value="25"<?php echo ($limit == 25) ? ' selected' : ''; ?>>25 <?php echo lang('admin input items_per_page'); ?></option>
                                    <option value="50"<?php echo ($limit == 50) ? ' selected' : ''; ?>>50 <?php echo lang('admin input items_per_page'); ?></option>
                                    <option value="75"<?php echo ($limit == 75) ? ' selected' : ''; ?>>75 <?php echo lang('admin input items_per_page'); ?></option>
                                    <option value="100"<?php echo ($limit == 100) ? ' selected' : ''; ?>>100 <?php echo lang('admin input items_per_page'); ?></option>
                                </select>
                            <?php endif; ?>
                        </div>
                        <div class="col-md-6 pagination justify-content-center">
                            <?php echo $pagination; ?>
                        </div>
                        <div class="col-md-2 text-right">
                            <?php if ($total) : ?>
                                <a href="<?php echo $this_url; ?>/export?sort=<?php echo $sort; ?>&dir=<?php echo $dir; ?><?php echo $filter; ?>" class="btn btn-success" data-toggle="tooltip" title="<?php echo lang('admin tooltip csv_export'); ?>"><span class="fa fa-download"></span> <?php echo lang('admin button csv_export'); ?></a>
                            <?php endif; ?>
                        </div>
                    </div>
                </div>

            </div>

            <?php // delete modal ?>
            <?php if ($total) : ?>
                <?php foreach ($faqs as $faq) : ?>
                    <div class="modal fade" id="modal-<?php echo $faq['id']; ?>" tabindex="-1" role="dialog" aria-labelledby="modal-label-<?php echo $faq['id']; ?>" aria-hidden="true">
                        <div class="modal-dialog">
                            <div class="modal-content">
                                <div class="modal-header">
                                    <h5 id="modal-label-<?php echo $faq['id']; ?>"><?php echo lang('faqs msg user_delete');  ?></h5>
                                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                                </div>
                                <div class="modal-body">
                                    <p><?php echo lang('faqs msg delete_confirm'); ?></p>
                                </div>
                                <div class="modal-footer">
                                    <button type="button" class="btn btn-default" data-dismiss="modal"><?php echo lang('core button cancel'); ?></button>
                                    <a href="<?php echo $this_url; ?>/delete/<?= $faq['id']; ?>" class="btn btn-info btn-delete-faq" data-id="<?php echo $faq['id'];  ?>"><?php echo lang('admin button delete'); ?></a>
                                </div>
                            </div>
                        </div>
                    </div>
                <?php endforeach; ?>
            <?php endif; ?>
        </div>   
    </div>
</div>
<style>
/**Tabla responsive **/
 
     table {
       display: block;
       overflow-x: auto;
     }
}
</style>

