<?php defined('BASEPATH') OR exit('No direct script access allowed'); ?>


<div class="card px-5">
    <div class="card-header">
        <h4><?= $page_title ?></h4>                    
    </div>
    <div class="card-block">
        

    <?php echo form_open('', array('role'=>'form')); ?>
        <?php $con = 0; ?>
        <?php foreach ($settings as $setting) : ?>        

            <?php // prepare field settings
            $field_data = array();

            if ($setting['is_numeric'])
            {
                $field_data['type'] = "number";
                $field_data['step'] = "any";
            }

            if ($setting['options'])
            {
                $field_options = array();
                if ($setting['input_type'] == "dropdown")
                {
                    $field_options[''] = lang('admin input select');
                }
                $lines = explode("\n", $setting['options']);
                foreach ($lines as $line)
                {
                    $option = explode("|", $line);
                    $field_options[$option[0]] = $option[1];
                }
            }

            switch ($setting['input_size'])
            {
                case "small":
                    $col_size = "col-sm-12";
                    break;
                case "medium":
                    $col_size = "col-sm-11";
                    break;
                case "large":
                    $col_size = "col-sm-10";
                    break;
                default:
                    $col_size = "col-sm-10";
            }

            if ($setting['input_type'] == 'textarea')
            {
                $col_size = "col-sm-11";
            }
            ?>

            <?php if ($setting['translate'] && $this->session->languages && $this->session->language) : ?>

                <?php // has translations ?>
                <?php
                $setting['value'] = (@unserialize($setting['value']) !== FALSE) ? unserialize($setting['value']) : $setting['value'];
                if ( ! is_array($setting['value']))
                {
                    $old_value = $setting['value'];
                    $setting['value'] = array();
                    foreach ($this->session->languages as $language_key=>$language_name)
                    {
                        $setting['value'][$language_key] = ($language_key == $this->session->language) ? $old_value : "";
                    }
                }
                ?>
               
                <div class="form-group row <?php echo $col_size; ?><?php echo form_error($setting['name']) ? ' has-error' : ''; ?>">
                    <?php echo form_label((strpos($setting['validation'], 'required') !== FALSE)? lang($setting['label']).' *' : lang($setting['label']), $setting['name'], array('class'=>'col-sm-3 col-form-label')); ?>
                    <!-- <?php if (strpos($setting['validation'], 'required') !== FALSE) : ?>
                        <span class="required">*</span>
                    <?php endif; ?> -->
                    
                    <div class="col-sm-8">
                        <ul class="nav nav-tabs" role="tablist">
                            <?php foreach ($this->session->languages as $language_key=>$language_name) : ?>
                                <li class="nav-item">
                                    <a class="nav-link<?php echo ($language_key == $this->session->language) ? ' active' : ''; ?>" data-toggle="tab" href="#<?php echo $language_key . $con; ?>" role="tab"><?php echo $language_name; ?></a>
                                </li>
                            <?php endforeach; ?>
                        </ul>

                        <div class="tab-content tabs">
                            <?php foreach ($this->session->languages as $language_key=>$language_name) : ?>
                                <div role="tabpanel" class="tab-pane<?php echo ($language_key == $this->session->language) ? ' active' : ''; ?>" id="<?php echo $language_key . $con; ?>">
                                    <br />
                                    <?php
                                    $field_data['name']  = $setting['name'] . "[" . $language_key . "]";
                                    $field_data['id']    = $setting['name'] . "-" . $language_key;
                                    $field_data['class'] = "form-control" . (($setting['show_editor']) ? " editor" : "");
                                    $field_data['value'] = (@$setting['value'][$language_key]) ? $setting['value'][$language_key] : '';

                                    // render the correct input method
                                    if ($setting['input_type'] == 'input')
                                    {
                                        echo '<div class="col-sm-8">';
                                        echo form_input($field_data);
                                        echo '</div>';
                                    }
                                    elseif ($setting['input_type'] == 'textarea')
                                    {
                                        echo '<div class="col-sm-8">';
                                        $field_data['class'] = "form-control editor";
                                        echo form_textarea($field_data);                                                  
                                        echo '</div>';
                                    }
                                    elseif ($setting['input_type'] == 'radio')
                                    {
                                        echo '<div class="col-sm-8">';
                                        foreach ($field_options as $value=>$label)
                                        {
                                            echo '<div class="form-radio"><div class="radio"><label>';
                                            echo form_radio(array('name'=>$field_data['name'], 'id'=>$field_data['id'] . "-" . $value, 'class'=>'flat', 'value'=>$value, 'checked'=>(($value == $field_data['value']) ? 'checked' : FALSE)));
                                            echo '<i class="helper"></i>'.lang(trim($label)) . " ";
                                            echo '</label></div></div>';
                                        }
                                        echo '</div>';
                                    }
                                    elseif ($setting['input_type'] == 'dropdown')
                                    {
                                        echo '<div class="col-sm-8">';
                                        echo form_dropdown($setting['name'], $field_options, $field_data['value'], 'id="' . $field_data['id'] . '" class="' . $field_data['class'] . '"');
                                        echo '</div>';
                                    }
                                    elseif ($setting['input_type'] == 'timezones')
                                    {
                                        echo '<div class="col-sm-8">';
                                        echo timezone_menu($field_data['value'], "form-control");
                                        echo '</div>';
                                    }
                                    ?>
                                </div>
                            <?php endforeach; ?>
                        </div>
                    </div>

                    <?php if ($setting['help_text']) : ?>
                        <label class="col-sm-3 col-form-label"></label>
                        <div class="col-sm-8 text-center">
                            <span class="f-s-italic"><?php echo lang($setting['help_text']); ?></span>
                        </div>
                    <?php endif; ?>
                </div>                

            <?php else : ?>

                <?php // no translations
                $field_data['name']  = $setting['name'];
                $field_data['id']    = $setting['name'];
                $field_data['class'] = "form-control" . (($setting['show_editor']) ? " editor" : "");
                $field_data['value'] = $setting['value'];
                ?>
                
                <div class="form-group row <?php echo $col_size; ?><?php echo form_error($setting['name']) ? ' has-error' : ''; ?>">
                    <?php echo form_label((strpos($setting['validation'], 'required') !== FALSE)? lang($setting['label']).' *' : lang($setting['label']), $setting['name'], array('class'=>'col-sm-3 col-form-label')); ?>
                    <!-- <?php if (strpos($setting['validation'], 'required') !== FALSE) : ?>
                        <span class="required">*</span>
                    <?php endif; ?> -->

                    <?php // render the correct input method
                    if ($setting['input_type'] == 'input')
                    {
                        echo '<div class="col-sm-8">';
                        echo form_input($field_data);
                        echo '</div>';
                    }
                    elseif ($setting['input_type'] == 'textarea')
                    {
                        echo '<div class="col-sm-8">';
                        echo form_textarea($field_data);
                        echo '</div>';
                    }
                    elseif ($setting['input_type'] == 'radio')
                    {
                        echo '<div class="col-sm-8">';

                        foreach ($field_options as $value=>$label)
                        {
                            echo '<div class="form-radio"><div class="radio"><label>';
                            echo form_radio(array('name'=>$field_data['name'], 'id'=>$field_data['id'] . "-" . $value, 'class'=>'flat', 'value'=>$value, 'checked'=>(($value == $field_data['value']) ? 'checked' : FALSE)));
                            echo '<i class="helper"></i>'.lang(trim($label)) . " ";
                            echo '</label></div></div>';
                        }
                        echo '</div>';
                    }
                    elseif ($setting['input_type'] == 'dropdown')
                    {
                        echo '<div class="col-sm-8">';
                        echo form_dropdown($setting['name'], $field_options, $field_data['value'], 'id="' . $field_data['id'] . '" class="' . $field_data['class'] . '"');
                        echo '</div>';
                    }
                    elseif ($setting['input_type'] == 'timezones')
                    {
                        echo '<div class="col-sm-8">';
                        echo timezone_menu($field_data['value'], "form-control");
                        echo '</div>';
                    }
                    ?>

                    <?php if ($setting['help_text']) : ?>                    
                        <label class="col-sm-3 col-form-label"></label>
                        <div class="col-sm-8 text-center">
                            <span class="f-s-italic"><?php echo lang($setting['help_text']); ?></span>
                        </div>
                    <?php endif; ?>
                </div>
                
            <?php endif; ?>
            <?php $con ++; ?>
        <?php endforeach; ?>

        <div><hr/></div>

        <?php if (is_allowed('update')){ ?>
            <div class="row text-center">
                <div class="form-group col-sm-12">
                    <a class="btn btn-primary m-r-20" href="<?php echo $cancel_url; ?>"><?php echo lang('core button cancel'); ?></a>
                    <button type="submit" name="submit" class="btn btn-info"><span class="fa fa-check-square-o"></span> <?php echo lang('core button save'); ?></button>
                </div>
            </div>
        <?php } ?>

        <div class="row"><br /></div>

    <?php echo form_close(); ?>
    </div>
    <!-- end of card-block -->            
</div>
            