<?php defined('BASEPATH') OR exit('No direct script access allowed'); ?>

<div class="card px-3">
    <div class="card-header borderless ">
        <h4><?= $page_title ?></h4>
    </div>
    <div class="card-block">
        <div class="table-responsive">
            <table id="order-table" class="table table-striped table-bordered nowrap">
                <thead>
                    <tr>
                        <td>
                            <?= lang('code')?>
                        </td>
                        <td>
                            <?= lang('users input dni')?>
                        </td>
                        <td>
                            <?= lang('users col username') ?>
                        </td>
                        <td>
                            <?= lang('date')?>
                        </td>
                        <td>
                            <?= lang('users col name') ?>
                        </td>
                        <td>
                            <?= lang('users col amount') ?>
                        </td>
                        <td>
                            <?= lang('admin col status') ?>
                        </td>
                        <td>
                            <?= lang('admin col approve') ?>
                        </td>
                        <td>
                            <?= lang('admin col denied')?>
                        </td>
                    </tr>
                </thead>
                <tbody>
                    <?php foreach($payouts as $payout):?>
                        <tr>
                            <td>
                                <?=$payout["cod"];?>
                            </td>
                            <td>
                                <?=$payout["dni"];?>
                            </td>
                            <td>
                                <?=$payout["username"];?>
                            </td>
                            <td>
                                <?=$payout["last_updated"];?>
                            </td>
                            <td>
                                <?=$payout["first_name"]." ".$payout["last_name"];?>
                            </td>
                            <td>
                                <?=$payout["usd"];?>
                            </td>
                            <td>
                            <?php
                                if ($payout['status'] == "done") {
                                    echo "<span class='pcoded-badge label label-success'>" . $payout['status'] . "</span>";
                                } elseif ($payout['status'] == "pending" || $payout['status'] == "waiting") {
                                    echo "<span class='pcoded-badge label label-warning'>" . $payout['status'] . "</span>";
                                } else {
                                    echo "<span class='pcoded-badge label label-danger'>" . $payout['status'] . "</span>";
                                }
                                ?>
                            </td>
                            <td>
                                <?php if( $payout["status"] == "pending" ): ?>
                                    <button class="btn btn-success btn-icon" onclick="modalData(1,<?php echo $payout['id'];?>)" data-toggle="modal" data-target="#modal-default">
                                    <i class="fa fa-check"></i>
                                    </button>
                                    <?php endif; ?>
                            </td>
                            <td>
                                <?php if( $payout["status"] == "pending" ): ?>
                                    <button class="btn btn-danger btn-icon" onclick="modalData(0,<?php echo $payout['id'];?>)" data-toggle="modal" data-target="#modal-default">
                                    <i class="fa fa-times"></i>
                                    </button>
                                    <?php endif; ?>
                            </td>
                        </tr>
                        <?php endforeach;?>
                </tbody>
            </table>

        </div>
    </div>

    <div class="modal fade" id="modal-default" style="display: none;">
      <div class="modal-dialog">
        <div class="modal-content">
          <div class="modal-header">              
            <h5 class="modal-title" id="modal-title"></h5>
            <button type="button" class="close" data-dismiss="modal" aria-label="Close" onclick="clean()">
                <span aria-hidden="true">×</span>
            </button>
          </div>
          <div class="modal-footer">
            <button type="button" class="btn btn-danger pull-left" data-dismiss="modal" onclick="clean()"><?php echo lang('users button cancel'); ?></button>
            <a href="" class="btn btn-success" id="payout-confirm"><?php echo lang('users button confirm'); ?></a>
          </div>
        </div>
        <!-- /.modal-content -->
      </div>
      <!-- /.modal-dialog -->
     </div>
    </div>
    <script>
       function modalData(type,id) {                  
         
          const a = document.getElementById('payout-confirm');
          const title = document.getElementById('modal-title');

          if (type === 0) {
              a.href=`<?= base_url();?>admin/transactions/deny/${id}`;
              title.innerText = '<?php echo lang('users msg deny-sure')?>';
          }
          else {
              a.href=`<?= base_url();?>admin/transactions/approve/${id}`;
              title.innerText = '<?php echo lang('users msg aprrove-sure')?>';
          }
      }

      function clean() {
        const a = document.getElementById('payout-confirm');
        const title = document.getElementById('modal-title');
        a.href = "";
        title.innerText = "";
               
    }
    </script>
