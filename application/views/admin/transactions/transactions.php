<?php if(false): ?>
<div class="card px-3">
  <div class="card-header">
    <h4><?= lang('admin title pending transactions') ?></h4>        
  </div>
  <div class="card-block">      
    <div class="card-content">
    <div class="dt-responsive table-responsive">        
        <table id="admin-transactions" class="table table-striped table-bordered nowrap">
          <thead>
            <tr>
              <th><?= lang('status') ?></th>
              <th><?= lang('username') ?></th>
              <th><?= lang('t_user') ?></th>
              <th><?= lang('email') ?></th>
              <th><?= lang('date') ?></th>
              <th><?= lang('code') ?></th>
              <th><?= lang('description') ?></th>       
              <th><?= lang('amount') ?></th>
            </tr>
          </thead>
          <tbody>
            <?php foreach ($pending_transactions as $transaction) { ?>
            <tr>
              <td>
                <select name="transaction-status" rowid="<?= $transaction['id'] ?>"
                  class="form-control transaction-status">
                  <option value="done" <?= ($transaction['status'] == 'done') ? 'selected' : '' ?>>Done</option>
                  <option value="pending" <?= ($transaction['status'] == 'pending') ? 'selected' : '' ?>>Pending
                  </option>
                  <option value="failed" <?= ($transaction['status'] == 'failed') ? 'selected' : '' ?>>Failed</option>
                  <option value="cancelled" <?= ($transaction['status'] == 'cancelled') ? 'selected' : '' ?>>Cancelled
                  </option>
                  <option value="rejected" <?= ($transaction['status'] == 'rejected') ? 'selected' : '' ?>>Rejected
                  </option>
                </select>
              </td>
              <td><?= $transaction['username'] ?></td>
              <td><?= $transaction['name_user'] ?></td>
              <td><?= $transaction['email'] ?></td>
              <td><?= $transaction['date'] ?></td>
              <td><?= $transaction['cod'] ?></td>
              <td>
                <?php 
                  echo $transaction['description'];
                      if($transaction['hash'] != ''){ 
                  ?>
                Deposit Hash : <span class="badge bg-blue"><?= $transaction['hash'] ?></span>
                <?php
                  }
                ?>
              </td>
              <td><?= $transaction['usd'] ?>
                <sub>S/</sub></td>
            </tr>
            <?php } ?>
          </tbody>
        </table>
      </div>
    </div>
  </div>
</div>
<?php endif; ?>
    
  

<div class="card px-3">
  <div class="card-header">
    <h4><?= lang('admin title transactions') ?></h4>        
  </div>
  <div class="card-block">          
    <div class="card-content">     
      <div class="dt-responsive table-responsive">
        <table id="datatable-responsive" class="table table-striped table-bordered nowrap">
          <thead>
            <tr>
              <th><?= lang('date') ?></th>
              <th><?= lang('users col fullname') ?></th>
              <th><?= lang('username') ?></th>
              <th><?= lang('code') ?></th>
              <th><?= lang('description') ?></th>  
              <th><?= lang('amount') ?></th>
              <th><?= lang('status') ?></th>
            </tr>
          </thead>
          <tbody>
            <?php foreach ($transactions as $transaction) {
                        if ($transaction['status'] != "pending") { ?>
            <tr>
              <td><?= $transaction['date'] ?></td>
              <td><?= $transaction['name_user'] ?></td>
              <td><?= $transaction['username']?></td>
              <td><?= $transaction['cod'] ?></td>
              <td><?= $transaction['description'] ?></td>
              <td><?= $transaction['usd'] ?>
                <sub>S/</td>
              <td>
              <?php
                if ($transaction['status'] == "done") {
                    echo "<span class='pcoded-badge label label-success'>" . $transaction['status'] . "</span>";
                } elseif ($transaction['status'] == "pending" || $transaction['status'] == "waiting") {
                    echo "<span class='pcoded-badge label label-warning'>" . $transaction['status'] . "</span>";
                } else {
                    echo "<span class='pcoded-badge label label-danger'>" . $transaction['status'] . "</span>";
                }
                ?>
              </td>
            </tr>
            <?php }
                    }?>
          </tbody>
        </table>
      </div>
    </div>   
  </div>
</div>

<style>
  #sailorTableArea{
    max-height: 500px;
    overflow-x: auto;
    overflow-y: auto;
  }
  #admin-all{
    white-space: nowrap;
  }
</style>