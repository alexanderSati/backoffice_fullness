<?php defined('BASEPATH') OR exit('No direct script access allowed');

$lang = (isset($user['language']) ? $user['language'] : $this->config->item('language'));
$origin_wallets = array("XMR"=>"Monero", "ETH"=>"Ethereum", "LTC"=>"Litecoin","DASH"=>"Dash", "BTC"=>"Bitcoin");

?>

<div class="wrapper_payment">
<div class="animate form">
    <section class="payment">

        <div class="alerts_messages">
            <?php // System messages ?>
            <?php if ($this->session->flashdata('message')) : ?>
                <div class="alert alert-success alert-dismissable">
                    <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
                    <?php echo $this->session->flashdata('message'); ?>
                </div>
            <?php elseif ($this->session->flashdata('error')) : ?>
                <div class="alert alert-danger alert-dismissable">
                    <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
                    <?php echo $this->session->flashdata('error'); ?>
                </div>
            <?php elseif (validation_errors()) : ?>
                <div class="alert alert-danger alert-dismissable">
                    <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
                    <?php echo validation_errors(); ?>
                </div>
            <?php elseif ($this->error) : ?>
                <div class="alert alert-danger alert-dismissable">
                    <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
                    <?php echo $this->error; ?>
                </div>
            <?php endif; ?>
        </div>
        <div class="login_content">
            <h1><?= $page_title ?></h1>

            <h4><?= lang('your_plan'); ?></h4>

            <div class="container show_plan">
                <div class="">

                    <div class="">
                            <!-- price element -->
                            <div class=" col-xs-12 element-pricing">
                                <div class="pricing">
                                    <div class="title">
                                        <h2><?= $current_plan["{$lang}_name"] ?></h2>
                                        <h2 style="font-size: 28px">S/<?= $current_plan["base"] ?></h2>
                                    </div>
                                    <div class="x_content">
                                        <div class="">
                                            <div class="pricing_features">
                                                <ul class="list-unstyled text-left">
                                                    <li><?= $current_plan["{$lang}_features"] ?></li>
                                                </ul>
                                            </div>
                                        </div>

                                    </div>
                                </div>
                            </div>
                            <!-- price element -->
                    </div>
                </div>
            </div>
        </div>
        <?php echo form_open('', array('class' => 'form-horizontal form-label-left', 'data-parsley-validate' => '','role' => 'form')); ?>
        <div class="item form-group col-xs-12 col-md-12">
            <?php echo form_label(lang('select-coin') . ': ', 'coin', array('class' => 'col-md-3 col-md-offset-2 col-xs-12 text-center')); ?>
            <select name="coin" id="coin" class="select-custom type-coin col-md-5 col-xs-12">
                <?php foreach ($origin_wallets as $key => $value) { ?>
                    <option value="<?= $key; ?>"><?= $value; ?></option>
                <?php } ?>
            </select>
        </div>
        
        <h4 class="col-md-12 col-xs-12 text-center"><?= lang('users title register qr_to_pay'); ?></h4>
        
        <div class="col-md-12 col-xs-12 display_qr">
            
        </div>
        
        <div class="form-group">
            <div class="col-md-12 col-xs-12">
                <button type="submit" name="submit" class="col-md-6 col-xs-12 m-0-a btn btn-success"><span class="glyphicon glyphicon-save"></span> <?php echo lang('renew'); ?></button>
                <a class="col-md-6 col-xs-12 m-0-a btn btn-primary cancel" href="<?= base_url(); ?>"><?= lang('back_to_home');?></a>
            </div>
        </div>

        <?php echo form_close(); ?>
        
        <div class="clearfix"></div>
        <br />
        <div class="login_content">
            <div>
                <h1><?php echo $this->settings->site_name; ?></h1>
            </div>
        </div>
    </section>    
</div>
</div>
