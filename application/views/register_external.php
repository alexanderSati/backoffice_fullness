<?php defined('BASEPATH') OR exit('No direct script access allowed');
$lang = (isset($user['language']) ? $user['language'] : $this->config->item('language'));

?>
<div themebg-pattern="theme1">
  <!-- Pre-loader start -->
  <div class="theme-loader">
    <div class="loader-track">
      <div class="preloader-wrapper">
        <div class="spinner-layer spinner-blue">
          <div class="circle-clipper left">
            <div class="circle"></div>
          </div>
          <div class="gap-patch">
            <div class="circle"></div>
          </div>
          <div class="circle-clipper right">
            <div class="circle"></div>
          </div>
        </div>
        <div class="spinner-layer spinner-red">
          <div class="circle-clipper left">
            <div class="circle"></div>
          </div>
          <div class="gap-patch">
            <div class="circle"></div>
          </div>
          <div class="circle-clipper right">
            <div class="circle"></div>
          </div>
        </div>

        <div class="spinner-layer spinner-yellow">
          <div class="circle-clipper left">
            <div class="circle"></div>
          </div>
          <div class="gap-patch">
            <div class="circle"></div>
          </div>
          <div class="circle-clipper right">
            <div class="circle"></div>
          </div>
        </div>

        <div class="spinner-layer spinner-green">
          <div class="circle-clipper left">
            <div class="circle"></div>
          </div>
          <div class="gap-patch">
            <div class="circle"></div>
          </div>
          <div class="circle-clipper right">
            <div class="circle"></div>
          </div>
        </div>
      </div>
    </div>
  </div>
  <!-- Pre-loader end -->
  
  <!--Section start -->
  <section class="login-block">
    <!-- Container-fluid starts -->
     <div class="container-fluid">
      <div class="row">
        <div class="col-sm-12">

           <?php echo form_open_multipart($this->uri->uri_string(), array('class' => 'md-float-material form-material', 'data-parsley-validate' => '','role' => 'form')); ?>
              <div class="text-center">
                <img width="200" src="/themes/able/images/logo.png" alt="logo">
              </div>
              <div class="auth-box card">
                <div class="card-block">
                  <!-- row text-left start -->
                  <div class="row text-left">
                    <div class="col-12">
                      <div class="text-center"> <h2><?= $page_title ?></h2></div>
                    </div>                    
                  </div>
                  <!-- rowe texft end --->
                
                  <!---System menssage start -->
                  <?php if ($this->session->flashdata('message')) : ?>
                    <div class="alert alert-success alert-dismissable">
                      <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
                      <?php echo $this->session->flashdata('message'); ?>
                    </div>
                    <?php elseif ($this->session->flashdata('error')) : ?>
                    <div class="alert alert-danger alert-dismissable">
                      <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
                      <?php echo $this->session->flashdata('error'); ?>
                    </div>
                    <?php elseif (validation_errors()) : ?>
                    <div class="alert alert-danger alert-dismissable">
                      <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
                      <?php echo validation_errors(); ?>
                    </div>
                    <?php elseif ($this->error) : ?>
                    <div class="alert alert-danger alert-dismissable">
                      <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
                      <?php echo $this->error; ?>
                    </div>
                  <?php endif; ?>
                  <!--- system menssage end -->
                  
                  <!----DNI--->
                  <div class="form-group form-primary m-t-25">
                    <?php echo form_input(array('name' => 'dni','required'=>'required', 'value' => set_value('dni', (isset($user['dni']) ? $user['dni'] : '')), 'class' => 'form-control')); ?>
                    <span class="form-bar"></span>
                    <?php echo form_label(lang('users input dni') . ' *', 'dni', array('class' => 'float-label')); ?>          
                  </div>
                  <!----./DNI--->

                  <!-- FIRSTNAME -->
                  <div class="form-group form-primary">
                    <?php echo form_input(array('name' => 'first_name','required'=>'required', 'value' => set_value('first_name', (isset($user['first_name']) ? $user['first_name'] : '')), 'class' => 'form-control')); ?>
                    <span class="form-bar"></span>
                    <?php echo form_label(lang('users input first_name') . ' *', 'first_name', array('class' => 'float-label')); ?>
                  </div>
                  <!-- ./FIRSTNAME -->

                  <!-- LASTNAME -->
                  <div class="form-group form-primary">
                    <?php echo form_input(array('name' => 'last_name','required'=>'required', 'value' => set_value('last_name', (isset($user['last_name']) ? $user['last_name'] : '')), 'class' => 'form-control')); ?>    
                    <span class="form-bar"></span>                  
                    <?php echo form_label(lang('users input last_name') . ' *', 'last_name', array('class' => 'float-label')); ?>             
                  </div>        
                  <!-- ./LASTNAME -->

                  <!-- EMAIL -->
                  <div class="form-group form-primary">
                    <?php echo form_input(array('name' => 'email','required'=>'required', 'value' => set_value('email', (isset($user['email']) ? $user['email'] : '')), 'class' => 'form-control', 'type' => 'email')); ?>
                    <span class="form-bar"></span>                                    
                    <?php echo form_label(lang('users input email') . ' *', 'email', array('class' => 'float-label')); ?>
                  </div>
                  <!-- ./EMAIL -->

                  <!--------BIRTHDATE-------->
                  <?php if(false): ?>
                    <div class="form-group form-primary">
                      <?php echo form_label(lang('users input birthdate') . ' *', 'birthdate', array('class' => 'control-label col-md-5 col-sm-5 col-xs-12')); ?>
                      <div class="col-md-3 col-sm-3 col-xs-12">
                        <?php echo form_input(array('name' => 'birthdate','required'=>'required', 'value' => set_value('birthdate', (isset($user['birthdate']) ? $user['birthdate'] : '')), 'class' => 'form-control col-md-7 col-xs-12', 'type' => 'date')); ?>
                      </div>
                    </div>
                  <?php endif; ?>
                  <!------- ./BIRTHDATE ------->

                  <!--------PHONE------------>
                  <div class="form-group form-primary">
                    <?php echo form_input(array('name' => 'phone','required'=>'required', 'value' => set_value('phone', (isset($user['phone']) ? $user['phone'] : '')), 'class' => 'form-control', 'type' => 'text')); ?>
                    <span class="form-bar"></span>  
                    <?php echo form_label(lang('users input phone') . ' *', 'phone', array('class' => 'float-label')); ?>
                  </div>
                  <!--------./PHONE------------>

                  <!--------ADDRESS---------->
                  <div class="form-group form-primary">
                    <?php echo form_input(array('name' => 'address','required'=>'required', 'value' => set_value('address', (isset($user['address']) ? $user['address'] : '')), 'class' => 'form-control', 'type' => 'text')); ?>           
                    <span class="form-bar"></span>                    
                    <?php echo form_label(lang('users input address') . ' *', 'address', array('class' => 'float-label')); ?>
                  </div>
                  <!------ ./ADDRESS --------->

                  <!--------ADDRESS 2---------->
                  <div class="form-group form-primary">
                    <?php echo form_input(array('name' => 'address2','value' => set_value('address2', (isset($user['address2']) ? $user['address2'] : '')), 'class' => 'form-control', 'type' => 'text')); ?>
                    <span class="form-bar"></span>  
                    <?php echo form_label(lang('users input address2'), 'address2', array('class' => 'float-label')); ?>
                  </div>
                  <!------ ./ADDRESS 2---------->

                  <!--------PAIS--------------->
                  <div class="form-group form-primary form-static-label">
                    <?php echo form_dropdown('country', $countries, false, 'id="country" required="required" class="select2_single form-control"'); ?>
                    <span class="form-bar"></span>  
                    <?php echo form_label(lang('users input country'), 'country', array('class' => 'float-label')); ?> 
                  </div>
                  <!--------./PAIS--------------->

                  <!--------DEPARTMENT --------------->
                  <div class="form-group form-primary">
                    <?php echo form_input(array('name' => 'department','required'=>'required' ,'value' => set_value('department', (isset($user['department']) ? $user['department'] : '')), 'class' => 'form-control', 'type' => 'text')); ?>
                    <span class="form-bar"></span>  
                    <?php echo form_label(lang('users input department'). ' *', 'department', array('class' => 'float-label')); ?>
                  </div>
                  <!--------./DEPARTMENT --------------->

                  <!--------CITY--------------->
                  <div class="form-group form-primary">
                    <?php echo form_input(array('name' => 'city', 'required'=>'required', 'value' => set_value('city', (isset($user['city']) ? $user['city'] : '')), 'class' => 'form-control', 'type' => 'text')); ?>
                    <span class="form-bar"></span>              
                    <?php echo form_label(lang('users input city'). ' *', 'city', array('class' => 'float-label')); ?>
                  </div>
                  <!------ ./CITY--------------->

                  <!--------LANGUAGE ----------->
                  <div class="form-group form-primary form-static-label">
                    <?php echo form_dropdown('language', $this->languages, $lang, 'id="language" required="required" class="form-control"'); ?>
                    <span class="form-bar"></span>
                    <?php echo form_label(lang('users input language'), 'language', array('class' => 'float-label')); ?>
                  </div>
                  <!-------- ./LANGUAGE ------->

                  <!--------POSTAL CODE------------->
                  <div class="form-group form-primary">
                    <?php echo form_input(array('name' => 'postal_code','required'=>'required','value' => set_value('postal_code', (isset($user['postal_code']) ? $user['postal_code'] : '')), 'class' => 'form-control', 'type' => 'text')); ?>
                    <span class="form-bar"></span>              
                    <?php echo form_label(lang('users input postal_code'). ' *', 'postal_code', array('class' => 'float-label')); ?> 
                  </div>
                  <!--------./POSTAL CODE------------->

                  <!-- TERMS AND CONDITIONS -->
                  <div class="col-md-12 text-center">
                    <div class="checkbox-fade fade-in-primary">
                      <label>
                        <?php echo form_checkbox(array('name' => 'terms', 'id'=>'terms', 'class' => 'form-control flat','checked'=> FALSE,'value'=> 'off','required'=>'required')); ?>
                        <span class="cr"><i class="cr-icon icofont icofont-ui-check txt-info"></i></span>
                        <span class="text-inverse"><?= lang('users input terms'); ?></span>
                      </label>
                    </div>
                  </div>
                  <!-- ./TERMS AND CONDITIONS -->


                  <!-- RECAPTCHA -->
                  <div class="form-group mt-4">
                    <div class="col-md-12 text-center">
                      <div class="g-recaptcha   text-center" data-sitekey="6Lcvaa0UAAAAAOcf3ZDNEQhrovpknWQb-q3ui_H9"></div>
                    </div>
                  </div>
                  <!-- ./RECAPTCHA -->

                  <!-- PLAN -->
                  <input type="hidden" name="plan" value="1" />
                  <!-- ./PLAN -->

                  <div class="form-group">
                    <div class="text-center mx-auto">
                      <a class="btn btn-primary cancel"
                        href="<?= $cancel_url ?>"><?php echo lang('core button cancel'); ?></a>
                      <?php if ($this->session->userdata('logged_in')) : ?>
                      <button type="submit" name="submit" class="btn btn-success"><span class="glyphicon glyphicon-save"></span>
                        <?php echo lang('core button save'); ?></button>
                      <?php else : ?>
                      <button id="send" type="submit" name="submit" class="btn btn-success send"><span
                          class="glyphicon glyphicon-ok"></span> <?php echo lang('users button confirm'); ?></button>
                      <?php endif; ?>
                    </div>
                  </div>

                </div>
                <!-- card-block end -->
              </div>
            <!-- auth-box end -->
           <?php echo form_close(); ?>
          <!-- form -->    
        </div>
        <!-- end of col-sm-12 -->
      </div>
      <!-- end of row -->
    </div>
    <!-- end content -->
 </section>
 <!-- section end--> 
</div>
<style>
.g-recaptcha>div {
  margin: auto;
  display: block
}
</style>