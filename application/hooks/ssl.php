<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of ssl
 *
 * @author Juan Manuel
 */

function redirect_ssl() {
    $CI =& get_instance();
    $class = $CI->router->fetch_class();
    $exclude =  array();  // add more controller name to exclude ssl.
    
    
    if(!$CI->input->is_cli_request()){
        if($CI->config->config['force_ssl']){

            $CI->config->config['base_url'] = str_replace('http://', 'https://', $CI->config->config['base_url']);

            if(!in_array($class,$exclude)) {
                $ssl = (isset($_SERVER['REDIRECT_HTTPS']) || isset($_SERVER['HTTPS'])) ? 'on' : 'off';

                if ($ssl != 'on'){
                    redirect(base_url($_SERVER['REQUEST_URI']));
                }
            }
        }
    }
}
