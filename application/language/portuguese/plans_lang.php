<?php defined('BASEPATH') OR exit('No direct script access allowed');
/**
 * Admin Language File
 */

// Titles
$lang['admin plans title']                = "Lista de planos";
$lang['plans title plans_add']            = "Adicionar plano";
$lang['plans title plans_edit']            = "Editar plano";
// Table Columns
$lang['plans col base']                 = "Base";
$lang['plans col period_days']                   = "Período de dias";
$lang['plans col portuguese_name']                  = "Nome";
$lang['plans col id']                    = "ID";


// Form Inputs
$lang['plans input email']                    = "Mail";
$lang['plans input first_name']               = "Nome";
$lang['plans input is_admin']                 = "É administrador";
$lang['plans input language']                 = "Idioma";
$lang['plans input plan']                     = "Plano";
$lang['plans input profile']                  = "Perfil";
$lang['plans input last_name']                = "Apelido";
$lang['plans input password']                 = "Senha";
$lang['plans input password_repeat']          = "Repetir a senha";
$lang['plans input status']                   = "Estado";
$lang['plans input username']                 = "Nome de usuário";
$lang['plans input username_email']           = "Nome de usuário ou e-mail";
$lang['plans input check']                    = "Selecione";
$lang['plans input base']                     = "Base";
$lang['plans input period_days']              = "Período";
$lang['plans input period_days_before']       = "Período anterior";
$lang['plans input min_profit']       		  = "Benefício Mínimo";
$lang['plans input max_profit']       		  = "Benefício máximo";
$lang['plans input spanish_name']       	  = "Nome";
$lang['plans input english_name']       	  = "Nome";
$lang['plans input portuguese_name']          = "Nome";
$lang['plans input spanish_features']         = "Características";
$lang['plans input english_features']         = "Características";
$lang['plans input portuguese_features']      = "Características";
$lang['plans input excluded_days']            = "Dias excluídos";

// Help
$lang['plans help passwords']                 = "Basta digitar a senha se você quiser alterá-la.";

// Messages
$lang['plans msg add_user_success']           = "O plano foi adicionado com sucesso!";
$lang['plans msg delete_confirm']             = "¿Claro que você deseja excluir <strong>%s</ strong>? Isso não pode ser desfeito..";
$lang['plans msg delete_user']                = "O plano foi excluído com sucesso";
$lang['plans msg edit_profile_success']       = "Seu perfil foi modificado com sucesso!";
$lang['plans msg edit_user_success']          = "O plano foi modificado com sucesso!!";
$lang['plans msg register_success']           = "Obrigado por se inscrever, %s! Confirme seu email por uma mensagem de confirmação. Depois que sua conta for verificada, você poderá fazer login com as credenciais fornecidas.";
$lang['plans msg password_reset_success']     = "Sua senha foi redefinida, %s! Por favor, verifique seu e-mail para sua nova senha temporária.";
$lang['plans msg validate_success']           = "Sua conta foi verificada. Agora você pode acessar sua conta.";
$lang['plans msg email_new_account']          = "<p>Obrigado por criar uma conta em %s. Clique no link abaixo para validar seu endereço de e-mail e ativar sua conta.<br /><br /><a href=\"%s\">%s</a></p>";
$lang['plans msg email_new_account_title']    = "Nova conta %s";
$lang['plans msg email_password_reset']       = "<p>Sua senha em %s Foi restaurado. Clique no link abaixo para fazer o login com sua nova senha:<br /><br /><strong>%s</strong><br /><br /><a href=\"%s\">%s</a>
                                                Uma vez logado, certifique-se de alterar a senha para algo que você possa lembrar.</p>";
$lang['plans msg email_password_reset_title'] = "Redefinir senha para %s";

// Errors
$lang['plans error add_user_failed']          = "O plano não pôde ser adicionado!";
$lang['plans error delete_user']              = "O plano não pôde ser excluído!";
$lang['plans error edit_profile_failed']      = "Seu perfil não pode ser modificado!";
$lang['plans error edit_user_failed']         = "O plano não pôde ser modificado!";
$lang['plans error email_exists']             = "O e-mail <strong>%s</strong> já existe!";
$lang['plans error email_not_exists']         = "Esse email não existe!";
$lang['plans error invalid_login']            = "Usuário ou senha inválidos";
$lang['plans error password_reset_failed']    = "Houve um problema ao redefinir sua senha. Por favor, tente novamente.";
$lang['plans error register_failed']          = "Sua conta não pôde ser criada no momento. Por favor, tente novamente.";
$lang['plans error user_id_required']         = "Um ID de usuário numérico é obrigatório!";
$lang['plans error user_not_exist']           = "Esse plano não existe!";
$lang['plans error username_exists']          = "O usuário <strong>%s</strong> já existe!";
$lang['plans error validate_failed']          = "Houve um problema ao validar sua conta. Por favor, tente novamente.";
$lang['plans error too_many_login_attempts']  = "Você fez muitas tentativas para fazer login muito rapidamente. Por favor espere %s segundos e tente novamente.";

