<?php defined('BASEPATH') OR exit('No direct script access allowed');
/**
 * Transactions Language File
 */


$lang['transfer-money']  = "Transferir dinheiro";
$lang['transfer-to']     = "Transferir para";
$lang['search']          = "Pesquisar";
$lang['clear']           = "Limpiar";
$lang['amount']          = "Quantidade";
$lang['token']           = "Token";
$lang['request-token']   = "Solicitar token";
$lang['transfer']        = "Transferir";
$lang['new-deposit']     = "Novo depósito";
$lang['button-transfer'] = "Gerar depósito de Bitcoin";
$lang['request-payout']  = "Faça um pagamento";
$lang['pay']             = "Comprar";
$lang['select-wallet']   = "Selecione carteira";
$lang['origen-wallet']   = "Carteira Original";
$lang['coin-tobuy']      = "Moeda para comprar";
$lang['coin-to-sell']    = "Moeda para vender";
$lang['dollar-price']    = "Preço em USD";
$lang['select-destination-wallet']   = "Selecione carteira de destino";
$lang['amount-trading']   = "Vá às compras <span class='value-currency'>XX</span> <span class='code-currency'>XMR</span> coins";
$lang['make-trading']    = "Fazer negociação";

// Sell and Buy orders
$lang['balance']        = "Equilíbrio";
$lang['price']          = "Preço";
$lang['total']          = "Total";
$lang['fee']            = "Taxa (1%)";
$lang['total_fee']      = "Total + Taxa";
$lang['sell']           = "Vender";
$lang['buy_trading']    = "Comprar";
$lang['trade_history']  = "História do Exchange";
$lang['sell_order']     = "Pedidos para venda";
$lang['buy_order']      = "Pedidos para comprar";

//wallets
$lang['dollar']         = "USD";
$lang['bitcoin']         = "BTC";
$lang['monero']         = "XMR";
$lang['ethereum']       = "ETH";
$lang['litecoin']       = "LTC";
$lang['trumpcoin']      = "TRUMP";
$lang['dash']           = "DASH";
$lang['solarcoin']      = "SLR";
$lang['bonus_referral'] = "Bônus de primeira compra";
$lang['bonus_activation'] = "Bônus de ativação";
$lang['bonus_infinity']  = "Residual até o infinito";
$lang['bonus_fullness']  = "Bônus FullnessGlobal";
$lang['bonus_upgrade']  = "Bônus Upgrade";
$lang['wallet_payment']  = "Pagamento de carteira";
$lang['label_payout']  = "Payout";
$lang['label_bonus_award'] = "Bônus de prêmio";
$lang['label_bonus_external_sales'] = "Bônus de e-commerce";    



