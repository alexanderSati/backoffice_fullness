<?php defined('BASEPATH') OR exit('No direct script access allowed');
/**
 * Admin Language File
 */

// Titles
$lang['admin title admin']                = "Administração";
$lang['admin title pending transactions'] = "Transações pendentes";
$lang['admin title transactions']         = "Todas as transações";
$lang['admin title payout_request']       = "Lista de payouts";  

// Buttons
$lang['admin button csv_export']          = "Exportação CSV";
$lang['admin button dashboard']           = "Dashboard";
$lang['admin button delete']              = "Excluir";
$lang['admin button edit']                = "Editar";
$lang['admin button messages']            = "Mensagens";
$lang['admin button settings']            = "Configurações";
$lang['admin button users']               = "Usuários";
$lang['admin button users_add']           = "Adicionar novo usuário";
$lang['admin button users_list']          = "Lista de usuários";
$lang['admin button admin_list']          = "Lista de administradores";
$lang['admin button activate']            = "Ativar";
$lang['admin button remove']              = "Retirar";

// Tooltips
$lang['admin tooltip csv_export']         = "Exportação de um arquivo CSV de todos os resultados com os filtros aplicados.";
$lang['admin tooltip filter']             = "Atualize os resultados com base nos seus filtros.";
$lang['admin tooltip filter_reset']       = "Limpe todos os filtros e classificação.";

// Form Inputs
$lang['admin input active']               = "Ativo";
$lang['admin input inactive']             = "Inativo";
$lang['admin input items_per_page']       = "items/page";
$lang['admin input select']               = "escolha...";
$lang['admin input username']             = "Nome de usuário";

//Week days
$lang['admin input monday']                 = "Segunda-feira";
$lang['admin input thuesday']               = "Terça";
$lang['admin input wednesday']              = "Quarta-feira";
$lang['admin input thursday']               = "Quinta-feira";
$lang['admin input friday']                 = "Sexta-feira";
$lang['admin input saturday']               = "Sabado";
$lang['admin input sunday']                 = "Domingo";

// Table Columns
$lang['admin col actions']                = "Ações";
$lang['admin col status']                 = "Estado";
$lang['admin col plan']                   = "Plano";
$lang['admin col rank']                   = "Gama";
$lang['admin col approve']                = "Aprovar";  
$lang['admin col denied']                 = "Negar";  


// Form Labels
$lang['admin label rows']                 = "%s Registro(s)";

$lang['error update']                     = "A transação não pode ser atualizada";
$lang['success update']                   = "Transação atualizada com sucesso";
