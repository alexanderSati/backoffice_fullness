<?php defined('BASEPATH') OR exit('No direct script access allowed');


$lang['withdraw_office']  = "Retirada do Escritório";
$lang['label_wholesaler']  = "Atacadista";
$lang['label_office']  = "Escritório";
$lang['label_banking']  = "Banco";
$lang['label_shipping_address']  = "Endereço para envio";
$lang['question_confirm_purchase']  = "¿Tem certeza de que fez a compra?";
$lang['no_shipping_selected']  = "Nenhum método de envio selecionado";
$lang['message']  = "Mensagem";
$lang['order_registered']  = "Pedido Registrado";
$lang['order_number']  = "Numero de ordem";
$lang['product_list']  = "Lista de produtos";
$lang['product_name']  = "Nome";
$lang['product_qty']  = "Quantidade";
$lang['product_price']  = "Preço";
$lang['subtotal']  = "Subtotal";
$lang['name_product'] = "Nome";
$lang['quantity_product'] = "Quantidade";
$lang['price_product']   = "Preço";
$lang['total_product']  = "Total";
$lang['shipping_label']  = "Expedição";
$lang['return_label']  = "Voltar";
$lang['pay_office_label']  = "Pagamento do escritório";
$lang['pay_wholesaler_label']  = "Pagamento por atacado";
$lang['pay_online_label']  = "Pagamento online <b class='text-warning' >em breve</b>";
$lang['payment_code']  = "Código de pagamento";
$lang['pay_creditcard_label']  = "Pagamento Visanet";
$lang['pay_registered_label']  = "Pagamento registrado";

