<?php defined('BASEPATH') OR exit('No direct script access allowed');
/**
 * Transactions Language File
 */


$lang['transfer-money']  = "Transferir dinheiro";
$lang['transfer-to']     = "Transferir para";
$lang['search']          = "Pesquisar";
$lang['clear']           = "Limpar";
$lang['amount']          = "Quantidade";
$lang['token']           = "Token";
$lang['security-pin'] = "PIN de segurança";
$lang['request-token']   = "Solicitar token";
$lang['transfer']        = "Transferir";
$lang['new-deposit']     = "Novo depósito";
$lang['new-btc-trasnfer']= "Nova transferência BTC";
$lang['button-transfer'] = "Gerar pedido de depósito";
$lang['button-coin-transfer'] = "Gerar pedido de depósito";
$lang['request-payout']  = "Solicitar Payout";
$lang['request-payout']  = "Solicitar un pago";
$lang['select-wallet']   = "Selecione carteiraa";
$lang['qr']              = "Adicionar código QR";
$lang['qr_label']        = "O código QR deve ser da carteira <span id='label-qr'>BTC</span>";
$lang['hash_label']      = "Por favor, forneça o hash de depósito BTC.";
$lang['invalid_hash']    = "BTC hash inválido";

/*
 * Tokens library
 */
$lang['toke_email_subject'] = "Seu código de segurança";
$lang['no-found-username']  = "Usuário não encontrado";
$lang['send-token']         = "O token foi enviado para o seu email";
$lang['show-token']         = "Por favor, use o seguinte token %s";
$lang['token-error']        = "O token digitado é inválido, solicite outro token";
$lang['second_password']    = "O PIN inserido é inválido";
$lang['transfer-success']   = "A transferência foi bem sucedida";
$lang['deposit-success']    = "O depósito foi feito com sucesso";
$lang['payout-success']     = "O pagamento foi solicitado com sucesso, em breve um executivo de conta processará.";
$lang['invalid-amount']     = "Transferência de valor inválido";
$lang['invalid-amount-payout']= "O valor inserido não atende aos valores permitidos, o valor mínimo é de 10 soles. ";
$lang['invalid-wallet']     = "Carteira de Transferência Inválida";
$lang['invalid-user']       = "Usuário inválido para transferir";
$lang['payout-limit']       = "Você excedeu o número máximo de pagamentos";
$lang['payout-limit-title'] = "Opção não disponível!";
$lang['payout-limit-text']  = "Lamentamos, você atingiu o número máximo de aplicativos permitidos (%d a cada %d %s), você pode criar um novo aplicativo depois:";
$lang['enough-funds']       = "Fundos insuficientes para transferência";
$lang['invalid-transfer']   = "Erro ao criar transferência";
$lang['invalid-deposit']    = "Erro ao solicitar o depósito";
$lang['invalid-payout']     = "Erro ao solicitar pagamento";

/*
 * Wallets Transactions
 */
$lang['invalid_btc_transfer'] = "Transferências da carteira BTC não são permitidas, transfira fundos de sua carteira operacional";
$lang['inactive_binary'] = 'Cada investidor deve ativar seu binário para fazer transferências e solicitar pagamentos';

/*
 * Deposit Transactions
 */
$lang['cancel_deposit']     = "Cancelar";
$lang['deposit_payed']      = "Pago";
$lang['qr_code']            = "Código QR";
$lang['deposit_message']    = "Depois de fazer seu investimento, clique no botão <code>PAGO</code>, para atualizar o status, caso contrário ele não será processado por um agente executivo";

/*
 * Transfers U2U
 */
$lang['amount_transfer_fee'] = "Comissão de transferência&nbsp;";
$lang['fee_tooltip'] = "Este valor será deduzido do valor inserido.";

$lang['origin-coin'] = "Moeda para depositar";
$lang['final_coin'] = "Carteira de destino";
$lang['amount_coin_tooltip'] = "Por favor preencha este campo com o valor a ser depositado";
$lang['activation_bonus'] = 'Bônus de ativação do usuário';
