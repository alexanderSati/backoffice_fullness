<?php defined('BASEPATH') OR exit('No direct script access allowed');



$lang['shopping_cart']                = "Carrinho de compras";
$lang['clear_shopping_cart']          = "Carro vazio";
$lang['add_shopping_cart']          = "Adicionar ao carrinho";
$lang['price_shopping_cart']          = "Preço";
$lang['close_cart_list']          = "Fechar";
$lang['buy_cart_list']          = "Comprar";
$lang['remove_cart_list']          = "Remover";
$lang['cart_empty']          = "O carro está vazio";
$lang['cart_name']          = "Nome";
$lang['cart_quantity']          = "Quantidade";
$lang['cart_action']          = "Ação";
$lang['cart_confirm_buy']    = "Tem certeza de que fez a compra?";
$lang['cancel_buy']    = "Cancelar";
$lang['cart_buy']    = "Comprar";
$lang['buy_successfull']    = "Compra bem sucedida";
$lang['insufficient_funds'] = "Fundos insuficientes";
$lang['cart_details']    = "Detalhes";
$lang['cart_date']    = "Data";
$lang['cart_discount']    = "Desconto";
$lang['cart_user']    = "Usuario";
$lang['cart_items']    = "Unid";
$lang['cart_state']    = "Estado";
$lang['cart_state_approved']    = "Aprovado";
$lang['cart_state_pending']    = "Pendente";
$lang['cart_state_denied']    = "Negado";
$lang['product_category']    = "Categoria";
$lang['cart_residual']       = "Residual";
$lang['cart_binary']       = "Binário";
$lang['cart_return']    = "Voltar";
$lang['cart_of']       = "de";     
$lang['external_buy_successfull']    = "Compra bem sucedida";
$lang['subtotal_price_shopping_cart']          = "Subtotal";
$lang['total_zero']          = "Você deve ter pelo menos 1 produto selecionado";
$lang['approve_state']    = "Aprovar";
$lang['order_approved']    = "Encomenda Aprovada";
$lang['product_ubication']    = "Localização";
$lang['order_number']    = "Numero de ordem";


$lang['denied_state']    = "Negar";
$lang['order_denied']    = "Pedido negado";
$lang['purchases'] = 'Compras';

$lang['name_product'] = "Nome";
$lang['quantity_product'] = "Quantidade";
$lang['price_product']   = "Preço";
$lang['total_product']  = "Total";
$lang['no_plan_price']    = "Você não atingiu o preço planejado";
$lang['first_plan_purchased'] = "Você deve aguardar a aprovação do seu pedido de compra";
$lang['change_shipping']  = "Voltar";
$lang['no_products_message']  = "Não há produtos para exibir";
$lang['residual_points']  = "Pontos residuais";
$lang['binary_points']  = "Pontos binários";
$lang['message_add']  = "Produto adicionado";
$lang['ask_cancel_upgrade']  = "Você quer cancelar a atualização";
$lang['no_quantity_in_stock']  = "Estoque insuficiente";
$lang['message_no_product_available'] = "Existe apenas {1} {2} em estoque";
$lang['lbl_tienda'] = "Tienda";
$lang['lbl_shipping_address'] = "Endereço para envio";
$lang['msg_unique_wholesaler'] = "Sólo puede adquirir produtos de um vendedor";
$lang['lbl_pay_wallet'] = "Pagamento de carteira";
$lang['lbl_pay_wholesaler'] = "Pagamento por atacado";
$lang['lbl_pay_oficina'] = "Pagamento do escritório";
$lang['lbl_pay_online'] = "Pagamento online, código";
$lang['lbl_wholesaler'] = "Atacadista";
$lang['lbl_payment_approved'] = "Pagamento aprovado";
$lang['pay_creditcard_label']  = "Pagamento Visanet";



