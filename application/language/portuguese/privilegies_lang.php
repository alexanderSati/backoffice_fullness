<?php defined('BASEPATH') OR exit('No direct script access allowed');
/**
 * Settings Language File
 */

// Titles
$lang['privilegies-title'] = "Editar privilégios";
$lang['read']              = "Ver";
$lang['insert']            = "Inserir";
$lang['update']            = "Atualização";
$lang['delete']            = "Remover";
$lang['module']            = "Módulo";
$lang['section']           = "Seção";
$lang['actions']           = "Ações";

// Messages
$lang['msg save_success']  = "Permissões foram modificadas com sucesso.";

// Errors
$lang['error save_failed'] = "Houve um problema ao salvar as permissões. Por favor, tente novamente.";
