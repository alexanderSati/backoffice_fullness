<?php defined('BASEPATH') OR exit('No direct script access allowed');
/**
 * Menu Language File
 */

// Titles menu
$lang['dashboard']                = "Página inicial";
$lang['users-groups']             = "Usuários e Grupos";
$lang['plans']                    = "Planos";
$lang['wallets']                  = "Carteira";
$lang['trading']                  = "Trade";
$lang['transactions']             = "Transações";
$lang['btc_wallet_transfer']      = "Transferências de carteira";
$lang['pending-transactions']     = "Transações pendentes";
$lang['reports']                  = "Relatórios";
$lang['faqs']                     = "Us. Fundadores";
$lang['settings']                 = "Configuração";
$lang['logout']                   = "Fechar Sessão";
$lang['new-admin']                = "Novo administrador";
$lang['administrators']           = "Administradores";
$lang['shopping']                 = "Loja";
$lang['categories']               = "Categorias";
$lang['products']                 = "Produtos";
$lang['products-user']            = "Comprar";
$lang['shop']                 	  = "Loja";
$lang['purchases-user']           = "Histórico de pedidos";
$lang['orders']           		  = "Encomendas";
$lang['shipping']                 = "Pontos de coleta";
$lang['products_quantities']      = "Stock do produto";
$lang['my-transactions']          = "Minhas transações";  
$lang['executive']                = "Executivos"; 
$lang['unilevel']                 = "Unilevel";  
$lang['my-stock']                 = "Meu stock";  

$lang['new-user']                 = "Novo usuário";
$lang['users']                    = "Usuários";
$lang['Users-by-capital']         = "Estadísticas de usuarios";

$lang['new-plan']                 = "Novo plano";
$lang['plans']                    = "Planos";
$lang['account']                  = "Minha conta";
$lang['binary_tree']              = "Binário";
$lang['binary_networks']          = "Minhas redes";
$lang['binary_unassigned']        = "Usuários não atribuídos";

$lang['new-faq']                  = "Novo";
$lang['privilegies']              = "Privilégios";

$lang['transfers']              = "Transferências";
$lang['deposit']                = "Depositar";
$lang['btc-deposit']            = "BTC Depositar";
$lang['coin-deposit']           = "Depósito de moedas";
$lang['payout']                 = "Solicitar payout";
$lang['wallets-transfer']       = "Transferência de carteira";
$lang['sign-friend']            = "Novo líder";

$lang['news']                   = "Notícias";
$lang['signs']                  = "Sinais";
$lang['bitwexchange-news']      = "Notícias do Bitwexchange";
$lang['order']                  = "Orders";

$lang['founders']               = "Fundadores";
$lang['pdf-fullness']           = "Catálogo";    
$lang['card-pay']               = "Pagamento";



//wallets
$lang['dollar']         = "USD";
$lang['bitcoin']         = "BTC";
$lang['monero']         = "XMR";
$lang['ethereum']       = "ETH";
$lang['litecoin']       = "LTC";
$lang['trumpcoin']      = "TRUMP";
$lang['dash']           = "DASH";
$lang['solarcoin']      = "SLR";

//link
$lang['link']           = "Link do convite";
$lang['link_shop']      = "Link da loja";

$lang['request_payouts'] = 'Solicitações de payout';