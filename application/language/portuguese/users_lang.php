<?php defined('BASEPATH') OR exit('No direct script access allowed');
/**
 * Users Language File
 */

// Titles
$lang['users title forgot']                   = "Esqueceu sua senha";
$lang['users title login']                    = "Iniciar sessão";
$lang['users title profile']                  = "Perfil";
$lang['users title register']                 = "Inscreva-se";
$lang['users title user_add']                 = "Adicionar usuário";
$lang['users title customer_add']             = "Adicionar cliente";
$lang['users title user_delete']              = "Confirme a exclusão do usuário";
$lang['users title user_edit']                = "Editar usuário";
$lang['users title user_list']                = "Lista de usuários";
$lang['users title user_stats']               = "Estatísticas do usuário";
$lang['users title account']                  = "Minha conta";
$lang['users title transactions']             = "Transações";
$lang['users title transfer']                 = "Transferir";
$lang['users title deposit']                  = "Novo depósito";
$lang['users title payout']                   = "Pagamento";
$lang['users title wallets']                  = "Carteira";
$lang['users title binary']                   = "Rede binária";
$lang['users title unassigned']               = "Usuários não atribuídos";
$lang['users title wallets transfer']         = "Transferência de carteira";
$lang['users title btc-wallets transfer']     = "Transferências BTC";
$lang['users title register qr_to_pay']       = "Digitalize o código QR para processar seu depósito";
$lang['users title renew-subscription']       = "Renovar assinatura";
$lang['users title link']                     = "Links";
$lang['users title categories_list']          = "Lista de categorias";
$lang['users title category_edit']            = "Editar categoria";
$lang['users title category_add']             = "Adicionar categoria";

$lang['users title product_list']             = "Lista de produtos";
$lang['users title product_add']              = "Adicionar produto";
$lang['users title new_site']                 = "Novo no site?";
$lang['users title change_language']          = "Mudar de idioma";

$lang['users title product_edit']             = "Editar produto";  
$lang['users title total_referrals']          = "Referências";
$lang['users title saldo']                    = "Balanço de plataforma";
$lang['users title binary_points']            = "Pontos binários";
$lang['users title plan_active']              = "Este é o seu plano ativo.";
$lang['users title residual_points']          = "Pontos residuais";
$lang['users title left-points']              = "Pontos da esquerda";
$lang['users title right-points']             = "Pontos da direita"; 
$lang['users title send_to']                  = "Envio para";
$lang['users title order']                    = "Ordem";         



$lang['users title shipping_details']         = "Pontos de coleta";
$lang['users title add_shipping_details']     = "Criar ponto de coleta";
$lang['users title edit_shipping_details']    = "Editar ponto de coleta";

$lang['users title products_quantities']      = "Stock de produtos";
$lang['users title add_products_quantities']  = "Criar stock de produtos";
$lang['users title edit_products_quantities'] = "Editar stock de produtos";

$lang['users title awards']                   = "Prêmios";
$lang['users title list_administrators']      = "Lista de administradores"; 
$lang['users title edit_admin']               = "Editar administrador";  
$lang['users title admin_add']                = "Adicionar administrador";

$lang['users title cart_list']                = "Lista da compra";  
$lang['users title next_prize']               = "Próximo prêmio";
$lang['users title point_missing']            = "Pontos faltantes";
$lang['users title progress_bar']             = "Barra de progresso";
$lang['users title select_option']            = "Escolha uma opção"; 
$lang['users title preferred_consumer']       = "Consumidor Preferencial";
$lang['users title points_in_residual']       = "pontos residuais";
$lang['users title points_in_binary']         = "pontos binários";
$lang['users title shipping_method']          = "Método de envio";
$lang['users title payment_method']           = "Método de pagamento";
$lang['users title available']                = "Disponível";
$lang['users title confirm_purchase']         = "Confirme a compra";

$lang['users title total_points_binary']      = "Total de pontos de liquidação binários"; 
$lang['users title maximum_plan']             = "Melhoria do plano não disponível";  

$lang['users title banking_pay']              = "Pagamento bancário"; 
$lang['users title executive_add']            = "Adicionar executivo"; 

$lang['users title executive_edit']           = "Editar executivo";

$lang['users title external_store']           = "Loja";
$lang['users title personal_informacion']     = "Dados pessoais"; 
$lang['users title unilevel']                 = "Unilevel";
$lang['users title month-wallet']             = "Filtrar por mês";
$lang['users title line-max']                 = "Linha máxima";
$lang['users title bonus']                    = "Bônus";
$lang['users title grupal-point']             = "Pontos do grupo";
$lang['users titel unilevel-rank']            = "Faixa";  



// Buttons
$lang['users button add_new_user']            = "Adicionar novo usuário";
$lang['users button register']                = "Criar uma conta";
$lang['users button reset_password']          = "Redefinir senha";
$lang['users button login_try_again']         = "Tenta de novo";
$lang['users button add_new_category']        = "Adicionar nova categoria";
$lang['users button add_new_product']         = "Adicionar novo produto";
$lang['users button cancel']                  = "Cancelar";
$lang['users button confirm']                 = "Confirmar";
$lang['users button add_shipping_details']    = "Criar ponto de coleta";
$lang['users button add_product_quantities']  = "Criar stock de produtos";
$lang['users button accept']                  = "Aceitar";
$lang['users button add_new_executive']       = "Adicionar novo executivo";


// Tooltips
$lang['users tooltip add_new_user']           = "Crie um novo usuário.";
$lang['users tooltip add_new_category']       = "Crie uma nova categoria.";
$lang['users tooltip add_new_product']        = "Crie um novo produto.";
$lang['users tooltip add_new_shipping']       = "Crie um novo ponto de coleta.";
$lang['users tooltip add_new_product_quantities']  = "Crie um novo stock de produtos.";
$lang['users tooltip add_new_executive']      = "Crie um novo executivo";  





// Links
$lang['users link forgot_password']           = "¿Você esqueceu sua senha?";
$lang['users link register_account']          = "Inscreva-se para uma conta.";

// Table Columns
$lang['users col fullname']                   = "Nome completo";
$lang['users col first_name']                 = "Nome";
$lang['users col is_admin']                   = "Administrador";
$lang['users col last_name']                  = "Apelido";
$lang['users col user_id']                    = "ID";
$lang['users col username']                   = "Nome de usuário";
$lang['users col created']                    = "Data de registro";
$lang['users col wallets']                    = "Carteiras (USD)";
$lang['users col BTC']                        = "BTC";
$lang['users col XMR']                        = "XMR";
$lang['users col ETH']                        = "ETH";
$lang['users col LTC']                        = "LTC";
$lang['users col TRUMP']                      = "TRUMP";
$lang['users col DASH']                       = "DASH";
$lang['users col SLR']                        = "SLR";
$lang['users col USD']                        = "BTC operative";
$lang['users col total']                      = "USD total em carteiras";
$lang['users col profit']                     = "Renda";
$lang['users col payout']                     = "Pagamentos";
$lang['users col categories_id']              = "ID";
$lang['users col name']                       = "Nome";
$lang['users col description']                = "Descrição";
$lang['users col product_name']               = "Nome do produto";  
$lang['users col product_price']              = "Preço ao cliente";
$lang['users col product_price_distributor']  = "Preço do distribuidor";
$lang['users col code']                       = "Code";
$lang['users col name_details']               = "Ponto de partida";
$lang['users col quantities']                 = "Quantidade";
$lang['users col amount']                     = "Valor";




// Form Inputs
$lang['users input email']                    = "Mail";
$lang['users input first_name']               = "Nome";
$lang['users input is_admin']                 = "É administrador";
$lang['users input language']                 = "Idioma";
$lang['users input id_user']                  = "Usuario ID";
$lang['users input amount']                   = "Quantidade";
$lang['users input token']                    = "Token";
$lang['users input user']                     = "usuários";
$lang['users input wallet']                   = "Carteira";
$lang['users input qr']                       = "Code QR";
$lang['users input country']                  = "País";
$lang['users input plan']                     = "Plano";
$lang['users input profile']                  = "Perfil";
$lang['profile-admin']                        = "Administrador";
$lang['profile-executive']                    = "Executivo de contas";
$lang['profile-user']                         = "Usuário Geral";
$lang['users input last_name']                = "Apelido";
$lang['users input password']                 = "Senha";
$lang['users input password_repeat']          = "Repetir a senha";
$lang['users input security_pin']             = "PIN de segurança";
$lang['users input security_pin_repeat']      = "Repetir PIN de segurança";
$lang['users input status']                   = "Estado";
$lang['users input side']                     = "Minha rede";
$lang['users input side-left']                = "Linha de negócios à esquerda";
$lang['users input side-right']               = "Linha de negócios à direita";
$lang['users input remember']                 = "Lembre-se, esta opção é irreversível.";
$lang['users input username']                 = "Nome de usuário";
$lang['users input username_email']           = "Nome de usuário ou e-mail";
$lang['users input check']                    = "Selecione";
$lang['users input image']                    = "Selecione a imagem";
$lang['users input terms']                    = "Eu aceito o <a href='https://bitwexchange.com/es/terminos-y-condiciones/' target='blank'>termos e condições</a> de uso";
$lang['users input dni']                      = "DNI";
$lang['users input birthdate']                = "Data de nascimento";
$lang['users input phone']                    = "Telefone";
$lang['users input address']                  = "Endereço";
$lang['users input address2']                 = "Linha de endereço 2";
$lang['users input city']                     = "Cidade";
$lang['users input department']               = "Departamento";
$lang['users input postal_code']              = "Código postal";
$lang['users input bank_account']             = "Descrição de uma conta bancária";

$lang['users input name']                     = "Nome";
$lang['users input description']              = "Descrição";

$lang['users input product_name']             = "Nome do produto";  
$lang['users input product_price']            = "Preço ao cliente";
$lang['users input category_id']              = "Categoria"; 
$lang['users input product_price_distributor']  = "Preço do distribuidor";
$lang['users input code']                     = "Code";
$lang['users input residual_points']          = "Pontos residuais";
$lang['users input binary_points']           = "Binário";
$lang['users input stock_quantity']           = "Stock";
$lang['users input shipping_types_id']        = "Tipo de remessa";
$lang['users input shiping_details_id']       = "Pegar ponto";
$lang['users input quantity']                 = "Quantidade";
$lang['users input shiping_details']          = "Escritório de transporte";                                        



// Help
$lang['users help passwords']                 = "Basta digitar a senha se você quiser alterá-la.";

// Messages
$lang['users msg add_user_success']           = "%s foi adicionado com sucesso!";
$lang['users msg delete_confirm']             = "¿Claro que você deseja excluir <strong>%s</ strong>? Isso não pode ser desfeito..";
$lang['users msg delete_user']                = "Foi eliminado com sucesso <strong>%s</strong>!";
$lang['users msg edit_profile_success']       = "Seu perfil foi modificado com sucesso!";
$lang['users msg edit_user_success']          = "%s foi modificado com sucesso!!";
$lang['users msg register_success']           = "Obrigado por se registrar %s! Confirme seu email por uma mensagem de confirmação. Depois que sua conta for verificada, você poderá fazer login com as credenciais fornecidas.";
$lang['users msg password_reset_success']     = "Sua senha foi redefinida, %s! Por favor, verifique seu email para sua nova senha temporária.";
$lang['users msg validate_success']           = "Sua conta foi verificada. Agora você pode acessar sua conta.";
$lang['users msg email_new_account']          = "<p>
    <img width=\"260\" src=\"https://app.bitwexchange.com/themes/gentella/images/logo.png\"
         alt=\"Bitwexchange\">
</p><p>Obrigado por criar uma conta em %s. Clique no link abaixo para validar seu endereço de e-mail e ativar sua conta.<br /><br /><a href=\"%s\">%s</a></p>";
$lang['users msg email_new_account_title']    = "Nova conta %s";
$lang['users msg email_password_reset']       = "<p>
    <img width=\"260\" src=\"https://app.bitwexchange.com/themes/gentella/images/logo.png\"
         alt=\"Bitwexchange\">
</p><p>Sua senha em %s Foi restaurado. Clique no link abaixo para fazer o login com sua nova senha:<br /><br /><strong>%s</strong><br /><br /><a href=\"%s\">%s</a>
                                                Uma vez logado, certifique-se de alterar a senha para algo que você possa lembrar.</p>";
$lang['users msg email_password_reset_title'] = "Redefinir senha para %s";
$lang['users link copy']                      = "Link copiado com sucesso para a área de transferência";
$lang['users msg edit_category_success']      = "A categoria %s foi modificado com sucesso!!";
$lang['users msg add_category_success']       = "A categoria %s foi adicionado com sucesso!";

$lang['users msg edit_product_success']       = "O produto %s foi modificado com sucesso!!";
$lang['users msg add_product_success']        = "O produto %s foi adicionado com sucesso!";

$lang['users msg remove_product']             = "Seu produto <strong>%s</strong> foi removido com sucesso";
$lang['users msg activate_product']           = "Seu produto <strong>%s</strong> foi ativado com sucesso";

$lang['users msg remove-sure']                = "Você tem certeza de conferir este produto??";
$lang['users msg activate-sure']              = "Você tem certeza de ativar este produto??";

$lang['users msg transfer']                   = "Certifique-se de transferir o dinheiro?";

$lang['users msg add_shipping_detail']        = "Pegar pontos %s foi adicionado com sucesso!";
$lang['users msg edit_shipping_detail']       = "Pegar pontos %s foi modificado com sucesso!";

$lang['users msg add_product_quantity']       = "O stock do produto foi adicionado com sucesso!";
$lang['users msg edit_product_quantity']      = "O stock do produto foi modificado com sucesso!";

$lang['users msg deny-sure']                  = "Você tem certeza de negar isso payout?";   
$lang['users msg aprrove-sure']               = "Tem certeza que você aprova isso payout?"; 

$lang['users msg confirm_select_option']      = "¿Você tem certeza de escolher %s?";               
$lang['users msg confirm_preferred']          = "¿Tem certeza de selecionar esta opção??";

$lang['users msg confirm_clear_cart']          = "¿Tem certeza de que esvaziou o carro?";
$lang['users msg confirm_remove_inventory']    = "¿Tem certeza de que você apagou este item?";
$lang['users msg correct_clear']               = "Carrinho de compras vazio!";
$lang['users msg correct_remove_inventory']    = "Item excluído!"; 
$lang['users msg maximum_plan']                = "Atualmente, você tem o plano mais recente e não tem uma melhoria disponível.";





// Errors
$lang['users error add_user_failed']          = "%s não foi possível adicionar!";
$lang['users error delete_user']              = "<strong>%s</strong> não pôde ser removido!";
$lang['users error edit_profile_failed']      = "Seu perfil não pode ser modificado!";
$lang['users error edit_user_failed']         = "%s não pôde ser modificado!";
$lang['users error email_exists']             = "O e-mail <strong>%s</strong> já existe!";
$lang['users error email_not_exists']         = "Esse email não existe!";
$lang['users error username_not_exists']      = "Esse nome de usuário não existe!";
$lang['users error invalid_login']            = "Nome de usuário ou senha inválidos";
$lang['users error password_reset_failed']    = "Houve um problema ao redefinir sua senha. Por favor, tente novamente.";
$lang['users error register_failed']          = "Sua conta não pôde ser criada no momento. Por favor, tente novamente.";
$lang['users error user_id_required']         = "Um ID de usuário numérico é obrigatório!";
$lang['users error user_not_exist']           = "Este usuário não existe!";
$lang['users error username_exists']          = "O usuário <strong>%s</strong> já existe!";
$lang['users error validate_failed']          = "Houve um problema ao validar sua conta. Por favor, tente novamente.";
$lang['users error validate_referral_failed'] = "Houve um problema ao validar o usuário que o refere. Por favor, tente novamente.";
$lang['users error too_many_login_attempts']  = "Você fez muitas tentativas para fazer login muito rapidamente. Por favor espere %s segundos e tente novamente.";
$lang['users error edit_category_failed']     = "A categoria %s não pôde ser modificado!";
$lang['users error add_category_failed']      = "A categoria %s não foi possível adicionar!";

$lang['users error edit_product_failed']      = "O produto %s não pôde ser modificado!";
$lang['users error add_product_failed']       = "O produto %s não foi possível adicionar!";

$lang['users error remove_product']           = "O produto %s não pôde ser removido";
$lang['users error activate_product']         = "O produto %s não pôde ser ativado!";
$lang['users error product_not_exist']        = "Este produto não existe!";
$lang['users error product_id_required']      = "Um ID de produto numérico é obrigatório!";
$lang['users error add_shipping_detail_failed'] = "Pegar ponto %s não foi possível adicionar!";
$lang['users error edit_shipping_detail_failed'] = "Pegar ponto %s não pôde ser modificado!";

$lang['users error add_product_quantity'] = "O stock do produto não pôde ser adicionado!";
$lang['users error edit_product_quantity'] = "O stock do produto não pôde ser modificado!";

$lang['users error number_negative']          = "O montante não pode ser negativo ou 0"; 
$lang['users error payout_by_active']         = "Você deve ativar sua conta para executar o payout"; 
$lang['users error missing_purchases']        = "Você deve fazer uma compra maior que 220 soles para fazer o payout";                
$lang['users error days_passed']              = "Você só pode payout de 1 a 8 de cada mês";  

$lang['users error category_exists']          = "Uma categoria com este nome já existe";
$lang['users error product_exists']          = "Um produto com este nome já existe";
$lang['users error shipping_exists']          = "Um pegar ponto com este nome já existe";

$lang['users error is_preferencial_user']     = "Você é um consumidor preferido, os consumidores preferenciais não podem adicionar usuários";  
$lang['users error is_not_active_add']        = "Você deve ativar sua conta com um plano para executar esta ação";  
$lang['users error is_not_active_transfer']   = "Você deve ativar sua conta com um plano para fazer uma transferência"; 

$lang['users error is_preferencial_user_register'] = "O usuário ao qual você está tentando se referir é um consumidor preferido, você só pode se referir a usuários com um plano";
$lang['users error plan_not_select']  = "Você deve selecionar um plano";
$lang['users error exist_shiping_details_id'] = "O produto já está vinculado a esse ponto de coleta";


//account
$lang['my-referrals']    =  "Minhas referências";
$lang['account-plan']    =  "Plano de conta";
$lang['account-rank']    =  "Prestígio Atual";
$lang['current-plan']    =  "Plano atual";
$lang['current-rank']    =  "Faixa atual";
$lang['plan-list']       =  "Lista de planos:";
$lang['rank-list']       =  "Lista de classificação:";
$lang['details']         =  "Detalhes";
$lang['volume']          =  "Volume";
$lang['period_days']     =  "Planeje dias";
$lang['days_remaining']  =  "Dias restantes";
$lang['daily']           =  "Diariamente";
$lang['profit']          =  "Ganho";
$lang['points']          =  "pontos";
$lang['residual']        =  "Residual";
   


$lang['enough-funds']       = "Fundos insuficientes para transferência";
$lang['user-already']       = "O usuário já é uma criança";
$lang['invalid-side']          = "Lado Inválido";
$lang['side-busy']       = "O lado não está disponível (ocupado)";
$lang['parent-not']         = "A árvore pai não existe";
$lang['error update side']  = "Ocorreu um erro ao atualizar o status do usuário";
$lang['success update side']= "O status do usuário foi atualizado com sucesso";

// Notifications

$lang['users info waiting approval'] = "Sua conta foi criada.";
$lang['no-data-table'] = "Nenhum dado encontrado";

//binary
$lang['side']               = "Linha de negócio";
$lang['left-points']        = "Pontuação da linha de negócios à esquerda";
$lang['right-points']       = "Pontuação da linha de negócios à dereita";
$lang['total-points']       = "Total de pontos";

//token
$lang['token']              = "Token";
$lang['request-token']      = "Pedido de token";
$lang['send-token']         = "O token foi enviado para o seu email";
$lang['show-token']         = "Por favor, use o seguinte token %s";
$lang['token-error']        = "O token digitado é inválido, solicite outro token";


$lang['access_denied_expired']  = "Desculpe, seu investimento acabou, verifique os detalhes da sua conta ou entre em contato com um executivo de conta";
$lang['renew']                  = "Clique aqui para renovar";
$lang['renewal-success']        = "Sua conta foi renovada com sucesso.";
$lang['renewal-pending']        = "Sua conta será renovada quando seu pagamento for confirmado.";
$lang['error required_renewal'] = "Renovação de conta necessária.";
$lang['select-coin']            = "Selecione a moeda para depositar";

$lang['upgrade']                = "Melhorar";
$lang['upgrade_title']          = "Melhorar status";
$lang['upgrade_button']         = "Clique aqui para melhorar o status";
$lang['upgrade_success']        = "Parabéns, sua conta foi atualizada com sucesso.";

$lang['active_since']           = "Ativo desde";
$lang['modal_title']            = "Confirmação";
$lang['question_confirm']       = "Você tem certeza de atualizar seu plano?";
$lang['days_left']              = "Dias restantes";

$lang['plan_current']           = "O plano selecionado é o seu plano atual.";
$lang['plan_donwgrade']         = "Desculpe, você não pode mudar seu plano para um menor.";
$lang['invalid_subscription']   = "Nenhuma inscrição ativa encontrada.";
$lang['invalid-wallet']         = "Carteira Inválida.";
$lang['invalid-plan']           = "O plano selecionado é inválido.";
$lang['payment_fail']           = "Desculpe, o pagamento não pôde ser processado.";
$lang['not-enough-funds']       = "Fundos insuficientes em sua carteira operacional.";

$lang['back_to_home']           = "Voltar ao início";
$lang['your_plan']              = "Seu plano";

/***
 * Awards
 */
$lang['cruise']                 = "CRUZEIRO";
$lang['suitcase']               = "1/2 MILHÃO DE MALA";
$lang['house_million']          = "1 MILHÃO DE CASA";
$lang['rank']                   = "Classificação";


/*
 * Monthly payments
 */
$lang['monthly_suspended'] = "Sua conta está inativa no momento";
$lang['monthly_pending'] = "Sua conta tem pagamentos mensais pendentes. <strong><a href='/user/transactions/deposit'>(Pagar agora)</a></strong>";
$lang['first_login'] = "Bem-vindo, o próximo passo é adquirir sua franquia pessoal <a href='/Cart' style='color: white'>Clique aqui</a>";
$lang["no_active_user_refer"] = "Você deve se registrar como um parceiro ativo para encaminhar usuários";
$lang["lbl_commision"] = "Comissão";
$lang["lbl_discount"] = "Desconto";
$lang["lbl_status"] = "Estado";
$lang['payment_method']  = "Método de pagamento";