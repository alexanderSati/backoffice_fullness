<?php defined('BASEPATH') OR exit('No direct script access allowed');

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of emails_lang
 *
 * @author Juan Manuel
 */


/*
 * Templates titles
 */

$lang['btc_deposit_admin'] = "Um novo depósito BTC foi solicitado";
$lang['btc_deposit_user'] = "Depósito BTC solicitado";
$lang['btc_deposit_user_done'] = "Depósito BTC processado";

$lang['coin_deposit_admin'] = "Um novo depósito em moeda foi solicitado";
$lang['coin_deposit_user'] = "Depósito da moeda solicitada";
$lang['coin_deposit_user_done'] = "Depósito de moeda processada";

$lang['btc_payout_admin'] = "Um novo payout foi solicitado";
$lang['btc_payout_user'] = "Payout solicitado";
$lang['btc_payout_user_done'] = "Payout processado";

$lang['inscription_admin'] = "Fullness global Novo usuário registrado";
$lang['inscription_user'] = "Fullness global Novo usuário registrado";