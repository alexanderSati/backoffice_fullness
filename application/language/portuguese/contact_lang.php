<?php defined('BASEPATH') OR exit('No direct script access allowed');
/**
 * Contact Language File
 */

// Titles
$lang['contact title']                  = "Entre em contato conosco";
$lang['contact title messages_list']    = "Mensagens";

// Buttons
$lang['contact button read']            = "Ler mensagem";

// Table Columns
$lang['contact col message_id']         = "ID";
$lang['contact col name']               = "Nome";
$lang['contact col email']              = "Correio eletrônico";
$lang['contact col title']              = "Título";
$lang['contact col created']            = "Recebido";

// Form Inputs
$lang['contact input name']             = "Nome";
$lang['contact input email']            = "Correio eletrônico";
$lang['contact input title']            = "Título";
$lang['contact input message']          = "Mensagem";
$lang['contact input captcha']          = "CAPTCHA Texto";
$lang['contact input created']          = "Recebido";

// Messages
$lang['contact msg send_success']       = "Obrigado por nos contactar, %s! Sua mensagem foi enviada.";
$lang['contact msg updated']            = "Mensagem atualizada!";

// Errors
$lang['contact error captcha']          = "The CAPTCHA text did not match.";
$lang['contact error send_failed']      = "Desculpe, %s. Houve um problema ao enviar sua mensagem. Por favor, tente novamente.";
$lang['contact error update_failed']    = "Não foi possível atualizar o status.";
