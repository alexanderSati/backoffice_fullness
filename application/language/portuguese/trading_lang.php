<?php defined('BASEPATH') OR exit('No direct script access allowed');
/**
 * Transactions Language File
 */

$lang['amount']         = "Quantidade";
$lang['dollar-price']   = "Preço";
$lang['coin-tobuy']     = "Moeda para comprar";
$lang['coin-to-sell']   = "Moeda para vender";
$lang['make-trading']   = "Moeda de comércio";
$lang['actions']        = "Ações";

// Sell and Buy orders
$lang['time']           = "Hora";
$lang['balance']        = "Fundos";
$lang['price']          = "Preço";
$lang['total']          = "Total";
$lang['fee']            = "Taxa";
$lang['total_fee']      = "Total + Taxa";
$lang['total_fee_less'] = "Total - taxa";
$lang['buy']            = "Comprar";
$lang['sell']           = "Vender";
$lang['buy_trading']    = "Comprar";
$lang['trade_history']  = "História do Mercado";
$lang['sell_order']     = "Ofertas de ventas";
$lang['buy_order']      = "Oferta de compra";
$lang['cancel']         = "Cancelar";


/*
 *New module errors
 */

$lang['tradig_invalid_action'] = "Ação inválida.";
$lang['tradig_invalid_coin'] = "Moeda Inválida.";
$lang['tradig_invalid_amount'] = "O montante inserido não atende aos requisitos do mercado.";
$lang['tradig_invalid_price'] = "O preço no BTC não atende aos requisitos do mercado.";

$lang['tradig_not_funds_wallet'] = "Fundos insuficientes na carteira selecionada.";
$lang['tradig_not_funds_btc_wallet'] = "Fundos insuficientes na carteira operacional.";
$lang['tradig_not_published'] = "Sua oferta não pôde ser publicada.";
$lang['tradig_published'] = "Sua oferta foi publicada com sucesso.";
$lang['tradig_processed'] = "O processo de transação com sucesso.";
$lang['tradig_canceled'] = "Sua oferta foi cancelada e reembolsada corretamente.";

$lang['tradig_not_found'] = "Oferta não encontrada.";
$lang['tradig_not_available'] = "Lamentamos, esta oferta não está mais disponível.";
$lang['tradig_access_denied'] = "Acesso negado.";

$lang['form_fee_text'] = "A participação de (0,8%) é processada em bitcoins.";


//Admin
$lang['trading_history']= "Histórico de negociação";
$lang['buy_user']       = "Usuário comprador";
$lang['sell_user']      = "Usuário do vendedor";
$lang['date']           = "Data";
$lang['type']           = "Digite";
$lang['coin']           = "Moeda";
$lang['total_usd']      = "Total USD";
$lang['total_btc']      = "Total BTC";
$lang['profit']         = "Ganho";
$lang['status']         = "Estado";
$lang['total_profit']   = "Ganho total";
$lang['n_transactions'] = "Número de transações";