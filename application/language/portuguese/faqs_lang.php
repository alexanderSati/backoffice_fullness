<?php defined('BASEPATH') OR exit('No direct script access allowed');
/**
 * Admin Language File
 */

// Titles
$lang['admin faqs title']                = "Usuários Fundadores";
$lang['faqs title faq_edit']             = "Editar usuário";
$lang['admin faqs add']                  = "Adicionar usuário";
// Table Columns
$lang['faqs col portuguese_question']      = "Nome de usuário";
$lang['faqs col portuguese_answer']        = "Descrição";
$lang['faqs col status']                = "Estado";
$lang['faqs col id']                    = "ID";
$lang['faqs tooltip add']               = "Adicionar novo usuário";


// Form Inputs
$lang['faqs input portuguese_question']         = "Usuário";
$lang['faqs input portuguese_answer']           = "Descrição";
$lang['faqs input status']                   = "Estado";
$lang['faqs input check']                    = "Select";

// Help
$lang['faqs help passwords']                 = "Basta digitar a senha se você quiser alterá-la.";

// Messages
$lang['faqs msg add_faq_success']           = "%s foi adicionado com sucesso!";
$lang['faqs msg delete_confirm']             = "Isso não pode ser desfeito..";
$lang['faqs msg delete_faq']                = "Foi eliminado com sucesso <strong>%s</strong>!";
$lang['faqs msg edit_profile_success']       = "Seu perfil foi modificado com sucesso!";
$lang['faqs msg edit_faq_success']          = "%s foi modificado com sucesso!!";
$lang['faqs msg register_success']           = "Obrigado por se inscrever, %s! Confirme seu email por uma mensagem de confirmação. Depois que sua conta for verificada, você poderá fazer login com as credenciais fornecidas.";
$lang['faqs msg password_reset_success']     = "Sua senha foi redefinida, %s! Por favor, verifique seu e-mail para sua nova senha temporária.";
$lang['faqs msg validate_success']           = "Sua conta foi verificada. Agora você pode acessar sua conta.";
$lang['faqs msg email_new_account']          = "<p>Obrigado por criar uma conta em% s. Clique no link abaixo para validar seu endereço de e-mail e ativar sua conta.<br /><br /><a href=\"%s\">%s</a></p>";
$lang['faqs msg email_new_account_title']    = "Nova conta %s";
$lang['faqs msg email_password_reset']       = "<p>Sua senha em %s foi redefinida. Clique no link abaixo para fazer o login com sua nova senha:<br /><br /><strong>%s</strong><br /><br /><a href=\"%s\">%s</a>
                                                Uma vez logado, certifique-se de alterar a senha para algo que você possa lembrar.</p>";
$lang['faqs msg email_password_reset_title'] = "Redefinir senha para %s";

$lang['faqs msg user_delete']                = "¿Certamente você quer remover este faq?";
$lang['faqs msg required-input']             = "você deve adicionar a pergunta para os três idiomas espanhol, português e inglês";


// Errors
$lang['faqs error add_faq_failed']          = "%s Seu perfil não pode ser modificado!";
$lang['faqs error delete_faq']              = "<strong>%s</strong> não pôde ser removido!";
$lang['faqs error edit_profile_failed']      = "Seu perfil não pode ser modificado!";
$lang['faqs error edit_faq_failed']         = "%s não pôde ser modificado!";
$lang['faqs error invalid_login']            = "Usuário ou senha inválidos";
$lang['faqs error register_failed']          = "Sua conta não pôde ser criada no momento. Por favor, tente novamente";
$lang['faqs error faq_id_required']         = "Um ID de usuário numérico é obrigatório!";
$lang['faqs error faq_not_exist']           = "Esse usuário não existe!";
$lang['faqs error faqname_exists']          = "O usuário <strong>%s</strong> já existe!";
$lang['faqs error validate_failed']          = "Houve um problema ao validar sua conta. Por favor, tente novamente.";
$lang['faqs error too_many_login_attempts']  = "Você fez muitas tentativas para fazer login muito rapidamente. Por favor espere %s segundos e tente novamente.";

