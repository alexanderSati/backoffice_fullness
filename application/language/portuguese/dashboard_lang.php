<?php defined('BASEPATH') OR exit('No direct script access allowed');
/**
 * Admin Dashboard Language File
 */

// Text
$lang['admin dashboard text welcome']  = "¡Olá e bem vindo ao conselho!";

// Buttons
$lang['admin dashboard btn demo']      = "Clique para demonstração do Jsi18n";

// Jsi18n Demo
$lang['admin dashboard jsi18n-sample'] = "Esta é uma demonstração da biblioteca jsi18n. Leva texto de um arquivo de idioma e insere isto em seus Javascripts. Veja a biblioteca jsi18n.php e dashboard_i18n.js para uso.";

// Dashboard
$lang['d_users']                   = "Usuários Ativos";
$lang['wallet_addresses']          = "Carteira";
$lang['total_btc']                 = "BTC total no site";
$lang['bitcoin_price']             = "Preço Bitcoin";
$lang['affiliate_Link']            = "link de referência";
$lang['revenue']                   = "Renda Total";
$lang['your_profit']               = "Ganhos";
$lang['recent_users']              = "Usuários Recentes";
$lang['market_price']              = "Preço de mercado";
$lang['daily_sale_plans']          = "Venda diária de planos";
$lang['latest_transactions']       = "Últimas transações";
$lang['pending_payments']          = "Pagamentos pendentes";
$lang['payments_done']             = "Pagamentos feitos";

//transactions table
$lang['username']          = "Nome de usuário";
$lang['t_user']            = "Usuario";
$lang['email']             = "E-mail";
$lang['transaction']       = "Transação";
$lang['sender']            = "Remetente";
$lang['Recipient']         = "Contêiner";
$lang['amount']            = "Quantidade";
$lang['time']              = "Data";
$lang['status']            = "Estado";

//gauge
$lang['begins']            = "Começar";
$lang['ends']              = "Termina";
$lang['days_remaining']    = "Dias restantes";
$lang['days']              = "Dias";

$lang['transactions']    =  "Transações";
$lang['daily-bonus']    =  "Bônus diário";
$lang['date']          =  "Data";
$lang['code']          =  "Code";
$lang['description']   =  "Descrição";
$lang['value_btc']     =  "Valor BTC";
$lang['rate']          =  "Taxa";
$lang['value_usd']     =  "Valor em S /";
$lang['status']        =  "Estado";
$lang['no-data']       =  "Nenhum resultado para o gráfico ainda";
$lang['welcome']      = "Bem-vindo ao i9life";

//date
$lang['date_year'] = 'Ano';
$lang['date_years'] = 'Anos';
$lang['date_month'] = 'Mês';
$lang['date_months'] = 'Meses';
$lang['date_week'] = 'Semana';
$lang['date_weeks'] = 'Semanas';
$lang['date_day'] = 'Dia';
$lang['date_days'] = 'Dias';
$lang['date_hour'] = 'Hora';
$lang['date_hours'] = 'Horas';
$lang['date_minute'] = 'Minuto';
$lang['date_minutes'] = 'Minutos';
$lang['date_second'] = 'Segundo';
$lang['date_seconds'] = 'Segundos';

$lang['click_here_to_download'] = "Livro branco";
$lang['file_to_download']           = "Arquivo para baixar";
$lang['countdown_end_of_month'] = 'Dias para "liquidação mensal"';
$lang['label_binary_amount'] = 'Bônus binário';
$lang['label_unilevel_amount'] = 'Bônus unilevel';
$lang['label_commission'] = 'Comissão';
