<?php defined('BASEPATH') OR exit('No direct script access allowed');
/**
 * Settings Language File
 */

// Titles
$lang['admin settings title']             = "Configurações";

// Messages
$lang['admin settings msg save_success']  = "Configurações salvas com sucesso.";

// Errors
$lang['admin settings error save_failed'] = "Houve um problema ao salvar a configuração. Por favor, tente novamente.";

//Settings
$lang['site_name']                 = "Nome do site";
$lang['help_site_name']            = "";

$lang['per_page_limit']            = "Itens por página";
$lang['help_per_page_limit']       = "";

$lang['meta_keywords']             = "Meta Keywords";
$lang['help_meta_keywords']        = "Lista de palavras-chave do site separada por vírgulas";

$lang['meta_description']          = "Descrição Meta";
$lang['help_meta_description']     = "Breve descrição que descreve seu site.";

$lang['site_email']                = "Email";
$lang['help_site_email']           = "E-mail para onde os e-mails serão enviados";

$lang['site_alerts_email']                = "Alertas de email";
$lang['help_site_alerts_email']           = "E-mail onde os e-mails serão enviados com alertas";

$lang['timezones']                 = "Fuso horário";
$lang['help_timezones']            = "";

$lang['privilegies_message']       = "Alerta de privilégio";
$lang['help_privilegies_message']  = "";

$lang['allow_new_users']           = "permitir novos usuários";
$lang['enabled']                   = "Habilitado";
$lang['disabled']                  = "Desativado";
$lang['help_allow_new_users']      = "";
$lang['welcome_message']           = "Mensagem de boas vindas *";

$lang['wallet_1']                 = "Carteira 1";
$lang['help_wallet_1']            = "";

$lang['wallet_2']                 = "Carteira 2";
$lang['help_wallet_2']            = "";

//coin wallets
$lang['wallet_ltc']               = "Carteira Litecoin";
$lang['help_wallet_ltc']          = "";
$lang['wallet_eth']               = "Carteira Ethereum";
$lang['help_wallet_eth']          = "";
$lang['wallet_xmr']               = "Carteira Monero";
$lang['help_wallet_xmr']          = "";
$lang['wallet_dash']              = "Carteira Dash";
$lang['help_wallet_dash']         = "";

/*
 * SMTP Labels
 */
$lang['smtp_host']                  = "SMTP Host";
$lang['smtp_port']                  = "SMTP Porto";
$lang['smtp_user']                  = "SMTP Usuário";
$lang['smtp_password']              = "SMTP Senha";

/*
 * Dashboard news
 */
$lang['dashboards_news']            = "Notícias da Diretoria";
$lang['help_dashboards_news']       = "Digite o slug da categoria para a qual a notícia a ser exibida pertence";

$lang['sign']                       = "Sinais";
$lang['help_sign']                  = "Digite o slug da categoria para a qual o post de sinais a exibir pertence";

$lang['file_to_download']           = "Arquivo para baixar";
$lang['help_file_to_download']      = "Digite o link do arquivo para fazer o download";