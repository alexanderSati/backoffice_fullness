<?php defined('BASEPATH') OR exit('No direct script access allowed');
/**
 * Core Language File
 */

// Buttons
$lang['core button admin']                   = "Admin";
$lang['core button cancel']              	= "Cancelar";
$lang['core button close']               	= "Fechar";
$lang['core button contact']               	= "Entre em contato";
$lang['core button filter']              	= "Buscar";
$lang['core button home']                	= "Home";
$lang['core button login']               	= "Iniciar sessão";
$lang['core button logout']              	= "Fechar Sessão";
$lang['core button profile']              	= "Perfil";
$lang['core button reset']               	= "Reinicialização";
$lang['core button register']               	= "Registrar";
$lang['core button save']                	= "Salvar";
$lang['core button search']              	= "Pesquisar";
$lang['core button toggle_nav']          	= "Toggle navigation";
$lang['core button link-left']               = "Clique no campo para copiar o link na linha esquerda";
$lang['core button link-right']              = "Clique no campo para copiar o link na linha direita";
$lang['core button link-shop']               = "Clique no campo para copiar o link de vendas";

// Text
$lang['core text no']                    	= "Não";
$lang['core text yes']                   	= "Sim";
$lang['core text page_rendered']            = "Página gerada em <strong>{elapsed_time}</strong> segundos";

// Emails
$lang['core email start']                	= "<!DOCTYPE html><html><head><style>
                                                    body { background: #fff; color: #333; }
                                               </style></head><body>";
$lang['core email end']                  	= "</body></html>";

// Additional date_lang
$lang['UM75']	                         	= "(UTC -7:00) Arizona Time";

// Errors
$lang['core error no_results']              = "¡Não foram encontrados resultados!";
$lang['core error session_language']        = "¡Houve um problema ao definir o idioma!";

$lang['years']      = "Anos";
$lang['months']     = "Meses";
$lang['days']       = "Dias";
$lang['hours']      = "Horas";
$lang['minutes']    = "Minutos";
$lang['seconds']    = "Segundos";

$lang['access_denied'] = "Acesso negado. Você não tem permissões suficientes";

$lang['captcha error'] = "¡A verificação não está correta!";

$lang['verify-spam'] = "* Está disponível no seu perfil. ";
$lang['pin_tooltip'] = "Está disponível no seu perfil.";
$lang['information'] = "* Apenas 4 caracteres";
$lang['end_of_the_month_message'] = "Ação não disponível no momento, tente em 1 hora. (fim do mês)";