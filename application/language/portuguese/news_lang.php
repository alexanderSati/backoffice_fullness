<?php defined('BASEPATH') OR exit('No direct script access allowed');
/**
 * Settings Language File
 */

// Titles
$lang['news'] = "Notícias";

$lang['error_news'] = "Notícias não encontradas";
$lang['welcome']      = "Últimas Atualizações";