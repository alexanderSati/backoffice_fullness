<?php defined('BASEPATH') OR exit('No direct script access allowed');
/**
 * Settings Language File
 */

// Titles
$lang['news'] = "Noticias";

$lang['error_news'] = "Noticia no encontrada";
$lang['welcome']      = "Últimas actualizaciones";