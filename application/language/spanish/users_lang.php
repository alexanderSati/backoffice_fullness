<?php defined('BASEPATH') OR exit('No direct script access allowed');
/**
 * Users Language File
 */

// Titles
$lang['users title forgot']                   = "Olvido su contraseña";
$lang['users title login']                    = "Iniciar sesión";
$lang['users title profile']                  = "Perfil";
$lang['users title register']                 = "Registrarse";
$lang['users title user_add']                 = "Añadir usuario";
$lang['users title customer_add']             = "Añadir cliente";
$lang['users title user_delete']              = "Confirmar Eliminar usuario";
$lang['users title user_edit']                = "Editar usuario";
$lang['users title user_list']                = "Lista de usuarios";
$lang['users title user_stats']               = "Estadísticas de usuarios";
$lang['users title account']                  = "Mi cuenta";
$lang['users title transactions']             = "Transacciones";
$lang['users title transfer']                 = "Transferencia";
$lang['users title deposit']                  = "Nuevo deposito";
$lang['users title payout']                   = "Pago";
$lang['users title wallets']                  = "Billetera";
$lang['users title binary']                   = "Red binaria";
$lang['users title unassigned']               = "Usuarios no asignados";
$lang['users title wallets transfer']         = "Transferencia de billeteras";
$lang['users title btc-wallets transfer']     = "Trasnferencias BTC";
$lang['users title register qr_to_pay']       = "Escanea el código qr para procesar su deposito";
$lang['users title renew-subscription']       = "Renovar suscripción";
$lang['users title link']                     = "Links";
$lang['users title categories_list']          = "Lista categorías";
$lang['users title category_edit']            = "Editar categoría";
$lang['users title category_add']             = "Añadir categoría";

$lang['users title product_list']             = "Lista de productos";
$lang['users title product_add']              = "Añadir producto";
$lang['users title new_site']                 = "Nuevo en el sitio?";
$lang['users title change_language']          = "Cambiar idioma";

$lang['users title product_edit']             = "Editar producto";
$lang['users title total_referrals']          = "Referidos";
$lang['users title saldo']                    = "Saldo en plataforma";
$lang['users title binary_points']            = "Puntos binarios";
$lang['users title plan_active']              = "Este es su plan activo";
$lang['users title residual_points']          = "Puntos residuales";
$lang['users title left-points']              = "Puntos izquierda";
$lang['users title right-points']             = "Puntos derecha";
$lang['users title send_to']                  = "Envío a";
$lang['users title order']                    = "Orden";



$lang['users title shipping_details']         = "Puntos de recogida";
$lang['users title add_shipping_details']     = "Crear punto de recogida";
$lang['users title edit_shipping_details']    = "Editar punto de recogida";

$lang['users title products_quantities']      = "Stock de productos";
$lang['users title add_products_quantities']  = "Crear stock de producto";
$lang['users title edit_products_quantities'] = "Editar stock producto";

$lang['users title awards']                   = "Premios";
$lang['users title list_administrators']      = "Lista de administradores";
$lang['users title edit_admin']               = "Editar administrador";
$lang['users title admin_add']                = "Añadir administrador";

$lang['users title cart_list']                = "Lista de la compra";
$lang['users title next_prize']               = "Siguiente premio";
$lang['users title point_missing']            = "Puntos faltantes";
$lang['users title progress_bar']             = "Barra de progreso";
$lang['users title select_option']            = "Elija una opción";
$lang['users title preferred_consumer']       = "Consumidor Preferente";
$lang['users title points_in_residual']       = "puntos residuales";
$lang['users title points_in_binary']         = "puntos binarios";
$lang['users title shipping_method']          = "Método de envío";
$lang['users title payment_method']           = "Método de pago";
$lang['users title available']                = "Disponible";
$lang['users title confirm_purchase']         = "Confirmar compra";

$lang['users title total_points_binary']      = "Puntaje acumulativo binario de la linea de pago o rama menor";
$lang['users title maximum_plan']             = "Mejora de plan no disponible";

$lang['users title banking_pay']              = "Pago bancario";
$lang['users title executive_add']            = "Añadir ejecutivo";

$lang['users title executive_edit']           = "Editar ejecutivo";

$lang['users title external_store']           = "Tienda";
$lang['users title personal_informacion']     = "Información personal";
$lang['users title unilevel']                 = "Unilevel";
$lang['users titel unilevel-rank']            = "Rango";


$lang['users title month-wallet']             = "Filtrar por mes";
$lang['users title line-max']                 = "Linea maxima";
$lang['users title bonus']                    = "Bono";
$lang['users title grupal-point']             = "Puntos grupales";

// Buttons
$lang['users button add_new_user']            = "Añadir usuario nuevo";
$lang['users button register']                = "Crear cuenta";
$lang['users button reset_password']          = "Restablecer la contraseña";
$lang['users button login_try_again']         = "Inténtalo de nuevo";
$lang['users button add_new_category']        = "Añadir nueva categoría";
$lang['users button add_new_product']         = "Añadir nuevo producto";
$lang['users button cancel']                  = "Cancelar";
$lang['users button confirm']                 = "Confirmar";
$lang['users button add_shipping_details']    = "Crear punto de recogida";
$lang['users button add_product_quantities']  = "Crear stock de producto";
$lang['users button accept']                  = "Aceptar";
$lang['users button add_new_executive']       = "Añadir nuevo ejecutivo";


// Tooltips
$lang['users tooltip add_new_user']           = "Crear un nuevo usuario.";
$lang['users tooltip add_new_category']       = "Crear una nueva categoría.";
$lang['users tooltip add_new_product']        = "Crear un nuevo producto.";
$lang['users tooltip add_new_shipping']       = "Crear un nuevo punto de recogida.";
$lang['users tooltip add_new_product_quantities']  = "Crear una nuevo stock de producto.";
$lang['users tooltip add_new_executive']      = "Crea un nuevo ejecutivo";




// Links
$lang['users link forgot_password']           = "¿Olvidaste tu contraseña?";
$lang['users link register_account']          = "Regístrese para una cuenta.";

// Table Columns
$lang['users col fullname']                   = "Nombre completo";
$lang['users col first_name']                 = "Nombre";
$lang['users col is_admin']                   = "Administrador";
$lang['users col last_name']                  = "Apellido";
$lang['users col user_id']                    = "ID";
$lang['users col username']                   = "Nombre de usuario";
$lang['users col created']                    = "Fecha de registro";
$lang['users col wallets']                    = "Billeteras (USD)";
$lang['users col BTC']                        = "BTC";
$lang['users col XMR']                        = "XMR";
$lang['users col ETH']                        = "ETH";
$lang['users col LTC']                        = "LTC";
$lang['users col TRUMP']                      = "TRUMP";
$lang['users col DASH']                       = "DASH";
$lang['users col SLR']                        = "SLR";
$lang['users col USD']                        = "BTC operative";
$lang['users col total']                      = "Total USD en billeteras";
$lang['users col profit']                     = "Ingresos";
$lang['users col payout']                     = "Pagos";
$lang['users col categories_id']              = "ID";
$lang['users col name']                       = "Nombre";
$lang['users col description']                = "Descripción";
$lang['users col product_name']               = "Nombre producto";
$lang['users col product_price']              = "Precio cliente";
$lang['users col product_price_distributor']  = "Precio distribuidor";
$lang['users col code']                       = "Codigo";
$lang['users col name_details']               = "Punto de recogida";
$lang['users col quantities']                 = "Cantidad";
$lang['users col amount']                     = "Valor";




// Form Inputs
$lang['users input email']                    = "Correo";
$lang['users input first_name']               = "Nombre";
$lang['users input is_admin']                 = "Es administrador";
$lang['users input language']                 = "Idioma";
$lang['users input id_user']                  = "usuario ID";
$lang['users input amount']                   = "Cantidad";
$lang['users input token']                    = "Token";
$lang['users input user']                     = "Usuario";
$lang['users input wallet']                   = "Billetera";
$lang['users input qr']                       = "Codigo QR";
$lang['users input country']                  = "País";
$lang['users input plan']                     = "Plan";
$lang['users input profile']                  = "Perfil";
$lang['profile-admin']                        = "Administrador";
$lang['profile-executive']                    = "Ejecutivo de cuentas";
$lang['profile-user']                         = "Usuario General";
$lang['users input last_name']                = "Apellido";
$lang['users input password']                 = "Contraseña";
$lang['users input password_repeat']          = "Repetir contraseña";
$lang['users input security_pin']             = "PIN de seguridad";
$lang['users input security_pin_repeat']      = "Repetir PIN de seguridad";
$lang['users input status']                   = "Estado";
$lang['users input side']                     = "Mi red";
$lang['users input side-left']                = "Linea de negocio izquierda";
$lang['users input side-right']               = "Linea de negocio derecha";
$lang['users input remember']                 = "Recuerda, Esta opcion es irreversible";
$lang['users input username']                 = "Nombre de usuario";
$lang['users input username_email']           = "Nombre de usuario o Correo";
$lang['users input check']                    = "Seleccionar";
$lang['users input image']                    = "Seleccionar Imagen";
$lang['users input terms']                    = "Acepto los <a href='https://bitwexchange.com/es/terminos-y-condiciones/' target='blank'>términos y condiciones</a> de uso";
$lang['users input dni']                      = "DNI";
$lang['users input birthdate']                = "Fecha de nacimiento";
$lang['users input phone']                    = "Telefono";
$lang['users input address']                  = "Dirección";
$lang['users input address2']                 = "Dirección Línea 2";
$lang['users input city']                     = "Ciudad";
$lang['users input department']               = "Departamento";
$lang['users input postal_code']              = "Código postal";
$lang['users input bank_account']             = "Descripción de una cuenta bancaria";

$lang['users input name']                     = "Nombre";
$lang['users input description']              = "Descripción";

$lang['users input product_name']             = "Nombre del producto";
$lang['users input product_price']            = "Precio cliente";
$lang['users input category_id']              = "Categoría";
$lang['users input product_price_distributor']  = "Precio distribuidor";
$lang['users input code']                     = "Codigo";
$lang['users input residual_points']          = "Puntos residuales";
$lang['users input binary_points']           = "Binario";
$lang['users input stock_quantity']           = "Stock";
$lang['users input shipping_types_id']        = "Tipo de envío";
$lang['users input shiping_details_id']       = "Punto de recogida";
$lang['users input quantity']                 = "Cantidad";
$lang['users input shiping_details']          = "Oficina de envio";



// Help
$lang['users help passwords']                 = "Solo escribe la contraseña si deseas cambiarla.";

// Messages
$lang['users msg add_user_success']           = "%s fue añadido exitosamente!";
$lang['users msg delete_confirm']             = "¿Seguro que quieres eliminar <strong>%s</ strong>? Esto no se puede deshacer.";
$lang['users msg delete_user']                = "Ha sido eliminado correctamente <strong>%s</strong>!";
$lang['users msg edit_profile_success']       = "Su perfil se modificó con éxito!";
$lang['users msg edit_user_success']          = "%s fue modificado exitosamente!!";
$lang['users msg register_success']           = "Gracias por registrarse, %s! Verifica tu correo por un mensaje de confirmación. Una vez que su cuenta ha sido verificada, usted será capaz de iniciar sesión con las credenciales proporcionadas.";
$lang['users msg password_reset_success']     = "Tu contraseña ha sido restablecida, %s! Por favor revise su correo electrónico para su nueva contraseña temporal.";
$lang['users msg validate_success']           = "Tu cuenta ha sido verificada. Ahora puede acceder a su cuenta.";
$lang['users msg email_new_account']          = "<p>
    <img width=\"260\" src=\"https://app.bitwexchange.com/themes/gentella/images/logo.png\"
         alt=\"Bitwexchange\">
</p><p>Gracias por crear una cuenta en %s. Haga clic en el enlace de abajo para validar su dirección de correo electrónico y activar su cuenta.<br /><br /><a href=\"%s\">%s</a></p>";
$lang['users msg email_new_account_title']    = "Nueva Cuenta de %s";
$lang['users msg email_password_reset']       = "<p>
    <img width=\"260\" src=\"https://app.bitwexchange.com/themes/gentella/images/logo.png\"
         alt=\"Bitwexchange\">
</p><p>Su contraseña en %s ha sido restablecida. Haga clic en el enlace de abajo para iniciar sesión con su nueva contraseña:<br /><br /><strong>%s</strong><br /><br /><a href=\"%s\">%s</a>
                                                 Una vez iniciada la sesión, asegúrese de cambiar la contraseña a algo que pueda recordar.</p>";
$lang['users msg email_password_reset_title'] = "Restablecer contraseña para %s";
$lang['users link copy']                      = "Link copiado satisfactoriamente al portapapeles";
$lang['users msg edit_category_success']      = "La categoría %s fue modificada exitosamente!!";
$lang['users msg add_category_success']       = "La categoría %s fue añadida exitosamente!";

$lang['users msg edit_product_success']       = "El producto %s fue modificado exitosamente!!";
$lang['users msg add_product_success']        = "El producto %s fue añadido exitosamente!";

$lang['users msg remove_product']             = "Tu producto <strong>%s</strong> fue retirado satisfactoriamente";
$lang['users msg activate_product']           = "Tu producto <strong>%s</strong> fue activado satisfactoriamente";

$lang['users msg remove-sure']                = "Esta seguro de retirar este producto?";
$lang['users msg activate-sure']              = "Esta seguro de activar este producto?";

$lang['users msg transfer']              = "Esta seguro de transferir el dinero?";

$lang['users msg add_shipping_detail']        = "El puntos de recogida %s fue añadido exitosamente!";
$lang['users msg edit_shipping_detail']       = "El puntos de recogida %s fue modificado exitosamente!";

$lang['users msg add_product_quantity']       = "El stock del producto fue añadida exitosamente!";
$lang['users msg edit_product_quantity']      = "El stock del producto fue modificada exitosamente!";

$lang['users msg deny-sure']                  = "Estas seguro de denegar este payout?";
$lang['users msg aprrove-sure']               = "Estas seguro de aprobar este payout?";

$lang['users msg confirm_select_option']      = "¿Estas segur@ de elegir %s?";
$lang['users msg confirm_preferred']          = "¿Estas segur@ de seleccionar esta opcion?";

$lang['users msg confirm_clear_cart']          = "¿Esta segur@ de vaciar el carro?";
$lang['users msg confirm_remove_inventory']    = "¿Esta seguro@ de eliminar este elemento?";
$lang['users msg correct_clear']               = "Carro de compras vaciado!";
$lang['users msg correct_remove_inventory']    = "Elemento eliminado!";
$lang['users msg maximum_plan']                = "Actualmente tiene el último plan y no tiene una mejora disponible.";





// Errors
$lang['users error add_user_failed']          = "%s no pudo ser añadido!";
$lang['users error delete_user']              = "<strong>%s</strong> no pudo ser eliminado!";
$lang['users error edit_profile_failed']      = "Su perfil no puede ser modificado!";
$lang['users error edit_user_failed']         = "%s no pudo ser modificado!";
$lang['users error email_exists']             = "El correo electrónico <strong>%s</strong> ya existe!";
$lang['users error email_not_exists']         = "Ese correo electrónico no existe!";
$lang['users error username_not_exists']      = "Ese nombre de usuario no existe!";
$lang['users error invalid_login']            = "Usuario o contraseña invalido";
$lang['users error password_reset_failed']    = "Hubo un problema al restablecer su contraseña. Por favor, vuelva a intentarlo.";
$lang['users error register_failed']          = "Tu cuenta no pudo crearce en este momento. Por favor, vuelva a intentarlo.";
$lang['users error user_id_required']         = "Se requiere un ID de usuario numérico!";
$lang['users error user_not_exist']           = "Ese usuario no existe!";
$lang['users error username_exists']          = "El usuario <strong>%s</strong> ya existe!";
$lang['users error validate_failed']          = "Hubo un problema al validar su cuenta. Por favor, vuelva a intentarlo.";
$lang['users error validate_referral_failed'] = "Hubo un problema al validar el usuario que lo refiere. Por favor, vuelva a intentarlo.";
$lang['users error too_many_login_attempts']  = "Usted ha hecho demasiados intentos para iniciar sesión con demasiada rapidez. Por favor, espere %s segundos y vuelva a intentarlo.";
$lang['users error edit_category_failed']     = "La categoría %s no pudo ser modificada!";
$lang['users error add_category_failed']      = "La categoría %s no pudo ser añadida!";

$lang['users error edit_product_failed']      = "El producto %s no pudo ser modificado!";
$lang['users error add_product_failed']       = "El producto %s no pudo ser añadido!";

$lang['users error remove_product']           = "El producto %s no pudo ser retirado";
$lang['users error activate_product']         = "El producto %s no pudo ser activado!";
$lang['users error product_not_exist']        = "Este producto no existe!";
$lang['users error product_id_required']      = "Se requiere un ID de producto numérico!";
$lang['users error add_shipping_detail_failed'] = "El punto de recogida %s no pudo ser añadido!";
$lang['users error edit_shipping_detail_failed'] = "El punto de recogida %s no pudo ser modificado!";

$lang['users error add_product_quantity'] = "El stock del producto no pudo ser añadida!";
$lang['users error edit_product_quantity'] = "El stock del producto no pudo ser modificada!";

$lang['users error number_negative']          = "La cantidad no puede ser negativa o 0";
$lang['users error payout_by_active']         = "Debes activar su cuenta para realizar el payout";
$lang['users error missing_purchases']        = "Debe realizar una compra mayor a 220 soles para realizar el payout";
$lang['users error days_passed']              = "Sólo puede realizar el payout del 1 al 8 de cada  mes";

$lang['users error category_exists']          = "Ya existe una categoría con este nombre";
$lang['users error product_exists']          = "Ya existe un producto con este nombre";
$lang['users error shipping_exists']          = "Ya existe un punto de recogida con este nombre";

$lang['users error is_preferencial_user']     = "Eres un consumidor preferente, los consumidores preferentes no pueden agregar usuarios";
$lang['users error is_not_active_add']        = "Debe activar su cuenta con un plan para poder realizar esta acción";
$lang['users error is_not_active_transfer']   = "Debes activar tu cuenta con un plan para hacer una transferencia";

$lang['users error is_preferencial_user_register'] = "El usuario por el que intenta referirse es un consumidor preferente, solo se puede referir de usuarios con plan";
$lang['users error plan_not_select']  = "Debe seleccionar un plan";
$lang['users error exist_shiping_details_id'] = "El producto ya está vinculado a ese punto de recogida";

//account
$lang['my-referrals']    =  "Mis referidos";
$lang['account-plan']    =  "Plan de la cuenta";
$lang['account-rank']    =  "Prestigio actual";
$lang['current-plan']    =  "Plan actual";
$lang['current-rank']    =  "Rango actual";
$lang['plan-list']       =  "Lista de planes:";
$lang['rank-list']       =  "Lista de rangos:";
$lang['details']         =  "Detalles";
$lang['volume']          =  "Volumen";
$lang['period_days']     =  "Días del plan";
$lang['days_remaining']  =  "Días restantes";
$lang['daily']           =  "Diariamente";
$lang['profit']          =  "Ganancia";
$lang['points']          =  "puntos";
$lang['residual']        =  "Residual";
$lang['label_residual']  =  "Detallado de Puntos Residuales";


$lang['enough-funds']       = "Fondos insuficientes para la transferencia";
$lang['user-already']       = "El usuario ya es un hijo";
$lang['invalid-side']          = "Lado no válido";
$lang['side-busy']       = "El lado no está disponible (ocupado)";
$lang['parent-not']         = "Árbol padre no existe";
$lang['error update side']  = "Hubo un error al actualizar el estado del usuario";
$lang['success update side']= "El estado del usuario se ha actualizado exitosamente";

// Notifications

$lang['users info waiting approval'] = "Su cuenta ha sido creada.";
$lang['no-data-table'] = "No hay datos encontrados";

//binary
$lang['side']               = "Linea de Negocio";
$lang['left-points']        = "Puntaje Línea de Negocio Izquierda";
$lang['right-points']       = "Puntaje Línea de Negocio Derecha";
$lang['total-points']       = "Puntos totales";
$lang['label_points_left']  = "Puntaje Izquierdo";
$lang['label_points_right'] = "Puntaje Derecho";
$lang['label_binary'] 		= "Detallado de Puntos Binarios";

//token
$lang['token']              = "Token";
$lang['request-token']      = "Solicitar token";
$lang['send-token']         = "El token se ha enviado a tu correo electrónico";
$lang['show-token']         = "Por favor usa el siguiente token %s";
$lang['token-error']        = "El token ingresado es invalido, por favor solicita otro token";


$lang['access_denied_expired']  = "Lo sentimos, su inversion a finalizado, verifique los detalles de su cuenta o contacte a un ejecutivo de cuenta";
$lang['renew']                  = "Click aqui para renovar";
$lang['renewal-success']        = "Su cuenta ha sido renovada con éxito.";
$lang['renewal-pending']        = "Su cuenta será renovada una vez se verifique su pago.";
$lang['error required_renewal'] = "Renovación de cuenta requerida.";
$lang['select-coin']            = "Seleccione la moneda a depositar";

$lang['upgrade']                = "Mejorar";
$lang['upgrade_title']          = "Subir status";
$lang['upgrade_button']         = "Click aqui para subir status";
$lang['upgrade_success']        = "Felicidades, su cuenta ha sido actualizada exitosamente.";

$lang['active_since']           = "Activo desde";
$lang['modal_title']            = "Confirmación";
$lang['question_confirm']       = "Estas seguro de actualizar tu plan?";
$lang['days_left']              = "Dias restantes";

$lang['plan_current']           = "El plan seleccionado es su actual plan.";
$lang['plan_donwgrade']         = "Lo sentimos, no puede cambiar su plan a uno inferior.";
$lang['invalid_subscription']   = "No se encontraron suscripciones activas.";
$lang['invalid-wallet']         = "Billetera invalida.";
$lang['invalid-plan']           = "El plan seleccionado es invalido.";
$lang['payment_fail']           = "Lo sentimos, el pago no pudo ser procesado.";
$lang['not-enough-funds']       = "Fondos insifucientes en su billetera operativa.";

$lang['back_to_home']           = "Volver al inicio";
$lang['your_plan']              = "Su plan";

/***
 * Awards
 */
$lang['cruise']                 = "CRUCERO";
$lang['suitcase']               = "MALETIN 1/2 MILLON";
$lang['house_million']          = "CASA 1 MILLÓN";
$lang['rank']                   = "Rangos";


/*
 * Monthly payments
 */
$lang['monthly_suspended'] = "Actualmente su cuenta se encuentra inactiva";
$lang['monthly_pending'] = "Su cuenta tiene mensualidades pendientes de pago. <strong><a href='/user/transactions/deposit'>(Pagar ahora)</a></strong>";
$lang['first_login'] = "Bienvenido, el siguiente paso es adquirir tu franquicia personal <a href='/Cart' style='color: white'>Click aquí</a>";
$lang["no_active_user_refer"] = "Debe registrarse como socio activo para referir usuarios";
$lang["lbl_commision"] = "Comisión";
$lang["lbl_discount"] = "Descuento";
$lang["lbl_status"] = "Estado";
$lang['form_validation_exist']		= 'El valor del campo {field} ya ha sido registrado.';
$lang['payment_method']  = "Método de pago";