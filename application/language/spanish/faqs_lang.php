<?php defined('BASEPATH') OR exit('No direct script access allowed');
/**
 * Admin Language File
 */

// Titles
$lang['admin faqs title']                = "Usuarios Fundadores";
$lang['faqs title faq_edit']             = "Editar usuario";
$lang['admin faqs add']                  = "Agregar usuario";
// Table Columns
$lang['faqs col spanish_question']      = "Usuario";
$lang['faqs col spanish_answer']        = "Descripción";
$lang['faqs col status']                = "Estado";
$lang['faqs col id']                    = "ID";
$lang['faqs tooltip add']               = "Agregar nuevo usuario";


// Form Inputs
$lang['faqs input spanish_question']         = "Usuario";
$lang['faqs input spanish_answer']           = "Descripción";
$lang['faqs input status']                   = "Estado";
$lang['faqs input check']                    = "Select";

// Help
$lang['faqs help passwords']                 = "Solo escribe la contraseña si deseas cambiarla.";

// Messages
$lang['faqs msg add_faq_success']           = "%s fue añadido exitosamente!";
$lang['faqs msg delete_confirm']             = "Esto no se puede deshacer.";
$lang['faqs msg delete_faq']                = "Ha sido eliminado correctamente <strong>%s</strong>!";
$lang['faqs msg edit_profile_success']       = "Su perfil se modificó con éxito!";
$lang['faqs msg edit_faq_success']          = "%s fue modificado exitosamente!!";
$lang['faqs msg register_success']           = "Gracias por registrarse, %s! Verifica tu correo por un mensaje de confirmación. Una vez que su cuenta ha sido verificada, usted será capaz de iniciar sesión con las credenciales proporcionadas.";
$lang['faqs msg password_reset_success']     = "Tu contraseña ha sido restablecida, %s! Por favor revise su correo electrónico para su nueva contraseña temporal.";
$lang['faqs msg validate_success']           = "Tu cuenta ha sido verificada. Ahora puede acceder a su cuenta.";
$lang['faqs msg email_new_account']          = "<p>Gracias por crear una cuenta en %s. Haga clic en el enlace de abajo para validar su dirección de correo electrónico y activar su cuenta.<br /><br /><a href=\"%s\">%s</a></p>";
$lang['faqs msg email_new_account_title']    = "Nueva Cuenta de %s";
$lang['faqs msg email_password_reset']       = "<p>Su contraseña en %s ha sido restablecida. Haga clic en el enlace de abajo para iniciar sesión con su nueva contraseña:<br /><br /><strong>%s</strong><br /><br /><a href=\"%s\">%s</a>
                                                 Una vez iniciada la sesión, asegúrese de cambiar la contraseña a algo que pueda recordar.</p>";
$lang['faqs msg email_password_reset_title'] = "Restablecer contraseña para %s";

$lang['faqs msg user_delete']                = "¿Seguro que quieres eliminar este faq?";
$lang['faqs msg required-input']             = "debe agregar la pregunta para los tres idiomas español, portugues y ingles";


// Errors
$lang['faqs error add_faq_failed']          = "%s no pudo ser añadido!";
$lang['faqs error delete_faq']              = "<strong>%s</strong> no pudo ser eliminado!";
$lang['faqs error edit_profile_failed']      = "Su perfil no puede ser modificado!";
$lang['faqs error edit_faq_failed']         = "%s no pudo ser modificado!";
$lang['faqs error invalid_login']            = "Usuario o contraseña invalido";
$lang['faqs error register_failed']          = "Tu cuenta no pudo crearce en este momento. Por favor, vuelva a intentarlo.";
$lang['faqs error faq_id_required']         = "Se requiere un ID de usuario numérico!";
$lang['faqs error faq_not_exist']           = "Ese usuario no existe!";
$lang['faqs error faqname_exists']          = "El usuario <strong>%s</strong> ya existe!";
$lang['faqs error validate_failed']          = "Hubo un problema al validar su cuenta. Por favor, vuelva a intentarlo.";
$lang['faqs error too_many_login_attempts']  = "Usted ha hecho demasiados intentos para iniciar sesión con demasiada rapidez. Por favor, espere %s segundos y vuelva a intentarlo.";

