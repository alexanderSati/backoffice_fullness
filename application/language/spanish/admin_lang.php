<?php defined('BASEPATH') OR exit('No direct script access allowed');
/**
 * Admin Language File
 */

// Titles
$lang['admin title admin']                = "Administración";
$lang['admin title pending transactions'] = "Transacciones Pendientes";
$lang['admin title transactions']         = "Todas las transacciones";
$lang['admin title payout_request']       = "Lista de payouts";  

// Buttons
$lang['admin button csv_export']          = "CSV Exporte";
$lang['admin button dashboard']           = "Dashboard";
$lang['admin button delete']              = "Borrar";
$lang['admin button edit']                = "Editar";
$lang['admin button messages']            = "Mensajes";
$lang['admin button settings']            = "Ajustes";
$lang['admin button users']               = "Usuarios";
$lang['admin button users_add']           = "Añadir usuario nuevo";
$lang['admin button users_list']          = "Lista de Usuarios";
$lang['admin button admin_list']          = "Lista de Administradores";
$lang['admin button activate']            = "Activar";
$lang['admin button remove']              = "Retirar";

// Tooltips
$lang['admin tooltip csv_export']         = "Exportación de un archivo CSV de todos los resultados con los filtros aplicados.";
$lang['admin tooltip filter']             = "Actualizar resultados en función de sus filtros.";
$lang['admin tooltip filter_reset']       = "Borrar todos los filtros y clasificación.";

// Form Inputs
$lang['admin input active']               = "Activo";
$lang['admin input inactive']             = "Inactivo";
$lang['admin input items_per_page']       = "items/page";
$lang['admin input select']               = "escoje...";
$lang['admin input username']             = "Nombre de usuario";

//Week days
$lang['admin input monday']                 = "Lunes";
$lang['admin input thuesday']               = "Martes";
$lang['admin input wednesday']              = "Miercoles";
$lang['admin input thursday']               = "Jueves";
$lang['admin input friday']                 = "Viernes";
$lang['admin input saturday']               = "Sábado";
$lang['admin input sunday']                 = "Domingo";

// Table Columns
$lang['admin col actions']                = "Acciones";
$lang['admin col status']                 = "Estado";
$lang['admin col plan']                   = "Plan";
$lang['admin col rank']                   = "Rango";
$lang['admin col approve']                = "Aprobar";  
$lang['admin col denied']                 = "Denegar";  


// Form Labels
$lang['admin label rows']                 = "%s Registro(s)";

$lang['error update']                     = "La transacción no se puedo actualizar";
$lang['success update']                   = "Transacción actualizada con éxito";
