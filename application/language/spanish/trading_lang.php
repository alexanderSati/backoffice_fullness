<?php defined('BASEPATH') OR exit('No direct script access allowed');
/**
 * Transactions Language File
 */

$lang['amount']         = "Cantidad";
$lang['dollar-price']   = "Precio";
$lang['coin-tobuy']     = "Moneda a comprar";
$lang['coin-to-sell']   = "Moneda a vrnder";
$lang['make-trading']   = "Negociar moneda";
$lang['actions']        = "Acciones";

// Sell and Buy orders
$lang['time']           = "Hora";
$lang['balance']        = "Fondos";
$lang['price']          = "Precio";
$lang['total']          = "Total";
$lang['fee']            = "Cuota";
$lang['total_fee']      = "Total + Cuota";
$lang['total_fee_less'] = "Total - Cuota";
$lang['buy']            = "Comprar";
$lang['sell']           = "Vender";
$lang['buy_trading']    = "Comprar";
$lang['trade_history']  = "Historial del Mercado";
$lang['sell_order']     = "Ofertas de ventas";
$lang['buy_order']      = "Oferta de compra";
$lang['cancel']         = "Cancelar";


/*
 *New module errors
 */

$lang['tradig_invalid_action'] = "Accion invalida.";
$lang['tradig_invalid_coin'] = "Moneda no valida.";
$lang['tradig_invalid_amount'] = "La cantidad ingresada no cumple con los requisitos del mercado.";
$lang['tradig_invalid_price'] = "El precio en BTC no cumple con los requisitos del mercado.";

$lang['tradig_not_funds_wallet'] = "Fondos insuficientes en la billetera seleccionada.";
$lang['tradig_not_funds_btc_wallet'] = "Fondos insuficientes en la billetera operativa.";
$lang['tradig_not_published'] = "Su oferta no pudo ser publicada.";
$lang['tradig_published'] = "Su oferta fue publicada existosamente.";
$lang['tradig_processed'] = "La transaccion de proceso exitosamente.";
$lang['tradig_canceled'] = "Su oferta ha sido cancelada y reembolsada correctamente.";

$lang['tradig_not_found'] = "Oferta no encontrada.";
$lang['tradig_not_available'] = "Lo lamentamos, esta oferta ya no se encuentra disponible.";
$lang['tradig_access_denied'] = "Acceso denegado.";

$lang['form_fee_text'] = "La cuota del (0.8%) es procesada en bitcoins.";


//Admin
$lang['trading_history']= "Historial de trading";
$lang['buy_user']       = "Usuario comprador";
$lang['sell_user']      = "Usuario vendedor";
$lang['date']           = "Fecha";
$lang['type']           = "Tipo";
$lang['coin']           = "Moneda";
$lang['total_usd']      = "Total USD";
$lang['total_btc']      = "Total BTC";
$lang['profit']         = "Ganacia";
$lang['status']         = "Estado";
$lang['total_profit']   = "Ganacia total";
$lang['n_transactions'] = "Número de transacciones";