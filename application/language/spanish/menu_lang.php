<?php defined('BASEPATH') OR exit('No direct script access allowed');
/**
 * Menu Language File
 */

// Titles menu
$lang['dashboard']                = "Inicio";
$lang['users-groups']             = "Usuarios y Grupos";
$lang['plans']                    = "Planes";
$lang['wallets']                  = "Billetera";
$lang['trading']                  = "Comercio";
$lang['transactions']             = "Transacciones";
$lang['btc_wallet_transfer']      = "Transferencias Billeteras";
$lang['pending-transactions']     = "Transacciones Pendientes";
$lang['reports']                  = "Reportes";
$lang['faqs']                     = "Us. Fundadores";
$lang['settings']                 = "Configuración";
$lang['logout']                   = "Cerrar sesión";
$lang['new-admin']                = "Nuevo administrador";
$lang['administrators']           = "Administradores";
$lang['shopping']                 = "Tienda";
$lang['categories']               = "Categorías";
$lang['products']                 = "Productos";
$lang['products-user']            = "Comprar";
$lang['shop']                 	  = "Tienda";
$lang['purchases-user']           = "Historico de órdenes";
$lang['orders']           		  = "Órdenes";
$lang['shipping']                 = "Puntos de recogida";
$lang['products_quantities']      = "Stock de productos";
$lang['my-transactions']          = "Mis transacciones";
$lang['executive']                = "Ejecutivos";
$lang['unilevel']                 = "Unilevel";  
$lang['my-stock']                 = "Mi stock";  

$lang['new-user']                 = "Nuevo usuario";
$lang['users']                    = "Usuarios";
$lang['Users-by-capital']         = "Estadísticas de usuarios";

$lang['new-plan']                 = "Nuevo plan";
$lang['plans']                    = "Planes";
$lang['account']                  = "Mi cuenta";
$lang['binary_tree']              = "Binario";
$lang['binary_networks']          = "Mis Redes";
$lang['binary_unassigned']        = "Usuarios no asignados";

$lang['new-faq']                  = "Nuevo";
$lang['privilegies']              = "Privilegios";

$lang['transfers']              = "Transferencias";
$lang['deposit']                = "Depósito";
$lang['btc-deposit']            = "BTC Depósito";
$lang['coin-deposit']           = "Depósito de monedas";
$lang['payout']                 = "Solicitar Payout";
$lang['wallets-transfer']       = "Transferencia de billeteras";
$lang['sign-friend']            = "Nuevo líder";

$lang['news']                   = "Noticias";
$lang['signs']                  = "Señales";
$lang['bitwexchange-news']      = "Noticias Bitwexchange";
$lang['order']                  = "Pedido";

$lang['founders']               = "Fundadores";
$lang['pdf-fullness']           = "Catálogo";
$lang['card-pay']               = "Pago";    



//wallets
$lang['dollar']         = "USD";
$lang['bitcoin']         = "BTC";
$lang['monero']         = "XMR";
$lang['ethereum']       = "ETH";
$lang['litecoin']       = "LTC";
$lang['trumpcoin']      = "TRUMP";
$lang['dash']           = "DASH";
$lang['solarcoin']      = "SLR";

//link
$lang['link']           = "Link de invitación";
$lang['link_shop']      = "Link tienda";

$lang['request_payouts'] = 'Solicitudes de Payout';