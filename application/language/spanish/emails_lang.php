<?php defined('BASEPATH') OR exit('No direct script access allowed');

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of emails_lang
 *
 * @author Juan Manuel
 */


/*
 * Templates titles
 */

$lang['btc_deposit_admin'] = "Un nuevo deposito BTC ha sido solicitado";
$lang['btc_deposit_user'] = "Deposito BTC solicitado";
$lang['btc_deposit_user_done'] = "Deposito BTC procesado";

$lang['coin_deposit_admin'] = "Un nuevo deposito de moneda ha sido solicitado";
$lang['coin_deposit_user'] = "Deposito de moneda solicitado";
$lang['coin_deposit_user_done'] = "Deposito de moneda procesado";

$lang['btc_payout_admin'] = "Un nuevo payout ha sido solicitado";
$lang['btc_payout_user'] = "Payout solicitado";
$lang['btc_payout_user_done'] = "Payout procesado";

$lang['inscription_admin'] = "Fullness global Nuevo usuario registrado";
$lang['inscription_user'] = "Fullness global Nuevo usuario registrado";