<?php defined('BASEPATH') OR exit('No direct script access allowed');
/**
 * Admin Dashboard Language File
 */

// Text
$lang['admin dashboard text welcome']  = "¡Hola y bienvenidos al tablero!";

// Buttons
$lang['admin dashboard btn demo']      = "Haga clic para Jsi18n demo";

// Jsi18n Demo
$lang['admin dashboard jsi18n-sample'] = "Esta es una demostración de la biblioteca jsi18n. Se necesita texto de un archivo de idioma y lo inserta en su Javascripts. Ver la biblioteca jsi18n.php y dashboard_i18n.js para su uso.";

// Dashboard
$lang['d_users']                   = "Usuarios Activos";
$lang['wallet_addresses']          = "Billetera";
$lang['total_btc']                 = "Total BTC en el sitio";
$lang['bitcoin_price']             = "Precio Bitcoin";
$lang['affiliate_Link']            = "link de referencia";
$lang['revenue']                   = "Total Ingresos";
$lang['your_profit']               = "Ganancias";
$lang['recent_users']              = "Usuarios recientes";
$lang['market_price']              = "Precio de mercado";
$lang['daily_sale_plans']          = "Venta diaria de planes";
$lang['latest_transactions']       = "Ultimas Transacciones";
$lang['pending_payments']          = "Pagos pendientes";
$lang['payments_done']             = "Pagos realizados";

//transactions table
$lang['username']          = "Nombre de Usuario";
$lang['t_user']            = "Usuario";
$lang['email']            = "E-mail";
$lang['transaction']       = "Transacción";
$lang['sender']            = "Remitente";
$lang['Recipient']         = "Recipiente";
$lang['amount']            = "Cantidad";
$lang['time']              = "Fecha";
$lang['status']            = "Estado";

//gauge
$lang['begins']            = "Empieza";
$lang['ends']              = "Termina";
$lang['days_remaining']    = "Días restantes";
$lang['days']              = "Días";

$lang['transactions']    =  "Transacciones";
$lang['daily-bonus']    =  "Bonus diario";
$lang['date']          =  "Fecha";
$lang['code']          =  "Codigo";
$lang['description']   =  "Descripción";
$lang['value_btc']     =  "Valor en BTC";
$lang['rate']          =  "Tarifa";
$lang['value_usd']     =  "Valor en S/";
$lang['status']        =  "Estado";
$lang['no-data']       =  "No hay resultados para el gráfico aún";
$lang['welcome']      = "Bienvenido a i9life";

//date
$lang['date_year'] = 'Año';
$lang['date_years'] = 'Años';
$lang['date_month'] = 'Mes';
$lang['date_months'] = 'Meses';
$lang['date_week'] = 'Semana';
$lang['date_weeks'] = 'Semanas';
$lang['date_day'] = 'Día';
$lang['date_days'] = 'Días';
$lang['date_hour'] = 'Hora';
$lang['date_hours'] = 'Horas';
$lang['date_minute'] = 'Minuto';
$lang['date_minutes'] = 'Minutos';
$lang['date_second'] = 'Segundo';
$lang['date_seconds'] = 'Segundos';

$lang['click_here_to_download'] = "Libro Blanco";
$lang['file_to_download']           = "Archivo a descargar";
$lang['countdown_end_of_month'] = 'Dias para la "liquidación mensual"';
$lang['label_binary_amount'] = 'Bono binario';
$lang['label_unilevel_amount'] = 'Bono uninivel';
$lang['label_commission'] = 'Comisión';
