<?php defined('BASEPATH') OR exit('No direct script access allowed');


$lang['withdraw_office']  = "Retiro en oficina principal";
$lang['label_wholesaler']  = "Mayorista";
$lang['label_office']  = "Oficina";
$lang['label_banking']  = "Bancario";
$lang['label_shipping_address']  = "Dirección de envío";
$lang['question_confirm_purchase']  = "¿Estás seguro(a) de realizar la compra?";
$lang['no_shipping_selected']  = "No se ha seleccionado un metodo de envío";
$lang['message']  = "Mensaje";
$lang['order_registered']  = "Orden registrada";
$lang['order_number']  = "Número de orden";
$lang['product_list']  = "Listado de productos";
$lang['product_name']  = "Nombre";
$lang['product_qty']  = "Cantidad";
$lang['product_price']  = "Precio";
$lang['subtotal']  = "Subtotal";
$lang['name_product'] = "Nombre";
$lang['quantity_product'] = "Cantidad";
$lang['price_product']   = "Precio";
$lang['total_product']  = "Total";
$lang['shipping_label']  = "Envío";
$lang['return_label']  = "Volver";
$lang['pay_office_label']  = "Pago en oficina principal";
$lang['pay_wholesaler_label']  = "Pago en Mayorista";
$lang['pay_online_label']  = "Depósitos y transferencias";
$lang['payment_code']  = "Código de pago";
$lang['pay_creditcard_label']  = "Pago Visanet";
$lang['pay_registered_label']  = "Pago registrado";

