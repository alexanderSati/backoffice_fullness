<?php defined('BASEPATH') OR exit('No direct script access allowed');
/**
 * Transactions Language File
 */


$lang['transfer-money']  = "Transferir dinero";
$lang['transfer-to']     = "Transferir a";
$lang['search']          = "Buscar";
$lang['clear']           = "Limpiar";
$lang['amount']          = "Cantidad";
$lang['token']           = "Token";
$lang['request-token']   = "Solicitar token";
$lang['transfer']        = "Transferir";
$lang['new-deposit']     = "Nuevo deposito";
$lang['button-transfer'] = "Generar depósito de Bitcoin";
$lang['request-payout']  = "Realizar un pago";
$lang['pay']             = "Comprar";
$lang['select-wallet']   = "Seleccionar Billetera";
$lang['origen-wallet']   = "Billetera de origen";
$lang['coin-tobuy']      = "Moneda a comprar";
$lang['coin-to-sell']    = "Moneda a vender";
$lang['dollar-price']    = "Precio en usd";
$lang['select-destination-wallet']   = "Selecionar billetera de destino";
$lang['amount-trading']   = "Vas a comprar <span class='value-currency'>XX</span> <span class='code-currency'>XMR</span> coins";
$lang['make-trading']    = "Hacer Trading";

// Sell and Buy orders
$lang['balance']        = "Balance";
$lang['price']          = "Precio";
$lang['total']          = "Total";
$lang['fee']            = "Couta (1%)";
$lang['total_fee']      = "Total + Cuota";
$lang['sell']           = "Vender";
$lang['buy_trading']    = "Comprar";
$lang['trade_history']  = "Historial de intercambio";
$lang['sell_order']     = "Ordenes en venta";
$lang['buy_order']      = "Ordenes a comprar";

//wallets
$lang['dollar']         = "USD";
$lang['bitcoin']         = "BTC";
$lang['monero']         = "XMR";
$lang['ethereum']       = "ETH";
$lang['litecoin']       = "LTC";
$lang['trumpcoin']      = "TRUMP";
$lang['dash']           = "DASH";
$lang['solarcoin']      = "SLR";
$lang['bonus_referral'] = "Bono de primera compra";
$lang['bonus_activation'] = "Bono de activación";
$lang['bonus_infinity']  = "Residual al infinito";
$lang['bonus_fullness']  = "Bono FullnessGlobal";
$lang['bonus_upgrade']  = "Bono de Upgrade";
$lang['wallet_payment']  = "Pago con billetera";
$lang['label_payout']  = "Payout";
$lang['label_bonus_award'] = "Bono de premiación";
$lang['label_bonus_external_sales'] = "Bono de e-commerce";    







