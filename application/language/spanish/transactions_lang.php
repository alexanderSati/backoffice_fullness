<?php defined('BASEPATH') OR exit('No direct script access allowed');
/**
 * Transactions Language File
 */


$lang['transfer-money']  = "Transferir dinero";
$lang['transfer-to']     = "Transferir a";
$lang['search']          = "Buscar";
$lang['clear']           = "Limpiar";
$lang['amount']          = "Cantidad";
$lang['token']           = "Token";
$lang['security-pin'] = "PIN de seguridad";
$lang['request-token']   = "Solicitar token";
$lang['transfer']        = "Transferir";
$lang['new-deposit']     = "Nuevo deposito";
$lang['new-btc-trasnfer']= "Nueva transferencia BTC";
$lang['button-transfer'] = "Generar solicitud de depósito";
$lang['button-coin-transfer'] = "Generar solicitud de depósito";
$lang['request-payout']  = "Solicitar Payout";
$lang['request-payout']  = "Solicitar un pago";
$lang['select-wallet']   = "Seleccionar Billetera";
$lang['qr']              = "Agregar codigo QR";
$lang['qr_label']        = "El codigo QR debe ser de la billetera <span id='label-qr'>BTC</span>";
$lang['hash_label']      = "Por favor, suministre el hash de deposito BTC.";
$lang['invalid_hash']    = "BTC hash invalido";

/*
 * Tokens library
 */
$lang['toke_email_subject'] = "Tu codigo de seguridad";
$lang['no-found-username']  = "No se encontro el Usuario";
$lang['send-token']         = "El token se ha enviado a tu correo electrónico";
$lang['show-token']         = "Por favor usa el siguiente token %s";
$lang['token-error']        = "El token ingresado es invalido, por favor solicita otro token";
$lang['second_password']    = "El PIN ingresado es invalido";
$lang['transfer-success']   = "La transferencia se ha realizado con éxito";
$lang['deposit-success']    = "El deposito se ha realizado con éxito";
$lang['payout-success']     = "El pago se ha solicitado con éxito, proximamente un ejecutivo de cuenta lo procesara.";
$lang['invalid-amount']     = "Transferencia de cantidad no válida";
$lang['invalid-amount-payout']= "La cantidad ingresada no cumple con los valores permitidos, la cantidad minima son 10 soles.";
$lang['invalid-wallet']     = "Billetera de transferencia no válida";
$lang['invalid-user']       = "Usuario inválido para transferir";
$lang['payout-limit']       = "Ha excedido el número máximo de pagos";
$lang['payout-limit-title'] = "Opcion no disponible!";
$lang['payout-limit-text']  = "Lo lamentamos, ha alcanzado el numero maximo de solicitudes permitidas (%d cada %d %s), podra crear una nueva solicitud despues del :";
$lang['enough-funds']       = "Fondos insuficientes para la transferencia";
$lang['invalid-transfer']   = "Error al crear la transferencia";
$lang['invalid-deposit']    = "Error al solicitar el deposito";
$lang['invalid-payout']     = "Error al solicitar el pago";

/*
 * Wallets Transactions
 */
$lang['invalid_btc_transfer'] = "Trasferencias desde la billetera BTC no son permitidas, transfiera fondos desde su billetera operativa";
$lang['inactive_binary'] = 'Cada inversor debe activar su binario para poder realizar transferencias y solicitar pagos';

/*
 * Deposit Transactions
 */
$lang['cancel_deposit']     = "Cancelar";
$lang['deposit_payed']      = "Pagado";
$lang['qr_code']            = "Código QR";
$lang['deposit_message']    = "Despues de realizar su inversion, por favor haga click en el boton <code>PAGADO</code>, para actualizar el estado, de lo contrario este no sera procesado por un agente ejecutivo";

/*
 * Transfers U2U
 */
$lang['amount_transfer_fee'] = "Comision de transferencia&nbsp;";
$lang['fee_tooltip'] = "Este valor sera descontado de la cantidad ingresada.";

$lang['origin-coin'] = "Moneda a depositar";
$lang['final_coin'] = "Billetera destino";
$lang['amount_coin_tooltip'] = "Por favor, llene este campo con la cantidad a ser depositada";
$lang['activation_bonus'] = 'Bonus de activación por el usuario ';
