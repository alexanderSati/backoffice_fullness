<?php defined('BASEPATH') OR exit('No direct script access allowed');



$lang['shopping_cart']                = "Carro de compras";
$lang['clear_shopping_cart']          = "Vaciar carro";
$lang['add_shopping_cart']          = "Añadir al carro";
$lang['price_shopping_cart']          = "Precio";
$lang['close_cart_list']          = "Cerrar";
$lang['buy_cart_list']          = "Comprar";
$lang['remove_cart_list']          = "Quitar";
$lang['cart_empty']          = "El carro está vacío";
$lang['cart_name']          = "Nombre";
$lang['cart_quantity']          = "Cantidad";
$lang['cart_action']          = "Acción";
$lang['cart_confirm_buy']    = "¿Estás seguro de hacer la compra?";
$lang['cancel_buy']    = "Cancelar";
$lang['cart_buy']    = "Comprar";
$lang['buy_successfull']    = "Compra satisfactoria";
$lang['insufficient_funds'] = "Fondos insuficientes";
$lang['cart_details']    = "Detalles";
$lang['cart_date']    = "Fecha";
$lang['cart_discount']    = "Descuento";
$lang['cart_user']    = "Usuario";
$lang['cart_items']    = "Items";
$lang['cart_state']    = "Estado";
$lang['cart_state_approved']    = "Aprobado";
$lang['cart_state_pending']    = "Pendiente";
$lang['cart_state_denied']    = "Negado";
$lang['product_category']    = "Categoría";
$lang['cart_residual']       = "Residual";
$lang['cart_binary']       = "Binario";
$lang['cart_return']    = "Regresar";
$lang['cart_of']       = "de";     
$lang['external_buy_successfull']    = "Compra exitosa";
$lang['subtotal_price_shopping_cart']          = "Subtotal";
$lang['total_zero']          = "Debe tener por lo menos 1 producto seleccionado";
$lang['approve_state']    = "Aprobar";
$lang['order_approved']    = "Orden aprobada";
$lang['product_ubication']    = "Ubicación";
$lang['order_number']    = "Número de orden";


$lang['denied_state']    = "Denegar";
$lang['order_denied']    = "Orden denegada";
$lang['purchases'] = 'Compras';

$lang['name_product'] = "Nombre";
$lang['quantity_product'] = "Cantidad";
$lang['price_product']   = "Precio";
$lang['total_product']  = "Total";
$lang['no_plan_price']    = "No ha alcanzado el precio del plan";
$lang['first_plan_purchased'] = "Debes esperar a que se apruebe tu orden de compra";
$lang['change_shipping']  = "Volver";
$lang['no_products_message']  = "No hay productos para mostrar";
$lang['residual_points']  = "Puntos residuales";
$lang['binary_points']  = "Puntos binarios";
$lang['message_add']  = "Producto agregado";
$lang['ask_cancel_upgrade']  = "Cancelar upgrade";
$lang['no_quantity_in_stock']  = "No hay suficiente stock";
$lang['message_no_product_available'] = "Solo hay {1} {2} en stock";
$lang['lbl_tienda'] = "Tienda";
$lang['lbl_shipping_address'] = "Dirección de envío";
$lang['msg_unique_wholesaler'] = "Sólo puede adquirir productos de un mismo vendedor";
$lang['lbl_pay_wallet'] = "Pago con billetera";
$lang['lbl_pay_wholesaler'] = "Pago en mayorista";
$lang['lbl_pay_oficina'] = "Pago en oficina";
$lang['lbl_pay_online'] = "Pago online, código";
$lang['lbl_wholesaler'] = "Mayorista";
$lang['lbl_payment_approved'] = "Pago aprobado";
$lang['pay_creditcard_label']  = "Pago Visanet";








