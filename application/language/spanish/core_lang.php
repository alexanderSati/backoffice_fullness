<?php defined('BASEPATH') OR exit('No direct script access allowed');
/**
 * Core Language File
 */

// Buttons
$lang['core button admin']                  = "Admin";
$lang['core button cancel']              	= "Cancelar";
$lang['core button close']               	= "Cerrar";
$lang['core button contact']               	= "Contacto";
$lang['core button filter']              	= "Continuar";
$lang['core button home']                	= "Home";
$lang['core button login']               	= "Iniciar sesión";
$lang['core button logout']              	= "Cerrar sesión";
$lang['core button profile']              	= "Perfil";
$lang['core button reset']               	= "Reiniciar";
$lang['core button register']               	= "Registrar";
$lang['core button save']                	= "Guardar";
$lang['core button search']              	= "Buscar";
$lang['core button toggle_nav']          	= "Toggle navigation";
$lang['core button link-left']               = "Haz click en el campo para copiar el link de la linea izquierda";
$lang['core button link-right']              = "Haz click en el campo para copiar el link de la linea derecha";
$lang['core button link-shop']               = "Haz click en el campo para copiar el link de venta";

// Text
$lang['core text no']                    	= "No";
$lang['core text yes']                   	= "Sí";
$lang['core text page_rendered']            = "Página generada en <strong>{elapsed_time}</strong> segundos";

// Emails
$lang['core email start']                	= "<!DOCTYPE html><html><head><style>
                                                    body { background: #fff; color: #333; }
                                               </style></head><body>";
$lang['core email end']                  	= "</body></html>";

// Additional date_lang
$lang['UM75']	                         	= "(UTC -7:00) Arizona Time";

// Errors
$lang['core error no_results']              = "¡No se han encontrado resultados!";
$lang['core error session_language']        = "¡Hubo un problema al establecer el idioma!";

$lang['years']      = "Años";
$lang['months']     = "Meses";
$lang['days']       = "Dias";
$lang['hours']      = "Horas";
$lang['minutes']    = "Minutos";
$lang['seconds']    = "Segundos";

$lang['access_denied'] = "Acceso denegado. Usted no tiene suficientes permisos";

$lang['captcha error'] = "¡La verificación no es correcta!";

$lang['verify-spam'] = "* Se encuentra disponible en su perfil.";
$lang['pin_tooltip'] = "Se encuentra disponible en su perfil";
$lang['information'] = "* Sólo 4 carateres";
$lang['end_of_the_month_message'] = "Acción no disponible en el momento, intente el 1ro de octubre";