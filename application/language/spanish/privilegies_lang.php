<?php defined('BASEPATH') OR exit('No direct script access allowed');
/**
 * Settings Language File
 */

// Titles
$lang['privilegies-title'] = "Editar Privilegios";
$lang['read']              = "Ver";
$lang['insert']            = "Insertar";
$lang['update']            = "Actualizar";
$lang['delete']            = "Eliminar";
$lang['module']            = "Modulo";
$lang['section']           = "Sección";
$lang['actions']           = "Acciones";

// Messages
$lang['msg save_success']  = "Permisos se han modificado correctamente.";

// Errors
$lang['error save_failed'] = "Hubo un problema al guardar los permisos. Por favor, vuelva a intentarlo.";
