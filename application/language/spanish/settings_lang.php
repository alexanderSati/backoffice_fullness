<?php defined('BASEPATH') OR exit('No direct script access allowed');
/**
 * Settings Language File
 */

// Titles
$lang['admin settings title']             = "Ajustes";

// Messages
$lang['admin settings msg save_success']  = "Configuración se ha guardado correctamente.";

// Errors
$lang['admin settings error save_failed'] = "Hubo un problema al guardar la configuración. Por favor, vuelva a intentarlo.";

//Settings
$lang['site_name']                 = "Nombre del sitio";
$lang['help_site_name']            = "";

$lang['per_page_limit']            = "Items Por Pagina";
$lang['help_per_page_limit']       = "";

$lang['meta_keywords']             = "Meta Keywords";
$lang['help_meta_keywords']        = "Lista de palabras clave del sitio separadas por comas";

$lang['meta_description']          = "Meta Description";
$lang['help_meta_description']     = "Breve descripción describiendo su sitio.";

$lang['site_email']                = "Email";
$lang['help_site_email']           = "Email donde serán enviados los correos electrónicos";

$lang['site_alerts_email']                = "Email de alertas";
$lang['help_site_alerts_email']           = "Email donde serán enviados los correos electrónicos con alertas";

$lang['timezones']                 = "Zona horaria";
$lang['help_timezones']            = "";

$lang['privilegies_message']       = "Alerta de privilegios";
$lang['help_privilegies_message']  = "";

$lang['allow_new_users']           = "Permitir nuevos usuarios";
$lang['enabled']                   = "Habilitado";
$lang['disabled']                  = "Deshabilitado";
$lang['help_allow_new_users']      = "";
$lang['welcome_message']           = "Mensaje de bienvenida *";

$lang['wallet_1']                 = "Billetera 1";
$lang['help_wallet_1']            = "";

$lang['wallet_2']                 = "Billetera 2";
$lang['help_wallet_2']            = "";

//coin wallets
$lang['wallet_ltc']               = "Billetera Litecoin";
$lang['help_wallet_ltc']          = "";
$lang['wallet_eth']               = "Billetera Ethereum";
$lang['help_wallet_eth']          = "";
$lang['wallet_xmr']               = "Billetera Monero";
$lang['help_wallet_xmr']          = "";
$lang['wallet_dash']              = "Billetera Dash";
$lang['help_wallet_dash']         = "";

/*
 * SMTP Labels
 */
$lang['smtp_host']                  = "SMTP Host";
$lang['smtp_port']                  = "SMTP Puerto";
$lang['smtp_user']                  = "SMTP Usuario";
$lang['smtp_password']              = "SMTP Contraseña";

/*
 * Dashboard news
 */
$lang['dashboards_news']            = "Noticias del tablero";
$lang['help_dashboards_news']       = "Ingrese el slug de la categoría a la cual pertenecen las noticias a mostrar";

$lang['sign']                       = "Señales";
$lang['help_sign']                  = "Ingrese el slug de la categoría a la cual pertenece el post de señales a mostrar";

$lang['file_to_download']           = "Archivo a descargar";
$lang['help_file_to_download']      = "Ingrese el enlace del archivo a descargar";