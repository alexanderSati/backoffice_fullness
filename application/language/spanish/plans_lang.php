<?php defined('BASEPATH') OR exit('No direct script access allowed');
/**
 * Admin Language File
 */

// Titles
$lang['admin plans title']                = "Lista de planes";
$lang['plans title plans_add']            = "Agregar Plan";
$lang['plans title plans_edit']            = "Editar Plan";
// Table Columns
$lang['plans col base']                 = "Base";
$lang['plans col period_days']                   = "Periodo de dias";
$lang['plans col spanish_name']                  = "Nombre";
$lang['plans col id']                    = "ID";


// Form Inputs
$lang['plans input email']                    = "Correo";
$lang['plans input first_name']               = "Nombre";
$lang['plans input is_admin']                 = "Es administrador";
$lang['plans input language']                 = "Idioma";
$lang['plans input plan']                     = "Plan";
$lang['plans input profile']                  = "Perfil";
$lang['plans input last_name']                = "Apellido";
$lang['plans input password']                 = "Contraseña";
$lang['plans input password_repeat']          = "Repetir contraseña";
$lang['plans input status']                   = "Estado";
$lang['plans input username']                 = "Nombre de usuario";
$lang['plans input username_email']           = "Nombre de usuario o Correo";
$lang['plans input check']                    = "Seleccionar";
$lang['plans input base']                     = "Base";
$lang['plans input period_days']              = "Período";
$lang['plans input period_days_before']       = "Período Anterior";
$lang['plans input min_profit']       		  = "Beneficio Mínimo";
$lang['plans input max_profit']       		  = "Máximo beneficio";
$lang['plans input spanish_name']       	  = "Nombre";
$lang['plans input english_name']       	  = "Nombre";
$lang['plans input spanish_features']         = "Características";
$lang['plans input english_features']         = "Características";
$lang['plans input excluded_days']            = "Días excluidos";

// Help
$lang['plans help passwords']                 = "Solo escribe la contraseña si deseas cambiarla.";

// Messages
$lang['plans msg add_user_success']           = "El plan fue añadido exitosamente!";
$lang['plans msg delete_confirm']             = "¿Seguro que quieres eliminar <strong>%s</ strong>? Esto no se puede deshacer.";
$lang['plans msg delete_user']                = "El plan ha sido eliminado correctamente";
$lang['plans msg edit_profile_success']       = "Su perfil se modificó con éxito!";
$lang['plans msg edit_user_success']          = "El plan fue modificado exitosamente!!";
$lang['plans msg register_success']           = "Gracias por registrarse, %s! Verifica tu correo por un mensaje de confirmación. Una vez que su cuenta ha sido verificada, usted será capaz de iniciar sesión con las credenciales proporcionadas.";
$lang['plans msg password_reset_success']     = "Tu contraseña ha sido restablecida, %s! Por favor revise su correo electrónico para su nueva contraseña temporal.";
$lang['plans msg validate_success']           = "Tu cuenta ha sido verificada. Ahora puede acceder a su cuenta.";
$lang['plans msg email_new_account']          = "<p>Gracias por crear una cuenta en %s. Haga clic en el enlace de abajo para validar su dirección de correo electrónico y activar su cuenta.<br /><br /><a href=\"%s\">%s</a></p>";
$lang['plans msg email_new_account_title']    = "Nueva Cuenta de %s";
$lang['plans msg email_password_reset']       = "<p>Su contraseña en %s ha sido restablecida. Haga clic en el enlace de abajo para iniciar sesión con su nueva contraseña:<br /><br /><strong>%s</strong><br /><br /><a href=\"%s\">%s</a>
                                                 Una vez iniciada la sesión, asegúrese de cambiar la contraseña a algo que pueda recordar.</p>";
$lang['plans msg email_password_reset_title'] = "Restablecer contraseña para %s";

// Errors
$lang['plans error add_user_failed']          = "El plan no pudo ser añadido!";
$lang['plans error delete_user']              = "El plan no pudo ser eliminado!";
$lang['plans error edit_profile_failed']      = "Su perfil no puede ser modificado!";
$lang['plans error edit_user_failed']         = "El plan no pudo ser modificado!";
$lang['plans error email_exists']             = "El correo electrónico <strong>%s</strong> ya existe!";
$lang['plans error email_not_exists']         = "Ese correo electrónico no existe!";
$lang['plans error invalid_login']            = "Usuario o contraseña invalido";
$lang['plans error password_reset_failed']    = "Hubo un problema al restablecer su contraseña. Por favor, vuelva a intentarlo.";
$lang['plans error register_failed']          = "Tu cuenta no pudo crearce en este momento. Por favor, vuelva a intentarlo.";
$lang['plans error user_id_required']         = "Se requiere un ID de usuario numérico!";
$lang['plans error user_not_exist']           = "Ese plan no existe!";
$lang['plans error username_exists']          = "El usuario <strong>%s</strong> ya existe!";
$lang['plans error validate_failed']          = "Hubo un problema al validar su cuenta. Por favor, vuelva a intentarlo.";
$lang['plans error too_many_login_attempts']  = "Usted ha hecho demasiados intentos para iniciar sesión con demasiada rapidez. Por favor, espere %s segundos y vuelva a intentarlo.";

