<?php defined('BASEPATH') OR exit('No direct script access allowed');
/**
 * Settings Language File
 */

// Titles
$lang['privilegies-title'] = "Edit Privilegies";
$lang['read']              = "Read";
$lang['insert']            = "Insert";
$lang['update']            = "Update";
$lang['delete']            = "Delete";
$lang['module']            = "Module";
$lang['section']           = "Section";
$lang['actions']           = "Actions";

// Messages
$lang['msg save_success']  = "Privilegies have been successfully saved.";

// Errors
$lang['error save_failed'] = "There was a problem saving privilegies. Please try again.";
