<?php defined('BASEPATH') OR exit('No direct script access allowed');



$lang['shopping_cart']                = "Shopping Cart";
$lang['clear_shopping_cart']          = "Clear Cart";
$lang['add_shopping_cart']          = "Add to cart";
$lang['price_shopping_cart']          = "Price";
$lang['close_cart_list']          = "Close";
$lang['buy_cart_list']          = "Buy now";
$lang['remove_cart_list']          = "Remove";
$lang['cart_empty']          = "Cart is empty";
$lang['cart_name']          = "Name";
$lang['cart_quantity']          = "Quantity";
$lang['cart_action']          = "Action";
$lang['cart_confirm_buy']    = "Are you sure you make the purchase?";
$lang['cancel_buy']    = "Cancel";
$lang['cart_buy']    = "Buy";
$lang['buy_successfull']    = "Buy successfull";
$lang['insufficient_funds'] = "Insufficient funds";
$lang['cart_details']    = "Details";
$lang['cart_date']    = "Date";
$lang['cart_discount']    = "Discount";
$lang['cart_user']    = "User";
$lang['cart_items']    = "Items";
$lang['cart_state']    = "State";
$lang['cart_state_approved']    = "Approved";
$lang['cart_state_pending']    = "Pending";
$lang['cart_state_denied']    = "Denied";
$lang['product_category']    = "Category";
$lang['cart_residual']       = "Residual";
$lang['cart_binary']       = "Binary";
$lang['cart_return']    = "Go Back";
$lang['cart_of']       = "of";     
$lang['external_buy_successfull']    = "Buy successfull";
$lang['subtotal_price_shopping_cart']          = "Subtotal";
$lang['total_zero']          = "You must have at least 1 product selected";
$lang['approve_state']    = "Approve";
$lang['order_approved']    = "Order approved";
$lang['product_ubication']    = "Ubication";
$lang['order_number']    = "Order number";


$lang['denied_state']    = "Denied";
$lang['order_denied']    = "Order Denied";
$lang['no_plan_price']    = "You have not reached the plan price";
$lang['first_plan_purchased'] = "You must wait for your purchase order to be approved";
$lang['change_shipping']  = "Return";
$lang['no_products_message']  = "There are no products to display";
$lang['residual_points']  = "Residual points";
$lang['binary_points']  = "Binary points";
$lang['message_add']  = "Product added";
$lang['ask_cancel_upgrade']  = "Do you want to cancel upgrade";
$lang['ask_cancel_upgrade']  = "Cancelar upgrade";
$lang['no_quantity_in_stock']  = "Not enough stock";
$lang['message_no_product_available'] = "There is only {1} {2} in stock";
$lang['lbl_tienda'] = "Shop";
$lang['lbl_shipping_address'] = "Shipping address";
$lang['msg_unique_wholesaler'] = "You can only buy products from one seller";
$lang['lbl_pay_wallet'] = "Wallet payment";
$lang['lbl_pay_wholesaler'] = "Wholesaler payment";
$lang['lbl_pay_oficina'] = "Office payment";
$lang['lbl_pay_online'] = "Online payment, code";
$lang['lbl_wholesaler'] = "Wholesaler";
$lang['lbl_payment_approved'] = "Payment approved";
$lang['pay_creditcard_label']  = "Visanet Payment";



