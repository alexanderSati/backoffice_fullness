<?php defined('BASEPATH') OR exit('No direct script access allowed');


$lang['withdraw_office']  = "Withdraw in office";
$lang['label_wholesaler']  = "Wholesaler";
$lang['label_office']  = "Office";
$lang['label_banking']  = "Banking";
$lang['label_shipping_address']  = "Shipping address";
$lang['question_confirm_purchase']  = "Are you sure to make the purchase?";
$lang['no_shipping_selected']  = "No shipping method selected";
$lang['message']  = "Message";
$lang['order_registered']  = "Order registered";
$lang['order_number']  = "Order number";
$lang['product_list']  = "Product list";
$lang['product_name']  = "Name";
$lang['product_qty']  = "Quantity";
$lang['product_price']  = "Price";
$lang['subtotal']  = "Subtotal";
$lang['name_product'] = "Name";
$lang['quantity_product'] = "Quantity";
$lang['price_product']   = "Price";
$lang['total_product']  = "Total";
$lang['shipping_label']  = "Shipping";
$lang['return_label']  = "Return";
$lang['pay_office_label']  = "Office Payment";
$lang['pay_wholesaler_label']  = "Wholesale Payment";
$lang['pay_online_label']  = "Online Payment <b class='text-warning' >before long</b>";
$lang['payment_code']  = "Payment code";
$lang['pay_creditcard_label']  = "Visanet Payment";
$lang['pay_registered_label']  = "Pay registered";

