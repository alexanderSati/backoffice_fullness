<?php defined('BASEPATH') OR exit('No direct script access allowed');
/**
 * Admin Language File
 */

// Titles
$lang['admin plans title']                = "Plans List";
$lang['plans title plans_add']            = "Add Plan";
$lang['plans title plans_edit']            = "Edit Plan";

// Table Columns
$lang['plans col base']                 = "Base";
$lang['plans col period_days']                   = "Period days";
$lang['plans col spanish_name']                  = "Name";
$lang['plans col id']                    = "ID";


// Form Inputs
$lang['plans input email']                    = "Email";
$lang['plans input first_name']               = "First Name";
$lang['plans input is_admin']                 = "Is Admin";
$lang['plans input language']                 = "Language";
$lang['plans input plan']                     = "Plan";
$lang['plans input profile']                  = "Profile";
$lang['plans input last_name']                = "Last Name";
$lang['plans input password']                 = "Password";
$lang['plans input password_repeat']          = "Repeat Password";
$lang['plans input status']                   = "Status";
$lang['plans input username']                 = "Username";
$lang['plans input username_email']           = "Username or Email";
$lang['plans input check']                    = "Select";
$lang['plans input base']                     = "Base";
$lang['plans input period_days']              = "Period Days";
$lang['plans input period_days_before']       = "Period Days Before";
$lang['plans input min_profit']       		  = "Minimum Profit";
$lang['plans input max_profit']       		  = "Maximum Profit";
$lang['plans input spanish_name']       	  = "Name";
$lang['plans input english_name']       	  = "Name";
$lang['plans input spanish_features']         = "features";
$lang['plans input english_features']         = "features";
$lang['plans input excluded_days']            = "Excluded Days";

// Help
$lang['plans help passwords']                 = "Only enter passwords if you want to change it.";

// Messages
$lang['plans msg add_user_success']           = "The plan was successfully added!";
$lang['plans msg delete_confirm']             = "Are you sure you want to delete <strong>%s</strong>? This can not be undone.";
$lang['plans msg delete_user']                = "You have succesfully deleted the plan";
$lang['plans msg edit_profile_success']       = "Your profile was successfully modified!";
$lang['plans msg edit_user_success']          = "The plan successfully modified!";
$lang['plans msg register_success']           = "Thanks for registering, %s! Check your email for a confirmation message. Once
                                                 your account has been verified, you will be able to log in with the credentials
                                                 you provided.";
$lang['plans msg password_reset_success']     = "Your password has been reset, %s! Please check your email for your new temporary password.";
$lang['plans msg validate_success']           = "Your account has been verified. You may now log in to your account.";
$lang['plans msg email_new_account']          = "<p>Thank you for creating an account at %s. Click the link below to validate your
                                                 email address and activate your account.<br /><br /><a href=\"%s\">%s</a></p>";
$lang['plans msg email_new_account_title']    = "New Account for %s";
$lang['plans msg email_password_reset']       = "<p>Your password at %s has been reset. Click the link below to log in with your
                                                 new password:<br /><br /><strong>%s</strong><br /><br /><a href=\"%s\">%s</a>
                                                 Once logged in, be sure to change your password to something you can
                                                 remember.</p>";
$lang['plans msg email_password_reset_title'] = "Password Reset for %s";

// Errors
$lang['plans error add_user_failed']          = "The plan could not be added!";
$lang['plans error delete_user']              = "The plan could not be deleted!";
$lang['plans error edit_profile_failed']      = "Your profile could not be modified!";
$lang['plans error edit_user_failed']         = "The plan could not be modified!";
$lang['plans error email_exists']             = "The email <strong>%s</strong> already exists!";
$lang['plans error email_not_exists']         = "That email does not exists!";
$lang['plans error invalid_login']            = "Invalid username or password";
$lang['plans error password_reset_failed']    = "There was a problem resetting your password. Please try again.";
$lang['plans error register_failed']          = "Your account could not be created at this time. Please try again.";
$lang['plans error user_id_required']         = "A numeric user ID is required!";
$lang['plans error user_not_exist']           = "That plan does not exist!";
$lang['plans error username_exists']          = "The username <strong>%s</strong> already exists!";
$lang['plans error validate_failed']          = "There was a problem validating your account. Please try again.";
$lang['plans error too_many_login_attempts']  = "You've made too many attempts to log in too quickly. Please wait %s seconds and try again.";
