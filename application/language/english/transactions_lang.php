<?php defined('BASEPATH') OR exit('No direct script access allowed');
/**
 * Transactions Language File
 */


$lang['transfer-money']  = "Transfer money";
$lang['transfer-to']     = "Transfer to";
$lang['search']          = "Search";
$lang['clear']           = "Clear";
$lang['amount']          = "Amount";
$lang['token']           = "Token";
$lang['request-token']   = "Request token";
$lang['security-pin']    = "Security PIN";
$lang['transfer']        = "Transfer";
$lang['new-deposit']     = "New deposit";
$lang['new-btc-trasnfer']= "New BTC wallet transfer";
$lang['button-transfer'] = "Request a Bitcoin Deposit";
$lang['button-coin-transfer'] = "Request a Coin Deposit";
$lang['request-payout']  = "Payout Request";
$lang['pay']             = "Pay";
$lang['select-wallet']   = "Select wallet";
$lang['qr']              = "Add your wallet QR";
$lang['qr_label']        = "The QR code must be from the <span id='label-qr'>BTC</span> wallet";
$lang['hash_label']      = "Please, fill your BTC deposit hash.";
$lang['invalid_hash']    = "Invalid BTC hash";

/*
 * Tokens library
 */
$lang['toke_email_subject'] = "Your security code";
$lang['no-found-username']  = "Username not found";
$lang['send-token']         = "The token was sent to your email";
$lang['show-token']         = "Please use this token %s";
$lang['token-error']        = "The token entered is invalid, please request another token";
$lang['second_password']    = "Security PIN is invalid";
$lang['transfer-success']   = "The transfer has been successfully processed";
$lang['deposit-success']    = "The deposit has been successfully processed";
$lang['payout-success']     = "The payout has been successfully requested";
$lang['invalid-amount']     = "Invalid amount transfer";
$lang['invalid-amount-payout']= "The value doesn't meet the transfer amount allowed, the minimum quantity is 10 soles";
$lang['invalid-wallet']     = "Invalid wallet to transfer";
$lang['invalid-user']       = "Invalid user to transfer";
$lang['payout-limit']       = "You have exceeded the maximum number of payouts";
$lang['payout-limit-title'] = "Payout not available!";
$lang['payout-limit-text']  = "We sorry, you have reached the number of allowed payouts (%d each %d %s), you will be able to request a new one at :";
$lang['enough-funds']       = "Insufficient funds for transfer";
$lang['invalid-transfer']   = "Error creating transfer";
$lang['invalid-deposit']    = "Error requesting a deposit";
$lang['invalid-payout']     = "Error requesting a payout";

/*
 * Wallets Transactions
 */
$lang['invalid_btc_transfer'] = "Transferences from BTC wallet are not allowed, try to transfer funds to your BTC operative wallet first";
$lang['inactive_binary'] = 'Each investor needs to activate its binary in order to be able to make transfers and to request payouts';

/*
 * Deposit Transactions
 */
$lang['cancel_deposit']     = "Cancel";
$lang['deposit_payed']      = "Paid";
$lang['qr_code']            = "QR Code";
$lang['deposit_message']    = "After processing your investment, please click in <code>PAID</code> button, to update it status, otherwise this transaction won't be processed by an executive agent";

/*
 * Transfers U2U
 */
$lang['amount_transfer_fee'] = "Transfer fee&nbsp;";
$lang['fee_tooltip'] = "This fee will be discounted from the amount.";

$lang['origin-coin'] = "Amount to deposit";
$lang['final_coin'] = "Funds source";
$lang['amount_coin_tooltip'] = "Please, fill it with the amount to deposit";

$lang['activation_bonus'] = 'User activation bonus ';