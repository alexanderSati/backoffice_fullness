<?php defined('BASEPATH') OR exit('No direct script access allowed');
/**
 * Admin Language File
 */

// Titles
$lang['admin faqs title']                = "Founding Users";
$lang['faqs title faq_edit']             = "Edit user";
$lang['admin faqs add']                  = "Add user";

// Table Columns
$lang['faqs col english_question']      = "User";
$lang['faqs col english_answer']        = "Description";
$lang['faqs col status']                = "Status";
$lang['faqs col id']                    = "ID";
$lang['faqs tooltip add']               = "Add new user";


// Form Inputs
$lang['faqs input english_question']         = "User";
$lang['faqs input english_answer']           = "Description";
$lang['faqs input status']                   = "Status";
$lang['faqs input check']                    = "Select";

// Help
$lang['faqs help passwords']                 = "Only enter passwords if you want to change it.";

// Messages
$lang['faqs msg add_faq_success']           = "%s was successfully added!";
$lang['faqs msg delete_confirm']             = "This can not be undone.";
$lang['faqs msg delete_faq']                = "You have succesfully deleted <strong>%s</strong>!";
$lang['faqs msg edit_profile_success']       = "Your profile was successfully modified!";
$lang['faqs msg edit_faq_success']          = "%s was successfully modified!";
$lang['faqs msg register_success']           = "Thanks for registering, %s! Check your email for a confirmation message. Once
                                                 your account has been verified, you will be able to log in with the credentials
                                                 you provided.";
$lang['faqs msg password_reset_success']     = "Your password has been reset, %s! Please check your email for your new temporary password.";
$lang['faqs msg validate_success']           = "Your account has been verified. You may now log in to your account.";
$lang['faqs msg email_new_account']          = "<p>Thank you for creating an account at %s. Click the link below to validate your
                                                 email address and activate your account.<br /><br /><a href=\"%s\">%s</a></p>";
$lang['faqs msg email_new_account_title']    = "New Account for %s";
$lang['faqs msg email_password_reset']       = "<p>Your password at %s has been reset. Click the link below to log in with your
                                                 new password:<br /><br /><strong>%s</strong><br /><br /><a href=\"%s\">%s</a>
                                                 Once logged in, be sure to change your password to something you can
                                                 remember.</p>";
$lang['faqs msg email_password_reset_title'] = "Password Reset for %s";

$lang['faqs msg user_delete']                = "Are you sure you want to delete this faq?";
$lang['faqs msg required-input']             = "you must add the question for the three languages ​​Spanish, Portuguese and English";

// Errors
$lang['faqs error add_faq_failed']          = "%s could not be added!";
$lang['faqs error delete_faq']              = "<strong>%s</strong> could not be deleted!";
$lang['faqs error edit_profile_failed']      = "Your profile could not be modified!";
$lang['faqs error edit_faq_failed']         = "%s could not be modified!";
$lang['faqs error invalid_login']            = "Invalid faqname or password";
$lang['faqs error register_failed']          = "Your account could not be created at this time. Please try again.";
$lang['faqs error faq_id_required']         = "A numeric faq ID is required!";
$lang['faqs error faq_not_exist']           = "That faq does not exist!";
$lang['faqs error faqname_exists']          = "The faqname <strong>%s</strong> already exists!";
$lang['faqs error validate_failed']          = "There was a problem validating your account. Please try again.";
$lang['faqs error too_many_login_attempts']  = "You've made too many attempts to log in too quickly. Please wait %s seconds and try again.";
