<?php defined('BASEPATH') OR exit('No direct script access allowed');

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of emails_lang
 *
 * @author Juan Manuel
 */


/*
 * Templates titles
 */

$lang['btc_deposit_admin'] = "A new BTC deposit is pending for approval";
$lang['btc_deposit_user'] = "BTC Deposit sucessfull requested";
$lang['btc_deposit_user_done'] = "BTC deposit processed";

$lang['coin_deposit_admin'] = "A new Coin deposit is pending for approval";
$lang['coin_deposit_user'] = "Coin Deposit sucessfull requested";
$lang['coin_deposit_user_done'] = "Coin deposit processed";

$lang['btc_payout_admin'] = "A new payout is pending for approval";
$lang['btc_payout_user'] = "Payout sucessfull requested";
$lang['btc_payout_user_done'] = "Payout processed";

$lang['inscription_admin'] = "Fullness global User Registration";
$lang['inscription_user'] = "Fullness global User Registration";