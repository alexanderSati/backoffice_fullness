<?php defined('BASEPATH') OR exit('No direct script access allowed');
/**
 * Transactions Language File
 */

$lang['amount']         = "Amount";
$lang['dollar-price']   = "Dollar price";
$lang['coin-tobuy']     = "Coin to buy";
$lang['coin-to-sell']   = "Coin to sell";
$lang['make-trading']   = "Make Trading";
$lang['actions']        = "Actions";

// Sell and Buy orders
$lang['time']           = "Time";
$lang['balance']        = "Balance";
$lang['price']          = "Price";
$lang['total']          = "Total";
$lang['fee']            = "Fee";
$lang['total_fee']      = "Total + Fee";
$lang['total_fee_less'] = "Total - Fee";
$lang['buy']            = "Buy";
$lang['sell']           = "Sell";
$lang['buy_trading']    = "Buy";
$lang['trade_history']  = "Trade History";
$lang['sell_order']     = "Sell Order";
$lang['buy_order']      = "Buy Order";
$lang['cancel']         = "Cancel";

/*
 *New module errors
 */

$lang['tradig_invalid_action'] = "Invalid trading action.";
$lang['tradig_invalid_coin'] = "Invalid trading coin.";
$lang['tradig_invalid_amount'] = "Entered amount doesn't meet the requirements.";
$lang['tradig_invalid_price'] = "Entered BTC price doesn't meet the requirements.";

$lang['tradig_not_funds_wallet'] = "Not enough funds in your wallet.";
$lang['tradig_not_funds_btc_wallet'] = "Not enough funds in your operative wallet.";
$lang['tradig_not_published'] = "Your order could not be published.";
$lang['tradig_published'] = "Your order was published successfully.";
$lang['tradig_processed'] = "Your order was successfully processed.";
$lang['tradig_canceled'] = "Your order was successfully canceled and refunded.";

$lang['tradig_not_found'] = "Trading not found.";
$lang['tradig_not_available'] = "Sorry, Trading is not available.";
$lang['tradig_access_denied'] = "Access denied, Invalid user or status.";

$lang['form_fee_text'] = "All tradings fee (0.8%) are processed in bitcoins.";


//Admin
$lang['trading_history']= "Trading History";
$lang['buy_user']       = "Buy User";
$lang['sell_user']      = "Sell User";
$lang['date']           = "Date";
$lang['type']           = "Type";
$lang['coin']           = "Coin";
$lang['total_usd']      = "Total USD";
$lang['total_btc']      = "Total BTC";
$lang['profit']         = "Profit";
$lang['status']         = "Status";
$lang['total_profit']   = "Trading total profit";
$lang['n_transactions']   = "Transactions";