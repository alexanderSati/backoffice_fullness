<?php defined('BASEPATH') OR exit('No direct script access allowed');
/**
 * Core Language File
 */

// Buttons
$lang['core button admin']                  = "Admin";
$lang['core button cancel']              	= "Cancel";
$lang['core button close']               	= "Close";
$lang['core button contact']               	= "Contact";
$lang['core button filter']              	= "Search";
$lang['core button home']                	= "Home";
$lang['core button login']               	= "Login";
$lang['core button logout']              	= "Logout";
$lang['core button profile']              	= "Profile";
$lang['core button reset']               	= "Reset";
$lang['core button register']               	= "Register";
$lang['core button save']                	= "Save";
$lang['core button search']              	= "Search";
$lang['core button toggle_nav']          	= "Toggle navigation";
$lang['core button link-left']               = "Click on the field to copy the link left line";
$lang['core button link-right']              = "Click on the field to copy the link right line";
$lang['core button link-shop']               = "Click on the field to copy the sales link";

// Text
$lang['core text no']                    	= "No";
$lang['core text yes']                   	= "Yes";
$lang['core text page_rendered']            = "Page rendered in <strong>{elapsed_time}</strong> seconds";

// Emails
$lang['core email start']                	= "<!DOCTYPE html><html><head><style>
                                                    body { background: #fff; color: #333; }
                                               </style></head><body>";
$lang['core email end']                  	= "</body></html>";

// Additional date_lang
$lang['UM75']	                         	= "(UTC -7:00) Arizona Time";

// Errors
$lang['core error no_results']              = "No results found!";
$lang['core error session_language']        = "There was a problem setting the language!";

$lang['years']      = "Years";
$lang['months']     = "Months";
$lang['days']       = "Days";
$lang['hours']      = "Hours";
$lang['minutes']    = "Minutes";
$lang['seconds']    = "Seconds";


$lang['access_denied'] = "Access denied. You don't have enough privileges to perform this action";

$lang['captcha error'] = "Verification captcha is incorrect!";

$lang['verify-spam'] = "* It is available on your profile.";
$lang['pin_tooltip'] = "It is available on your profile";
$lang['information'] = "* Only 4 characters";
$lang['end_of_the_month_message'] = "Action not available at the moment, try  1 hour later. (End of the month)";