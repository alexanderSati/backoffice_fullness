<?php defined('BASEPATH') OR exit('No direct script access allowed');
/**
 * Users Language File
 */

// Titles
$lang['users title forgot']                   = "Forgot Password";
$lang['users title login']                    = "Login";
$lang['users title profile']                  = "Profile";
$lang['users title register']                 = "Register";
$lang['users title user_add']                 = "Add User";
$lang['users title customer_add']             = "Add Customer";
$lang['users title user_delete']              = "Confirm Delete User";
$lang['users title user_edit']                = "Edit User";
$lang['users title user_list']                = "User List";
$lang['users title user_stats']               = "Users stats";
$lang['users title account']                  = "My account";
$lang['users placeholder created']            = "dd-mm-yyyy";
$lang['users title transactions']             = "Transactions";
$lang['users title transfer']                 = "Transfer";
$lang['users title deposit']                  = "New deposit";
$lang['users title payout']                   = "Payout";
$lang['users title wallets']                  = "Wallet";
$lang['users title binary']                   = "Binary Network";
$lang['users title unassigned']               = "Unassigned users";
$lang['users title wallets transfer']         = "Wallets transfer";
$lang['users title btc-wallets transfer']     = "BTC Wallets transfer";
$lang['users title register qr_to_pay']       = "Scan QR code to process your deposit";
$lang['users title renew-subscription']       = "Renew subscription";
$lang['users title link']                     = "Links";
$lang['users title categories_list']           = "Categories List";
$lang['users title category_edit']            = "Edit category";
$lang['users title category_add']             = "Add Category";

$lang['users title product_list']             = "Product list";
$lang['users title product_add']              = "Add product";
$lang['users title new_site']                 = "New to site?";
$lang['users title change_language']          = "Change language";

$lang['users title product_edit']             = "Edit product";  
$lang['users title total_referrals']          = "Referrals";
$lang['users title saldo']                    = "Platform balance";
$lang['users title binary_points']            = "Binary points";
$lang['users title plan_active']              = "This is your active plan";
$lang['users title binary_points']            = "Residual points";
$lang['users title residual_points']          = "Residual points";
$lang['users title left-points']              = "Left points";
$lang['users title right-points']             = "Right points"; 
$lang['users title send_to']                  = "Send to";
$lang['users title order']                    = "Order";  


$lang['users title shipping_details']         = "Pick up";
$lang['users title add_shipping_details']     = "Add pick up";
$lang['users title edit_shipping_details']    = "Edit pick up";


$lang['users title products_quantities']      = "Products stock";
$lang['users title add_products_quantities']  = "Add product stock";
$lang['users title edit_products_quantities'] = "Edit product stock";
$lang['users title awards']                   = "Awards";

$lang['users title list_administrators']      = "Administrators list";  
$lang['users title edit_admin']               = "Edit administrator";
$lang['users title admin_add']                = "Add administrator";

$lang['users title cart_list']                = "Cart list";  
$lang['users title next_prize']               = "Next prize";
$lang['users title point_missing']            = "Missing points";
$lang['users title progress_bar']             = "Progress bar";  
$lang['users title select_option']            = "Choose an option"; 
$lang['users title preferred_consumer']       = "Preferred Consumer";
$lang['users title points_in_residual']       = "residual points";
$lang['users title points_in_binary']         = "binary points";
$lang['users title shipping_method']          = "Shipping method";
$lang['users title payment_method']           = "Payment method";
$lang['users title available']                = "Available"; 
$lang['users title confirm_purchase']         = "Confirm Purchase";  

$lang['users title total_points_binary']      = "Total binary settlement points";               
$lang['users title maximum_plan']             = "Plan upgrade not available";   

$lang['users title banking_pay']              = "Banking pay";
$lang['users title executive_add']            = "Add executive"; 

$lang['users title executive_edit']           = "Edit Executive";

$lang['users title external_store']           = "Store";
$lang['users title personal_informacion']     = "Personal information"; 
$lang['users title unilevel']                 = "Unilevel";     
$lang['users title month-wallet']             = "Filter by month";
$lang['users title line-max']                 = "Maximum line";
$lang['users title bonus']                    = "Bonus";
$lang['users title grupal-point']             = "Group points";
$lang['users titel unilevel-rank']            = "Rank";  



// Buttons
$lang['users button add_new_user']            = "Add New User";
$lang['users button register']                = "Create Account";
$lang['users button reset_password']          = "Reset Password";
$lang['users button login_try_again']         = "Try Again";
$lang['users button add_new_category']        = "Add New Category";
$lang['users button add_new_product']         = "Add New Product";
$lang['users button cancel']                  = "Cancel";
$lang['users button confirm']                 = "Confirm";
$lang['users button add_shipping_details']    = "Add pick up";
$lang['users button add_product_quantities']  = "Add product stock";
$lang['users button accept']                  = "Accept";
$lang['users button add_new_executive']       = "Add New Executive";



// Tooltips
$lang['users tooltip add_new_user']           = "Create a brand new user.";
$lang['users tooltip add_new_category']       = "Create a brand new category.";
$lang['users tooltip add_new_product']        = "Create a brand new product.";
$lang['users tooltip add_new_shipping']       = "Create a pick up.";
$lang['users tooltip add_new_product_quantities']  = "Create a product stock.";
$lang['users tooltip add_new_executive']      = "Create a new executive ";  


// Links
$lang['users link forgot_password']           = "Forgot your password?";
$lang['users link register_account']          = "Register for an account.";

// Table Columns
$lang['users col fullname']                   = "Full Name";
$lang['users col first_name']                 = "First Name";
$lang['users col is_admin']                   = "Admin";
$lang['users col last_name']                  = "Last Name";
$lang['users col user_id']                    = "ID";
$lang['users col username']                   = "Username";
$lang['users col created']                    = "Sign Up date";
$lang['users col wallets']                    = "Wallets (USD)";
$lang['users col BTC']                        = "BTC";
$lang['users col XMR']                        = "XMR";
$lang['users col ETH']                        = "ETH";
$lang['users col LTC']                        = "LTC";
$lang['users col TRUMP']                      = "TRUMP";
$lang['users col DASH']                       = "DASH";
$lang['users col SLR']                        = "SLR";
$lang['users col USD']                        = "BTC operative";
$lang['users col total']                      = "Total USD in wallets";
$lang['users col profit']                     = "Profit";
$lang['users col payout']                     = "Payout";
$lang['users col categories_id']              = "ID";
$lang['users col name']                       = "Name";
$lang['users col description']                = "Description";
$lang['users col product_name']               = "Product name";  
$lang['users col product_price']              = "Price client";
$lang['users col product_price_distributor']  = "Price distributor"; 
$lang['users col code']                       = "Code"; 
$lang['users col name_details']               = "Pick up";
$lang['users col quantities']                 = "Quantities"; 
$lang['users col amount']                     = "Amount";




// Form Inputs
$lang['users input email']                    = "Email";
$lang['users input first_name']               = "First Name";
$lang['users input is_admin']                 = "Is Admin";
$lang['users input language']                 = "Language";
$lang['users input id_user']                  = "User id";
$lang['users input amount']                   = "Amount";
$lang['users input token']                    = "Token";
$lang['users input user']                     = "User";
$lang['users input wallet']                   = "Wallet";
$lang['users input qr']                       = "Code QR";
$lang['users input country']                  = "Country";
$lang['users input plan']                     = "Plan";
$lang['users input profile']                  = "Profile";
$lang['profile-admin']                        = "Administrator";
$lang['profile-executive']                    = "Account executive";
$lang['profile-user']                         = "General User";
$lang['users input last_name']                = "Last Name";
$lang['users input password']                 = "Password";
$lang['users input password_repeat']          = "Repeat password";
$lang['users input security_pin']             = "Security PIN";
$lang['users input security_pin_repeat']      = "Repeat security PIN";
$lang['users input status']                   = "Status";
$lang['users input side']                     = "My Network:";
$lang['users input side-left']                = "Line of business left";
$lang['users input side-right']               = "Line of business right";
$lang['users input remember']                 = "Remember, this option is irreversible";
$lang['users input username']                 = "Username";
$lang['users input username_email']           = "Username or Email";
$lang['users input check']                    = "Select";
$lang['users input image']                    = "Select Image";
$lang['users input terms']                    = "Accept the <a href='https://bitwexchange.com/terms-and-conditions/' target='blank'>terms and conditions</a>";
$lang['users input dni']                      = "DNI";
$lang['users input birthdate']                = "Birthdate";
$lang['users input phone']                    = "Phone";
$lang['users input address']                  = "Address";
$lang['users input address2']                 = "Address 2";
$lang['users input city']                     = "City";
$lang['users input department']               = "Department";
$lang['users input postal_code']              = "Postal Code";
$lang['users input bank_account']             = "Description of a bank account";

$lang['users input name']                       = "Name";
$lang['users input description']                = "Description";

$lang['users input product_name']             = "Product name";  
$lang['users input product_price']            = "Price client";
$lang['users input category_id']              = "Category";
$lang['users input product_price_distributor'] = "Price distributor"; 
$lang['users input code']                     = "Code";
$lang['users input residual_points']          = "Residual points";
$lang['users input binary_points']            = "Binary";
$lang['users input stock_quantity']           = "Stock";
$lang['users input shipping_types_id']        = "Shipping type";
$lang['users input shiping_details_id']       = "pick up"; 
$lang['users input quantity']                 = "Quantity";
$lang['users input shiping_details']          = "Shipping Office";                   

// Help
$lang['users help passwords']                 = "Only enter passwords if you want to change it.";

// Messages
$lang['users msg add_user_success']           = "%s was successfully added!";
$lang['users msg delete_confirm']             = "Are you sure you want to delete <strong>%s</strong>? This can not be undone.";
$lang['users msg delete_user']                = "You have succesfully deleted <strong>%s</strong>!";
$lang['users msg edit_profile_success']       = "Your profile was successfully modified!";
$lang['users msg edit_user_success']          = "%s was successfully modified!";
$lang['users msg register_success']           = "Thanks for registering, %s! Check your email for a confirmation message. Once
                                                 your account has been verified, you will be able to log in with the credentials
                                                 you provided.";
$lang['users msg password_reset_success']     = "Your password has been reset, %s! Please check your email for your new temporary password.";
$lang['users msg validate_success']           = "Your account has been verified. You may now log in to your account.";
$lang['users msg email_new_account']          = "<p>
    <img width=\"260\" src=\"https://app.bitwexchange.com/themes/gentella/images/logo.png\"
         alt=\"Bitwexchange\">
</p><p>Thank you for creating an account at %s. Click the link below to validate your
                                                 email address and activate your account.<br /><br /><a href=\"%s\">%s</a></p>";
$lang['users msg email_new_account_title']    = "New Account for %s";
$lang['users msg email_password_reset']       = "<p>
    <img width=\"260\" src=\"https://app.bitwexchange.com/themes/gentella/images/logo.png\"
         alt=\"Bitwexchange\">
</p><p>Your password at %s has been reset. Click the link below to log in with your
                                                 new password:<br /><br /><strong>%s</strong><br /><br /><a href=\"%s\">%s</a>
                                                 Once logged in, be sure to change your password to something you can
                                                 remember.</p>";
$lang['users msg email_password_reset_title'] = "Password Reset for %s";
$lang['users link copy']                      = "Link copied to the clipboard successfully ";

$lang['users msg edit_category_success']      = "The category %s was successfully modified!";
$lang['users msg add_category_success']       = "The category %s was successfully added!";

$lang['users msg edit_product_success']       = "The product %s was successfully modified!";
$lang['users msg add_product_success']        = "The product %s was successfully added!";

$lang['users msg remove_product']             = "your product <strong>%s</strong> was removed satisfactorily";
$lang['users msg activate_product']           = "your product <strong>%s</strong> was successfully activated";

$lang['users msg remove-sure']                = "Are you sure of remove this product?";
$lang['users msg activate-sure']              = "Are you sure of activate this product?";

$lang['users msg transfer']                   = "Are you sure of transfer the money?";
 
$lang['users msg add_shipping_detail']        = "The waypoint %s was successfully added!";
$lang['users msg edit_shipping_detail']       = "The waypoint %s was successfully modified!";

$lang['users msg add_product_quantity']       = "The product stock was successfully added!";
$lang['users msg edit_product_quantity']      = "The product stock was successfully modified!";

$lang['users msg deny-sure']                  = "Are you sure to deny this payout?";
$lang['users msg aprrove-sure']               = "Are you sure you approve this payout?"; 
$lang['users msg confirm_select_option']      = "¿Are you sure to choose %s?"; 
$lang['users msg confirm_preferred']          = "¿Are you sure to select this option?";

$lang['users msg confirm_clear_cart']          = "¿Are you sure you empty the cart?";
$lang['users msg confirm_remove_inventory']    = "¿Are you sure you delete this item?"; 
$lang['users msg correct_clear']               = "Shopping cart cleaned!";
$lang['users msg correct_remove_inventory']    = "Item deleted!"; 
$lang['users msg maximum_plan']                = "You currently have the latest plan and do not have an upgrade available.";

// Errors
$lang['users error add_user_failed']          = "%s could not be added!";
$lang['users error delete_user']              = "<strong>%s</strong> could not be deleted!";
$lang['users error edit_profile_failed']      = "Your profile could not be modified!";
$lang['users error edit_user_failed']         = "%s could not be modified!";
$lang['users error email_exists']             = "The email <strong>%s</strong> already exists!";
$lang['users error email_not_exists']         = "That email does not exists!";
$lang['users error username_not_exists']      = "That username does not exists!";
$lang['users error invalid_login']            = "Invalid username or password";
$lang['users error password_reset_failed']    = "There was a problem resetting your password. Please try again.";
$lang['users error register_failed']          = "Your account could not be created at this time. Please try again.";
$lang['users error user_id_required']         = "A numeric user ID is required!";
$lang['users error user_not_exist']           = "That user does not exist!";
$lang['users error username_exists']          = "The username <strong>%s</strong> already exists!";
$lang['users error validate_failed']          = "There was a problem validating your account. Please try again.";
$lang['users error validate_referral_failed'] = "There was a problem validating your referral user. Please try again!";
$lang['users error too_many_login_attempts']  = "You've made too many attempts to log in too quickly. Please wait %s seconds and try again.";
$lang['users error edit_category_failed']     = "The category %s could not be modified!";
$lang['users error add_category_failed']      = "The category %s could not be added!";

$lang['users error edit_product_failed']      = "The product %s could not be modified!";
$lang['users error add_product_failed']       = "The product %s could not be added!";

$lang['users error remove_product']           = "The product %s could not be removed!";
$lang['users error activate_product']         = "The product %s could not be activated!";
$lang['users error product_not_exist']        = "The product does not exist!";
$lang['users error product_id_required']      = "A numeric product ID is required!";

$lang['users error add_shipping_detail_failed'] = "The pick up %s could not be added!";
$lang['users error edit_shipping_detail_failed'] = "The pick up %s could not be modified!";

$lang['users error add_product_quantity']     = "The product stock could not be added!";
$lang['users error edit_product_quantity']    = "The product stock could not be modified!";

$lang['users error number_negative']          = "The quantity cannot be negative or 0";
$lang['users error payout_by_active']         = "You must activate your account to payout";  
$lang['users error missing_purchases']        = "You must make a purchase greater than 220 soles to payout";   
$lang['users error days_passed']              = "You can only payout from 1 to 8 of each month";  

$lang['users error category_exists']          = "A category with this name already exists";
$lang['users error product_exists']           = "A product with this name already exists";
$lang['users error shipping_exists']          = "A pick up point with this name already exist";
$lang['users error is_preferencial_user']     = "You are a preferred consumer, preferred consumers cannot add users";
$lang['users error is_not_active_add']        = "You must activate your account with a plan to perform this action"; 
$lang['users error is_not_active_transfer']   = "You must activate your account with a plan to make a transfer"; 

$lang['users error is_preferencial_user_register'] = "The user you are trying to refer to is a preferred consumer, you can only refer to users with a plan";
$lang['users error plan_not_select']  = "You must select a plan";
$lang['users error exist_shiping_details_id'] = "The product is already linked to that pick up";


//account
$lang['my-referrals']    =  "My referrals";
$lang['account-plan']    =  "Account Plan";
$lang['account-rank']    =  "Account Rank";
$lang['current-plan']    =  "Current Plan";
$lang['current-rank']    =  "Current Rank";
$lang['plan-list']       =  "Plan list:";
$lang['rank-list']       =  "Rank list:";
$lang['details']         =  "Details";
$lang['volume']          =  "Volume";
$lang['period_days']     =  "Plan days";
$lang['days_remaining']  =  "Days remaining";
$lang['daily']           =  "Daily";
$lang['profit']          =  "Profit";
$lang['points']          =  "points";
$lang['residual']        =  "Residual";   

$lang['enough-funds']       = "Insufficient funds for transfer";
$lang['user-already']       = "User is already a child";
$lang['invalid-side']       = "Invalid side";
$lang['side-busy']          = "Side is not available (busy)";
$lang['parent-not']         = "Parent tree doesn't exist";
$lang['error update side']  = "There was an error updating user status";
$lang['success update side']= "User status successfully updated";

// Notifications

$lang['users info waiting approval'] = "Your account has been created successfully.";
$lang['no-data-table'] = "No data found";

//binary
$lang['side']               = "Business line";
$lang['left-points']        = "Score Business line left";
$lang['right-points']       = "Score Business line right";
$lang['total-points']       = "Total Points";

//token
$lang['token']              = "Token";
$lang['request-token']      = "Request token";
$lang['send-token']         = "The token was sent to your email";
$lang['show-token']         = "Please use this token %s";
$lang['token-error']        = "The token entered is invalid, please request another token";

$lang['access_denied_expired'] = "Sorry, your investment has come to end, check your account details or contact an account executive.";
$lang['renew']              = "Click here to renew it";
$lang['renewal-success']    = "Your account has been successfully renewed";
$lang['renewal-pending']    = "Your account will be renewed once your payment is verified.";
$lang['error required_renewal'] = "You need renew your account.";
$lang['select-coin']        = "Select coin to deposit";

$lang['upgrade']            = "Upgrade";
$lang['upgrade_title']      = "Upgrade plan";
$lang['upgrade_button']     = "Click here to upgrade status";
$lang['upgrade_success']    = "Congratuations, your account has been successfully upgraded";

$lang['active_since']       = "Active since";
$lang['days_left']          = "Remaining days";
$lang['modal_title']       = "Confirmation";
$lang['question_confirm']       = "are you sure upgrade your plan?";


$lang['plan_current']       = "Selected plan is the same as your current plan";
$lang['plan_donwgrade']     = "You cannot downgrade your current plan.";
$lang['invalid_subscription'] = "No subscriptions were found.";
$lang['invalid-wallet']     = "Invalid wallet.";
$lang['invalid-plan']       = "Selected plan is invalid.";
$lang['payment_fail']       = "Payment could not be processed.";
$lang['not-enough-funds']   = "Not enough funds in your operative wallet.";

$lang['back_to_home']       = "Back to home";
$lang['your_plan']          = "Your plan";

/*
 * Awards
 */

$lang['cruise']                 = "CRUISE";
$lang['suitcase']               = "1/2 MILLION SUITCASE";
$lang['house_million']          = "1 MILLION HOUSE";
$lang['rank']                   = "Ranks";


$lang['monthly_suspended'] = "Your account is currently inactive";
$lang['monthly_pending'] = "Your account has monthly fee payments pending. <strong><a href='/user/transactions/deposit'>(Pay it here)</a></strong>";
$lang['first_login'] = "Welcome, the next step is to acquire your personal franchise <a href='/Cart' style='color: white'>Click here</a>";
$lang["no_active_user_refer"] = "You must register as an active partner to refer users";
$lang["lbl_commision"] = "Commission";
$lang["lbl_discount"] = "Discount";
$lang["lbl_status"] = "Status";
$lang['form_validation_exist']		= 'The value of {field} field already registered.';
$lang['payment_method']  = "Payment method";