<?php defined('BASEPATH') OR exit('No direct script access allowed');
/**
 * Transactions Language File
 */


$lang['transfer-money']  = "Transfer money";
$lang['transfer-to']     = "Transfer to";
$lang['search']          = "Search";
$lang['clear']           = "Clear";
$lang['amount']          = "Amount";
$lang['token']           = "Token";
$lang['request-token']   = "Request token";
$lang['transfer']        = "Transfer";
$lang['new-deposit']     = "New deposit";
$lang['button-transfer'] = "Request Bitcoin Deposit";
$lang['request-payout']  = "Request a payment";
$lang['pay']             = "Pay";
$lang['select-wallet']   = "Select wallet";
$lang['origen-wallet']   = "Source wallet ";
$lang['coin-tobuy']      = "Coin to buy";
$lang['coin-to-sell']    = "Coin to sell";
$lang['dollar-price']    = "Dollar price";
$lang['select-destination-wallet']   = "Select destination wallet ";
$lang['amount-trading']   = "you are going to buy <span class='value-currency'>XX</span> <span class='code-currency'>XMR</span> coins";
$lang['make-trading']   = "make Trading";

// Sell and Buy orders
$lang['balance']        = "Balance";
$lang['price']          = "Price";
$lang['total']          = "Total";
$lang['fee']            = "Fee (1%)";
$lang['total_fee']      = "Total + Fee";
$lang['sell']           = "Sell";
$lang['buy_trading']    = " Buy";
$lang['trade_history']  = "Trade History";
$lang['sell_order']     = "Sell Order";
$lang['buy_order']      = "Buy Order";

//wallets
$lang['dollar']         = "USD";
$lang['bitcoin']         = "BTC";
$lang['monero']         = "XMR";
$lang['ethereum']       = "ETH";
$lang['litecoin']       = "LTC";
$lang['trumpcoin']      = "TRUMP";
$lang['dash']           = "DASH";
$lang['solarcoin']      = "SLR";
$lang['bonus_referral'] = "First purchase bonus";
$lang['bonus_activation'] = "Activation bonus";
$lang['bonus_infinity']  = "Residual to infinity";
$lang['bonus_fullness']  = "FullnessGlobal Bonus";
$lang['bonus_upgrade']  = "Upgrade Bonus";
$lang['wallet_payment']  = "Wallet payment";
$lang['label_payout']  = "Payout";
$lang['label_bonus_award'] = "Award bonus";
$lang['label_bonus_external_sales'] = "E-commerce bonus";    


