<?php defined('BASEPATH') OR exit('No direct script access allowed');
/**
 * Settings Language File
 */

// Titles
$lang['admin settings title']             = "Settings";

// Messages
$lang['admin settings msg save_success']  = "Settings have been successfully saved.";

// Errors
$lang['admin settings error save_failed'] = "There was a problem saving settings. Please try again.";

//Settings
$lang['site_name']                 = "Site Name";
$lang['help_site_name']            = "";

$lang['per_page_limit']            = "Items Per Page";
$lang['help_per_page_limit']       = "";

$lang['meta_keywords']             = "Meta Keywords";
$lang['help_meta_keywords']        = "Comma-seperated list of site keywords";

$lang['meta_description']          = "Meta Description";
$lang['help_meta_description']     = "Short description describing your site.";

$lang['site_email']                = "Site Email";
$lang['help_site_email']           = "Email address all emails will be sent from.";

$lang['site_alerts_email']                = "Alerts Email";
$lang['help_site_alerts_email']           = "Email address to send all alerts notifications.";

$lang['timezones']                 = "Timezone";
$lang['help_timezones']            = "";

$lang['privilegies_message']       = "Privileges Alert";
$lang['help_privilegies_message']  = "";

$lang['allow_new_users']           = "Allow new users";
$lang['enabled']                   = "Enabled";
$lang['disabled']                  = "Disabled";
$lang['help_allow_new_users']      = "";
$lang['welcome_message']           = "Welcome message *";

$lang['wallet_1']                 = "Wallet 1";
$lang['help_wallet_1']            = "";

$lang['wallet_2']                 = "Wallet 2";
$lang['help_wallet_2']            = "";

//coin wallets
$lang['wallet_ltc']               = "Wallet Litecoin";
$lang['help_wallet_ltc']          = "";
$lang['wallet_eth']               = "Wallet Ethereum";
$lang['help_wallet_eth']          = "";
$lang['wallet_xmr']               = "Wallet Monero";
$lang['help_wallet_xmr']          = "";
$lang['wallet_dash']              = "Wallet Dash";
$lang['help_wallet_dash']         = "";

/*
 * SMTP Labels
 */
$lang['smtp_host']                  = "SMTP Host";
$lang['smtp_port']                  = "SMTP Port";
$lang['smtp_user']                  = "SMTP User";
$lang['smtp_password']              = "SMTP Password";

/*
 * Dashboard news
 */
$lang['dashboards_news']            = "Dashboard news";
$lang['help_dashboards_news']       = "Dashboard news";

$lang['sign']                       = "Sign";
$lang['help_sign']                  = "Sign";

$lang['file_to_download']           = "File to download";
$lang['help_file_to_download']      = "File to download";