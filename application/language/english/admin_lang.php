<?php defined('BASEPATH') OR exit('No direct script access allowed');
/**
 * Admin Language File
 */

// Titles
$lang['admin title admin']                = "Administration";
$lang['admin title pending transactions'] = "Pending Transactions";
$lang['admin title transactions']         = "All Transacciones";
$lang['admin title payout_request']       = "List of payouts";  

// Buttons
$lang['admin button csv_export']          = "CSV Export";
$lang['admin button dashboard']           = "Dashboard";
$lang['admin button delete']              = "Delete";
$lang['admin button edit']                = "Edit";
$lang['admin button messages']            = "Messages";
$lang['admin button settings']            = "Settings";
$lang['admin button users']               = "Users";
$lang['admin button users_add']           = "Add New User";
$lang['admin button users_list']          = "List of Users";
$lang['admin button admin_list']          = "List of Administrators";
$lang['admin button activate']            = "Activate";
$lang['admin button remove']              = "Remove";

// Tooltips
$lang['admin tooltip csv_export']         = "Export a CSV file of all results with filters applied.";
$lang['admin tooltip filter']             = "Update results based on your filters.";
$lang['admin tooltip filter_reset']       = "Clear all your filters and sorting.";

// Form Inputs
$lang['admin input active']               = "Active";
$lang['admin input inactive']             = "Inactive";
$lang['admin input items_per_page']       = "items/page";
$lang['admin input select']               = "select...";
$lang['admin input username']             = "Username";

//Week days
$lang['admin input monday']                 = "Monday";
$lang['admin input thuesday']               = "Tuesday";
$lang['admin input wednesday']              = "Wednesday";
$lang['admin input thursday']               = "Thursday";
$lang['admin input friday']                 = "Friday";
$lang['admin input saturday']               = "Saturday";
$lang['admin input sunday']                 = "Sunday";


// Table Columns
$lang['admin col actions']                = "Actions";
$lang['admin col status']                 = "Status";
$lang['admin col plan']                   = "Plan";
$lang['admin col rank']                   = "Rank";
$lang['admin col approve']                = "Approve";  
$lang['admin col denied']                 = "Denied";  


// Form Labels
$lang['admin label rows']                 = "%s row(s)";

$lang['error update']                     = "The transaction can not be updated";
$lang['success update']                   = "Transaction Updated Successfully";