<?php defined('BASEPATH') OR exit('No direct script access allowed');
/**
 * Menu Language File
 */

// Titles menu
$lang['dashboard']                = "Homepage";
$lang['users-groups']             = "Users and Groups";
$lang['plans']                    = "Plans";
$lang['wallets']                  = "Wallet";
$lang['trading']                  = "Trading";
$lang['transactions']             = "Transactions";
$lang['btc_wallet_transfer']      = "Wallets Transfer";
$lang['pending-transactions']     = "Pending Transactions";
$lang['reports']                  = "Reports";
$lang['faqs']                     = "Founding Users";
$lang['settings']                 = "Settings";
$lang['logout']                   = "Logout";
$lang['new-admin']                = "New administrator";
$lang['administrators']           = "Administrators";
$lang['shopping']                 = "Shopping";
$lang['categories']               = "Categories";
$lang['products']                 = "Products";
$lang['products-user']            = "Buy";
$lang['shop']                 	  = "Store";
$lang['purchases-user']           = "Order History";
$lang['orders']           		  = "Orders";
$lang['shipping']                 = "Pick up";  
$lang['products_quantities']      = "Product stock";
$lang['my-transactions']          = "My transactions";  
$lang['executive']                = "Executives"; 
$lang['unilevel']                 = "Unilevel"; 
$lang['my-stock']                 = "My stock";  

$lang['new-user']                 = "New user";
$lang['users']                    = "Users";
$lang['Users-by-capital']         = "Users stats";

$lang['new-plan']                 = "New plan";
$lang['plans']                    = "Plans";
$lang['account']                  = "My Account";
$lang['binary_tree']              = "Binary";
$lang['binary_networks']          = "My Networks";
$lang['binary_unassigned']        = "Unassigned users";

$lang['new-faq']                  = "New";
$lang['privilegies']              = "Privilegies";

$lang['transfers']              = "Transfers";
$lang['deposit']                = "Deposit";
$lang['btc-deposit']            = "BTC Deposit";
$lang['coin-deposit']           = "Coin Deposit";
$lang['payout']                 = "Request Payout";
$lang['wallets-transfer']       = "Wallets Transfer";
$lang['sign-friend']            = "New leader";

$lang['news']                   = "News";
$lang['signs']                  = "Signs";
$lang['bitwexchange-news']      = "Bitwexchange News";
$lang['order']                  = "Request";

$lang['founders']               = "Founders";
$lang['pdf-fullness']           = "Catalogue";    
$lang['card-pay']               = "Payment";


//wallets
$lang['dollar']         = "USD";
$lang['bitcoin']         = "BTC";
$lang['monero']         = "XMR";
$lang['ethereum']       = "ETH";
$lang['litecoin']       = "LTC";
$lang['trumpcoin']      = "TRUMP";
$lang['dash']           = "DASH";
$lang['solarcoin']      = "SLR";

//link
$lang['link']           = "Invitation link";
$lang['link_shop']      = "Store Link";

$lang['request_payouts'] = 'Payout requests';