<?php defined('BASEPATH') OR exit('No direct script access allowed');
/**
 * Admin Dashboard Language File
 */

// Text
$lang['admin dashboard text welcome']  = "Hello and welcome to the dashboard!";

// Buttons
$lang['admin dashboard btn demo']      = "Click for Jsi18n Demo";

// Jsi18n Demo
$lang['admin dashboard jsi18n-sample'] = "This is a demonstration of the Jsi18n library. It takes text from a language file and inserts it into your Javascripts. See the jsi18n.php library and dashboard_i18n.js for usage.";

// Dashboard
$lang['d_users']                   = "Active Users";
$lang['wallet_addresses']          = "Wallet addresses";
$lang['total_btc']                 = "Total BTC website";
$lang['bitcoin_price']             = "Bitcoin price";
$lang['affiliate_Link']            = "Referral Link";
$lang['revenue']                   = "Total Revenue";
$lang['your_profit']               = "Your profit";
$lang['recent_users']              = "Recent Users";
$lang['daily_sale_plans']          = "Daily sale of plans";
$lang['latest_transactions']       = "Latest Transactions";
$lang['pending_payments']          = "Pending payments";
$lang['payments_done']             = "Payments done";

//transactions table
$lang['username']          = "Username";
$lang['t_user']            = "User";
$lang['email']            = "E-mail";
$lang['transaction']       = "Transaction";
$lang['sender']            = "Sender";
$lang['recipient']         = "Recipient";
$lang['amount']            = "Amount";
$lang['time']              = "Time";
$lang['status']            = "Status";

//gauge
$lang['begins']            = "Begins";
$lang['ends']              = "Ends";
$lang['days_remaining']    = "Days remaining";
$lang['days']              = "Days";

$lang['transactions']    =  "Transactions";
$lang['daily-bonus']    =  "Daily profit";
$lang['date']          =  "Date";
$lang['code']          =  "Code";
$lang['description']   =  "Description";
$lang['value_btc']     =  "Value in BTC";
$lang['rate']          =  "Rate";
$lang['value_usd']     =  "Value in S/";
$lang['status']        =  "Status";
$lang['no-data']       =  "No chart results yet";
$lang['welcome']      = "Welcome to i9life";

//date
$lang['date_year'] = 'Year';
$lang['date_years'] = 'Years';
$lang['date_month'] = 'Month';
$lang['date_months'] = 'Months';
$lang['date_week'] = 'Week';
$lang['date_weeks'] = 'Weeks';
$lang['date_day'] = 'Day';
$lang['date_days'] = 'Days';
$lang['date_hour'] = 'Hour';
$lang['date_hours'] = 'Hours';
$lang['date_minute'] = 'Minute';
$lang['date_minutes'] = 'Minutes';
$lang['date_second'] = 'Second';
$lang['date_seconds'] = 'Seconds';

$lang['click_here_to_download'] = "White Book";
$lang['file_to_download']           = "File to download";
$lang['countdown_end_of_month'] = 'Days for "monthly settlement"';
$lang['label_binary_amount'] = 'Binary Bonus';
$lang['label_unilevel_amount'] = 'Unilevel Bonus';
$lang['label_commission'] = 'Commission';