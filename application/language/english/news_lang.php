<?php defined('BASEPATH') OR exit('No direct script access allowed');
/**
 * Settings Language File
 */

// Titles
$lang['news'] = "News";

$lang['error_news'] = "News not found";
$lang['welcome']      = "Latest news";