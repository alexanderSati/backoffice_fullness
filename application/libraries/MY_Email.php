<?php

/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

class MY_Email extends CI_Email {

    	/**
	 * Constructor - Sets Email Preferences
	 *
	 * The constructor can be passed an array of config values
	 *
	 * @param	array	$config = array()
	 * @return	void
	 */
    
    
	public function __construct(array $config = array())
	{
            
            $this->ci =& get_instance();
            $this->ci->load->model('settings_model');
            $settings = $this->ci->settings_model->get_settings();
            
            $this->ci->settings = new stdClass();
            foreach ($settings as $setting)
            {
                $this->ci->config->set_item($setting['name'], $setting['value']);
            }
            

            if (!$config){
                $config = array(
                        'protocol' => "smtp",
                        'smtp_host' => "ssl://{$this->ci->config->item('smtp_host')}",
                        'smtp_port' => $this->ci->config->item('smtp_port'),
                        'smtp_user' => $this->ci->config->item('smtp_user'),
                        'smtp_pass' => $this->ci->config->item('smtp_password'),
                        'mailtype' => 'html',
                        'charset' => 'utf-8',
                        'newline' => "\r\n"
                );  
            }
            
             $this->charset = config_item('charset');
            $this->initialize($config);
            $this->_safe_mode = ( ! is_php('5.4') && ini_get('safe_mode'));

            isset(self::$func_override) OR self::$func_override = (extension_loaded('mbstring') && ini_get('mbstring.func_override'));

            log_message('info', 'Email Class Initialized');
	}
        
}

?>
