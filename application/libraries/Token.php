<?php defined('BASEPATH') OR exit('No direct script access allowed');


/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of Token
 *
 * @author Juan Manuel Pinzon
 */
class Token {
    
    const LENGHT = 8;
            
    public function __construct()
    {
            // load CI instance
            $this->ci =& get_instance();
            $this->ci->load->library('encryption');
            $this->ci->load->helper('string');
            $this->ci->load->model('users_model','users');
            $this->ci->load->model('tokens_model','tokens');
    }
    
    function create($id = 0)
    {
        if ($this->ci->session->userdata('logged_in') and ( $id == 0)) {
            $id = $this->ci->user['id'];
        }

        $token = random_string('alnum', self::LENGHT);

        $fields = array(
            'id_user' => $id,
            'token' => $token,
            'salt' => $this->ci->encryption->encrypt($token),
            'status' => 'active'
        );

        $this->ci->tokens->insert($fields);

        return $token;
    }
    
    function validate($token = null){  
        
        $user = $this->ci->session->userdata('logged_in');
        $id = $user['id'];

        if($token){
            $parameters = array(
                'id_user' => $id,
                'token' => $token,
                'status' => 'active'
                );

            $current = date("Y-m-d H:i:s");
            $result = $this->ci->tokens->get($parameters); 
                                 
            // if token exists
            if ($result){
            
                // token validation
                $salt = $this->ci->encryption->decrypt($result['salt']);
                if ($salt !== $result['token']){
                    return false;
                }
                
                // expire validation
                if ($current > $result['expire']) {
                    $this->_inactivate($result['id']);
                    return false;
                }

                // Limit token to user
                if($id !== $result['id_user']){
                    return false;
                }
                
                $this->_inactivate($result['id']);
                return true;
                
            }else{
                return false;
            }
        }
    }
    
    function send($id = null, $view = null){
        
        $data = array();
        $user = $this->ci->users->get_user($id);

        if ($user){
            
            $this->ci->lang->load('transactions',$user['language']);
            
            $data['token'] = $this->create($id);
            
            if(empty($view)){
                $view = 'token_transactions';
            }
        
            
            $html = $this->ci->load->view("emails/{$user['language']}/{$view}", $data, TRUE);

            // send email
            $this->ci->load->library('email');
            $this->ci->email->from("{$this->ci->settings->site_email}");
            $this->ci->email->to($user['email']);
            $this->ci->email->subject($this->ci->lang->line('toke_email_subject'));
            $this->ci->email->message($html);
            $this->ci->email->set_mailtype("html");
            $this->ci->email->send();
            #echo $this->email->print_debugger();
            
            return $data['token'];
        }
        
        return false;
    }

    
    // Update status of token 
    private function _inactivate($id = null){
        return $this->ci->tokens->update($id,array('status' => 'expired'));
    }         
            
            
}
