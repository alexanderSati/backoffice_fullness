<?php defined('BASEPATH') OR exit('No direct script access allowed');

class Traceability {
    var $start;
    var $end;
    var $profit;
    var $profit_by_rank;
    var $user_by_rank;
    public function __construct()
    {
        $this->ci =& get_instance();
        $this->ci->load->model('residual_model');     
        $this->ci->load->model('users_model');
        $this->ci->load->library('transaction');
        $this->ci->load->library('email');

    }

    public function insert_settlement_binary(array $data = [])
    {
        if($data['type'] == 'binary'){
            $this->ci->residual_model->insert_settlement($data);
            /*
            $binary_points = $this->ci->residual_model->get_binary_points($data['id_user']);
            $rank = $this->ci->residual_model->calculate_bonus_for_binary_points($binary_points['total_points']);
            if($rank >= 1){
                if($this->ci->transaction->bonus_award($data['id_user'], $binary_points, $rank)){
                    
                }
            }*/
        }
    }

    public function quarterly_participation_bonus()
    {
        $this->calculate_quarter();
        $result = $this->ci->residual_model->get('SUM(points) AS total_points', ['type' => 'residual'], $this->start, $this->end, null, 'id_point_traceability', 'ASC', false, false, 'point_traceability');
        $this->calculate_profit_percent($result['total_points']);
        $this->ci->residual_model->calculate_bonus_for_binary_points($points);
        $this->destribute_profit_percentages_by_rank();
        $list_users = $this->ci->residual_model->get('id_user , points', ['type' => 'residual'], $this->start, $this->end, null, 'id_point_traceability', 'ASC', false, false, 'point_traceability');
        $qualifieds = $this->qualifieds($list_users);
        $this->add_quarterly_participation_bonus($qualifieds);
    }

    private function add_quarterly_participation_bonus($qualifieds)
    {
        foreach ($qualifieds as $id_user => $rank) {
            if(is_Active($id_user)){
                $bonus = $this->profit_by_rank[$rank] / $this->user_by_rank[$rank];
                $this->ci->transaction->quarterly_participation_bonus($id_user, $bonus, $rank);
            }
        }
    }

    private function qualifieds($list_users)
    {
        $this->user_by_rank = [1=>0,2=>0,3=>0,4=>0,5=>0,6=>0];
        $preparing_qualifieds = [];
        foreach ($list_users AS $user) {
            $rank = $this->ci->residual_model->calculate_bonus_for_binary_points($user['points']);
            $preparing_qualifieds[$user['id_user']][] = ['points'=>$user['points'],'rank'=>$rank];
        }

        $qualified_rank = [];
        foreach ($preparing_qualifieds AS $id_user => $values_per_month) {
            foreach ($values_per_month AS $values) {
                $qty = 1;
                $qualified_rank[$id_user][$values['rank']] = isset($qualified_rank[$id_user][$values['rank']]) ? ++$qualified_rank[$id_user][$values['rank']] : 1;
            }
        }
        $qualified = [];
        foreach($qualified_rank AS $id_user => $pre_qualified){
            krsort($pre_qualified);
            $sum_qty = 0;
            foreach ($pre_qualified as $rank => $qty) {
                $qty += $sum_qty;
                if($qty >= 2){
                    $qualified[$id_user]=$rank;
                    ++$this->user_by_rank[$rank];
                    break;
                }else{
                    ++$sum_qty;
                }
            }
        }
        return $qualified;
    }

    private function destribute_profit_percentages_by_rank(){
        $percentage_by_rank = [
            1 => 24,
            2 => 20,
            3 => 13,
            4 => 11,
            4 => 10,
            5 => 7,
            6 => 5
        ];
        foreach($percentage_by_rank as $rank => $percent){
            $this->profit_by_rank[$rank] = ($this->profit * $percent)/100;
        }
    }

    private function calculate_profit_percent($total_point){
        $this->profit = $total_point > 0 ? ($total_point * 3)/100 : 0;
    }
    
    private function calculate_quarter(){
        $schedule = [3,6,9,12];
        $current_month = date('n');
        $current_month = 9;
        if(in_array($current_month, $schedule)){
            $this->end = date('Y-m-t');
            $start = strtotime ( '-3 month' , strtotime ( $this->end ) ) ;
            $this->start = date ( 'Y-m-j' , $start );
        }else{
            die('It\'s not time to run the cron');
        }
    }

    private function send_mail_rank($user)
    {
        //Cargar vista html
        $html = $this->ci->load->view("emails/".$user['language']."/rank_up",$user, TRUE);
    
        //Configurar y enviar correo
        $this->ci->email->from($this->ci->settings->site_email);
        $this->ci->email->to($user['email']);
        $this->ci->email->cc($this->ci->settings->site_email);
        
        if ($user['language'] == 'english') {
         $this->ci->email->subject('Rank up for ' . $user['first_name']);
        }
        elseif ($user['language'] == 'spanish') {
          $this->ci->email->subject('Subida de rango para ' . $user['first_name']);     
        }
        elseif ($user['language'] == 'portuguese') {
          $this->ci->email->subject('Aumento do Rank para ' . $user['first_name']);        
        }
        
        $this->ci->email->message($html);
        $this->ci->email->set_mailtype("html");
        $this->ci->email->send();
    }
}
