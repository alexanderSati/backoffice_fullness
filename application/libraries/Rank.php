<?php defined('BASEPATH') OR exit('No direct script access allowed');

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */



/**
 * Description of Wallet
 *
 * @author Juan Manuel Pinzon
 */
class Rank {

    public function __construct()
    {

            // just in case url helper has not load yet
            $this->ci =& get_instance();

            // Load models
            $this->ci->load->model('users_model','users');
            $this->ci->load->model('ranks_model','ranks');
            $this->ci->load->library('transaction');
            $this->ci->load->library('wallet');

    }

    

    function get($uid = null){

        if($uid){
            return $this->ci->ranks->get(array('id_user' => $uid));
        }        

    }



    function create($uid = null){

        if($uid){
            return $this->ci->ranks->insert($uid);
        }

        

    }

    

    function get_allowed(){

        $date = date("Y-m-d");
        return $this->ci->ranks->get(" DATE_FORMAT(last_updated, '%Y-%m-%d') < '{$date}'");

    }

    

    function update($uid = null, $data = array()){

        if($uid && $data){
             return $this->ci->ranks->update($uid, $data);
        }        

    }

    

    function add_bonus($uid = null, $bonus = null)
    {
        if ($uid && $bonus) {
            return $this->ci->ranks->add_bonus($uid, $bonus);
        }

    }

    public function get_rank($id_user){

        $transactions = $this->ci->transaction->get_transactions_by_types($id_user, [
            "status"=>"done",
            "type"=>'infinity',
            "id_user"=>$id_user
        ], "10");

        if( empty( $transactions ) ){
            return [ "id"=>0,"name"=> "no_rank"];
        }

        $rango = [
            [ "id"=>1,"name"=> "Asistente"],
            [ "id"=>2,"name"=> "Executivo"],
            [ "id"=>2,"name"=> "Executive"],
            [ "id"=>3,"name"=> "Senior"],
            [ "id"=>4,"name"=> "Ruby"],
            [ "id"=>5,"name"=> "Diamante"],
            [ "id"=>6,"name"=> "Doble Diamante"],
            [ "id"=>7,"name"=> "Triple Diamante"],
            [ "id"=>8,"name"=> "Red Diamond"],
            [ "id"=>9,"name"=> "Black Diamond"],
            [ "id"=>10,"name"=> "Grand Diamond"],
            [ "id"=>11,"name"=> "Grand Black Diamond"]
        ];

        foreach( $rango as $r ){

            if( strpos( strtolower( $transactions[0]['description'] ), strtolower( $r["name"] ))   ){
                return $r;
            }
        }
    }

}

