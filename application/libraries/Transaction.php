<?php

defined('BASEPATH') OR exit('No direct script access allowed');

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of Transactions
 *
 * @author Juan Manuel Pinzon
 */
class Transaction {

    /**
     * load configuration
     * 
     * @param array $config 
     */
    const MIN_PAYOUT = 10;
    const MAX_BTC_PAYOUT = 600;
    const MAX_WALLETS_PAYOUT = 300;
    const MAX_PAYOUTS_DAYS = 15;
    const COMMISSION_7 = 800;
    const COMMISSION_14 = 3000;
    const TRANSFER_U2U_FEE = 1;
    const TRADING_FEE = 1;
    const COIN_DEPOSIT_FEE = 5;
    const MONTHLY_FEE = 20;

    var $statuses;

    function __construct() {
        // just in case url helper has not load yet
        $this->ci = & get_instance();


        // Load models
        $this->ci->load->model('users_model', 'users');
        $this->ci->load->model('plans_model', 'plans');
        $this->ci->load->model('subscriptions_model', 'subscriptions');
        $this->ci->load->model('transactions_model', 'transactions');
        $this->ci->load->library('wallet');
        $this->ci->load->library('currency');
        $this->ci->load->library('binarytree');
        $this->ci->load->library('referral');




        $this->statuses = array('done', 'pending', 'failed', 'cancelled', 'rejected', 'waiting');
    }

    function addBonus(array $subscription = array()) {

        if ($subscription) {
            
            // Generate bonus amount
            //$bonus = mt_rand($subscription['min_profit']*10,($subscription['max_profit'])*10) / 10;

//            $per = range(0.1, 0.5, 0.1);
//            shuffle($per);
//
//            $bonus = $subscription['min_profit'] + $per[0];
            //$bonus = $subscription['min_profit'] / 2; // Los pagos diarios se reducirán en un 50%
            
            if($subscription['id_plan']){
               
                switch ($subscription['id_plan']) {
                    case 1: //300
                            $range = [0.8,0.9,1];
                        break;
                    case 2: // 1000
                            $range = [3.6,3.7,3.8,3.9];
                        break;
                    case 3: // 10000
                            $range = [36,37,38];
                        break;
                    case 4: // 500
                            $range = [1.7,1.8,1.9,2];
                        break;

                    default:
                        $range = [0.8];
                        break;
                }
            }else{
                exit();
            }
            
            // Randomize among values
            shuffle($range);
            $bonus = $range[0];

            $transaction = array(
                'id_user' => $subscription['id_user'],
                'description' => 'Daily profit',
                'usd' => $bonus,
                'btc' => $this->ci->currency->btc($bonus),
                'rate' => $this->ci->currency->rate('USD'),
                'type' => 'bonus',
                'date' => date('Y-m-d H:i:s')
            );
            
            // Monthly payment validation
            if(!is_active($subscription['id_user'])){
                $bonus = 0;
                $transaction['usd'] = 0;
                $transaction['btc'] = 0;
                $transaction['description'] = 'Daily profit (Invalid monthly fee status)';
            }

            // Insert transaction history
            $inserted = $this->ci->transactions->insert($transaction);

            if ($inserted) {
                $subscription['period_days_left'] = $subscription['period_days_left'] - 1;
                $subscription['profit'] = $subscription['profit'] + $bonus;
                //$subscription['base']   = $subscription['base'] - $subscription['min_profit'];
                $subscription['last_updated'] = date("Y-m-d H:i:s");

                if ($subscription['period_days_left'] == 0) {
                    $subscription['status'] = "expired";
                }

                if (date("Y-m-d", strtotime($subscription['period_days_left'])) >= date("Y-m-d")) {
                    $subscription['status'] = "expired";
                }

                // Add USD to wallet
                $this->ci->wallet->add_funds($subscription['id_user'], $bonus, 'USD');

                // Update account profits and status
                $this->ci->subscriptions->update($subscription);
            }
        }
    }

    function addBinaryBonus($id = null, $bonus = null, $description = 'Binary Tree profit') {

        if ($id && $bonus) {

            $transaction = array(
                'id_user' => $id,
                'description' => $description,
                'usd' => $bonus,
                'btc' => $this->ci->currency->btc($bonus),
                'rate' => $this->ci->currency->rate('USD'),
                'type' => 'binary',
                'date' => date('Y-m-d H:i:s')
            );

            // Insert transaction history
            $inserted = $this->ci->transactions->insert($transaction);

            if ($inserted) {
                // Load suscription data
                //$subscription = $this->ci->subscriptions->get_active($id);
                //$subscription['profit'] = $subscription['profit'] + $bonus;
                // Update account profits and status
                //$this->ci->subscriptions->update($subscription);
                // Add bonus to user operative wallet
                $this->ci->wallet->add_funds($id, $bonus, 'BTC');
            }
        }
    }
    

    function addRankBonus($id = null, $bonus = null, $rank = 0) {

        $ranks = array(
            0 => 'Unranked',
            1 => 'Trader',
            2 => 'Executive trader',
            3 => 'Professional trader',
            4 => 'Presidential trader',
            5 => 'Master trader',
            6 => 'International master trader',
        );

        if ($id && $bonus) {

            $transaction = array(
                'id_user' => $id,
                'description' => "Rank profit ({$ranks[$rank]})",
                'usd' => $bonus,
                'btc' => null,
                'rate' => $this->ci->currency->rate('USD'),
                'type' => 'rank',
                'date' => date('Y-m-d H:i:s')
            );

            // Insert transaction history
            $inserted = $this->ci->transactions->insert($transaction);

            if ($inserted) {
                // Load suscription data
                $subscription = $this->ci->subscriptions->get_active($id);
                $subscription['profit'] = $subscription['profit'] + $bonus;

                // Update account profits and status
                $this->ci->subscriptions->update($subscription);

                // Add bonus to USD wallet
                $this->ci->wallet->add_funds($id, $bonus, 'USD');
            }
        }
    }

    function addTreePoints($id = null, $points = null, $side = 'left') {

        if ($id && $points) {

            $line = ($side == 'left') ? "A" : "B";

            $transaction = array(
                'id_user' => $id,
                'description' => "Business line ({$line}) {$points} points bonus",
                'usd' => 0,
                'btc' => 0,
                'rate' => 0,
                'type' => 'points',
                'status' => 'done',
                'date' => date('Y-m-d H:i:s')
            );

            // Insert transaction history
            $this->ci->transactions->insert($transaction);
        }
    }

    function addUpgrade($id = null, $amount = null,$pago_wallet=null) {

        if ($id && $amount) {

            $transaction = array(
                'id_user' => $id,
                'reference'=>(empty($pago_wallet)? null:$pago_wallet),
                'description' => "Plan Upgrade".(empty($pago_wallet)? "":"pago con wallet".$pago_wallet),
                'usd' => -$amount,
                'btc' => 0,
                'rate' => 0,
                'type' => 'upgrade',
                'status' => 'pending',
                'date' => date('Y-m-d H:i:s')
            );

            // Insert transaction history
            $this->ci->transactions->insert($transaction);
        }
    }

    
    
    /**
     * Create a new monthly payment for an user
     * @param unt $uid
     * @return int
     */
    function addMonthlyPayment($uid = null, $custom_ref = null){
        
        $this->ci->load->helper('array_helper');
        
        $custom = ($custom_ref) ? str_replace('/', '-', '15/'.$custom_ref) :null;
        
        if ($uid) {
            
            // Random app wallet
            $wallets = array();
            if($this->ci->settings->wallet_1){
                $wallets[]  = $this->ci->settings->wallet_1;
            }
            
            if($this->ci->settings->wallet_2){
                $wallets[]  = $this->ci->settings->wallet_2;
            }
            
            $wallet = random_element($wallets);

            // Set amounts and rates
            $btcs = $this->ci->currency->btc(self::MONTHLY_FEE);
            $rate = $this->ci->currency->rate('USD');
            
            // Get payment month reference
            $reference = ($custom) ? date('m/Y', strtotime($custom)) : date('m/Y',  strtotime('last day of next month'));
            $human_format = ($custom) ? date('F/Y', strtotime($custom)) : date('F/Y', strtotime('last day of next month'));
            
            // Insert transaction history
            $payment = array(
                'id_user' => $uid,
                'reference' => $reference,
                'description' => "Monthly Fee : <code>{$human_format}</code><br>Deposit to wallet : {$wallet}",
                'usd' => self::MONTHLY_FEE,
                'btc' => $btcs,
                'rate' => $rate,
                'wallet' => 'USD',
                'type' => 'monthly',
                'status' => 'waiting'
            );
                
            // Insert transaction history for "from" user
            $this->ci->transactions->insert($payment);
            return $this->ci->db->insert_id();
        }
    }

    function _sendEmail($uid = null, $template = null, $data = array()) {
        $user = $this->ci->users->get_user($uid);

        if ($user) {
            $this->ci->lang->load('emails', $user['language']);
            $html = $this->ci->load->view("emails/{$user['language']}/{$template}", $data, TRUE);

            // send email
            $this->ci->load->library('email');
            $this->ci->email->from("{$this->ci->settings->site_email}");
            $this->ci->email->to($user['email']);
            $this->ci->email->subject($this->ci->lang->line($template));
            $this->ci->email->message($html);
            $this->ci->email->set_mailtype("html");
            $this->ci->email->send();
        }
    }

    /*
     * Controllers methods
     */

    // Transfer bitcoins between internal users
    function transfer_u2u($from = null, $to = null, $amount = 0, $currency = 'USD') {
        /*
         * Errors list
         * -1 Invalid amount
         * -2 Invalid user
         * -3 Invalid wallet symbol
         * -4 Not enough funds
         */

        // Default transfer fee
        $fee = 0;

        if ($from && $to && $amount) {

            // No se puede transferir fondos desde la billetera BTC, debe ser de operative BTC
            // if ($currency == 'USD') {
            //     return -3;
            // }

            // Get users profiles
            $user_from = $this->ci->users->get_user($from);
            $user_to = $this->ci->users->get_user($to);

            // Get Currency value of "from" user
            $USD_from = $this->ci->wallets->get($user_from['id'], $currency);

            if ($USD_from >= $amount) {

                // Update funds over each user wallets
                $t1 = $this->ci->wallet->subtract_funds($user_from['id'], $amount, $currency);

                // Apply transfer fee
                if ($currency != 'BTC' && $currency != 'USD') {
                    $fee = ($amount * (self::TRANSFER_U2U_FEE / 100));
                }
                $amount_fee = $amount - $fee;

                $t2 = $this->ci->wallet->add_funds($user_to['id'], $amount_fee, $currency);

                // Return errors on wallets
                if ($t1 != 1 || $t2 != 1) {
                    return ($t1 != 1) ? $t1 : $t2;
                } else {
                    // Insert transaction history
                    $from = array(
                        'id_user' => $user_from['id'],
                        'description' => "Transfer between accounts ({$user_to['username']})",
                        'usd' => -$amount,
                        'btc' => 0,
                        'wallet' => strtoupper($currency),
                        'rate' => 0,
                        'type' => 'transfer',
                    );

                    // Insert transaction history for "from" user
                    $this->ci->transactions->insert($from);

                    // Insert transaction history
                    $to = array(
                        'id_user' => $user_to['id'],
                        'description' => "Transference from {$user_from['username']}",
                        'usd' => $amount_fee,
                        'btc' => 0,
                        'wallet' => strtoupper($currency),
                        'rate' => 0,
                        'type' => 'transfer',
                    );
                    

                    //Enviar email transferecia
                    $this->enviarEmailTransferFrom($user_from, $user_to, $amount_fee);
                    $this->enviarEmailTransferTo($user_from, $user_to, $amount_fee);

                    // Insert transaction history for "to" user
                    $this->ci->transactions->insert($to);
                    return true;
                }
            } else {
                // Not enough funds
                return -4;
            }
        }
    }
    function enviarEmailTransferFrom($from, $to, $amount)
    {     
        $results['user_from'] = $from;
        $results['user_to'] = $to;
        $results['amount'] = $amount;
       

        //Cargar vista html
        $html = $this->ci->load->view("emails/".$results['user_from']['language']."/transfer_from", $results, TRUE);

        //Configurar y enviar correo
        $this->ci->load->library('email');
        $this->ci->email->from($this->ci->settings->site_email);
        $this->ci->email->to($results['user_from']['email']);

        if ($results['user_from']['language'] == 'english' ) {
            $this->ci->email->subject('Transfer by ' . $results['user_from']['first_name']);
        }elseif ($results['user_from']['language'] == 'spanish') {
            $this->ci->email->subject('Tranferencia por ' . $results['user_from']['first_name']);            
        }elseif ($results['user_from']['language'] == 'portuguese') {
            $this->ci->email->subject('Transferência por ' . $results['user_from']['first_name']);                        
        }

        $this->ci->email->message($html);
        $this->ci->email->set_mailtype("html");
        $this->ci->email->send();

  
    }
    function enviarEmailTransferTo($from, $to, $amount)
    {
        $results['user_from'] = $from;
        $results['user_to'] = $to;
        $results['amount'] = $amount;      


        //Cargar vista html
        $html = $this->ci->load->view("emails/".$results['user_to']['language']."/transfer_to", $results, TRUE);

        //Configurar y enviar correo
        $this->ci->load->library('email');
        $this->ci->email->from($this->ci->settings->site_email);
        $this->ci->email->to($results['user_to']['email']);

        if ($results['user_from']['language'] == 'english' ) {
            $this->ci->email->subject('You received a transfer for ' . $results['user_from']['first_name']);
        }elseif ($results['user_from']['language'] == 'spanish') {
            $this->ci->email->subject('Recibiste una transferencia por ' . $results['user_from']['first_name']);            
        }elseif ($results['user_from']['language'] == 'portuguese') {
            $this->ci->email->subject('Você recebeu uma transferência para ' . $results['user_from']['first_name']);                        
        }
        
        $this->ci->email->message($html);
        $this->ci->email->set_mailtype("html");
        $this->ci->email->send();
    }

    function transfer_btw_wallets($uid = null, $from = null, $to = null, $amount = 0) {

        if ($uid && $from && $to && $amount) {

            /*
             * Error list
             * -1 Invalid amount
             * -2 User not found
             * -3 Invalid wallet
             * -4 No allowed from BTC(USD) to other coins
             * -5 No enough funds
             */

            if ($amount <= 0) {
                return -1;
            }

            $user = $this->ci->users->get_user($uid);
            if (!$user) {
                return -2;
            }

            $allowed = array('USD', 'BTC');

            // If it's an allowed wallet
            //if(!array_key_exists($from, $this->ci->wallets->table->fields) || !array_key_exists($to, $this->ci->wallets->table->fields)){
            if (!in_array($from, $allowed) || !in_array($to, $allowed)) {
                return -3;
            }

            if ($to == $from) {
                return -3;
            }

            // From USD wallet to another wallet is not allowed
            if ($from == 'USD' && $to != 'BTC') {
                return -4;
            }

            // Convert virtual coin amount to USD
            $this->ci->currency->load(($from == 'USD') ? 'BTC' : $from);

            // Convert transfer amount in USD
            $trans_usd = $this->ci->currency->usd($amount);

            // Get user operative wallet funds where substract funds
            $from_wallet = $this->ci->wallet->get($uid, $from);
            $from_wallet_usd = ($from == 'USD' || $from == 'BTC') ? $from_wallet : $this->ci->currency->usd($from_wallet);

            // Wallet funds validation
            if ($from_wallet_usd < $trans_usd) {
                return -5;
            }

            // Convert virtual coin amount to USD
            $this->ci->currency->load(($to == 'USD') ? 'BTC' : $to);
            $to_amount = $this->ci->currency->btc($trans_usd);
            $to_rate = $this->ci->currency->rate();

            $from_txt = ($from == 'USD') ? 'BTC' : $from;
            $to_txt = ($to == 'BTC') ? "BTC (operative)" : "({$to})";

            // Insert transaction history
            $data = array(
                'id_user' => $user['id'],
                'description' => "Transference {$amount} {$from_txt} to {$to_txt} wallet",
                'usd' => -$trans_usd,
                'btc' => -$amount,
                'wallet' => 'usd',
                'rate' => $to_rate,
                'type' => 'transfer',
                'status' => 'done'
            );

            // Insert transaction 
            $transid = $this->ci->transactions->insert($data, true);
            if ($transid) {

                $subs = $this->ci->wallet->subtract_funds($user['id'], (($from == 'USD' || $from == 'BTC') ? $trans_usd : $amount), $from);
                $add = $this->ci->wallet->add_funds($user['id'], (($to == 'USD' || $to == 'BTC') ? $trans_usd : $to_amount), $to);

                if ($subs and $add) {
                    return true;
                }
            }
        }
        return false;
    }

    // Return true or false in case transactions fails
    function btc_deposit($uid = null, $usd = 0, $wallet = null) {

        /*
         * Errors list
         * -1 Invalid amount
         * -2 Invalid user
         * -3 Invalid wallet
         */

        if (intval($usd) <= 0) {
            return -1;
        }

        if (!$wallet) {
            return -3;
        }

        if ($uid and $usd) {

            $user = $this->ci->users->get_user($uid);

            if (!$user) {
                return -2;
            }

            $btcs = $this->ci->currency->btc($usd);
            $rate = $this->ci->currency->rate('USD');

            // Insert transaction history
            $from = array(
                'id_user' => $user['id'],
                'description' => 'BTC Deposit:' . $wallet,
                'usd' => $usd,
                'btc' => $btcs,
                'rate' => $rate,
                'wallet' => 'USD',
                'type' => 'deposit',
                'status' => 'waiting'
            );

            // Insert transaction history for "from" user
            $transid = $this->ci->transactions->insert($from, true);

            if ($transid) {
                $data = array(
                    'transaction' => $transid,
                    'date' => date('Y-m-d H:i'),
                    'name' => "{$user['first_name']} {$user['last_name']}",
                    'username' => $user['username'],
                    'email' => $user['email'],
                    'usd' => $usd,
                    'btc' => $btcs
                );

                // Admin email
                //$this->_sendEmail(1,'btc_deposit_admin',$data);
                // User email
                $this->_sendEmail($user['id'], 'btc_deposit_user', $data);
                return $this->ci->db->insert_id();
            }

            return false;
        }
    }
    
    // Return true or false in case transactions fails
    function coin_deposit($uid = null, $amount = null, $coin = null, $amount_final = null, $coin_final = null, $wallet = null ) {
        
        /*
         * Errors list
         * -1 Invalid amount
         * -2 Invalid user
         * -3 Invalid wallet
         */

        if ($amount_final <= 0) {
            return -1;
        }

        if (!$wallet) {
            return -3;
        }

        if ($uid and $amount_final) {

            $user = $this->ci->users->get_user($uid);

            if (!$user) {
                return -2;
            }

            $this->ci->currency->load($coin);
            $amount_usd = $this->ci->currency->usd($amount);
            $amount_rate = $this->ci->currency->usd(1);

            $this->ci->currency->load('BTC');
            
            $btcs = $this->ci->currency->btc($amount_usd);
            
            $this->ci->currency->load($coin_final);
            $rate = $this->ci->currency->btc($amount_rate);
            $custom_code = "{$coin}|{$coin_final}|";
            
            if($coin_final == "USD"){
                $amount_final = $this->ci->currency->usd($amount_final);
            }

            // Insert transaction history
            $from = array(
                'id_user' => $user['id'],
                'description' => "Coin Deposit {$amount} <code>{$coin}</code> to <code>{$coin_final}</code>:<br/>{$wallet}",
                'usd' => round($amount_final, 8),
                'btc' => round($btcs, 8),
                'rate' => round($rate, 8),
                'wallet' => $coin_final,
                'type' => 'coin-deposit',
                'status' => 'waiting'
            );

            // Insert transaction history for "from" user
            $transid = $this->ci->transactions->insert($from, true, $custom_code);

            if ($transid) {
                $data = array(
                    'transaction' => $transid,
                    'date' => date('Y-m-d H:i'),
                    'name' => "{$user['first_name']} {$user['last_name']}",
                    'username' => $user['username'],
                    'email' => $user['email'],
                    'amount' => $amount,
                    'amount_final' => $amount_final,
                    'coin' => $coin,
                    'coin_final' => $coin_final,
                    'usd' => $amount_usd,
                    'btc' => $btcs
                );

                // Admin email
                //$this->_sendEmail(1,'btc_deposit_admin',$data);
                // User email
                $this->_sendEmail($user['id'], 'coin_deposit_user', $data);
                return $this->ci->db->insert_id();
            }

            return false;
        }
    }

    // Return transaction list given a users id
    function user($id, $type = null) {
        $where = array();
        $where['id_user'] = $id;
        if(is_array($type)){
            $where = "id_user={$id} AND type IN (".join(',',$type).") ";
        }elseif ($type) {
            $where['type'] = $type;
        }
        return ($id) ? $this->ci->transactions->get($where, 1000) : array();
    }

    //Return transaction by month and user id 
    function user_month($where){
        return ($where) ? $this->ci->transactions->get($where, 1000) : array();
    }

    // Return transaction list given an user id
    function user_types($id, $types = null) {

        $sql = "id_user = {$id} and type IN({$types}) AND usd >= 0";
        return ($id) ? $this->ci->transactions->get($sql, 1000) : array();
    }

    // Return transactions list for adminers
    function admin($where = null) {
        $items = $this->ci->transactions->get($where,2000,'last_updated','DESC');

        foreach ($items as &$transaction) {
            $user = $this->ci->users->get_user($transaction['id_user']);
            $transaction['name_user'] = $user['first_name'] . " " . $user['last_name'];
            $transaction['username'] = $user['username'];
            $transaction['email'] = $user['email'];
            
//            if(strlen($transaction['description']) > 60){
//                $new_description = strip_tags($transaction['description']);
//                $new_description = substr($new_description, 0, 60);
//                $transaction['description'] = $new_description . '...';
//            }
        }

        return $items;
    }

    // Return daily bonus given a user id (chart)
    function userBonus($id) {
        return ($id) ? $this->ci->transactions->get(array('id_user' => $id, 'type' => 'bonus', 'status' => 'done'), 7) : array();
    }

    // Update status transaction
    function updateStatus($id = null, $status = 'pending') {

        /*
         * Error list
         * -1 Invalid status
         * -2 Invalid transaction
         * -3 Nothing to do (Already done)
         * -4 Invalid Transaction
         */

        $status = strtolower(filter_var($status, FILTER_SANITIZE_STRING));
        $id = filter_var($id, FILTER_SANITIZE_NUMBER_INT);

        $transaction = $this->ci->transactions->get_row($id);

        if (!in_array($status, $this->statuses)) {
            return -1;
        }

        if ($transaction) {

            if ($transaction['status'] == $status) {
                return -3;
            }

            switch ($transaction['type']) {
                case 'deposit':

                    $data = array(
                        'reviewer' => $this->ci->user['id'],
                        'status' => $status,
                        'last_updated' => date("Y-m-d H:i:s")
                    );

                    $email_data = array(
                        'transaction' => $transaction['cod'],
                        'date' => date('Y-m-d H:i'),
                        'usd' => $transaction['usd'],
                        'btc' => $transaction['btc'],
                        'status' => $status
                    );

                    if ($status == 'done') {
                        $this->ci->wallet->add_funds($transaction['id_user'], $transaction['usd'], 'USD');
                    }

                    // $this->_sendEmail($transaction['id_user'], 'btc_deposit_user_done', $email_data);
                    return $this->ci->transactions->update($id, $data);
                    
                case 'coin-deposit':

                    $data = array(
                        'reviewer' => $this->ci->user['id'],
                        'status' => $status,
                        'last_updated' => date("Y-m-d H:i:s")
                    );

                    $email_data = array(
                        'transaction' => $transaction['cod'],
                        'date' => date('Y-m-d H:i'),
                        'usd' => $transaction['usd'],
                        'wallet' => $transaction['wallet'],
                        'btc' => $transaction['btc'],
                        'status' => $status
                    );

                    if ($status == 'done') {
                        $this->ci->wallet->add_funds($transaction['id_user'], $transaction['usd'], $transaction['wallet']);
                    }

                    // $this->_sendEmail($transaction['id_user'], 'coin_deposit_user_done', $email_data);
                    return $this->ci->transactions->update($id, $data);

                case 'payout':

                    $data = array(
                        'reviewer' => $this->ci->user['id'],
                        'status' => $status,
                        'last_updated' => date("Y-m-d H:i:s")
                    );

                    $email_data = array(
                        'transaction' => $transaction['cod'],
                        'date' => date('Y-m-d H:i'),
                        'usd' => $transaction['usd'],
                        'btc' => $transaction['btc'],
                        'amount' => $transaction['btc'],
                        'wallet' => $transaction['wallet'],
                        'wallet_qr' => $transaction['description'],
                        'status' => $status
                    );

                    $updated = $this->ci->transactions->update($id, $data);

                    if ($updated) {
                        // In case payout was reject or fail return funds to user wallet
                        if ($status != 'done') {
                            if ($transaction['wallet'] == 'BTC' || $transaction['wallet'] == 'USD') {
                                $this->ci->wallet->add_funds($transaction['id_user'], $transaction['usd'], 'USD');
                            } else {
                                $this->ci->wallet->add_funds($transaction['id_user'], $transaction['btc'], $transaction['wallet']);
                            }
                        }

                        // $this->_sendEmail($transaction['id_user'], 'btc_payout_user_done', $email_data);
                    }

                    return $updated;

                case 'referral' :

                    $data = array(
                        'status' => $status,
                        'last_updated' => date("Y-m-d H:i:s")
                    );

                    if ($status == 'done') {

                        // add wallet bonus
                        $this->ci->wallet->add_funds($transaction['id_user'], $transaction['usd'], 'BTC');

                        // add wallet profit
                        $this->ci->subscriptions->add_profit($transaction['id_user'], $transaction['usd']);
                    }

                    return $this->ci->transactions->update($id, $data);
                    
                case 'trading' :
                case 'monthly' :

                    $data = array(
                        'status' => $status,
                        'last_updated' => date("Y-m-d H:i:s")
                    );
                    return $this->ci->transactions->update($id, $data);
                    
                case 'renewal' :
                    
                    if ($status == 'done') {
                        $this->ci->subscriptions->activate($transaction['id_user']);
                    }else {
                        $subscription = $this->ci->subscriptions->get_last($transaction['id_user']);
                        $this->ci->subscriptions->delete($subscription['id']);
                    }
                    
                    $data = array(
                        'status' => $status,
                        'last_updated' => date("Y-m-d H:i:s")
                    );
                    return $this->ci->transactions->update($id, $data);

                default:
                    return -4;
            }
        } else {
            return -2;
        }
    }

    function payout($id = null, $symbol = 'USD', $amount = 0) {

    


        /*
         * Payout amount validations
         * EN BTC EL MAXIMO RETIRO POSIBLE SERA DE 600 USD. 
         * EN LAS DEMAS BILLETERAS DISPONIBLES SOLO HABRA UN MAXIMO PAYOUT DE 300 USD
         */

        // if ($usd < self::MIN_PAYOUT) {
        //     return -1;
        // }

        // if ($symbol == 'USD' || $symbol == 'BTC') {
        //     if ($usd > self::MAX_BTC_PAYOUT) {
        //         return -1;
        //     }
        // } else {
        //     if ($usd > self::MAX_WALLETS_PAYOUT) {
        //         return -1;
        //     }
        // }

        /*
         * Get user information
         */
        $user = $this->ci->users->get_user($id);

        if (!$user) {
            return -2;
        }


        /*
         * Payout day limits validation 
         */

        if (!$this->is_payout_available($id)) {
            return -4;
        }


        /*
         * Wallets and funds validations
         */


        // If it's an allowed wallet
        if (!array_key_exists($symbol, $this->ci->wallets->table->fields)) {
            return -3;
        }

        /*
         * OPERATIVE BTC NO ES POSIBLE HACER PAYOUT
         */
        if ($symbol == 'BTC') {
            return -3;
        }

        // Get wallet amount from user
        if ($symbol == 'USD') {

            $wallet = $this->ci->wallet->get($id, 'USD');

            if ($amount > $wallet) {
                return -5;
            }
        }
       

        $comission = 20;

        $userPayout = array(
            'id_user' => $user['id'],
            'date' => date('Y-m-d H:i'),
            'username' => $user['username'],
            'email' => $user['email'],
            'dni' => $user['dni'],
            'first_name' => $user['first_name'],
            'last_name' => $user['last_name'],
            'wallet' => $symbol,
            'amount' => $amount,
            'usd' => $amount
        
        );

        $adminPayout = array(
            'id_user' => $user['id'],
            'date' => date('Y-m-d H:i'),
            'username' => $user['username'],
            'email' => $this->ci->settings->site_email,
            'dni' => $user['dni'],
            'first_name' => $user['first_name'],
            'last_name' => $user['last_name'],
            'wallet' => $symbol,
            'amount' => $amount,
            'usd' => $usd
        );

        // Insert transaction history
        $payout_data = array(
            'id_user' => $user['id'],
            'description' => "Pago con billetera (S/.)",
            //'usd' => -$usd,
            //'btc' => -$amount,
            'btc'=>0,
            'usd' => abs($amount),
            'wallet' => $symbol,
            'rate' => $rate,
            'type' => 'payout',
            'status' => 'pending'
        );

        // Insert transaction 
        $transid = $this->ci->transactions->insert($payout_data, true);

        if ($transid) {


            // Discount wallet funds    
             $this->ci->wallet->subtract_funds($user['id'], $amount, 'USD');
            

            $userPayout['transaction'] = $transid;
            $adminPayout['transaction'] = $transid;

            // Admin email
            $this->_sendEmail(1, 'btc_payout_admin', $adminPayout);
            // User email
            $this->_sendEmail($user['id'], 'btc_payout_user', $userPayout);
            return true;
        }

        return false;
    }

    /**
     * Insert transaction record when a trading was pusblished
     * @param array $trading
     * @return booeland or id
     */
    function trading(array $trading) {
        if ($trading) {
            switch ($trading['type']) {
                case 'sell':
                    // Insert transaction history
                    $data = array(
                        'id_user' => $trading['id_user'],
                        'description' => "Trading sell offer ({$trading['amount']} {$trading['coin']})",
                        'usd' => -$trading['amount'],
                        'btc' => $trading['btc_total'],
                        'wallet' => $trading['coin'],
                        'rate' => $trading['btc_total'],
                        'type' => 'trading',
                        'status' => 'pending'
                    );
                    break;

                case 'buy':

                    // Insert transaction history
                    $data = array(
                        'id_user' => $trading['id_user'],
                        'description' => "Trading buy offer ({$trading['amount']} {$trading['coin']})",
                        'usd' => -$trading['btc_total'],
                        'btc' => -$trading['btc_total'],
                        'wallet' => 'BTC',
                        'rate' => $trading['btc_price'],
                        'type' => 'trading',
                        'status' => 'pending'
                    );
                    break;
            }
            return $this->ci->transactions->insert($data, false);
        }
        return false;
    }
    

    /**
     * Insert transaction when a trading was sell and paid
     * @param int $id_user
     * @param array $trading
     * @return boolean or id
     */
    function tradingPaid($id_user, array $trading) {
        if ($id_user && ($trading['type'] == 'sell')) {
            
            $btc = new Currency('BTC');
            
            // Insert payment history
            $data = array(
                'id_user' => $id_user,
                'description' => "Trading purchased ({$trading['amount']} {$trading['coin']})",
                'usd' => -round($btc->usd($trading['btc_total']), 2),
                'btc' => -number_format($trading['btc_total'], 8),
                'wallet' => 'usd',
                'rate' => $trading['btc_price'],
                'type' => 'trading',
                'status' => 'done'
            );
            return $this->ci->transactions->insert($data, false);
        }
        return false;
    }
    
    /**
     * Insert transaction when an user sell 
     * coins to another using a trading buy offer
     * @param int $id_user
     * @param array $trading
     * @return boolean
     */
    function tradingSell($id_user, array $trading) {
        if ($id_user && ($trading['type'] == 'buy')) {
            
            $btc = new Currency('BTC');
            
            // Insert payment history
            $data = array(
                'id_user' => $id_user,
                'description' => "Trading sell (-{$trading['amount']} {$trading['coin']})",
                'usd' => round($btc->usd($trading['btc_total']), 2),
                'btc' => number_format($trading['btc_total'], 8),
                'wallet' => 'usd',
                'rate' => $trading['btc_price'],
                'type' => 'trading',
                'status' => 'done'
            );
            return $this->ci->transactions->insert($data, false);
        }
        return false;
    }
    
    /**
     * Transaction Fee
     * Create transaction history in user dashboard
     * @param array $trading
     * @return int id or false
     */
    function tradingFee($id_user, array $trading) {
        if ($id_user) {
            $btc = new Currency('BTC');

            // Insert transaction history
            $data = array(
                'id_user' => $id_user,
                'description' => "Trading {$trading['type']} offer fee",
                'usd' => -round($btc->usd($trading['btc_fee']), 2),
                'btc' => -number_format($trading['btc_fee'], 8),
                'wallet' => 'usd',
                'rate' => number_format($trading['btc_fee'], 8),
                'type' => 'tradingfee',
                'status' => 'done'
            );

            // Insert transaction 
            return $this->ci->transactions->insert($data, false);
        }
        return false;
    }
    
    /**
     * Insert transaction when an user renew own account 
     * @param int $user
     * @param int $plan
     * @param boolean $deposit
     * @param string $coin
     * @param string $wallets_code
     * @return boolean
     */
    function renewal($user = null, $plan = null, $deposit = false, $coin = null, $wallets_code = null) {
        if ($user && $plan) {

            /*
             * Error list
             * -1 Invalid plan
             * -3 Invalid wallet
             * -4 Error processing
             * -5 No enough funds
             */

            $plan = $this->ci->plans->get($plan);
            $wallet = $this->ci->wallet->get($user['id'], 'USD');

            if (!$plan) {
                return -1;
            }

            if (!$wallet) {
                return -3;
            }
            
            $subs = array(
                'id_user' => $user['id'],
                'plan' => $plan['id']
            );
            
            if($deposit && $coin && $wallets_code){
                
                $last_subscription = $this->ci->subscriptions->get_last($user['id']);
                $subs['profit'] = $last_subscription['profit'];
                
                $description = "Renewal payment from <code>{$coin}</code> |({$plan['english_name']})|\n{$wallets_code}";
                
                $this->ci->currency->load($coin);
                $amount = round($this->ci->currency->btc($plan['base']), 8);
                $rate = round($this->ci->currency->btc(1), 8);

                $this->ci->currency->load('BTC');
                $btcs = round($this->ci->currency->btc($plan['base']), 8);
                
                // Insert transaction record
                $data = array(
                    'id_user' => $user['id'],
                    'description' => $description,
                    'usd' => $amount,
                    'btc' => $btcs,
                    'wallet' => $coin,
                    'rate' => $rate,
                    'type' => 'renewal',
                    'status' => 'pending'
                );
                // Insert transaction 
                $transid = $this->ci->transactions->insert($data, true);
                if ($transid) {
                    // Create a new subscription
                    $this->ci->subscriptions->add($subs);
                }
                
                return true;
            }

            // founds validation
            if ($wallet >= $plan['base']) {

                // Insert transaction record
                $data = array(
                    'id_user' => $user['id'],
                    'description' => "Renewal payment ({$plan['english_name']})",
                    'usd' => -$plan['base'],
                    'btc' => 0,
                    'wallet' => 'USD',
                    'rate' => 0,
                    'type' => 'renewal',
                    'status' => 'done'
                );

                // Insert transaction 
                $transid = $this->ci->transactions->insert($data, true);

                if ($transid) {
                    //Plan payment
                    $this->ci->wallet->subtract_funds($user['id'], $plan['base'], 'USD');
                    
                    // Create a new subscription
                    $this->ci->subscriptions->add($subs);
                    
                    // Create a subscription plan of bonus and activate it
                    $this->ci->subscriptions->activate($user['id']);
                    
                    return true;
                } else {
                    return -4;
                }
            } else {
                return -5;
            }
        }

        return false;
    }

    // add bonus to user wallet triggered by cron job
    function referral_bonus() {

        $arg = array(
            'type' => 'referral',
            'status' => 'pending'
        );

        $list = $this->ci->transactions->get($arg, 50);

        foreach ($list as $pending) {
            $this->updateStatus($pending['id'], $status = 'done');
        }

        return true;
    }
    
    //
    function is_payout_available($uid, $formated = false) {
        if ($uid) {
            
            $subscription = $this->ci->subscriptions->get_subscription($uid);
            $plan = $this->ci->plans->get_plan($subscription['id_plan']);
            // Gey payouts of current week
            $payout = $this->ci->transactions->get("id_user={$uid} AND type='payout' AND status IN('done','pending') AND date > (CURRENT_TIMESTAMP - INTERVAL {$plan['max_payouts_frecuency']} {$plan['max_payout_interval']})", $plan['max_payout_attemp'], 'date', 'DESC');
            
            if ($payout) {

                $datetime1 = new DateTime($payout[0]['date']);
                $datetime2 = new DateTime();
                $difference = $datetime1->diff($datetime2);

                if (!$formated) {
                    
                    if (sizeof($payout) < $plan['max_payout_attemp']) {
                        return true;
                    }
                    return false;
                    
//                    return ($difference->days > self::MAX_PAYOUTS_DAYS) ? TRUE : FALSE;
                } else {
                    $options_interval = array("day" => "D", "month" => "M", "year" => "Y");
                    $interval = "P" . $plan['max_payouts_frecuency'] . $options_interval[$plan['max_payout_interval']];
//                    $interval = "P" . self::MAX_PAYOUTS_DAYS . "D";
                    $datetime1->add(new DateInterval($interval));
                    
                    return $datetime1->format('Y-m-d');
                }
            } else {
                // If user never has requested a payout
                return true;
            }
        }
        return FALSE;
    }

    function inscription($user = null, $plan = null, $wallet = null) {

        if ($user && $plan && $wallet) {

            $user = $this->ci->users->get_user($user);

            $plan = $this->ci->plans->get($plan);

            // Insert transaction record
            $data_trans = array(
                'id_user' => $user['id'],
                'description' => "Initial Deposit ({$plan['english_name']}):{$wallet}",
                'usd' => $plan['base'],
                'btc' => $this->ci->currency->btc($plan['base']),
                'wallet' => 'USD',
                'rate' => 0,
                'type' => 'subscription',
                'status' => 'done'
            );

            // Insert transaction 
            $transid = $this->ci->transactions->insert($data_trans, true);

            if ($transid) {
                $data = array(
                    'transaction' => $transid,
                    'date' => date('Y-m-d H:i'),
                    'username' => $user['username'],
                    'description' => "Initial Deposit ({$plan['english_name']}):{$wallet}",
                    'email' => $user['email'],
                    'usd' => $data_trans['usd'],
                    'btc' => $data_trans['btc']
                );

                // Admin email
                $this->_sendEmail(1, 'inscription_admin', $data);
                // User email
                $this->_sendEmail($user['id'], 'inscription_user', $data);
                return $this->ci->db->insert_id();
            }
        }
        return false;
    }
    
    function is_renewal_available($uid) {
        // Gey payouts of current week
        $transaction = $this->ci->transactions->get("id_user={$uid} AND type='renewal' AND status = 'pending'", 1, 'date', 'DESC');
        
        return !empty($transaction) ? true : false ;
    }

    function purchase($id_user, $id_order, $amount)
    {
        // Insert transaction history
        $transaction_data = array(
            'id_user' => $id_user,
            'reference'=>$id_order,
            'description' => "Compra de productos orden #{$id_order}",
            'usd' => abs($amount),
            'btc' => 0,
            'wallet' => 'USD',
            'rate' => 0,
            'type' => 'purchase',
            'status' => 'pending'
        );

        // Insert transaction 
        $transid = $this->ci->transactions->insert($transaction_data, true);
    }


    /**
     * Crea la transaccion que indica la compra con monto
     * de plan
     */
    function purchase_plan($id_user, $amount, $options = null)
    {
        // Insert transaction history
        $transaction_data = array(
            'id_user' => $id_user,
            'reference'=>'',
            'description' => "Compra de plan orden #{$options["reference"]}",
            'usd' => abs($amount),
            'btc' => 0,
            'wallet' => 'USD',
            'rate' => 0,
            'type' => 'purchase_plan',
            'status' => 'pending'
        );

        if( !empty( $options ) ){
            $transaction_data = array_merge( $transaction_data, $options );
        }

        // Insert transaction 
        $transid = $this->ci->transactions->insert($transaction_data, true);
    }

    /**
     * Crea el registro de una transaccion cuando se ha pagado 
     * una compra con saldo de wallet
     */
    function wallet_payment($id_user, $id_order, $amount, $options = null)
    {

        // Insert transaction history
        $transaction_data = array(
            'id_user' => $id_user,
            'reference'=>$id_order,
            'description' => "Pago con saldo de billetera de la orden #{$id_order}",
            'usd' => abs($amount),
            'btc' => 0,
            'wallet' => 'USD',
            'rate' => 0,
            'type' => 'wallet_payment',
            'status' => 'done'
        );

        if(!empty( $options )){
            $transaction_data = array_merge( $transaction_data, $options);
        }

        // Insert transaction 
        $transid = $this->ci->transactions->insert($transaction_data, true);
    }


    function purchaseRefund($id_user, $description = "", $amount)
    {
        // Insert transaction history
        $transaction_data = array(
            'id_user' => $id_user,
            'description' =>$description,
            'usd' => $amount,
            'btc' => 0,
            'wallet' => 'USD',
            'rate' => 0,
            'type' => 'purchase_refund',
            'status' => 'done'
        );

        // Insert transaction 
        $transid = $this->ci->transactions->insert($transaction_data, true);
    }

    function add_infinity_bonus($id_user, $bonus = 0, $description = 'Infinite Bonus Benefit') {
        if ($this->add_bonus_transaction($id_user, $bonus, $description, 'infinity')) {
            $this->ci->wallet->add_funds($id_user, $bonus, 'USD');
            $this->ci->subscriptions->add_profit($id_user, $bonus);
        }
    }

    function add_activation_bonus($id_user, $bonus, $description)
    {
        if ($this->add_bonus_transaction($id_user, $bonus, $description, 'activation')) {
            $this->ci->wallet->add_funds($id_user,$bonus);
            $this->ci->subscriptions->add_profit($id_user,$bonus);
        }
    }
    function add_bonus_fullness($id_user)
    {
        //$transaction = $this->ci->transactions->get(['id_user' => $id_user, 'type' => 'bonus_fullness_global']);
        //if(!$transaction){    
            if ($this->add_bonus_transaction($id_user, 300, 'Bono por tener dos referidos en MASTER', 'bonus_fullness_global')) {
                $this->ci->wallet->add_funds($id_user, 300);
                $this->ci->subscriptions->add_profit($id_user, 300);
            }
        //}
    }

    function add_bonus_transaction($id_user, $bonus, $description, $type, $reference = null)
    {
        $transaction = array(
            'id_user' => $id_user,
            'description' => $description,
            'usd' => $bonus,
            'btc' => 0,
            'rate' => 0,
            'type' => $type,
            'date' => date('Y-m-d H:i:s'),
            'reference' => $reference
        );
    
        // Insert transaction history
        return $this->ci->transactions->insert($transaction);
    }

    function bonus_award($id_user, $points, $rank)
    {
        $exits = $this->ci->transactions->get([
            'id_user' => $id_user, 
            'type' => 'bonus_award',
            'reference' => $rank
        ]);
        
        $subscription = $this->ci->subscriptions->get_active($id_user);
        $directs = $this->ci->referral->get_directs_subscription($id_user, $subscription["id_plan"]);
        log_message('DEBUG', 'INFO BONO_500 usuario:'.$id_user.' rango: '.$rank.' hijos: '.var_export($directs, true));

        if(empty($exits) && $directs["left_count"] > 0 && $directs["right_count"] > 0 ){
            $bonus = 0;
            if($rank == 1)
                $bonus = 500;
            if($this->add_bonus_transaction($id_user, $bonus, 'new rank: ' . _rank_title($rank), 'bonus_award', $rank)){
                if($bonus > 0){
                    $this->ci->wallet->add_funds($id_user, $bonus);
                    $this->ci->subscriptions->add_profit($id_user, $bonus);
                }
                return true;
            }
        }
        return false;
        
    }
    function quarterly_participation_bonus($id_user, $bonus, $rank)
    {
        if($this->add_bonus_transaction($id_user, $bonus, 'quarterly participation bonus by rank: ' . _rank_title($rank), 'quarterly', $rank)){
            if($bonus > 0){
                $this->ci->wallet->add_funds($id_user, $bonus);
                $this->ci->subscriptions->add_profit($id_user, $bonus);
            }
            return true;
        }
    }

    public function add_transaction_bonus_upgrade( $id_user, $amount, $options = null ){

        // Insert transaction history
        $transaction_data = array(
           'id_user' => $id_user,
           'reference'=>'',
           'description' => "Bono de upgrade #{$options["reference"]}",
           'usd' => abs($amount),
           'btc' => 0,
           'wallet' => 'USD',
           'rate' => 0,
           'type' => 'upgrade_bonus',
           'status' => 'pending'
       );

       if( !empty( $options ) ){
           $transaction_data = array_merge( $transaction_data, $options );
       }

       // Insert transaction 
       $transid = $this->ci->transactions->insert($transaction_data, true);
       if($transid!= false){
            $this->ci->wallet->add_funds($id_user,abs($amount));
            $this->ci->subscriptions->add_profit($id_user,abs($amount));
       }
       
    }


    public function get_payouts_old($select, $where, $offset=0, $limit=20){
        $where['transactions.type']= 'payout';
        $num_results = $this->ci->transactions->get_rows('count(transactions.id) as num', $where, null, null, null, true)[0]['num'];
        
        $payouts_list = $this->ci->transactions->get_rows($select, $where, $offset, $limit, 'users.id DESC' , true);
        return [
            "num"=>$num_results,
            "results"=>$payouts_list
        ];
    }
    public function get_payouts($select, $where, $offset=0, $limit=20){
        $where['transactions.type']= 'payout';
        
        $payouts_list = $this->ci->transactions->get_rows($select, $where, $offset, $limit, 'users.id DESC' , true);
        return $payouts_list;

    }


    public function get_transactions_by_types($id_user, $where, $limit){
        $transactions = $this->ci->transactions->get_rows("*",
            $where,
            0,
            $limit,
            "id DESC"
        );

        return $transactions;
    }


    public function refund($id_user, $amount, $description)
    {
        // Insert transaction history
        $transaction_data = array(
            'id_user' => $id_user,
            'reference'=>'',
            'description' => $description,
            'usd' => abs($amount),
            'btc' => 0,
            'wallet' => 'USD',
            'rate' => 0,
            'type' => 'refund',
            'status' => 'done'
        );

        // Insert transaction 
        $transid = $this->ci->transactions->insert($transaction_data, true);
    }


    /**
     * Cuando el usuario es preferencial se le debe devolver 
     * el 10% de la compra
     */
    public function add_refund_preferencial_user($id_user, $ammount, $shopping_order_id){


        $this->add_bonus_transaction(
            $id_user, 
            $ammount, 
            "Refund 10% from total buy from  preferencial user #".sprintf('%011d',$shopping_order_id), 
            "refund"
        );

        $this->ci->wallet->add_funds($id_user, $ammount, 'USD');

        
    }

    public function get_by_id( $transaction_id ){
        return $this->ci->transactions->get_row( $transaction_id );
    }

    /**
     * Cuando se aprueve la orden de un usuario external se le agregar
     * el 70% de la mitad de el valor de la compra
     */

    function add_transaction_bonus_sales($id_user, $amount, $options = null){
        
        // Insert transaction history
        $transaction_data = array(
            'id_user' => $id_user,
            'reference'=>'',
            'description' => "Transaccion de orden de compra #{$options["reference"]}",
            'usd' => abs($amount),
            'btc' => 0,
            'wallet' => 'USD',
            'rate' => 0,
            'type' => 'bonus_external_sales',
            'status' => 'done'
        );
 
        // Insert transaction 
        $transid = $this->ci->transactions->insert($transaction_data, true);
        if($transid != false){
             $this->ci->wallet->add_funds($id_user,abs($amount));
             $this->ci->subscriptions->add_profit($id_user,abs($amount));      
        }
    } 


    public function get_sum_by_type_and_user( $id_user, $types = null ){
        return $this->ci->transactions->get_sum_by_type_and_user($id_user, $types);
    }
}