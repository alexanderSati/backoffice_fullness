<?php defined('BASEPATH') OR exit('No direct script access allowed');

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of Currencies
 *
 * @author Juan Manuel
 */
class Currency {
    
    var $id;
    var $name;
    var $symbol;
    var $price_usd;
    var $price_btc;
    var $last_updated;
    var $coins;
    
    function __construct($symbol = 'BTC') {
        
        // just in case url helper has not load yet
        $this->ci =& get_instance();

        // Load models
        $this->ci->load->model('currencies_model','currencies');
        $this->load($symbol);
        
        $this->coins = $this->ci->currencies->get_symbols();
    }
    
    function load($symbol = 'BTC'){
                
        if ($symbol){
            
            $currency = $this->ci->currencies->get(($symbol == 'USD') ? 'BTC' : $symbol);
            foreach($currency as $key => $value){
                $this->$key = $value;
            }
        }        
        return $this;
    }
    
    
    // USD to BTC
    function btc($amount = null){
        if ($amount){
            return ($this->symbol == 'BTC') ? (round($amount / $this->price_usd,8)) : ($amount / $this->price_usd );
        }
        
        return false;
    }
    
    // BTC to USD
    function usd($amount = null){
        if ($amount){
            return ($this->symbol == 'BTC') ? (($amount / $this->price_btc) * $this->price_usd) : ($amount * $this->price_usd);
        }
        return false;
    }
    

    // BTC or USD rate
    function rate($symbol = 'USD'){
        if ($symbol){
            return ($symbol == 'USD') ? $this->price_usd : $this->price_btc;
        }
        return false;
    }
    
}
