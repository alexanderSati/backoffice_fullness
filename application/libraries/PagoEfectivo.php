<?php defined('BASEPATH') OR exit('No direct script access allowed');

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of BinaryTree
 *
 * @author Juan Manuel Pinzon
 */

class PagoEfectivo {
  
  private $token;

  function __construct() {
      
      // just in case url helper has not load yet
      $this->ci =& get_instance();
      $this->ci->load->model("Shopping_order_model");
      $this->ci->load->model("Users_model");
      date_default_timezone_set('America/Bogota');
  }

  public function connect(){
    // Get cURL resource
    $curl = curl_init();

    $fecha = date('Y-m-d').'T'.date('H:i:s').'-05:00';


    $params = json_encode([
      'accessKey'=>ACCESS_KEY, 
      'idService'=>SERVICE_ID, 
      'dateRequest'=>date('Y-m-d').'T'.date('H:i:s').'-05:00',
      'hashString'=>hash("sha256",SERVICE_ID.".".ACCESS_KEY.".".SECRET_KEY.".".$fecha )
    ]);
 
    curl_setopt_array($curl, [
        CURLOPT_RETURNTRANSFER => true,
        CURLOPT_URL => AUTH_URL,
        CURLOPT_USERAGENT => 'Mozilla/5.0 (Windows NT 6.1; Win64; x64; rv:47.0) Gecko/20100101 Firefox/47.0',
        CURLOPT_POST => true,
        CURLOPT_POSTFIELDS => $params,
        CURLOPT_HTTPHEADER=>[
          'Content-Type: application/json',
          'Content-Length: ' . strlen($params)
		]
    ]);
    // Send the request & save response to $resp
    $resp = json_decode(curl_exec($curl), true);
    // Close request to clear up some resources
    curl_close($curl);
    if( $resp["code"] == "100" ){
      $this->token = $resp["data"]["token"];
      return true;
    }
    else{// Mensajes de error que pueden ser devueltos por el API
      log_message('debug', 'INFO error conectando a API '.var_export($resp, true).var_export( json_decode( $params ), true ));
      
      
      switch( $resp["code"] ){
        case "111":
          return "Los datos de la solicitud no son validos.";
        case "112":
          return "Un atributo o elemento requerido no ha sido especificado.";
        case "113":
          return "Un atributo o elemento especificado contiene datos no válidos.";
        case "114":
          return "Un valor de atributo o elemento especificado no cumple con el formato esperado.";
        case "115":
          return "Longitud inválida.";
        case "116":
          return "Rango inválido.";
        case "117":
          return "Rango de fecha inválido.";
        case "508":
          return "Servicio asociado al token es inválido.";
        case "509":
          return "La fecha de expiración supera el límite establecido.";
        case "510":
          return "La fecha de expiración es menor a la fecha actual.";
        case "511":
          return "Usted ha generado un Código CIP que
                  excede el valor máximo permitido en este
                  comercio. Por favor intente con un
                  importe de menor cuantía.";
        case "591":
          return "Expirado.";
        case "592":
          return "Generado.";
        case "593":
          return "Pagado.";
        case "594":
          return "Eliminado.";
        case "595":
          return "No encontrado.";
        case "166":
          return "Error interno del servidor.";
      }
    }
  }


  private function send_request_cips($campos=[]){
    $curl = curl_init();
    $params = [
      'currency'=>'',
      'amount'=>'',
      'transactionCode'=>'',
      'adminEmail'=>'',
      'dateExpiry'=>'',
      'paymentConcept'=>'',
      //'additionalData'=>'',
      'userEmail'=>'',
      'userId'=>'',
      'userName'=>'',
      'UserLastName'=>'',
      'userUbigeo'=>'',
      'userCountry'=>'',
      'userDocumentType'=>'',
      'userDocumentNumber'=>'',
      'userCodeCountry'=>'',
      'userPhone'=>'',
      'service'=>''
    ];
    $params = [];
    $params= json_encode(array_merge( $params, $campos ));
     //$params= json_encode($params+ $campos );
    $params = preg_replace('/\"\d+\.\d+\"/', $campos["amount"],$params);

    curl_setopt_array($curl, [
        CURLOPT_RETURNTRANSFER => true,
        CURLOPT_URL => GENERATE_CIPS_URL,
        CURLOPT_USERAGENT => 'Mozilla/5.0 (Windows NT 6.1; Win64; x64; rv:47.0) Gecko/20100101 Firefox/47.0',
        CURLOPT_POST => true,
        CURLOPT_POSTFIELDS => $params,
        CURLOPT_HTTPHEADER=>[
          'Content-Type: application/json',
          'Content-Length: ' . strlen($params),
          'Accept-Language: en-US',
          'Origin: web',
          'Authorization: Bearer '.$this->token
		]
    ]);

    // Send the request & save response to $resp
    $resp = json_decode(curl_exec($curl), true);
    curl_close($curl);
    if( $resp["code"] == "100" ){
      return $resp["data"];
    }
    else{
      log_message('debug', 'INFO error obteniendo CIPS '.var_export($resp, true)."\n".var_export(json_decode($params, true), true)."\n".var_export( $params, true ));
      return [];
    }
    


  }


  public function get_cips( $orders_id ){
    $orden = $this->ci->Shopping_order_model->get(
      '*',
      ["shopping_order_id"=>$orders_id]
    );
    $user = $this->ci->Users_model->get_user($orden["users_id"]);

    $campos = [
      "currency"=>"PEN",
      "amount"=>number_format(round(floatval($this->ci->cart->total()), 2, PHP_ROUND_HALF_UP),2),
      "transactionCode"=>$orden["shopping_order_id"],
      //"adminEmail"=>$this->ci->settings->site_email,
      "adminEmail"=>"administracion@fullnessglobal.com",
      "dateExpiry"=>date("Y-m-d",strtotime( date()." + 5 days" ))." ".date("H:i:s")."-05:00",
      "paymentConcept"=>"Backoffice FullnessGlobal",
      // "additionalData"=>"",
      "userEmail"=> $user["email"],
      "userId"=>$user["id"],
      "userName"=>$user["first_name"],
      "userLastName"=>$user["last_name"],
      "userLastName"=>$user["last_name"],
      "userUbigeo"=>"150115",
      "userCoutry"=>"PER",
      "userDocumentType"=>"DNI",
      "userDocumentNumber"=>$user["dni"],
      "userCodeCountry"=>"+51",
      "userPhone"=>substr($user["phone"], 0,9) ,
      //"serviceId"=>SERVICE_ID
      "serviceId"=>11
    ];
    

    return $this->send_request_cips( $campos );

  }
}