<?php defined('BASEPATH') OR exit('No direct script access allowed');
/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of Wallet
 *
 * @author Juan Manuel Pinzon
 */
class Plan {
    
    var $plans = array();
    var $ids = array();
    
    public function __construct()
    {
            // just in case url helper has not load yet
            $this->ci =& get_instance();
            
            // Load models
            $this->ci->load->model('users_model');
            $this->ci->load->model('plans_model');
            $this->ci->load->model('subscriptions_model');
            $this->ci->load->library('transaction');
            $this->ci->load->library('referral');
            $this->ci->load->library('wallet');
            $this->ci->load->library('binarytree');
            
            $this->plans =  $this->get_active();
            $this->ids = array_column($this->plans, 'id');
    }
    
    function upgrade($uid = null, $plan = null){

        /*
         * Errors
         * -1 Invalid data
         * -2 Invalid subscription
         * -3 Nothing to update
         * -4 Invalid plan
         * -5 Downgrade not available
         * -6 Not enought funds
         * -7 Payment fail
         */
        
        // Data validation
        if (!$uid || !$plan){
            return -1;
        }
        
        // Get user subscription
        $subscription = $this->ci->subscriptions_model->get_active($uid);
        
        if (!$subscription){
            return -2;
        }
        
        // Plan validation
        if ($subscription['id_plan'] == $plan){
            return -3;
        }
        
        // Prevent Downgrade
        if ($subscription['id_plan'] >= $plan){
            return -5;
        }
        
        // Get new plan data
        $new_plan = $this->get_plan($plan);
        
        // Get user operative wallet funds
        $wallet = $this->ci->wallet->get($uid,'BTC');
        
        // Get volume base difference to pay
        $diff_price = $new_plan['base'] - $subscription['base'];
        
        // Funds validation
        if($wallet < $diff_price){
            return -6;
        }else{
        
            // Create payments plan schedule excluding days
            $plan_schedule = $this->create_interval(
                    $subscription['start_date'], 
                    $new_plan['period_days'],
                    $new_plan['excluded_days']
                );

            $new_subscription = array(
                'id'            => $subscription['id'],
                'id_plan'       => $plan,
                'base'          => $new_plan['base'],
                'period_days'   => $new_plan['period_days'],
                'period_days_left' => $new_plan['period_days']  - ($subscription['period_days'] - $subscription['period_days_left']),
                'min_profit'    => $new_plan['min_profit'],
                'max_profit'    => $new_plan['max_profit'],
                'end_date'      => end($plan_schedule),
                'last_updated'  => date("Y-m-d H:i:s")
            );

            // Susbstract funds from user operative wallet
            $payment = $this->ci->wallet->subtract_funds($uid,$diff_price,'BTC');

            if($payment > 0){
                
                // Insert payment transaccion 
                $this->ci->transaction->addUpgrade($uid,$diff_price);
                
                // Update user subsbcription
               $this->ci->subscriptions_model->update($new_subscription);
               
                // Get referral user row
               $user_ref = $this->ci->referral->get_rules(array('id_user' => $uid));

               // Update susbcription rules 
                $this->ci->referrals_model->update(
                    array('id' => $user_ref['id'], 'id_user' => $uid, 'status' => 'active'),
                    array('value_rule1' => 20, 'value_rule2' => 20)
                );
                
                // Load points to referrer tree
                if($user_ref['id_referral'] != ''){
                    
                    // Get referrer tree
                    $parent_points = $this->ci->binarytree->get_score($user_ref['id_referral']);
                    
                    // Load point to referrer tree from plan upgrade
                    $points = ($parent_points) ? ($parent_points["{$user_ref['referral_side']}_points"] + $new_plan['points_upgrade']) : $new_plan['points_upgrade'];
                    
                    // Update referrer tree row
                    $parent_tree = array(
                        'id_user' => $user_ref['id_referral'],
                        "{$user_ref['referral_side']}_points" => $points
                    );
                    $this->ci->binarytree->update_parent($parent_tree);
                    
                    // Insert transaction points record
                    $this->ci->transaction->addTreePoints($user_ref['id_referral'],$new_plan['points_upgrade'],$user_ref['referral_side']);
                }
                
               return TRUE;
            }else{
                return -7;
            }
        }
        
        return FALSE;
    }
    
    
    
    function get_active(){
        return $this->ci->plans_model->get_active();
    }
    
    
    
    function get_plan($id = null){
        if ($id){
            return $this->ci->plans_model->get_plan($id);
        }
        return false;
    }
    
    
    
    function create_interval($start = "now", $days = 10, $excluded = 'Sun', $holidays = array()){
        
        // Declare initial values
        $interval = array();
        $counter = 0;
        
        $start = new DateTime($start);
        $end = new DateTime("+3 year");
        $excluded = (!is_array($excluded)) ? explode(',',$excluded) : $excluded;
        $end->modify('+1 day');

        // create an iterateable period of date (P1D equates to 1 day)
        $period = new DatePeriod($start, new DateInterval('P1D'), $end);

        foreach($period as $dt) {
                $day = $dt->format('D');
                $curr = $dt->format('Y-m-d');

                // substract excluded days
                if (!in_array($day,$excluded) && !in_array($curr,$holidays)) {
                    // elapsed days
                    if ($counter < $days){
                        $interval[] = $curr;
                        $counter++;
                    }else{
                        break;
                    }
                }
            }
         
        return $interval;
        
    }

    /**
     * Cambia el plan de la subscripcion
     */
    public function activarPlan( $user_id, $plan_id ){
        $subscription = $this->ci->subscriptions_model->get_active( $user_id );
        $plan_id=empty($plan_id)?$subscription["id_plan"]:$plan_id;
        
        $new_plan = $this->get_plan($plan_id);

        // Create payments plan schedule excluding days
        $plan_schedule = $this->create_interval(
            $subscription['start_date'], 
            $new_plan['period_days'],
            $new_plan['excluded_days']
        );

        $new_subscription = array(
            'id'            =>$subscription['id'],
            'id_plan'       => $plan_id,
            'base'          => $new_plan['base'],
            'period_days'   => $new_plan['period_days'],
            'period_days_left' => $new_plan['period_days']  - ($subscription['period_days'] - $subscription['period_days_left']),
            'min_profit'    => $new_plan['min_profit'],
            'max_profit'    => $new_plan['max_profit'],
            'end_date'      => end($plan_schedule),
            'last_updated'  => date("Y-m-d H:i:s"),
            'preferencial_user' => '0',
            'id_user'=>$user_id
        );

        
        $this->ci->subscriptions_model->update($new_subscription);

        $padre = $this->ci->referral->direct_parents($user_id, 1, true);

        log_message('debug',"INFO PUNTOS_BINARIOS_ACTIVACION usuario:".$user_id." plan:".$plan_id." puntos:".$new_plan['points']. " padre:".(empty($padre)? "NO": $padre[0]));
        if( !empty( $padre ) ){
            $reg_referral = $this->ci->referrals->get(["id_user"=>$user_id]);
            if( $reg_referral["referral_side"] == "left" ){
                $this->ci->binarytree->add_binary_points($padre[0], $new_plan['points'], "left");
            }
            else if($reg_referral["referral_side"] == "right"){
    
                $this->ci->binarytree->add_binary_points($padre[0], $new_plan['points'], "right");
            }
        }

    }
    
            
    
}