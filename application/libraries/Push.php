<?php

defined('BASEPATH') OR exit('No direct script access allowed');

/**
 * Pusher for CodeIgniter
 *
 * Simple library that wraps the Pusher PHP library (https://github.com/pusher/pusher-http-php)
 * and give access to the Pusher methods using regular CodeIgniter syntax.
 *
 * This library requires that Pusher PHP Library is installed with composer, and that CodeIgniter
 * config is set to autoload the vendor folder. More information in the CodeIgniter user guide at
 * http://www.codeigniter.com/userguide3/general/autoloader.html?highlight=composer
 *
 */

require FCPATH. '/vendor/autoload.php';

Class Push extends Pusher{

    function __construct() {
        
        $this->ci = & get_instance();

        // Load config
        $this->ci->load->config('pusher');

        // Get config variables
        $this->app_id = $this->ci->config->item('pusher_app_id');
        $this->app_key = $this->ci->config->item('pusher_app_key');
        $this->app_secret = $this->ci->config->item('pusher_app_secret');
        $this->app_cluster = $this->ci->config->item('pusher_app_cluster');

        $options = array(
            'cluster' => $this->app_cluster,
            'encrypted' => true
        );
        
        parent::__construct($this->app_key, $this->app_secret, $this->app_id, $options);

    }

}
