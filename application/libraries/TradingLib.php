<?php

defined('BASEPATH') OR exit('No direct script access allowed');

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of Trading
 *
 * @author Julian David Serna Echeverri
 * @author Juan Manuel Pinzon
 */
class TradingLib {

    var $errors;
    var $user;

    const FEE = 0.8;
    const DECIMALS = 8;

    static $allowedActions;
    static $allowedCoins;

    function __construct() {
        // just in case url helper has not load yet
        $this->ci = & get_instance();

        $this->user = (object) $this->ci->session->userdata('logged_in');

        $this->ci->load->language('trading');

        $this->ci->load->library('currency');
        $this->ci->load->library('wallet');
        $this->ci->load->library('transaction');
        $this->ci->load->library('push');

        // Load models
        $this->ci->load->model('TradingM_model');

        $this->history = new TradingM_model('history');
        $this->markets = new TradingM_model('markets');
        $this->profits = new TradingM_model('profits');

        // Get virtual coins
        self::$allowedCoins = array(
            'USD' => array('label' => "Bitcoin", 'allowed' => false),
            'BTC' => array('label' => "BTC Operative", 'allowed' => false),
            'XMR' => array('label' => "XMR - Monero", 'allowed' => true),
            'ETH' => array('label' => "ETH - Etherum", 'allowed' => true),
            'LTC' => array('label' => "LTC - Litecoin", 'allowed' => true),
            'TRUMP' => array('label' => "TRUMP - TrumCoin", 'allowed' => true),
            'DASH' => array('label' => "DASH - Dashcoin", 'allowed' => true),
            'SLR' => array('label' => "SLR - SolarCoin", 'allowed' => true
            )
        );

        self::$allowedActions = array('sell', 'buy', 'list');
    }

    function buy(array $trading) {
        // Load currencies
        $usd = new Currency();
        $btc = new Currency('BTC');

        // Get user wallets
        $Coin_funds = $this->ci->wallet->get($this->user->id, $trading['coin']);
        $BTC_funds = $this->ci->wallet->get($this->user->id, 'BTC');
        
        // Trading Total and fee
        $USD_fee = $btc->usd($trading['btc_fee']);
        $USD_total = $btc->usd($trading['btc_total']);
        
        /*
         * Validations
         */
        if (($trading['status'] === 'inactive') || ($trading['type'] !== 'buy')) {
            $this->errors[] = $this->ci->lang->line('tradig_access_denied');
        }

        // If user have enought funds in coin wallet
        if ($trading['amount'] > $Coin_funds) {
            $this->errors[] = $this->ci->lang->line('tradig_not_funds_wallet');
        }
        
        // If user have enought funds in operative wallet
        if ($USD_fee > $BTC_funds) {
            $this->errors[] = $this->ci->lang->line('tradig_not_funds_btc_wallet');
        }
        
        // Action not allowed when seller and buyer are the same
        if($this->user->id === $trading['id_user']){
            $this->errors[] = $this->ci->lang->line('tradig_access_denied');
        }
        
        if(!$this->errors){
            
            /*
             * Seller (Vendedor y/o usuario actual quien vende)
             */
            
            // Substrac fee from BTC wallet
            $this->ci->wallet->subtract_funds($this->user->id, $USD_fee, 'BTC');
            // Substract amount from wallet
            $this->ci->wallet->subtract_funds($this->user->id, $trading['amount'], $trading['coin']);
            
            // Add transaction seller transaction in dashboard
            $this->ci->transaction->tradingSell($this->user->id, $trading);
            
            // Add transaction seller fee in dashboard
            $this->ci->transaction->tradingFee($this->user->id, $trading);
            
            // Paid sell offer (Pagar en BTC Operativa lo que vendio)
            $this->ci->wallet->add_funds($this->user->id, $USD_total, 'BTC');
            
            /*
             * Buyer process (Comprador, quien dejo el dinero en deposito)
             */
            
            // Load amount to buyer coin wallet
            $this->ci->wallet->add_funds($trading['id_user'], $trading['amount'], $trading['coin']);
            
            // Update transaction to done
            $this->ci->transaction->updateStatus($trading['id_transaction'],'done');


            /* 
             * Site process
             */
            
            // Load profit to admin dashboard
            $this->saveProfit($trading['id'], $USD_fee, $trading['btc_fee']);

            // Remove it from Live Market
            $this->unpublishLiveMarket($trading['id']);

            // Add it to market to history
            $this->moveToHistory($trading['id']);
            
            return true;
            
        }
            
        
        return false;
    }

    function sell(array $trading) {
        if ($trading) {
            
            // Load currencies
            $usd = new Currency();
            $btc = new Currency('BTC');
            
            // Trading total + fee
            $BTC_total = round($trading['btc_total'] + $trading['btc_fee'], self::DECIMALS);
            $USD_total = $btc->usd($BTC_total);
            
            // Trading total + fee
            $BTC_fee = $trading['btc_fee'];
            $USD_fee = $btc->usd($BTC_fee);

            // Get user wallet 
            $USD_funds = $this->ci->wallet->get($this->user->id, 'BTC');
            $BTC_funds = $usd->btc($USD_funds);

            // Validations
            if (($trading['status'] === 'inactive') || ($trading['type'] !== 'sell')) {
                $this->errors[] = $this->ci->lang->line('tradig_access_denied');
            }

            // If user have enought funds in operative wallet
            if ($BTC_total > $BTC_funds) {
                $this->errors[] = $this->ci->lang->line('tradig_not_funds_btc_wallet');
            }
            
            // Action not allowed when seller and buyer are the same
            if($this->user->id === $trading['id_user']){
                $this->errors[] = $this->ci->lang->line('tradig_access_denied');
            }
            
            if (!$this->errors) {

                /*
                 *  Seller process
                 */
                // Load bitcoins to seller operative wallet (Pay to seller whitout fee)
                $this->ci->wallet->add_funds($trading['id_user'], $btc->usd($trading['btc_total']), 'BTC');
                
                // Update transaction to done
                $this->ci->transaction->updateStatus($trading['id_transaction'],'done');
                
                     
                /*
                 *  Buyer process
                 */
                
                // Substract fee from BTC Operative
                $this->ci->wallet->subtract_funds($this->user->id, $USD_total, 'BTC');
                // Load Coin amount to symbol wallet
                $this->ci->wallet->add_funds($this->user->id, $trading['amount'], $trading['coin']);
                // Add transactions (fee and history)
                $this->ci->transaction->tradingPaid($this->user->id,$trading);
                $this->ci->transaction->tradingFee($this->user->id, $trading);
                
                /*
                 * Site process
                 */
                
                // Load profit to admin dashboard
                $this->saveProfit($trading['id'], $USD_fee, $BTC_fee);
                
                // Remove it from Live Market
                $this->unpublishLiveMarket($trading['id']);
                
                // Add it to market to history
                $this->moveToHistory($trading['id']);
                
                return true;
            }
        }
        return false;
    }

    /**
     * Publish a trading row in markets
     * @param string $action sell or buy
     * @param string $symbol symbol in allowed coins
     * @param decimal $amount 
     * @return type
     */
    function publish($action = null, $symbol = null, $amount = 0, $btc_price = 0) {

        // Load currencies and users wallets
        $btc = new Currency('BTC');
        $BTC_funds = $this->ci->wallet->get($this->user->id, 'BTC');
        $Coin_funds = $this->ci->wallet->get($this->user->id, $symbol);

        // Essential validations
        if (!$action || (!in_array($action, self::$allowedActions))) {
            $this->errors[] = $this->ci->lang->line('tradig_invalid_action');
        }

        if (!$symbol || (!array_key_exists($symbol, self::$allowedCoins))) {
            $this->errors[] = $this->ci->lang->line('tradig_invalid_coin');
        }

        if ($amount <= 0) {
            $this->errors[] = $this->ci->lang->line('tradig_invalid_amount');
        }

        if ($btc_price <= 0) {
            $this->errors[] = $this->ci->lang->line('tradig_invalid_price');
        }

        if (!$this->errors) {

            // Transaction amounts
            $btc_total = round($amount * $btc_price, self::DECIMALS);

            // Fee
            $btc_fee = round(($btc_total * (self::FEE / 100)), self::DECIMALS);
            $usd_fee = $btc->usd($btc_fee);

            // Total with Fee 
            $btc_fee_total = number_format(($btc_total  + $btc_fee), self::DECIMALS);
            $usd_total = $btc->usd($btc_fee_total);

            $row = array(
                'id_user' => $this->user->id,
                'type' => $action,
                'coin' => $symbol,
                'amount' => $amount,
                'btc_price' => $btc_price,
                'btc_total' => $btc_total,
                'btc_fee' => $btc_fee,
                'btc_fee_total' => $btc_fee_total,
            );

            // Only check funds when sell action was selected
            if ($action == 'sell') {
                if ($Coin_funds < $amount) {
                    $this->errors[] = $this->ci->lang->line('tradig_not_funds_wallet');
                }
            }
            
            if ($action == 'buy') {
                if ($BTC_funds < $usd_total ) {
                    $this->errors[] = $this->ci->lang->line('tradig_not_funds_btc_wallet');
                }
            }

            if ($BTC_funds < $usd_fee) {
                $this->errors[] = $this->ci->lang->line('tradig_not_funds_btc_wallet');
            }

            if (!$this->errors) {

                // Get new market ID
                $row['id'] = $this->markets->insert($row);
                if ($row['id']) {

                    if ($action === 'sell') {

                        // Substract amount from coin wallet
                        $this->ci->wallet->subtract_funds($this->user->id, $amount, $symbol);

                        // Substract fee from BTC Operative
                        $this->ci->wallet->subtract_funds($this->user->id, $usd_fee, 'BTC');

                        // Load fee transaction into user hisory
                        $this->ci->transaction->tradingFee($this->user->id,$row);

                    } elseif ($action == 'buy') {
                        // Substract total btc amount from BTC Operative
                        $this->ci->wallet->subtract_funds($this->user->id, $usd_total, 'BTC');
                    }
                    
                    // Load profit to admin dashboard
                    $this->saveProfit($row['id'], $usd_fee, $btc_fee);

                    // Create user dashboard transaction record and update market transaction
                    $row['id_transaction'] = $this->ci->transaction->trading($row);
                    $this->markets->update($row['id'], $row);

                    // Send to live markets
                    $this->pushLiveMarket('publish', $row);

                    return $row['id'];
                }
            }
        }

        return false;
    }

    /**
     * Remove a trading row given id and refund amounts
     * @param type $id
     * @return boolean  True when success
     */
    function unpublish($id = null) {
        if ($id) {

            $trading = $this->markets->get_row($id);
            
            if ($trading) {

                // Validations
                if ($trading['id_user'] !== $this->user->id || $trading['status'] == 'inactive') {
                    $this->errors[] = $this->ci->lang->line('tradig_access_denied');
                }

                // Return funds to wallets
                if ($trading['type'] == 'sell') {
                    $this->ci->wallet->add_funds($trading['id_user'], $trading['amount'], $trading['coin']);
                }

                if ($trading['type'] == 'buy') {
                    $btc = new Currency('BTC');
                    $this->ci->wallet->add_funds($trading['id_user'], $btc->usd($trading['btc_total']), 'BTC');
                }
                
                // Update transaction to done
                $this->ci->transaction->updateStatus($trading['id_transaction'],'cancelled');
                
                // Remove it from Live Market
                $this->unpublishLiveMarket($trading['id']);
                
                // Add it to market to history
                $this->moveToHistory($trading['id']);
                
                return true;
            } else {
                $this->errors[] = $this->ci->lang->line('tradig_not_found');
            }
        }
        return false;
    }

    /**
     * Return market given a coin symbol and action
     * @param string $coin
     * @param string $type
     * @return array or false
     */
    function markets($coin = null, $type = null) {

        if ($coin) {
            $args['coin'] = strtoupper($coin);
            $args['status'] = 'active';

            if (!empty($type)) {
                $args['type'] = $type;
            }
            return $this->markets->get($args);
        }

        return false;
    }

    /**
     * Return all records given a coin
     * @param string $coin
     * @param int $limit default 1000
     * @return array or false
     */
    function history($coin = null, $limit = 1000) {
        if ($coin) {
            $args['coin'] = strtoupper($coin);
            return $this->history->get($args, $limit,'archived');
        }

        return false;
    }
    

    /**
     * Save profit in database for admin dashobard
     * @param type $id_trading
     * @param double $usd
     * @param double $btc
     */
    function saveProfit($id_trading, $usd, $btc) {
        $profit = array(
            'id_trading' => $id_trading,
            'profit_usd' => $usd,
            'profit_btc' => $btc,
        );

        $this->profits->insert($profit);
    }

    
    /**
     * Validate if coin symbol is allowed
     * @param string $symbol
     * @return boolean
     */
    function isAllowed($event = null, $object = null) {
        if ($event && $object) {

            switch ($event) {
                case 'coin':
                    $coin = strtoupper($object);
                    if (!isset(self::$allowedCoins[$coin])) {
                        return false;
                    }
                    if (self::$allowedCoins[$coin]['allowed'] != true) {
                        return false;
                    }
                    break;
                case 'action':
                    if (!in_array($object, self::$allowedActions)) {
                        return false;
                    }
                    break;

                default:
                    return false;
            }
        }
        return true;
    }
    
    
    /**
     * Purchase (sell, buy) from browser
     * @param int $id
     * @return boolean
     */
    function processLiveMarket($id = null) {
        if ($id) {

            $trading = $this->markets->get_row($id);

            if ($trading) {
                switch ($trading['type']) {
                    case 'sell':
                        return $this->sell($trading);
                    case 'buy':
                        return $this->buy($trading);
                    default:
                        return false;
                }
            } else {
                $this->errors[] = $this->ci->lang->line('tradig_not_available');
            }
            return false;
        }
    }

    /**
     * Send row data to live markets
     * @param type $data
     */
    function pushLiveMarket($action = null, $data = null) {
        if ($action && $data) {

            switch ($action) {
                case 'publish':
                    $trading = array(
                        'id' => $data['id'],
                        'type' => $data['type'],
                        'coin' => $data['coin'],
                        'amount' => $data['amount'],
                        'btc_price' => number_format($data['btc_price'], self::DECIMALS),
                        'btc_total' => number_format(($data['btc_total'] + $data['btc_fee']), self::DECIMALS),
                    );
                    $this->ci->push->trigger($data['coin'], 'publish', $trading);
                    break;

                default:
                    break;
            }
        }
    }
    
    /**
     * Remove row data from live markets
     * @param int $id
     * return boolean
     */
    function unpublishLiveMarket($id = null) {
        if ($id) {
              $trading = $this->markets->get_row($id);
                $data = array(
                    'id' => $id,
                    'type' => $trading['type'],
                );
                return $this->ci->push->trigger($trading['coin'], 'unpublish', $data);
        }
        return false;
    }
    
    
    /**
     * Move market to history given its id
     * @param type $id
     * @return boolean
     */
    function moveToHistory($id = null){
        if($id){
            
            $trading = $this->markets->get_row($id);
            
            if($trading){
                // Remove it from markets table
                $this->markets->delete($id);

                // Data handle to be migrated
                $trading['id_market'] = $trading['id'];
                $trading['archived'] = date("Y-m-d H:i:s");
                $trading['status'] = 'inactive';
                unset($trading['id']);

                //Insert data in historiy table
                $row =$this->history->insert($trading);

                $history = array(
                    'id' => $trading['id_market'],
                    'type' => $trading['type'],
                    'amount' => number_format($trading['amount'],self::DECIMALS),
                    'btc_total' => number_format($trading['btc_total'],self::DECIMALS),
                    'time' => date("H:i:s")
                );
                
                // Send to market live event
                $this->ci->push->trigger($trading['coin'], 'history', $history);
                
                return $row;
            }
        }
        return false;        
    }
    
    /**
     * Public function to return calculate fee
     * @param double $amount
     * @return double
     */
    function getFee($amount = 0){
        if($amount){
            return ($amount * (self::FEE / 100));
        }
    }

    
    /**
     * Custom admin query for general resume of profits
     * @return type
     */
    function adminResume(){
        $query = "SELECT DATE_FORMAT(`date`, '%d-%m-%Y') as 'date' , SUM(`profit_usd`) as 'profit_usd' , SUM(`profit_btc`) as 'profit_btc', COUNT(*) as 'amount' FROM `trading_profits` GROUP BY DATE_FORMAT(`date`, '%d-%m-%Y') ORDER BY `date` DESC";
        return $this->profits->query($query);
    }

}
