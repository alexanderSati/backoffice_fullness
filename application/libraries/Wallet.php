<?php defined('BASEPATH') OR exit('No direct script access allowed');
/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of Wallet
 *
 * @author Juan Manuel Pinzon
 */
class Wallet {
    
    const DECIMALS = 8;
   
    
    public function __construct()
    {
            // just in case url helper has not load yet
            $this->ci =& get_instance();
            
            
            // Load models
            $this->ci->load->model('users_model','users');
            $this->ci->load->model('wallets_model','wallets');
            $this->ci->load->library('currency');
            
    }
    
    /**
     * @param int $id (user_id)
     * @param String [wallet] (USD|BTC|TRUMP .....)
     */
    function get($id = null,$wallet = null){
        if ($id && $wallet){
            return $this->ci->wallets->get($id,$wallet);
        }
    }
    
    function create($id = null){
        if($id && is_numeric($id)){
            $this->ci->wallets->insert($id);
        }        
    }
    
    function add_funds($user = 0,$amount = 0,$wallet = 'USD'){
        
        /*
         * Errors list
         * -1 Invalid amount
         * -2 Invalid user
         * -3 Invalid wallet symbol
         */
        
        $symbol = strtoupper($wallet);
        $amount = abs($amount);
         
        if (!$user){
            return -1;
        }
        
       // If wallet exists and values are valid
        if ($amount <= 0 || !is_numeric($amount)){
            return -2;
        }
        
        if(!array_key_exists($symbol, $this->ci->wallets->table->fields)){
            return -3;
        }
        
        // Get wallet given user id
        $c_amount = round($this->ci->wallets->get($user,$wallet),self::DECIMALS);
        $n_amount = $c_amount + round($amount,self::DECIMALS);

        return $this->ci->wallets->update($user,array($wallet => $n_amount ));
                
    }
    
    
    function subtract_funds($user = 0,$amount = 0,$wallet = 'USD'){
        
        /*
         * Errors list
         * -1 Invalid user
         * -2 Invalid amount
         * -3 Invalid wallet symbol
         */
        
        $symbol = strtoupper($wallet);
        $amount = abs($amount);
         
        if (!$user){
            return -1;
        }
        
       // If wallet exists and values are valid
        if ($amount <= 0 || !is_numeric($amount)){
            return -2;
        }
        
        if(!array_key_exists($symbol, $this->ci->wallets->table->fields)){
            return -3;
        }
        
        // Get wallet given user id
        $c_amount = round($this->ci->wallets->get($user,$wallet),self::DECIMALS);
        $n_amount = $c_amount - round($amount,self::DECIMALS);

        return $this->ci->wallets->update($user,array($wallet => $n_amount ));
                
    }
}