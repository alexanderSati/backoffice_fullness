<?php defined('BASEPATH') OR exit('No direct script access allowed');

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of BinaryTree
 *
 * @author Juan Manuel Pinzon
 */

class Pagovisanet {
  
  private $token;

  function __construct(){
    $this->CI = & get_instance();
    $this->token = $this->connect();
    $this->CI->load->model("PaymentMethod_model","payment_method");
    $this->CI->load->model("Users_model","users_model");


  }

  private function connect(){
    // Get cURL resource

    $header = array("Content-Type: application/json");
    $ch = curl_init();
    curl_setopt($ch, CURLOPT_URL,URL_AUTH_VISANET);
    curl_setopt($ch, CURLOPT_HTTPAUTH, CURLAUTH_BASIC);
    curl_setopt($ch, CURLOPT_USERPWD, VISANET_USER.":".VISANET_PASSWORD);
    curl_setopt($ch, CURLOPT_HEADER, FALSE);
    curl_setopt($ch, CURLOPT_HTTPHEADER, $header);
    curl_setopt($ch, CURLOPT_CUSTOMREQUEST, "GET");
    curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, FALSE);
    curl_setopt($ch, CURLOPT_RETURNTRANSFER, TRUE);
    $key = curl_exec($ch);

    return $key;
  }


  public function start_session($amount,$id_user ){
    $ch = curl_init();
    $usuario = $this->CI->users_model->get_user($id_user);

    $header = ["Content-Type: application/json","Authorization: ".$this->token];
    $request_body = json_encode([
      "amount"=>$amount,
      "channel"=>"web",
      "antifraud"=>[
        "clientIp"=>$_SERVER['HTTP_CLIENT_IP'],
        "merchantDefineData"=>[
          "MDD4"=>$usuario["email"],
          "MDD32"=>$usuario["id"],
          "MDD33"=>"DNI",
          "MDD34"=>$usuario["dni"],
          "MDD70"=>1,
          "MDD75"=>"Registrado",

        ]
      ]
    ]);
    
    log_message('DEBUG','INFO: '.$request_body);

    curl_setopt($ch, CURLOPT_URL, URL_SESSION_VISANET);
    curl_setopt($ch, CURLOPT_HTTPAUTH, CURLAUTH_BASIC);
    curl_setopt($ch, CURLOPT_HEADER, FALSE);
    curl_setopt($ch, CURLOPT_HTTPHEADER, $header);
    curl_setopt($ch, CURLOPT_POST, TRUE);
    curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, FALSE);
    curl_setopt($ch, CURLOPT_POSTFIELDS, $request_body);
    curl_setopt($ch, CURLOPT_RETURNTRANSFER, TRUE);
    $response = curl_exec($ch);


    $resp = json_decode($response, true);

    return $resp;
  }


  public function register($id_user, $shopping_order_id, $token, $session_key, $expiration, $amount){
    $this->CI->payment_method->register_visanet($id_user, $shopping_order_id, $token, $session_key, $expiration, $amount);
  }

  public function get_token(){
    return $this->token;
  }


  public function get_session($id_user, $shopping_order_id, $amount){
    return $this->CI->payment_method->get_session_visanet( $id_user, $shopping_order_id,$amount );
  }


  public function get_authorization_payment($shopping_order_id){
    $header =[
      "Content-Type: application/json",
      "Authorization: ".$this->token
    ];

    $pago = $this->CI->payment_method->obtenerRegistroPorId(["visanet.shopping_order_id"=>$shopping_order_id]);

    $request_body = [
      "antifraud"=>null,
      "captureType"=>"manual",
      "channel"=>"web",
      "countable"=>false,
      "order"=>[
        "amount"=>number_format($pago["amount"],2),
        "tokenId"=>$pago["code"],
        "purchaseNumber"=>$shopping_order_id,
        //"purchaseNumber"=>1200008,
        "currency" =>"PEN"
      ]
    ];



    $ch = curl_init();
    curl_setopt($ch, CURLOPT_URL, URL_PAY_AUTHORIZATION_VISANET);
    curl_setopt($ch, CURLOPT_HTTPAUTH, CURLAUTH_BASIC);
    curl_setopt($ch, CURLOPT_HEADER, FALSE);
    curl_setopt($ch, CURLOPT_HTTPHEADER, $header);
    curl_setopt($ch, CURLOPT_POST, TRUE);
    curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, FALSE);
    curl_setopt($ch, CURLOPT_POSTFIELDS, json_encode($request_body));
    curl_setopt($ch, CURLOPT_RETURNTRANSFER, TRUE);
    $response = curl_exec($ch);
    
    log_message('DEBUG','INFO: '.json_encode($request_body)."\n".$response);
    
    return ["status"=>curl_getinfo($ch, CURLINFO_HTTP_CODE), "data"=>json_decode( $response, true )];
  }
}