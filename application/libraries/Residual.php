<?php defined('BASEPATH') OR exit('No direct script access allowed');

class Residual {
    var $start;
    var $end;

    function __construct() {
        
        // just in case url helper has not load yet
        $this->ci =& get_instance();
        
        $this->ci->load->model('residual_model');
        $this->ci->load->library('referral');
        $this->ci->load->library('rank');
        $this->ci->load->model('users_model');
        $this->start = date('Y-m-01');
        $this->end = date('Y-m-t');
    }


    public function query( $sql ){
        return $this->db->query($sql);
    }

    public function add_infinity_bonus($id_user, $points)
    {
        $insert = [
            'id_user' => $id_user,
            'points' => $points
        ];
        $this->ci->users_model->update_infinity_bonus($id_user);
        return $this->ci->residual_model->insert_settlement($insert);
    }

    public function add_points($id_user, $points, $description){
        $insert = [
            'id_user' => $id_user,
            'points' => $points,
            'description' => $description
        ];
        return $this->ci->residual_model->insert($insert);
    }

    public function get_score($id_user, $child = false)
    {
        
        $results = $this->ci->residual_model->get('SUM(points) AS total_points', ['id_user' => $id_user], $this->start, $this->end);
        return (int) $results['total_points'];
    }
    
    public function get_residual_table($id_user){
        $results = $this->ci->residual_model->get('*', ['id_user' => $id_user]);
        return !empty($results[0]) ? $results : array($results);
    }
    protected function percentage_profit(int $rank)
    {
        switch ($rank) {
            case 1:
                $percent = 9;
                break;
            case 2:
                $percent = 13;
                break;
            case 3:
                $percent = 17;
                break;
            case 4:
                $percent = 21;
                break;
            case 5:
            case 6:
            case 7:
            case 8:
            case 9:
            case 10:
            case 11:                
                $percent = 25;
                break;
            default:
                $percent = 0;
                break;
        }
        return $percent;
    }

    protected function _calculate_rank($pp, $prepare_points_referrals)
    {
        
        $rank_check = null;
        if (!empty($prepare_points_referrals)){
            #echo "<pre>prepare_points_referrals:";
            #print_r($prepare_points_referrals);
            #echo "</pre>";
            do {
                if (is_null($rank_check)){
                    foreach ($prepare_points_referrals as $key => $referral) {
                        if ($referral['gp'] > 0){
                            $gp += $referral['gp'];
                        }else{
                            $gp += $referral['pp'];
                        }
                        
                        $sum +=  $referral['pp'] + $referral['gp'];
                    }
                    $points = $pp + $sum;
                    $total_pg = $pp + $gp;
                    $rank = $this->rank_by_points($points);
                }else{
                    $rank = $rank_check;
                }
                $max_line = $this->maximum_line($rank, $points);
                if ($rank > 0){
                    $sum = 0;
                    foreach ($prepare_points_referrals as $key => $referral) {
                        if ($referral['gp'] > 0){
                            $score = $referral['gp'];
                        }else{
                            $score = $referral['pp'];
                        }
                        $la = ($score > $max_line ? $max_line : $score);
                        $sum += $la;
                    }
                    
                    $points = $pp + $sum;
                }
                $rank_check = $this->rank_by_points($points);
            } while($rank != $rank_check);
    
            return [
                'rank' => $rank,
                'pg' => $total_pg,
                'score' => $points,
                'max_line' => $max_line
            ];
        }else{
            return [
                'rank' => 0,
                'pg' => 0,
                'score' => $pp,
                'max_line' => 0
            ];
        }
    }
    
    protected function rank_by_points($points)
    {
        switch ($points) {
            case ($points >= 5000000):
                $rank = 11; # grandblack diamond
                break;
            case ($points >= 2000000):
                $rank = 10; # grand diamond
                break;
            case ($points >= 1000000):
                $rank = 9; # black diamond
                break;
            case ($points >= 500000):
                $rank = 8; # red diamond
                break;
            case ($points >= 300000):
                $rank = 7; # triple diamante
                break;
            case ($points >= 120000):
                $rank = 6; # doble diamante 
                break;
            case ($points >= 60000):
                $rank = 5; # diamante 
                break;
            case ($points >= 30000):
                $rank = 4; # ruby
                break;
            case ($points >= 15000):
                $rank = 3; # senior
                break;
            case ($points >= 5000):
                $rank = 2; # executivo
                break;    
            case ($points >= 2000):
                $rank = 1; # assistantprepare_points
                break;
            default:
                $rank = 0;        
                break;
        }
        return $rank;
    }

    protected function prepare_points($id_user)
    {  
        $pp = 0;
        $gp = 0;
        $score = 0;
        $pp = $this->get_score($id_user);
        
        

        $prepare_points_referrals = $this->prepare_points_referrals($id_user);
        $calculated = $this->_calculate_rank($pp, $prepare_points_referrals);
        $percentage_profit = $this->percentage_profit($calculated['rank']);

        $bonus = ($pp*$percentage_profit)/100;
        
        $prepare_points = [
            'id_user' => $id_user,
            'pp' => $pp,
            'gp' => $calculated['pg'],
            'score' => $calculated['score'],
            'rank' => $calculated['rank'],
            'max_line' => $calculated['max_line'],
            'percentage_profit' => $percentage_profit, 
            'prepare_points_referrals' => $prepare_points_referrals,
            'bonus' => $bonus
        ];


        #echo "Entra a calcular el bonus <br>";
        #echo "<pre>Bonus del usuario {$id_user}: ";
        #print_r($prepare_points_referrals);
        #echo "</pre>";

        
        
        $prepare_points['bonus'] += $this->prepare_bonus($prepare_points_referrals, $prepare_points['rank'], $prepare_points['percentage_profit']);

        return $prepare_points;
    }

    protected function prepare_bonus($prepare_points_referrals, $rank, $percentage_profit, $bonus = 0)
    {
        foreach ($prepare_points_referrals as $key => $referral) {
            $percent_tpm = 0;
            if($rank > $referral['rank']){
                if ($referral['rank'] > 0){
                    $percent_tpm = $percentage_profit - $referral['percentage_profit'];
                }else{
                    $percent_tpm = $percentage_profit;
                }

                #echo "El bonus lo genera el referido {$referral['id_user']} con los puntos " . ($referral['gp'] + $referral['pp']) ." con porcentaje {$percent_tpm} genera un bonus de " . ((($referral['gp'] + $referral['pp']) * $percent_tpm) / 100) ."<br>";
                if ($referral['rank'] == 0){
                    $bonus +=  ($referral['pp'] * $percent_tpm) / 100;
                }else{
                    $bonus +=  ($referral['gp'] * $percent_tpm) / 100;
                }
                
            }
            if (!empty($referral['prepare_points_referrals']) && $referral['rank'] == 0){
                $bonus += $this->prepare_bonus($referral['prepare_points_referrals'], $rank, $percentage_profit);
            }
        }
        
        return $bonus;
    }

    protected function prepare_points_referrals($id_user)
    {        
        $prepare_points_referrals = [];
        $referrals = $this->ci->referral->get_referrals($id_user);
        foreach ($referrals as $key => $referral) {
            array_push($prepare_points_referrals, $this->prepare_points($referral['id_user']));
        }
        return $prepare_points_referrals;
    }

    public function unilevel($id_user)
    {
        $prepare_points = $this->prepare_points($id_user);
        return $prepare_points;
    }







    
    public function infinity_bonus($id_user)
    {
        $prepare_points = $this->prepare_points($id_user);
        $prepare_points_referrals = $this->prepare_points_referrals($id_user);
        
        foreach ($prepare_points_referrals as $key => $referral) {
            $percent_tpm = 0;
            if($prepare_points['rank'] > $referral['rank']){
                if ($referral['rank'] > 0){
                    $percent_tpm = $prepare_points['percentage_profit'] - $referral['percentage_profit'];
                    $prepare_points['points'] +=  ($referral['points'] * $percent_tpm) / 100;
                }else{
                    $prepare_points['points'] +=  $referral['max_line'] > 0 ? $referral['max_line'] : $referral['total_points'];
                }
            }else{
                $prepare_points['points'] +=  $referral['max_line'] > 0 ? $referral['max_line'] : $referral['total_points'];
            }
        }

        return $prepare_points;
    }
    
    protected function maximum_line($rank)
    {
        $maximum_line = [
            0,1400,3000,9000,15000,30000,500000,120000,200000,400000,600000,1000000
        ];
        return $maximum_line[$rank];
    }

    public function get_allowed_users()
    {
        $results = $this->ci->residual_model->get('residual.id_user', [], $this->start, $this->end, $limit = 100, $order_by = 'id_user', $order = 'DESC', true, true);

        $return = [];
        foreach ($results as $key => $result) {
            array_push($return, $result['id_user']);
        }
        return $return;
    }

    /**
     * Carlos Aguirre 2019-09-13 10:53:12
     * Obtiene el monto de liquidacion de residual para un 
     * usuario enviado por parametro
     * @param int $id_user ( id del usuario )
     * @return float ( monto de liquidacion )
     */
    public function get_amount_liquidation_residual($id_user){
        $this->ci->load->library("transaction");
        $transaction = $this->ci->transaction->get_transactions_by_types($id_user,[
            "id_user"=>$id_user,
            "type"=>'infinity',
            "status"=>'done',
            "YEAR(`date`)"=>date('Y'),            
            "MONTH(`date`)"=>date('m')
        ],10);

        if( empty($transaction) ){
            return 0;
        }
        else{
            return $transaction[0]['usd'];
        }
    }
    
}