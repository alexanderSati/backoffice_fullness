<?php defined('BASEPATH') OR exit('No direct script access allowed');

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of BinaryTree
 *
 * @author Juan Manuel Pinzon
 */

class Binarytree {
    
    function __construct() {
        
        // just in case url helper has not load yet
        $this->ci =& get_instance();

        // Load models
        $this->ci->load->model('binarytree_model');
        $this->ci->load->model('plans_model','plans');
        $this->ci->load->model('referrals_model','referrals');
        $this->ci->load->model('users_model');
        $this->ci->load->model('Shopping_order_model', 'shopping_order');
        $this->ci->load->model('point_traceability_model', 'point_traceability');


        $this->ci->load->model('subscriptions_model','subscriptions');
        $this->ci->load->library([
            'transaction',
            'residual',
            'plan'
        ]);
        $this->ci->load->library("referral", null, "referral_lib");
    }
    
    
    // Create an user record into tree table with assigned points
    function create($id = null, $plan = null){
        if ($id && $plan){
            
            // Get plan data to extract points
            $plan = $this->ci->plans->get($plan);

            // Create array with user values
            $data = array(
                'id_user' => $id,
                'points' => $plan['points']    
            );
            
            $this->ci->binarytree_model->insert($data);
        }        
    }
    
    
    function add_child($parent = null, $child = null, $handler = 'left'){
        if($parent && $child){
            
            /*
             * Errors list
             * -1 User is already a child
             * -2 Invalid handler
             * -3 Branch is not available (busy)
             * -4 Parent tree dones't exist
             */
            
            if($this->is_child($child)){
                return -1;
            }
            
            if($handler != 'left' &&  $handler != 'right'){
                return -2;
            }
            
            // Ge current childs from parent
            $parent_childs = $this->ci->binarytree_model->get_childs($parent);
            
            $branch = strtolower("{$handler}_child");
            
            $data = array(
                "{$branch}" => $child
            );
            
            if($parent_childs){
                return ($parent_childs[$branch] != '') ? -3 : $this->ci->binarytree_model->update($parent,$data);
            }else{
                return -4;
            }
            
        }
    }
    
    
    function add_bonus(array $records = array()){
        if($records){

            foreach($records as $record){
                // Update wallet and profit
                $description = "Bono binario por ({$record['minor']} pts) : lado ".( strtolower($record['handler'])=="right"? "derecho":"izquierdo" );                
                $this->ci->transaction->addBinaryBonus($record['id_user'],$record['bonus'],$description);
                
                // add wallet profit
                $this->ci->subscriptions->add_profit($record['id_user'],$record['bonus']);
           }
        }
    }
    
    function update_parent($data){
        if ($data){
            // Update tree last update
            return $this->ci->binarytree_model->update($data['id_user'],$data);
        }
        
    }
    
    
    function points_back(array &$user_row =array()){
        
        if($user_row){

            // Get both branchs
            $left_array = $this->get_points($user_row['id_user'],'left');
            $right_array = $this->get_points($user_row['id_user'],'right');

            $get_directs = $this->ci->referrals->get_directs($user_row['id_user']);
            $list_users = explode(',',$get_directs['users']);
            $my_referrals = [];
            foreach($list_users as $id){
                if(isset($right_array[$id])){
                    array_push($my_referrals, $right_array[$id] );
                }
                if(isset($left_array[$id])){
                    array_push($my_referrals, $left_array[$id] );
                }
            }
            
            // if(is_array($my_referrals)){
            //     $count_master = 0;
            //     foreach($my_referrals AS $referral){
            //         if($referral == 700){
            //             ++$count_master;
            //         }
            //     }
            //     if($count_master >= 2){
            //         $this->ci->transaction->add_bonus_fullness($user_row['id_user']);
            //     }
            // }
            //Get json sides, compare and update new childs
            $stored_left = (array) json_decode($user_row['left_json'],true);
            $stored_right = (array) json_decode($user_row['right_json'],true);

            $new_left = array_diff_key($left_array, $stored_left);
            $new_right = array_diff_key($right_array, $stored_right);

            if($new_left){
                // Add points to left side
                $sum = array_sum($new_left);
                $user_row['left_points'] = ($user_row['left_points'] + $sum);

                // Update left json
                $user_row['left_json'] = json_encode(array_replace($stored_left,$new_left),true);

                //sumar al total
                $user_row['total_points'] += $sum;
            }

            if($new_right){
                // Add point to right side
                $sum = array_sum($new_right);
                $user_row['right_points'] = ($user_row['right_points'] + $sum);

                // Update right json 
                $user_row['right_json'] = json_encode(array_replace($stored_right,$new_right),true);

                //sumar al total
                $user_row['total_points'] += $sum;

            }
        }
    }

    public function add_binary_points($id_user, $points=0, $side ="left"){
        $register = $this->ci->binarytree_model->get("*", ["id_user"=>$id_user],1,"id_tree", "DESC");
        if( !empty( $register ) ){
            if( $side == "left" ){
                $points =   intval($register[0]["left_points"])+$points;
                $this->ci->binarytree_model->update( $id_user,["left_points"=> $points, "last_bonus"=>date("Y-m-d H:i:s")] );
            }
            else if( $side == "right"){
                $points =   intval($register[0]["right_points"])+$points;
                $this->ci->binarytree_model->update( $id_user,["right_points"=> $points, "last_bonus"=>date("Y-m-d H:i:s")] );
                
            }
        }
    }
    
    // Return a limited list of parents peding for bonus bonus
    function bonus_list(){
        
        $date = date("Y-m-d");
        
        //Select users with assigned childs at left or right side
        $select = "*";
        $where = "(COALESCE(left_child,right_child) IS NOT NULL OR COALESCE(left_child,right_child) > 0) AND (DATE_FORMAT(last_bonus, '%Y-%m-%d') < '{$date}' OR last_bonus IS NULL)";
        $query = $this->ci->binarytree_model->get($select, $where,1000, "id_tree", "desc");
        
        return ($query) ? $query : array();
    }
    
    
    
    // Return parent id to assing into tree branch
    function next_branch($ref = null, $handler = 'left', $ref2 = null){
        if($ref){
            $last = $ref;
            
            if(is_array($ref2)){
                $branchs = $ref2;
            }else{
                $branchs =  $this->get_branch($ref,$handler);
            }
            
            if ($branchs){
                $this->find_next($branchs,$handler,$last,$ref);
                return $last;
            }else{
                return $ref;
            }
        }
        return false;
    }
    
    
    function find_next($branch = array(), $handler = 'left', &$last, $ref = 0){
        
        $arr = array_keys($branch);
            
            if($handler == 'left'){
                if (isset($arr[0])){
                    $last = $arr[0];
                }
            }

            if($handler == 'right'){
                if (isset($arr[1])){
                    $last = $arr[1];
                }
            }  
            
            if($last == 0 || $last == 1){
                $last = $ref;
            }else{
                if($branch){
                    if (isset($branch[$last])){
                        if (is_array($branch[$last])){
                            $this->find_next($branch[$last],$handler,$last,$last);
                        }
                    }
                }
            }
    }
    
    function array_keys_multi(array $array)
    {
        $keys = array();

        foreach ($array as $key => $value) {
            $keys[] = $key;

            if (is_array($value)) {
                $keys = array_merge($keys, array_keys_multi($value));
            }
        }

        return $keys;
    }
    
    
    // Return user tree given a parent
    function get_childs($parent = null) {
                
        if($parent == '' || $parent == null){
            exit;
        }
        
        $child = array();
        $childs = $this->ci->binarytree_model->get_childs($parent);

        if($childs){
            if($childs['left_child']){
                $child[$childs['left_child']] = $this->get_childs($childs['left_child']);
            }elseif($childs['right_child']){
                $child[0] = array();
            }

            if($childs['right_child']){
                $child[$childs['right_child']] = $this->get_childs($childs['right_child']);
            }elseif($childs['left_child']){
                $child[1] = array();
            }
        }
          
        return $child;
    }
    
    
    // Return user tree given a parent and branch side
    function get_branch($parent = null, $handle = 'left', $keys = true) {
                
        if($parent == '' || $parent == null){
            exit;
        }
        
        $child = array();
        $childs = $this->ci->binarytree_model->get_childs($parent);
        
        if($childs){
            if ($handle == 'left'){
                if($childs['left_child']){
                    if ($keys){
                        $child[$childs['left_child']] = $this->get_childs($childs['left_child']);
                    }else{
                        $child[] = $this->get_childs($childs['left_child']);
                    }
                }
            }

            if ($handle == 'right'){
                if($childs['right_child']){
                    if($keys){
                        $child[$childs['right_child']] = $this->get_childs($childs['right_child']);
                    }else{
                        $child[] = $this->get_childs($childs['right_child']);
                    }
                } 
            }
        }
        
        return $child;
    }
    
    function get_points($parent = null, $handler = 'right'){
        /*
         * Errors list
         * -1 Invalid handler
         * -2 Parent tree doesn't exist
         */
        
        if($parent && $handler){
            
            // Handler validation
            if($handler != 'left' &&  $handler != 'right'){
                return -1;
            }
            
            // Get branch side given a handler
            $branch = $this->get_branch($parent,$handler);
            
            if ($branch){
                $values = array_keys_multi($branch);
                return $this->ci->binarytree_model->get_points($values);
            }else{
                return -2;
            }
        }
        return false;
    }
    
    function get_score($id = null){
        if($id){
            
           $row = $this->ci->binarytree_model->get_score($id);

           $this->points_back($row);

           // Update user tree with new values (left and right points, profit, json sides)
           $this->update_parent($row);
           
           return $row;
        }
    }
    
    function is_child($child = null){
        if($child){
            if($this->ci->binarytree_model->find_child($child) > 0){
                return true;
            }else{
                return false;
            }
        }        
    }
    
    
    function get_unassigned($id = null){
        if($id){
            
            $unssigned = array();
            
            // Get list of referreds
            $childs = $this->ci->referrals->get_all(array('id_referral' => $id));
                        
            // Get current childs on tree branch (first level)
            $assigned = $this->ci->binarytree_model->get_childs($id);
            
            foreach($childs as $child){
                if (!in_array($child['id_user'], $assigned)){
                   $unssigned[] = $child['id_user'];
                }
            }
            
            return $unssigned;
            
        }
        return false;
    }

    public function parents($id_user, $lvl = 5, $active = false)
    {
        
        $parents = [];
        for ($i=0; $i < $lvl; $i++) { 
            if ($i > 0 && !empty($parent)) {
                $id_user = $parent['id_user'];
            }
            $parent = $this->ci->binarytree_model->find_is_child($id_user);
            if($parent){
                if($active){
                    if(is_active($parent['id_user'])){
                        array_push($parents, $parent);
                    }else{
                        $id_user = $parent['id_user'];
                        --$i;
                    }
                }else{
                    array_push($parents, $parent);
                }
            }
        }
        
        return $parents;
    }


    

    public function update_tree_activation($id_user, $points)
    {
        $this->ci->binarytree_model->update($id_user, ['points' => $points]);
    }

    /**
     * Carlos Aguirre 2019-07-24 14:54:17
     * Añade los puntos  residuales al usuario que ha adquirido 
     * un plan en especifico
     * Cosultivo:15
     * Star:50
     * Plus:100
     * Master:250
     * @param int $id_user
     */

    public function add_residual_points_upgrade_plan($id_user, $plan_id){

        $plan_id = "".$plan_id;
        $puntos_residuales = [
            "5"=>15,
            "6"=>50,
            "7"=>100,
            "8"=>250
        ];

        $usuario = $this->ci->users_model->get_user($id_user);
        $plan_referido = $this->ci->plan->get_plan( $plan_id );

        // Si la subscripcion del padre esta activa le añade los puntos
        $this->ci->residual->add_points(
            $id_user, 
            $puntos_residuales[$plan_id],
            "Puntos Residuales por compra de plan de ".$usuario["dni"]." - ".$usuario["first_name"]." plan: ".$plan_referido["spanish_name"]
        );
        log_message('debug',"INFO RESIDUAL_POINTS_UPGRADE usuario(".$id_user.") puntos (".$puntos_residuales[$plan_id].") PLAN:".$plan_id);
    }


    public function add_binary_points_plan_purchase($referal_id, $plan_id){
        $plan_id = "".$plan_id;
        $puntos_binarios = [
            "5"=>15,
            "6"=>100,
            "7"=>300,
            "8"=>700
        ];

        $padre_id_user = $this->ci->referral_lib->direct_parents($referal_id, 1, false);
        

        if( !empty( $padre_id_user  ) ){
            $padre = $this->ci->referrals->get(["id_user"=>$referal_id]);
         
            if( $padre["referral_side"] == "left" ){
                $this->add_binary_points($padre_id_user[0], $puntos_binarios[$plan_id], "left");
            }
            else if($padre["referral_side"] == "right"){
    
                $this->add_binary_points($padre_id_user[0], $puntos_binarios[$plan_id], "right");
            }
            log_message('debug', 'INFO PUNTOS_BINARIOS_COMPRA_PLAN:arbol(padre '.$padre_id_user[0].')puntos ('.$puntos_binarios[$plan_id].') ');
        }
        else{
            log_message('debug', 'INFO PUNTOS_BINARIOS_COMPRA_PLAN: no hay papa');

        }
    
    
    }

    public function activation_bonus($id_user)
    {
        $parents = $this->ci->referral_lib->direct_parents($id_user, 8, true);
        $bonus = (220 * 3) / 100;
        $full_name = $this->ci->users_model->get_full_name($id_user);
        foreach ($parents as $parent) {
            #$parent['id_user']
            if( $parent != 0 ){
                $description = 'Bono de activación de usuario ' . $full_name;
                $this->ci->transaction->add_activation_bonus($parent, $bonus, $description);
            }
            
        }
    }

    /**
     * Carlos Aguirre 2019-08-01 09:55:11
     * Genera la transaccion de bono por upgrade
     * pagando la diferencia de comision, del nuevo plan con el 
     * anterior 
     */
    public function add_upgrade_bonus( $id_user, $old_plan_id, $new_plan_id ){
        $plan_anterior = $this->ci->plan->get_plan( $old_plan_id );
        $plan_nuevo = $this->ci->plan->get_plan( $new_plan_id );

        $saldo_bono = floatval( $plan_nuevo["commission"] ) - floatval( $plan_anterior["commission"] );
        $padre = $this->ci->referral_lib->direct_parents( $id_user, 1, true );
        $usuario =  $this->ci->users_model->get_user($id_user);
        /*
        if( !empty( $padre ) ){
            $transaction_id = $this->ci->transaction->add_transaction_bonus_upgrade(
                $padre[0], 
                $saldo_bono, 
                [
                    "description"=> "Bono de upgrade de ".$plan_anterior["spanish_name"]." a ".$plan_nuevo["spanish_name"]." usuario:".$usuario['username'],
                    "status"=>"done"
                ]
            );
        }*/
        
    }


    public function add_residual_points_products($id_user, $shopping_order_id){
        $products = $this->ci->shopping_order->get_items_order('*',[
            "shopping_order_id"=>$shopping_order_id
        ]);

        $puntos_residuales = 0;

        foreach( $products as $product ){
            $puntos_residuales += $product["residual_points"]*intval($product['quantity']);
        }

        log_message('debug', 'INFO PUNTOS_RESIDUALES_PRODUCTO:usuario('.$id_user.') puntos('.$puntos_residuales.')');
        $this->ci->residual->add_points(
            $id_user, 
            $puntos_residuales,
            "Puntos por compra de productos orden #".sprintf('%011d',$shopping_order_id)
        );
    }

    public function add_binary_points_products($id_user, $shopping_order_id){
        $products = $this->ci->shopping_order->get_items_order('*',[
            "shopping_order_id"=>$shopping_order_id
        ]);

        $puntos_binarios = 0;
        foreach( $products as $product ){
            $puntos_binarios += $product["binary_points"]*intval($product['quantity']);
        }

        $padre_id_user = $this->ci->referral_lib->direct_parents($id_user, 1, false);
        
        $padre = $this->ci->referrals->get(["id_user"=>$id_user]);

        if( !empty( $padre_id_user  ) ){
            $padre = $this->ci->referrals->get(["id_user"=>$id_user]);

            if( $padre["referral_side"] == "left" ){
                $this->add_binary_points($padre_id_user[0], $puntos_binarios, "left");
            }
            else if($padre["referral_side"] == "right"){
    
                $this->add_binary_points($padre_id_user[0], $puntos_binarios, "right");
            }
            log_message('debug', 'INFO PUNTOS_BINARIOS_PRODUCTO:arbol(padre '.$padre_id_user[0].')puntos ('.$puntos_binarios.') valores('.var_export($padre, true).')orden'.$shopping_order_id);
        }
        else{
            log_message('debug', 'INFO PUNTOS_BINARIOS_PRODUCTO: no tiene padre');

        }
       
    }
    function get_points_all_time($id_user)
    {
        $results = $this->ci->point_traceability->get('SUM(points) AS total_points', ['id_user' => $id_user, 'type' => 'binary', "YEAR(`date`)"=>date("Y"),"MONTH(`date`)"=>date("m")]);
        return (int) $results['total_points'];
    }

    /**
     * Carlos Aguirre 2019-09-10 10:38:56
     * Funcion para verificar que un usuario ya halla hecho binario
     * y que ambos hijos esten activos dentro de la plataforma
     * @param int $id_user 
     * @return bool 
     */
    function is_binary_active( $id_user ){
        $brazo = array_keys( $this->get_childs($id_user) );
        if( empty($brazo) || count( $brazo ) < 2 ){
            return false;
        }

        return ( is_active( $brazo[0] ) && is_active( $brazo[1] )  );
    }


    public function get_user_tree($id_user){
        return $this->ci->binarytree_model->get(
            "*",
            [
            "id_user"=>$id_user
            ],
            1
        );

    }

    /**
     * Carlos Aguirre 2019-09-13 10:53:12
     * Obtiene el monto de liquidacion de binario para un 
     * usuario enviado por parametro
     * @param int $id_user ( id del usuario )
     * @return float ( monto de liquidacion )
     */
    public function get_amount_liquidation_binary($id_user){
        $this->ci->load->library("transaction");
        $transaction = $this->ci->transaction->get_transactions_by_types($id_user,[
            "id_user"=>$id_user,
            "type"=>'binary',
            "status"=>'done',
            "YEAR(`date`)"=>date('Y'),            
            "MONTH(`date`)"=>date('m')
        ],10);

        if( empty($transaction) ){
            return 0;
        }
        else{
            return $transaction[0]['usd'];
        }
    }



    public function get_sum_by_side($id_user, $side){
        $user = $this->ci->binarytree_model->get('*',["id_user"=>$id_user])[0];
        if( $side == 'left' ){
            $puntos = $user["left_points"];
            if( empty($this->get_child($id_user, $side)) ){
                return $puntos;
            }
            else{
                return $puntos + $this->sum_points( $this->get_child($id_user, $side) );
            }
        }

        if( $side == 'right' ){
            $puntos = $user["right_points"];
            if( empty($this->get_child($id_user, $side)) ){
                return $puntos;
            }
            else{
                return $puntos + $this->sum_points( $this->get_child($id_user, $side) );
            }
        }
    }

    public function sum_points($id_user){

        $user = $this->ci->binarytree_model->get('*',["id_user"=>$id_user])[0];

        if( empty($this->get_child( $id_user, 'left' )) && empty($this->get_child( $id_user, 'right' )) ){
            return $user["left_points"]+$user["right_points"];
        }
        else{

            $ptosEquipoIzq = 0;
            $ptosEquipoDer = 0;
            if( !empty($this->get_child( $id_user, 'left' )) ){
                $ptosEquipoIzq = $this->sum_points($this->get_child( $id_user, 'left' ));
            }
            if( !empty($this->get_child( $id_user, 'right' )) ){
                $ptosEquipoDer = $this->sum_points($this->get_child( $id_user, 'right' ));
            }

            return $user["left_points"] + $user["right_points"] + $ptosEquipoIzq + $ptosEquipoDer;
        }
    }


    public function get_child( $id_user, $side ){
        $user = $this->ci->binarytree_model->get('*',["id_user"=>$id_user]);
        if( empty($user) ){
            return null;
        }
        if( $side == 'left' ){
            if( empty($user[0]['left_child']) ){
                return null;
            }
            else{
                return $user[0]['left_child'];
            }
        }
        else{
            if( empty($user[0]['right_child']) ){
                return null;
            }
            else{
                return $user[0]['right_child'];
            }
        }
    }


    public function get_table_binary( $id_user ){
        return $this->ci->binarytree_model->get_table_binary($id_user);
    }
}    