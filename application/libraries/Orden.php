<?php defined('BASEPATH') OR exit('No direct script access allowed');

class Orden{
  public function __construct()
    {
      // just in case url helper has not load yet
      $this->ci =&get_instance();
      
      
      // Load models
      $this->ci->load->model('Shopping_order_model','orden_model');
      $this->ci->load->model('ShippingOrder_model', "shipping_model");
      $this->ci->load->model('Shipping_details_model', "shipping_details_model");
      $this->ci->load->library('wallet');
      $this->ci->load->library('transaction');


      
            
    }

  
  /**
   * Crea un registro temporal en la tabla shopping_order
   * para obtener el consecutivo de la orden y poder mostrarlo
   * al usuario
   * @return int ( consecutivo de la orden ) 
   */
  public function create_temporal_orden($user_id){

    // Si ya tenia ordenes temporales antiguas
    // debe eliminarlas de la base de datos
    $this->ci->orden_model->eliminar( [
      "state"=>'temporal',
      "users_id"=>$user_id
    ]);
    

    $data = [
      "users_id"=>$user_id,
      "users_id_referal"=>null,
      "total_value"=>0,
      "total_discount"=>0,
      "state"=>'temporal'
    ];

    
    // crea el registro en `shopping_order` y retorna el id de insersion
    return $this->ci->orden_model->create( $data );
  }

  /**
   * Crea un registro temporal en la tabla shopping_order
   * para obtener el consecutivo de la orden y poder mostrarlo
   * al usuario externo
   * @return int ( consecutivo de la orden ) 
   */

   public function create_temporal_orden_user_external($user_id, $users_id_referal){

    // Si ya tenia ordenes temporales antiguas
    // debe eliminarlas de la base de datos
    $this->ci->orden_model->eliminar( [
      "state"=>'temporal',
      "users_id"=>$user_id
    ]);
    

    $data = [
      "users_id"=>$user_id,
      "users_id_referal"=>$users_id_referal,
      "total_value"=>0,
      "total_discount"=>0,
      "state"=>'temporal'
    ];

    
    // crea el registro en `shopping_order` y retorna el id de insersion
    return $this->ci->orden_model->create( $data );
  }


  /**
   * Carlos Aguirre 2019-07-22 08:04:33
   * Obtiene una orden temporal creada para un usuario
   * en especifico
   * @param int $user_id ( id del usuario )
   * @return array ( orden temporal )
   */
  public function get_temporal_orden($user_id){
  
    return $this->ci->orden_model->get('*',[
      "users_id"=>$user_id,
      "state"=>'temporal'
    ], 
    1,
    "created DESC");
  }


  /**
   * Carlos Aguirre 2019-07-22 09:55:33
   * Registra el metodo de envio que se ha
   * elegido para la orden de compra
   * @param int $shopping_order_id id de la orden de compra
   * @param array $envio el campo de envio
   * @return void
   */
  public function registrar_envio($shopping_order_id, $envio){

    $descripcion = "";
    $shipping_details_id = null;

    if( !empty( $envio["direccion"] ) ){
      $descripcion = $envio["direccion"];
    }
    else if( !empty( $envio["oficina"] ) ){
      $shipping_details_id = $envio["oficina"];
    }
    else{
      $shipping_details_id = $envio["mayorista"];
    }


    $shipping_order_data = [
      "shipping_details_id"=>$shipping_details_id,
      "shopping_order_id"=> $shopping_order_id,
      "description"=>$descripcion,
      "last_update"=> date("Y-m-d H:i:s")
    ];

    $this->ci->shipping_model->crear( $shipping_order_data );
  }
  /**
   * Carlos Aguirre 2019-07-24 11:00:17
   * Obtiene el metodo de envio que se ha seleccionado en una 
   * orden
   */
  public function obtenerMetodoEnvio( $shopping_order_id ){
    $envio = $this->ci->shipping_model->obtenerRegistroPorId($shopping_order_id);

    if( empty( $envio["description"] ) ){
      return ["tipo"=>$envio["type_name"], "nombre"=>$envio["detail_name"]." (".$envio["detail_description"].")", "code" => sprintf('%011d',$shopping_order_id)];
    }
    if( $envio["description"] != "RETIRO EN OFICINA PPAL" ){
      return ["tipo"=>"COURIER", "nombre"=>$envio["description"], "code" => sprintf('%011d',$shopping_order_id) ];

    }
    return ["tipo"=>"OFICINA PPAL", "nombre"=>$envio["description"], "code" => sprintf('%011d',$shopping_order_id) ];
  }



  /**
   * Carlos Aguirre 2019-10-10 09:18:42
   * Obtiene el mayorista al que se le han
   * adquirido los productos de la orden
   * @param int $shopping_order_id ( id de la orden )
   * @return Array 
   */
  public function get_wholesaler_order( $shopping_order_id ){
    $products = $this->ci->orden_model->get_shopping_products("*",
                                    [
                                      "product_id >"=>1, 
                                      "shipping_details_id is not null"=>null,
                                      "shipping_details_id <>"=>"0",
                                      "shopping_order_id"=>$shopping_order_id
                                    ]);

    if(empty($products)){
      return [];
    }
    else{
      return $this->ci->shipping_details_model->obtenerRegistroPorId($products[0]["shipping_details_id"]);
    }
  }

  /**
   * Carlos Aguirre 2019-10-10 10:05:34
   * traslada saldo de un usuario a un mayorista
   * cuando un usuario le compra con saldo en cuenta
   * @param int $shopping_order_id ( id de la orden de compra )
   * @param int $user_wholesaler_id ( usuario que maneja a el mayorista )
   * @param int $payment_qty ( cantidad a trasladar )
   * @return void
   */
  public function add_payment_wallet_to_wholesaler( $shopping_order_id, $user_wholesaler_id, $payment_qty ){
    
    $this->ci->wallet->add_funds( $user_wholesaler_id, $payment_qty );
    $this->ci->transaction->wallet_payment($user_wholesaler_id,$shopping_order_id,$payment_qty,["reference"=>""]);
  
  }


}