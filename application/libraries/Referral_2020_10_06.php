<?php defined('BASEPATH') OR exit('No direct script access allowed');
/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of Wallet
 *
 * @author Juan Manuel Pinzon
 */
class Referral {
    
        
    public function __construct()
    {
        // just in case url helper has not load yet
        $this->ci =& get_instance();
        
        // Load models
        $this->ci->load->model('users_model','users');
        $this->ci->load->model('subscriptions_model','subscriptions');
        $this->ci->load->model('plans_model','plans');
        $this->ci->load->model('referrals_model');
        $this->ci->load->model('transactions_model','transactions');

        $this->ci->load->library('transaction');
        $this->ci->load->library('binarytree');
        $this->ci->load->library('wallet');
        $this->ci->load->library('rank');
    }

    /**
     * @param null $referral
     * @param null $referred
     * @param null $plan
     * @param string $side
     * @return bool
     */
    function create($referral = null, $referred = null, $plan = null, $side = "left"){
        if ($referred && $plan){
            
            $subcription = $this->ci->subscriptions->get_active($referral);
            // Get plan data 
            $plan = $this->ci->plans->get($plan);
            
            $row = array(
                'id_user'           => $referred,
                'id_referral'       => $referral,
                'id_ref_subscription'=> $subcription['id'],
                'referral_side'     => $side,
                'base'              => $plan['base'],
                'value_rule1'       => $plan['value_rule1'],
                'value_rule2'       => $plan['value_rule2'],
                'max_value_rule'    => $plan['max_value_rule'],
                'number_referrals'  => 0,
                'status'            => 'inactive'
            );
            
            $this->ci->referrals_model->insert($row);
            
        }
        
        return false;
        
    }
    
    function activate($id = null){
        if($id){
              
            // Get rules data from user
            $referred = $this->get_rules(array('id_user' => $id, 'status' => 'inactive'));
            //$referred_data = $this->ci->users->get_user($id);

            // if there is an inactive referral rules  else do nothing
            if($referred){ 

                // Activate referred rules
                $this->ci->referrals_model->update(
                    array('id_user' => $referred['id_user'], 'status' => 'inactive'),
                    array('status' => 'active')
                );

                
            }
        }
    }

    public function commission($id)
    {
        // Add referral commission if there if one
        
        $subcription = $this->ci->subscriptions->get_active($id);
        $plan = $this->ci->plans->get($subcription['id_plan']);
        $parents = $this->direct_parents($id, 5, false);


        $user = $this->ci->users->get_user($id);
        if (!$this->ci->users->email_exists($user['email'], TRUE)) {
            foreach ($parents as $key => $parent) {
                if($this->ci->users->get_user($parent)!= false && is_active($parent)){
                    $transaction = array(
                        'id_user'       => $parent,
                        'description'   => "Bono de pago de comisión por referido {$user['first_name']} {$user['last_name']}",
                        'usd'           => $plan['commission'],
                        'wallet'        => 'USD',
                        'status'        => 'done',
                        'type'          => 'referral',
                    );
                    // Insert transaction history
                    if($this->ci->transactions->insert($transaction)){
                        $this->ci->wallet->add_funds($parent,$plan['commission']);
                        $this->ci->subscriptions->add_profit($parent,$plan['commission']);
                    }
                }
            }
        }
        // update referrals count
        $data = array(
            'number_referrals' => ($referral['number_referrals'] + 1),
        );

        $this->ci->referrals_model->update(array('id_user' => $referral['id_user']),$data);
    }
    
    function get_rules($id = null){ 
        return ($id) ? $this->ci->referrals_model->get($id) : array();
    }
    
    
    function get_directs($id = null){
        if($id){
            return $this->ci->referrals_model->get_directs($id);
        }
        return false;
    }
    
    /**
     * Get directs referrals with user id and subscription id
     * 
     * @param null $id
     * @param null $subscription
     * @return arrray Direct referrals
     */
    function get_directs_subscription($id = null, $subscription = null){
        if($id && $subscription){
            return $this->ci->referrals_model->get_directs_subscription($id, $subscription);
        }
        return false;
    }
    
    function get_referrals($id = null){
        if($id){
            $referrals =  $this->ci->referrals_model->get_referrals($id);
            
            foreach($referrals as &$referral){
                
                // Get user information
                $user = $this->ci->users->get_user($referral['id_user']);
                $subs = $this->ci->subscriptions->get_subscription($referral['id_user']);
                $rank = $this->ci->rank->get($referral['id_user']);
                $plan = $this->ci->plans->get($subs['id_plan']);

                $referral['rank']       = _rank_title($rank['rank']);
                $referral['username']   = $user['username'];
                $referral['image']      = $user['image'];
                $referral['first_name'] = $user['first_name'];
                $referral['last_name']  = $user['last_name'];
                $referral['plan']       = $subs['id_plan'];
                $referral['spanish_name']= $plan['spanish_name'];
                $referral['english_name']= $plan['english_name'];
                $referral['start_date'] = $subs['start_date'];
                
                unset($user,$subs,$rank);
                
            }
            
            return $referrals;
            
        }
        return false;
    }


    public function direct_parents( $id_user, $lvl = 1, $active = false ){

        $direct_parent = [];
        for ($i=0; $i < $lvl; $i++) { 
            if ($i > 0 && !empty($id_user)) {
                $id_user =$parent['id_referral'];
            }

            $parent = $this->ci->referrals_model->get(["id_user"=>$id_user]);


            //var_dump("padre de ".$id_user." es ".$parent["id_referral"]);

            if(!empty($parent)){
                if($active){
                    
                    if(is_active($parent['id_referral'])){
                        array_push($direct_parent, $parent['id_referral']);
                    }else{
                        --$i;
                        $id_user = $parent['id_referral'];
                    }
                }else{
                    array_push($direct_parent,$parent['id_referral']);
                }
            }

        }
        
        return $direct_parent;
    }

    /**
     * Consulta los patrocinadores que han referido dos o mas
     * personas master en la primera o segunda quincena del mes 
     * actual
     */
    public function get_with_two_referrals_master(){
        $sql ="
        select 
            u.id as 'id_user',
            u.username as 'patrocinador',
            uref.username as 'referido',
            date(tr.`date`) as 'fecha',
            count( tr.id ) as 'cant'
        from users u
        join referrals ref
            on ref.id_referral = u.id
        join users uref
            on uref.id = ref.id_user
        join transactions tr
            on tr.id_user = ref.id_user
        where 
         tr.type = 'purchase_plan' and tr.status='done' and hash='8' 
         and
         date(tr.`date`) between '%s' and '%s'
        group by u.id
        having cant > 1
        ";


        $primeraQuincena = $this->ci->referrals_model->query(sprintf($sql, date("Y-m-01"), date("Y-m-15")));
        $segundaQuincena = $this->ci->referrals_model->query(sprintf($sql, date("Y-m-16"), date("Y-m-t")));


        return ($primeraQuincena + $segundaQuincena);

    }
}