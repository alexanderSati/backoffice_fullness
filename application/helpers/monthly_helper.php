<?php

defined('BASEPATH') OR exit('No direct script access allowed');
/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */


if (!function_exists('isMonthlyActive')) {

    /**
     * Perform a user validation for monthly payment
     * @return boolean
     */
    function isMonthlyActive($uid = null) {

        $CI = & get_instance();
        $CI->load->model('transactions_model');
        $CI->load->library('transaction');
        
        $user_id = ($uid) ? $uid : $CI->user['id'];

        // Initial status
        $allowed = true;

        // Query params
        $params = array(
            'id_user' => $user_id,
            'reference' => date('m/Y'),
            'type' => 'monthly',
        );

        // Users accounts before deployment are allowed
        /*if ($params['reference'] != '10/2017') {

            $deposit = $CI->transactions_model->get_where_row($params, 'id', 'DESC');
            
            if ($deposit) {

                switch ($deposit['status']) {
                    case 'pending':
                    case 'waiting':
                        $allowed = false;
                        break;
                    case 'rejected':
                    case 'failed':
                    case 'cancelled':

                        // Create a new payment to allow users deposit again
                        $CI->transaction->addMonthlyPayment($user_id, date('m/Y'));
                        $allowed = false;
                        break;
                    case 'done':
                        $allowed = true;
                        break;

                    default:
                        $allowed = false;
                        break;
                }
            } else {
                // Create a new monthly payment if it doesn't exists
                $CI->transaction->addMonthlyPayment($user_id, date('m/Y'));
                $allowed = false;
            }
        } else {
            $allowed = true;
        }*/
        return $allowed;
    }

}

if (!function_exists('isMonthlyPending')) {

    function isMonthlyPending($uid = null) {
    
        $CI = & get_instance();
        $CI->load->model('transactions_model');
        $CI->load->library('transaction');
        
        $user_id = ($uid) ? $uid : $CI->user['id'];

        // Query params
        $params = array(
            'id_user' => $user_id,
            'reference' => date('m/Y',  strtotime('last day of next month')),
            'status' => 'waiting',
            'type' => 'monthly',
        );
        
        $deposit = $CI->transactions_model->get_where_row($params, 'id', 'DESC');
        
        return ($deposit) ? true : false;
        
    }

}

