<?php defined('BASEPATH') OR exit('No direct script access allowed');



/**

 * Outputs an array in a user-readable JSON format

 *

 * @param array $array

 */

if ( ! function_exists('display_json'))

{

    function display_json($array)

    {

        $data = json_indent($array);



        header('Cache-Control: no-cache, must-revalidate');

        header('Expires: Mon, 26 Jul 1997 05:00:00 GMT');

        header('Content-type: application/json');



        echo $data;

    }

}





/**

 * Convert an array to a user-readable JSON string

 *

 * @param array $array - The original array to convert to JSON

 * @return string - Friendly formatted JSON string

 */

if ( ! function_exists('json_indent'))

{

    function json_indent($array = array())

    {

        // make sure array is provided

        if (empty($array))

            return NULL;



        //Encode the string

        $json = json_encode($array);



        $result        = '';

        $pos           = 0;

        $str_len       = strlen($json);

        $indent_str    = '  ';

        $new_line      = "\n";

        $prev_char     = '';

        $out_of_quotes = true;



        for ($i = 0; $i <= $str_len; $i++)

        {

            // grab the next character in the string

            $char = substr($json, $i, 1);



            // are we inside a quoted string?

            if ($char == '"' && $prev_char != '\\')

            {

                $out_of_quotes = !$out_of_quotes;

            }

            // if this character is the end of an element, output a new line and indent the next line

            elseif (($char == '}' OR $char == ']') && $out_of_quotes)

            {

                $result .= $new_line;

                $pos--;



                for ($j = 0; $j < $pos; $j++)

                {

                    $result .= $indent_str;

                }

            }



            // add the character to the result string

            $result .= $char;



            // if the last character was the beginning of an element, output a new line and indent the next line

            if (($char == ',' OR $char == '{' OR $char == '[') && $out_of_quotes)

            {

                $result .= $new_line;



                if ($char == '{' OR $char == '[')

                {

                    $pos++;

                }



                for ($j = 0; $j < $pos; $j++)

                {

                    $result .= $indent_str;

                }

            }



            $prev_char = $char;

        }



        // return result

        return $result . $new_line;

    }

}





/**

 * Save data to a CSV file

 *

 * @param array $array

 * @param string $filename

 * @return bool

 */

if ( ! function_exists('array_to_csv'))

{

    function array_to_csv($array = array(), $filename = "export.csv")

    {

        $CI = get_instance();



        // disable the profiler otherwise header errors will occur

        $CI->output->enable_profiler(FALSE);



        if ( ! empty($array))

        {

            // ensure proper file extension is used

            if ( ! substr(strrchr($filename, '.csv'), 1))

            {

                $filename .= '.csv';

            }



            try

            {

                // set the headers for file download

                header("Expires: Mon, 26 Jul 1997 05:00:00 GMT");

                header("Last-Modified: " . gmdate("D, d M Y H:i:s") . "GMT");

                header("Cache-Control: no-cache, must-revalidate");

                header("Pragma: no-cache");

                header("Content-type: text/csv");

                header("Content-Description: File Transfer");

                header("Content-Disposition: attachment; filename={$filename}");



                $output = @fopen('php://output', 'w');



                // used to determine header row

                $header_displayed = FALSE;



                foreach ($array as $row)

                {

                    if ( ! $header_displayed)

                    {

                        // use the array keys as the header row

                        fputcsv($output, array_keys($row));

                        $header_displayed = TRUE;

                    }



                    // clean the data

                    $allowed = '/[^a-zA-Z0-9_ %\|\[\]\.\(\)%&-]/s';

                    foreach ($row as $key => $value)

                    {

                        $row[$key] = preg_replace($allowed, '', $value);

                    }



                    // insert the data

                    fputcsv($output, $row);

                }



                fclose($output);



            }

            catch (Exception $e) {}

        }



        exit;

    }

}





/**

 * Generates a random password

 *

 * @return string

 */

if ( ! function_exists('generate_random_password'))

{

    function generate_random_password()

    {

        $characters = "abcdefghijklmnopqrstuwxyzABCDEFGHIJKLMNOPQRSTUWXYZ0123456789";

        $pass = array();

        $alpha_length = strlen($characters) - 1;



        for ($i = 0; $i < 8; $i++)

        {

            $n = rand(0, $alpha_length);

            $pass[] = $characters[$n];

        }



        return implode($pass);

    }

}





/**

 * Retrieves list of language folders

 *

 * @return array

 */

if ( ! function_exists('get_languages'))

{

    function get_languages()

    {

        $CI = get_instance();



        if ($CI->session->languages)

        {

            return $CI->session->languages;

        }



        $CI->load->helper('directory');



        $language_directories = directory_map(APPPATH . '/language/', 1);

        if ( ! $language_directories)

        {

            $language_directories = directory_map(BASEPATH . '/language/', 1);

        }



        $languages = array();

        foreach ($language_directories as $language)

        {

            if (substr($language, -1) == "/" || substr($language, -1) == "\\")

            {

                $languages[substr($language, 0, -1)] = ucwords(str_replace(array('-', '_'), ' ', substr($language, 0, -1)));

            }

        }



        $CI->session->languages = $languages;



        return $languages;

    }

}





/**

 * Return a formated time diff (ago)

 *

 * @return array

 */

if ( ! function_exists('format_interval'))

{

    function format_interval($date = null) {

        

        if ($date){

            

            // Difference minutes of orden

            $date1 = new DateTime(date("Y-m-d H:i:s"));

            $date2 = new DateTime($date);

            $interval = $date1->diff($date2);

   

            // Load settings languages

            $CI = &get_instance();



            $result = "";

            if ($interval->y) { $result .= $interval->format("%y ".$CI->lang->line("years")." "); }

            if ($interval->m) { $result .= $interval->format("%m ".$CI->lang->line("months")." "); }

            if ($interval->d) { $result .= $interval->format("%d ".$CI->lang->line("days")." "); }

            if ($interval->h) { $result .= $interval->format("%h ".$CI->lang->line("hours")." "); }

            if ($interval->i) { $result .= $interval->format("%i ".$CI->lang->line("minutes")." "); }

            if (!$interval->d && $interval->s) { $result .= $interval->format("%s ".$CI->lang->line("seconds")." "); }



            return $result;

        }

    }

    

}



/**

 * Return a values from a multidimensional array

 *

 * @return array

 */

if ( ! function_exists('array_keys_multi'))

{

    

    function array_keys_multi(array $array)

    {

        $keys = array();



        foreach ($array as $key => $value) {

            $keys[] = $key;



            if (is_array($value)) {

                $keys = array_merge($keys, array_keys_multi($value));

            }

        }



        return $keys;

    }

    

}





/**

 * Return a values from a multidimensional array

 *

 * @return array

 */

if ( ! function_exists('array_walk_multi'))

{

    

    function array_walk_multi(array $array, $keys = false)

    {

        // Load settings languages

        $CI = &get_instance();

        $CI->load->model('users_model');        

        $root = array();



        $lang = (isset($user['language']) ? $user['language'] : $CI->config->item('language'));

        

        $default = array(

            'image' => '/uploads/user-anonymous-disabled-2.gif',

            'text' => array(

                'name' => 'Available',

                'desc' => "Plan"

                )

           ); 

        

        foreach ($array as $key => $value) {

            

            if ($key > 1){

                // Get user data

                $user = $CI->users_model->get_user($key);



                $subscription = $CI->subscriptions->get_active($key);

                $my_plan = $CI->plans->get($subscription['id_plan']);



                $item = array(

                    'image' => ($user['image'] != '') ? "/uploads/{$user['image']}" : '/uploads/user-anonymous-disabled.gif',

                    'collapsed' => true,

                    'text' => array(

                        'name' => ucwords(strtolower("{$user['username']}")),

                        'desc' => ($my_plan["{$lang}_name"] != "") ? $my_plan["{$lang}_name"] : "Plan"

                        ),

                     'children' => (is_array($value)) ? array_walk_multi($value,$keys) : array()

                 ); 

            }else{

                $item = $default;

            }

                    

            if ($keys){

                $root[$key] = $item;

            }else{

                $root[] = $item;

            }

            

        }

        

        return $root;



    }

    

}



/**

 * Return a values from a multidimensional array

 *

 * @return array

 */

if(! function_exists('array_walk_multi_external')) {



    function array_walk_multi_external(array $array, $keys = false)

    {

        // Load settings languages

        $CI = &get_instance();

        $CI->load->model('users_model');        

        $root = array();



        $lang = (isset($user['language']) ? $user['language'] : $CI->config->item('language'));

        

        $default = array(

            'image' => '/uploads/user-anonymous-disabled-2.gif',

            'text' => array(

                'name' => 'Available',

                'desc' => "0"

                )

           ); 

        

        foreach ($array as $key => $value) {

            

            if ($key > 1){

                // Get user data

                $user = $CI->users_model->get_user($key);



                $residual_pp = $CI->residual->get_score($key);

                $unilevel = $CI->residual->unilevel($key);

                $is_active = is_active($key);

            

                

                $item = array(

                    'image' => ($user['image'] != '') ? "/uploads/{$user['image']}" : '/uploads/user-anonymous-disabled.gif',

                    'collapsed' => true,

                    'text' => array(

                        'name' => ucwords(strtolower("{$user['username']}")),

                        /*'title' =>  'PP : ' . $unilevel['pp'].' - BONUS : ' . $unilevel['bonus'],

                            'desc' => lang('users titel unilevel-rank').': ' ._rank_title($unilevel['rank']),

                            'contact' => array(

                                'val' => 'Max : ' . $unilevel['max_line'] .' - PG : ' . $unilevel['gp'] .' - PC : ' . $unilevel['score'] 

                        ),*/

                    ),

                    'HTMLclass' => ($is_active ? '' :'not_active'),

                    #'innerHTML' => '<img src="'.$img.'"><p class="node-name">'.ucwords(strtolower($user['username'])).'</p><a class="collapse-switch"></a>' .'<p>PP : ' . $residual_pp.' <p></p> Rank : ' . _rank_title($residual_pg['rank']).' <p></p> Max : ' . $residual_pg['max_line'] .' <p></p> PG : ' . $residual_pg['points'] . '</p>',

                    'children' => (is_array($value)) ? array_walk_multi_external($value,$keys) : array()

                 ); 

            }else{

                $item = $default;

            }

                    

            if ($keys){

                $root[$key] = $item;

            }else{

                $root[] = $item;

            }

            

        }

        

        return $root;



    }



}







if ( ! function_exists('_is_allowed'))

{

    

    /**

     * Return privilegies for an especific method

     *

     * @param  string $action read,write,update,delete

     * @return boolean

     */

    

    function is_allowed($action = 'read'){

        

        // Load core

        $CI = &get_instance();

        

        $CI->load->model('privilegies_model','privilegies');

        $router =& load_class('Router', 'core');

        

        $class = $router->fetch_class();

        $method =  $router->fetch_method();

        $profile = (isset($CI->user['profile'])) ? $CI->user['profile'] : 'guest';

        

        $rights = $CI->privilegies->get($class,$method,$action);

        

        if ($rights){

            $allowed = explode(',',$rights->$action);

            return (in_array($profile,$allowed)) ? true : false;

        }

        return false;

    }

}



if ( ! function_exists('_rank_title'))

{



    /**

     * Return trader info for a user

     *

     * @param  Int id_user

     * @return string

     */



    function _rank_title($rank = null){

        

        $ranks = array(

            0 => 'Entrepreneur',

            1 => 'Assistant',

            2 => 'Executive',

            3 => 'Senior',

            4 => 'Ruby',

            5 => 'Diamond',

            6 => 'Double Diamond',

            7 => 'Triple Diamond',

            8 => 'Red Diamond',

            9 => 'Black Diamond',

            10 => 'Grand Diamond',

            11 => 'Grand Black Diamond'

        );

        

        if ($rank != '' || $rank == 0){

            return $ranks[$rank];

        }

                

    }

    

}



if ( ! function_exists('_trader_info'))

{



    /**

     * Return trader info for a user

     *

     * @param  Int id_user

     * @return string

     */



    function _trader_info($user_id = null){

        

        if(!$user_id){

            // Load core

            $CI = &get_instance();

            $CI->load->library("rank");



            $user_id = $CI->user['id'];

            $rank = $CI->rank->get($user_id);

        

            if(!$rank['rank']){

                $rank['rank'] = 0;

            }

            return _rank_title($rank['rank']);

        }

        

        $rank = _get_calculate_rank($user_id);

        

        return _rank_title($rank);

    }

}



if ( ! function_exists('_get_calculate_rank'))

{

    

    function _get_calculate_rank($user_id){

        

        // Load core

        $CI = &get_instance();

        $user_id = !$user_id ? $CI->user['id'] : $user_id;

        $CI->load->library("residual");

        // get user subscription

        $points = $CI->residual->get_score($user_id);

       

        if ($points){

            return _calculate_rank($points);

        }else{

            return 0;

        }

    }

}



if ( ! function_exists('_calculate_rank'))

{

    function _calculate_rank($points, $my_rank = 0){

        switch ($points) {

            case ($points >= 5000000):

                return 11; # grandblack diamond

                break;

            case ($points >= 2000000):

                return 10; # grand diamond

                break;

            case ($points >= 1000000):

                return 9; # black diamond

                break;

            case ($points >= 500000):

                return 8; # red diamond

                break;

            case ($points >= 300000):

                return 7; # triple diamante

                break;

            case ($points >= 120000):

                return 6; # doble diamante 

                break;

            case ($points >= 60000):

                return 5; # diamante 

                break;

            case ($points >= 30000):

                return 4; # ruby

                break;

            case ($points >= 15000):

                return 3; # senior

                break;

            case ($points >= 5000):

                return 2; # executivo

                break;    

            case ($points >= 2000):

                return 1; # assistant

                break;

            default:

                return $my_rank;        

                break;

        }

    }

}

if ( ! function_exists('privilegies_message'))

{

    function privilegies_message(){

        $CI = &get_instance();

        

        $CI->load->model('settings_model');

        

        $settings_field = $CI->settings_model->get_privilegies_message();

        if( $settings_field === FALSE ){

            return $CI->lang->line("access_denied");

        }else{

            $settings_field = (@unserialize($settings_field->value) !== FALSE) ? unserialize($settings_field->value) : $settings_field->value;

            foreach ($CI->session->languages as $language_key => $language_name)

            {

                if ($language_key == $CI->session->language){

                    return $settings_field[$language_key];

                }

            }

        } 

    }

}





if (!function_exists('is_active')) {

    function is_active($uid = null) {

        $CI = & get_instance();

        $CI->load->model('subscriptions_model', 'subscriptions');

        $user_id = ($uid) ? $uid : $CI->user['id'];



        $allowed = $CI->subscriptions->is_active($user_id);

        return $allowed;      



        //return is_preferencial_user( $user_id )? true: $allowed;

    }

}



if (!function_exists('is_first_login')) {

    function is_first_login($uid = null) {

        $CI = & get_instance();

        $CI->load->model('subscriptions_model', 'subscriptions');

        $user_id = ($uid) ? $uid : $CI->user['id'];

        $allowed = $CI->subscriptions->is_first_login($user_id);      

        return $allowed;

    }

}



if (!function_exists('is_preferencial_user')) {

    function is_preferencial_user($uid = null) {

        $CI = & get_instance();

        $CI->load->model('subscriptions_model', 'subscriptions');

        $user_id = ($uid) ? $uid : $CI->user['id'];

        $allowed = $CI->subscriptions->is_preferencial_user($user_id);      

        return $allowed;

    }

}