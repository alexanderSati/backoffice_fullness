<?php defined('BASEPATH') OR exit('No direct script access allowed');

/*
 * Created by: Juan Manuel Pinzon
 * Date : 06-02-2017
 */


class Faqs_model extends CI_Model {

    /**
     * @vars
     */
    private $_db;


    /**
     * Constructor
     */
    function __construct()
    {
        parent::__construct();
        
        $this->_db = new stdClass();

        // define primary table
        $this->_db->_name = 'faqs';
        
        // define fields in table and default values
        $this->_db->_fields = array(
            'id'            => null,
            'status'        => 'active',
            'spanish_question'      => null,
            'spanish_answer'  => null,
            'english_question'      => null,
            'english_answer'  => null,
            'portuguese_question'      => null,
            'portuguese_answer'  => null,
        );
    }


    /**
     * Return a  list of all available faqs
     *
     * @param  string $sort
     * @return array|boolean
     */
    function get_all($select = null, $sort = 'spanish_question',$where = null)
    {
        
        if ($select){
            $this->db->select($select);
        }
        
        $this->db->from($this->_db->_name);
        
        if ($where){
            $this->db->where($where);
        }

        $this->db->order_by($sort,'ASC');
        $faqs = $this->db->get()->result_array();
        
        if ($faqs){
            return $faqs;
        }
        
        return false;
    }
    
    function get($faq = null){
        
        if (is_numeric($faq)){
            $this->db->from($this->_db->_name);
            $this->db->where('id',$faq);
            $query = $this->db->get();
            
            return ($query->num_rows() > 0) ? $query->row_array() : false;
        }
        
    }
    
    function get_active(){
        return $this->get_all(null,'spanish_question',"`status`='active'");
    }

    /**
     * Get list of non-deleted users
     *
     * @param  int $limit
     * @param  int $offset
     * @param  array $filters
     * @param  string $sort
     * @param  string $dir
     * @return array|boolean
     */
    function get_all_row($limit = 0, $offset = 0, $filters = array(), $sort = 'base', $dir = 'ASC')
    {
        $sql = "
            SELECT SQL_CALC_FOUND_ROWS *
            FROM {$this->_db->_name }
            WHERE status != 'deleted'
        ";

        if ( ! empty($filters))
        {
            foreach ($filters as $key=>$value)
            {
                $value = $this->db->escape('%' . $value . '%');
                $sql .= " AND {$key} LIKE {$value}";
            }
        }

        $sql .= " ORDER BY {$sort} {$dir}";

        if ($limit)
        {
            $sql .= " LIMIT {$offset}, {$limit}";
        }

        $query = $this->db->query($sql);

        if ($query->num_rows() > 0)
        {
            $results['results'] = $query->result_array();
        }
        else
        {
            $results['results'] = NULL;
        }

        $sql = "SELECT FOUND_ROWS() AS total";
        $query = $this->db->query($sql);
        $results['total'] = $query->row()->total;

        return $results;
    }

    /**
     * Add a new faq
     *
     * @param  array $data
     * @return mixed|boolean
     */
    function add_faq($data = array())
    {
        if ($data)
        {
            
            // exclude innecesary keys
            $save = array(
                'spanish_question' => $data['spanish_question'],
                'english_question' => $data['english_question'],
                'spanish_answer' => $data['spanish_answer'],
                'english_answer' => $data['english_answer'],
                'portuguese_question' => $data['portuguese_question'],
                'portuguese_answer' => $data['portuguese_answer'],
                'status' => $data['status'],
            );

            $this->db->insert($this->_db->_name, $save);

            if ($id = $this->db->insert_id())
            {
                return $id;
            }
        }

        return FALSE;
    }

    /**
     * Edit an existing faq
     *
     * @param  array $data
     * @return boolean
     */
    function edit_faq($data = array())
    {
        if ($data)
        {
            $update = array(
                'spanish_question' => $data['spanish_question'],
                'english_question' => $data['english_question'],
                'spanish_answer' => $data['spanish_answer'],
                'english_answer' => $data['english_answer'],
                'portuguese_question' => $data['portuguese_question'],
                'portuguese_answer' => $data['portuguese_answer'],
                'status' => $data['status'],
            );

            $this->db->where('id', $data['id']);
            $this->db->update($this->_db->_name, $update);

            if ($this->db->affected_rows())
            {
                return TRUE;
            }
        }

        return FALSE;
    }

    /**
     * Soft delete an existing user
     *
     * @param  int $id
     * @return boolean
     */
    function delete_faq($id = NULL)
    {
        if ($id)
        {
            $sql = "
                UPDATE {$this->_db->_name}
                SET
                    status = 'deleted'
                WHERE id = " . $this->db->escape($id) . "
                    AND id > 1
            ";

            $this->db->query($sql);

            if ($this->db->affected_rows())
            {
                return TRUE;
            }
        }

        return FALSE;
    }
   

}
