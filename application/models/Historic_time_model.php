<?php
/**
*/
class Historic_time_model extends CI_Model {
  function __construct() {
    parent::__construct();
  }

  function get($select = "*", $array_where = false, $array_like = false, $first = false, $offset = false, $order_by = false) {

    $data = array();

    if( $order_by != false)
    {
      $order = key($order_by);

      if ($order != null)
      {
        $sort = $order_by[$order];
        $this -> db -> order_by($order, $sort);
      }
    }

    $this -> db -> select($select);
    $this -> db -> from('historic_time_order');

    if($array_where != false)
    $this -> db -> where($array_where);

    if($array_like != false)
    $this -> db -> like($array_like);

    if($offset != false)
    {
      $this -> db -> limit($offset, $first);
    }

    $query = $this -> db -> get();

    if ($query -> num_rows() > 0)
    {
      foreach ($query->result() as $rows)
      {
        $data[] = $rows;
      }

      $query -> free_result();

      return $data[0];

    }else {
      return array();
    }
  }

  function insert($data_array) {
    $data_array['created_at']=date('Y-m-d H:i:s');
    $data_array['updated_at']=date('Y-m-d H:i:s');
    $this -> db -> insert('historic_time_order', $data_array);
    return $this -> db -> insert_id();
  }

  public function remove($arr_where) {
    $this -> db -> where($arr_where);
    $this -> db -> delete('historic_time_order');
    return $this->db->affected_rows();
  }

  function update($data_array, $array_where) {
    $this -> db -> where($array_where);
    $this -> db -> update('historic_time_order', $data_array);
    return $this -> db -> affected_rows();
  }
}
?>
