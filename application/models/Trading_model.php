<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of Trading_model
 *
 * @author Julian David Serna Echeverri
 */

class Trading_model extends CI_Model{
    
    
    function __construct() {
        parent::__construct();
        
        $this->table_orders = new stdClass();
        $this->table_orders->name = "trading_orders";
        $this->table_orders->fields = array(
            'id_user'   => null,
            'date'      => date("Y-m-d"),
            'time'      => date("H:i:s"),
            'type'      => null,
            'amount'    => 0,
            'coin'      => '',
            'price'     => 0,
            'fee'       => 0,
            'total_usd' => 0,
            'total_btc' => 0,
            'status'    => 'active'
        );
        
        $this->table_history = new stdClass();
        $this->table_history->name = "trading_history";
        $this->table_history->fields = array(
            'user_buy'   => null,
            'user_sell'   => null,
            'date'      => date("Y-m-d"),
            'time'      => date("H:i:s"),
            'type'      => null,
            'amount'    => 0,
            'coin'      => '',
            'price'     => 0,
            'fee'       => 0,
            'total_usd' => 0,
            'total_btc' => 0,
            'profit'    => 0,
            'status'    => 'active'
        );
        
        $this->allowed_types = array( 'buy', 'sell' );
    }
    
    function insert_orders($data = array()){
        $insert = false;
        
        if ($data){
            $fields = array_replace($this->table_orders->fields, $data);

            if(isset($fields['id_user']) && in_array($fields['type'], $this->allowed_types)){
                $insert = $this->db->insert($this->table_orders->name, $fields);
            }
        }
        if ($insert){
            return $this->db->insert_id();
        }else{
             return false;
        }
    }
    
    function insert_history($data = array()){
        $insert = false;
        
        if ($data){
            $fields = array_replace($this->table_history->fields, $data);

            if(isset($fields['id_user']) && in_array($fields['type'], $this->allowed_types)){
                $insert = $this->db->insert($this->table_history->name, $fields);
            }
        }
        if ($insert){
            return $this->db->insert_id();
        }else{
             return false;
        }
    }
    
    function get_orders($where = array(), $limit = null, $order_by = 'date', $order = 'DESC'){
        
        $this->db->from($this->table_orders->name);
        
        if($where){
            $this->db->where($where);
        }
        
        if($limit){
            $this->db->limit($limit);
        }
        
        $this->db->order_by($order_by,$order);
        $query = $this->db->get();
        
        return ($query->num_rows() > 0) ? $query->result_array() : array();
        
    }
    
    function get_history($where = array(), $limit = null, $order_by = 'date', $order = 'DESC'){
        
        $this->db->from($this->table_history->name);
        
        if($where){
            $this->db->where($where);
        }
        
        if($limit){
            $this->db->limit($limit);
        }
        
        $this->db->order_by($order_by,$order);
        $query = $this->db->get();
        
        return ($query->num_rows() > 0) ? $query->result_array() : array();
        
    }
    
    function get_row_orders($id = null){
        
        if ($id){
            $this->db->from($this->table_orders->name);
            $this->db->where('id',$id);
            $this->db->limit(1);
            $query = $this->db->get();
            return ($query->num_rows() > 0) ? $query->row_array() : array();
        }
        
    }
    
    function get_row_history($id = null){
        
        if ($id){
            $this->db->from($this->table_history->name);
            $this->db->where('id',$id);
            $this->db->limit(1);
            $query = $this->db->get();
            return ($query->num_rows() > 0) ? $query->row_array() : array();
        }
        
    }
    
    function update_orders($id = null, array $data = array()){
        if ($id && $data){
            $this->db->where('id', $id);
            $this->db->update($this->table_orders->name,$data);

            if ($this->db->affected_rows())
            {
                return true;
            }
        }
        return false;
    }
    
    function update_history($id = null, array $data = array()){
        if ($id && $data){
            $this->db->where('id', $id);
            $this->db->update($this->table_history->name,$data);

            if ($this->db->affected_rows())
            {
                return true;
            }
        }
        return false;
    }
    
    function remove_orders($id = null) {
        if($id) {
            $this->db->where('id', $id);
            $this->db->delete($this->table_orders->name);
        }
    }
    
    function remove_history($id = null) {
        if($id) {
            $this->db->where('id', $id);
            $this->db->delete($this->table_history->name);
        }
    }
}