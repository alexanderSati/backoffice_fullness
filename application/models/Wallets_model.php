<?php defined('BASEPATH') OR exit('No direct script access allowed');
/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of Wallets_model
 *
 * @author Juan Manuel Pinzon
 */

class Wallets_model extends CI_Model{
    
    function __construct() {
        parent::__construct();
        
        $this->table = new stdClass();
        $this->table->name = "wallets";
        $this->table->fields = array(
            'id_user' => null,
            'BTC' => 0,
            'XMR' => 0,
            'ETH' => 0,
            'LTC' => 0,
            'TRUMP' => 0,
            'DASH' => 0,
            'USD' => 0,
            'SLR' => 0
        );
    }
    
    function insert($id = null){
        if($id){
            $data = $this->table->fields;
            $data['id_user'] = $id;
            $this->db->insert($this->table->name,$data);            
        }        
    }
    
    
    function get($id = null,$wallet = null){
        
        if ($id){
            $this->db->from($this->table->name);
            $this->db->where('id_user',$id);
            $this->db->limit(1);
            
            $query = $this->db->get();

            if ($query->num_rows() > 0){
                $wallets = $query->row_array();
                return ($wallet) ? ($wallets[$wallet]) : $wallets;
            }else{
                return array();
            }                    
        }
    }
    
    
    function delete(){
        
    }
    
    function update($id = null,$data = array()){
        if ($id && $data){
            $this->db->where('id_user',$id);
            $this->db->update($this->table->name,$data);
            return $this->db->affected_rows();
        }        
        return false;
    }
    
}
