<?php

defined('BASEPATH') OR exit('No direct script access allowed');

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of Transactions_model
 *
 * @author Juan Manuel Pinzon
 */
class Transactions_model extends CI_Model {

    function __construct() {
        parent::__construct();

        $this->table = new stdClass();
        $this->table->name = "transactions";
        $this->table->fields = array(
            'id_user' => null,
            'cod' => 'plan',
            'reference' => null,
            'hash' => null,
            'description' => null,
            'btc' => null,
            'rate' => null,
            'usd' => null,
            'wallet' => null,
            'type' => null,
            'status' => 'done',
            'reviewer' => null,
            'date' => date('Y-m-d H:i:s'),
            'last_updated' => date('Y-m-d H:i:s'),
        );

        $this->allowed_types = array(
            'payplan' => "REFPAYMENT",
            'renewal' => "RENPAYMENT",
            'bonus' => "NETPROFIT",
            'referral' => "REFPROFIT",
            'binary' => "TREEPROFIT",
            'refund' => "REFOUND",
            'payout' => "PAYOUT",
            'transfer' => "TRANSFER",
            'discount' => "DISCPROFIT",
            'deposit' => "BTCD",
            'coin-deposit' => "COINDEPOSIT",
            'trading' => "WTRADING",
            'rank' => "RANKPROFIT",
            'points' => "TREEPOINTS",
            'upgrade' => "UPGRADE",
            'subscription' => "SUBSCRIPTION",
            'tradingfee' => "TRADINGFEE",
            'monthly' => "MONTHLYFEE",
            'purchase_plan' => "PURCHASE_PLAN",
            'purchase' => "PURCHASE",
            'purchase_refund' => "PURCHASE_REFUND",
            'upgrade_bonus' => "UPGRADE_BONUS",
            'bonus_fullness_global' => 'BONUS_FULLNESS_GLOBAL',
            'infinity' => 'BONUS_INFINITY',
            'activation' => 'BONUS_ACTIVATION',
            'bonus_award' => 'BONUS_AWARD',
            'quarterly' => 'QUAITERLY',
            'wallet_payment'=> 'WALLET_PAYMENT',
            'bonus_external_sales' => 'BONUS_EXTERNAL_SALES'
        );
    }

    /*
     * Protected Methods
     */

    function insert($data = array(), $code = true, $custom = null) {

        $insert = false;
        $date = new DateTime();

        if ($data) {
            $fields["last_updated"] = date("Y-m-d H:i:s");
            $fields = array_replace($this->table->fields, $data);

            if (isset($fields['id_user']) && array_key_exists($fields['type'], $this->allowed_types)) {
                // Tokenize transaction
                $fields['cod'] = ($custom) ? $custom : $this->allowed_types[$fields['type']];

                $fields['cod'] .= $date->getTimestamp() . $fields['id_user'];
                $insert = $this->db->insert($this->table->name, $fields);
            }
        }
        if ($insert) {
            return ($code) ? $fields['cod'] : $this->db->insert_id();
        } else {
            return false;
        }
    }

    function get( $where = array(), $limit = null, $order_by = 'date', $order = 'DESC') {

        $this->db->from($this->table->name);

        if ($where) {
            $this->db->where($where);
        }

        if ($limit) {
            $this->db->limit($limit);
        }

        $this->db->order_by($order_by, $order);
        $query = $this->db->get();

        return ($query->num_rows() > 0) ? $query->result_array() : array();
    }

    public function get_rows($select = "*", $where = null,  $offset,  $limit, $order_by = null, $join = false  ){

        
        $this->db->select($select)
                        ->from( $this->table->name )
                        ->where($where)
                        ->limit($limit, $offset)
                        ->order_by($order_by);
        if($join){
            $this->db->join('users', 'users.id = transactions.id_user');
        }

        return $this->db->get()->result_array();

    }

    function get_where_row($where = array(), $order_by = 'date', $order = 'DESC') {
        if ($where) {

            $this->db->from($this->table->name);
            $this->db->where($where);
            $this->db->limit(1);
            $this->db->order_by($order_by, $order);
            $query = $this->db->get();

            return ($query->num_rows() > 0) ? $query->row_array() : array();
        }
        return false;
    }

    function get_row($id = null) {

        if ($id) {
            $this->db->from($this->table->name);
            $this->db->where('id', $id);
            $this->db->limit(1);
            $query = $this->db->get();
            return ($query->num_rows() > 0) ? $query->row_array() : array();
        }
    }

    function update($id = null, array $data = array()) {
        if ($id && $data) {
            $data["last_updated"] = date("Y-m-d H:i:s");
            $this->db->where('id', $id);
            $this->db->update($this->table->name, $data);

            if ($this->db->affected_rows()) {
                return true;
            }
        }
        return false;
    }

    public function get_sum_by_type_and_user($id_user, $type=null){
        $this->db->select("
            SUM(USD) as total,
            type
        ")
        ->from($this->table->name)
        ->where([
            "id_user"=>$id_user,
            "status"=>'done'
        ]);

        if( !empty( $type ) ){
            $this->db->where_in('type', $type);
        }

        $result = $this->db->group_by('type')->get()->result_array();
        if( !empty($result) ){
            $result_formated = [];
            foreach( $result as $key => $value ){
                $result_formated[$value["type"]] = $value["total"];
            }
            return $result_formated;
        }
        else{
            return [];
        } 
    }

    /*
     * End Protected methods
     */
}