<?php defined('BASEPATH') OR exit('No direct script access allowed');

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of Tokens_model
 *
 * @author Juan Manuel
 */
class Tokens_model extends CI_Model{
    
    const EXPIRE = "+15 minutes";
    
    function __construct() {
        parent::__construct();
        
        $this->table = new stdClass();
        $this->table->name = "tokens";
        
        $this->table->fields = array(
            'id_user'   => null,
            'token'     => null,
            'salt'      => null,
            'created'   => date("Y-m-d H:i:s"),
            'expire'   => date("Y-m-d H:i:s",  strtotime(self::EXPIRE)),
            'status'    => 'active'
        );
        
    }
    
    function insert(array $data = array()){
        if ($data){
            
            $values = array_replace($this->table->fields, $data);
            $this->db->insert($this->table->name,$values);
                if ($id = $this->db->insert_id()){
                    return $id;
                }
            return false;
        }
        
    }
    
    function get($parameters = array()){

        if($parameters){
            $this->db->from($this->table->name);
            $this->db->where($parameters);
            $query = $this->db->get();
                   
            if ($query->num_rows() == 1) {
                return $query->row_array();
            } elseif ($query->num_rows() > 1) {
                return $query->result_array();
            } else {
                return false;
            }
        }
    }
    
    function update($id = null, $data = array()){
        if ($id and $data){
            $this->db->where('id',$id);
            $this->db->update($this->table->name,$data);
            return $this->db->affected_rows();
        }
    }
}
