<?php defined('BASEPATH') OR exit('No direct script access allowed');

class Point_traceability_model extends CI_Model {
    
    function __construct()
    {
        parent::__construct();

        // define primary table
        $this->_db = 'point_traceability';
    }

    function get($select = null, $where = [])
    {

        $this->db->from($this->_db);

        if ($select){            
            $this->db->select($select);
        }
       
        if ($where){
            $this->db->where($where);
        }
       
        $query = $this->db->get();
    
        if ($query->num_rows() == 1){
            return $query->row_array();
        }elseif($query->num_rows() > 1){
            return $query->result_array();
        }else{
            return [];
        }
    }
}