<?php defined('BASEPATH') OR exit('No direct script access allowed');

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of Privilegies
 *
 * @author Juan Manuel Pinzon
 */
class Privilegies_model extends CI_Model {
    //put your code here
    
    function __construct() {
        parent::__construct();
        
        $this->table = new stdClass();
        $this->table->name = "privilegies";
        
    }
    
    
    /**
     * Return privilegies for an especific class y method
     *
     * @param  string $class
     * @param  string $method
     * @param  string $action read,write,update,delete
     * 
     * @return boolean
     */
    
    function get($class = null ,$method = null, $action = null){
        
        if ($class && $method){
            if ($action){
                $this->db->select("{$action}");
            }
            $this->db->from($this->table->name);
            $this->db->where('class', $class);
            $this->db->where('method', $method);
            return $this->db->get()->row();
        }
    }

    /**
     * Return a  list of all available privilegies
     *
     * @param  string $sort
     * @return array|boolean
     */
    function get_all($select = null, $sort = 'id',$where = null)
    {

        if ($select){
            $this->db->select($select);
        }

        $this->db->from($this->table->name);

        if ($where){
            $this->db->where($where);
        }

        $this->db->order_by($sort,'ASC');
        $privilegies = $this->db->get()->result_array();

        if ($privilegies){
            return $privilegies;
        }

        return false;
    }

    /**
     * Edit an existing privilegies
     *
     * @param  array $data
     * @param  int $id
     * @return boolean
     */
    function edit_privilegies($data = array(), $id = null)
    {
        if ($data and $id)
        {
            $this->db->where('id', $id);
            $this->db->update($this->table->name, $data);

            if ($this->db->affected_rows())
            {
                return TRUE;
            }
        }

        return FALSE;
    }
    
    
}
