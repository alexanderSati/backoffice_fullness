<?php defined('BASEPATH') OR exit('No direct script access allowed');

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of Tokens_model
 *
 * @author Juan Manuel
 */
class Ranks_model extends CI_Model{
    
    
    function __construct() {
        parent::__construct();
        
        $this->table = new stdClass();
        $this->table->name = "ranks";
        
        $this->table->fields = array(
            'id_user'   => null,
            'rank'     => 0,
            'profit'   => 0,
            'last_updated' => date("Y-m-d H:i:s"),
        );
        
    }
    
    function insert($id = null){
        if ($id){
            $data = $this->table->fields;
            $data['id_user'] = $id;
            $this->db->insert($this->table->name,$data);
                if ($id = $this->db->insert_id()){
                    return $id;
                }
            return false;
        }
    }
    
    function get($where){
        if($where){
            $this->db->from($this->table->name);
            if($where){
                $this->db->where($where);
            }
            $query = $this->db->get();
            
            if ($query->num_rows() == 1){
                return $query->row_array();
            }elseif($query->num_rows() > 1){
                return $query->result_array();
            }else{
                return false;
            }
        }
    }
    
    function update($id = null, $data = array()){
        if ($id and $data){
            $this->db->where('id_user',$id);
            $this->db->update($this->table->name,$data);
            return $this->db->affected_rows();
        }
    }

    function add_bonus($id = null, $bonus = null)
    {
        if ($id and $bonus) {
            $this->db->select('left_points, right_points');
            $this->db->from('tree');
            $this->db->where('id_user', $id);
            $result = $this->db->get();

            if($result['left_points'] > $result['right_points'])
            {
                $sum = array_sum($bonus);

                $result['left_point'] += $sum;
            }
            elseif($result['right_points'] > $result['left_point'])
            {
                $sum = array_sum($bonus);

                $result['right_points'] += $sum;
            }

            $this->db->where('id_user', $id);
            $this->db->update('tree', array(
                'left_point' => $result['left_point'],
                'right_points' => $result['left_point']
            ));
        }
    }
}
