<?php defined('BASEPATH') OR exit('No direct script access allowed');

class Residual_model extends CI_Model {
    private $_db;

    function __construct()
    {
        parent::__construct();
        $this->_db = new stdClass();
        $this->_db->name = "residual";
        $this->_db->point_traceability = "point_traceability";
        
        
        $this->_db->_fields = [
        	'id_user' => 0,
            'points' => 0,
            'description' => ''
        ];

        $this->_db->_fields_point_traceability = [
        	'id_user' => 0,
            'points' => 0,
            'type' => 'residual'
        ];

    }

    public function get_binary_points($id_user)
    {
        $query = $this->db->from($this->_db->point_traceability)
            ->select('SUM(points) AS total_points')
            ->where(['id_user' => $id_user, 'type' => 'binary'])
            ->get();
        
        return $query->row_array();
    }
    public function calculate_bonus_for_binary_points($points = 0)
    {
        $return = [];
        switch (true) {
            case ($points >= 3000 && $points < 12000):
                $return = 1;
                break;
            case ($points >= 12000 && $points < 40000):
                $return = 2;
                break;
            case ($points >= 40000 && $points < 90000):
                $return = 3;
                break;
            case ($points >= 90000 && $points < 200000):
                $return = 4;
                break;
            case ($points >= 200000 && $points < 600000):
                $return = 5;
                break;
            case ($points >= 600000 && $points < 1400000):
                $return = 6;
                break;
            case ($points >= 1400000 && $points < 3000000):
                $return = 7;
                break;
            case ($points >= 3000000 && $points < 7000000):
                $return = 8;
                break;
            case ($points >= 7000000 && $points < 7000000):
                $return = 9;
                break;
            case ($points >= 7000000):
                $return = 10;
                break;
            default:
                $return = 0;
                break;
        }
        return $return;
    }
    public function insert_settlement($data = []){
        if($data){
            $values = array_replace($this->_db->_fields_point_traceability, $data);
            
            $this->db->insert($this->_db->point_traceability,$values);
            
            if ($id = $this->db->insert_id())
            {                
                return $id;
            }
        }
        return false;
    }

    function insert($data = []){
        if($data){
            $values = array_replace($this->_db->_fields, $data);
            
            $this->db->insert($this->_db->name,$values);
            
            if ($id = $this->db->insert_id())
            {
                return $id;
            }
        }
        return false;
    }

    function get($select = null, $where = [], $start = null, $end = null, $limit = null, $order_by = 'id_residual', $order = 'ASC', $distinct = false, $cron = false, $table = null, $group_by = null){
        if(is_null($table)){
            $table = $this->_db->name;
        }
    
        $this->db->from($table);
        
        if ($select){
            if($distinct){
                $this->db->distinct();
                $this->db->select($select);}
            $this->db->select($select);
        }
        if ($cron){
            $this->db->join('users', "users.id = {$table}.id_user", 'right')->where('(users.infinity_bonus < "' . date('Y-m-t') . '" OR users.infinity_bonus IS NULL)');;
        }
        if ($where){
            $this->db->where($where);
        }

        if($start && $end){
            $this->db->where("{$table}.date BETWEEN '{$start}' AND '{$end}'");
        }
        
        if ($order_by && $order){
            $this->db->order_by($order_by,$order);
        }
        if($limit){
            $this->db->limit($limit);
        }
        if($group_by){
            $this->db->group_by($group_by);
        }
        $query = $this->db->get();
    
        if ($query->num_rows() == 1){
            return $query->row_array();
        }elseif($query->num_rows() > 1){
            return $query->result_array();
        }else{

            return [];
        }
    }
    
    function update($id = null, $data = []){
        if ($id && $data){
            $this->db->where('id_user',$id);
            $this->db->update($this->table->name,$data);
            return $this->db->affected_rows();
        }
        return false;
    }
}
        