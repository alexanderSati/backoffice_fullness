<?php



defined('BASEPATH') OR exit('No direct script access allowed');



class Orders_model extends CI_Model
{

    private $table;

    function __construct(){
        parent::__construct();
        $this->table ="shopping_order";

    }

    /**
     * Carlos Aguirre 2019-07-17 14:58:35
     * Obtiene registros de la tabla `shopping_orders`
     */
    public function get($select, $where, $limit=null){

        return $this->db->select(
            $select
        )->from(
            $this->table
        )->where(
            $where
        )->limit(
            $limit
        )->get()->result_array();

    }


    /** 
     * Carlos Aguirre 2019-07-17 14:53:42
     * Obtiene el listado de compras realizadas por los usuario
     */
    public function get_orders($where){

        $this->db->select('
                        DATE_FORMAT(shopping_order.created, "%Y/%m/%e %r") as "created",
                        shopping_order.shopping_order_id,
                        shopping_order.users_id_referal,
                        ROUND(shopping_order.total_value, 2) as total_value, 
                        ROUND(shopping_order.total_discount, 2) as total_discount, 
                        users.first_name,
                        users.last_name,
                        users.profile,
                        shopping_order.state,
                        IF(shiping_details.id IS NULL, 0, 1) as "mayorista",
                        payment_method.status as "payment_status",
                        ifnull( sp.name, "" ) as wholesaler
                        ')
                    ->from('shopping_products')
                    ->join(
                        'shopping_order',
                        'shopping_order.shopping_order_id = shopping_products.shopping_order_id',
                        'left'
                    )
                    ->join(
                        'users',
                        'users.id = shopping_order.users_id',
                        'left'
                    )
                    ->join(
                        'shiping_details',
                        'shiping_details.id_user = users.id',
                        'left'
                    )

                    ->join(
                        'shiping_details sp',
                        'shopping_products.shipping_details_id = sp.id',
                        'left'
                    )
                    ->join(
                        'payment_method',
                        'payment_method.shopping_order_id = shopping_order.shopping_order_id',
                        'left'
                    )
                    ->where($where)
                    ->group_by('shopping_order.shopping_order_id')
                    ->order_by("shopping_order.state DESC, shopping_order.created DESC");


        return $this->db->get()->result_array();

    }


    /**
     * Carlos Aguirre 2019-07-17 14:42:18
     * Obtiene el listado de productos que se adquirieron 
     * en una orden
     */
    public function get_items_order($shopping_order_id){



        $this->db->select(
                    "
                    product.product_id,
                    product.product_image,
                    product.product_name,
                    shopping_products.quantity,
                    ROUND(shopping_products.value, 2) as value,
                    product.residual_points,
                    product.binary_points,
                    product.product_name,
                    ROUND((shopping_products.quantity * shopping_products.value), 2) as total_product,
                    ROUND(shopping_order.total_value, 2) as total_value
                    "
                    )
                    ->from('shopping_products')
                    ->join(
                        'product', 
                        "product.product_id = shopping_products.product_id"
                    )
                    ->join(
                        'shopping_order',
                        'shopping_order.shopping_order_id = shopping_products.shopping_order_id'
                    )
                    ->where("shopping_products.shopping_order_id",$shopping_order_id);


        return $this->db->get()->result_array();

    }







    public function get_total_order($shopping_order_id){

        $total = $this->db->select('sum(quantity * value ) as total')
                            ->from('shopping_products')
                            ->where([
                                'value >='=>0,
                                'shopping_order_id'=>$shopping_order_id
                            ])
                            ->get()
                            ->first_row('array');


        if( empty($total) || $total['total'] == null ){
            return 0;
        }
        else{

            return  $total['total'];

        }

    }

    /**
     * Carlos Aguirre 2019-07-17 08:24:19
     * actualiza un registro de la base de datos
     */
    public function update( $data, $where){
  
        return $this->db->update("shopping_order", $data, $where);
  
    }

    /**
     * Carlos Aguirre 2020-02-20 15:30:00
     * Obtiene el nombre del usuario que aprobó una
     * orden de compra en el sistema
     * @param int $shopping_order_id
     * @return Array
     */
    public function get_approved_user( $shopping_order_id ){
        $result = $this->db->select("
            u.id,
            u.username,
            u.first_name,
            u.last_name
        ")
        ->from("users u")
        ->join( "shopping_order so", "u.id = so.id_user_approved" )
        ->where("so.shopping_order_id", $shopping_order_id)
        ->get()->first_row('array');
        return $result;
    }

    /**
     * Carlos Aguirre 2020-02-20 15:39:27
     * Obtiene el estado de una orden ( entregado ) 
     * 0->no entregado
     * 1->entregado
     * @param int $shopping_order_id
     * @return Array
     */
    public function get_delivered_order( $shopping_order_id ){
        $result = $this->db->select("
            so.product_delivered
        ")
        ->from("shopping_order so")
        ->where("so.shopping_order_id", $shopping_order_id)
        ->get()->first_row('array');


        return $result['product_delivered']==1;
    }



}