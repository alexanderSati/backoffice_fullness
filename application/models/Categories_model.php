<?php defined('BASEPATH') OR exit('No direct script access allowed');

class Categories_model extends CI_Model {

    /**
     * @vars
     */
    private $_db;


    /**
     * Constructor
     */
    function __construct()
    {
        parent::__construct();

        $this->load->library('wallet');

        // define primary table
        $this->_db = 'categories';
    }


    /**
     * Get list of non-deleted categories
     *
     * @param  int $limit
     * @param  int $offset
     * @param  array $filters
     * @param  string $sort
     * @param  string $dir
     * @return array|boolean
     */

    function get_all($limit = 0, $offset = 0, $filters = array(), $sort = 'category_id', $dir = 'DESC')
    {
        $sql = "
            SELECT SQL_CALC_FOUND_ROWS *
            FROM {$this->_db}
            WHERE deleted = '0'
        ";

        if ( ! empty($filters))
        {
            foreach ($filters as $key=>$value)
            {             
                    $value = $this->db->escape('%' . $value . '%');
                    $sql .= " AND {$key} LIKE {$value}";
            }
        }
        
        $sql .= " ORDER BY {$sort} {$dir}";

        if ($limit)
        {
            $sql .= " LIMIT {$offset}, {$limit}";
        }

        $query = $this->db->query($sql);

        if ($query->num_rows() > 0)
        {
            $results['results'] = $query->result_array();
        }
        else
        {
            $results['results'] = NULL;
        }

        $sql = "SELECT FOUND_ROWS() AS total";
        $query = $this->db->query($sql);
        $results['total'] = $query->row()->total;

        return $results;
    }

    function get_all_external($limit = 0, $offset = 0, $filters = array(), $sort = 'category_id', $dir = 'DESC')
    {
        $sql = "
            SELECT SQL_CALC_FOUND_ROWS *
            FROM {$this->_db}
            WHERE category_id <> '28' AND category_id <> '27'
        ";

        if ( ! empty($filters))
        {
            foreach ($filters as $key=>$value)
            {             
                    $value = $this->db->escape('%' . $value . '%');
                    $sql .= " AND {$key} LIKE {$value}";
            }
        }
        
        $sql .= " ORDER BY {$sort} {$dir}";

        if ($limit)
        {
            $sql .= " LIMIT {$offset}, {$limit}";
        }

        $query = $this->db->query($sql);

        if ($query->num_rows() > 0)
        {
            $results['results'] = $query->result_array();
        }
        else
        {
            $results['results'] = NULL;
        }

        $sql = "SELECT FOUND_ROWS() AS total";
        $query = $this->db->query($sql);
        $results['total'] = $query->row()->total;

        return $results;
    }



    /**
     * Get specific category
     *
     * @param  int $id
     * @return array|boolean
     */
    function get_category($id = NULL)
    {
        if ($id)
        {
            
            if (is_numeric($id)){
                $sql = "
                    SELECT *
                    FROM {$this->_db}
                    WHERE category_id = " . $this->db->escape($id);
            }

            $query = $this->db->query($sql);

            if ($query->num_rows())
            {
                return $query->row_array();
            }
        }

        return FALSE;
    }

    /**
     * Edit an existing category
     *
     * @param  array $data
     * @return boolean
     */
    function edit_category($data = array())
    {
        if ($data)
        {
            $this->db->where('category_id',  $data['category']['category_id']);
            $this->db->update($this->_db, array(
                'name' => $data['category']['name'],
                'description' => $data['category']['description'],
                'user_updated' => $data['id_user']
            ));

            if ($this->db->affected_rows())
            {
                return TRUE;
            }
        }

        return FALSE;
    }

     /**
     * Add a new category
     *
     * @param  array $data
     * @return mixed|boolean
     */
    function add_category($data = array())
    {
        if ($data)
        {

            $sql = "
                INSERT INTO {$this->_db} (
                    name,
                    description,
                    user_created
                ) VALUES (
                    " . $this->db->escape($data['category']['name']) . ",               
                    " . $this->db->escape($data['category']['description']) . ",
                    " . $this->db->escape($data['id_user']). " 
                )
            ";

            $this->db->query($sql);

            if ($id = $this->db->insert_id())
            {
                return $id;
            }
        }

        return FALSE;
    }

    /**
     * Get categorias for dropdpwm
     * @return array
     */

    function getCategories()
    {
       $this->db->select('*');
       $this->db->from($this->_db);
       $this->db->where('deleted', 0);

       $query = $this->db->get();
       $results = $query->result_array();

       return array_combine(array_column($results, 'category_id'),array_column($results, 'name'));
    }
    /**
     * Get category by name
     */
    function get_category_name($name)
    {
        $this->db->select('*');
        $this->db->from($this->_db);
        $this->db->where('name', $name);

        $query = $this->db->get();

        $result = $query->row_array();
    
        return $result;
    }

}