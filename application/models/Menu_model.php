<?php
class Menu_model extends CI_Model {
    
    /**
     * @vars
     */
    private $_db;

    /**
     * Constructor
     */
    function __construct()
    {
        parent::__construct();
        $this->_db = "menus";
    }


    public function all()
    {
	return $this->db->get($this->_db)->result_array();
    }
        
    public function profile($profile = null){
            
        $menus = array();
        
        if ($profile){
            $this->db->from($this->_db);
            $this->db->where('profile',$profile);
            $this->db->where('status',1);
            $menus = $this->db->get()->result_array();
        }
        
        return $menus;
           
    }

}