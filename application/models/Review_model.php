<?php
class Review_model extends CI_Model {
	function __construct() {
		parent::__construct();
	}

	function get($select = "*", $array_where = false, $array_like = false, $first = false, $offset = false, $order_by = false) {
		$data = array();
		if( $order_by != false){
			$order = key($order_by);
			if ($order != null) {
				$sort = $order_by[$order];
				$this -> db -> order_by($order, $sort);
			}
		}

		$this -> db -> select($select);
		$this -> db -> from('reviews');
		$this -> db -> join('users','idUser = idDom');

		if($array_where != false)
			$this -> db -> where($array_where);
		if($array_like != false)
			$this -> db -> like($array_like);
		if($offset != false){
			$this -> db -> limit($offset, $first);
		}

		$query = $this -> db -> get();
		if ($query -> num_rows() > 0) {
			foreach ($query->result() as $rows) {
				$data[] = $rows;
			}
			$query -> free_result();
			return $data;
		} else {
			return null;
		}
	}

	function total($array_where, $array_like) {
		$this -> db -> select('count(*) as total');
		$this -> db -> join('users','idUser = idDom');
		$this -> db -> where($array_where);
		$this -> db -> like($array_like);
		$this -> db -> from('reviews');
		$query = $this -> db -> get();
		$rows = $query -> result();
		$query -> free_result();
		return $rows[0] -> total;
	}

	function get_by_id($id) {
		$select = '*';
		$array_where = array('reviews.idReview' => $id);
		$array_like = array();
		$order_by = array();
		$result = $this -> get($select, $array_where, $array_like, 0, 1, $order_by);

		if($result){
			return $result[0];
		}else{
			return array();
		}
	}

	function get_by_fullname($name) {
		$select = 'reviews.idReview as id,reviews.name,reviews.created_at,
			reviews.email,reviews.phone,reviews.rating,reviews.comment,reviews.idOrder,users.fullname as dom';
		$array_where = array('users.fullname' => $name);
		$array_like = array();
		$order_by = array();
		return $this -> get($select, $array_where, $array_like, 0, 1000, $order_by);
	}

	function get_by_order($idOrder){
		$select = 'reviews.idReview as id,reviews.name,reviews.created_at,
			reviews.email,reviews.phone,reviews.rating,reviews.comment,reviews.idOrder,users.fullname as dom';
		$array_where = array('reviews.idOrder' => $idOrder);
		$array_like = array();
		$order_by = array();
		return $this -> get($select, $array_where, $array_like, 0, 1000, $order_by);
	}

	function insert($data_array) {
		$data_array['created_at']=date('Y-m-d H:i:s');
		$this -> db -> insert('reviews', $data_array);
		return $this->db->insert_id();
	}

	function getRating($idDom){
		$select = 'SUM(reviews.rating) as totalRating, COUNT(reviews.idReview) as total';
		$arr_where = array('reviews.idDom' => $idDom);
		$result = $this -> get($select,$arr_where, null,0,1000,null);
		if ($result) {
			return $result[0];
		} else {
			return array();
		}
	}

	public function remove($arr_where) {
		$this -> db -> where($arr_where);
		$this -> db -> delete('reviews');
		return $this->db->affected_rows();
	}

	public function remove_by_id($id) {
		$array_where = array('idReview' => $id);
		return $this -> remove($array_where);
	}
}
?>
