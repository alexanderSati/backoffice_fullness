<?php defined('BASEPATH') OR exit('No direct script access allowed');





/*


 * Created by: Juan Manuel Pinzon


 * Date : 06-02-2017


 */








class Referrals_model extends CI_Model {





    /**


     * @vars


     */


    private $_db;








    /**


     * Constructor


     */


    function __construct()


    {


        parent::__construct();


        


        $this->table = new stdClass();





        // define primary table


        $this->table->name = 'referrals';


        


        // define fields in table and default values


        $this->table->fields = array(


            'id_user'           => null,


            'id_referral'       => null,


            'id_ref_subscription'=> null,


            'base'              => 0,


            'value_rule1'       => null,


            'value_rule2'       => null,


            'max_value_rule'    => 0,


            'number_referrals'  => 0,


            'status'            => 'inactive'


        );


        


    }


   


    function insert(array $data = array()){


        if ($data){


            $data = array_replace($this->table->fields, $data);            


            return $this->db->insert($this->table->name,$data);


        }


    }





    public function query($sql){


        return $this->db->query($sql)->result("array");


    }





    function get($where = array()){


        


        $this->db->from($this->table->name);


        if ($where){


            $this->db->where($where);


        }





        $this->db->order_by('id','DESC');


        $this->db->limit(1);





        $query =  $this->db->get();





        return ($query->num_rows() > 0) ? $query->row_array() : array();


    }


    


    function get_all($where){


        


        if($where){


            $this->db->where($where);


        }


        


        return $this->db->get($this->table->name)->result_array();


    }





    function update($where = array(), array $data = array()){


        if ($where && $data){


            


            if ($where){


                $this->db->where($where);


            }


            


            $this->db->update($this->table->name,$data);


            


            if ($this->db->affected_rows())


            {


                return true;


            }


        }


        return false;


    }


    


    


    function get_directs($id = null){


        if($id){


            


            $sql = "select 


                count(*) total,


                COALESCE(sum(case when referral_side = 'left' then 1 else 0 end),0) left_count,


                COALESCE(sum(case when referral_side = 'right' then 1 else 0 end),0) right_count,


                GROUP_CONCAT(id_user SEPARATOR ',') users


            from {$this->table->name}


            where id_referral = {$id} AND status='active'";


        


            $query = $this->db->query($sql);


            return ($query->num_rows() > 0) ? $query->row_array() : array();


        }


        


        return false;


    }


    


    function get_referrals($id = null){


        if($id){


            


            $this->db->select('id_user,referral_side');


            $this->db->where('id_referral', $id);


            $this->db->where('status','active');


            $this->db->order_by('id_user','DESC');


            $this->db->from($this->table->name);


            $query = $this->db->get();


            return ($query->num_rows() > 0) ? $query->result_array() : array();


        }


        


        return false;


    }


    


    /**


     * Get directs referrals with user id and subscription id


     * 


     * @param null $id


     * @param null $subscription


     * @return arrray Direct referrals otherwise false


     */


    function get_directs_subscription($id = null, $subscription = null ){


        if($id && $subscription){


            $sql = "select 


                count(*) total,


                COALESCE(sum(case when referral_side = 'left' then 1 else 0 end),0) left_count,


                COALESCE(sum(case when referral_side = 'right' then 1 else 0 end),0) right_count,


                GROUP_CONCAT(r.id_user SEPARATOR ',') users


            from {$this->table->name} r


            JOIN subscriptions s on r.id_user = s.id_user


            WHERE 


            s.id_plan in (5,6,7,8)


            AND s.preferencial_user = '0'


            AND id_referral = {$id} AND r.status='active' AND id_ref_subscription={$subscription}";


        


            $query = $this->db->query($sql);


            return ($query->num_rows() > 0) ? $query->row_array() : [];


        }


        


        return false;


    }





    /**


     * Get unilevel parants


     */





    function tree_unilevel($items = array(), $id_user = null){


        $childs = array();    





        foreach ($items as  $value) {


            if($value['id_referral'] == $id_user){


                $childs[$value['id_user']] = $this->tree_unilevel($items, $value['id_user']);


            }


        }    


            


        return $childs;


    }





     /**


     * Get items


    */


    function get_items() {


        $this->db->select('id,id_user,id_referral');


        $this->db->from($this->table->name);


        $this->db->order_by('id_referral, id_user');





        $query = $this->db->get();





        return $query->result_array();


    }





}


