<?php defined('BASEPATH') OR exit('No direct script access allowed');

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of Profile_model
 *
 * @author Juan Manuel Pinzon
 */


class Profiles_model extends CI_Model {

    /**
     * @vars
     */
    private $_db;


    /**
     * Constructor
     */
    function __construct()
    {
        parent::__construct();
        
        $this->_db = "profiles";
    }
    
    
    function get_all(){
        
        
        $this->db->from($this->_db);
        $this->db->where('status','active');
        return $this->db->get()->result_array();
        
    }
    
    function insert(){
        
    }
    
    function update(){
        
    }
    
    function delete(){
        
    }
    
    
    
    
    
}