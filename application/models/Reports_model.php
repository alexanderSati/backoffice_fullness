<?php
class Reports_model extends CI_Model {

	function __construct() {
		parent::__construct();
	}
	
	function getExtrasInOrdersByAllie($startDate,$endDate,$idAllie) {
		$data = [];
		$this -> db -> select('SUM(fextra.price) as extra_price');
		$this -> db -> from('food_extra_orders as fextra');
		$this -> db -> join('food_orders as ford','ford.id = fextra.food_order_id');
		$this -> db -> join('foods as foo','foo.idFood = ford.food_id');
		$this -> db -> where("foo.allie_id = {$idAllie} and ford.created_at >= '{$startDate}' and ford.created_at <= '{$endDate} 23:59'");

		$query = $this -> db -> get();
		
		if ($query -> num_rows() > 0) {
			foreach ($query->result() as $rows) {
				$data[] = $rows;
			}
			$query -> free_result();
			return $data;
		} else {
			return [];
		}
	}

	function getTotalProducts($startDate,$endDate,$idAllie) {
		$data = [];

		$this -> db -> select_sum('ford.quantity','total_products');
		$this -> db -> from('orders as ord');
		$this -> db -> join('food_orders AS ford','ford.order_id = ord.idOrder');
		$this -> db -> join('foods as foo','ford.food_id = foo.idFood');
		$this -> db -> join('allies as al','al.idAllie = foo.allie_id');
		$this -> db -> where("ord.created_at >= '{$startDate}' AND ord.created_at <='{$endDate} 23:59' AND ord.state = 4 AND al.idAllie = {$idAllie}");
		$this -> db -> group_by('al.idAllie');

		$query = $this -> db -> get();

		if ($query -> num_rows() > 0) {
			foreach ($query->result() as $rows) {
				$data[] = $rows;
			}
			$query -> free_result();
			return $data;
		} else {
			return [];
		}
	}

	function getSalesAllies($startDate,$endDate){
		$data = [];
		
		$this -> db -> select('al.idAllie,al.name,al.shipfee,al.isRate,al.new_fit_rate,ord.idOrder as norder,COUNT(DISTINCT ord.idOrder) as total_orders,
			SUM((ford.price * ford.quantity) - ROUND((ford.price * ford.quantity) * (ford.discount_percent/100))) + COALESCE((SUM((fsize.price * fsize.quantity) - ROUND((fsize.price * fsize.quantity) * (ford.discount_percent/100)))),0) as total_sell');
		$this -> db -> from('orders AS ord');
		$this -> db -> join('food_orders AS ford','ford.order_id = ord.idOrder');
		$this -> db -> join('foods AS foo','ford.food_id = foo.idFood');
		$this -> db -> join('allies AS al','al.idAllie = foo.allie_id');
		$this -> db -> join('food_size_orders AS fsize','fsize.food_order_id = ford.id','left');
		$this -> db -> where("ord.created_at >= '{$startDate}' AND ord.created_at <='{$endDate} 23:59' AND ord.state = 4");
		$this -> db -> group_by('al.idAllie');

		$query = $this -> db -> get();

		if ($query -> num_rows() > 0) {
			foreach ($query->result() as $rows) {
				$data[] = $rows;
			}
			$query -> free_result();
			return $data;
		} else {
			return [];
		}
	}

	function getServicesAllies($startDate,$endDate){
		$this -> db -> select('COUNT(DISTINCT ord.idOrder) as total_services, SUM(ord.total_price) as total_sell_services,al.idAllie,al.name,al.isRate,al.new_fit_rate');
		$this -> db -> from('orders AS ord');		
		$this -> db -> join('users AS us','ord.user_id = us.idUser');
		$this -> db -> join('allies AS al','al.idAllie = us.idAllie');
		$this -> db -> where("ord.created_at >= '{$startDate}' AND ord.created_at <='{$endDate} 23:59' AND ord.state = 4 AND us.perm = 3 AND ord.content LIKE 'Servicio externo'");
		$this -> db -> group_by('al.idAllie');
		
		$query = $this -> db -> get();

		if ($query -> num_rows() > 0) {
			foreach ($query->result() as $rows) {
				$data[] = $rows;
			}
			$query -> free_result();
			return $data;
		} else {
			return [];
		}
	}

	function getPotencialClient($startDate, $endDate){
		$this -> db -> select('ord.full_name,ord.phone,COUNT(DISTINCT ord.idOrder) as total_orders, SUM(ord.total_price) as total_price');
		$this -> db -> from('orders AS ord');		
		$this -> db -> join('users AS us','ord.user_id = us.idUser');
		$this -> db -> where("ord.created_at >= '{$startDate}' AND ord.created_at <='{$endDate} 23:59' AND ord.state = 4 AND us.perm = 2 AND ord.content LIKE 'Servicio externo'");
		$this -> db -> having("COUNT(ord.idOrder) >= 9");
		$this -> db -> group_by('us.idUser');

		$query = $this -> db -> get();

		if ($query -> num_rows() > 0) {
			foreach ($query->result() as $rows) {
				$data[] = $rows;
			}
			$query -> free_result();
			return $data;
		} else {
			return [];
		}
	}

	function getSalesRequested($startDate, $endDate){
		$this -> db -> select('us.fullname, us.idUser, COUNT(DISTINCT ord.idOrder) as total_orders, SUM(ord.shipping_fee) as total_sell');
		$this -> db -> from('orders AS ord');		
		$this -> db -> join('users AS us','ord.user_id = us.idUser');
		$this -> db -> where("ord.created_at >= '{$startDate}' AND ord.created_at <='{$endDate} 23:59' AND ord.state = 4 AND us.perm = 2 AND ord.content= 'Servicio externo' AND us.idAllie = 0");
		$this -> db -> group_by('us.idUser');

		$query = $this -> db -> get();		
		if ($query -> num_rows() > 0) {
			foreach ($query->result() as $rows) {
				$data[] = $rows;
			}
			$query -> free_result();
			return $data;
		} else {
			return [];
		}
	}

	function getCollectExternalDom($startDate, $endDate, $idDom){
		$this -> db -> select('COALESCE(SUM(ord.shipping_fee),0) as collect_dom, COALESCE(SUM(ord.collect_company),0) as collect_company');		
		$this -> db -> from('orders AS ord');		
		$this -> db -> join('users AS us','ord.dom_id = us.idUser');
		$this -> db -> where("ord.created_at >= '{$startDate}' AND ord.created_at <='{$endDate} 23:59' AND ord.state = 4");
		$this -> db -> where("ord.content = 'Servicio externo' AND ord.dom_id ={$idDom}");
		$this -> db -> group_by('ord.dom_id');

		$query = $this -> db -> get();		

		if ($query -> num_rows() > 0) {
			foreach ($query->result() as $rows) {
				$data[] = $rows;
			}
			$query -> free_result();
			return $data[0];
		} else {
			return [];
		}
	}

	function getCollectAppDom($startDate, $endDate, $idDom){
		$this -> db -> select('COALESCE(SUM(ord.shipping_fee),0) as collect_app');		
		$this -> db -> from('orders AS ord');				
		$this -> db -> where("ord.created_at >= '{$startDate}' AND ord.created_at <='{$endDate} 23:59' AND ord.state = 4");
		$this -> db -> where("ord.content IS NULL AND ord.dom_id ={$idDom}");		

		$query = $this -> db -> get();		

		if ($query -> num_rows() > 0) {
			foreach ($query->result() as $rows) {
				$data[] = $rows;
			}
			$query -> free_result();
			return $data[0];
		} else {
			return [];
		}
	}

	function getOrderDomiciliaryAllies($startDate, $endDate,$allie_id){
		$data = [];
		$this -> db -> select('ord.idOrder,ord.content,ord.message,ord.total_price,us.fullname,ord.created_at');
		$this -> db -> from ('orders AS ord');
		$this -> db -> join('users AS us', 'ord.dom_id = us.idUser');
		$this -> db -> where("ord.created_at >='{$startDate}' and ord.created_at <='{$endDate} 23:59' and ord.user_id = {$allie_id} and ord.state = 4 and ord.content = 'Servicio externo'");

		$query = $this -> db -> get();

		if ($query -> num_rows() > 0) {
			foreach ($query->result() as $rows) {
				$data[] = $rows;
			}
			$query -> free_result();
			return $data;
		} else {
			return [];
		}
	}

	function getTotalOrders($startDate, $endDate){
		$data = [];
		$this -> db -> select('us.fullname, us.idUser, us.rating,COUNT(DISTINCT ord.idOrder) as total_orders');
		$this -> db -> from('orders AS ord');
		$this -> db -> join('users AS us','ord.dom_id = us.idUser');
		$this -> db -> where("ord.created_at >= '{$startDate}' AND ord.created_at <='{$endDate} 23:59' AND ord.state = 4");
		$this -> db -> group_by("us.idUser");

		$query = $this -> db -> get();

		if ($query -> num_rows() > 0) {
			foreach ($query->result() as $rows) {
				$data[] = $rows;
			}
			$query -> free_result();
			return $data;
		} else {
			return [];
		}
	}

	function getOrderApp($startDate,$endDate,$dom){
		$data = [];		
		$this -> db -> select('ord.*,al.shipfee as shipfee,
			SUM((ford.price * ford.quantity) - ROUND((ford.price * ford.quantity) * (ford.discount_percent /100)))  + COALESCE((SUM((fsize.price * fsize.quantity) - ROUND((fsize.price * fsize.quantity) * (ford.discount_percent/100)))),0) + COALESCE((SELECT SUM(fextra.price) FROM `food_extra_orders` as fextra JOIN food_orders as ford on ford.id = fextra.food_order_id WHERE ford.order_id = ord.idOrder),0) as total_sell');		
		$this -> db -> from('orders AS ord');
		$this -> db -> join('food_orders AS ford','ford.order_id = ord.idOrder');
		$this -> db -> join('foods AS foo','ford.food_id = foo.idFood');						
		$this -> db -> join('allies AS al','foo.allie_id = al.idAllie');					
		$this -> db -> join('food_size_orders AS fsize','ford.id = fsize.food_order_id','left');
		$this -> db -> where("ord.created_at BETWEEN '{$startDate}' and '{$endDate} 23:59' and ord.state = 4 and ord.dom_id = {$dom}");
		$this -> db -> group_by('ord.idOrder');

		$query = $this -> db -> get();
		
		if ($query -> num_rows() > 0) {
			foreach ($query->result() as $rows) {
				$data[] = $rows;
			}
			$query -> free_result();
			return $data;
		} else {
			return [];
		}
	}

	function getExternalOrder($startDate,$endDate,$dom){
		$data = [];
		$this ->db ->select("ord.*,(ord.shipping_fee + ord.collect_company) as total_sell");		
		$this -> db -> from('orders AS ord');
		$this -> db -> where("ord.created_at >= '{$startDate}' AND ord.created_at <='{$endDate} 23:59' AND ord.state = 4");
		$this -> db -> where("ord.content ='Servicio externo' AND ord.dom_id ={$dom}");
		$query = $this -> db -> get();		
		if ($query -> num_rows() > 0) {
			foreach ($query->result() as $rows) {
				$data[] = $rows;
			}
			$query -> free_result();
			return $data;
		} else {
			return [];
		}
	}

	function getOrderAppAllies($startDate,$endDate,$allie){
		$data = [];
		$this -> db -> select('ord.*,al.shipfee as shipfee,
			SUM((ford.price * ford.quantity) - ROUND((ford.price * ford.quantity) * (ford.discount_percent /100)))  + COALESCE((SUM((fsize.price * fsize.quantity) - ROUND((fsize.price * fsize.quantity) * (ford.discount_percent/100)))),0) + COALESCE((SELECT SUM(fextra.price) FROM `food_extra_orders` as fextra JOIN food_orders as ford on ford.id = fextra.food_order_id WHERE ford.order_id = ord.idOrder),0) as total_sell');		
		$this -> db -> from('orders AS ord');
		$this -> db -> join('food_orders AS ford','ford.order_id = ord.idOrder');
		$this -> db -> join('foods AS foo','ford.food_id = foo.idFood');						
		$this -> db -> join('allies AS al','foo.allie_id = al.idAllie');					
		$this -> db -> join('food_size_orders AS fsize','ford.id = fsize.food_order_id','left');
		$this -> db -> where("ord.created_at BETWEEN '{$startDate}' and '{$endDate} 23:59' and ord.state = 4 and foo.allie_id = {$allie}");
		$this -> db -> group_by('ord.idOrder');

		$query = $this -> db -> get();

		if ($query -> num_rows() > 0) {
			foreach ($query->result() as $rows) {
				$data[] = $rows;
			}
			$query -> free_result();
			return $data;
		} else {
			return [];
		}
	}

	function getOrderServiceAllies($startDate,$endDate,$allie){
		$data = [];
		$this -> db -> select('ord.*');
		$this -> db -> from('orders AS ord');
		$this -> db -> join('users AS us','ord.user_id = us.idUser');
		$this -> db -> where("ord.created_at >= '{$startDate}' AND ord.created_at <='{$endDate} 23:59' AND ord.state = 4");
		$this -> db -> where("ord.content ='Servicio externo' AND us.idAllie ={$allie}");
		$query = $this -> db -> get();

		if ($query -> num_rows() > 0) {
			foreach ($query->result() as $rows) {
				$data[] = $rows;
			}
			$query -> free_result();
			return $data;
		} else {
			return [];
		}
	}
}
?>
