<?php
defined('BASEPATH') OR exit('No direct script access allowed');
class Shopping_order_model extends CI_Model
{

  function __construct() {
    parent::__construct();
    

    
  }

  /**
   * Carlos Aguirre 2019-07-17 15:13:46
   * @param int $user_id obtiene la cantidad de compras 
   * que han sido realizadas por un usuario
   */
  public function get_quantity_purchases_user($user_id){
    $cantidad = $this->db->select('count( shopping_order_id ) as number')
                  ->from('shopping_order')
                  ->where([
                    "users_id"=>$user_id,
                    "state"=>'approved'
                  ])
                  ->get()
                  ->first_row('array');

    return $cantidad["number"];
                  
  }

  /**
   * Consulta los registros de la tabla `shopping_order`
   */
  public function get( $select="*", $where, $limit=null, $order = null ){
    $result = $this->db->select($select)
                        ->from( 'shopping_order' )
                        //->join( 'users', 'users.id = shopping_order.users_id', 'left' )
                        ->where($where)
                        ->limit($limit)
                        ->order_by( $order )
                        ->get();
    if($this->db->affected_rows()==0){
        return [];
    }
    elseif( $this->db->affected_rows() > 1){
        return $result->result_array();
    }
    else{
        return $result->first_row('array');
    }
  }


  /**
   * Consulta los registros de la tabla `shopping_order`
   */
  public function get_shopping_products( $select="*", $where, $limit=null, $order = null ){
    $result = $this->db->select($select)
                        ->from( 'shopping_products' )
                        ->where($where)
                        ->limit($limit)
                        ->order_by( $order )
                        ->get();
    if($this->db->affected_rows()==0){
      return [];
    }
    else 
      return $result->result_array();
   
  }



  
   /**
   * Carlos Aguirre 2019-07-10 08:19:46
   * Inserta un registro en la tabla `shopping_order`
   */
  public function create($data){
    $data["created"] = date("Y-m-d H:i:s");
    $data["modified"] = date("Y-m-d H:i:s");
    $this->db->insert("shopping_order", $data);
    return $this->db->insert_id();
  }

  /**
   * Carlos Aguirre 2019-07-10 08:20:43
   * Inserta un registro en la tabla "shopping_products"
   */
  public function create_shopping_products($data){

      $this->db->insert("shopping_products", $data);
      return $this->db->insert_id();
  }


  /** Carlos Aguirre 2019-07-22 07:54:06
  *   @param $condicion un array con las condiciones de borrado que se pondran en el where
  *   @return void
  *   Descripcion: Borra registros de la base de datos dada una condicion
  **/
  public function eliminar( $condicion ){
    $this->db->where( $condicion );
    $this->db->delete( "shopping_order" );
  }


  /**
   * Carlos Aguirre 2019-07-22 08:37:03
   * Actualiza un registro de la tabla `shopping_order`
   */
  public function actualizar( $data ){
    $data["modified"] = date("Y-m-d H:i:s");
    $this->db->set( $data )
             ->where( [
              "shopping_order.shopping_order_id" => $data['shopping_order_id']
             ] )
             ->update( "shopping_order" );
  }



  /**
   * Carlos Aguirre 2019-07-22 14:39:57
   * Obtiene la cantidad que se a pagado con
   * wallets en una orden de compra
   * @param int $orden_id `shopping_products.shopping_order_id`
   * @return array
   */
  public function get_pago_wallet($orden_id){
    return $this->db->select("*")
                      ->from("shopping_products")
                      ->where([
                        "shopping_products.shopping_order_id"=>$orden_id,
                        "shopping_products.value <="=>0,
                      ])
                      ->limit(1)
                      ->get()
                      ->first_row("array");

  }


  /**
   * Carlos Aguirre 2019-07-30 08:39:50
   * Obtiene el listado de productos que se han adquirido de una orden
   * @return Array registros de la tabla `shopping_products`
   */
  public function get_items_order($select = "*", $where = null, $limit = null, $order = null){
    return $this->db->select($select)
                    ->from("shopping_products")
                    ->join( "product", "product.product_id = shopping_products.product_id	", "left" )
                    ->where(  $where)
                    ->limit($limit)
                    ->order_by($order)
                    ->get()
                    ->result_array();
  }

  /**
   * Carlos Aguirre
   * @param int $id_user
   * @return int  ( el total de compras realizadas en el mes )
   */
  public function get_total_purchase_user( $id_user ){


    $total =$this->db->query("
    SELECT
      sum(sp.quantity*sp.value) as total
    FROM shopping_products sp
    JOIN shopping_order so
      ON sp.shopping_order_id = so.shopping_order_id
    WHERE 
      sp.value >= 0
    AND 
      so.users_id = ".$this->db->escape($id_user)."
    AND
      so.state = 'approved'
    AND DATE(so.created) BETWEEN '".date('Y-m-')."01' and LAST_DAY(NOW()) ;
    ")->row_array();


    if(empty($total) || $total['total'] == null){
      return 0;
    }
    return floatval($total['total']);
  }


  /**
   * Actualiza en la tabla de productos relacionados
   * con una orden `shopping_products` los puntos que le
   * otorgo
   */
  public function update_points_product( $shopping_order_id ){
    $products =$this->get_items_order(
                          '*',
                          [ 
                            "shopping_order_id"=>$shopping_order_id
                          ]);

    foreach( $products as $product ){
      $this->db->update(
                    'shopping_products', 
                    [
                      'points_residual'=>$product["residual_points"],
                      'points_binary'=>$product["binary_points"],

                    ], 
                    [
                      'shopping_products_id	' =>$product["shopping_products_id"]
                    ]);
    }
    
    
  }


  public function update_first_product( $shopping_order_id, $data ){
    $products =$this->get_items_order(
      '*',
      [ 
        "shopping_order_id"=>$shopping_order_id,
        "product.product_id !=" => 1      
      ],
      1);
    
    if( !empty( $products ) ){

      $points = $data['points_residual'] / $products[0]["quantity"];
      $data['points_residual'] = $points;
      $this->db->update(
        'shopping_products', 
        $data, 
        [
          'shopping_products_id	' =>$products[0]["shopping_products_id"]
        ]
        );

    }

      
  }


  /**
   * Carlos Aguirre 2019-10-25 16:33:02
   * Checka si un usuario ha adquirido un plan durante el
   * mes actual, esto sirve para otorgarle puntos por productos
   * @param $id_user
   * @return boolean 
   */
  public function check_buy_plan_current_month($id_user){
    $result = $this->db->query("
    SELECT 
        so.shopping_order_id as 'id_order',
        so.created,
        tr.id,
        u.id as 'id_user',
        u.username,
        p.spanish_name
    FROM shopping_order so
    JOIN transactions tr
        ON so.shopping_order_id = tr.reference
    JOIN users u
        ON u.id = so.users_id
    JOIN plans p
        ON p.id = tr.hash
    WHERE 
        month(so.created) = month(now()) 
        AND 
        type = 'purchase_plan'
        AND 
        date(so.created) > '2019-09-25'
        AND
        so.state  ='approved'
        AND
        tr.status = 'done'
        AND
        u.id = ".$id_user."
    ")->result_array();
    return count($result)>0;
  }
}