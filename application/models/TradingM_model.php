<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of Trading_model
 *
 * @author Julian David Serna Echeverri
 * @author Juan Manuel Pinzon
 */
class TradingM_model extends CI_Model {

    // Declare custom names ans fields for each database table
    var $tables = array(
        'history' => array(
            'name' => 'trading_history',
            'fields' => array(
                'id_market'=> null,
                'id_user'   => null,
                'id_transaction'=> null,
                'type'      => null,
                'coin'      => null,
                'amount'    => 0,
                'btc_price' => 0,
                'btc_total' => 0,
                'btc_fee'   => 0,
                'btc_fee_total' => 0,
                'status' => 'inactive',
                'archived' => null,
            ),
        ),
        'markets' => array(
            'name' => 'trading_markets',
            'fields' => array(
                    'id_user'   => null,
                    'id_transaction'=> null,
                    'type'      => null,
                    'coin'      => null,
                    'amount'    => 0,
                    'btc_price' => 0,
                    'btc_total' => 0,
                    'btc_fee'   => 0,
                    'btc_fee_total' => 0,
                    'status' => 'active'
            ),
        ),
        'profits' => array(
            'name' => 'trading_profits',
            'fields' => array(
                'id_trading' => null,
                'profit_usd' => null,
                'profit_btc' => null,
            ),
        ),
    );

    function __construct($table = null) {

        parent::__construct();
        
        try {
            
            if ($table) {

                $this->table = new stdClass();
                $this->table->name = $this->tables[$table]['name'];
                $this->table->fields = $this->tables[$table]['fields'];
            }else{
                throw new Exception("Invalid datable");
            }
            
        } catch (Exception $exc) {
            return $exc->getTraceAsString();
        }

    }

    function insert($data = array()) {

        if ($data) {
            $fields = array_replace($this->table->fields, $data);
            
            $fields['date'] = date("Y-m-d H:i:s");
                                        
            $insert = $this->db->insert($this->table->name, $fields);
            
            if ($insert) {
                return $this->db->insert_id();
            }
        }
        
        return false;
    }

    function get($where = array(), $limit = null, $order_by = 'date', $order = 'DESC') {

        $this->db->from($this->table->name);

        if ($where) {
            $this->db->where($where);
        }

        if ($limit) {
            $this->db->limit($limit);
        }

        $this->db->order_by($order_by, $order);
        $query = $this->db->get();

        return ($query->num_rows() > 0) ? $query->result_array() : array();
    }

    function get_row($id = null) {

        if ($id) {
            $this->db->from($this->table->name);
            $this->db->where('id', $id);
            $this->db->limit(1);
            $query = $this->db->get();
            return ($query->num_rows() > 0) ? $query->row_array() : array();
        }
    }

    function update($id = null, array $data = array()) {
        if ($id && $data) {
            $this->db->where('id', $id);
            $this->db->update($this->table->name, $data);

            if ($this->db->affected_rows()) {
                return true;
            }
        }
        return false;
    }

    function delete($id = null) {
        if ($id) {
            $this->db->where('id', $id);
            return $this->db->delete($this->table->name);
        }
        return false;
    }
    
    function query($query){
        $result = $this->db->query($query);
        return ($result->num_rows() > 0) ? $result->result_array() : array();
    }

}
