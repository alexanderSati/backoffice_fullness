<?php defined('BASEPATH') OR exit('No direct script access allowed');
class End_of_the_month_model extends CI_Model {

    private $_db;
    
    function __construct()
    {
        parent::__construct();
        
        $this->_db = new stdClass();

        // define primary table
        $this->_db->_name = 'end_of_the_month';
        
        // define fields in table and default values
        $this->_db->_fields = array(
            'step' => 0,
            'complete' => 0,
            'date_at' => date('Y-m-d'),
        );

    }

    function init()
    {
        $insert = $this->insert($this->_db->_name, $this->_db->_fields);
        if($insert){
            $data = ['id_end_of_the_month' => $this->db->insert_id()];
            return $this->get($data);
        }
        return false;
    }

    function insert($table = null, $fields)
    {
        if(is_null($table)){
            $table = $this->_db->_name;
        }
        $data = array_replace($this->_db->_fields, $fields);
        return $this->db->insert($table, $data);
    }

    function update($id = null, array $data = []) {
        if ($id && $data) {
            $this->db->where('id_end_of_the_month', $id);
            $this->db->update($this->_db->_name, $data);
            
            if ($this->db->affected_rows()) {
                return true;
            } 
        }
        return false;
    }

    function get($where = [], $limit = 1){
        
        $this->db->from($this->_db->_name);
        
        if ($where){
            $this->db->where($where);
        }
        $this->db->order_by("id_end_of_the_month", "DESC");
        $this->db->limit($limit); 
        $query =$this->db->get();
        if ($query->num_rows() == 1){
            return $query->row_array();
        }elseif($query->num_rows() > 1){    
            return $query->result_array();
        }
        return false;
                
    }
}
