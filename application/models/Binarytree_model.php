<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of BinaryTree_model
 *
 * @author Juan Manuel Pinzon
 */
class BinaryTree_model extends CI_Model {
    
    function __construct() {
        parent::__construct();
        
        $this->table = new stdClass();
        $this->table->name = "tree";
        
        $this->table->fields = array(
            'id_user'       => null,
            'points'        => 0,
            'left_child'    => null,
            'right_child'   => null,
            'left_points'   => 0,
            'right_points'  => 0,
            'left_json'     => '{}',
            'right_json'    => '{}',
            'profit'        => 0,
            'last_bonus'    => date("Y-m-d H:i:s")
        );
    }    
    
    function insert($data = array()){
        if($data){
            $values = array_replace($this->table->fields, $data);
            $this->db->insert($this->table->name,$values);
            
            if ($id = $this->db->insert_id())
            {
                return $id;
            }
        }
        return false;
    }

    
    function get($select = null, $where = array(), $limit = 10, $order_by = 'id_tree', $order = 'ASC'){
        
        $this->db->from($this->table->name);
        if ($select){
            $this->db->select($select);
        }
        
        if ($where){
            $this->db->where($where);
        }
        
        if ($order_by && $order){
            $this->db->order_by($order_by,$order);
        }
        
        $this->db->limit($limit);
        $query = $this->db->get();
        return ($query->num_rows() > 0) ? $query->result_array() : array();
    }
    
    function delete(){
        
    }
    
    function update($id = null, $data = array()){
        if ($id && $data){
            if( !empty( $data['id_tree'] ) ){
                unset($data['id_tree']);
            }
            $this->db->where('id_user',$id);
            $this->db->update($this->table->name,$data);
            return $this->db->affected_rows();
        }
        return false;
    }

    
    function get_childs($parent = null){
        
        if($parent){
            $this->db->select('left_child,right_child');
            $this->db->from($this->table->name);
            $this->db->where('id_user',$parent);
            $this->db->limit(1);
            $query = $this->db->get();
            
            return ($query->num_rows() > 0) ? $query->row_array() : array();
        }
        return false;
    }
    
    function get_points($childs = array()){
        if($childs){
            $this->db->select('tree.id_user, points');
            $this->db->from($this->table->name);
            $this->db->join('subscriptions', 'tree.id_user = subscriptions.id_user');
            $this->db->where('subscriptions.preferencial_user', '0');
            $this->db->where_in('tree.id_user',$childs);
            $this->db->having('points > 0');
            $query = $this->db->get();
            
            if($query->num_rows() > 0){
                $results = $query->result_array();
                return array_combine(array_column($results,'id_user'), array_column($results,'points'));
            }
        }
        return [];
    }
    
    function get_score($id){
        
        if($id){
            //$this->db->select('left_points,right_points, (`left_points`+`right_points`) AS `pts`, profit');
            $this->db->from($this->table->name);
            $this->db->where('id_user',$id);
            $this->db->limit(1);
            
            $query = $this->db->get();
            return ($query->num_rows() > 0) ? $query->row_array() : array();
            
        }
        return false;
    }
    
    // Verify is an user already exist on database
    function find_child($id = null){
        if($id){
            $this->db->from($this->table->name);
            $this->db->where('left_child', $id);
            $this->db->or_where('right_child', $id); 
            return $this->db->count_all_results();
        }
    }

    function find_is_child($id = null, $active = false){
        if($id){
            $this->db->from($this->table->name);

            if ($active){
                $this->db->join("subscriptions", "{$this->table->name}.id_user = subscriptions.id_user")
                ->join("shopping_order", "{$this->table->name}.id_user = shopping_order.users_id", 'right')
                ->where_in('id_plan', [5,6,7,8])
                ->where('preferencial_user', '0');
            }
            $this->db->where('left_child', $id);
            $this->db->or_where('right_child', $id); 
            $query = $this->db->get();
            return ($query->num_rows() > 0) ? $query->row_array() : [];
        }
    }

    function count_child($uid)
    {
        if ($uid) {
            $this->db->select('left_json, right_json');
            $this->db->from($this->table->name);
            $this->db->where('id_user',$uid);
            $this->db->limit(1);
            $query = $this->db->get();
            $results = $query->row_array();
            $sum = count(json_decode($results['left_json'], TRUE)) + count(json_decode($results['right_json'], TRUE));
            
            return ($query->num_rows() > 0) ? $sum : 0; 

        }
    }


    public function get_table_binary($id_user){
        $this->db->select("*")
                    ->from("binary")
                    ->where(["users_id"=>$id_user]);

        return $this->db->get()->result_array();
    }
    
}
