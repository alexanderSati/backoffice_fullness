<?php defined('BASEPATH') OR exit('No direct script access allowed');

class Products_model extends CI_Model {

    /**
     * @vars
     */
    private $_db;


    /**
     * Constructor
     */
    function __construct()
    {
        parent::__construct();

        $this->load->library('wallet');

        // define primary table
        $this->_db = 'product';
    }


    /**
     * Get list of non-deleted products
     *
     * @param  int $limit
     * @param  int $offset
     * @param  array $filters
     * @param  string $sort
     * @param  string $dir
     * @return array|boolean
     */

    function get_all($limit = 0, $offset = 0, $filters = array(), $sort = 'product_id', $dir = 'desc')
    {
        $sql = "
            SELECT SQL_CALC_FOUND_ROWS *
            FROM {$this->_db}
            WHERE deleted != '2' and product_id > 1
        ";

        if ( ! empty($filters))
        {
            foreach ($filters as $key=>$value)
            {             
                    $value = $this->db->escape('%' . $value . '%');
                    $sql .= " AND {$key} LIKE {$value}";
            }
        }
        
        $sql .= " ORDER BY {$sort} {$dir}";

        if ($limit)
        {
            $sql .= " LIMIT {$offset}, {$limit}";
        }

        $query = $this->db->query($sql);

        if ($query->num_rows() > 0)
        {
            $results['results'] = $query->result_array();
        }
        else
        {
            $results['results'] = NULL;
        }

        $sql = "select FOUND_ROWS() AS total";
        $query = $this->db->query($sql);
        $results['total'] = $query->row()->total;

        return $results;
    }

    /**
     * Add new product
     *
     * @param  array $data
     * @return mixed|boolean
     */
    function add_product(&$data = array())
    {
        if ($data)
        {

            $sql = "
                INSERT INTO {$this->_db} (
                    code,
                    category_id,
                    product_name,
                    product_price,
                    product_price_distributor,
                    product_image,
                    user_created,
                    residual_points,
                    binary_points,
                    stock_quantity

                ) VALUES (
                    " . $this->db->escape($data['code']) . ",
                    " . $this->db->escape($data['category_id']) . ",
                    " . $this->db->escape($data['product_name']) . ",
                    " . $this->db->escape($data['product_price']) . ",
                    " . $this->db->escape($data['product_price_distributor']) . ",
                    " . $this->db->escape("products/".$data['image']) . ",
                    " . $this->db->escape($data['id_user']). ",
                    " . $this->db->escape($data['residual_points']).",
                    " . $this->db->escape($data['binary_points']) .",
                    " . $this->db->escape($data['stock_quantity']) ."

                )";

            $this->db->query($sql);

            if ($id = $this->db->insert_id())
            {
                return $id;
            }
        }

        return FALSE;
    }

    /**
     * Edit an existing user
     *
     * @param  array $data
     * @return boolean
     */
    function edit_product($data = array())
    {
        if ($data)
        {

            $update_data = [
                "code"=>$data['code'],
                "product_name"=>$data['product_name'],
                "product_price"=>$data['product_price'],
                "product_price_distributor"=>$data['product_price_distributor'],
                "category_id"=>$data['category_id'],
                "user_updated"=>$data['id_user'],
                "residual_points"=>$data['residual_points'],
                "binary_points"=>$data['binary_points'],
                "stock_quantity"=>$data['stock_quantity']

            ];
            if($data['image'] != ''){
                if (!empty($data['image'])) {
                    unlink('uploads/' . $data['image_to_delete']);
                }
                $update_data["product_image"]="products/".$data['image'];
            }

            $this->db->update($this->_db, $update_data, "product_id =".$data['product_id']);

            if ($this->db->affected_rows())
            {
                return TRUE;
            }
          

            
        }

        return FALSE;
    }
    
    /**
     * Get specific product
     *
     * @param  int $id
     * @return array|boolean
     */
    function get_product($id = NULL)
    {
        if ($id)
        {
            
            if (is_numeric($id)){
                $sql = "
                    SELECT *
                    FROM {$this->_db}
                    WHERE product_id = " . $this->db->escape($id);
            }

            $query = $this->db->query($sql);

            if ($query->num_rows())
            {
                return $query->row_array();
            }
        }

        return FALSE;
    }

    function remove_product($id = NULL)
    {
        if($id) {
            $sql = "
                UPDATE {$this->_db}
                SET ";

            $sql .= "
                deleted = " . 1 . "
                WHERE product_id = " . $this->db->escape($id);

            $this->db->query($sql);

            if ($this->db->affected_rows())
            {
                return TRUE;
            }
        }
        return FALSE;
    }

    function activate_product($id = NULL)
    {
        if($id) {
            $sql = "
                UPDATE {$this->_db}
                SET ";

            $sql .= "
                deleted = " . 0 . "
                WHERE product_id = " . $this->db->escape($id);

            $this->db->query($sql);
            if ($this->db->affected_rows())
            {
                return TRUE;
            }
        }

        return FALSE;
    }

    /**
     * Reduce la cantidad de existencias de productos en stock tabla `shipping_products_quantities`
     * en caso de realizada una compra a una determinada
     * entidad como un mayorista
     * @param int $product_id
     * @param int $shipping_details_id ( id de la tabla `shiping_details` )
     * @param int $quantity la cantidad a restar en stock
     * @return boolean ( True exito | False operacin fallida )
     */
    public function reducir_catidad_en_stock( $product_id, $shipping_details_id, $quantity ){
        $stock = $this->db->select('*')
                        ->from('shipping_products_quantities')
                        ->where([
                            'shiping_details_id'=>$shipping_details_id,
                            'product_id'=>$product_id
                        ])
                        ->limit(1)
                        ->get()
                        ->first_row('array');
        
        if((intval($stock['quantity']) - intval($quantity)) >= 0){
            
            $stock['last_updated'] = date('Y-m-d H:i:s');
            $stock['quantity'] = intval($stock['quantity']) - intval($quantity);
            $this->db->update('shipping_products_quantities', $stock, "id=".$stock['id']);
            return true;
        }
        else
            return false;
    }

    /**
     * Agrega cantidad de existencias de productos en stock tabla `shipping_products_quantities`
     * en caso de realizada una devolucion compra a una determinada
     * entidad como un mayorista
     * @param int $product_id
     * @param int $shipping_details_id ( id de la tabla `shiping_details` )
     * @param int $quantity la cantidad a restar en stock
     * @return boolean ( True exito | False operacin fallida )
     */
    public function agregar_catidad_en_stock( $product_id, $shipping_details_id, $quantity ){
        $stock = $this->db->select('*')
        ->from('shipping_products_quantities')
        ->where([
            'shiping_details_id'=>$shipping_details_id,
            'product_id'=>$product_id
        ])
        ->limit(1)
        ->get()
        ->first_row('array');

        $stock['last_updated'] = date('Y-m-d H:i:s');
        $stock['quantity'] = intval($stock['quantity']) + intval($quantity);
        $this->db->update('shipping_products_quantities', $stock, "id=".$stock['id']);
        return true;
  
    }

    /**
    * Get products for dropdpwm
    * @return array
    */

    function get_products()
    {
        $this->db->select('p.*');
        $this->db->from($this->_db.' p');    
        $this->db->where('p.product_id >','1');

        $query = $this->db->get();
        $results = $query->result_array();
        return array_combine(array_column($results, 'product_id'),array_column($results, 'product_name'));
    } 
    
    /**
     * Get product by name
     */
    function get_product_name($name)
    {
        $this->db->select('*');
        $this->db->from($this->_db);
        $this->db->where('product_name', $name);

        $query = $this->db->get();

        $result = $query->row_array();
    
        return $result;
    }

  
}