<?php defined('BASEPATH') OR exit('No direct script access allowed');

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of Locations_model
 *
 * @author Juan Manuel
 */
class Locations_model extends CI_Model {
    //put your code here
    
    function __construct() {
        parent::__construct();
        
        $this->table = new stdClass();
        $this->table->countries = "countries";
        $this->table->states = "states";
        $this->table->cities = "cities";
        
    }
    
    function getCountries($combined = false){
        $this->db->from($this->table->countries);
        $this->db->order_by('name','ASC');
        $results = $this->db->get()->result_array();
        return ($combined) ? array_combine(array_column($results, 'sortname'),array_column($results, 'name')) : $results;
    }
    
    function getCountryName($alpha2 = null){
        if ($alpha2){
            $this->db->where('sortname',$alpha2);
            $result = $this->db->get($this->table->countries,1);
            
            return ($result->num_rows() > 0) ? $result->row()->name : false;
        }
    }
}
