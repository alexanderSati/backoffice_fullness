<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class ProductsQuantities_model extends CI_Model {

    public function __construct()
    {
        parent::__construct();
        $this->load->database();
        $this->load->model("orders_model");
        $this->load->model("Shipping_details_model");


        // Your own constructor code
        $this->nombreTabla= "shipping_products_quantities";
    
    }
    /**
     * Metodo para obtener los registros
     *  
     */
    public function obtenerRegistros($where)
    {
        $this->db->select('p.product_name, d.name, q.quantity, q.id, p.residual_points, p.binary_points');
        $this->db->from($this->nombreTabla. " q");
        $this->db->where($where);
        $this->db->join('product p' , 'q.product_id = p.product_id');
        $this->db->join('shiping_details d', 'q.shiping_details_id = d.id');

        $query = $this->db->get();

        return $query->result_array();
    }

    /**
     * Metodo para verificar si existe el registro
     */
    function get_register_existing($where){
        
        unset($where['submit']);
        unset($where['quantity']);
        
        $this->db->select('*');
        $this->db->from($this->nombreTabla);
        $this->db->where($where);
    
        $query = $this->db->get();

        return empty($query->result_array()) ? false : true;
    }
    
    /**
     * Metodo para crear un registro
     */
    public function crear(&$data = array())
    {
        if ($data) {
          unset($data['submit']);

          $this->db->insert($this->nombreTabla, $data);
        
          if ($id = $this->db->insert_id())
          {
              return $id;
          }
        }
      return FALSE;


    }
    /**
     * Metodo para obtener registro por ID
     */

    public function obtenerRegistroPorId( $id = "" ){
        return $this->db->select('*')
                        ->from( $this->nombreTabla )
                        ->where( [
                            $this->nombreTabla.".id" => $id
                        ] )
                        ->get()
                        ->first_row( 'array' );
    }

    /**
     *  Metodo para actualizar registro
     */

    public function actualizar( $data ){
        if ($data)
        {
            unset($data['submit']);

            $this->db->where($this->nombreTabla.".id", $data['id']);
            $this->db->update($this->nombreTabla, $data);

           if ($this->db->affected_rows())
           {
                return TRUE;
            }
        }
        return FALSE;
    }

    /**
     * Carlos Felipe Aguirre 2019-09-18 16:03:50
     * @param int $shipping_details_id ubicacion en donde buscar el producto
     * @param int $product_id producto a consultar
     * @return int cantidad de productos disponibles
     */
    public function get_quantity_available( $shipping_details_id = null, $product_id ){
        if( $shipping_details_id == 0 || empty( $shipping_details_id ) ){
            return PHP_INT_MAX;
        }

        $register = $this->obtenerRegistros([
            "p.product_id"=>$product_id,
            "shiping_details_id"=>$shipping_details_id
        ]);

        if( empty( $register ) ){
            return 0;
        }

        return intval($register[0]["quantity"]);
    }


    public function load_quantities_from_purchase( $shopping_order_id ){
        
        $orden= $this->orders_model->get("*",["shopping_order_id"=>$shopping_order_id], 1);
        $shipping_detail = $this->Shipping_details_model->get("*", [ "id_user"=>$orden[0]["users_id"]], 1);
        
        $products = $this->orders_model->get_items_order( $shopping_order_id );

        if( !empty( $shipping_detail ) ){
            
            foreach( $products as $product ){
                
                // Si el mayorista ya tiene asignado el producto
                // aumenta sus existencias
                if( 
                    $this->get_register_existing([ 
                        "product_id"=>$product['product_id'],  
                        "shiping_details_id"=>$shipping_detail['id']
                    ]) 
                ){
                    $registro = $this->obtenerRegistros([
                        "q.product_id"=>$product['product_id'],  
                        "q.shiping_details_id"=>$shipping_detail['id']
                    ]);
                    
                    $update_regiter["id"] = $registro[0]['id'];
                    $update_regiter["last_updated"] = date("Y-m-d H:i:s");
                    $update_regiter["quantity"] = intval($registro[0]['quantity']) + $product["quantity"];
                    $this->actualizar( $update_regiter );
                }
                else{
                    $new_product_stock = [
                        "product_id"=>$product['product_id'],
                        "shiping_details_id"=>$shipping_detail['id'],
                        "quantity"=>$product["quantity"],
                        "last_updated"=>date("Y-m-d H:i:s")
                    ];
                    $this->crear($new_product_stock);
                }
            }


        }
    }
}