<?php defined('BASEPATH') OR exit('No direct script access allowed');

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of Currencies_model
 *
 * @author Juan Manuel
 */
class Currencies_model extends CI_Model{
    
    function __construct() {
        parent::__construct();
        
        $this->table = new stdClass();
        $this->table->name = "currencies";
        $this->table->fields = array(
            'id' => '',
            'name' => '',
            'symbol' => '',
            'price_usd' => '',
            'price_btc' => '',
            'last_updated' => date('Y-m-d h:i:s'),
        );
    }
    
    function get($symbol = null){
        
        $this->db->from($this->table->name);
        
        if ($symbol){
            $this->db->where('symbol',$symbol);
        }
        
        $query =$this->db->get();
        
        if ($query->num_rows() == 1){
            return $query->row_array();
        }elseif($query->num_rows() > 1){    
            return $query->result_array();
        }
        
        return false;
                
    }
    
    function get_ids(){
        
        $this->db->select('cod,id');
        $this->db->from($this->table->name);
        $query =$this->db->get();
        
        if($query->num_rows() > 0){
            $vCoins = $query->result_array();
            return array_combine(array_column($vCoins, 'cod'), array_column($vCoins, 'id'));
        }
        
        return array();
    }
    
    /**
     * Return allowed virtual coins with symbol as a value
     * @return array
     */
    function get_symbols(){
        
        $this->db->select('cod,symbol');
        $this->db->from($this->table->name);
        $query =$this->db->get();
        
        if($query->num_rows() > 0){
            $vCoins = $query->result_array();
            return array_combine(array_column($vCoins, 'cod'), array_column($vCoins, 'symbol'));
        }
        
        return array();
    }
    
    function insert(){
       
    }
    
    function update($id = null, array $data = array()){
        if ($id && $data){
            $this->db->where('id', $id);
            $this->db->update($this->table->name,$data);
        }
    }
    
    function delete(){
        
    }    
}
