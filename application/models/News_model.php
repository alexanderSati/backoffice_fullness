<?php
class News_model extends CI_Model {
    
    /**
     * @vars
     */
    private $_db;
    private $database;

    /**
     * Constructor
     */
    function __construct()
    {
        parent::__construct();
        $this->_db = "wp_posts";
        $this->database = $this->load->database('wordpress', TRUE);
    }
    
    public function get_post_by_id($id = null) {
        if($id){
            $this->database->select('*');
            $this->database->from($this->_db);
            $this->database->where("id",$id);
            return $this->database->get()->row();
        }
        return false;
    }
    
    public function get_post_by_slug($slug = null) {
        if($slug){
            $this->database->select('*');
            $this->database->from($this->_db);
            $this->database->where("post_name",$slug);
            return $this->database->get()->row();
        }
        return false;
    }
    
    public function get_featured_image($id = null) {
        if($id){
            $this->database->select('*');
            $this->database->from('wp_postmeta');
            $this->database->join($this->_db, "{$this->_db}.ID = wp_postmeta.meta_value", 'left');
            $this->database->where('post_id',$id);
            $this->database->where('wp_postmeta.meta_key', '_thumbnail_id');
            $query = $this->database->get();
            
            if ($query->num_rows() > 0) {
                return $query->row()->guid;
            }
        }
        return false;
    }

    public function all_by_category_id($id = null, $limit = 4)
    {
        if ($id) {
            $this->database->select('*');
            $this->database->from($this->_db);
            $this->database->join('wp_term_relationships', 'wp_term_relationships.object_id = wp_posts.id', 'left');
            $this->database->join('wp_term_taxonomy', 'wp_term_taxonomy.term_taxonomy_id = wp_term_relationships.term_taxonomy_id', 'left');
            $this->database->join('wp_terms', 'wp_terms.term_id = wp_term_taxonomy.term_id', 'left');
            $this->database->where("wp_terms.term_id",$id);
            $this->database->order_by("{$this->_db}.post_date", 'DESC');
            $this->database->limit($limit);
            
            if( $limit == 1 ){
                return $this->database->get()->row_array();
            }
            return $this->database->get()->result_array();
        }
	return false;
    }
    
    public function all_by_category_name($name = null )
    {
	if ($name) {
            $this->database->select('*');
            $this->database->from($this->_db);
            $this->database->join('wp_term_relationships', 'wp_term_relationships.object_id = wp_posts.id', 'left');
            $this->database->join('wp_term_taxonomy', 'wp_term_taxonomy.term_taxonomy_id = wp_term_relationships.term_taxonomy_id', 'left');
            $this->database->join('wp_terms', 'wp_terms.term_id = wp_term_taxonomy.term_id', 'left');
            $this->database->where('wp_terms.name',$name);
            return $this->database->get()->result_array();
        }
	return false;
    }
    
    public function get_category_by_slug($slug = null)
    {
	if ($slug) {
            $this->database->select('*');
            $this->database->from('wp_terms');
            $this->database->where('wp_terms.slug',$slug);
            return $this->database->get()->row();
        }
	return false;
    }
    
    public function get_category_id($slug = null) {
        
        $category = $this->get_category_by_slug($slug);
        
        if (!empty($category)) {
            return $category->term_id;
        }
        return false;
    }
    
    public function get_base_url_wordpress() {
        $this->database->select('option_value');
        $this->database->from('wp_options');
        $this->database->where('option_name', 'siteurl');
        
        $query = $this->database->get()->row();
        if ($query) {
            return $query->option_value;
        }
        return  false;
    }
}