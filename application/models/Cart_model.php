<?php


defined('BASEPATH') OR exit('No direct script access allowed');


class Cart_model extends CI_Model


{





    function __construct(){





        parent::__construct();


        $this->load->model([


            'subscriptions_model',


            "shopping_order_model"


        ]);





        $this->load->library('Plan');








    }





    /**


     * Carlos Aguirre 2019-07-15 08:22:39


     * @param String $name_product filtrar nombre de producto


     * @param String $price_min precio minimo del producto


     * @param String $price_max precio maximo del producto


     * @param int $category categoria a la que pertenece un producto


     * @param int $offset inicio de la paginacion


     * @param int $limit cantidad de registros por consulta


     * @return array productos filtrados y paginados


     */


    function fetch_all($name_product, $price_min, $price_max, $category, $shoppingDetailsId,$offset=0, $limit)
    {
        $price = "";
        $tipo_usuario = "'' as tip_usuario";
        $descuento = "";
        $subscription = $this->subscriptions_model->get_active($this->user['id']);

        // si es usuario ejecutivo mostrar 12% de descuento
        if( $this->user['profile'] == 'executive' ){

            $price = "(product_price_distributor * 0.88) as 'product_price'";
            $tipo_usuario = "'usuario ejecutivo 12% de descuento' as tip_usuario";
            $descuento = 12;

        }

        // si es usuario ejecutivo mostrar 12% de descuento
        elseif( $subscription["preferencial_user"] == "1" && $subscription["base"] == 0 ){
            $price = "(product_price * 0.8) as 'product_price'";
            $tipo_usuario = "'Comprador preferencial 20% descuento' as tip_usuario";
            $descuento = 20;

        }
        else{

            $plan = $this->plan->get_plan($subscription["id_plan"]);

            // si es plan gratuito debe mostrar el descuento del
            // nuevo plan que va a adquirir
            if($plan["base"] == 0){

                $newPlan =  $this->plan->get_plan($this->session->userdata("plan_id"));
                $price = "(product_price - ((product_price * ".$newPlan["discount"].")/100)) as 'product_price'";
                $tipo_usuario = "'Comprador primera vez ".$newPlan["discount"]."% descuento' as tip_usuario";
                $descuento = round($newPlan["discount"]);

            }

            // Si no tiene plan gratuito entonces ya realizo la primera compra
            // y debe mostrar el 50% de descuento ( precio de distribuidor )
            else{

                //Si esta realizando el upgrade no aplica el descuento del 50% sino 
                // del nuevo plan que va aquirir
                if(!empty($this->session->userdata("plan_id"))) {

                    $newPlan =  $this->plan->get_plan($this->session->userdata("plan_id"));
                    $price = "(product_price - ((product_price * ".$newPlan["discount"].")/100)) as 'product_price'";
                    $tipo_usuario = "'Comprador primera vez ".$newPlan["discount"]."% descuento' as tip_usuario";
                    $descuento = round($newPlan["discount"]);

                }else {

                    $price = "product_price_distributor as 'product_price'";
                    $tipo_usuario = "'Comprador mas de una vez 50% descuento' as tip_usuario";
                    $descuento = 50;

                }
            }
        }

        $this->db->select([
            "product.product_id",
            "category_id",
            "code",
            "residual_points",
            "binary_points",
            "product_name",
            $price,
            "product_image",
            $tipo_usuario,
            "'".$shoppingDetailsId."' as 'shiping_details_id'",
            "product_price as 'precio_original'",
            "'".$descuento."' as descuento"
        ])
        ->from('product')
        ->join(
            'shipping_products_quantities', 
            'shipping_products_quantities.product_id = product.product_id', 
            'left' 
        )
        ->order_by('product.product_name ASC')
        ->group_by( 'product.product_id' )
        ->limit( $limit, $offset );

        if( !empty( $name_product ) ){
            $this->db->like('product_name', $name_product );
        }

        if( !empty( $price_min ) ){
            $this->db->where('product_price >=', $price_min );
        }

        if( !empty( $price_max ) ){
            $this->db->where('product_price <=', $price_max );
        }

        if(!empty($shoppingDetailsId)){

            $this->db->where([
                'shipping_products_quantities.shiping_details_id'=> $shoppingDetailsId,
                'quantity >'=>0

            ] );
        }

        if( empty($shoppingDetailsId) ){
            $this->db->where("stock_quantity >", 0);
            $this->db->where('deleted', 0 );
        }

        if( !empty( $category) ){

            $this->db->where('category_id', $category );

        }

        

        $products = $this->db->get()->result_array();
        return $products;                                

    }





    /**


     * Carlos Aguirre 2019-07-13 09:44:14


     * Obtiene el numero total de registros en la tabla


     */


    public function get_total_rows($name_product, $price_min, $price_max, $category, $shoppingDetailsId,$offset=0){








        $this->db->select('count(*) as number')


        ->from('product')


        ->join(


            'shipping_products_quantities', 


            'shipping_products_quantities.product_id = product.product_id', 


            'left' 


        )


        ->order_by('product_created DESC');





        if( !empty( $name_product ) ){


            $this->db->like('product_name', $name_product );


        }





        if( !empty( $price_min ) ){


            $this->db->where('product_price >=', $price_min );


        }





        if( !empty( $price_max ) ){


            $this->db->where('product_price <=', $price_max );


        }








        if(!empty($shoppingDetailsId)){


            $this->db->where([


                'shipping_products_quantities.shiping_details_id'=> $shoppingDetailsId,


                'quantity >'=>0


            ] );


        }





        if( empty($shoppingDetailsId) ){


            $this->db->where("stock_quantity >", 0);


        }





        if( !empty( $category) ){


            $this->db->where('category_id', $category );


        }





        $this->db->where('deleted', 0 );





        $result = $this->db->get()->first_row('array')['number'];


        


        


        return $result;


        


    }





   





    /**


     * Carlos Aguirre 2019-07-10 15:06:33


     * Obtiene el listado de compras de productos


     */


    public function get_purchases( $user_id ){


        $this->db->select('


                        DATE_FORMAT(shopping_order.created, "%Y/%m/%e %r") as "created",


                        shopping_order.shopping_order_id,


                        ROUND(shopping_order.total_value, 2) as total_value, 


                        ROUND(shopping_order.total_discount, 2) as total_discount, 


                        users.first_name,


                        users.last_name,


                        users.profile,


                        shopping_order.state')


                    ->from('shopping_products')


                    ->join(


                        'shopping_order',


                        'shopping_order.shopping_order_id = shopping_products.shopping_order_id'


                    )


                    ->join(


                        'users',


                        'users.id = shopping_order.users_id'


                    )


                    ->where(["users_id"=>$user_id ])


                    ->group_by('shopping_order.shopping_order_id')


                    ->order_by("shopping_order.created", "DESC");


        


        return $this->db->get()->result_array();


    }





    /**


     * Carlos Aguirre 2019-07-11 07:51:13


    * Obtiene el listado de productos adquiridos en una orden


    * @param int $shopping_order_id (id de la orden de compra)


    * @return array listado de productos comprados


    */


    public function get_items_purchase($shopping_order_id){


        $this->db->select(


                    "product.product_image,


                    product.product_name,


                    shopping_products.quantity,


                    ROUND(shopping_products.value, 2) as value,


                    product.product_name,


                    round((shopping_products.points_residual * shopping_products.quantity)) as residual_points,


                    round((shopping_products.points_binary * shopping_products.quantity)) as binary_points,


                    ROUND((shopping_products.quantity * shopping_products.value), 2) as total_product,


                    ROUND(shopping_order.total_value, 2) as total_value


                    "


                    )


                    ->from('shopping_products')


                    ->join(


                        'product', 


                        "product.product_id = shopping_products.product_id"


                    )


                    ->join(


                        'shopping_order',


                        'shopping_order.shopping_order_id = shopping_products.shopping_order_id'


                    )


                    ->where("shopping_products.shopping_order_id",$shopping_order_id);





        return $this->db->get()->result_array();


    }





    public function fetch_all_external($name_product, $price_min, $price_max, $category, $shoppingDetailsId,$offset=0, $limit)


    {





        $descuento=0;


        $this->db->select([


            "product.product_id",


            "category_id",


            "code",


            "residual_points",


            "binary_points",


            "product_name",


            "product_image",            


            "'".$shoppingDetailsId."' as 'shiping_details_id'",


            "product_price as 'precio_original'",


            "'".$descuento."' as descuento"


        ])


        ->from('product')


        ->join(


            'shipping_products_quantities', 


            'shipping_products_quantities.product_id = product.product_id', 


            'left' 


        )


        ->order_by('product.product_name ASC')

        ->group_by('product.product_id')


        ->limit( $limit, $offset );





        if( !empty( $name_product ) ){


            $this->db->like('product_name', $name_product );


        }





        if( !empty( $price_min ) ){


            $this->db->where('product_price >=', $price_min );


        }





        if( !empty( $price_max ) ){


            $this->db->where('product_price <=', $price_max );


        }








        if(!empty($shoppingDetailsId)){


            $this->db->where([


                'shipping_products_quantities.shiping_details_id'=> $shoppingDetailsId,


                'quantity >'=>0


            ] );


        }





        if( empty($shoppingDetailsId) ){


            $this->db->where("stock_quantity >", 0);


        }





        if( !empty( $category) ){


            $this->db->where('category_id', $category );


        }





        $this->db->where('deleted', 0 );





        


       


        $products = $this->db->get()->result_array();





        return $products;   


    }


}