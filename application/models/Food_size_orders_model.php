<?php

class Food_size_orders_model extends CI_Model {
  function __construct() {
    parent::__construct();
  }

  function get($select = "*", $array_where = false, $array_like = false, $first = false, $offset = false, $order_by = false) {
		$data = array();
		if( $order_by != false) {
			$order = key($order_by);
			if ($order != null) {
				$sort = $order_by[$order];
				$this -> db -> order_by($order, $sort);
			}
		}

		$this -> db -> select($select);
		$this -> db -> from('food_size_orders');
		if($array_where != false)
			$this -> db -> where($array_where);
		if($array_like != false)
			$this -> db -> like($array_like);
		if($offset != false){
			$this -> db -> limit($offset, $first);
		}

		$query = $this -> db -> get();
		if ($query -> num_rows() > 0) {
			foreach ($query->result() as $rows) {
				$data[] = $rows;
			}
			$query -> free_result();
			return $data;
		} else {
			return null;
		}
	}


  function get_by_id($id) {
    $select = '*';
    $array_where = array('id' => $id);
    $array_like = array();
    $order_by = array();
    $result = $this -> get($select, $array_where, $array_like, 0, 1, $order_by);
    if ($result) {
			return $result[0];
		} else {
			return array();
		}
  }

  function getFoodSizeOrders($idOrder) {
		$this -> db -> select('fso.price, s.name, fso.quantity');
		$this -> db -> where('fso.food_order_id = ' . $idOrder);
		$this -> db -> from('food_size_orders as fso');
		$this -> db -> join('sizes as s','fso.food_size_id = s.id');
    	$this -> db ->group_by('s.id');
		$query = $this -> db -> get();
		$rows = $query -> result();
		return $rows;
	}

  function insert($data_array) {
    $data_array['created_at']=date('Y-m-d H:i:s');
    $data_array['updated_at']=date('Y-m-d H:i:s');
    $this -> db -> insert('food_size_orders', $data_array);
    return $this -> db -> insert_id();
  }

  public function remove($arr_where) {
    $this -> db -> where($arr_where);
    $this -> db -> delete('food_size_orders');
    return $this->db->affected_rows();
  }

  public function remove_by_id($id) {
    $array_where = array('id' => $id);
    return $this -> remove($array_where);
  }

  function update($data_array, $array_where) {
    $this -> db -> where($array_where);
    $this -> db -> update('food_size_orders', $data_array);
  }
}
?>
