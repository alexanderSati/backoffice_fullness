<?php
class Cities_model extends CI_Model {
	function __construct() {
		parent::__construct();
	}

	function get($select = "*", $array_where = false, $array_like = false, $first = false, $offset = false, $order_by = false) {
		$data = array();
		if( $order_by != false){
			$order = key($order_by);
			if ($order != null) {
				$sort = $order_by[$order];
				$this -> db -> order_by($order, $sort);
			}
		}

		$this -> db -> select($select);
		$this -> db -> from('cities');
		
		if($array_where != false)
			$this -> db -> where($array_where);
		if($array_like != false)
			$this -> db -> like($array_like);
		if($offset != false){
			$this -> db -> limit($offset, $first);
		}

		$query = $this -> db -> get();
		if ($query -> num_rows() > 0) {
			foreach ($query->result() as $rows) {
				$data[] = $rows;
			}
			$query -> free_result();
			return $data;
		} else {
			return null;
		}
	}

	function total($array_where, $array_like) {
		$this -> db -> select('count(*) as total');
		$this -> db -> where($array_where);
		$this -> db -> like($array_like);
		$this -> db -> from('cities');
		$query = $this -> db -> get();
		$rows = $query -> result();
		$query -> free_result();
		return $rows[0] -> total;
	}

	function get_by_id($id) {
		$select = '*';
		$array_where = array('idCity' => $id);
		$array_like = array();
		$order_by = array();
		$result = $this -> get($select, $array_where, $array_like, 0, 1, $order_by);
		if ($result) {
			return $result[0];
		} else {
			return array();
		}
	}

	function get_currency(){
		$select = '*';
		$array_where = array();
		$array_like = array();
		$order_by = array();
		return $this -> get($select, $array_where, $array_like, 0, 500, $order_by);
	}

	function get_by_exact_name($name){
		$select = '*';
		$array_like=array();
		$array_where = array('name'=>$name);
		$order_by = array();
		return $this -> get($select, $array_where, $array_like, 0, 1, $order_by);
	}

	function get_by_name($name, $first, $offset) {
		$select = '*';
		$array_where = array();
		$array_like = array('name'=>$name);
		$order_by = array();
		return $this -> get($select, $array_where, $array_like, $first, $offset, $order_by);
	}

	function get_by_name_and_diff_id($id,$name){
		$select = '*';
		$array_where = array('name'=>$name,'idCity <>'=>$id);
		$array_like = array();
		$order_by = array();
		return $this -> get($select, $array_where, $array_like, 0, 1, $order_by);
	}

	function get_by_id_and_name($id,$name, $first, $offset) {
		$select = '*';
		$array_where = array();
		$array_like = array('name'=>$name,'idCity'=>$id);
		$order_by = array();
		return $this -> get($select, $array_where, $array_like, $first, $offset, $order_by);
	}

	function insert($data_array) {
		$data_array['created_at']=date('Y-m-d H:i:s');
		$data_array['updated_at']=date('Y-m-d H:i:s');
		$this -> db -> insert('countries', $data_array);
		return $this -> db -> insert_id();
	}

	public function remove($arr_where) {
		$this -> db -> where($arr_where);
		$this -> db -> delete('cities');
		return $this->db->affected_rows();
	}

	public function remove_by_id($id) {
		$array_where = array('idCity' => $id);
		return $this -> remove($array_where);
	}

	function update($data_array, $array_where) {
		$this -> db -> where($array_where);
		$this -> db -> update('cities', $data_array);
	}

	/**
	 * Obtiene el nombre de la ciudad donde se realizo
	 * el pedido( orden ) a partir del id ( orders.idOrder ) 
	 */
	public function get_city_by_order_id( $order_id ){

		$result = $this->db->select('cities.*')
												->from('orders')
												->join('users', 'users.idUser = orders.user_id', 'left')
												->join('cities', 'cities.idCity = users.city_id', 'left')
												->where( 'orders.idOrder', $order_id )
												->get()
												->first_row('array');
		if( empty( $result ) ){
			return "";

		}
		else{
			return $result['name'];
		}
	}


	/**
	 * Obtiene el nombre de la ciudad donde el usuario esta 
	 * registrado
	 */
	public function get_city_by_users_id( $users_id ){

		$result = $this->db->select('cities.*')
												->from('users')
												->join('cities', 'cities.idCity = users.city_id', 'left')
												->where( 'users.idUser', $users_id )
												->get()
												->first_row('array');
		if( empty( $result ) ){
			return "";

		}
		else{
			return $result['name'];
		}
	}


	/**
	 * Obtiene el registro de una ciudad a partir del nombre otorgado
	 * como parametro
	 */
	public function get_city_by_name( $city_name ){

		$result = $this->db->select('*')
													->from('cities')
													->like( 'name', $city_name, 'both' )
													->order_by('cities.idCity', 'DESC')
													->get()
													->first_row('array');
	
		if( empty( $result ) ){
			return [];
		}				
		else{
			return $result;
		}
	}

	/**
	 * Obtiene el listado de ciudades con ´Aliados´
	 * activos, es decir, donde la aplicacion puede ser
	 * usada
	 */
	public function get_available_cities(){
		$result = $this->db->select('cities.*')
			->from('cities')
			->join( 'allies', '(cities.idCity = allies.id_city )' )
			->where( ['allies.activated'=>1])
			->group_by("cities.idCity")
			->order_by('cities.name', 'ASC')
			->get()
			->result_array();
		return $result;

	}


	/**
	 * Obtiene el registro de una ciudad a partir del nombre de la ciudad y 
	 * el nombre del depto
	 * como parametro
	 * @param String $city_name
	 * @param String $state_name
	 * @return Array
	 */
	public function get_idCity_by_name_and_state( $state_name, $city_name ){

		$result = $this->db->select('*')
													->from('cities')
													->join( 'states', 'cities.state_id = states.IdState' )
													->like( 'cities.name', urldecode($city_name), 'both' )
													->like( 'states.name', urldecode($state_name), 'both' )
													->order_by('cities.idCity', 'DESC')
													->get()
													->first_row('array');

		if( empty( $result ) ){
			return [];
		}				
		else{
			return $result;
		}
	}
}
?>