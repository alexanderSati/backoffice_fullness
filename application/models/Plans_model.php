<?php defined('BASEPATH') OR exit('No direct script access allowed');

/*
 * Created by: Juan Manuel Pinzon
 * Date : 06-02-2017
 */


class Plans_model extends CI_Model {

    /**
     * @vars
     */
    private $_db;


    /**
     * Constructor
     */
    function __construct()
    {
        parent::__construct();
        
        $this->_db = new stdClass();

        // define primary table
        $this->_db->_name = 'plans';
        
        // define fields in table and default values
        $this->_db->_fields = array(
            'base'          => 0,
            'period_days'   => 0,
            'min_profit'      => 0,
            'max_profit'      => 0,
            'start_days'      => 5,
            'status'        => 'active',
            'spanish_name'      => null,
            'spanish_features'  => null,
            'english_name'      => null,
            'english_features'  => null,
            'points'            => 0,
            'points_upgrade'    => 0,
            'value_rule1'       => null,
            'value_rule2'       => null,
            'max_value_rule'    => 0
        );
    }


    /**
     * Return a  list of all available plans
     *
     * @param  string $sort
     * @return array|boolean
     */
    function get_all($select = null, $sort = 'name',$where = null)
    {
        
        if ($select){
            $this->db->select($select);
        }
        
        $this->db->from($this->_db->_name);
        
        if ($where){
            $this->db->where($where);
        }
        
        $this->db->order_by($sort,'ASC');
        $plans = $this->db->get()->result_array();
        
        if ($plans){
            return $plans;
        }
        
        return false;
    }
    
    function get($plan = null){
        
        if (is_numeric($plan)){
            $this->db->from($this->_db->_name);
            $this->db->where('id',$plan);
            $query = $this->db->get();
            
            return ($query->num_rows() > 0) ? $query->row_array() : false;
        }
        
    }
    
    function get_active(){
        return $this->get_all(null,'base',"`status`='active'");
    }

    /**
     * Get list of non-deleted users
     *
     * @param  int $limit
     * @param  int $offset
     * @param  array $filters
     * @param  string $sort
     * @param  string $dir
     * @return array|boolean
     */
    function get_all_row($limit = 0, $offset = 0, $filters = array(), $sort = 'base', $dir = 'ASC')
    {
        $sql = "
            SELECT SQL_CALC_FOUND_ROWS p.* , s.id as has_subscription
            FROM {$this->_db->_name } as p
            LEFT JOIN subscriptions as s ON s.id_plan = p.id
            WHERE p.status != 'deleted'
        ";

        if ( ! empty($filters))
        {
            foreach ($filters as $key=>$value)
            {
                $value = $this->db->escape('%' . $value . '%');
                $sql .= " AND p.{$key} LIKE {$value}";
            }
        }

        $sql .= " GROUP BY p.id";
        $sql .= " ORDER BY p.{$sort} {$dir}";

        if ($limit)
        {
            $sql .= " LIMIT {$offset}, {$limit}";
        }

        $query = $this->db->query($sql);

        if ($query->num_rows() > 0)
        {
            $results['results'] = $query->result_array();
        }
        else
        {
            $results['results'] = NULL;
        }

        $sql = "SELECT FOUND_ROWS() AS total";
        $query = $this->db->query($sql);
        $results['total'] = $query->row()->total;

        return $results;
    }
   
    /**
     * Add a new Plan
     *
     * @param  array $data
     * @return mixed|boolean
     */
    function add_plan($data = array())
    {
        if ($data)
        {

            $data['excluded_days'] = implode(",", $data['excluded_days']);

            $data_insert = array(
                'base' => $data['base'],
                'period_days' => $data['period_days'],
                'period_days_before' => $data['period_days_before'],
                'min_profit' => $data['min_profit'],
                'max_profit' => $data['max_profit'],
                'status' => $data['status'],
                'excluded_days' => $data['excluded_days'],
                'spanish_name' => $data['spanish_name'],
                'spanish_features' => $data['spanish_features'],
                'english_name' => $data['english_name'],
                'english_features' => $data['english_features'],
                'portuguese_name' => $data['portuguese_name'],
                'portuguese_features' => $data['portuguese_features'],                
                'last_updated' => date('Y-m-d H:i:s')
            );

            $this->db->insert($this->_db->_name, $data_insert); 

            if ($id = $this->db->insert_id())
            {
                return $id;
            }
        }

        return FALSE;
    }

    /**
     * Get specific Plan
     *
     * @param  int $id
     * @return array|boolean
     */
    function get_plan($id = NULL)
    {
        if ($id)
        {
            $sql = "
                SELECT *
                FROM {$this->_db->_name}
                WHERE id = " . $this->db->escape($id) . "
            ";

            $query = $this->db->query($sql);

            if ($query->num_rows())
            {
                return $query->row_array();
            }
        }

        return FALSE;
    }

    /**
     * Edit an existing user
     *
     * @param  array $data
     * @return boolean
     */
    function edit_plan($data = array())
    {
        if ($data && empty( $data['id'] ) == FALSE )
        {
            $data['excluded_days'] = implode(",", $data['excluded_days']);

            $data_update = array(
                'base' => $data['base'],
                'period_days' => $data['period_days'],
                'period_days_before' => $data['period_days_before'],
                'min_profit' => $data['min_profit'],
                'max_profit' => $data['max_profit'],
                'status' => $data['status'],
                'excluded_days' => $data['excluded_days'],
                'spanish_name' => $data['spanish_name'],
                'spanish_features' => $data['spanish_features'],
                'english_name' => $data['english_name'],
                'english_features' => $data['english_features'],
                'portuguese_name' => $data['portuguese_name'],
                'portuguese_features' => $data['portuguese_features'],  
                'last_updated' => date('Y-m-d H:i:s')
            );

            $this->db->where('id', $data['id']);
            $this->db->update($this->_db->_name, $data_update); 

            if ($this->db->affected_rows())
            {
                return TRUE;
            }
        }

        return FALSE;
    }

    /**
     * Soft delete an existing Plan
     *
     * @param  int $id
     * @return boolean
     */
    function delete_plan($id = NULL)
    {
        if ($id)
        {
            $sql = "
                UPDATE {$this->_db->_name}
                SET
                    status = 'deleted',
                    last_updated = '" . date('Y-m-d H:i:s') . "'
                WHERE id = " . $this->db->escape($id) . " LIMIT 1 ";

            $this->db->query($sql);

            if ($this->db->affected_rows())
            {
                return TRUE;
            }
        }

        return FALSE;
    }

}
