<?php (defined('BASEPATH')) OR exit('No direct script access allowed');

class MY_Loader extends CI_Loader{
	
    function __construct()
    {
        parent::__construct();
        
        // get main CI object
        $this->CI = & get_instance();
    }

}
