<?php defined('BASEPATH') OR exit('No direct script access allowed');

/**
 * Base Private Class - used for all private pages
 */
class Private_Controller extends MY_Controller {

    var $data;

    /**
     * Constructor
     */
    function __construct()
    {
        parent::__construct();
        $this->load->model('Subscriptions_model','subscription');
  
        if (!$this->session->userdata('logged_in') && strpos(current_url(), "notify_payment")== false){
            redirect(base_url('login'));
        }

        // make sure this user is setup as admin
        if (
            $this->user['is_admin'] &&
            strpos( strtolower( base_url("orders") ), current_url()) != FALSE

        ){
            redirect(base_url('admin/dashboard'));
        }


        // Monthly payments notifications
        if(!is_active() && !is_first_login() && !is_preferencial_user() && $this->user['profile'] == 'user'){
            $this->session->set_flashdata('error', lang('monthly_suspended'));
        }

        if(is_first_login()  && $this->user["is_admin"] != 1){
            $this->session->set_flashdata('message', lang("first_login"));
        }

        
        // Susbcription validation         
        $active = $this->subscription->get_active($this->user['id']);
        $expired_allowed = array('/user/renewal','/user/wallets');
        
        if(!$active && $this->user['profile'] == 'user') {
            $this->session->set_flashdata('error', lang('access_denied_expired'));
//            if (!in_array(trim($this->current_uri),$expired_allowed)){
                $this->session->unset_userdata('logged_in');
                redirect('renew/' . $this->user['id']);
//            }
        } 
        
        
   

        // load the admin language file
        $this->lang->load('admin');

        // prepare theme name
        $this->settings->theme = strtolower($this->config->item('gentella_theme'));

        // set up global header data
        $this

            ->add_root_css( "bower_components/bootstrap/css/bootstrap.min.css" )
            ->add_root_css( "assets/pages/waves/css/waves.min.css" )
            ->add_root_css( "assets/icon/feather/css/feather.css" )
            ->add_root_css( "assets/css/widget.css" )
            ->add_root_css( "assets/css/style.css" )
            ->add_root_css( "assets/css/pages.css" )
            ->add_root_css( "assets/icon/font-awesome/css/font-awesome.min.css" )

            ->add_css_theme( "custom.css" )

            ->add_js_theme( "custom.js" )

            ->add_root_js( "bower_components/jquery/js/jquery.min.js" )
            ->add_root_js( "bower_components/jquery-ui/js/jquery-ui.min.js" )
            ->add_root_js( "bower_components/popper.js/js/popper.min.js" )
            ->add_root_js( "bower_components/bootstrap/js/bootstrap.min.js" )
            ->add_root_js( "assets/pages/waves/js/waves.min.js" )
            ->add_root_js( "bower_components/jquery-slimscroll/js/jquery.slimscroll.js" )
            ->add_root_js( "assets/pages/chart/float/jquery.flot.js" )
            ->add_root_js( "assets/pages/chart/float/jquery.flot.categories.js" )
            ->add_root_js( "assets/pages/chart/float/curvedLines.js" )
            ->add_root_js( "assets/pages/chart/float/jquery.flot.tooltip.min.js" )
            ->add_root_js( "assets/pages/widget/amchart/amcharts.js" )
            ->add_root_js( "assets/pages/widget/amchart/serial.js" )
            ->add_root_js( "assets/pages/widget/amchart/light.js" )
            ->add_root_js( "assets/js/pcoded.min.js" )
            ->add_root_js( "assets/js/vertical/vertical-layout.min.js" )
            ->add_root_js( "assets/pages/dashboard/custom-dashboard.min.js" )
            ->add_root_js( "assets/js/script.min.js" );

        // declare main template
        // $this->template = "../../{$this->settings->root_folder}/themes/{$this->settings->theme}/template.php";
        $this->template = "../../themes/{$this->settings->theme}/template.php";
    }

}