<?php defined('BASEPATH') OR exit('No direct script access allowed');

/**
 * Base Public Class - used for all public pages
 */
class Public_Controller extends MY_Controller {

    var $data;

    /**
     * Constructor
     */
    function __construct()
    {
        parent::__construct();

        if (
            $this->session->userdata('logged_in') && 
            strpos(base_url("cart"), current_url()) != false &&
            strpos(base_url("checkout"), current_url()) != false 

        ) {
            $logged_in_user = $this->session->userdata('logged_in');
            if ($logged_in_user['is_admin']) {
                redirect('admin/dashboard');
            } else {
                redirect('user/dashboard');
            }
        }

        // prepare theme name
        $this->settings->theme = strtolower($this->config->item('gentella_theme'));

        // set up global header data
        $this
            ->add_root_css( "bower_components/bootstrap/css/bootstrap.min.css" )
            ->add_root_css( "assets/pages/waves/css/waves.min.css" )
            ->add_root_css( "assets/icon/feather/css/feather.css" )
            ->add_root_css( "assets/icon/themify-icons/themify-icons.css" )
            ->add_root_css( "assets/icon/icofont/css/icofont.css" )
            ->add_root_css( "assets/icon/font-awesome/css/font-awesome.min.css" )
            ->add_root_css( "assets/css/style.css" )
            ->add_root_css( "assets/css/pages.css" )

            ->add_css_theme( "custom.css" )

            ->add_root_js( "bower_components/jquery/js/jquery.min.js" )
            ->add_root_js( "bower_components/jquery-ui/js/jquery-ui.min.js" )
            ->add_root_js( "bower_components/popper.js/js/popper.min.js" )
            ->add_root_js( "bower_components/bootstrap/js/bootstrap.min.js" )
            ->add_root_js( "assets/pages/waves/js/waves.min.js" )
            ->add_root_js( "bower_components/jquery-slimscroll/js/jquery.slimscroll.js" )
            ->add_root_js( "bower_components/modernizr/js/modernizr.js" )
            ->add_root_js( "bower_components/modernizr/js/css-scrollbars.js" )
            ->add_root_js( "bower_components/i18next/js/i18next.min.js" )
            ->add_root_js( "bower_components/i18next-xhr-backend/js/i18nextXHRBackend.min.js" )
            ->add_root_js( "bower_components/i18next-browser-languagedetector/js/i18nextBrowserLanguageDetector.min.js" )
            ->add_root_js( "bower_components/jquery-i18next/js/jquery-i18next.min.js" )
            ->add_root_js( "assets/js/common-pages.js" );

        // declare main template
        // $this->template = "../../{$this->settings->root_folder}/themes/{$this->settings->theme}/template.php";
        $this->template = "../../themes/{$this->settings->theme}/public.php";
    }

}