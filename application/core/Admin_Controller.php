<?php defined('BASEPATH') OR exit('No direct script access allowed');

/**
 * Base Admin Class - used for all administration pages
 */
class Admin_Controller extends MY_Controller {

    var $data;

    /**
     * Constructor
     */
    function __construct()
    {
        parent::__construct();

        // must be logged in
        if ( ! $this->user)
        {
            if (current_url() != base_url())
            {
                //store requested URL to session - will load once logged in
                $data = array('redirect' => current_url());
                $this->session->set_userdata($data);
            }

            redirect('login');
        }

        // make sure this user is setup as admin
        if (!$this->user['is_admin'])
        {
            redirect(base_url());
        }

        // load the admin language file
        $this->lang->load('admin');

        // prepare theme name
        $this->settings->theme = strtolower($this->config->item('gentella_theme'));

        // set up global header data
        $this

            ->add_root_css( "bower_components/bootstrap/css/bootstrap.min.css" )
            ->add_root_css( "assets/pages/waves/css/waves.min.css" )
            ->add_root_css( "assets/icon/feather/css/feather.css" )
            ->add_root_css( "assets/css/style.css" )
            ->add_root_css( "assets/css/widget.css" )
            ->add_root_css( "assets/css/pages.css" )
            ->add_root_css( "assets/icon/font-awesome/css/font-awesome.min.css" )

            ->add_css_theme( "custom.css" )

            ->add_js_theme( "custom.js" )

            ->add_root_js( "bower_components/jquery/js/jquery.min.js" )
            ->add_root_js( "bower_components/jquery-ui/js/jquery-ui.min.js" )
            ->add_root_js( "bower_components/popper.js/js/popper.min.js" )
            ->add_root_js( "bower_components/bootstrap/js/bootstrap.min.js" )
            ->add_root_js( "assets/pages/waves/js/waves.min.js" )
            ->add_root_js( "bower_components/jquery-slimscroll/js/jquery.slimscroll.js" )
            ->add_root_js( "assets/pages/chart/float/jquery.flot.js" )
            ->add_root_js( "assets/pages/chart/float/jquery.flot.categories.js" )
            ->add_root_js( "assets/pages/chart/float/curvedLines.js" )
            ->add_root_js( "assets/pages/chart/float/jquery.flot.tooltip.min.js" )
            ->add_root_js( "assets/pages/widget/amchart/amcharts.js" )
            ->add_root_js( "assets/pages/widget/amchart/serial.js" )
            ->add_root_js( "assets/pages/widget/amchart/light.js" )
            ->add_root_js( "assets/js/pcoded.min.js" )
            ->add_root_js( "assets/js/vertical/vertical-layout.min.js" )
            ->add_root_js( "assets/pages/dashboard/custom-dashboard.min.js" )
            ->add_root_js( "assets/js/script.min.js" );

        // declare main template
        // $this->template = "../../{$this->settings->root_folder}/themes/{$this->settings->theme}/template.php";
        $this->template = "../../themes/{$this->settings->theme}/template.php";
    }

}
