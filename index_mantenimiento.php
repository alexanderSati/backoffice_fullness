<?php


if($_SERVER['REMOTE_ADDR'] == "190.017.164.146" || $_SERVER['REMOTE_ADDR'] == "181.63.73.182" || $_SERVER['REMOTE_ADDR'] == "186.84.240.207"){


/**
 * CodeIgniter
 *
 * An open source application development framework for PHP
 *
 * This content is released under the MIT License (MIT)
 *
 * Copyright (c) 2014 - 2016, British Columbia Institute of Technology
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 *
 * @package	CodeIgniter
 * @author	EllisLab Dev Team
 * @copyright	Copyright (c) 2008 - 2014, EllisLab, Inc. (https://ellislab.com/)
 * @copyright	Copyright (c) 2014 - 2016, British Columbia Institute of Technology (http://bcit.ca/)
 * @license	http://opensource.org/licenses/MIT	MIT License
 * @link	https://codeigniter.com
 * @since	Version 1.0.0
 * @filesource
 */

/*
 *---------------------------------------------------------------
 * APPLICATION ENVIRONMENT
 *---------------------------------------------------------------
 *
 * You can load different configurations depending on your
 * current environment. Setting the environment also influences
 * things like logging and error reporting.
 *
 * This can be set to anything, but default usage is:
 *
 *     development
 *     testing
 *     production
 *
 * NOTE: If you change these, also change the error_reporting() code below
 */
define('ENVIRONMENT', isset($_SERVER['CI_ENV']) ? $_SERVER['CI_ENV'] : 'development');

/*
 *---------------------------------------------------------------
 * ERROR REPORTING
 *---------------------------------------------------------------
 *
 * Different environments will require different levels of error reporting.
 * By default development will show errors but testing and live will hide them.
 */

switch (ENVIRONMENT)
{
	case 'development':
		error_reporting( E_ERROR|E_PARSE|E_STRICT );
		ini_set('display_errors', 1);
		// error_reporting(0);
		// ini_set('display_errors', 0);
	break;

	case 'testing':
	case 'production':
		ini_set('display_errors', 0);
		if (version_compare(PHP_VERSION, '5.3', '>='))
		{
			error_reporting(E_ALL & ~E_NOTICE & ~E_DEPRECATED & ~E_STRICT & ~E_USER_NOTICE & ~E_USER_DEPRECATED);
		}
		else
		{
			error_reporting(E_ALL & ~E_NOTICE & ~E_STRICT & ~E_USER_NOTICE);
		}
	break;

	default:
		header('HTTP/1.1 503 Service Unavailable.', TRUE, 503);
		echo 'The application environment is not set correctly.';
		exit(1); // EXIT_ERROR
}

/*
 *---------------------------------------------------------------
 * SYSTEM DIRECTORY NAME
 *---------------------------------------------------------------
 *
 * This variable must contain the name of your "system" directory.
 * Set the path if it is not in the same directory as this file.
 */
	$system_path = 'system';

/*
 *---------------------------------------------------------------
 * APPLICATION DIRECTORY NAME
 *---------------------------------------------------------------
 *
 * If you want this front controller to use a different "application"
 * directory than the default one you can set its name here. The directory
 * can also be renamed or relocated anywhere on your server. If you do,
 * use an absolute (full) server path.
 * For more info please see the user guide:
 *
 * https://codeigniter.com/user_guide/general/managing_apps.html
 *
 * NO TRAILING SLASH!
 */
	$application_folder = 'application';

/*
 *---------------------------------------------------------------
 * VIEW DIRECTORY NAME
 *---------------------------------------------------------------
 *
 * If you want to move the view directory out of the application
 * directory, set the path to it here. The directory can be renamed
 * and relocated anywhere on your server. If blank, it will default
 * to the standard location inside your application directory.
 * If you do move this, use an absolute (full) server path.
 *
 * NO TRAILING SLASH!
 */
	$view_folder = '';


/*
 * --------------------------------------------------------------------
 * DEFAULT CONTROLLER
 * --------------------------------------------------------------------
 *
 * Normally you will set your default controller in the routes.php file.
 * You can, however, force a custom routing by hard-coding a
 * specific controller class/function here. For most applications, you
 * WILL NOT set your routing here, but it's an option for those
 * special instances where you might want to override the standard
 * routing in a specific front controller that shares a common CI installation.
 *
 * IMPORTANT: If you set the routing here, NO OTHER controller will be
 * callable. In essence, this preference limits your application to ONE
 * specific controller. Leave the function name blank if you need
 * to call functions dynamically via the URI.
 *
 * Un-comment the $routing array below to use this feature
 */
	// The directory name, relative to the "controllers" directory.  Leave blank
	// if your controller is not in a sub-directory within the "controllers" one
	// $routing['directory'] = '';

	// The controller class file name.  Example:  mycontroller
	// $routing['controller'] = '';

	// The controller function you wish to be called.
	// $routing['function']	= '';


/*
 * -------------------------------------------------------------------
 *  CUSTOM CONFIG VALUES
 * -------------------------------------------------------------------
 *
 * The $assign_to_config array below will be passed dynamically to the
 * config class when initialized. This allows you to set custom config
 * items or override any default config values found in the config.php file.
 * This can be handy as it permits you to share one application between
 * multiple front controller files, with each file containing different
 * config values.
 *
 * Un-comment the $assign_to_config array below to use this feature
 */
	// $assign_to_config['name_of_config_item'] = 'value of config item';



// --------------------------------------------------------------------
// END OF USER CONFIGURABLE SETTINGS.  DO NOT EDIT BELOW THIS LINE
// --------------------------------------------------------------------

/*
 * ---------------------------------------------------------------
 *  Resolve the system path for increased reliability
 * ---------------------------------------------------------------
 */

	// Set the current directory correctly for CLI requests
	if (defined('STDIN'))
	{
		chdir(dirname(__FILE__));
	}

	if (($_temp = realpath($system_path)) !== FALSE)
	{
		$system_path = $_temp.DIRECTORY_SEPARATOR;
	}
	else
	{
		// Ensure there's a trailing slash
		$system_path = strtr(
			rtrim($system_path, '/\\'),
			'/\\',
			DIRECTORY_SEPARATOR.DIRECTORY_SEPARATOR
		).DIRECTORY_SEPARATOR;
	}

	// Is the system path correct?
	if ( ! is_dir($system_path))
	{
		header('HTTP/1.1 503 Service Unavailable.', TRUE, 503);
		echo 'Your system folder path does not appear to be set correctly. Please open the following file and correct this: '.pathinfo(__FILE__, PATHINFO_BASENAME);
		exit(3); // EXIT_CONFIG
	}

/*
 * -------------------------------------------------------------------
 *  Now that we know the path, set the main path constants
 * -------------------------------------------------------------------
 */
	// The name of THIS file
	define('SELF', pathinfo(__FILE__, PATHINFO_BASENAME));

	// Path to the system directory
	define('BASEPATH', $system_path);

	// Path to the front controller (this file) directory
	define('FCPATH', dirname(__FILE__).DIRECTORY_SEPARATOR);

	// Name of the "system" directory
	define('SYSDIR', basename(BASEPATH));

	// The path to the "application" directory
	if (is_dir($application_folder))
	{
		if (($_temp = realpath($application_folder)) !== FALSE)
		{
			$application_folder = $_temp;
		}
		else
		{
			$application_folder = strtr(
				rtrim($application_folder, '/\\'),
				'/\\',
				DIRECTORY_SEPARATOR.DIRECTORY_SEPARATOR
			);
		}
	}
	elseif (is_dir(BASEPATH.$application_folder.DIRECTORY_SEPARATOR))
	{
		$application_folder = BASEPATH.strtr(
			trim($application_folder, '/\\'),
			'/\\',
			DIRECTORY_SEPARATOR.DIRECTORY_SEPARATOR
		);
	}
	else
	{
		header('HTTP/1.1 503 Service Unavailable.', TRUE, 503);
		echo 'Your application folder path does not appear to be set correctly. Please open the following file and correct this: '.SELF;
		exit(3); // EXIT_CONFIG
	}

	define('APPPATH', $application_folder.DIRECTORY_SEPARATOR);

	// The path to the "views" directory
	if ( ! isset($view_folder[0]) && is_dir(APPPATH.'views'.DIRECTORY_SEPARATOR))
	{
		$view_folder = APPPATH.'views';
	}
	elseif (is_dir($view_folder))
	{
		if (($_temp = realpath($view_folder)) !== FALSE)
		{
			$view_folder = $_temp;
		}
		else
		{
			$view_folder = strtr(
				rtrim($view_folder, '/\\'),
				'/\\',
				DIRECTORY_SEPARATOR.DIRECTORY_SEPARATOR
			);
		}
	}
	elseif (is_dir(APPPATH.$view_folder.DIRECTORY_SEPARATOR))
	{
		$view_folder = APPPATH.strtr(
			trim($view_folder, '/\\'),
			'/\\',
			DIRECTORY_SEPARATOR.DIRECTORY_SEPARATOR
		);
	}
	else
	{
		header('HTTP/1.1 503 Service Unavailable.', TRUE, 503);
		echo 'Your view folder path does not appear to be set correctly. Please open the following file and correct this: '.SELF;
		exit(3); // EXIT_CONFIG
	}

	define('VIEWPATH', $view_folder.DIRECTORY_SEPARATOR);

/*
 * --------------------------------------------------------------------
 * LOAD THE BOOTSTRAP FILE
 * --------------------------------------------------------------------
 *
 * And away we go...
 */
require_once BASEPATH.'core/CodeIgniter.php';

} else {
	?>

	<!doctype html>
	<!--[if lt IE 7]>      <html class="no-js lt-ie9 lt-ie8 lt-ie7" lang=""> <![endif]-->
	<!--[if IE 7]>         <html class="no-js lt-ie9 lt-ie8" lang=""> <![endif]-->
	<!--[if IE 8]>         <html class="no-js lt-ie9" lang=""> <![endif]-->
	<!--[if gt IE 8]><!-->
	<html class="no-js" lang="zxx">
	<!--<![endif]-->
	<head>
		<!--====== USEFULL META ======-->
		<meta charset="UTF-8">
		<meta http-equiv="X-UA-Compatible" content="IE=edge">
		<meta name="viewport" content="width=device-width, initial-scale=1">
		<meta name="description" content="" />
		<meta name="keywords" content="" />
	
		<!--====== TITLE TAG ======-->
		<title>Fullness Global</title>
	
		<!--====== FAVICON ICON =======-->
		<link rel="shortcut icon" type="image/ico" href="img/fullimg/favicon.png" />
	
		<!--====== STYLESHEETS ======-->
		<link rel="stylesheet" href="css/normalize.css">
		<link rel="stylesheet" href="css/animate.css">
		<link rel="stylesheet" href="css/magnific-popup.css">
		<link rel="stylesheet" href="css/typed.css">
		<link href="css/bootstrap.min.css" rel="stylesheet">
		<link href="css/font-awesome.min.css" rel="stylesheet">
	
		<!--====== MAIN STYLESHEETS ======-->
		<link href="style.css" rel="stylesheet">
		<link href="css/responsive.css" rel="stylesheet">
	
		<script src="js/vendor/modernizr-2.8.3.min.js"></script>
		<!--[if lt IE 9]>
			<script src="//oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
			<script src="//oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
			<![endif]-->
			
		<!-- Global site tag (gtag.js) - Google Analytics -->
		<script async src="https://www.googletagmanager.com/gtag/js?id=UA-143446119-1">
		</script>
		<script>
		  window.dataLayer = window.dataLayer || [];
		  function gtag(){dataLayer.push(arguments);}
		  gtag('js', new Date());
		
		  gtag('config', 'UA-143446119-1');
		</script>
	
	</head>
	
	<body class="transparent-layer shader-bg">
	
		<!--[if lt IE 8]>
			<p class="browserupgrade">You are using an <strong>outdated</strong> browser. Please <a href="http://browsehappy.com/">upgrade your browser</a> to improve your experience.</p>
		<![endif]-->
	
		<!--- PRELOADER -->
		<div class="preeloader">
			<div class="preloader-spinner">
				<img src="img/loading.svg" width="100%" alt="">
			</div>
		</div>
	
		<!--START MAIN AREA-->
		<div class="main-area" id="home">
			<div class="main-area-bg"></div>
			<div id="surface-shader" data-ambient-color="#00aff0" data-diffuse-color="#666666"></div>
	
			<!--WELCOME AREA CONTENT-->
			<div class="welcome-text-area">
				<div class="container">
				  <div class="row">
					  <div class="col-md-12 text-center">
						<img src="img/pc.png " alt="logo" class="logo">
					  </div>
				  </div>
					<div class="row">
						<div class="col-md-10 col-lg-10 col-md-offset-1 col-lg-offset-1 col-sm-12 col-xs-12">
							<div class="welcome-text text-center">
								<div class="clock-countdown">
									<div class="site-config" data-date="02/07/2020 23:59:59" data-date-timezone="+0"></div>
									<div class="coundown-timer">
										<div class="single-counter"><span class="days">00</span><span class="normal">Dias</span></div>
										<div class="single-counter"><span class="hours">02</span><span class="normal">Horas</span></div>
										<div class="single-counter"><span class="minutes">00</span><span class="normal">Minutos</span></div>
										<div class="single-counter"><span class="seconds">00</span><span class="normal">Segundos</span></div>
									</div>
								</div>
								<h3>Regresaremos pronto, Fullness Global esta trabajando en mejoras de nuestra plataforma lamentamos las molestias </h3>
							   
								<h1 class="hidden-xs cd-headline clip is-full-width">
									<span class="hero-text">para</span>
									<span class="cd-words-wrapper">
										<b class="is-visible">emprender</b>
										<b>crecer</b>
										<b>avanzar</b>
									</span>
								</h1>
	
							   
							</div>
						</div>
					</div>
				</div>
			</div>
			<div class="social-book-mark">
				<ul>
					<!--<li><a href="#"><i class="fa fa-facebook"></i></a></li>
					<li><a href="#"><i class="fa fa-twitter"></i></a></li>
					<li><a href="#"><i class="fa fa-google-plus"></i></a></li>
					<li><a href="#"><i class="fa fa-linkedin"></i></a></li>
					<li><a href="#"><i class="fa fa-pinterest"></i></a></li>-->
				</ul>
			</div>
			<div class="information-contact">
				<ul>
					<!--<li><a class="contact-button" href="#"><i class="fa fa-envelope-o"></i></a></li>
					<li><a class="info-button" href="#"><i class="fa fa-info"></i></a></li>-->
				</ul>
			</div>
			<!--WELCOME AREA CONTENT END-->
	
			<!--LEFT CONTACT CONTENT-->
			<div class="left-contact-content">
				<div class="push-content-close"><i class="fa fa-close"></i></div>
				<div class="contact-address-and-details section-padding">
					<div class="row">
						<div class="col-md-12 col-lg-12 col-sm-12 col-xs-12">
							<div class="logo text-center">
								<!--<img src="/img/fullimg/logo.png " alt="logo">-->
								<h3>Fullness Global</h3>
							</div>
						</div>
					</div>
	
	
					<!--<div class="contact-details">
						<div class="col-md-12 col-xs-12">
							<div class="area-title title-inverse">
								<div class="title-box"></div>
								<h2>Contact Us</h2>
							</div>
						</div>
						<div class="row">
							<div class="col-md-6 col-lg-6 col-sm-12 col-xs-12">
								<div class="single-contact">
									<h4>Address:</h4>
									<p>H-496, R-10, West Nakhalpara, Dhaka, 1215 United States.</p>
								</div>
								<div class="single-contact">
									<h4>Email:</h4>
									<p><a href="mailto:info@yourcompany.com">info@yourcompany.com</a></p>
								</div>
								<div class="single-contact">
									<h4>Phone:</h4>
									<p>+88 012345 123 123</p>
								</div>
							</div>
							<div class="col-md-6 col-lg-6 col-sm-12 col-xs-12">
								<div class="single-contact">
									<h4>Website:</h4>
									<p><a href="http://www.youwebsite.com/">www.youwebsite.com</a></p>
								</div>
								<div class="single-contact">
									<h4>Fax:</h4>
									<p>+88 012345 123 123</p>
								</div>
								<div class="single-contact">
									<h4>Follow Us On:</h4>
									<ul>
										<li><a href="#"><i class="fa fa-facebook"></i></a></li>
										<li><a href="#"><i class="fa fa-twitter"></i></a></li>
										<li><a href="#"><i class="fa fa-google-plus"></i></a></li>
										<li><a href="#"><i class="fa fa-linkedin"></i></a></li>
										<li><a href="#"><i class="fa fa-pinterest"></i></a></li>
									</ul>
								</div>
							</div>
						</div>
						<p class="contact-hidding">Do You Need To Send A Personal Query ? Send Query Now !</p>
					</div>-->
					<div class="contact-form-area">
						<div class="row">
							<div class="col-md-12 col-lg-12 col-sm-12 col-xs-12">
								<h4>Ingresa tus datos aquí</h4>
								<div class="contact-details">
									<div class="col-md-12 col-xs-12">
									  <p>Bienvenido a nuestra familia Fullness Global, despues de realizar el pre-registro y una vez validado el email, procederemos a crear tu usuario y contraseña el cual recibiras via email. Para que puedas entrar a nuestro backoffice</p>
									</div>
								</div>
								<?php require 'form.php' ?>
	
							</div>
	
						</div>
					</div>
				</div>
			</div>
			<!--LEFT CONTACT CONTENT END-->
	
			<!--RIGHT ABOUT AREA-->
			<div class="right-details-content">
				<div class="push-content-close"><i class="fa fa-close"></i></div>
				<div class="about-area section-padding">
					<div class="row">
						<div class="col-md-12 col-xs-12">
							<div class="area-title">
								<div class="title-box"></div>
								<h2>About Us</h2>
							</div>
						</div>
					</div>
					<div class="row">
						<div class="col-md-12 col-xs-12">
							<div class="about-details">
								<p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Quas aut quidem numquam! Molestias nisi odio itaque eaque vitae dolorem ipsum a, exercitationem laboriosam in quibusdam debitis sequi impedit quia officiis, esse ut fuga doloribus voluptas magnam quasi quo atque dolorum.</p>
								<p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Quas aut quidem numquam! Molestias nisi odio itaque eaque vitae dolorem ipsum a, exercitationem laboriosam in quibusdam debitis sequi impedit quia officiis, esse ut fuga doloribus voluptas magnam quasi quo atque dolorum.</p>
							</div>
						</div>
					</div>
				</div>
				<div class="service-area padding-bottom">
					<div class="row">
						<div class="col-md-12 col-xs-12">
							<div class="area-title">
								<div class="title-box"></div>
								<h2>Our Services</h2>
							</div>
						</div>
					</div>
					<div class="row">
						<div class="col-md-6 col-xs-12">
							<div class="single-service text-center">
								<div class="servive-icon"><i class="fa fa-laptop"></i></div>
								<h3>Branding</h3>
								<p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Quas aut quidem numquam! Molestias nisi odio.</p>
							</div>
						</div>
						<div class="col-md-6 col-xs-12">
							<div class="single-service text-center">
								<div class="servive-icon"><i class="fa fa-line-chart"></i></div>
								<h3>Marketing</h3>
								<p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Quas aut quidem numquam! Molestias nisi odio.</p>
							</div>
						</div>
						<div class="col-md-6 col-xs-12">
							<div class="single-service text-center">
								<div class="servive-icon"><i class="fa fa-newspaper-o"></i></div>
								<h3>Web Design</h3>
								<p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Quas aut quidem numquam! Molestias nisi odio.</p>
							</div>
						</div>
						<div class="col-md-6 col-xs-12">
							<div class="single-service text-center">
								<div class="servive-icon"><i class="fa fa-camera-retro"></i></div>
								<h3>Photography</h3>
								<p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Quas aut quidem numquam! Molestias nisi odio.</p>
							</div>
						</div>
					</div>
				</div>
				<div class="gallery-area padding-bottom">
					<div class="row">
						<div class="col-md-12 col-xs-12">
							<div class="area-title">
								<div class="title-box"></div>
								<h2>Our Project</h2>
							</div>
						</div>
					</div>
					<div class="row">
						<div class="col-md-6 col-xs-12">
							<div class="single-gallery">
								<div class="gallery-thumb">
									<img src="img/gallery/gallery-1.jpg" alt="">
								</div>
								<a href="img/gallery/gallery-1.jpg" class="gallery-view" data-effect="mfp-zoom-in">
									<h3>Branding Design</h3>
									<p>Branding and illustration</p>
								</a>
							</div>
						</div>
						<div class="col-md-6 col-xs-12">
							<div class="single-gallery">
								<div class="gallery-thumb">
									<img src="img/gallery/gallery-2.jpg" alt="">
								</div>
								<a href="img/gallery/gallery-2.jpg" class="gallery-view" data-effect="mfp-zoom-in">
									<h3>Branding Design</h3>
									<p>Branding and illustration</p>
								</a>
							</div>
						</div>
						<div class="col-md-6 col-xs-12">
							<div class="single-gallery">
								<div class="gallery-thumb">
									<img src="img/gallery/gallery-3.jpg" alt="">
								</div>
								<a href="img/gallery/gallery-3.jpg" class="gallery-view" data-effect="mfp-zoom-in">
									<h3>Branding Design</h3>
									<p>Branding and illustration</p>
								</a>
							</div>
						</div>
						<div class="col-md-6 col-xs-12">
							<div class="single-gallery">
								<div class="gallery-thumb">
									<img src="img/gallery/gallery-4.jpg" alt="">
								</div>
								<a href="img/gallery/gallery-4.jpg" class="gallery-view" data-effect="mfp-zoom-in">
									<h3>Branding Design</h3>
									<p>Branding and illustration</p>
								</a>
							</div>
						</div>
					</div>
				</div>
				<div class="footer-area dark-bg">
					<div class="footer-copyright text-center">
						<p>Copyright &copy; <a href="http://igniweb.com">IGNIWEB</a> All Right Reserved.</p>
					</div>
				</div>
			</div>
			<!--RIGHT ABOUT AREA END-->
	
		</div>
		<!--END MAIN AREA-->
	
		<!--====== SCRIPTS JS ======-->
		<script src="js/vendor/jquery-1.12.4.min.js"></script>
		<script src="js/vendor/bootstrap.min.js"></script>
	
		<!--====== PLUGINS JS ======-->
		<script src="js/vendor/jquery.easing.1.3.js"></script>
		<script src="js/vendor/jquery-migrate-1.2.1.min.js"></script>
		<script src="js/typed.js"></script>
		<script src="js/jquery.downCount.js"></script>
		<script src="js/jquery.nicescroll.min.js"></script>
		<script src="js/jquery.magnific-popup.min.js"></script>
		<script src="js/jquery.ajaxchimp.js"></script>
		<script src="js/contact-form.js"></script>
		<script src="js/shader/fss.min.js"></script>
		<script src="js/shader/shader-active.js"></script>
	
		<!--===== ACTIVE JS=====-->
		<script src="js/main.js"></script>
	</body>
	
	
	</html>

<?php
}
?>