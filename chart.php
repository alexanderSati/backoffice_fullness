<?php 
$coin_get = empty( $_GET['coin'] ) ? 'bitcoin' : $_GET['coin']; ?>
<div id="charts"> <div id="highcharts-graph" data-highcharts-chart="0"></div> </div>
<div id="metadata" class="hidden" data-slug="<?php echo $coin_get; ?>" data-name="<?php echo ucfirst($coin_get); ?>" data-apidomain="https://graphs.coinmarketcap.com/currencies/bitcoin/"></div>
<div id="static_domain" data-staticdomain="files.coinmarketcap.com"></div>
<script src="/themes/gentella/js/jquery.min.js"></script>
<script src="/themes/gentella/js/base.min.js"></script>
<script src="/themes/gentella/js/highcharts.js"></script>
<script src="/themes/gentella/js/detail.min.js"></script>