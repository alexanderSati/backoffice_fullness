<?php defined('BASEPATH') OR exit('No direct script access allowed');
/**
 * Admin Template
 */
?>
<!DOCTYPE html>
<html>

<head>
  <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <!-- <link rel="shortcut icon" type="image/x-icon" href="/themes/<?= $this->settings->theme ?>/images/icon.png?v=<?php echo $this->settings->site_version; ?>">
    <link rel="icon" type="image/x-icon" href="/themes/<?= $this->settings->theme ?>/images/icon.png?v=<?php echo $this->settings->site_version; ?>"> -->
  <title><?php echo $page_title; ?> - <?php echo $this->settings->site_name; ?></title>

  <?php // CSS files ?>
  <?php if (isset($css_files) && is_array($css_files)) : ?>
  <?php foreach ($css_files as $css) : ?>
  <?php if (!is_null($css)) : ?>
  <link rel="stylesheet" href="<?php echo $css; ?>?v=<?php echo $this->settings->site_version; ?>"><?php echo "\n"; ?>
  <?php endif; ?>
  <?php endforeach; ?>
  <?php endif; ?>

  <script src="/themes/<?= $this->settings->theme ?>/js/jquery.min.js"></script>

  <!--[if lt IE 9]>
    <script src="../assets/js/ie8-responsive-file-warning.js"></script>
    <![endif]-->

  <!-- HTML5 shim and Respond.js IE8 support of HTML5 elements and media queries -->
  <!--[if lt IE 9]>
    <script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
    <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
    <![endif]-->
  <!-- <script>
        (function(d, w, c) {
            w.ChatraID = 'ayhprFCLzMGXvb54T';
            var s = d.createElement('script');
            w[c] = w[c] || function() {
                (w[c].q = w[c].q || []).push(arguments);
            };
            s.async = true;
            s.src = (d.location.protocol === 'https:' ? 'https:': 'http:')
            + '//call.chatra.io/chatra.js';
            if (d.head) d.head.appendChild(s);
        })(document, window, 'Chatra');
    </script> -->
  <script>
  window.ChatraSetup = {
    chatWidth: 400,
    chatHeight: 550
  };
  </script>
  <script>
  (function(i, s, o, g, r, a, m) {
    i['GoogleAnalyticsObject'] = r;
    i[r] = i[r] || function() {
      (i[r].q = i[r].q || []).push(arguments)
    }, i[r].l = 1 * new Date();
    a = s.createElement(o),
      m = s.getElementsByTagName(o)[0];
    a.async = 1;
    a.src = g;
    m.parentNode.insertBefore(a, m)
  })(window, document, 'script', 'https://www.google-analytics.com/analytics.js', 'ga');

  ga('create', 'UA-92102278-1', 'auto');
  ga('send', 'pageview');
  </script>
</head>

<body class="welcome_login">

  <div class="">
    <input id="site_url" value="<?= base_url() ?>" type="hidden" />

    <?php // Main content ?>
    <?php echo $content; ?>

  </div>

  <?php // Javascript files ?>
  <?php if (isset($js_files) && is_array($js_files)) : ?>
  <?php foreach ($js_files as $js) : ?>
  <?php if (!is_null($js)) : ?>
  <?php echo "\n"; ?>
  <script type="text/javascript" src="<?php echo $js; ?>?v=<?php echo $this->settings->site_version; ?>"></script>
  <?php echo "\n"; ?>
  <?php endif; ?>
  <?php endforeach; ?>
  <?php endif; ?>
  <?php if (isset($js_files_i18n) && is_array($js_files_i18n)) : ?>
  <?php foreach ($js_files_i18n as $js) : ?>
  <?php if (!is_null($js)) : ?>
  <?php echo "\n"; ?>
  <script type="text/javascript">
  <?php echo "\n" . $js . "\n"; ?>
  </script><?php echo "\n"; ?>
  <?php endif; ?>
  <?php endforeach; ?>
  <?php endif; ?>

</body>

</html>