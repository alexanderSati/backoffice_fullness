<?php defined('BASEPATH') OR exit('No direct script access allowed');
/**
 * Admin Template
 */
?>
<!DOCTYPE html>
<html>

<head>
  <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <!-- <link rel="shortcut icon" type="image/x-icon" href="/themes/<?= $this->settings->theme ?>/images/icon.png?v=<?php echo $this->settings->site_version; ?>"> -->
  <!-- <link rel="icon" type="image/x-icon" href="/themes/<?= $this->settings->theme ?>/images/icon.png?v=<?php echo $this->settings->site_version; ?>"> -->
  <title><?php echo $page_title; ?> - <?php echo $this->settings->site_name; ?></title>
  <!-- Global site tag (gtag.js) - Google Analytics -->
  <script async src="https://www.googletagmanager.com/gtag/js?id=UA-143446119-1"></script>
  <script>
  window.dataLayer = window.dataLayer || [];

  function gtag() {
    dataLayer.push(arguments);
  }
  gtag('js', new Date());

  gtag('config', 'UA-143446119-1');
  </script>

  <?php // CSS files ?>
  <?php if (isset($css_files) && is_array($css_files)) : ?>
  <?php foreach ($css_files as $css) : ?>
  <?php if ( ! is_null($css)) : ?>
  <link rel="stylesheet" href="<?php echo $css; ?>?v=<?php echo $this->settings->site_version; ?>"><?php echo "\n"; ?>
  <?php endif; ?>
  <?php endforeach; ?>
  <?php endif; ?>

  <script src="/themes/<?= $this->settings->theme ?>/js/jquery.min.js"></script>

  <!--[if lt IE 9]>
    <script src="../assets/js/ie8-responsive-file-warning.js"></script>
    <![endif]-->

  <!-- HTML5 shim and Respond.js IE8 support of HTML5 elements and media queries -->
  <!--[if lt IE 9]>
    <script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
    <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
    <![endif]-->
  <?php if(isset($chat)) {
            if($chat) {?>
  <script>
  (function(d, w, c) {
    w.ChatraID = 'ayhprFCLzMGXvb54T';
    var s = d.createElement('script');
    w[c] = w[c] || function() {
      (w[c].q = w[c].q || []).push(arguments);
    };
    s.async = true;
    s.src = (d.location.protocol === 'https:' ? 'https:' : 'http:') +
      '//call.chatra.io/chatra.js';
    if (d.head) d.head.appendChild(s);
  })(document, window, 'Chatra');
  </script>
  <script>
  window.ChatraSetup = {
    chatWidth: 400,
    chatHeight: 550
  };
  </script>
  <?php } } ?>
  <!-- Global site tag (gtag.js) - Google Analytics -->
  <script async src="https://www.googletagmanager.com/gtag/js?id=UA-143446119-1"></script>
  <script>
  window.dataLayer = window.dataLayer || [];

  function gtag() {
    dataLayer.push(arguments);
  }
  gtag('js', new Date());

  gtag('config', 'UA-143446119-1');
  </script>
</head>

<body class="nav-md">

  <div class="container body">


    <div class="main_container">

      <div class="col-md-3 left_col">
        <div class="left_col scroll-view">

          <div class="navbar nav_title" style="border: 0;">
            <a href="<?php echo base_url('admin/dashboard'); ?>" class="site_title">
              <img width="112" src="/themes/<?= $this->settings->theme ?>/images/logo.png"
                alt="<?php echo $this->settings->site_name; ?>">
            </a>
          </div>
          <div class="clearfix"></div>


          <!-- menu prile quick info -->
          <!-- /menu prile quick info -->

          <br />

          <!--  sidebar menu  -->
          <?php  if($this->session->userdata('logged_in') ): ?>
          <?php $this->load->view('menu', NULL); ?>
          <?php elseif( strpos(base_url('cart'),current_url()) != false ): ?>
          <?php $this->load->view('sidebar_cart', NULL); ?>
          <?php  endif; ?>
          <!--  ./sidebar menu  -->


          <!-- /menu footer buttons -->
          <?php  if($this->session->userdata('logged_in')): ?>
          <div class="sidebar-footer hidden-small">
            <a href="<?php echo base_url('user/profile'); ?>" data-toggle="tooltip" data-placement="top"
              title="<?php echo lang('core button profile'); ?>">
              <span class="glyphicon glyphicon glyphicon-user" aria-hidden="true"></span>
            </a>
            <a href="<?php echo base_url('logout'); ?>" data-toggle="tooltip" data-placement="top"
              title="<?php echo lang('core button logout'); ?>">
              <span class="glyphicon glyphicon-off" aria-hidden="true"></span>
            </a>
          </div>
          <?php  endif; ?>
          <!-- /menu footer buttons -->
        </div>
      </div>

      <style>
      .header-stats {
        text-align: center;
        color: white;
        background: black;
        margin-bottom: 20px;
      }

      .header-stats .column {
        padding: 15px;
      }

      .header-stats .coins {
        max-width: 60px;
      }

      .header-stats .name {
        font-size: 1em;
        font-weight: bold
      }

      .header-stats .rate {
        font-size: 2em;
        font-weight: bold
      }
      </style>

      <!-- top navigation -->
      <div class="top_nav">
        <input id="site_url" value="<?= base_url() ?>" type="hidden" />
        <?php
                $CI = &get_instance();

                $CI->load->model('subscriptions_model','subscriptions');
                $CI->load->library('currency');
                $subscription = $CI->subscriptions->get_active($this->user['id']);
                ?>

        <div class="col-xs-12  col-sm-12 header-stats">
          <div class="col-sm-1 col-xs-1  column">
            <div class="nav toggle">
              <a id="menu_toggle"><i class="fa fa-bars"></i></a>
            </div>
          </div>

          <div class="col-sm-2 col-sm-offset-9 col-xs-10">
            <div class="nav_menu">
              <nav class="" role="navigation">
                <?php if( $this->session->userdata('logged_in') ): ?>
                <ul class="nav navbar-nav navbar-right">
                  <li class="">
                    <a href="javascript:;" class="user-profile dropdown-toggle" data-toggle="dropdown"
                      aria-expanded="false">
                      <div class="profile_pic">
                        <img src="/uploads/<?= ($this->user['image'] != "") ? $this->user['image'] : 'img.jpg' ?>"
                          alt="..." class="img-circle profile_img">
                      </div>
                      <p><?= $this->user['username'] ?> <span class=" fa fa-angle-down"></span></p>
                    </a>
                    <ul class="dropdown-menu dropdown-usermenu animated fadeInDown pull-right">
                      <li><a href="<?php echo base_url('user/profile'); ?>">
                          <?php echo lang('core button profile'); ?></a>
                      </li>
                      <?php if ($this->user['is_admin']) { ?>
                      <li>
                        <a href="<?php echo base_url('/admin/settings'); ?>">
                          <span><?php echo lang('admin button settings'); ?></span>
                        </a>
                      </li>
                      <?php } ?>
                      <li><a href="<?php echo base_url('logout'); ?>"><i class="fa fa-sign-out pull-right"></i>
                          <?php echo lang('core button logout'); ?></a>
                      </li>
                    </ul>
                  </li>

                  <li>
                    <span class="dropdown" style="display: inline-block;margin-top: 12px;">
                      <button id="session-language" type="button" data-toggle="dropdown" aria-haspopup="true"
                        aria-expanded="false" class="btn btn-default">
                        <i class="fa fa-language"></i>
                        <span class="caret"></span>
                      </button>
                      <ul id="session-language-dropdown" class="dropdown-menu" role="menu"
                        aria-labelledby="session-language">
                        <?php foreach ($this->languages as $key=>$name) : ?>
                        <li>
                          <a href="#" rel="<?php echo $key; ?>">
                            <?php if ($key == $this->session->language) : ?>
                            <i class="fa fa-check selected-session-language"></i>
                            <?php endif; ?>
                            <?php echo $name; ?>
                          </a>
                        </li>
                        <?php endforeach; ?>
                      </ul>
                    </span>
                  </li>

                </ul>
                <?php endif; ?>
              </nav>
            </div>
          </div>
        </div>

      </div>

      <!-- /top navigation -->


      <!-- page content -->
      <div class="right_col" role="main">

        <br />
        <div class="principal-content">

          <?php // System messages ?>
          <?php if ($this->session->flashdata('message')) : ?>
          <div class="alert alert-success alert-dismissable">
            <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
            <?php echo $this->session->flashdata('message'); ?>
          </div>
          <?php elseif ($this->session->flashdata('error')) : ?>
          <div class="alert alert-danger alert-dismissable">
            <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
            <i class="fa fa-exclamation-triangle" aria-hidden="true"></i>
            <?php echo $this->session->flashdata('error'); ?>
          </div>
          <?php elseif ($this->session->flashdata('warning')) : ?>
          <div class="alert alert-warning alert-dismissable">
            <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
            <i class="fa fa-info-circle" aria-hidden="true"></i>
            <?php echo $this->session->flashdata('warning'); ?>
          </div>
          <?php elseif (validation_errors()) : ?>
          <div class="alert alert-danger alert-dismissable">
            <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
            <?php echo validation_errors(); ?>
          </div>
          <?php elseif ($this->error) : ?>
          <div class="alert alert-danger alert-dismissable">
            <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
            <?php echo $this->error; ?>
          </div>
          <?php endif; ?>

          <?php // Main content ?>
          <?php echo $content; ?>

        </div>

        <!-- footer content -->
        <!--            <footer>-->
        <!--                <div class="copyright-info">-->
        <!--                    <p class="pull-right">Gentelella - Bootstrap Admin Template by <a href="https://colorlib.com">Colorlib</a>-->
        <!--                    </p>-->
        <!--                </div>-->
        <!--                <div class="clearfix"></div>-->
        <!--            </footer>-->
        <!-- /footer content -->

      </div>
      <!-- /page content -->
    </div>


  </div>

  <div id="custom_notifications" class="custom-notifications dsp_none">
    <ul class="list-unstyled notifications clearfix" data-tabbed_notifications="notif-group">
    </ul>
    <div class="clearfix"></div>
    <div id="notif-group" class="tabbed_notifications"></div>
  </div>

  <?php // Javascript files ?>
  <?php if (isset($js_files) && is_array($js_files)) : ?>
  <?php foreach ($js_files as $js) : ?>
  <?php if ( ! is_null($js)) : ?>
  <?php echo "\n"; ?><script type="text/javascript"
    src="<?php echo $js; ?>?v=<?php echo $this->settings->site_version; ?>"></script><?php echo "\n"; ?>
  <?php endif; ?>
  <?php endforeach; ?>
  <?php endif; ?>
  <?php if (isset($js_files_i18n) && is_array($js_files_i18n)) : ?>
  <?php foreach ($js_files_i18n as $js) : ?>
  <?php if ( ! is_null($js)) : ?>
  <?php echo "\n"; ?><script type="text/javascript">
  <?php echo "\n" . $js . "\n"; ?>
  </script><?php echo "\n"; ?>
  <?php endif; ?>
  <?php endforeach; ?>
  <?php endif; ?>

</body>

</html>