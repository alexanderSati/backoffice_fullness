/**
 * Created by Igniweb038 on 13/02/17.
 */
$('#datatable-responsive').DataTable({
    "order": [[ 0, "desc" ]]
});

var clipboard = new Clipboard('.btn-copy');

$('.btn-left').click(function () {
    $('.btn-left').attr('title','copied!');
    $('.btn-left').tooltip('show');
});
$('.btn-left').hover(function () {
    $('.btn-left').tooltip('destroy');
});
$('.btn-right').click(function () {
    $('.btn-right').attr('title','copied!');
    $('.btn-right').tooltip('show');
});
$('.btn-right').hover(function () {
    $('.btn-right').tooltip('destroy');
});