/**
 * Created by Igniweb on 06/07/17.
 */
$(document).ready(function () {

    $('#sellTable').DataTable({
        responsive: true,
        pageLength: 10,
        colReorder: true,
        searching: false,
        order: [[ 0, 'desc']],
//        columnDefs: [
//            { className: "hidden", "targets": 0 }
//          ],
        fnCreatedRow: function (nRow, aData) {
            $(nRow).attr('id', aData.id);
            $(nRow).attr('class', aData.class);
        }
    });


    $('#buyTable').DataTable({
        responsive: true,
        pageLength: 10,
        colReorder: true,
        searching: false,
        order: [[ 0, 'desc']],
//        columnDefs: [
//            { className: "hidden", "targets": 0 }
//          ],
        fnCreatedRow: function (nRow, aData) {
            $(nRow).attr('id', aData.id);
            $(nRow).attr('class', aData.class);
        }
    });

    $('#historyTable').DataTable({
        responsive: true,
        pageLength: 25,
        colReorder: true,
        searching: false,
        order: [[ 0, 'desc']],
        
        fnCreatedRow: function (nRow, aData) {
            class_history = aData.class;
            if(aData.class_type){
                class_history += ' ' +  aData.class_type
            }
            $(nRow).attr('class', class_history);
        }
    });

    $('.calc').on('keyup blur', function () {
        fee_calc(this);
    });

    function fee_calc(form) {
        
        amount = $(form).find("[name=amount]").val();
        price = $(form).find("[name=btc_price]").val();
        
        if($.isNumeric(amount) === true && $.isNumeric(price) === true){
            total = amount * price;
            fee = (total) * window.feePercent;
            if ($(form).hasClass('buy')) {
                total_fee = total + fee;
            }

            if ($(form).hasClass('sell')) {
                total_fee = total - fee;
            }
        }else {
            total = 0;
            fee = 0;
            total_fee = 0;
        }

        $(form).find("[name=btc_fee]").val(parseFloat(fee).toFixed(8));
        $(form).find("[name=btc_total]").val(parseFloat(total).toFixed(8));
        $(form).find("[name=btc_fee_total]").val(parseFloat(total_fee).toFixed(8));
    }
        
        // Enable pusher logging - don't include this in production
        Pusher.logToConsole = false;

        var pusher = new Pusher(window.pusherKey, {
            cluster: window.pusherCluster,
            encrypted: true
        });

        var tradingChannel = pusher.subscribe(window.pusherChannel);

        tradingChannel.bind('publish', function (publish) {

            var button;
            var table;
            var data;
            
            if(publish.type === 'sell'){
                button = '<a href="/user/trading/get/'+publish.coin+'/'+publish.id+'" class="btn btn-success btn-xs">Buy</a>';
                table = '#sellTable';
            }
            
            if(publish.type === 'buy'){
                button = '<a href="/user/trading/get/'+publish.coin+'/'+publish.id+'" class="btn btn-warning btn-xs">Sell</a>';
                table = '#buyTable';
            }
            
            data = [
                publish.id, 
                publish.btc_price, 
                publish.amount,
                publish.btc_total,
                button
            ];
            
            data.id = publish.id;
            data.class = 'animg';
            $(table).DataTable().row.add(data).draw().node();                


        });

        tradingChannel.bind('history', function (history) {
                data = [
                    history.time, 
                    history.type, 
                    history.amount,
                    history.btc_total
                ];
                
            data.class = 'animg';    
            data.class_type = history.type + '_order';    
            $("#historyTable").DataTable().row.add(data).draw().node();
        });
        
        tradingChannel.bind('unpublish', function (market) {
            
            if(market.type === 'sell'){
                $("#sellTable").DataTable().row("#"+market.id).remove().draw();
            }else if(market.type === 'buy'){
                $("#buyTable").DataTable().row("#"+market.id).remove().draw();
            }
        });
    
});

$(function () {
    $("input[name='amount']").bind('change keydown keyup',function (event){
        
        if (event.shiftKey == true) {
            event.preventDefault();
        }

        if ((event.keyCode >= 48 && event.keyCode <= 57) || 
            (event.keyCode >= 96 && event.keyCode <= 105) || 
            event.keyCode == 8 || event.keyCode == 9 || event.keyCode == 37 ||
            event.keyCode == 39 || event.keyCode == 46 || event.keyCode == 190 || event.keyCode == 110) {

        } else {
            event.preventDefault();
        }

        if($(this).val().indexOf('.') !== -1 && event.keyCode == 190){
            event.preventDefault(); 
        }
        //if a decimal has been added, disable the "."-button
    })
    
    $("input[name='amount']").blur(function() {
         $(this).val(parseFloat($(this).val()).toFixed(8));
    });
})