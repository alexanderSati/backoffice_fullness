/**
 * Created by Igniweb038 on 15/02/17.
 */
$('#datatable-responsive').dataTable({
  "iDisplayLength": 25,
  "order": [[0, "desc"]]
});

$('.btn_open_modal_order').on('click', function () {
  console.log(this);
  $.get(
    $('#site_url').val() + "orders/get_items_order",
    {
      shopping_order_id: $(this).attr("data-shopping_order_id")
    },
    function (response) {
      $("#order_products").html("");

      $("#shipping_type").html(response.items.tipo);
      $("#shipping_name").html(response.items.nombre);


      response.productos.forEach(element => {
        $("#order_products").append(`
          <tr>
            <td align="center">
              <img src="`+ $('#site_url').val() + `uploads/product/${element.product_image}" style="width: 50px;" />
            </td>
            <td>${element.product_name}</td>
            <td>${element.quantity}</td>
            <td align="right">$${element.value}</td>
            <td align="right">$${element.total_product}</td>
          </tr>
        `);
      });

      if (response.length > 0) {
        $("#order_products").append(`
          <tr>
            <td colspan="4" align="right">Total</td>
            <td align="right">$${response[0].total_value}</td>
          </tr>
        `);
      }

    },
    "json"
  );
});