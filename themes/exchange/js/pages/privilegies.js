/**
 * Created by Igniweb038 on 15/02/17.
 */
$('.datatable-responsive').dataTable({
    "bPaginate": false,
    "searching": false,
    "aoColumns": [
        { 'bSortable': false, 'aTargets': [ 1 ] }
    ]
});