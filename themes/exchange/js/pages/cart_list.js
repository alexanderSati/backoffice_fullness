function dibujar_tabla() {
  $.get(
    $("#site_url").val() + "/cart/get_list_items_cart",
    function (response) {
      let table_row = ``;

      if (response.success && response.items != undefined) {
        response.items.forEach(
          function (item) {

            if (item.options.code == "DESCUENTO_BILLETERA") {
              $("#btn_pay_wallet").prop("disabled", true);
            }
            table_row += `
          <tr>
            <td>${item.options.code}</td>
            <td>${item.name}</td>
            <td>${item.qty}</td>
            <td>${item.price}</td>
            <td>${item.subtotal}</td>
            <td>
              <button 
                type="button" 
                name="remove" 
                class="btn btn-danger btn-xs remove_inventory" 
                id="${item.rowid}"
                data-code="${item.options.code}">
                Quitar            
              </button>
            </td>
            
          </tr>
        
        `;
          }
        )

        table_row += `
          <tr>
            <td colspan="5" align="right" >
              Total
            </td>
            <td align="right" >
              $${response.total}
            </td>
          </tr>
        `;

        $('#items_purchase').html(table_row);

      }
      else {
        $('#items_purchase').html("");
      }

      console.log("Total:" + response.total)
      if (response.total <= 0) {
        $("#btn_comprar").prop("disabled", true);
      }
      else {
        $("#btn_comprar").prop("disabled", false);
      }

      if ($('#value_plan').val() != 0) {
        if (response.total < parseFloat($('#value_plan').val())) {
          $("#btn_comprar").prop("disabled", "true");
        }
      }


    },
    "json"
  );

}


function agregarPagoPorBilletera(element) {

  let product_id = 1;
  let product_name = "DESCUENTO_BILLETERA";
  let product_price = $("#wallet_discount").val();
  let quantity = 1;
  let code = "DESCUENTO_BILLETERA";
  $.ajax({
    url: $('#site_url').val() + "cart/add",
    method: "POST",
    data: {
      product_id: product_id,
      product_name: product_name,
      product_price: product_price,
      quantity: quantity,
      [$("#csrf_data").attr("name")]: $("#csrf_data").val(),
      code: code
    },
    dataType: "json",
    success: function (data) {
      if (data.success) {
        $('#mensaje_billetera').show(100).delay(4800).hide(100);
        //$('#cart_details').html(data.view);
        dibujar_tabla();
        $("#wallet_discount").val("");

        $("#btn_pay_wallet").prop("disabled", true);


        $("#csrf_data").attr("name", data.csrf.name);
        $("#csrf_data").val(data.csrf.hash);
      }
      else {
        $('#mensaje_billetera_error').show(100).delay(4800).hide(100);
      }


    }
  });
}




$(document).ready(function () {
  dibujar_tabla();




  $(document).on('click', '.remove_inventory', function () {

    if ($(this).attr("data-code") == "DESCUENTO_BILLETERA") {
      $("#btn_pay_wallet").prop("disabled", false);
    }



    var row_id = $(this).attr("id");
    if (confirm("Are you sure you want to remove this?")) {
      $.ajax({
        url: $('#site_url').val() + "cart/remove",
        method: "POST",
        data: {
          row_id: row_id,
          [$("#csrf_data").attr("name")]: $("#csrf_data").val()
        },
        dataType: "json",
        success: function (data) {
          alert("Product removed from cart");
          dibujar_tabla()
          $('#cart_details').html(data.view);

          $("#csrf_data").attr("name", data.csrf.name);
          $("#csrf_data").val(data.csrf.hash);
        }
      });
    } else {
      return false;
    }
  });

  $(document).on('click', '#clear_cart', function () {
    if (confirm("Are you sure you want to clear cart?")) {
      $.ajax({
        url: $('#site_url').val() + "cart/clear",
        data: {
          [$("#csrf_data").attr("name")]: $("#csrf_data").val()
        },
        dataType: "json",
        success: function (data) {
          alert("Your cart has been clear...");
          //$('#cart_details').html(data.view);
          dibujar_tabla();
          $("#csrf_data").attr("name", data.csrf.name);
          $("#csrf_data").val(data.csrf.hash);
        }
      });
    } else {
      return false;
    }
  });

  $("#oficina").select2({
    placeholder: 'Select an option',
    height: '1em',
    width: "98%"
  });

  $("#select_mayorista").select2({
    placeholder: 'Select an option',
    height: '1em',
    width: "98%"

  });

});


function open_tab(tab_id) {
  console.log('#' + tab_id);
  $('#' + tab_id).collapse('show');

  if (tab_id == "courier") {
    $('#ret_oficina').collapse('hide');
    $('#retiro_mayorista').collapse('hide');

    $("#oficina").select2("val", "");
    $("#select_mayorista").select2("val", "");

  }
  if (tab_id == "ret_oficina") {
    $('#retiro_mayorista').collapse('hide');
    $('#courier').collapse('hide');

    $("#direccion_courier").val("");
    $("#select_mayorista").select2("val", "");
  }
  if (tab_id == "retiro_mayorista") {
    $('#courier').collapse('hide');
    $('#ret_oficina').collapse('hide');

    $("#direccion_courier").val("");
    $("#oficina").select2("val", "");
  }

}







