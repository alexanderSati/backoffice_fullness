$(document).ready(function () {

  permitir_checkout();

  $('.add_cart').click(function () {
    var product_id = $(this).data("productid");
    var product_name = $(this).data("productname");
    var product_price = $(this).data("price");
    var product_code = $(this).data("code");
    var quantity = $('#' + product_id).val();
    if (quantity != '' && quantity > 0) {
      $.ajax({
        url: $('#site_url').val() + "cart/add",
        method: "POST",
        data: {
          product_id: product_id,
          product_name: product_name,
          product_price: product_price,
          quantity: quantity,
          code: product_code,
          [$("#csrf_data").attr("name")]: $("#csrf_data").val()
        },
        dataType: "json",
        success: function (data) {
          //alert("Product Added into Cart");
          $(".my-float").addClass("bounce animated infinite");
          setTimeout(() => { $(".my-float").removeClass("bounce animated infinite"); }, 4000)
          $('#cart_details').html(data.view);
          $('#' + product_id).val('');
          $("#csrf_data").attr("name", data.csrf.name);
          $("#csrf_data").val(data.csrf.hash);
          $("#parcial_price").html(data.total_cart);
          permitir_checkout();

        }
      });
    } else {
      alert("Please Enter quantity");
    }
  });


  $(document).on('click', '.remove_inventory', function () {
    var row_id = $(this).attr("id");
    if (confirm("Are you sure you want to remove this?")) {
      $.ajax({
        url: $('#site_url').val() + "cart/remove",
        method: "POST",
        data: {
          row_id: row_id,
          [$("#csrf_data").attr("name")]: $("#csrf_data").val()
        },
        dataType: "json",
        success: function (data) {
          alert("Product removed from cart");
          $('#cart_details').html(data.view);

          $("#csrf_data").attr("name", data.csrf.name);
          $("#csrf_data").val(data.csrf.hash);
          $("#parcial_price").html(data.total_cart);
          permitir_checkout();
        }
      });
    } else {
      return false;
    }
  });

  $(document).on('click', '#clear_cart', function () {
    if (confirm("Are you sure you want to clear cart?")) {
      $.ajax({
        url: $('#site_url').val() + "cart/clear",
        data: {
          [$("#csrf_data").attr("name")]: $("#csrf_data").val()
        },
        dataType: "json",
        success: function (data) {
          alert("Your cart has been clear...");
          $('#cart_details').html(data.view);

          $("#csrf_data").attr("name", data.csrf.name);
          $("#csrf_data").val(data.csrf.hash);
          $("#parcial_price").html(data.total_cart);
        }
      });
    } else {
      return false;
    }
  });

});


function permitir_checkout() {
  console.log("permitir_checkout()");
  let precioParcial = $("#parcial_price").html();
  let precioTotal = $("#total_price").html();

  let total_cart = parseFloat($("#total_cart").html());
  let link = "";

  // Si se esta haciendo la compra de un plan
  if (precioParcial != undefined) {
    precioParcial = parseFloat(precioParcial);
    precioTotal = parseFloat(precioTotal);


    // Si ya se alcanzó el monto del plan
    if (precioParcial >= precioTotal) {

      link = $("#btn_go_checkout a").attr("data-link");
      $("#btn_go_checkout a").attr("href", link);

    }
    // si no se alcanzo el monto del plan
    else {
      link = $("#btn_go_checkout a").attr("href");
      $("#btn_go_checkout a").attr("href", "#");

      if (link != "#")
        $("#btn_go_checkout a").attr("data-link", link);

    }
  }

}

$("#category").select2({
  placeholder: 'Select an option',
  height: '1em'
});







