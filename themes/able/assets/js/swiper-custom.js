"use strict";
$(document).ready(function() {
    setTimeout(function(){
var swiper = new Swiper('.swiper-container', {
        pagination: '.swiper-pagination',
        slidesPerView: 6,
        paginationClickable: true,
        spaceBetween: 20,
        loop: true,
        breakpoints: {
                // when window width is <= 576px
                576: {
                        slidesPerView: 3
                },
                // when window width is <= 992px
                992: {
                        slidesPerView: 4
                },
                // when window width is <= 1200px
                1200: {
                        slidesPerView: 3
                }
        }
    });
},350);
});