<?php defined('BASEPATH') OR exit('No direct script access allowed');
/**
 * Admin Template
 */
?>
<!DOCTYPE html>
<html>

<head>
  <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <meta name="viewport" content="width=device-width, initial-scale=1.0, user-scalable=0, minimal-ui">
  <!-- <link rel="shortcut icon" type="image/x-icon" href="/themes/<?= $this->settings->theme ?>/images/icon.png?v=<?php echo $this->settings->site_version; ?>">
    <link rel="icon" type="image/x-icon" href="/themes/<?= $this->settings->theme ?>/images/icon.png?v=<?php echo $this->settings->site_version; ?>"> -->
  <title><?php echo $page_title; ?> - <?php echo $this->settings->site_name; ?></title>

  <?php // CSS files ?>
  <?php if (isset($css_files) && is_array($css_files)) : ?>
  <?php foreach ($css_files as $css) : ?>
  <?php if (!is_null($css)) : ?>
  <link rel="stylesheet" href="<?php echo $css; ?>?v=<?php echo $this->settings->site_version; ?>"><?php echo "\n"; ?>
  <?php endif; ?>
  <?php endforeach; ?>
  <?php endif; ?>

  <script src="/themes/<?= $this->settings->theme ?>/js/jquery.min.js"></script>

  <!--[if lt IE 9]>
    <script src="../assets/js/ie8-responsive-file-warning.js"></script>
    <![endif]-->

  <!-- HTML5 shim and Respond.js IE8 support of HTML5 elements and media queries -->
  <!--[if lt IE 9]>
    <script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
    <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
    <![endif]-->
  <script async src="https://www.googletagmanager.com/gtag/js?id=UA-143446119-1"></script>
  <script>
  window.dataLayer = window.dataLayer || [];

  function gtag() {
    dataLayer.push(arguments);
  }
  gtag('js', new Date());

  gtag('config', 'UA-143446119-1');
  </script>
</head>

<body themebg-pattern="theme4"
  style="background:#ECF0F5;">

  <input id="site_url" value="<?= base_url() ?>" type="hidden" />
  <div class="pcode-content">
      <div class="page-header" style="background-image: linear-gradient(#004DAA, #a7ec51); height: 230px;">
        <div class="page-block">
            <div class="row align-items-center">
                <div class="col-md-4">
                    <div class="text-center">                         
                          <img src="/themes/able/images/logo-(1-).png" alt="log-fullness" srcset="">                      
                    </div>
                </div>
                <div class="col-md-4">
                  <h3 class="text-center text-white">
                    <?=lang('users title external_store')?>
                  </h3>
                </div>
                <div class="col-md-4">
                  <div class="text-center">
                    <span class="dropdown-primary dropdown drop-lang">
                        <label class="text-white"><?php echo lang('users title change_language')?></label>
                          <button id="session-language" type="button" data-toggle="dropdown" aria-haspopup="true"
                                aria-expanded="false" class="btn waves-effect waves-dark btn-outline-default btn-icon">
                                <i class="fa fa-language"></i>
                            </button>
                            <ul id="session-language-dropdown" class="dropdown-menu p-3" role="menu"
                                aria-labelledby="session-language">
                            <?php foreach ($this->languages as $key=>$name) : ?>
                            <li>
                            <a href="#" rel="<?php echo $key; ?>">
                              <?php if ($key == $this->session->language) : ?>
                                <i class="fa fa-check selected-session-language"></i>
                                    <?php endif; ?>
                                    <?php echo $name; ?>
                                  </a>
                                </li>
                            <?php endforeach; ?>
                          </ul>
                      </span>
                  </div>
                </div>
            </div>
        </div>
    </div>
    <div class="pcode-inner-content">
      <div class="main-body">
        <div class="page-wrapper">
          <div class="page-body" style="margin-top: -10%;" >
            <?php // Main content ?>
            <?php echo $content; ?>
          </div>
        </div>
      </div>
    </div>
  </div>

  <?php // Javascript files ?>
  <?php if (isset($js_files) && is_array($js_files)) : ?>
  <?php foreach ($js_files as $js) : ?>
  <?php if (!is_null($js)) : ?>
  <?php echo "\n"; ?>
  <script type="text/javascript" src="<?php echo $js; ?>?v=<?php echo $this->settings->site_version; ?>"></script>
  <?php echo "\n"; ?>
  <?php endif; ?>
  <?php endforeach; ?>
  <?php endif; ?>
  <?php if (isset($js_files_i18n) && is_array($js_files_i18n)) : ?>
  <?php foreach ($js_files_i18n as $js) : ?>
  <?php if (!is_null($js)) : ?>
  <?php echo "\n"; ?>
  <script type="text/javascript">
  <?php echo "\n" . $js . "\n"; ?>
  </script><?php echo "\n"; ?>
  <?php endif; ?>
  <?php endforeach; ?>
  <?php endif; ?>

</body>

</html>