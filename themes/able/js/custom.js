/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/* Sidebar Menu active class */
$(function () {
    var url = window.location;
    $('#nav-left-menu a[href="' + url + '"]').parent('li').addClass('active');
    $('#nav-left-menu a').filter(function () {
        return this.href == url;
    }).parent('li').addClass('active').parent('ul').slideDown().parent().addClass('active pcoded-trigger');
});
