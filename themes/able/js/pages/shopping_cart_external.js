$(document).ready(function () {

  
    $('.add_cart').click(function () {
      var product_id = $(this).data("productid");
      var product_name = $(this).data("productname");
      var product_price = $(this).data("price");
      var product_code = $(this).data("code");
      var shipping_details_id = $(this).data("shipping_details_id");
      var quantity = $('#' + product_id).val();
      if (quantity != '' && quantity > 0) {
        $.ajax({
          url: $('#site_url').val() + "cart_external/add",
          method: "POST",
          data: {
            product_id: product_id,
            product_name: product_name,
            product_price: product_price,
            quantity: quantity,
            code: product_code,
            shipping_details_id: shipping_details_id,
            [$("#csrf_data").attr("name")]: $("#csrf_data").val()
          },
          dataType: "json",
          success: function (data) {
            if( data.success ){
            //alert("Product Added into Cart");
            $(".btn_open_modal").delay(1).addClass("animated infinite  swing faster")
            setTimeout(function () { $(".btn_open_modal").delay(1).removeClass("animated swing faster") }, 3500);
            $('#cart_details').html(data.view);
            $('#' + product_id).val('');
            $("#csrf_data").attr("name", data.csrf.name);
            $("#csrf_data").val(data.csrf.hash);
            $("#parcial_price").html(parseFloat(data.total_cart).toFixed(2));
            $('#total_cart_external').val(data.total_cart);
            notify($("#message_add").val(), 'inverse');      
            }
            else{
              
              $('#' + product_id).val(data.stock);
              notify(data.message, 'inverse');
            }
          }
        });
      } else {
        swal({
          title: $('#zero_quantity').val(),
          confirmButtonText: $('#confirm_button').val(),
        });
      }
    });
  
  
    $(document).on('click', '.remove_inventory', function () {
      var row_id = $(this).attr("id");
      swal({
        title: $('#confirm_remove_inventory').val(),
        type: "warning",
        showCancelButton: true,
        confirmButtonClass: "btn-danger",
        confirmButtonText: $('#confirm_button').val(),
        cancelButtonText: $('#cancel_button').val(),
        closeOnConfirm: false
      },
        function () {
          $.ajax({
            url: $('#site_url').val() + "cart_external/remove",
            method: "POST",
            data: {
              row_id: row_id,
              [$("#csrf_data").attr("name")]: $("#csrf_data").val()
            },
            dataType: "json",
            success: function (data) {
              // alert("Product removed from cart");
              $('#cart_details').html(data.view);
  
              $("#csrf_data").attr("name", data.csrf.name);
              $("#csrf_data").val(data.csrf.hash);
              $("#parcial_price").html(parseFloat(data.total_cart).toFixed(2));
              $('#total_cart_external').val(data.total_cart);
            }
          });
          swal($('#correct_remove_inventory').val(), "", "success");
        });
      // } else {
      //   return false;
      // }
    });
  
    $(document).on('click', '#clear_cart', function () {
  
      swal({
        title: $('#confirm_clear_cart').val(),
        type: "warning",
        showCancelButton: true,
        confirmButtonClass: "btn-danger",
        confirmButtonText: $('#confirm_button').val(),
        cancelButtonText: $('#cancel_button').val(),
        closeOnConfirm: false
      },
        function () {
          $.ajax({
            url: $('#site_url').val() + "cart_external/clear",
            data: {
              [$("#csrf_data").attr("name")]: $("#csrf_data").val()
            },
            dataType: "json",
            success: function (data) {
              // alert("Your cart has been clear...");
              $('#cart_details').html(data.view);
  
              $("#csrf_data").attr("name", data.csrf.name);
              $("#csrf_data").val(data.csrf.hash);
              $("#parcial_price").html(parseFloat(data.total_cart).toFixed(2));
              $('#total_cart_external').val(data.total_cart);

            }
          });
          swal($('#correct_clear').val(), "", "success");
        });
  
      // if (confirm("Are you sure you want to clear cart?")) {
  
      // } else {
      //   return false;
      // }
    });
  
  });
  
  
  $("#category").select2({
    placeholder: 'Select an option',
    height: '1em'
  });
  
  function update_qty(value, element) {
    console.log("actualizar cantidad", value);
    let cantidad = parseInt(value);
    let rowid = $(element).attr("data-rowid");
    console.log(cantidad);
    if (!isNaN(cantidad) && cantidad > 0) {
  
      $.post(
        $("#site_url").val() + "cart_external/update_qty",
        {
          rowid: rowid,
          qty: cantidad
        },
        function (data) {
          if(data.success){
          $('#cart_details').html(data.view);
          $("#csrf_data").attr("name", data.csrf.name);
          $("#csrf_data").val(data.csrf.hash);
          $("#parcial_price").html(parseFloat(data.total_cart).toFixed(2));
          $('#total_cart_external').val(data.total_cart);
        }else {
          $(element).val(data.stock);
          notify(data.message, 'inverse');
        }
        },
        "json"
      );
    }
  
  }
  
  /**
   * Une el select de la ventana modal, con el 
   * select del formulario de filtrado de productos
   */
  function seleccionar_envio() {
    console.log("seleccionar_envio\n", $("#items_ubication").val());
    let envio = $("#items_ubication").val();
    $("#shopping_details").val(envio).trigger("change");
  }
  
  let UrlParams = new URLSearchParams(window.location.search);
  
  if (
    UrlParams.get("shopping_details_id") == null
  ) {
    $('#modal_recogida').modal('show');
  }
  
  function change_shipping() {    
    $.get(
      $('#site_url').val() + "cart_external/clear",
      function () {
  
        window.location.href = $('#site_url').val() + "cart_external/referrals/" + $('#referral-user').val();
      }
    );
  
  }
  
  function notify(message, type) {
    $.growl({
      message: message
    }, {
        type: type,
        allow_dismiss: false,
        label: 'Cancel',
        className: 'btn-xs btn-inverse',
        placement: {
          from: 'top',
          align: 'right'
        },
        delay: 2500,
        animate: {
          enter: 'animated fadeInRight',
          exit: 'animated fadeOutRight'
        },
        offset: {
          x: 30,
          y: 30
        }
      });
  };

  function send_purchase(){

    if($('#total_cart_external').val() > 0) {
      window.location.href = $('#site_url').val() + "register/external_user/" + $('#referral-user').val();
    } else {
      swal({
          title: $('#total_cart_zero').val(),
          confirmButtonText: $('#confirm_button').val(),
        });
    }
  }
  
  
  
  
  