function dibujar_tabla() {
    console.log("dibujar_tabla()");
    $.get(
      $("#site_url").val() + "/cart_external/get_list_items_cart",
      function (response) {  
        console.log(response);
            
        
        let table_row = ``;
        let total = 0;
        if (response.success && response.items != undefined) {

          comprobar_stock_productos( response.items );

          response.items.forEach(
            function (item) {
              if (item.subtotal >= 0) {
                total += item.subtotal
              }
  
              if (item.options.code == "DESCUENTO_BILLETERA") {
                $("#btn_pay_wallet").prop("disabled", true);
              }
              table_row += `
              <tr>
                <td>${item.options.code}</td>
                <td>${item.name}</td>
              `;
           
              console.log("ID del item:", item);
              if (item.id != 1) {
  
                table_row += `
                <td>
                  <input type="number" class="form-control" min="1" value="${item.qty}" onchange="update_qty(this.value, this)" data-rowid="${item.rowid}" />
                </td>
                `;
              }
              else {
                table_row += '<td>' + item.qty + '</td>';
              }
              table_row += `
              <td>${parseFloat(item.price).toFixed(2)}</td>
              <td>${parseFloat(item.subtotal).toFixed(2)}</td>
              <td>
                <button 
                  type="button" 
                  name="remove" 
                  class="btn remove_inventory" 
                  id="${item.rowid}"
                  data-code="${item.options.code}">
                  <i class="feather icon-trash-2 f-w-600 f-16 text-c-red"></i>           
                </button>
              </td>
              
            </tr>
          
          `;
            }
          )
  
          table_row += `
            <tr>
              <td colspan="7" align="right" >
                Total
              </td>
              <td align="right" >
                S/${response.total}
              </td>
            </tr>
          `;
  
          $('#items_purchase').html(table_row);
  
        }
        else {
          $('#items_purchase').html("");
        }
  
        console.log("Total:" + total)
        if (total <= 0) {
          $("#btn_comprar").prop("disabled", true);
        }
        else {
          $("#btn_comprar").prop("disabled", false);
        }
  
        if ($('#value_plan').val() != 0) {
          if (total < parseFloat($('#value_plan').val())) {
            $("#btn_comprar").prop("disabled", "true");
          }
        }
  
  
      },
      "json"
    );
  
  }
  
  
  function agregarPagoPorBilletera(element) {
  
    let product_id = 1;
    let product_name = "DESCUENTO_BILLETERA";
    let product_price = $("#wallet_discount").val();
    let quantity = 1;
    let code = "DESCUENTO_BILLETERA";
    $.ajax({
      url: $('#site_url').val() + "cart_external/add",
      method: "POST",
      data: {
        product_id: product_id,
        product_name: product_name,
        product_price: product_price,
        quantity: quantity,
        [$("#csrf_data").attr("name")]: $("#csrf_data").val(),
        code: code
      },
      dataType: "json",
      success: function (data) {
        if (data.success) {
          $('#mensaje_billetera').show(100).delay(4800).hide(100);
          //$('#cart_details').html(data.view);
          dibujar_tabla();
          $("#wallet_discount").val("");
  
          $("#btn_pay_wallet").prop("disabled", true);
  
  
          $("#csrf_data").attr("name", data.csrf.name);
          $("#csrf_data").val(data.csrf.hash);
        }
        else {
          $('#mensaje_billetera_error').show(100).delay(4800).hide(100);
        }
  
  
      }
    });
  }
  
  
  
  
  $(document).ready(function () {
    dibujar_tabla();
  
    var icons = {
      header: "zmdi zmdi-chevron-down",
      activeHeader: "zmdi zmdi-chevron-up"
    };
    $("#color-accordion").accordion({
      heightStyle: "content",
      icons: icons
    });
  
  
  
  
    $(document).on('click', '.remove_inventory', function () {
  
      if ($(this).attr("data-code") == "DESCUENTO_BILLETERA") {
        $("#btn_pay_wallet").prop("disabled", false);
      }
  
  
  
      var row_id = $(this).attr("id");
      swal({
        title: $('#confirm_remove_inventory').val(),
        type: "warning",
        showCancelButton: true,
        confirmButtonClass: "btn-danger",
        confirmButtonText: $('#confirm_button').val(),
        cancelButtonText: $('#cancel_button').val(),
        closeOnConfirm: false
      },
        function () {
  
          $.ajax({
            url: $('#site_url').val() + "cart_external/remove",
            method: "POST",
            data: {
              row_id: row_id,
              [$("#csrf_data").attr("name")]: $("#csrf_data").val()
            },
            dataType: "json",
            success: function (data) {
              dibujar_tabla()
              //$('#cart_details').html(data.view);
  
              $("#csrf_data").attr("name", data.csrf.name);
              $("#csrf_data").val(data.csrf.hash);
            }
          });
          swal($('#correct_remove_inventory').val(), "", "success");
        });
      // } else {
      //   return false;
      // }
    });
  
    $(document).on('click', '#clear_cart', function () {
      swal({
        title: $('#confirm_clear_cart').val(),
        type: "warning",
        showCancelButton: true,
        confirmButtonClass: "btn-danger",
        confirmButtonText: $('#confirm_button').val(),
        cancelButtonText: $('#cancel_button').val(),
        closeOnConfirm: false
      },
        function () {
          $.ajax({
            url: $('#site_url').val() + "cart_external/clear",
            data: {
              [$("#csrf_data").attr("name")]: $("#csrf_data").val()
            },
            dataType: "json",
            success: function (data) {
  
              //$('#cart_details').html(data.view);
              dibujar_tabla();
              $("#csrf_data").attr("name", data.csrf.name);
              $("#csrf_data").val(data.csrf.hash);
            }
          });
          swal($('#correct_clear').val(), "", "success");
        });
      // } else {
      //   return false;
      // }
    });
  
    $("#oficina").select2({
      placeholder: 'Select an option',
      height: '1em',
      width: "98%"
    });
  
    $("#select_mayorista").select2({
      placeholder: 'Select an option',
      height: '1em',
      width: "98%"
  
    });
  
  });
  
  
  function open_tab(tab_id) {
    console.log('#' + tab_id);
  
    if (tab_id == "courier" && !$("#check_courier").prop("disabled")) {
      $('#ret_oficina').collapse('hide');
      $('#retiro_mayorista').collapse('hide');
  
      $("#oficina").select2("val", "");
      $("#select_mayorista").select2("val", "");
      $('#' + tab_id).collapse('show');
  
    }
    if (tab_id == "ret_oficina" && !$("#check_oficina").prop("disabled")) {
      $('#retiro_mayorista').collapse('hide');
      $('#courier').collapse('hide');
  
      $("#direccion_courier").val("");
      $("#select_mayorista").select2("val", "");
      $('#' + tab_id).collapse('show');
    }
    if (tab_id == "retiro_mayorista" && !$("#check_mayorista").prop("disabled")) {
      $('#courier').collapse('hide');
      $('#ret_oficina').collapse('hide');
  
      $("#direccion_courier").val("");
      $("#oficina").select2("val", "");
      $('#' + tab_id).collapse('show');
    }
  
  }
  
  function enviar_formulario() {
    console.log("enviar_formulario()");
    let envio_courier = $('[name="envio[direccion]"]').val();
    let envio_oficina = $('[name="envio[oficina]"]').val();
    let envio_mayorista = $('[name="envio[mayorista]"]').val();
  
    if (envio_courier == 0 && envio_oficina == 0 && envio_mayorista == 0) {
      $("#modal-default").modal('hide');
      $("#no_shipping_modal").modal('show');
    } else {
      $("select").removeAttr("disabled");
      $("#envio_formulario").submit();
      $('#btn_comprar').prop('disabled', true);
    }
  
  }
  
  function update_qty(value, element) {
    console.log("actualizar cantidad", value);
    let cantidad = parseInt(value);
    let rowid = $(element).attr("data-rowid");
    console.log(cantidad);
    if (!isNaN(cantidad) && cantidad > 0) {
  
      $.post(
        $("#site_url").val() + "/cart_external/update_qty",
        {
          rowid: rowid,
          qty: cantidad
        },
        function (data) {
          dibujar_tabla();
        },
        "json"
      );
    }
  }
  
  function establecer_envio(envio_id) {
    console.log("establecer_envio(" + envio_id + ");");
    console.log("mayorista: " + $(`#select_mayorista option[value='${envio_id}']`).length)
    console.log("oficina: " + $(`#oficina option[value='${envio_id}']`).length)
  
    if ($(`#select_mayorista option[value='${envio_id}']`).length > 0) {
  
      setTimeout(function () {
  
        $("#envio_formulario").hide();
        $("#check_oficina").prop("disabled", true);
        $("#check_courier").prop("disabled", true);
        $("#check_mayorista").click();
        $('#retiro_mayorista').collapse('show');
        $("#select_mayorista").val(envio_id).trigger("change");
        $("#select_mayorista").select2("enable", false)
  
      }, 1500);
  
    }
    else if ($(`#oficina option[value='${envio_id}']`).length > 0) {
  
      setTimeout(function () {
  
        $("#envio_formulario").hide();
        $("#check_mayorista").prop("disabled", true);
        $("#check_courier").prop("disabled", true);
        $("#check_oficina").click();
        $('#ret_oficina').collapse('show');
        $("#oficina").val(envio_id).trigger("change");
        $("#oficina").select2("enable", false)
      }, 1500);
  
    }
    else {
      $("#check_mayorista").prop("disabled", true);
      $("#check_oficina").prop("disabled", true);
    }
  }
  let envio_id = $("#tipo_envio").val();
  establecer_envio(envio_id);

  function comprobar_stock_productos( productos ){
    console.log("comprobar_stock_productos();");
  
    let stock = true;
    let message = $("#message_no_product").val();
    for(  let i in productos){
      if( !productos[i].is_available ){
        stock = false;
        message = message.replace('{1}',productos[i].stock);
        message = message.replace('{2}',productos[i].name);
  
      }
    }
  
    if( !stock ){
      $("#btn-open-buy").prop("disabled",false);
      $("#no_stock_alert").show();
      $("#no_stock_alert_message").html(message);
    }
    else{
      $("#btn-open-buy").prop("disabled",false);
      $("#no_stock_alert").hide();
      $("#no_stock_alert_message").html(message);
    }
  
  }
  
  
  
  
  
  
  
  
  