/**
 * Created by Igniweb038 on 08/02/17.
 */

$(document).ready(function () {
    load_qr();
    
    
    $('#coin').on('change', function () {
       load_qr();
    });
});


function load_qr() {
    var coin = $('#coin').val();
       
    $.ajax({
         type: "get",
         url: 'ajax/load_qr/'+coin,
         dataType: 'json',
         success: function (data) {
             $('.display_qr').html(data.img_html);
         }
     });
}