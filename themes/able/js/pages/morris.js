/**
 * Created by Igniweb038 on 13/02/17.
 */
// get data from view
console.log(chart);

new Morris.Area({
    element: 'graph_line',
    xkey: 'date',
    ykeys: ['value'],
    xLabels:'days',
    labels: ['Value'],
    yLabelFormat: function (y) {
        return '$' + y.toString();
    },
    lineColors: ['#2b1642', '#000000', '#000000', '#000000'],
    data: chart
});