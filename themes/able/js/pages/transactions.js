/**
 * Created by Igniweb038 on 15/02/17.
 */

$('#datatable-responsive').dataTable({
    "iDisplayLength": 25,
    "order": [[0, "desc"]]
});

$('#admin-all').dataTable({
    "iDisplayLength": 25,
    "order": [[1, "desc"]]
});

$('#admin-transactions').dataTable({
    "order": [[1, "desc"]]
});

$(document).ready(function () {
    $('#search-transfer').click(function () {
        var name = $('#transfer-to').val();
        $.ajax({
            type: "get",
            url: 'ajax/saerch-user/' + name,
            success: function (obj) {
                var response = JSON.parse(obj);
                if (response.message == "success") {
                    console.log(response.data);
                    
                    $('.notifications-transfer').html("")
                    $('.continue-transfer').removeClass("hide").slideDown();
                    $('#dni-user-to-transfer').val(response.data.dni);
                    $('#name-user-to-transfer').val(response.data.first_name+" "+response.data.last_name);
                    $('#id_user').val(response.data.id);
                    $('#transfer-to').attr("readonly", "readonly");
                    $('#amount').focus();
                } else {
                    $('.continue-transfer').slideUp();
                    var notification = '\
                        <div class="alert alert-danger alert-dismissable">\
                            <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>\
                            ' + response.data + '\
                        </div>';
                    $('.notifications-transfer').html(notification);
                }
            }
        });
    });

    $(".request-token").click(function () {
        $.ajax({
            type: "get",
            url: 'ajax/request-token/',
            success: function (data) {
                var notification = '\
                        <div class="alert alert-info alert-dismissable">\
                            <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>\
                            <strong>' + data + '\
                            </strong>\
                        </div>';
                $('.notifications-transfer').html(notification);
                createAutoClosingAlert(".alert-success", 600000);
            }
        });
    });

    $('#clear-transfer').click(function () {        
        $('.continue-transfer').slideUp();
        $('#transfer-to').val("").removeAttr("readonly").focus();
        $('.notifications-transfer').html("");
        $('#dni-user-to-transf').val("");
        $('#name-user-to-transf').val("");
        $('#amount-transfer').val("");
        $('#security-pin').val("");
    });

    $('#amount').on('keyup blur', function () {
        var amount = $(this).val();
        if (typeof $('#type').val() !== "undefined") {
            var type = $('#type').val();
        } else {
            var type = $('#wallet').val();
        }

        $('.value-currency').text(amount);
        $.ajax({
            type: "get",
            url: 'ajax/convert-price/' + amount + '/' + type,
            success: function (data) {
                $("#amount_final").val(data);
                $("#amount_usd").val(data);
            }
        });
    });

    $('#wallet').on('change', function () {
        update_curreny();
        fee_calc();
    });

    $('#amount-transfer').on('keyup blur', function () {
        fee_calc();
    });

    $('#amount_coin_deposit').on('keyup blur', function () {
        var amount = $(this).val();

        var type = '';
        if (typeof $('#type').val() !== "undefined") {
            type = $('#type').val();
        }

        var type_final = '';
        if (typeof $('#type_final').val() !== "undefined") {
            type_final = $('#type_final').val();
        }

        $.ajax({
            type: "get",
            url: 'ajax/convert-price-coin/' + amount + '/' + type + '/' + type_final,
            success: function (data) {
                $("#amount_coin_final").val(data);
            }
        });
    });

    $('#amount-deposit').on('keyup blur', function () {
        var amount = $(this).val();

        $.ajax({
            type: "get",
            url: 'ajax/convert-price-btc/' + amount,
            success: function (data) {
                $("#amount_final").val(data);
            }
        });
    });
    $('#type').change(function () {
        var amount = $('#amount').val();
        var type = $(this).val();
        $('.code-currency').text(type);
        $('.value-currency').text(amount);
        $.ajax({
            type: "get",
            url: 'ajax/convert-price/' + amount + '/' + type,
            success: function (data) {
                $("#amount_final").val(data);
                $("#amount_usd").val(data);
            }
        });
    });

    $('tbody').on('change', '.transaction-status', function () {
        var status = $(this).val();
        var id = $(this).attr('rowid');
        console.log(status + " " + id);
        $.ajax({
            type: "get",
            url: 'transactions/ajax/change-status/' + id + '/' + status,
            success: function () {
                window.location.replace("transactions");
            }
        });
    });

    $('#wallet').change(function () {
        $('#label-qr').text($(this).val());
        var type = $(this).val();
        var amount = $('#amount').val();
        $.ajax({
            type: "get",
            url: 'ajax/convert-price/' + amount + '/' + type,
            success: function (data) {
                $("#amount_final").val(data);
                $("#amount_usd").val(data);
            }
        });
    });

    $('.type-coin').change(function () {
        var amount = $('#amount_coin_deposit').val();
        var type = '';
        if (typeof $('#type').val() !== "undefined") {
            type = $('#type').val();
        }

        var type_final = '';
        if (typeof $('#type_final').val() !== "undefined") {
            type_final = $('#type_final').val();
        }

        $.ajax({
            type: "get",
            url: 'ajax/convert-price-coin/' + amount + '/' + type + '/' + type_final,
            success: function (data) {
                $("#amount_coin_final").val(data);
            }
        });
    });

    $("#modal_qr").on("hidden.bs.modal", function () {
        $("#modal_qr").removeData('bs.modal');
    });
});


// Monthly payments QR modal
function savePayment() {
    var hash = $('#hash');
    
    if(hash.val() !== ''){
        saveLink = btcLink.concat(hash.val());
        window.location.href = saveLink;
    }else{
        alert(hashLang);
    }
}

function createAutoClosingAlert(selector, delay) {
    var alert = $(selector).alert();
    window.setTimeout(function () {
        alert.slideUp(500, function () {
            alert.hide();
        });
    }, delay);
}

/* Transfer money */
function update_curreny() {
    var wallet = $('#wallet') || '';
    if (wallet && wallet.val() != '') {
        switch (wallet.val()) {
            case 'BTC':
                $('[name="amount"]').attr('type', 'number');
                $('[for="amount"] sub').html('(USD)');
                break;

            default:
                $('[name="amount"]').attr('type', 'text');
                $('[for="amount"] sub').html('(' + wallet.val() + ')');
                break;
        }
    }
}

function fee_calc() {
    var amount = $('#amount-transfer').val();
    var type = $('#wallet').val();
    var fee;

    if (type !== 'BTC') {
        var fee = 0;
        if (parseFloat(amount) > 0) {
            $.ajax({
                type: "get",
                url: 'ajax/calc-fee/' + amount + '/' + type,
                success: function (data) {
                    var response = JSON.parse(data);
                    $('#amount_fee').val(response.fee + ' ' + type);
                }
            });
        } else {
            $('#amount_fee').val(fee + ' ' + type);
        }
        $("#fee-group").removeClass('hidden');
    } else {
        $("#fee-group").addClass('hidden');
        $('#amount_fee').val(0);
    }
}
