/**
 * Created by Igniweb038 on 13/02/17.
 */
$(document).ready(function() {
    $(".select2_single").select2({
        placeholder: 'seleccione una opción'
    });
    $(".select2_group").select2({});
    $(".select2_multiple").select2({
        maximumSelectionLength: 4,
        placeholder: "With Max Selection limit 4",
        allowClear: true
    });
});