/**
 * Created by Igniweb038 on 15/02/17.
 */
$('#datatable-responsive').dataTable({
  "iDisplayLength": 25,
  "order": [[0, "desc"]]
});

$('.btn_open_modal_order').on('click', function () {
  console.log(this);
  $.get(
    $('#site_url').val() + "orders/get_items_order",
    {
      shopping_order_id: $(this).attr("data-shopping_order_id")
    },
    function (response) {
      $("#order_products").html("");
      $("#shipping_code").html(response.items.code);
      $("#shipping_type").html(response.items.tipo);
      $("#shipping_name").html(response.items.nombre);
      if( response.pago != null ){
        if(response.pago.method == "PAGO_OFICINA"){
          $("#payment_method").html($('[name="PAGO_OFICINA"]').val());
        }
        if(response.pago.method == "PAGO_MAYORISTA"){
          $("#payment_method").html($('[name="PAGO_MAYORISTA"]').val());
        }
        if(response.pago.method == "BILLETERA"){
          $("#payment_method").html($('[name="BILLETERA"]').val());
        }
        if(response.pago.method == "PAGO_ONLINE"){
          $("#payment_method").html($('[name="PAGO_ONLINE"]').val()+`
            <a target='_blank' href="`+response.pago.url+`"><b>`+response.pago.code+`</b></a>
          `);
        }


      }
      else{
        $("#payment_method").html("");
      }


      response.productos.forEach(element => {
        $("#order_products").append(`
          <tr>
            <td align="center">
              <img src="`+ $('#site_url').val() + `uploads/${element.product_image}" style="width: 50px;" />
            </td>
            <td>${element.product_name}</td>
            <td>${element.quantity}</td>
            <td>${element.residual_points}</td>
            <td>${element.binary_points}</td >
            <td align="right">S/. ${element.value}</td>
            <td align="right">S/. ${element.total_product}</td>
          </tr>
        `);
      });

      if (response.length > 0) {
        $("#order_products").append(`
          <tr>
            <td colspan="4" align="right">Total</td>
            <td align="right">S/. ${response[0].total_value}</td>
          </tr>
        `);
      }

    },
    "json"
  );
});



function aprobar(id, type) {
  console.log("aprobar orden:" + id);
  $("#btn_approve_" + id).prop("disabled", true);
  if (type === 0) {
    window.location.href = $("#site_url").val() + "orders/approve/" + id;    
  } else if(type == 1) {
    window.location.href = $("#site_url").val() + "orders/approve_external/" + id;        
  }
  else if( type==2 ){
    window.location.href = $("#site_url").val() + "orders/approve_wholesaler/" + id;
  }
}

function rechazar(id, type) {
  console.log("rechazar orden:" + id);
  $("#btn_deny_" + id).prop("disabled", true);
  if (type === 0) {
    window.location.href = $("#site_url").val() + "orders/denied/" + id;  
  } else if(type == 1) {
    window.location.href = $("#site_url").val() + "orders/denied_external/" + id;        
  }  

}
