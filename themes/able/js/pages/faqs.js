/**
 * Created by Igniweb038 on 11/02/17.
 */
$('.btn-delete-faq').click(function() {
    window.location.href = "/admin/faqs/delete/" + $(this).attr('data-id');
});

$(document).ready(()=> {
    $('#message-required').hide();
});

$('#tab-button').click(function(){
    if ($("#english_question").val() === null || $("#english_question").val() === '') {

        window.scrollTo(0,0);
        $('#message-required').show();

        $('#english').addClass('active');
        $('#english').addClass('show');
        $('#nav_english').addClass('active');

        $('#spanish').removeClass('active');
        $('#spanish').removeClass('show');
        $('#nav_spanish').removeClass('active');

        $('#portuguese').removeClass('active');
        $('#portuguese').removeClass('show');
        $('#nav_portuguese').removeClass('active');

    }
    else if($("#spanish_question").val() === null || $("#spanish_question").val() === ''){

        window.scrollTo(0,0);
        $('#message-required').show();

        $('#spanish').addClass('active');
        $('#spanish').addClass('show');
        $('#nav_spanish').addClass('active');
        
        
        $('#english').removeClass('active');
        $('#english').removeClass('show');
        $('#nav_english').removeClass('active');

        $('#portuguese').removeClass('active');
        $('#portuguese').removeClass('show');
        $('#nav_portuguese').removeClass('active');

    }
    else if($("#portuguese_question").val() === null || $("#portuguese_question").val() === ''){

        window.scrollTo(0,0);
        $('#message-required').show();

        $('#portuguese').addClass('active');
        $('#portuguese').addClass('show');
        $('#nav_portuguese').addClass('active');
        
        
        $('#english').removeClass('active');
        $('#english').removeClass('show');
        $('#nav_english').removeClass('active');

        $('#spanish').removeClass('active');
        $('#spanish').removeClass('show');
        $('#nav_spanish').removeClass('active');

    }
})