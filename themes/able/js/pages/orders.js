/**

 * Created by Igniweb038 on 15/02/17.

 */

$('#datatable-responsive').dataTable({

  "iDisplayLength": 25,

  "order": [[0, "desc"]]

});




function consultar_detalles(order_id){
  $("#opened_order_id").val( order_id );
  $.get(
    $('#site_url').val() + "orders/get_items_order",
    {
      shopping_order_id: order_id
    },
    function (response) {

      $("#order_products").html("");
      $("#shipping_code").html(response.items.code);
      $("#shipping_type").html(response.items.tipo);
      $("#shipping_name").html(response.items.nombre);
      $("#observacion_orden").val(response.observation);
      $("#user_approved").html(response.user_approve);
      if(response.delivered){

        $("#product_delivered").prop("checked", true);
      }
      else{
        $("#product_delivered").prop("checked", false);
      }


      if( response.pago != null ){

        if(response.pago.method == "PAGO_OFICINA"){
          $("#payment_method").html($('[name="PAGO_OFICINA"]').val());
        }

        if(response.pago.method == "PAGO_MAYORISTA"){
          $("#payment_method").html($('[name="PAGO_MAYORISTA"]').val());
        }

        if(response.pago.method == "BILLETERA"){

          $("#payment_method").html($('[name="BILLETERA"]').val());

        }

        if(response.pago.method == "PAGO_ONLINE"){
          $("#payment_method").html($('[name="PAGO_ONLINE"]').val()+`
            <a target='_blank' href="`+response.pago.url+`"><b>`+response.pago.code+`</b></a>
          `);
        }

        if(response.pago.method == "PAGO_VISANET"){

          $("#payment_method").html($('[name="PAGO_VISANET"]').val()+
          (response.pago.status=='approved'? "<span class='label label-success'>Pago aprobado</span>":"")+
          (response.pago.status=='canceled'? "<span class='label label-danger'>Pago denegado:"+response.pago.description+"</span>":""));

        }
      }
      else{
        $("#payment_method").html("");
      }

      response.productos.forEach(element => {

        $("#order_products").append(`
          <tr>
            <td align="center">
              <img src="`+ $('#site_url').val() + `uploads/${element.product_image}" style="width: 50px;" />
            </td>
            <td>${element.product_name}</td>
            <td>${element.quantity}</td>
            <td>${element.residual_points}</td>
            <td>${element.binary_points}</td >
            <td align="right">S/. ${element.value}</td>
            <td align="right">S/. ${element.total_product}</td>
          </tr>
        `);
      });

      if (response.length > 0) {
        $("#order_products").append(`
          <tr>
            <td colspan="4" align="right">Total</td>
            <td align="right">S/. ${response[0].total_value}</td>
          </tr>
        `);
      }
    },
    "json"
  );

}







function aprobar(id, type) {

  console.log("aprobar orden:" + id);
  $("#btn_approve_" + id).prop("disabled", true);

  if (type === 0) {
    window.location.href = $("#site_url").val() + "orders/approve/" + id;    
  } else if(type == 1) {
    window.location.href = $("#site_url").val() + "orders/approve_external/" + id;        
  }

  else if( type==2 ){
    window.location.href = $("#site_url").val() + "orders/approve_wholesaler/" + id;
  }

}



function rechazar(id, type) {

  console.log("rechazar orden:" + id);
  $("#btn_deny_" + id).prop("disabled", true);

  if (type === 0) {
    window.location.href = $("#site_url").val() + "orders/denied/" + id;  
  } else if(type == 1) {
    window.location.href = $("#site_url").val() + "orders/denied_external/" + id;        
  }  

}


function guardar_observacion(){
  let shopping_order_id = $("#opened_order_id").val();
  let observacion = $("#observacion_orden").val();

  $.post(
    $("#site_url").val()+"index.php/orders/set_observation",
    {
      shopping_order_id:shopping_order_id,
      observation: observacion
    },
    function( resp ){
      if(  resp.success ){
        $("#observation_saved").show(1).delay(10000).hide(1);
      }
    },
    'json'
  );
}


function establecer_envio(){
  let shopping_order_id = $("#opened_order_id").val();
  let delivered = $('#product_delivered').is(":checked");
  $.post(
    $("#site_url").val()+"index.php/orders/set_delivered",
    {
      shopping_order_id:shopping_order_id,
      delivered: delivered
    },
    function( resp ){

    }
  );
}

