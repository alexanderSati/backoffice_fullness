/**
 * Created by Igniweb038 on 15/02/17.
 */

$('.users-status').on('change', function () {
    var side = $(this).val();
    var id = $(this).attr('rowid');
    console.log(side + " " + id);
    $.ajax({
        type: "get",
        url: 'ajax/change-status/'+id+'/'+side,
        success: function (data) {
            window.location.replace("unassigned");
        }
    });
});