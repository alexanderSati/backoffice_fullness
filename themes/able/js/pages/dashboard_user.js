
$(document).ready(function () {
  var timerId = countdown(
    new Date($("#last_day_month").val()),
    function (ts) {

      $("#countdown_days").html(ts.days);
      $("#countdown_hours").html(ts.hours);
      $("#countdown_minutes").html(ts.minutes);
      $("#countdown_seconds").html(ts.seconds);

    },
    countdown.DAYS | countdown.HOURS | countdown.MINUTES | countdown.SECONDS
  );
});