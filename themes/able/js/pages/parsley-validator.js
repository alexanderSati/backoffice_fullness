/**
 * Created by Igniweb038 on 07/02/17.
 */
<!-- form validation -->

$(document).ready(function () {
    $.listen('parsley:field:validate', function () {
        validateFront();
    });
    $('form .btn.send').on('click', function () {
        $('form').parsley().validate();
        validateFront();
    });
    var validateFront = function () {
        if (true === $('form').parsley().isValid()) {
            $('.bs-callout-info').removeClass('hidden');
            $('.bs-callout-warning').addClass('hidden');
        } else {
            $('.bs-callout-info').addClass('hidden');
            $('.bs-callout-warning').removeClass('hidden');
        }
    };
});
try {
    hljs.initHighlightingOnLoad();
} catch (err) {
}
<!-- /form validation -->