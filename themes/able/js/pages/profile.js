/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
$(document).ready(function () {
    $(".request-token").click(function () {
        $(".loader-bg").css('display', '');
        window.scrollTo(0,0);
        $.ajax({
            type: "get",
            url: 'profile/ajax/request-token/',
            success: function (data) {
                var notification = '\
                        <div class="alert alert-info alert-dismissable">\
                            <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>\
                            <strong>'+ data +'\
                            </strong>\
                        </div>';
                $(".loader-bg").css('display', 'none');                        
                $('#token-message').html(notification);
                createAutoClosingAlert(".alert-success", 600000);
            }
        });
    });
});

function createAutoClosingAlert(selector, delay) {
    var alert = $(selector).alert();
    window.setTimeout(function() {
        alert.slideUp(500, function () {
            alert.hide();
        });
    }, delay);
}