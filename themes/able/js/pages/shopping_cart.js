$(document).ready(function () {

  permitir_checkout();

  $('.add_cart').click(function () {
    var product_id = $(this).data("productid");
    var product_name = $(this).data("productname");
    var product_price = $(this).data("price");
    var product_code = $(this).data("code");
    var shipping_details_id = $(this).data("shipping_details_id");
    var binary = $(this).data("binary");
    var residual = $(this).data("residual");
    var quantity = $('#' + product_id).val();
    if (quantity != '' && quantity > 0) {
      $.ajax({
        url: $('#site_url').val() + "cart/add",
        method: "POST",
        data: {
          product_id: product_id,
          product_name: product_name,
          product_price: product_price,
          quantity: quantity,
          code: product_code,
          residual: residual,
          binary: binary,
          shipping_details_id: shipping_details_id,
          [$("#csrf_data").attr("name")]: $("#csrf_data").val()
        },
        dataType: "json",
        success: function (data) {
          console.log(data);
          if( data.success ){

            $(".btn_open_modal").delay(1).addClass("animated infinite  swing faster")
            setTimeout(function () { $(".btn_open_modal").delay(1).removeClass("animated swing faster") }, 3500);
            $('#cart_details').html(data.view);
            $('#' + product_id).val('');
            $("#csrf_data").attr("name", data.csrf.name);
            $("#csrf_data").val(data.csrf.hash);
            $("#parcial_price").html(parseFloat(data.total_cart).toFixed(2));
            $('#total_cart_external').val(data.total_cart);
            permitir_checkout();
            notify($("#message_add").val(), 'inverse');
          }
          else{
            $('#' + product_id).val(data.stock);
            notify(data.message, 'inverse');
          }

        }
      });
    } else {
      swal({
        title: $('#zero_quantity').val(),
        confirmButtonText: $('#confirm_button').val(),
      });
    }
  });


  $(document).on('click', '.remove_inventory', function () {
    var row_id = $(this).attr("id");
    swal({
      title: $('#confirm_remove_inventory').val(),
      type: "warning",
      showCancelButton: true,
      confirmButtonClass: "btn-danger",
      confirmButtonText: $('#confirm_button').val(),
      cancelButtonText: $('#cancel_button').val(),
      closeOnConfirm: false
    },
      function () {
        $.ajax({
          url: $('#site_url').val() + "cart/remove",
          method: "POST",
          data: {
            row_id: row_id,
            [$("#csrf_data").attr("name")]: $("#csrf_data").val()
          },
          dataType: "json",
          success: function (data) {
            // alert("Product removed from cart");
            $('#cart_details').html(data.view);

            $("#csrf_data").attr("name", data.csrf.name);
            $("#csrf_data").val(data.csrf.hash);
            $("#parcial_price").html(parseFloat(data.total_cart).toFixed(2));
            $('#total_cart_external').val(data.total_cart);
            permitir_checkout();
          }
        });
        swal($('#correct_remove_inventory').val(), "", "success");
      });
    // } else {
    //   return false;
    // }
  });

  $(document).on("click", "#cancel_upgrade", function(){
    console.log("Cancel");
    swal({
      title: $('#cancel_upgrade').attr("data-message")+"?",
      type: "warning",
      showCancelButton: true,
      confirmButtonClass: "btn-danger",
      confirmButtonText: $('#confirm_button').val(),
      cancelButtonText: $('#cancel_button').val(),
      closeOnConfirm: false
    },
    function () {
      
      window.location.href= $('#site_url').val()+"user/upgrade/cancel_upgrade";
   
    }
  );
  });



  $(document).on('click', '#clear_cart', function () {

    swal({
        title: $('#confirm_clear_cart').val(),
        type: "warning",
        showCancelButton: true,
        confirmButtonClass: "btn-danger",
        confirmButtonText: $('#confirm_button').val(),
        cancelButtonText: $('#cancel_button').val(),
        closeOnConfirm: false
      },
      function () {
        $.ajax({
          url: $('#site_url').val() + "cart/clear",
          data: {
            [$("#csrf_data").attr("name")]: $("#csrf_data").val()
          },
          dataType: "json",
          success: function (data) {
            // alert("Your cart has been clear...");
            $('#cart_details').html(data.view);

            $("#csrf_data").attr("name", data.csrf.name);
            $("#csrf_data").val(data.csrf.hash);
            $("#parcial_price").html(parseFloat(data.total_cart).toFixed(2));
            $('#total_cart_external').val(data.total_cart);
          }
        });
        swal($('#correct_clear').val(), "", "success");
      }
    );

    // if (confirm("Are you sure you want to clear cart?")) {

    // } else {
    //   return false;
    // }
  });

});


function permitir_checkout() {
  console.log("permitir_checkout()");
  let precioParcial = $("#parcial_price").html();
  let precioTotal = $("#total_price").html();

  let total_cart = parseFloat($("#total_cart").html());
  let link = "";

  console.log("Precio parcial:", precioParcial);
  console.log("carro total:", total_cart);
  console.log("Precio total:", precioTotal);
  console.log(parseFloat(precioTotal));


  // Si se esta haciendo la compra de un plan
  if (precioParcial != undefined) {
    precioParcial = parseFloat(precioParcial.replace(",", ""));
    precioTotal = parseFloat(precioTotal);


    // Si ya se alcanzó el monto del plan
    if (precioParcial >= precioTotal) {

      $("#btn_go_checkout").attr("data-permitir_compra", "yes");
      $("#no_plan_price").hide();

    }
    // si no se alcanzo el monto del plan
    else {
      $("#btn_go_checkout").attr("data-permitir_compra", "no");
      $("#no_plan_price").show();

    
    }
  }

}

$("#category").select2({
  placeholder: 'seleccione',
  height: '1em'
});

function update_qty(value, element) {
  console.log("actualizar cantidad", value);
  let cantidad = parseInt(value);
  let rowid = $(element).attr("data-rowid");
  console.log(cantidad);
  if (!isNaN(cantidad) && cantidad > 0) {

    $.post(
      $("#site_url").val() + "/cart/update_qty",
      {
        rowid: rowid,
        qty: cantidad
      },
      function (data) {
        if(data.success){

          $('#cart_details').html(data.view);
          $("#csrf_data").attr("name", data.csrf.name);
          $("#csrf_data").val(data.csrf.hash);
          $("#parcial_price").html(parseFloat(data.total_cart).toFixed(2));
          $('#total_cart_external').val(data.total_cart);
          permitir_checkout();
        }
        else{
          $(element).val(data.stock);
          notify(data.message, 'inverse');
        }
      },
      "json"
    );
  }

}

/**
 * Une el select de la ventana modal, con el 
 * select del formulario de filtrado de productos
 */
function seleccionar_envio() {
  console.log("seleccionar_envio\n", $("#items_ubication").val());
  let envio = $("#items_ubication").val();
  $("#shopping_details").val(envio).trigger("change");
}

let UrlParams = new URLSearchParams(window.location.search);

if (
  UrlParams.get("shopping_details_id") == null
) {
  $('#modal_recogida').modal('show');
}

function change_shipping() {
  $.get(
    $('#site_url').val() + "cart/clear",
    function () {

      window.location.href = $('#site_url').val() + "cart";
    }
  );

}


function notify(message, type) {
  $.growl({
    message: message
  }, {
      type: type,
      allow_dismiss: false,
      label: 'Cancel',
      className: 'btn-xs btn-inverse',
      placement: {
        from: 'top',
        align: 'right'
      },
      delay: 2500,
      animate: {
        enter: 'animated fadeInRight',
        exit: 'animated fadeOutRight'
      },
      offset: {
        x: 30,
        y: 30
      }
    });
};

function send_purchase(){
  console.log("Permitir compra:"+$("#btn_go_checkout").attr("data-permitir_compra"));
  if($("#btn_go_checkout").attr("data-permitir_compra")=="yes"){
    if($('#total_cart_external').val() > 0) {
      window.location.href = $('#site_url').val() + "checkout/cart_list";
    } else {
      swal({
          title: $('#total_cart_zero').val(),
          confirmButtonText: $('#confirm_button').val(),
        });
    }
  }
}

$("#category").change(function(){
  console.log("Cambio", this.value);
  $("#btn-filter-form").click();
});








