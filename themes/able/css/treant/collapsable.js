
    var chart_config = {
        chart: {
            container: "#collapsable-example",

            scrollbar: "fancy",

            animateOnInit: true,
            
            node: {
                collapsable: true,
            },
            animation: {
                nodeAnimation: "easeOutBounce",
                nodeSpeed: 700,
                connectorsAnimation: "bounce",
                connectorsSpeed: 700
            }
        },
        nodeStructure: {
            image: "treant/img/malory.png",
            children: [
                {
                    image: "treant/img/lana.png",
                    // collapsed: true,
                    children: [
                        {
                            image: "treant/img/figgs.png",
                            children: [
                                {
                                    image: "treant/img/figgs.png",
                                    
                                },
                                {
                                    image: "treant/img/figgs.png"
                                }
                            ]

                        },
                        {
                            image: "treant/img/figgs.png"
                        }
                    ]
                },
                {
                    image: "treant/img/sterling.png",
                    children: [
                        {
                            image: "treant/img/woodhouse.png"
                        },
                        {
                            image: "treant/img/figgs.png",
                            children: [
                                {
                                    image: "treant/img/figgs.png",
                                    
                                },
                                {
                                    image: "treant/img/figgs.png",
                                    children: [
                                        {
                                            image: "treant/img/figgs.png",
                                            
                                        },
                                        {
                                            image: "treant/img/figgs.png",
                                            children: [
                                                {
                                                    image: "treant/img/figgs.png",
                                                    
                                                },
                                                {
                                                    image: "treant/img/figgs.png",
                                                    children: [
                                                        {
                                                            image: "treant/img/figgs.png",
                                                            
                                                        },
                                                        {
                                                            image: "treant/img/figgs.png"
                                                        }
                                                    ]
                                                }
                                            ]
                                        }
                                    ]
                                }
                            ]
                        }
                    ]
                }
            ]
        }
    };

/* Array approach
    var config = {
        container: "#collapsable-example",

        animateOnInit: true,
        
        node: {
            collapsable: true
        },
        animation: {
            nodeAnimation: "easeOutBounce",
            nodeSpeed: 700,
            connectorsAnimation: "bounce",
            connectorsSpeed: 700
        }
    },
    malory = {
        image: "treant/img/malory.png"
    },

    lana = {
        parent: malory,
        image: "treant/img/lana.png"
    }

    figgs = {
        parent: lana,
        image: "treant/img/figgs.png"
    }

    sterling = {
        parent: malory,
        childrenDropLevel: 1,
        image: "treant/img/sterling.png"
    },

    woodhouse = {
        parent: sterling,
        image: "treant/img/woodhouse.png"
    },

    pseudo = {
        parent: malory,
        pseudo: true
    },

    cheryl = {
        parent: pseudo,
        image: "treant/img/cheryl.png"
    },

    pam = {
        parent: pseudo,
        image: "treant/img/pam.png"
    },

    chart_config = [config, malory, lana, figgs, sterling, woodhouse, pseudo, pam, cheryl];

*/