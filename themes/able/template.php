<?php defined('BASEPATH') OR exit('No direct script access allowed');
/**
 * Admin Template
 */
?>
<!DOCTYPE html>
<html>

<head>
  <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <!-- <link rel="shortcut icon" type="image/x-icon" href="/themes/<?= $this->settings->theme ?>/images/icon.png?v=<?php echo $this->settings->site_version; ?>"> -->
  <!-- <link rel="icon" type="image/x-icon" href="/themes/<?= $this->settings->theme ?>/images/icon.png?v=<?php echo $this->settings->site_version; ?>"> -->
  <title><?php echo $page_title; ?> - <?php echo $this->settings->site_name; ?></title>
  <!-- Global site tag (gtag.js) - Google Analytics -->
  <script async src="https://www.googletagmanager.com/gtag/js?id=UA-143446119-1"></script>
  <script>
  window.dataLayer = window.dataLayer || [];

  function gtag() {
    dataLayer.push(arguments);
  }
  gtag('js', new Date());

  gtag('config', 'UA-143446119-1');
  </script>

  <?php // CSS files ?>
  <?php if (isset($css_files) && is_array($css_files)) : ?>
  <?php foreach ($css_files as $css) : ?>
  <?php if ( ! is_null($css)) : ?>
  <link rel="stylesheet" href="<?php echo $css; ?>?v=<?php echo $this->settings->site_version; ?>"><?php echo "\n"; ?>
  <?php endif; ?>
  <?php endforeach; ?>
  <?php endif; ?>

  <script src="/themes/<?= $this->settings->theme ?>/js/jquery.min.js"></script>

  <!--[if lt IE 9]>
    <script src="../assets/js/ie8-responsive-file-warning.js"></script>
    <![endif]-->

  <!-- HTML5 shim and Respond.js IE8 support of HTML5 elements and media queries -->
  <!--[if lt IE 9]>
    <script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
    <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
    <![endif]-->
  <?php if(isset($chat)) {
            if($chat) {?>
  <script>
  (function(d, w, c) {
    w.ChatraID = 'ayhprFCLzMGXvb54T';
    var s = d.createElement('script');
    w[c] = w[c] || function() {
      (w[c].q = w[c].q || []).push(arguments);
    };
    s.async = true;
    s.src = (d.location.protocol === 'https:' ? 'https:' : 'http:') +
      '//call.chatra.io/chatra.js';
    if (d.head) d.head.appendChild(s);
  })(document, window, 'Chatra');
  </script>
  <script>
  window.ChatraSetup = {
    chatWidth: 400,
    chatHeight: 550
  };
  </script>
  <?php } } ?>

</head>

<body>
  <input id="site_url" value="<?= base_url() ?>" type="hidden" />
  <!-- [ Pre-loader ] start -->
  <div class="loader-bg">
    <div class="loader-bar"></div>
  </div>
  <!-- [ Pre-loader ] end -->
  <div id="pcoded" class="pcoded">
    <div class="pcoded-overlay-box"></div>
    <div class="pcoded-container navbar-wrapper">
      <!-- [ Header ] start -->
      <nav class="navbar header-navbar pcoded-header">
        <div class="navbar-wrapper">
          <div class="navbar-logo">
            <a class="mobile-menu waves-effect waves-light" id="mobile-collapse" href="#!">
              <i class="feather icon-toggle-right"></i>
            </a>
            <a href="<?php echo base_url('admin/dashboard'); ?>" class="site_title">
              <img width="112" src="/themes/<?= $this->settings->theme ?>/images/logo.png"
                alt="<?php echo $this->settings->site_name; ?>">
            </a>
            <a class="mobile-options waves-effect waves-light">
              <i class="feather icon-more-horizontal"></i>
            </a>
          </div>
          <div class="navbar-container container-fluid">
            <ul class="nav-right">
              <li class="header-notification header-lang">
                <div class="dropdown-primary dropdown drop-lang">
                  <div class="dropdown-toggle" data-toggle="dropdown">
                    <i class="fa fa-language"></i>
                  </div>
                  <ul id="session-language-dropdown" class="show-notification notification-view dropdown-menu"
                    data-dropdown-in="fadeIn" data-dropdown-out="fadeOut" aria-labelledby="session-language">
                    <li class="lang-title">
                      <h6><?php echo lang('users title change_language')?></h6>
                    </li>
                    <?php foreach ($this->languages as $key=>$name) : ?>
                    <li>
                      <a href="#" rel="<?php echo $key; ?>">
                        <?php if ($key == $this->session->language) : ?>
                        <i class="fa fa-check selected-session-language"></i>
                        <?php endif; ?>
                        <span class="notification-msg"><?php echo $name; ?></span>
                      </a>
                    </li>
                    <?php endforeach; ?>
                  </ul>
                </div>
              </li>
              <li class="user-profile header-notification">
                <div class="dropdown-primary dropdown">
                  <div class="dropdown-toggle" data-toggle="dropdown">
                    <img src="/uploads/<?= ($this->user['image'] != "") ? $this->user['image'] : 'img.jpg' ?>" alt="..."
                      class="img-circle profile_img" style="height: 40px;width: 40px;">
                    <span><?= $this->user['username'] ?></span>
                    <i class="feather icon-chevron-down"></i>
                  </div>
                  <ul class="show-notification profile-notification dropdown-menu" data-dropdown-in="fadeIn"
                    data-dropdown-out="fadeOut">
                    <?php if($this->user['profile'] == 'user') {?>
                    <li>
                      <a href="<?php echo base_url('user/profile'); ?>">
                        <i class="feather icon-user"></i>
                        <?php echo lang('core button profile'); ?>
                      </a>
                    </li>
                    <?php } ?>
                    <?php if($this->user['profile'] == 'executive') {?>
                    <li>
                      <a href="<?php echo base_url('user/profile/executive'); ?>">
                        <i class="feather icon-user"></i>
                        <?php echo lang('core button profile'); ?>
                      </a>
                    </li>
                    <?php }?>
                    <?php if ($this->user['is_admin']) { ?>
                    <li>
                      <a href="<?php echo base_url('/admin/settings'); ?>">
                        <i class="feather icon-settings"></i>
                        <?php echo lang('admin button settings'); ?>
                      </a>
                    </li>
                    <?php } ?>
                    <li>
                      <a href="<?php echo base_url('logout'); ?>">
                        <i class="feather icon-log-out"></i>
                        <?php echo lang('core button logout'); ?>
                      </a>
                    </li>
                  </ul>
                </div>
              </li>
            </ul>
          </div>
        </div>
      </nav>
      <!-- [ Header ] end -->

      <div class="pcoded-main-container">
        <div class="pcoded-wrapper">
          <!--  sidebar menu  -->
          <?php  if($this->session->userdata('logged_in') ): ?>
          <?php $this->load->view('menu', NULL); ?>
          <?php elseif( strpos(base_url('cart'),current_url()) != false ): ?>
          <?php $this->load->view('sidebar_cart', NULL); ?>
          <?php  endif; ?>
          <!--  ./sidebar menu  -->

          <div class="pcoded-content">
            <!-- [ breadcrumb ] start -->
            <div class="page-header">
              <div class="page-block" >
                
              </div>
            </div>
            <!-- [ breadcrumb ] end -->
            <div class="pcoded-inner-content" style="margin-top: -80px;">
              <div class="main-body">
                <div class="page-wrapper">
                  <div class="page-body">

                    <?php // System messages ?>
                    <?php if ($this->session->flashdata('message')) : ?>
                    <div class="alert alert-success background-success">
                      <button type="button" class="close" data-dismiss="alert" aria-label="Close">&times;</button>
                      <span class="f-16 f-w-600"><?php echo $this->session->flashdata('message'); ?></span>
                    </div>
                    <?php endif; ?>
                    <?php if ($this->session->flashdata('error')) : ?>
                    <div class="alert alert-danger background-danger">
                      <button type="button" class="close" data-dismiss="alert" aria-label="Close">&times;</button>
                      <i class="fa fa-exclamation-triangle" aria-hidden="true"></i>
                      <span class="f-16 f-w-600"><?php echo $this->session->flashdata('error'); ?></span>
                    </div>
                    <?php endif; ?>
                    <?php if ($this->session->flashdata('warning')) : ?>
                    <div class="alert alert-warning background-warning">
                      <button type="button" class="close" data-dismiss="alert" aria-label="Close">&times;</button>
                      <i class="fa fa-info-circle" aria-hidden="true"></i>
                      <span class="f-16 f-w-600"><?php echo $this->session->flashdata('warning'); ?></span>
                    </div>
                    <?php elseif (validation_errors()) : ?>
                    <div class="alert alert-danger background-danger">
                      <button type="button" class="close" data-dismiss="alert" aria-label="Close">&times;</button>
                      <span class="f-16 f-w-600"><?php echo validation_errors(); ?></span>
                    </div>
                    <?php elseif ($this->error) : ?>
                    <div class="alert alert-danger background-danger">
                      <button type="button" class="close" data-dismiss="alert" aria-label="Close">&times;</button>
                      <span class="f-16 f-w-600"><?php echo $this->error; ?></span>
                    </div>
                    <?php endif; ?>
                    <?php // System messages ?>

                    <!-- [ page content ] start -->
                    <?php echo $content; ?>
                    <!-- [ page content ] end -->

                  </div>
                </div>
              </div>
            </div>
          </div>

        </div>
      </div>
    </div>
  </div>

  <?php // Javascript files ?>
  <?php if (isset($js_files) && is_array($js_files)) : ?>
  <?php foreach ($js_files as $js) : ?>
  <?php if ( ! is_null($js)) : ?>
  <?php echo "\n"; ?><script type="text/javascript"
    src="<?php echo $js; ?>?v=<?php echo $this->settings->site_version; ?>"></script><?php echo "\n"; ?>
  <?php endif; ?>
  <?php endforeach; ?>
  <?php endif; ?>
  <?php if (isset($js_files_i18n) && is_array($js_files_i18n)) : ?>
  <?php foreach ($js_files_i18n as $js) : ?>
  <?php if ( ! is_null($js)) : ?>
  <?php echo "\n"; ?><script type="text/javascript">
  <?php echo "\n" . $js . "\n"; ?>
  </script><?php echo "\n"; ?>
  <?php endif; ?>
  <?php endforeach; ?>
  <?php endif; ?>

</body>

</html>